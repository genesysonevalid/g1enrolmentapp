﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gv.ExodusBc.AD;

namespace Gv.ExodusBc.LN
{
   public class CATALOGSLN
    {
        public static List<EN.Catalogs> GetAll(int _CatalogTypesId)
        {
            try
            {
                return AD.CATALOGSAD.GetAll(_CatalogTypesId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
