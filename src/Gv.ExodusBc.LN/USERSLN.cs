﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.LN
{
    public class USERSLN
    {
        public static void DeleteUsers()
        {
            try
            {
                AD.USERSAD.DeleteUsers();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static void AddUsers(List<EN.Users> _usuarios)
        {
            try
            {
                AD.USERSAD.AddUsers(_usuarios);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static EN.Users GetUserByUserName(string userName)
        {
            try
            {
                return AD.USERSAD.GetUserByUserName(userName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static Boolean GetAccessDatebyUserId(long UserId, string _fechaAcceso, string _hora)
        {
            try
            {
                return AD.USERSAD.GetAccessDatebyUserId(UserId, _fechaAcceso, _hora);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static EN.Users GetAccessDatebyUserId(long UserId)
        {
            try
            {
                return AD.USERSAD.GetDiasbyUserId(UserId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static EN.Users EditUser(EN.Users users)
        {
            try
            {
                return AD.USERSAD.EditUser(users);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
