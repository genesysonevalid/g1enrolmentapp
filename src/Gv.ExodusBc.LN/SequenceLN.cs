﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.LN
{
    public class SequenceLN
    {
        public static long GetSequenceEnrolment()
        {
            try
            {
                return AD.SequenceAD.GetSequenceEnrolment();
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
        public static long GetSequenceConsultaAfis()
        {
            try
            {
                return AD.SequenceAD.GetSequenceConsultaAfis();
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
    }
}
