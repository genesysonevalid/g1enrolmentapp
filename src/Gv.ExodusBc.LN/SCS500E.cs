﻿using Cogent.Biometrics;
using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Gv.ExodusBc.EN;
using System.Runtime.InteropServices;
using System.Text;

namespace Gv.ExodusBc.LN
{
    public class CS500E
    {
        public int cs500eInicializado = -1;
        public int TotalDedos = 0;
        public bool nistGenerado = false;
        private bool SeCapturaHuella = true;
        private int captura = -1;

        public CS500E()
        {

        }

        public void comprobarEscaner()
        {
            try
            {
                if (SeCapturaHuella)
                {
                    if(cs500eInicializado < 0)
                    {
                        cs500eInicializado = CLSFPCaptureDllWrapper.CLS_Initialize();
                        if (cs500eInicializado >= 0)
                        {
                            CLSFPCaptureDllWrapper.CLS_SetLanguage(CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_RESOURCE.SPANISH);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void prepararCapturaHuellas()
        {
            CLSFPCaptureDllWrapper.CLS_Clear();
        }

        public int capturarHuellas(string numeroTransaccion)
        {
            try
            {
                CLSFPCaptureDllWrapper.CLS_SetTransNo(numeroTransaccion);
                captura = CLSFPCaptureDllWrapper.CLS_Capture(CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_TYPE.IDFLATS_SEPARATE_THUMBS);
                
                return captura;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public int capturarHuellasDerecha(string numeroTransaccion)
        {
            CLSFPCaptureDllWrapper.CLS_SetTransNo(numeroTransaccion);
            captura = CLSFPCaptureDllWrapper.CLS_CaptureFP(CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_TYPE.FLAT, CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.PLAIN_RIGHT_SLAP);
            return captura;
        }

        public int capturarHuellasIzquierda(string numeroTransaccion)
        {
            CLSFPCaptureDllWrapper.CLS_SetTransNo(numeroTransaccion);
            captura = CLSFPCaptureDllWrapper.CLS_CaptureFP(CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_TYPE.FLAT, CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.PLAIN_LEFT_SLAP);
            return captura;
        }

        public int capturarHuellasPulgares(string numeroTransaccion)
        {
            CLSFPCaptureDllWrapper.CLS_SetTransNo(numeroTransaccion);
            captura = CLSFPCaptureDllWrapper.CLS_CaptureFP(CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_TYPE.FLAT, CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.PLAIN_TWO_THUMBS);
            return captura;
        }

        public bool validarHuellasCapturadas()
        {
            return (captura == 0);
        }

        public int crearHuellasCapturadas(string numeroTransaccion, string path, int tipo, string metodo) //devolverá el numero de huellas capturadas
        {
            //Tipo (0 = Todos), (1 = Dedos derecha), (2 = Dedos izquierda), (3 = Dedos pulgares)
            string dirWSQ = Path.Combine(path, numeroTransaccion);
            string dirBmp = Path.Combine(path, numeroTransaccion);
            int huellasCapturadas = 0;

            if(tipo == 0 || tipo == 1)
            {
                if (Directory.Exists(dirWSQ))
                    Directory.Delete(dirWSQ, true);
                else
                    Directory.CreateDirectory(dirWSQ);

                if (Directory.Exists(dirBmp))
                    Directory.Delete(dirBmp, true);
                else
                    Directory.CreateDirectory(dirBmp);
            }
            else
            {
                if (!Directory.Exists(dirWSQ))
                    Directory.CreateDirectory(dirWSQ);

                if (!Directory.Exists(dirBmp))
                    Directory.CreateDirectory(dirBmp);
            }

            int respSaveWSQ = CLSFPCaptureDllWrapper.CLS_SaveAllImages(CLSFPCaptureDllWrapper.IMG_TYPE.WSQ, dirWSQ, CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_SAVE_IMG.SEG_FING_ONLY);
            int respSaveImages = CLSFPCaptureDllWrapper.CLS_SaveAllImages(CLSFPCaptureDllWrapper.IMG_TYPE.BMP, dirBmp, CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_SAVE_IMG.SEG_FING_ONLY);

            CLSFPCaptureDllWrapper.CLS_SetTransNo(numeroTransaccion);
            CLSFPCaptureDllWrapper.CLS_SaveAllImages(CLSFPCaptureDllWrapper.IMG_TYPE.WSQ, Path.Combine(dirWSQ, metodo), CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_SAVE_IMG.SEG_FING_ONLY);

            if (respSaveWSQ == 0 && respSaveImages == 0)
            {
                string[] archivoImagenes = Directory.GetFiles(dirBmp);
                CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER[] dedos = {
                    CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.RIGHT_THUMB,
                    CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.RIGHT_INDEX,
                    CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.RIGHT_MIDDLE,
                    CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.RIGHT_RING,
                    CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.RIGHT_LITTLE,
                    CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.LEFT_THUMB,
                    CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.LEFT_INDEX,
                    CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.LEFT_MIDDLE,
                    CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.LEFT_RING,
                    CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.LEFT_LITTLE
                };

                for (int i = 1; i <= 10; i++)
                {
                    //variables temporales para cada dedo
                    string fileWSQ = Path.Combine(dirWSQ, numeroTransaccion);
                    string fileBmp = Path.Combine(dirBmp, numeroTransaccion);
                    int calidad = 0;
                    CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_IMG_EXP estado = CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_IMG_EXP.NO_EXCEPTION; //el estado del dedo 0=present, 1=amputado, 2=vendado

                    byte[] wsqBytes;
                    byte[] imgBytes;

                    fileWSQ += string.Format("_{0}.wsq", i.ToString("00"));
                    fileBmp += string.Format("_{0}.bmp", i.ToString("00"));
                    var existe = archivoImagenes.Contains(fileBmp);

                    CLSFPCaptureDllWrapper.CLS_GetImageException(dedos[i - 1], ref estado);
                    if (existe)
                    {
                        huellasCapturadas += 1;
                        CLSFPCaptureDllWrapper.CLS_GetImageNFIQ(dedos[i - 1], ref calidad, CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_IMPRESSION_TYPE.PLAIN);
                        wsqBytes = File.ReadAllBytes(fileWSQ);
                        imgBytes = File.ReadAllBytes(fileBmp);
                    }

                }

                return huellasCapturadas;
            }
            return 0;
        }

        //public List<EN.GenesysOneEntities> getHuellasCapturas(string numeroTransaccion, string path, string metodo)
        //{
            //List<EN.EnrolmentTemplate> fingers = new List<EN.EnrolmentTemplate>();
            //try
            //{
            //    string dirWSQ = Path.Combine(path, numeroTransaccion + "\\" + metodo);
            //    if (Directory.Exists(dirWSQ))
            //    {
            //        string[] archivoImagenes = Directory.GetFiles(dirWSQ);
            //        CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER[] dedos = {
            //            CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.RIGHT_THUMB,
            //            CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.RIGHT_INDEX,
            //            CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.RIGHT_MIDDLE,
            //            CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.RIGHT_RING,
            //            CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.RIGHT_LITTLE,
            //            CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.LEFT_THUMB,
            //            CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.LEFT_INDEX,
            //            CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.LEFT_MIDDLE,
            //            CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.LEFT_RING,
            //            CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.LEFT_LITTLE
            //        };
            //        int dedo = 0;
            //        for (int i = 0; i < archivoImagenes.Length - 1; i++)
            //        {
            //            CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_IMG_EXP estado = CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_IMG_EXP.NO_EXCEPTION;
            //            string fileWSQ = Path.Combine(dirWSQ, numeroTransaccion);
            //            int calidad = 0;
            //            string fileWSQ = archivoImagenes[i + 1]; //string.Format("_{0}.wsq", (i + 1).ToString("00"));
            //            bool archivoWSQ = fileWSQ.EndsWith(".wsq");
            //            if (File.Exists(fileWSQ) && archivoWSQ)
            //            {
            //                CLSFPCaptureDllWrapper.CLS_GetImageException(dedos[dedo], ref estado);
            //                CLSFPCaptureDllWrapper.CLS_GetImageNFIQ(dedos[dedo], ref calidad, CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_IMPRESSION_TYPE.PLAIN);
            //                byte[] wsqBytes = File.ReadAllBytes(fileWSQ);

            //                EN.EnrolmentTemplate fp = new EN.EnrolmentTemplate();
            //                fp.index = dedo + 1;
            //                fp.score = calidad;
            //                fp.status = 1;
            //                fp.wsq = Convert.ToBase64String(wsqBytes);
            //                fingers.Add(fp);
            //                dedo++;
            //            }

            //        }
            //    }
            //    else fingers = null;
            //}
            //catch (Exception ex)
            //{
            //    fingers = null;
            //    throw ex;
            //}
            //return fingers;
        //}

        //public List<EN.EnrolmentTemplate> getHuellasCapturas(string path)
        //{
        //    List<EN.EnrolmentTemplate> fingers = new List<EN.EnrolmentTemplate>();
        //    try
        //    {
        //        string dirWSQ = path;
        //        if (Directory.Exists(dirWSQ))
        //        {
        //            string[] archivoImagenes = Directory.GetFiles(dirWSQ);
        //            CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER[] dedos = {
        //                CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.RIGHT_THUMB,
        //                CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.RIGHT_INDEX,
        //                CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.RIGHT_MIDDLE,
        //                CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.RIGHT_RING,
        //                CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.RIGHT_LITTLE,
        //                CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.LEFT_THUMB,
        //                CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.LEFT_INDEX,
        //                CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.LEFT_MIDDLE,
        //                CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.LEFT_RING,
        //                CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.LEFT_LITTLE
        //            };
        //            int dedo = 0;
        //            for (int i = 0; i < archivoImagenes.Length - 1; i++)
        //            {
        //                CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_IMG_EXP estado = CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_IMG_EXP.NO_EXCEPTION;
        //                int calidad = 0;
        //                string fileWSQ = archivoImagenes[i + 1];
        //                bool archivoWSQ = fileWSQ.EndsWith(".wsq");
        //                if (File.Exists(fileWSQ) && archivoWSQ)
        //                {
        //                    CLSFPCaptureDllWrapper.CLS_GetImageException(dedos[dedo], ref estado);
        //                    CLSFPCaptureDllWrapper.CLS_GetImageNFIQ(dedos[dedo], ref calidad, CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_IMPRESSION_TYPE.PLAIN);
        //                    byte[] wsqBytes = File.ReadAllBytes(fileWSQ);

        //                    EN.EnrolmentTemplate fp = new EN.EnrolmentTemplate();
        //                    fp.index = dedo + 1;
        //                    fp.score = calidad;
        //                    fp.status = 1;
        //                    fp.wsq = Convert.ToBase64String(wsqBytes);
        //                    fingers.Add(fp);
        //                    dedo++;
        //                }
        //            }
        //        }
        //        else fingers = null;
        //    }
        //    catch (Exception ex)
        //    {
        //        fingers = null;
        //        throw ex;
        //    }
        //    return fingers;
        //}

        public void desconectarEscaner()
        {
            if(cs500eInicializado != 1)
            {
                CLSFPCaptureDllWrapper.CLS_Clear();
                CLSFPCaptureDllWrapper.CLS_Terminate();
            }
        }

        public int SalvarHuellasCapturadas(string numeroTransaccion, string wsqPath ) //devolverá el numero de huellas capturadas
        {
           
            int huellasCapturadas = 0;

            if (!Directory.Exists(wsqPath))
                Directory.CreateDirectory(wsqPath);

            int respSaveWSQ = CLSFPCaptureDllWrapper.CLS_SaveAllImages(CLSFPCaptureDllWrapper.IMG_TYPE.WSQ, wsqPath, CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_SAVE_IMG.SEG_FING_ONLY);
         

            if (respSaveWSQ == 0)
            {
                string[] archivoImagenes = Directory.GetFiles(wsqPath,"*.wsq");
                huellasCapturadas = archivoImagenes.Count();

                //CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER[] dedos = {
                //    CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.RIGHT_THUMB,
                //    CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.RIGHT_INDEX,
                //    CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.RIGHT_MIDDLE,
                //    CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.RIGHT_RING,
                //    CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.RIGHT_LITTLE,
                //    CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.LEFT_THUMB,
                //    CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.LEFT_INDEX,
                //    CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.LEFT_MIDDLE,
                //    CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.LEFT_RING,
                //    CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.LEFT_LITTLE
                //};

                //for (int i = 1; i <= 10; i++)
                //{
                //    //variables temporales para cada dedo
                //    string fileWSQ = Path.Combine(wsqPath, numeroTransaccion);
                //    int calidad = 0;
                //    CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_IMG_EXP estado = CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_IMG_EXP.NO_EXCEPTION; //el estado del dedo 0=present, 1=amputado, 2=vendado
                //    CLSFPCaptureDllWrapper.CLS_GetImageException(dedos[i - 1], ref estado);

                //    byte[] wsqBytes;

                //    fileWSQ += string.Format("_{0}.wsq", i.ToString("00"));

                //    var existe = archivoImagenes.Contains(fileWSQ);
                //    if (existe)
                //    {
                //        huellasCapturadas += 1;
                //        CLSFPCaptureDllWrapper.CLS_GetImageNFIQ(dedos[i - 1], ref calidad, CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_IMPRESSION_TYPE.PLAIN);
                //        wsqBytes = File.ReadAllBytes(fileWSQ);
                //    }
                //}

                return huellasCapturadas;
            }
            return 0;
        }

        public class FuncionesScanner
        {
            [DllImport("CLSFPCaptureDLL.dll")]
            public static extern int CLS_Capture(int nCaptureType);
            [DllImport("CLSFPCaptureDLL.dll")]
            public static extern int CLS_CaptureFP(int nCaptureType, int nFGP);
            [DllImport("CLSFPCaptureDLL.dll")]
            public static extern int CLS_Clear();
            [DllImport("CLSFPCaptureDLL.dll")]
            public static extern int CLS_EnableManaulCut(int nTimeDuration);
            [DllImport("CLSFPCaptureDLL.dll")]
            public static extern int CLS_EnableNFIQ(int bEnable);
            [DllImport("CLSFPCaptureDLL.dll")]
            public static extern int CLS_ExtractFIR381(int nImgType, ref byte[] pBuf, ref int pLen);
            [DllImport("CLSFPCaptureDLL.dll")]
            public static extern int CLS_ExtractFMR378(ref byte[] pBuf, ref int pLen);
            [DllImport("CLSFPCaptureDLL.dll")]
            public static extern int CLS_ExtractMMF(int nFingPos, ref byte[] pBuf, ref int pLen);
            [DllImport("CLSFPCaptureDLL.dll")]
            public static extern int CLS_ExtractNFTS(int nFingPos, ref byte[] pBuf, ref int pLen);
            [DllImport("CLSFPCaptureDLL.dll")]
            public static extern int CLS_FreeBuffer(ref byte[] pBuf);
            [DllImport("CLSFPCaptureDLL.dll")]
            public static extern int CLS_GetFlowStatus();
            [DllImport("CLSFPCaptureDLL.dll")]
            public static extern int CLS_GetImage(int nImpType, int nFGP, int nImgSaveType, ref byte[] ppImgBuf, ref int pnImgBufLen);
            [DllImport("CLSFPCaptureDLL.dll")]
            public static extern int CLS_GetImageException(int nFingPos, ref int pnExpCode);
            [DllImport("CLSFPCaptureDLL.dll")]
            public static extern int CLS_GetImageNFIQ(int nFingPos, ref int pnNFIQ, int nImpressionType);
            [DllImport("CLSFPCaptureDLL.dll")]
            public static extern int CLS_GetImageQuality(int nFingPos, ref int pnQuality, int nImpressionType);
            [DllImport("CLSFPCaptureDLL.dll")]
            public static extern int CLS_GetImageSegInfo(int nFingPos, ref int pnLeft, ref int pnTop, ref int pnRight, ref int pnBottom);
            [DllImport("CLSFPCaptureDLL.dll")]
            public static extern int CLS_GetImageSize(int nFingPos, ref int pnWidth, ref int pnHeight, int nImpressionType);
            [DllImport("CLSFPCaptureDLL.dll")]
            public static extern int CLS_GetLastErr();
            [DllImport("CLSFPCaptureDLL.dll")]
            public static extern int CLS_Initialize();
            [DllImport("CLSFPCaptureDLL.dll")]
            public static extern int CLS_SaveAllImages(int nImageTyepe, [MarshalAs(UnmanagedType.LPStr)] StringBuilder lpszImgOutDir, int nSaveSegFing, bool bShowMsgBox);
            [DllImport("CLSFPCaptureDLL.dll")]
            public static extern int CLS_SetLanguage(int nLang);
            [DllImport("CLSFPCaptureDLL.dll")]
            public static extern int CLS_SetTransNo([MarshalAs(UnmanagedType.LPStr)] StringBuilder pszTrnasNo);
            [DllImport("CLSFPCaptureDLL.dll")]
            public static extern int CLS_ShowPalmCard(int nCardType);
            [DllImport("CLSFPCaptureDLL.dll")]
            public static extern int CLS_ShowTPCard(int nCardType);
            [DllImport("CLSFPCaptureDLL.dll")]
            public static extern int CLS_Terminate();
        }
    }
}
