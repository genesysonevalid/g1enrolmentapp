﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.LN
{
    public class PROFESSIONALASSOCIATIONSLN
    {
        public static List<EN.ProfessionalAssociations> GetAll()
        {
            try
            {
                return AD.PROFESSIONALASSOCIATIONSAD.GetAll();
            }
            catch (Exception ex) { throw ex; }
        }

        public static async Task<List<EN.ProfessionalAssociations>> GetAllAsync()
        {
            try
            {
                return await AD.PROFESSIONALASSOCIATIONSAD.GetAllAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
