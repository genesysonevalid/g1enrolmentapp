﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.LN
{
    public class USERROLESLN
    {
        public enum ROLDESCRIPCION
        {
            ADMINISTRADOR = 1,
            APROBACION = 2,
            TRAMITES = 3

        }

        public static EN.UserRoles GetById(long rolId)
        {
            try
            {
                return AD.USERROLESAD.GetById(rolId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
