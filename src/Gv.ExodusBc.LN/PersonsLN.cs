﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.LN
{
    public class PersonsLN
    {
        public enum PersonaHuellasEstado
        {
            HuellasCompletas = 0,
            SinHuellas = 1,
            TemporalmenteSinHuellas = 2,
            HuellasTemporalmenteIncompletas = 3,
            HuellasIncompletas = 4
        }
    }
}
