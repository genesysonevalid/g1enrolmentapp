﻿using Gv.ExodusBc.AD;
using Gv.ExodusBc.EN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.LN
{
    public class DocumentTypePersonLN
    {
        public enum DocumentListContract
        {
            CONTRATOPERSONANATURAL = 1,
            CONTRATOPERSONAJURIDICA = 2,
        }

        public static List<EN.DocumentTypePerson> GetAll()
        {
            try
            {
                return AD.DocumentTypePersonAD.GetAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<EN.DocumentTypePerson> GetDocumentByPersonType(int prsTypeId)
        {
            try
            {
                return AD.DocumentTypePersonAD.GetDocumentByPersonType(prsTypeId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int GetDocumentByPerson(int personTypeId)
        {
            try
            {
                return AD.DocumentTypePersonAD.GetDocumentByPerson(personTypeId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int GetDocumentByTypePerson(int personTypeId)
        {
            try
            {
                return DocumentTypePersonAD.GetDocumentByTypePerson(personTypeId);
            }
            catch (Exception) { throw; }
        }

    }
}
