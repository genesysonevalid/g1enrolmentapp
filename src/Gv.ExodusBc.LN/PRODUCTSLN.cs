﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.LN
{
    public class PRODUCTSLN
    {

        public enum VigenciaCertificados
        {
            UNO = 1,
            DOS = 2,
            TRES = 3,
            CUATRO = 4,
            CINCO = 5
        }

        public static List<EN.Products> GetAll()
        {
            try
            {
                return AD.PRODUCTSAD.GetAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<EN.Products> GetProductByTypeProduct(int prsTypeId)
        {
            try
            {
                return AD.PRODUCTSAD.GetProductByTypeProduct(prsTypeId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static string GetProductByDescription(int personTypeId, int productId)
        {
            try
            {
                return AD.PRODUCTSAD.GetProductByDescription(personTypeId, productId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
