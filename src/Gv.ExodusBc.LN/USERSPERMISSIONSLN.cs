﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.LN
{
    public class USERSPERMISSIONSLN
    {
        public static List<EN.UserPermissions> GetPermisosUsers(int rolId)
        {
            try
            {
                return AD.USERSPERMISSIONSAD.GetPermisosUsers(rolId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
