﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.LN
{
    public class LOGIN_FAIL_LOGLN
    {
        public static EN.LoginFailLog GetFailLogbyUserId(long UserId)
        {
            try
            {
                return AD.LOGINFAILLOGAD.GetFailLogbyUserId(UserId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static EN.LoginFailLog EditLoginFailLog(EN.LoginFailLog loginFailLog)
        {
            try
            {
                return AD.LOGINFAILLOGAD.EditFailCount(loginFailLog);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
