﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.LN
{
    public class CERTIFICATEOFFICELN
    {
        public static string GetCodeCertOffice(int certOfId)
        {
            try
            {
                return AD.CERTIFICATEOFFICEAD.GetCodeCertOffice(certOfId);
            }
            catch (Exception ex) { throw ex; }
        }
    }
}
