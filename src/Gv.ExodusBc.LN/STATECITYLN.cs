﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gv.ExodusBc.AD;

namespace Gv.ExodusBc.LN
{
    public static class STATECITYLN
    {
        public static List<EN.StateCity> GetStateCitybyStateCountryId(int _stateCountryId)
        {
            try
            {
                return AD.STATECITYAD.GetStateCitybyStateCountryId(_stateCountryId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
