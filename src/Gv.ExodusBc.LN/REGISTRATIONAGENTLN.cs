﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.LN
{
    public class REGISTRATIONAGENTLN
    {
        public static EN.RegistrationAgent GetCodeAgent(int agentId)
        {
            try
            {
                return AD.REGISTRATIONAGENTAD.GetCodeAgent(agentId);
            }
            catch (Exception ex) { throw ex; }

        }
    }
}
