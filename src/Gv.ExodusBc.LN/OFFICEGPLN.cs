﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.LN
{
    public class OFFICEGPLN
    {
        public static EN.OfficeGP GetOfficegpbyOfficeCode(int office_code)
        {
            try
            {
                return AD.OFFICEGPAD.GetOfficeGPbyOfficeCode(office_code);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
