﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.LN
{
    public class OFFICELN
    {
        public static EN.Office GetOfficebyOfficeCode(long office_code)
        {
            try
            {
                return AD.OFFICEAD.GetOfficebyOfficeCode(office_code);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static List<EN.Office> GetAll()
        {
            try
            {
                return AD.OFFICEAD.GetAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
