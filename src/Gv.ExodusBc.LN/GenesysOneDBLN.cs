﻿using Gv.ExodusBc.EN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.LN
{
    public class GenesysOneDBLN
    {

        public static void SincronizarCatalogos(
           List<DetailLicense> detailLicense,
           List<License> license,
           List<LoginFailLog> loginFailLog,
           List<ModuleOptions> moduleOptions,
           List<Modules> modules,
           List<Office> office,
           List<RegistrationAgent> registrationAgent,
           List<Settings> settings,
           List<UserPermissions> userPermissions,
           List<UserRoles> userRoles,
           List<Users> users,
           List<WebServiceTokens> webServiceTokens,
           List<WorkStations> workStations,
           List<Catalogs> catalogs,
           List<CatalogStatus> catalogStatus,
           List<CatalogTypes> catalogTypes,
           List<CertificateOffice> certificateOffice,
           List<Country> country,
           List<DocumentType> documentType,
           List<DocumentTypePerson> documentTypePerson,
           List<Nationality> nationality,
           List<Neighborhood> neighborhood,
           List<OfficeGP> OfficeGP,
           List<PersonProfessions> personProfessions,
           List<PersonType> personType,
           List<Products> products,
           List<ProfessionalAssociations> professionalAssociations,
           List<ReasonRejection> reasonRejection,
           List<ReasonReview> reasonReview,
           List<RegistrationAuthority> registrationAuthority,
           List<RequestStatus> requestStatus,
           List<StateCity> stateCity,
           List<StateCountry> stateCountry,
           List<TransactionStatus> transactionStatus,
           List<ZipCodes> ZipCodes
           )
        {
            try
            {
                AD.GenesysOneDB.SincronizarCatalogos
                 (
                    detailLicense,
                    license,
                    loginFailLog,
                    moduleOptions,
                    modules,
                    office,
                    registrationAgent,
                    settings,
                    userPermissions,
                    userRoles,
                    users,
                    webServiceTokens,
                    workStations,
                    catalogs,
                    catalogStatus,
                    catalogTypes,
                    certificateOffice,
                    country,
                    documentType,
                    documentTypePerson,
                    nationality,
                    neighborhood,
                    OfficeGP,
                    personProfessions,
                    personType,
                    products,
                    professionalAssociations,
                    reasonRejection,
                    reasonReview,
                    registrationAuthority,
                    requestStatus,
                    stateCity,
                    stateCountry,
                    transactionStatus,
                    ZipCodes);
            }
            catch (Exception ex) { throw ex; }
        }

        public static void ActualizarFechaUltimaSincronizacion()
        {
            try
            {
                AD.GenesysOneDB.ActualizarFechaUltimaSincronizacion();
            }
            catch (Exception e) { throw e; }
        }
    }
}
