﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.LN
{
    public class DOCUMENTTYPELN
    {
       public enum DocumentType
        {
            IDENTIDAD = 1,
            RTN = 2,
            PASAPORTE = 3,
            IDENTIDAD_GTM = 5,

        }

        public enum DocumentTypeCaracters
        {
            IDENTIDAD = 13,
            RTN = 14,
            RESIDENTE = 15
        }


        public static List<EN.DocumentType> GetAll()
        {
            try
            {
                return AD.DOCUMENTTYPEAD.GetAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<EN.DocumentType> GetDocumentById()
        {
            try
            {
                return AD.DOCUMENTTYPEAD.GetDocumentById();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static async Task<List<EN.DocumentType>> GetDocumentByIdAsync()
        {
            try
            {
                return await AD.DOCUMENTTYPEAD.GetDocumentByIdAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static async Task<List<EN.DocumentType>> GetDocumentByIdNew(int IdDoc)
        {
            try
            {
                return await AD.DOCUMENTTYPEAD.GetDocumentByIdNew(IdDoc);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }




        public static List<EN.DocumentType> GetDocumentByTypeDocumentId()
        {
            try
            {
                return AD.DOCUMENTTYPEAD.GetDocumentByTypeDocumentId();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static async Task<List<EN.DocumentType>> GetDocumentByTypeDocumentIdAsync()
        {
            try
            {
                return await AD.DOCUMENTTYPEAD.GetDocumentByTypeDocumentIdAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
