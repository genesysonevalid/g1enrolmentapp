﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.LN
{
    public class SETTINGSLN
    {
        public static List<EN.Settings> GetSettings()
        {
            try
            {
                return AD.SETTINGSAD.GetSettings();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static EN.Settings GetSettingsValor( string valor)
        {
            try
            {
                return AD.SETTINGSAD.GetSettingsValor(valor);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetValue(string name)
        {
            try
            {
                return AD.SETTINGSAD.GetValue(name);
            }
            catch (Exception ex) { throw ex; }
        }

        public static void ActualizarFechaUltimaSincronizacion()
        {
            try
            {
                AD.SETTINGSAD.ActualizarFechaUltimaSincronizacion();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
