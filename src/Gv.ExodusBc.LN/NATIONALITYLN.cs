﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.LN
{
    public class NATIONALITYLN
    {
        public static List<EN.Nationality> GetAll()
        {
            try
            {
                return AD.NATIONALITYAD.GetAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static EN.Nationality GetAlpha3byNationalityId(int Id)
        {
            try
            {
                return AD.NATIONALITYAD.GetAlpha3byNationalityId(Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
