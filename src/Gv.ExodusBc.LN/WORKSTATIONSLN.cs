﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.LN
{
    public class WORKSTATIONSLN
    {
        public static EN.WorkStations GeWorkstationbyHostName(string Hostname)
        {
            try
            {
                return AD.WORKSTATIONSAD.GeWorkstationbyHostName(Hostname);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public static EN.WorkStations VerificarEstacionLicenciada(string codigoProcesador, ref string mensaje)
        {

            var objEstacion = AD.WORKSTATIONSAD.GeEstacionByCodigo(codigoProcesador);

            if (objEstacion != null)
            {
                var objLicencia = AD.WORKSTATIONSAD.GeLicenciasByEstacionId(Convert.ToInt32(objEstacion.WorkStationsId));

                if (objLicencia != null)
                {
                    return objEstacion;
                }
                else
                {
                    //Mensaje Maquina no Licenciada
                    mensaje = "La estación de trabajo no esta licenciada. Licenciar la estación desde la administración web.";
                    return null;
                }
            }
            else
            {
                //Mensaje Maquina no está registrada
                mensaje = "La estación de trabajo no está registrada. Registra la estación desde la administración web.";
                return null;
            }
        }
    }
   
}
