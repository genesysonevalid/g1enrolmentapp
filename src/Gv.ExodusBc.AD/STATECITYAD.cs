﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gv.ExodusBc.EN;

namespace Gv.ExodusBc.AD
{
    public static class STATECITYAD
    {
        public static List<StateCity> GetStateCitybyStateCountryId(int _stateCountryId)
        {
            try
            {
                using (var db = new GenesysOneDB())
                {
                    var obj = db.StateCity.Where(x => x.FKStateCountryId == _stateCountryId);

                    if (obj == null)
                        return null;
                    else
                        return obj.ToList();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
