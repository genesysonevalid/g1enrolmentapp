﻿using Gv.ExodusBc.EN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.AD
{
    public class REASONREJECTIONAD
    {
        public static List<ReasonRejection> GetAll()
        {
            try
            {
                using (var db = new GenesysOneDB())
                {
                    var obj = db.ReasonRejection.ToList();
                    return obj;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
