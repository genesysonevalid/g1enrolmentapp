﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.AD
{
    public class SETTINGSAD
    {
        public static List<EN.Settings> GetSettings()
        {
            try
            {
                using (var db = new GenesysOneDB())
                {
                    var obj = db.Settings.ToList();
                    return obj.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static EN.Settings GetSettingsValor(string valor)
        {
            try
            {
                using (var db = new GenesysOneDB())
                {
                    var obj = db.Settings.Where(x => x.Value == valor).FirstOrDefault();
                    return obj;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetValue(string nombre)
        {
            try
            {
                using (var db = new GenesysOneDB())
                {
                    var obj = db.Settings.Where(x => x.Name == nombre).FirstOrDefault();
                    return obj.Value;
                }
            }
            catch (Exception ex) { throw ex; }
        }


        public static void ActualizarFechaUltimaSincronizacion()
        {
            try
            {
                using (var db = new GenesysOneDB())
                {
                    var registro = db.Settings.FirstOrDefault(x => x.Name == "ULTIMASINCRONIZACION");
                    if (registro != null)
                    {
                        registro.Value = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss");
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex) { throw ex; }
        }



    }
}
