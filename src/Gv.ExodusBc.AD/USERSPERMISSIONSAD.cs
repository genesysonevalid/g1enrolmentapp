﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.AD
{
    public class USERSPERMISSIONSAD
    {
        public static List<EN.UserPermissions> GetPermisosUsers(int rolId)
        {
            try
            {
                using (var db = new GenesysOneDB())
                {
                    var lstPermisos = db.UserPermissions.Where(x => x.FKRolId == rolId);
                    if (lstPermisos.Any()) return lstPermisos.ToList();
                    else return new List<EN.UserPermissions>();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
