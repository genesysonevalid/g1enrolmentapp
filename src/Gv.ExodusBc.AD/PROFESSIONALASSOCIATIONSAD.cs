﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.AD
{
    public class PROFESSIONALASSOCIATIONSAD
    {
        public static List<EN.ProfessionalAssociations> GetAll()
        {
            try
            {
                using (var db = new GenesysOneDB())
                {
                    var obj = db.ProfessionalAssociations.ToList();
                    return obj;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static async Task<List<EN.ProfessionalAssociations>> GetAllAsync()
        {
            try
            {
                using (var db = new GenesysOneDB())
                {
                    var obj = await db.ProfessionalAssociations.ToListAsync();
                    return obj;
                }
            }
            catch (Exception ex) { throw ex; }
        }
    }
}
