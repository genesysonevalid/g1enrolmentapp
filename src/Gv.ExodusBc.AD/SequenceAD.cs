﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.AD
{
    public class SequenceAD
    {
        public static long GetSequenceEnrolment()
        {
            try
            {
                using (SqlConnection cnn = new SqlConnection(ConfiguracionAD.GetConnectionStringSQL()))
                {
                    cnn.Open();
                    string SQL = "SELECT NEXT VALUE FOR dbo.sqEnrolmentId";
                    SqlCommand cmd = new SqlCommand(SQL);
                    cmd.Connection = cnn;
                    SqlDataReader DataReader = cmd.ExecuteReader();
                    DataReader.Read();
                    long Ultimo = (long)(DataReader[0]);
                    return Ultimo;
                }

            }
            catch (Exception)
            {

                throw;
            }
        }
        public static int GetSequenceConsultaAfis()
        {
            try
            {
                using (SqlConnection cnn = new SqlConnection(ConfiguracionAD.GetConnectionStringSQL()))
                {
                    cnn.Open();
                    string SQL = "SELECT NEXT VALUE FOR dbo.sqConsultaAfis";
                    SqlCommand cmd = new SqlCommand(SQL);
                    cmd.Connection = cnn;
                    SqlDataReader DataReader = cmd.ExecuteReader();
                    DataReader.Read();
                    int Ultimo = Convert.ToInt32(DataReader[0]);
                    return Ultimo;
                }

            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
