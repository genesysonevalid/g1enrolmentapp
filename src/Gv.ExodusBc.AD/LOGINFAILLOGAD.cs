﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.AD
{
    public class LOGINFAILLOGAD
    {
        public static EN.LoginFailLog GetFailLogbyUserId(long UserId)
        {
            try
            {
                using (var db = new GenesysOneDB())
                {
                    var obj = db.LoginFailLog.Where(x => x.FKUserId == UserId).FirstOrDefault();
                    return obj;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static EN.LoginFailLog EditFailCount(EN.LoginFailLog loginFailLog)
        {
            try
            {
                using (var db = new GenesysOneDB())
                {
                    db.Entry(loginFailLog).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    return loginFailLog;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
