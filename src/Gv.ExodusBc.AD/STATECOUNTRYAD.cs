﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gv.ExodusBc.EN;

namespace Gv.ExodusBc.AD
{
    public static class STATECOUNTRYAD
    {
        public static List<StateCountry> GetAll()
        {
            try
            {
                using (var db = new GenesysOneDB())
                {
                    var obj = db.StateCountry.ToList();
                    return obj;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
