﻿using Gv.ExodusBc.EN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.AD
{
    public class PRODUCTSAD
    {
        public static List<Products> GetAll()
        {
            try
            {
                using (var db = new GenesysOneDB())
                {
                    var obj = db.Products.Where(x => x.FKCatalogStatusId == 1).ToList();
                    return obj;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<Products> GetProductByTypeProduct(int prsTypeId)
        {
            try
            {
                using (var db = new GenesysOneDB())
                {
                    var obj = db.Products.Where(x => x.FKCatalogStatusId == 1 && x.FKPersonTypeId == prsTypeId).ToList();
                    return obj;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetProductByDescription(int personTypeId, int productId)
        {
            try
            {
                using (var db = new GenesysOneDB())
                {
                    var obj = db.Products.Where(x => x.FKCatalogStatusId == 1 && x.FKPersonTypeId == personTypeId && x.ProductId == productId).FirstOrDefault();
                    if (obj == null) return null;
                    else return obj.Description;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
