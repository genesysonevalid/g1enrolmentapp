﻿using System.Data.Entity.Core.EntityClient;

namespace Gv.ExodusBc.AD
{
    public class ConfiguracionAD
    {
        public static string ConnectionStringSQL = string.Empty;

        public static string ConnectionString { get; private set; }

        /// <summary>
        /// Crea el string de conexion a la base de datos SQLite utilizando el formato de EF.
        /// </summary>
        /// <param name="connectionString">El string de conexion con la ruta del archivo SQLite y su contraseña.</param>
        public static void GetConnectionString(string connectionString)
        {
            var entityBuilder = new EntityConnectionStringBuilder
            {
                ProviderConnectionString = connectionString,
                Provider = "System.Data.SQLite.EF6",
                Metadata = @"res://*/GenesysOneDBContext.csdl|res://*/GenesysOneDBContext.ssdl|res://*/GenesysOneDBContext.msl"
            };
            ConnectionString = entityBuilder.ConnectionString;
        }

        /// <summary>
        /// Crea el string de conexion a la base de datos SQL utilizando una cadena de conexión.
        /// </summary>
        public static string GetConnectionStringSQL()
        {
            //**PRODUCCIÓN**
            return ConnectionStringSQL = "Data Source=192.168.0.143;Initial Catalog=GV_GENESYSONE_VALID;User ID=sa;Password=V1s10nSQL@2k22";
            //=========================================================================================================================

            //**TESTING**
            //return ConnectionStringSQL = "Data Source=192.168.1.37;Initial Catalog=GV_GENESYSONE_VALID;User ID=sa;Password=Temp0ral16";
            //=========================================================================================================================
        }

    }
}
