﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.AD
{
    public class NATIONALITYAD
    {
        public static List<EN.Nationality> GetAll()
        {
            try
            {
                using (var db = new GenesysOneDB())
                {
                    var obj = db.Nationality.ToList();
                    return obj;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static EN.Nationality GetAlpha3byNationalityId(int Id)
        {
            try
            {
                using (var db = new GenesysOneDB())
                {
                    var obj = db.Nationality.Where(x=>x.NationalityId==Id).FirstOrDefault();
                    return obj;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
