﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.AD
{
    public class DocumentTypePersonAD
    {
        public static List<EN.DocumentTypePerson> GetAll()
        {
            try
            {
                using (var db = new GenesysOneDB())
                {
                    var obj = db.DocumentTypePerson.ToList();
                    return obj;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static List<EN.DocumentTypePerson> GetDocumentByPersonType(int personTypeId)
        {
            try
            {
                using (var db = new GenesysOneDB())
                {
                    var obj = db.DocumentTypePerson.Where(x => x.FKPersonType == personTypeId && x.FKCatalogSatatusId == 1).ToList();
                    return obj;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int GetDocumentByTypePerson(int personTypeId)
        {
            try
            {
                using (var db = new GenesysOneDB())
                {
                    var rq = db.DocumentTypePerson.Where(x => x.FKPersonType == personTypeId && x.FKCatalogSatatusId == 1).Count();
                    if (rq != 0) return rq;
                    else return 0;
                }
            }
            catch (Exception ex) { throw ex; }
        }



        public static int GetDocumentByPerson(int personTypeId)
        {
            try
            {
                using (var db = new GenesysOneDB())
                {
                    var objDocument = db.DocumentTypePerson.Where(x => x.FKPersonType == personTypeId && x.FKCatalogSatatusId == 1).ToList();
                    if (objDocument.Count() > 0) return objDocument.Count();
                    else return 0;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }




    }
}
