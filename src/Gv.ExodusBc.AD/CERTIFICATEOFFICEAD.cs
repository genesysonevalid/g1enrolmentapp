﻿using System;
using System.Linq;



namespace Gv.ExodusBc.AD
{
    public class CERTIFICATEOFFICEAD
    {
        public static string GetCodeCertOffice(int certId)
        {
            try
            {
                using (var db = new GenesysOneDB())
                {
                    var obj = db.CertificateOffice.Where(x => x.CertificateOfficeId == certId).FirstOrDefault();
                    if (obj == null) return null;
                    else return obj.Code;
                }
            }
            catch (Exception ex) { throw ex; }
        }
    }
}
