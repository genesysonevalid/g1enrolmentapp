﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.AD
{
    public class OFFICEAD
    {
        public static EN.Office GetOfficebyOfficeCode(long Office_code)
        {
            try
            {
                using (var db = new GenesysOneDB())
                {
                    var obj = db.Office.Where(x => x.OfficeId == Office_code).FirstOrDefault();
                    return obj;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<EN.Office> GetAll()
        {
            try
            {
                using (var db = new GenesysOneDB())
                {
                    var obj = db.Office.ToList();
                    return obj;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
