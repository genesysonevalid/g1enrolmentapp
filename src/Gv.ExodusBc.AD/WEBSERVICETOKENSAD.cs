﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.AD
{
    public class WEBSERVICETOKENSAD
    {
        public static EN.WebServiceTokens GetWebServiceTokenbyConsumidor(string consumidor)
        {
            try
            {
                using (var db = new GenesysOneDB())
                {
                    var obj = db.WebServiceTokens.Where(x => x.Consumer == consumidor && x.FKCatalogStatusId == 1).FirstOrDefault();
                    return obj;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
  }
