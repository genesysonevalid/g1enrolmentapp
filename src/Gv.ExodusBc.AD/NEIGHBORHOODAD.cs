﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.AD
{
    public class NEIGHBORHOODAD
    {
        public static List<EN.Neighborhood> GetNeighborhoodbyStateCityId(int _stateCityId)
        {
            try
            {
                using (var db = new GenesysOneDB())
                {
                    var obj = db.Neighborhood.Where(x => x.FKStateCityId == _stateCityId).ToList();
                    return obj;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
