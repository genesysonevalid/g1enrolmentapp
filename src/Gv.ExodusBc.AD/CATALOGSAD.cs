﻿using System;
using System.Collections.Generic;
using System.Linq;
using Gv.ExodusBc.EN;

namespace Gv.ExodusBc.AD
{
    public class CATALOGSAD
    {
        public static List<Catalogs> GetAll(int _CatalogTypesId)
        {
            try
            {
                using (var db = new GenesysOneDB())
                {
                    var obj = db.Catalogs.Where(x => x.FKCatalogTypesId == _CatalogTypesId).ToList();
                    return obj;
                }
            }
            catch (Exception ex) { throw ex; }
        }
    }
}
