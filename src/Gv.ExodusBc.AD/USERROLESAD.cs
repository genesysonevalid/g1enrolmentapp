﻿using Gv.ExodusBc.EN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.AD
{
    public class USERROLESAD
    {
        public static EN.UserRoles GetById(long rolId)
        {
            try
            {
                using (var db = new GenesysOneDB())
                {
                    var obj = db.UserRoles.Where(x => x.UserRolesId == rolId).FirstOrDefault();
                    return obj;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
