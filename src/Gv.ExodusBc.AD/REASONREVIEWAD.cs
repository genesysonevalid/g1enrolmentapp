﻿using Gv.ExodusBc.EN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.AD
{
    public class REASONREVIEWAD
    {
        public static List<ReasonReview> GetAll()
        {
            try
            {
                using (var db = new GenesysOneDB())
                {
                    var obj = db.ReasonReview.ToList();
                    return obj;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
