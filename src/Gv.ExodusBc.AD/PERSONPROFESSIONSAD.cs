﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.AD
{
    public class PERSONPROFESSIONSAD
    {
        public static List<EN.PersonProfessions> GetAll()
        {
            try
            {
                using (var db = new GenesysOneDB())
                {
                    var obj = db.PersonProfessions.Where(x => x.FKCatalogStatusId == 1).ToList();
                    return obj;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
