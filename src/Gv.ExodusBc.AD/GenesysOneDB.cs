﻿using Gv.ExodusBc.EN;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace Gv.ExodusBc.AD
{

    partial class GenesysOneDBContext : DbContext
    {
        /// <summary>
        /// Clase parcial para agregar un constructor que permita configurar el DbContext pasando 
        /// como parametro el string de conexion.
        /// </summary>
        /// <param name="connectionString"></param>
        public GenesysOneDBContext(string connectionString)
        : base(connectionString)
        {
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }
    }


    /// <summary>
    /// Clase que hereda de GenesysOneEntities que permite realizar consultas utilizando el string
    /// de conexion definido en la configuracion de esta librería (ConfiguracionAD.ConnectionString).
    /// </summary>
    public class GenesysOneDB : GenesysOneDBContext
    {
        public GenesysOneDB()
          : base(ConfiguracionAD.ConnectionString)
        {

        }

        /// <summary>
        /// Sincroniza los catalogos de la base de datos local con los del servidor central.
        /// </summary>
        public static void SincronizarCatalogos(
                List<DetailLicense> detailLicense,
                List<License> license,
                List<LoginFailLog> loginFailLog,
                List<ModuleOptions> moduleOptions,
                List<Modules> modules,
                List<Office> office,
                List<RegistrationAgent> registrationAgent,
                List<Settings> settings,
                List<UserPermissions> userPermissions,
                List<UserRoles> userRoles,
                List<Users> users,
                List<WebServiceTokens> webServiceTokens,
                List<WorkStations> workStations,
                List<Catalogs> catalogs,
                List<CatalogStatus> catalogStatus,
                List<CatalogTypes> catalogTypes,
                List<CertificateOffice> certificateOffice,
                List<Country> country,
                List<DocumentType> documentType,
                List<DocumentTypePerson> documentTypePerson,
                List<Nationality> nationality,
                List<Neighborhood> neighborhood,
                List<OfficeGP> officeGP,
                List<PersonProfessions> personProfessions,
                List<PersonType> personType,
                List<Products> products,
                List<ProfessionalAssociations> professionalAssociations,
                List<ReasonRejection> reasonRejection,
                List<ReasonReview> reasonReview,
                List<RegistrationAuthority> registrationAuthority,
                List<RequestStatus> requestStatus,
                List<StateCity> stateCity,
                List<StateCountry> stateCountry,
                List<TransactionStatus> transactionStatus,
                List<ZipCodes> zipCodes)

        {
            using (var db = new GenesysOneDB())
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        db.Configuration.AutoDetectChangesEnabled = false;
                        db.Database.ExecuteSqlCommand($"DELETE FROM {nameof(EN.DetailLicense)}");
                        db.Database.ExecuteSqlCommand($"DELETE FROM {nameof(EN.License)}");
                        db.Database.ExecuteSqlCommand($"DELETE FROM {nameof(EN.LoginFailLog)}");
                        db.DetailLicense.AddRange(detailLicense);
                        db.License.AddRange(license);
                        db.LoginFailLog.AddRange(loginFailLog);

                        db.Database.ExecuteSqlCommand($"DELETE FROM {nameof(EN.ModuleOptions)}");
                        db.Database.ExecuteSqlCommand($"DELETE FROM {nameof(EN.Modules)}");
                        db.Database.ExecuteSqlCommand($"DELETE FROM {nameof(EN.Office)}");
                        db.ModuleOptions.AddRange(moduleOptions);
                        db.Modules.AddRange(modules);
                        db.Office.AddRange(office);

                        db.Database.ExecuteSqlCommand($"DELETE FROM {nameof(EN.RegistrationAgent)}");
                        db.Database.ExecuteSqlCommand($"DELETE FROM {nameof(EN.Settings)}");
                        db.Database.ExecuteSqlCommand($"DELETE FROM {nameof(EN.UserPermissions)}");
                        db.RegistrationAgent.AddRange(registrationAgent);
                        db.Settings.AddRange(settings);
                        db.UserPermissions.AddRange(userPermissions);

                        db.Database.ExecuteSqlCommand($"DELETE FROM {nameof(EN.UserRoles)}");
                        db.Database.ExecuteSqlCommand($"DELETE FROM {nameof(EN.Users)}");
                        db.Database.ExecuteSqlCommand($"DELETE FROM {nameof(EN.WebServiceTokens)}");
                        db.Database.ExecuteSqlCommand($"DELETE FROM {nameof(EN.WorkStations)}");
                        db.UserRoles.AddRange(userRoles);
                        db.Users.AddRange(users);
                        db.WebServiceTokens.AddRange(webServiceTokens);
                        db.WorkStations.AddRange(workStations);


                        db.Database.ExecuteSqlCommand($"DELETE FROM {nameof(EN.Catalogs)}");
                        db.Database.ExecuteSqlCommand($"DELETE FROM {nameof(EN.CatalogStatus)}");
                        db.Database.ExecuteSqlCommand($"DELETE FROM {nameof(EN.CatalogTypes)}");
                        db.Database.ExecuteSqlCommand($"DELETE FROM {nameof(EN.CertificateOffice)}");
                        db.Database.ExecuteSqlCommand($"DELETE FROM {nameof(EN.Country)}");
                        db.Catalogs.AddRange(catalogs);
                        db.CatalogStatus.AddRange(catalogStatus);
                        db.CatalogTypes.AddRange(catalogTypes);
                        db.CertificateOffice.AddRange(certificateOffice);
                        db.Country.AddRange(country);

                        db.Database.ExecuteSqlCommand($"DELETE FROM {nameof(EN.DocumentType)}");
                        db.Database.ExecuteSqlCommand($"DELETE FROM {nameof(EN.DocumentTypePerson)}");
                        db.DocumentType.AddRange(documentType);
                        db.DocumentTypePerson.AddRange(documentTypePerson);

                        db.Database.ExecuteSqlCommand($"DELETE FROM {nameof(EN.Nationality)}");
                        db.Database.ExecuteSqlCommand($"DELETE FROM {nameof(EN.Neighborhood)}");
                        db.Nationality.AddRange(nationality);
                        db.Neighborhood.AddRange(neighborhood);

                        db.Database.ExecuteSqlCommand($"DELETE FROM {nameof(EN.OfficeGP)}");
                        db.Database.ExecuteSqlCommand($"DELETE FROM {nameof(EN.PersonProfessions)}");
                        db.Database.ExecuteSqlCommand($"DELETE FROM {nameof(EN.PersonType)}");
                        db.Database.ExecuteSqlCommand($"DELETE FROM {nameof(EN.Products)}");
                        db.Database.ExecuteSqlCommand($"DELETE FROM {nameof(EN.ProfessionalAssociations)}");
                        db.OfficeGP.AddRange(officeGP);
                        db.PersonProfessions.AddRange(personProfessions);
                        db.PersonType.AddRange(personType);
                        db.Products.AddRange(products);
                        db.ProfessionalAssociations.AddRange(professionalAssociations);

                        db.Database.ExecuteSqlCommand($"DELETE FROM {nameof(EN.ReasonRejection)}");
                        db.Database.ExecuteSqlCommand($"DELETE FROM {nameof(EN.ReasonReview)}");
                        db.Database.ExecuteSqlCommand($"DELETE FROM {nameof(EN.RegistrationAuthority)}");
                        db.Database.ExecuteSqlCommand($"DELETE FROM {nameof(EN.RequestStatus)}");
                        db.ReasonRejection.AddRange(reasonRejection);
                        db.ReasonReview.AddRange(reasonReview);
                        db.RegistrationAuthority.AddRange(registrationAuthority);
                        db.RequestStatus.AddRange(requestStatus);

                        db.Database.ExecuteSqlCommand($"DELETE FROM {nameof(EN.StateCity)}");
                        db.Database.ExecuteSqlCommand($"DELETE FROM {nameof(EN.StateCountry)}");
                        db.Database.ExecuteSqlCommand($"DELETE FROM {nameof(EN.TransactionStatus)}");
                        db.Database.ExecuteSqlCommand($"DELETE FROM {nameof(EN.ZipCodes)}");
                        db.StateCity.AddRange(stateCity);
                        db.StateCountry.AddRange(stateCountry);
                        db.TransactionStatus.AddRange(transactionStatus);
                        db.ZipCodes.AddRange(zipCodes);

                        db.SaveChanges();

                        transaction.Commit();
                    }
                    catch (Exception e)
                    {
                        transaction.Rollback();
                        throw e;
                    }
                }
            }
        }


        public static void ActualizarFechaUltimaSincronizacion()
        {
            try
            {
                using (var db = new GenesysOneDB())
                {
                    var registro = db.Settings.FirstOrDefault(x => x.Name == "ULTIMASINCRONIZACION");
                    if (registro != null)
                    {
                        registro.Value = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss");
                        //registro.Value = ultimaFecha;
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex) { throw ex; }
        }

    }
}
