﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gv.ExodusBc.EN;

namespace Gv.ExodusBc.AD
{
    public class REGISTRATIONAUTHORITYAD
    {
        public static string GetCodeRA(int raId)
        {
            try
            {
                using (var db = new GenesysOneDB())
                {
                    var obj = db.RegistrationAuthority.Where(x => x.RegistrationAuthorityId == raId).FirstOrDefault();
                    if (obj == null) return null;
                    else return obj.Code;
                }
            }
            catch (Exception ex) { throw ex; }
        }
    }
}
