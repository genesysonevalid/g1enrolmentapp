﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gv.ExodusBc.EN;

namespace Gv.ExodusBc.AD
{
    public class REGISTRATIONAGENTAD
    {
        public static List<RegistrationAgent> GetAll()
        {
            try
            {
                using (var db = new GenesysOneDB())
                {
                    var obj = db.RegistrationAgent.ToList();
                    return obj.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static RegistrationAgent GetCodeAgent(int agentId)
        {
            try
            {
                using (var db = new GenesysOneDB())
                {
                    var obj = db.RegistrationAgent.Where(x => x.FKUserId == agentId).FirstOrDefault();
                    if (obj == null) return null;
                    else return obj;
                }
            }
            catch (Exception ex) { throw ex; }
          
        }
    }
}
