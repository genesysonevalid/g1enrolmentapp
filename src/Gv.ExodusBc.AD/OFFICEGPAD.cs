﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.AD
{
    public class OFFICEGPAD
    {
        public static EN.OfficeGP GetOfficeGPbyOfficeCode(int Office_code)
        {
            try
            {
                using (var db = new GenesysOneDB())
                {
                    var obj = db.OfficeGP.Where(x => x.FKOfficeId == Office_code).FirstOrDefault();
                    return obj;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}
