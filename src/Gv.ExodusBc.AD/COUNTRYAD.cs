﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gv.ExodusBc.EN;

namespace Gv.ExodusBc.AD
{
    public static class COUNTRYAD
    {
        public static List<Country> GetAll()
        {
            try
            {
                using (var db = new GenesysOneDB())
                {
                    var obj = db.Country.ToList();
                    return obj.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
