﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.AD
{
    public class DOCUMENTTYPEAD
    {
        public static List<EN.DocumentType> GetAll()
        {
            try
            {
                using (var db = new GenesysOneDB())
                {
                    var obj = db.DocumentType.ToList();
                    return obj;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<EN.DocumentType> GetDocumentById()
        {
            try
            {
                using (var db = new GenesysOneDB())
                {
                    var obj = db.DocumentType.Where(x => x.DocumentTypeId == 2).ToList();
                    return obj;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static async Task<List<EN.DocumentType>> GetDocumentByIdAsync()
        {
            try
            {
                using (var db = new GenesysOneDB())
                {
                    var obj = await db.DocumentType.Where(x => x.DocumentTypeId == 2).ToListAsync();
                    return obj;
                }
            }
            catch (Exception ex) { throw ex; }
        }


        public static async Task<List<EN.DocumentType>> GetDocumentByIdNew(int IdDoc)
        {
            try
            {
                using (var db = new GenesysOneDB())
                {
                    var obj = await db.DocumentType.Where(x => x.DocumentTypeId == IdDoc).ToListAsync();
                    return obj;
                }
            }
            catch (Exception ex) { throw ex; }
        }




        public static List<EN.DocumentType> GetDocumentByTypeDocumentId()
        {
            try
            {
                var ids = new List<long> { 1, 2, 3, 5, 7 };
                using (var db = new GenesysOneDB())
                {
                    var obj = db.DocumentType.Where(x => ids.Contains(x.DocumentTypeId)).ToList();
                    return obj;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public static async Task<List<EN.DocumentType>> GetDocumentByTypeDocumentIdAsync()
        {
            try
            {
                var ids = new List<long> { 1, 3, 5, 7 };
                using (var db = new GenesysOneDB())
                {
                    var obj = await db.DocumentType.Where(x => ids.Contains(x.DocumentTypeId)).ToListAsync();

                    if (obj == null) return null;
                    else return obj;
                }
            }
            catch (Exception ex) { throw ex; }
        }

    }
}
