﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.AD
{
    public class USERSAD
    {
        public static void DeleteUsers()
        {
            try
            {
                using (var db = new GenesysOneDB())
                {
                    if (db.Users.Any())
                    {
                        db.Users.RemoveRange(db.Users.ToList());
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static EN.Users GetUserByUserName(string userName)
        {
            try
            {
                using (var db = new GenesysOneDB())
                {
                    var obj = db.Users.Where(x => x.Name == userName).FirstOrDefault();
                    return obj;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public static EN.Users EditUser(EN.Users users)
        {
            try
            {
                using (var db = new GenesysOneDB())
                {
                    db.Entry(users).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    return users;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static void AddUsers(List<EN.Users> lst)
        {
            try
            {
                using (var db = new GenesysOneDB())
                {
                    db.Users.AddRange(lst);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static Boolean GetAccessDatebyUserId(long UserId, string fechaAcceso, string _hora)
        {
            try
            {
                using (var db = new GenesysOneDB())
                {
                    TimeSpan _horaAcceso = Convert.ToDateTime(_hora).TimeOfDay;
                    DateTime _fechaAcceso = Convert.ToDateTime(fechaAcceso);
                    var obj = db.Users.Where(x => x.UserId == UserId).FirstOrDefault();

                    DateTime fechaInicio = Convert.ToDateTime(obj.StartTime.ToString("%H:%M"));
                    DateTime fechaFinal = Convert.ToDateTime(obj.EndTime.ToString("%H:%M"));
                    if (obj.StartTime != null && obj.EndTime != null)
                    {
                        string horaInicio = (obj.StartTime).ToString("HH:mm");
                        TimeSpan _hI = Convert.ToDateTime(horaInicio).TimeOfDay;
                        string horaFinal = (obj.EndTime).ToString("HH:mm");
                        TimeSpan _hF = Convert.ToDateTime(horaFinal).TimeOfDay;
                        if (_fechaAcceso >= fechaInicio.Date && _fechaAcceso <= fechaFinal.Date)
                        {
                            if (_horaAcceso >= _hI && _horaAcceso <= _hF) return true;
                            else return false;
                        }
                        else return false;
                    }
                    else return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static EN.Users GetDiasbyUserId(long UserId)
        {
            try
            {
                using (var db = new GenesysOneDB())
                {
                    var obj = db.Users.Where(x => x.UserId == UserId).FirstOrDefault();
                    return obj;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static EN.Users GetUserById(long UserId)
        {
            try
            {
                using (var db = new GenesysOneDB())
                {
                    var obj = db.Users.Where(x => x.UserId == UserId).FirstOrDefault();
                    return obj;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
