﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gv.ExodusBc.EN;


namespace Gv.ExodusBc.AD
{
    public class WORKSTATIONSAD
    {
        public static WorkStations GeWorkstationbyHostName(string Hostname)
        {
            try
            {
                using (var db = new GenesysOneDB())
                {
                    var objWorkstation = db.WorkStations.Where(x => x.HostName == Hostname && x.FKCatalogStatusId == 1).FirstOrDefault();

                    if (objWorkstation == null)
                        return null;
                    else
                        return objWorkstation;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static EN.WorkStations GeEstacionByCodigo(string codigo)
        {

            try
            {
                using (var db = new GenesysOneDB())
                {

                    var objEstacion = db.WorkStations.Where(x => x.Code == codigo && x.FKCatalogStatusId == 1).FirstOrDefault();

                    if (objEstacion == null)
                        return null;
                    else
                        return objEstacion;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        public static EN.DetailLicense GeLicenciasByEstacionId(int estacionId)
        {

            try
            {
                using (var db = new GenesysOneDB())
                {
                    var objLicencia = db.DetailLicense.Where(x => x.FKWorkStationId == estacionId && x.FKCatalogStatusId == 1).FirstOrDefault();
                    if (objLicencia == null)
                        return null;
                    else
                        return objLicencia;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }



        }

    }
}
