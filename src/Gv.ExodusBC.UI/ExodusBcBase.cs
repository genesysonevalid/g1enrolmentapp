﻿using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using Gv.ExodusBc.EN;
using Gv.ExodusBc.LN;
using Gv.ExodusBc.UI.WebReference;
using Newtonsoft.Json.Linq;
using System;
using System.Data;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Management;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Configuration;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.Utils;
using Gv.Utilidades;
using System.Security.Cryptography;
using Newtonsoft.Json;
using System.Runtime.Serialization;
using Wsq2Bmp;
using System.Reflection;
using DevExpress.XtraGrid.Columns;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;

namespace Gv.ExodusBc.UI
{
    public class ExodusBcBase
    {
        public static ExodusBcService web = new ExodusBcService();

        [DllImport("kernel32.dll")]
        static extern bool SetLocalTime(ref SYSTIME time);

        private struct SYSTIME
        {
            public short Year;
            public short Month;
            public short DayOfWeek;
            public short Day;
            public short Hour;
            public short Minutes;
            public short Seconds;
            public short Milliseconds;
        }


        public class ClasesPersonalizadas
        {
            public class HuellasCapturar
            {
                public string ID { get; set; }
                public int TotalDedos { get; set; }
                public int EstadoHuellas { get; set; }
                public string ObservacionHuellas { get; set; }
                public List<Dedos> Dedos { get; set; }
            }
            public class Dedos
            {
                public int Dedo { get; set; }
                public int Calidad { get; set; }
                public int Estado { get; set; }
                public byte[] WSQ { get; set; }
                public byte[] Imagen { get; set; }

                public Dedos(int _dedo, int _calidad, int _estado, byte[] _wsq, byte[] _imagen)
                {
                    this.Dedo = _dedo;
                    this.Calidad = _calidad;
                    this.Estado = _estado;
                    this.WSQ = _wsq;
                    this.Imagen = _imagen;
                }

            }

        }
        public class Helper
        {
            public static void actualizarHoraFechaSesion()
            {
                VariablesGlobales.FechaHora = DateTime.Now;
            }

            public static string imageToBase64(Image img)
            {
                string base64 = string.Empty;
                try
                {
                    using (MemoryStream m = new MemoryStream())
                    {
                        Image imageIn = img;
                        MemoryStream ms = new MemoryStream();
                        imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                        byte[] imageBytes = ms.ToArray();
                        base64 = Convert.ToBase64String(imageBytes);
                    }
                    return base64;
                }
                catch { return base64; }
            }
            public static Image ImageFromFile(string filePath)
            {
                if (filePath == null) throw new ArgumentNullException("filePath");
                using (FileStream stream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
                {
                    return Image.FromStream(stream);
                }
            }

            public static Image base64ToImage(string base64String)
            {
                byte[] imageBytes = Convert.FromBase64String(base64String);
                using (var ms = new MemoryStream(imageBytes, 0, imageBytes.Length))
                {
                    Image image = Image.FromStream(ms, true);
                    return image;
                }
            }

            public static Image byteToImage(byte[] byteImage)
            {
                using (var ms = new MemoryStream(byteImage))
                {
                    Image image = Image.FromStream(ms, true);
                    return image;
                }
            }

            public static void LimpiarControles(Control.ControlCollection controles)
            {
                foreach (Control ctr in controles)
                {
                    if (ctr is TextEdit)
                        (ctr as TextEdit).Text = String.Empty;
                    else if (ctr is LookUpEdit)
                        (ctr as LookUpEdit).EditValue = null;
                    else if (ctr is PictureEdit)
                        (ctr as PictureEdit).EditValue = null;
                    else if (ctr is CheckEdit)
                        (ctr as CheckEdit).Checked = false;
                    else if (ctr is RadioGroup)
                        (ctr as RadioGroup).SelectedIndex = -1;
                    else if (ctr is CheckedListBoxControl)
                        (ctr as CheckedListBoxControl).SelectedIndex = -1;
                    else if (ctr is DateTimePicker)
                        (ctr as DateTimePicker).Value = DateTime.Now;
                    else if (ctr.HasChildren)
                    {
                        LimpiarControles(ctr.Controls);
                    }
                }
            }

            public static string imgBase64 = "iVBORw0KGgoAAAANSUhEUgAAABoAAAAaCAYAAACpSkzOAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAFFSURBVEhL7ZXNSsNAEIBT9VG0jyF4ErznJczuzMabiOQxLHhQehPfySL2HQoi1Zl1FtfsT9akBw/9YEi6mZmPJrNJtWfndF13JKdFUP4BHWbfvwpRCkGDedca7+q6PpTlJABworRZUc1L01zNZTkPNTdU8OlCaXzMyawEzJuXv75s22O5HIcbUvKHK/KKozJu6Eu8WEhKGgV4HykMZGkJbhDxVNLScDNuGjb4keUkDeK5tBomKwN83onEkZOFMVLiKJNNlDisDPApLjFbgPZCUqeRGWEbbkAkfRxDEheTZP0d78U2sjZOlpbghp8JnT+E1/4oy0ncdNkBSUxjqWwmb+Feg3CEczJ6Md9IWhz+ppDo9Xdhep8kZRqvJSUNf0+oeD0kcfRltOeWJbfOwmNNRQut2zNZysKN+V/Qs70tluz5Z1TVFwlD1CTtrleUAAAAAElFTkSuQmCC";

            public static DateTime GetFecha(string fecha)
            {
                DateTime rs = DateTime.Now;
                try
                {
                    string[] splitFecha = fecha.Split('/');
                    fecha = splitFecha[2] + "-" + splitFecha[1] + "-" + splitFecha[0];
                    rs = new DateTime(Int32.Parse(splitFecha[2]), Int32.Parse(splitFecha[1]), Int32.Parse(splitFecha[0]));
                }
                catch { }
                return Convert.ToDateTime(rs.Date.ToString("yyyy-MM-dd"));
            }

            public static bool PingHost(string nameOrAddress)
            {
                bool pingable = false;
                Ping pinger = null;

                try
                {
                    pinger = new Ping();
                    PingReply reply = pinger.Send(nameOrAddress);
                    pingable = reply.Status == IPStatus.Success;
                }
                catch (PingException) { }
                finally
                {
                    if (pinger != null)
                    {
                        pinger.Dispose();
                    }
                }
                return pingable;
            }

            public static string GetCodigoProcesador()
            {
                string id = string.Empty;
                try
                {
                    ManagementObjectSearcher mbs = new ManagementObjectSearcher("Select * from Win32_BIOS");
                    ManagementObjectCollection mbsList = mbs.Get();

                    foreach (ManagementObject mo in mbsList)
                        id = mo["SerialNumber"].ToString();
                    return id;
                }
                catch (Exception) { return id; }
            }

            public static string GetVersion()
            {
                Assembly assembly = Assembly.GetEntryAssembly();
                FileVersionInfo fileVersion = FileVersionInfo.GetVersionInfo(assembly.Location);
                assembly = null;
                return fileVersion.FileVersion;
            }

            public static void EncriptarCadenaConexion()
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                string provider = "DataProtectionConfigurationProvider";
                ConfigurationSection connstring = config.ConnectionStrings;
                if (!connstring.SectionInformation.IsProtected)
                {
                    connstring.SectionInformation.ProtectSection(provider);
                    connstring.SectionInformation.ForceSave = true;
                    config.Save(ConfigurationSaveMode.Full);
                }
            }

            public class Combobox
            {

                public static void setCombo(LookUpEdit Combo, int _catalogTypesId)
                {
                    Combo.Properties.Columns.Clear();
                    Combo.Properties.DataSource = null;
                    Combo.Properties.DataSource = CATALOGSLN.GetAll(_catalogTypesId);
                    Combo.Properties.ValueMember = "CatalogsId";
                    Combo.Properties.DisplayMember = "Name";
                    Combo.Properties.Columns.Add(new LookUpColumnInfo("Name", 0, "opcion"));
                }
                public static void setComboProfesiones(LookUpEdit Combo)
                {
                    Combo.Properties.Columns.Clear();
                    Combo.Properties.DataSource = null;
                    Combo.Properties.DataSource = PERSONPROFESSIONSLN.GetAll();
                    Combo.Properties.ValueMember = "PersonProfessionId";
                    Combo.Properties.DisplayMember = "Name";
                    Combo.Properties.Columns.Add(new LookUpColumnInfo("Name", 0, "opcion"));
                }

                public static void GetColegioProfesional(LookUpEdit Combo)
                {
                    Combo.Properties.Columns.Clear();
                    Combo.Properties.DataSource = null;
                    Combo.Properties.DataSource = PROFESSIONALASSOCIATIONSLN.GetAll();
                    Combo.Properties.ValueMember = "ProfessionalAssociationsId";
                    Combo.Properties.DisplayMember = "Description";
                    Combo.Properties.Columns.Add(new LookUpColumnInfo("Description", 0, "Descripción"));
                }





                public static async Task GetColegioProfesionalAsync(LookUpEdit Combo)
                {
                    Combo.Properties.Columns.Clear();
                    Combo.Properties.DataSource = null;
                    Combo.Properties.DataSource = await PROFESSIONALASSOCIATIONSLN.GetAllAsync();
                    Combo.Properties.ValueMember = "ProfessionalAssociationsId";
                    Combo.Properties.DisplayMember = "Name";
                    Combo.Properties.Columns.Add(new LookUpColumnInfo("Description", 0, "Descripción")); 
                }

                public static void SetPersonaHuellaEstados(LookUpEdit Combo)
                {
                    List<PersonaHuellaEstados> lst = new List<PersonaHuellaEstados>();
                    lst.Add(new PersonaHuellaEstados(0, "Huellas Completas"));
                    lst.Add(new PersonaHuellaEstados(1, "Sin Huellas"));
                    lst.Add(new PersonaHuellaEstados(2, "Temporalmente Sin Huellas"));
                    lst.Add(new PersonaHuellaEstados(3, "Huellas Temporalmente Incompletas"));
                    lst.Add(new PersonaHuellaEstados(4, "Huellas Incompletas"));

                    Combo.Properties.DataSource = null;
                    Combo.Properties.DataSource = lst;
                    Combo.Properties.ValueMember = "Id";
                    Combo.Properties.DisplayMember = "Valor";
                    Combo.Properties.Columns.Clear();
                    Combo.Properties.Columns.Add(new LookUpColumnInfo("Valor", 0, "Valor"));
                    Combo.ItemIndex = 0;
                }

                public static void setEstadosBandeja(LookUpEdit Combo)
                {
                    List<EstadosSolicitud> lst = new List<EstadosSolicitud>();
                    lst.Add(new EstadosSolicitud(6, "Instalados"));
                    lst.Add(new EstadosSolicitud(12, "Certificado revocado por el titular"));
                    lst.Add(new EstadosSolicitud(13, "Certificado revocado por un agente de registro"));
                    lst.Add(new EstadosSolicitud(16, "Solicitud rechazada en la emisión por Valid"));


                    Combo.Properties.DataSource = null;
                    Combo.Properties.DataSource = lst;
                    Combo.Properties.ValueMember = "Id";
                    Combo.Properties.DisplayMember = "Valor";
                    Combo.Properties.Columns.Clear();
                    Combo.Properties.Columns.Add(new LookUpColumnInfo("Valor", 0, "Valor"));
                    Combo.ItemIndex = 0;
                }

                public static void setComboCountry(LookUpEdit Combo)
                {

                    Combo.Properties.DataSource = null;
                    Combo.Properties.DataSource = COUNTRYLN.GetAll();
                    Combo.Properties.ValueMember = "CountryId";
                    Combo.Properties.DisplayMember = "Name";
                    Combo.Properties.Columns.Clear();
                    Combo.Properties.Columns.Add(new LookUpColumnInfo("Name", 0, "opcion"));

                }
                public static void setComboStateCountry(LookUpEdit Combo)
                {

                    Combo.Properties.DataSource = null;
                    Combo.Properties.DataSource = STATECOUNTRYLN.GetAll();
                    Combo.Properties.ValueMember = "StateCountryId";
                    Combo.Properties.DisplayMember = "Name";
                    Combo.Properties.Columns.Clear();
                    Combo.Properties.Columns.Add(new LookUpColumnInfo("Name", 0, "opcion"));

                }

                public static void setComboStateCity(LookUpEdit Combo, int _stateCountryId)
                {

                    Combo.Properties.DataSource = null;
                    Combo.Properties.DataSource = STATECITYLN.GetStateCitybyStateCountryId(_stateCountryId);
                    Combo.Properties.ValueMember = "StateCityId";
                    Combo.Properties.DisplayMember = "Name";
                    Combo.Properties.Columns.Clear();
                    Combo.Properties.Columns.Add(new LookUpColumnInfo("Name", 0, "opcion"));

                }
                public static void setComboNationality(LookUpEdit Combo)
                {

                    Combo.Properties.DataSource = null;
                    Combo.Properties.DataSource = NATIONALITYLN.GetAll();
                    Combo.Properties.ValueMember = "NationalityId";
                    Combo.Properties.DisplayMember = "Name";
                    Combo.Properties.Columns.Clear();
                    Combo.Properties.Columns.Add(new LookUpColumnInfo("Name", 0, "opcion"));

                }
                public static void setComboTipoDocumento(LookUpEdit Combo)
                {

                    Combo.Properties.DataSource = null;
                    Combo.Properties.DataSource = DOCUMENTTYPELN.GetAll();
                    Combo.Properties.ValueMember = "DocumentTypeId";
                    Combo.Properties.DisplayMember = "Description";
                    Combo.Properties.Columns.Clear();
                    Combo.Properties.Columns.Add(new LookUpColumnInfo("Description", 0, "opcion"));
                    Combo.ItemIndex = 0;

                }

                public static void GetComboDocumentByPersonType(LookUpEdit Combo, int PersonTypeId)
                {
                    Combo.Properties.DataSource = null;
                    Combo.Properties.DataSource = DocumentTypePersonLN.GetDocumentByPersonType(PersonTypeId);
                    Combo.Properties.ValueMember = "DocumentTypePersonId";
                    Combo.Properties.DisplayMember = "Name";
                    Combo.Properties.Columns.Clear();
                    Combo.Properties.Columns.Add(new LookUpColumnInfo("Name", 0, "opcion"));
                    Combo.ItemIndex = 0;

                }


                public static void setComboNeighborhood(LookUpEdit Combo, int _stateCityId)
                {

                    Combo.Properties.DataSource = null;
                    Combo.Properties.DataSource = NEIGHBORHOODLN.GetNeighborhoodbyStateCityId(_stateCityId);
                    Combo.Properties.ValueMember = "NeighborhoodId";
                    Combo.Properties.DisplayMember = "Name";
                    Combo.Properties.Columns.Clear();
                    Combo.Properties.ImmediatePopup = true;
                    Combo.Properties.Columns.Add(new LookUpColumnInfo("Name", 0, "opcion"));

                }

                public static void setColonias(SearchLookUpEdit Combo, int _stateCityId)
                {

                    Combo.Properties.DataSource = null;
                    Combo.Properties.DataSource = NEIGHBORHOODLN.GetNeighborhoodbyStateCityId(_stateCityId);
                    Combo.Properties.ValueMember = "NeighborhoodId";
                    Combo.Properties.DisplayMember = "Name";

                    Combo.Properties.BestFitMode = BestFitMode.BestFit;
                    Combo.Properties.PopupFilterMode = PopupFilterMode.Contains;
                    GridColumn column = Combo.Properties.View.Columns.AddField("NeighborhoodId");

                    column.Visible = false;
                    column = Combo.Properties.View.Columns.AddField("Name");
                    column.Caption = "Name";
                    column.Visible = true;

                }



                public static void setComboProducts(LookUpEdit Combo)
                {
                    Combo.Properties.Columns.Clear();
                    Combo.Properties.DataSource = null;
                    Combo.Properties.DataSource = PRODUCTSLN.GetAll();
                    Combo.Properties.ValueMember = "ProductId";
                    Combo.Properties.DisplayMember = "Code";
                    Combo.Properties.Columns.Add(new LookUpColumnInfo("Code", 0, "CÓDIGO"));
                    Combo.Properties.Columns.Add(new LookUpColumnInfo("Description", 0, "DESCRIPCIÓN DE PRODUCTO"));
                }
                public static void setComboOffice(LookUpEdit Combo)
                {
                    Combo.Properties.Columns.Clear();
                    Combo.Properties.DataSource = null;
                    Combo.Properties.DataSource = OFFICELN.GetAll();
                    Combo.Properties.ValueMember = "OfficeId";
                    Combo.Properties.DisplayMember = "Name";
                    //Combo.Properties.Columns.Add(new LookUpColumnInfo("OfficeId", 0, "CÓDIGO"));
                    Combo.Properties.Columns.Add(new LookUpColumnInfo("Name", 0, "Nombre de la Oficina"));
                }

                public static void GetComboProductsByProductId(LookUpEdit Combo, int prsTypePerson)
                {
                    Combo.Properties.Columns.Clear();
                    Combo.Properties.DataSource = null;
                    var lstProduct = PRODUCTSLN.GetProductByTypeProduct(prsTypePerson);
                    var lstFinal = lstProduct.Select(x => new Products
                    {
                        ProductId = x.ProductId,
                        Code = x.Code,
                        Description = x.Description.ToUpper()
                    }).ToList();
                    Combo.Properties.DataSource = lstFinal;
                    Combo.Properties.ValueMember = "ProductId";
                    Combo.Properties.DisplayMember = "Code";
                    Combo.Properties.Columns.Add(new LookUpColumnInfo("Code", 0, "CÓDIGO"));
                    Combo.Properties.Columns.Add(new LookUpColumnInfo("Description", 0, "DESCRIPCIÓN DE PRODUCTO"));
                }

                public static void GetCostumers(LookUpEdit Combo)
                {
                    Combo.Properties.Columns.Clear();
                    Combo.Properties.DataSource = null;
                    Combo.Properties.DataSource = RequestCertificate.GetCustomer();
                    Combo.Properties.ValueMember = "CustomerId";
                    Combo.Properties.DisplayMember = "DocumentNumber";
                    Combo.Properties.Columns.Add(new LookUpColumnInfo("DocumentNumber", 0, "N. DOCUMENTO"));
                    Combo.Properties.Columns.Add(new LookUpColumnInfo("Names", 0, "NOMBRES"));
                    Combo.Properties.Columns.Add(new LookUpColumnInfo("LastNames", 0, "APELLIDOS"));
                }

                public static void GetAllCostumers(LookUpEdit Combo)
                {
                    Combo.Properties.Columns.Clear();
                    Combo.Properties.DataSource = null;
                    Combo.Properties.DataSource = web.GetAllCustomer();
                    Combo.Properties.ValueMember = "CustomerId";
                    Combo.Properties.DisplayMember = "DocumentNumber";
                    Combo.Properties.Columns.Add(new LookUpColumnInfo("DocumentNumber", 0, "N. DOCUMENTO"));
                    Combo.Properties.Columns.Add(new LookUpColumnInfo("Names", 0, "NOMBRES"));
                    Combo.Properties.Columns.Add(new LookUpColumnInfo("LastNames", 0, "APELLIDOS"));
                }

                public static void SetComboDocumentType(LookUpEdit Combo)
                {
                    Combo.Properties.Columns.Clear();
                    Combo.Properties.DataSource = null;
                    Combo.Properties.DataSource = DOCUMENTTYPELN.GetAll();
                    Combo.Properties.ValueMember = "DocumentTypeId";
                    Combo.Properties.DisplayMember = "Description";
                    Combo.Properties.Columns.Add(new LookUpColumnInfo("Code", 0, "Codigo"));
                    Combo.Properties.Columns.Add(new LookUpColumnInfo("Description", 0, "Descripción"));
                }

                public static void SetComboByDocumentType(LookUpEdit Combo)
                {
                    Combo.Properties.Columns.Clear();
                    Combo.Properties.DataSource = null;
                    Combo.Properties.DataSource = VariablesGlobales.lstDocumentoPJ;
                    Combo.Properties.ValueMember = "DocumentTypeId";
                    Combo.Properties.DisplayMember = "Description";
                    Combo.Properties.Columns.Add(new LookUpColumnInfo("Code", 0, "Codigo"));
                    Combo.Properties.Columns.Add(new LookUpColumnInfo("Description", 0, "Descripción"));
                }

                public static async Task SetComboByDocumentTypeIdAsync(LookUpEdit Combo)
                {
                    Combo.Properties.Columns.Clear();
                    Combo.Properties.DataSource = null;
                    Combo.Properties.DataSource = await DOCUMENTTYPELN.GetDocumentByIdAsync();
                    Combo.Properties.ValueMember = "DocumentTypeId";
                    Combo.Properties.DisplayMember = "Description";
                    Combo.Properties.Columns.Add(new LookUpColumnInfo("Code", 0, "Codigo"));
                    Combo.Properties.Columns.Add(new LookUpColumnInfo("Description", 0, "Descripción"));
                }

                public static async Task SetComboByDocumentTypeIdNew(LookUpEdit Combo, int idDoc)
                {
                    Combo.Properties.Columns.Clear();
                    Combo.Properties.DataSource = null;
                    Combo.Properties.DataSource = await DOCUMENTTYPELN.GetDocumentByIdNew(idDoc);
                    Combo.Properties.ValueMember = "DocumentTypeId";
                    Combo.Properties.DisplayMember = "Description";
                    Combo.Properties.Columns.Add(new LookUpColumnInfo("Code", 0, "Codigo"));
                    Combo.Properties.Columns.Add(new LookUpColumnInfo("Description", 0, "Descripción"));
                }




                public static void SetComboByTypeDocumentId(LookUpEdit Combo)
                {
                    Combo.Properties.Columns.Clear();
                    Combo.Properties.DataSource = null;
                    Combo.Properties.DataSource = VariablesGlobales.lstDocumentoPN;
                    Combo.Properties.ValueMember = "DocumentTypeId";
                    Combo.Properties.DisplayMember = "Description";
                    Combo.Properties.Columns.Add(new LookUpColumnInfo("Code", 0, "Codigo"));
                    Combo.Properties.Columns.Add(new LookUpColumnInfo("Description", 0, "Descripción"));
                }

                public static async Task GetComboByTypeDocumentIdAsync(LookUpEdit Combo, int idDoc)
                {
                    Combo.Properties.Columns.Clear();
                    Combo.Properties.DataSource = null;
                    Combo.Properties.DataSource = await DOCUMENTTYPELN.GetDocumentByTypeDocumentIdAsync();
                    Combo.Properties.ValueMember = "DocumentTypeId";
                    Combo.Properties.DisplayMember = "Description";
                    Combo.Properties.Columns.Add(new LookUpColumnInfo("Code", 0, "Codigo"));
                    Combo.Properties.Columns.Add(new LookUpColumnInfo("Description", 0, "Descripción"));
                }



                public static void SetComboReasonReview(LookUpEdit Combo)
                {
                    Combo.Properties.Columns.Clear();
                    Combo.Properties.DataSource = null;
                    Combo.Properties.DataSource = REASONREVIEWLN.GetAll();
                    Combo.Properties.ValueMember = "ReasonReviewId";
                    Combo.Properties.DisplayMember = "Name";
                    Combo.Properties.Columns.Add(new LookUpColumnInfo("Name", 0, "Descripción"));
                }

                public static void SetComboReasonRejection(LookUpEdit Combo)
                {
                    Combo.Properties.Columns.Clear();
                    Combo.Properties.DataSource = null;
                    Combo.Properties.DataSource = REASONREJECTIONLN.GetAll();
                    Combo.Properties.ValueMember = "ReasonRejectionId";
                    Combo.Properties.DisplayMember = "Name";
                    Combo.Properties.Columns.Add(new LookUpColumnInfo("Name", 0, "Descripción"));
                }
            }

            public class Grid
            {
                public static void GetGridAdjuntos(GridControl gridAdjuntos, GridView visorAdjuntos)
                {
                    visorAdjuntos.Columns[0].Caption = "Nombre";
                    visorAdjuntos.Columns[0].AppearanceCell.TextOptions.HAlignment = HorzAlignment.Near;
                    visorAdjuntos.Columns[0].AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center;
                    visorAdjuntos.Columns[0].AppearanceHeader.ForeColor = Color.MidnightBlue;

                    visorAdjuntos.Columns[1].Caption = "Descripción";
                    visorAdjuntos.Columns[1].AppearanceCell.TextOptions.HAlignment = HorzAlignment.Near;
                    visorAdjuntos.Columns[1].AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center;
                    visorAdjuntos.Columns[1].AppearanceHeader.ForeColor = Color.MidnightBlue;


                    visorAdjuntos.Columns[2].Caption = "Tipo";
                    visorAdjuntos.Columns[2].AppearanceCell.TextOptions.HAlignment = HorzAlignment.Center;
                    visorAdjuntos.Columns[2].AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center;
                    visorAdjuntos.Columns[2].AppearanceHeader.ForeColor = Color.MidnightBlue;

                    visorAdjuntos.Columns[3].Caption = "Archivo";
                    visorAdjuntos.Columns[3].AppearanceCell.TextOptions.HAlignment = HorzAlignment.Center;
                    visorAdjuntos.Columns[3].AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center;
                    visorAdjuntos.Columns[3].AppearanceHeader.ForeColor = Color.MidnightBlue;
                    visorAdjuntos.Columns[3].Visible = false;

                    visorAdjuntos.Columns[4].Caption = "Tamaño";
                    visorAdjuntos.Columns[4].AppearanceCell.TextOptions.HAlignment = HorzAlignment.Center;
                    visorAdjuntos.Columns[4].AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center;
                    visorAdjuntos.Columns[4].AppearanceHeader.ForeColor = Color.MidnightBlue;

                    visorAdjuntos.BestFitColumns();

                }
            }
            public class PersonaHuellaEstados
            {
                public int Id { get; set; }
                public string Valor { get; set; }

                public PersonaHuellaEstados(int pId, string pValor)
                {
                    this.Id = pId;
                    this.Valor = pValor;
                }

            }
            public class EstadosSolicitud
            {
                public int Id { get; set; }
                public string Valor { get; set; }

                public EstadosSolicitud(int pId, string pValor)
                {
                    this.Id = pId;
                    this.Valor = pValor;
                }

            }




            public static string ExtraerExcepcion(Exception ex)
            {
                string mensaje = "";
                if (ex.InnerException != null)
                {
                    if (ex.InnerException.InnerException != null)
                        mensaje = ex.InnerException.InnerException.Message;
                    else
                        mensaje = ex.InnerException.Message;
                }
                else
                    mensaje = ex.Message;

                return mensaje;
            }

            public class Mensajes
            {
                public static void getMensajeSinConexion()
                {
                    XtraMessageBox.Show(frmMain2.lookAndFeelMsgBox, "Sin conexión con el servidor, por favor espere.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

                public static void getSinFilaSeleccionada()
                {
                    XtraMessageBox.Show(frmMain2.lookAndFeelMsgBox, "Debe seleccionar un registro para continuar.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

                public static void getSinDatosParaBusqueda()
                {
                    XtraMessageBox.Show(frmMain2.lookAndFeelMsgBox, "Debe ingresar parametros para poder ejecutar la búsqueda.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        public class Utilities
        {
            private static ExodusBcService web = new ExodusBcService();


            public static string GetVersion()
            {
                return Assembly.GetExecutingAssembly().GetName().Version.ToString();
            }

            public static byte[] ImageToByteArrayBmp(Image imageIn)
            {
                using (var ms = new MemoryStream())
                {
                    Bitmap bmp = new Bitmap(imageIn);
                    bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                    return ms.ToArray();
                }


            }
            public static string ImageToBase64(Image img)
            {
                string base64 = string.Empty;
                try
                {
                    using (MemoryStream m = new MemoryStream())
                    {
                        Image imageIn = img;
                        MemoryStream ms = new MemoryStream();
                        imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                        byte[] imageBytes = ms.ToArray();
                        base64 = Convert.ToBase64String(imageBytes);
                    }
                    return base64;
                }
                catch { return base64; }
            }

            public static Image ByteArrayToImage(byte[] byteArrayIn)
            {
                try
                {
                    MemoryStream ms = new MemoryStream(byteArrayIn);
                    Image returnImage = Image.FromStream(ms);
                    return returnImage;

                }
                catch (Exception)
                {

                    return null;
                }

            }
            public static byte[] ImageToByteArray(Image imageIn)
            {
                MemoryStream ms = new MemoryStream();
                imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                return ms.GetBuffer();
            }

            public static byte[] ImageToByteArrayJpg(Image imageIn)
            {
                MemoryStream ms = new MemoryStream();
                imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                return ms.GetBuffer();
            }


            public static bool checkDNS(string host, string recType)
            {
                bool result = false;
                try
                {
                    using (Process proc = new Process())
                    {
                        proc.StartInfo.FileName = "nslookup";
                        proc.StartInfo.Arguments = string.Format("-type={0} {1}", recType, host);
                        proc.StartInfo.CreateNoWindow = true;
                        proc.StartInfo.ErrorDialog = false;
                        proc.StartInfo.RedirectStandardError = true;
                        proc.StartInfo.RedirectStandardOutput = true;
                        proc.StartInfo.UseShellExecute = false;
                        proc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                        proc.OutputDataReceived += (object sender, DataReceivedEventArgs e) =>
                        {
                            if ((e.Data != null) && (!result))
                                result = e.Data.StartsWith(host);
                        };
                        proc.ErrorDataReceived += (object sender, DataReceivedEventArgs e) =>
                        {
                            if (e.Data != null)
                            {
                                //read error output here, not sure what for?
                            }
                        };
                        proc.Start();
                        proc.BeginErrorReadLine();
                        proc.BeginOutputReadLine();
                        proc.WaitForExit(30000); //timeout after 30 seconds.
                    }
                }
                catch
                {
                    result = false;
                }
                return result;
            }

            public static decimal FormatByteSize(decimal length)
            {
                const int KB = 1024;
                return Math.Round(length / KB, 0);

            }
            public static void PrepararProgressBar(ProgressBarControl prgb, LabelControl lbl, int valor)
            {
                lbl.ForeColor = Color.White;
                lbl.Text = valor.ToString();
                lbl.Visible = true;
                prgb.Visible = true;

                switch (valor)
                {
                    case 1:
                        {
                            prgb.BackColor = Color.LightGreen;
                            prgb.Properties.Appearance.BorderColor = Color.SeaGreen;
                            prgb.Properties.StartColor = Color.Green;
                            prgb.Properties.EndColor = Color.Green;
                            prgb.Text = "5";
                            lbl.BackColor = Color.Green;

                            break;
                        }

                    case 2:
                        {
                            prgb.BackColor = Color.LightGreen;
                            prgb.Properties.Appearance.BorderColor = Color.SeaGreen;
                            prgb.Properties.StartColor = Color.Green;
                            prgb.Properties.EndColor = Color.Green;
                            prgb.Text = "4";
                            lbl.BackColor = Color.Green;
                            break;
                        }

                    case 3:
                        {
                            prgb.BackColor = Color.Khaki;
                            prgb.Properties.Appearance.BorderColor = Color.DarkGoldenrod;
                            prgb.Properties.StartColor = Color.Chocolate;
                            prgb.Properties.EndColor = Color.Chocolate;
                            prgb.Text = "3";
                            lbl.BackColor = Color.Chocolate;
                            break;
                        }

                    case 4:
                        {
                            prgb.BackColor = Color.Pink;
                            prgb.Properties.Appearance.BorderColor = Color.Brown;
                            prgb.Properties.StartColor = Color.Red;
                            prgb.Properties.EndColor = Color.Red;
                            prgb.Text = "2";
                            lbl.BackColor = Color.Red;
                            break;
                        }

                    case 5:
                        {
                            prgb.BackColor = Color.Pink;
                            prgb.Properties.Appearance.BorderColor = Color.Brown;
                            prgb.Properties.StartColor = Color.Red;
                            prgb.Properties.EndColor = Color.Red;
                            prgb.Text = "1";
                            lbl.BackColor = Color.Red;
                            break;
                        }

                }


            }

            public static DateTime getFechaActual()
            {
                if (VariablesGlobales.LocalTimeSynchronized)
                    return DateTime.Now;
                else
                    return SyncLocalTimeFromServer();
            }
            public static string LeerVariablesConfig(string nombreVariable)
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                return config.AppSettings.Settings[nombreVariable].Value;
            }
            public static class StreamImagenes
            {
                public static Image ImageFromFile(string filePath)
                {
                    if (filePath == null) throw new ArgumentNullException("filePath");
                    using (FileStream stream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
                    {
                        return Image.FromStream(stream);
                    }
                }
            }

            public static string getSecuenciaAfis()
            {
                try
                {
                    if (VariablesGlobales.MODOTRABAJO.Equals("ON"))
                    {
                        JArray post = new JArray();
                        JObject ob1 = (JObject)JToken.FromObject(new JsonItem("S", "SELECT CONCAT('C',TO_CHAR(SYSDATE, 'YYYYMMDDHH24MiSS')) AS SECUENCIA FROM DUAL"));
                        post.Add(ob1);
                        string srv = "";
                        string pre = post.ToString().Substring(0, post.ToString().Length);
                        string res = web.GetJson(VariablesGlobales.TOKEN, pre, "");
                        dynamic ar = JArray.Parse(res)[0];

                        if (ar.Id == "S")
                        {
                            JArray items = ar.Obj;
                            foreach (JObject job in items)
                                srv = job.GetValue("SECUENCIA").ToString();
                        }
                        return srv;
                    }
                    else return string.Concat("C", DateTime.Now.ToString("yyyyMMddHHmmss"));
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            public static String ObtenerMaquinaId()
            {
                String id = String.Empty;
                try
                {
                    ManagementObjectSearcher mbs = new ManagementObjectSearcher("Select * from Win32_BIOS");
                    ManagementObjectCollection mbsList = mbs.Get();

                    foreach (ManagementObject mo in mbsList)
                        id = mo["SerialNumber"].ToString();
                    return Seguridad.GVC.EncriptarEnUnaVia(id);
                }
                catch (Exception) { return id; }
            }

            public static bool isIP(string texto)
            {
                if (texto.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries).Length == 4)
                {
                    IPAddress ipAddr;
                    if (IPAddress.TryParse(texto, out ipAddr))
                        return true;
                    else
                        return false;
                }
                else
                    return false;
            }

            public static bool isMacAddress(string texto)
            {
                texto = texto.Replace(" ", "").Replace(":", "").Replace("-", "");
                Regex r = new Regex("^([:xdigit:]){12}$");

                if (r.IsMatch(texto))
                    return true;
                else
                    return false;
            }

            public static bool isMail(string texto)
            {
                try
                {
                    var addr = new System.Net.Mail.MailAddress(texto);
                    return addr.Address == texto;
                }
                catch
                {
                    return false;
                }
            }

            public static bool IsNumeric(string texto)
            {
                return texto.All(char.IsNumber);
            }

            public static int getEdad(DateTime fechaNacimiento)
            {
                int edad = DateTime.Now.Year - fechaNacimiento.Year;
                if ((fechaNacimiento.Month > DateTime.Now.Month) || (fechaNacimiento.Month == DateTime.Now.Month && fechaNacimiento.Day > DateTime.Now.Day))
                    edad--;
                return edad;
            }

            public static string getHostName()
            {
                String HostName = String.Empty;
                try
                {
                    HostName = Dns.GetHostName();
                    return HostName;
                }
                catch (Exception) { return HostName; }
            }

            public static string getLocalIP()
            {
                try
                {
                    var host = Dns.GetHostEntry(Dns.GetHostName());
                    foreach (var ip in host.AddressList)
                    {
                        if (ip.AddressFamily == AddressFamily.InterNetwork)
                        {
                            return ip.ToString();
                        }
                    }
                    return string.Empty;
                }
                catch (Exception)
                {
                    return string.Empty;
                }
            }

            public static bool existsFileSQLite()
            {
                string path = Path.Combine(VariablesGlobales.PathDataDB, VariablesGlobales.NameDB);
                try
                {
                    return (File.Exists(path));
                }
                catch
                {
                    return false;
                }
            }

            public static bool copyFileSQLite()
            {
                string origen = VariablesGlobales.PathDataDBInstall;
                string destino = Path.Combine(VariablesGlobales.PathDataDB, VariablesGlobales.NameDB);
                try
                {
                    if (File.Exists(origen))
                        File.Copy(origen, destino, true);

                    return true;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            private static DateTime SyncLocalTimeFromServer()
            {
                DateTime serverTime = DateTime.Now;

                try
                {
                    if (VariablesGlobales.MODOTRABAJO.Equals("ON"))
                    {
                        JArray post = new JArray();
                        JObject ob1 = (JObject)JToken.FromObject(new JsonItem("S", "SELECT TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:Mi:SS') AS FECHA FROM DUAL"));
                        post.Add(ob1);
                        string pre = post.ToString().Substring(0, post.ToString().Length);
                        string res = web.GetJson(VariablesGlobales.TOKEN, pre, "");
                        dynamic ar = JArray.Parse(res)[0];

                        if (ar.Id == "S")
                        {
                            JArray items = ar.Obj;
                            foreach (JObject job in items)
                                serverTime = DateTime.Parse(job.GetValue("FECHA").ToString());
                        }
                        SYSTIME s = new SYSTIME();
                        s.Year = (short)serverTime.Year;
                        s.Month = (short)serverTime.Month;
                        s.DayOfWeek = (short)serverTime.DayOfWeek;
                        s.Day = (short)serverTime.Day;
                        s.Hour = (short)serverTime.Hour;
                        s.Minutes = (short)serverTime.Minute;
                        s.Seconds = (short)serverTime.Second;
                        s.Milliseconds = (short)serverTime.Millisecond;
                        SetLocalTime(ref s);
                        VariablesGlobales.LocalTimeSynchronized = true;
                    }



                }
                catch (Exception)
                {
                    VariablesGlobales.LocalTimeSynchronized = false;
                }

                return serverTime;
            }
        }

        public class LoginWebService
        {
            private static ExodusBcService web = new ExodusBcService();

            public static string getStatusConecction()
            {

                try
                {
                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("HORA", "SELECT GETDATE() AS FECHA"));
                    post.Add(ob1);
                    string srv = "";
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, "");
                    dynamic ar = JArray.Parse(res)[0];

                    if (ar.Id == "HORA")
                    {
                        JArray items = ar.Obj;
                        foreach (JObject job in items)
                            srv = job.GetValue("FECHA").ToString();
                    }
                    return srv;
                }
                catch (Exception ex)
                {
                    return string.Empty;
                }
            }

            public static JArray getWorkStation(string Hostname)
            {
                try
                {
                    string sql = string.Format("SELECT * FROM WorkStations WHERE HostName = '{0}'", Hostname);
                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("WorkStations", sql));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, Hostname);
                    dynamic ar = JArray.Parse(res)[0];

                    return ar.Obj;
                    if (ar.Id == "WorkStations")
                        return null;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            public static JArray getOffice(int OfficeCode, string Hostname)
            {
                try
                {
                    string sql = string.Format("SELECT * FROM Office WHERE OfficeId = '{0}'", OfficeCode);
                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("Office", sql));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, Hostname);
                    dynamic ar = JArray.Parse(res)[0];

                    if (ar.Id == "Office")
                        return ar.Obj;
                    return null;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            public static JArray getUser(string UserName, string Hostname)
            {
                try
                {
                    string sql = string.Format("SELECT * FROM USERS WHERE LOGIN_NAME = '{0}'", UserName);
                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("User", sql));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, Hostname);
                    dynamic ar = JArray.Parse(res)[0];

                    if (ar.Id == "User")
                        return ar.Obj;
                    return null;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }



            public static JArray getUserSync(string UserName, string Hostname)
            {
                try
                {
                    string sql = string.Format("SELECT * FROM USERS_SYNC WHERE USER_ID = (SELECT USER_ID FROM USERS WHERE LOGIN_NAME='{0}')", UserName);
                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("User", sql));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, Hostname);
                    dynamic ar = JArray.Parse(res)[0];

                    if (ar.Id == "User")
                        return ar.Obj;
                    return null;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            public static JArray getUserIntentos(long UserID, string Hostname)
            {
                try
                {
                    string sql = string.Format("SELECT * FROM LOGIN_FAIL_LOG WHERE USER_ID = {0}", UserID);
                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("User", sql));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, Hostname);
                    dynamic ar = JArray.Parse(res)[0];

                    if (ar.Id == "User")
                        return ar.Obj;
                    return null;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            public static JArray getUserHorarios(long UserID, string Hostname)
            {
                try
                {
                    string sql = string.Format("SELECT A.ACCESS_BEGIN_DATE, A.ACCESS_END_DATE, A.ACCESS_BEGIN_TIME, ACCESS_END_TIME, B.DIAS, " +
                    "   (SELECT CASE WHEN " +
                    "        (SELECT ROUND(SYSDATE - MAX(PASS_CREATE_TIME), 0) AS DIFF FROM PASSWORD_HISTORY WHERE USER_ID = A.USER_ID) > " +
                    "        (SELECT(SELECT CONFIG_NUM_VALUE FROM SYSTEM_CONFIG WHERE CONFIG_NAME = 'PASS_EXPIRE_INTERVAL') + (SELECT CONFIG_NUM_VALUE FROM SYSTEM_CONFIG WHERE CONFIG_NAME = 'PASS_EXPIRE_GRACE') AS TOTAL FROM DUAL) " +
                    "        THEN 'SI' ELSE 'NO' " +
                    "    END FROM DUAL) AS BLOQUEA " +
                    "FROM SYSTEM_USER A INNER JOIN SYSTEM_USER_GV B ON B.USER_ID = A.USER_ID WHERE A.USER_ID = {0}", UserID);

                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("User", sql));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, Hostname);
                    dynamic ar = JArray.Parse(res)[0];
                    if (ar.Id == "User")
                        return ar.Obj;
                    return null;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            public static JArray getModulos(string Hostname)
            {
                try
                {
                    string sql = string.Format("SELECT * FROM ROLE_MODULOS WHERE ESTADO = {0}", 1);
                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("Modulos", sql));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, Hostname);
                    dynamic ar = JArray.Parse(res)[0];

                    if (ar.Id == "Modulos")
                        return ar.Obj;
                    return null;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            public static JArray getAcciones(string Hostname)
            {
                try
                {
                    string sql = string.Format("SELECT A.*,B.NOMBRE AS XNOMBRE FROM ROLE_ACCIONES A INNER JOIN ROLE_MODULOS B ON B.ROLE_MODULO_ID=A.FK_ROLE_MODULO_ID WHERE A.ESTADO = {0}", 1);
                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("Acciones", sql));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, Hostname);
                    dynamic ar = JArray.Parse(res)[0];

                    if (ar.Id == "Acciones")
                        return ar.Obj;
                    return null;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            public static JArray getAcciones(long role_id, string Hostname)
            {
                try
                {
                    string sql = string.Format("SELECT B.*,C.NOMBRE AS XNOMBRE FROM ROLE_MODULO_ACCIONES A INNER JOIN ROLE_ACCIONES B ON B.ROLE_ACCION_ID = A.FK_ROLE_ACCION_ID INNER JOIN ROLE_MODULOS C ON C.ROLE_MODULO_ID = B.FK_ROLE_MODULO_ID WHERE A.FK_ROLE = {0} AND B.ESTADO = 1 AND C.ESTADO = 1", role_id);
                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("Acciones", sql));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, Hostname);
                    dynamic ar = JArray.Parse(res)[0];

                    if (ar.Id == "Acciones")
                        return ar.Obj;
                    return null;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            public static bool setTimeLogin(long sesion_id, string Hostname)
            {
                try
                {
                    string sql = string.Format("UPDATE LoginSession SET LogoutTime = SYSDATE WHERE LoginSessionId = {0}", sesion_id);
                    string res = web.Insertar(VariablesGlobales.TOKEN, sql, VariablesGlobales.HOST_NAME);

                    return (res == "\"\"");
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            public static bool UpdateUserStatus(int UserId, string Hostname)
            {
                try
                {
                    string sql = string.Format("UPDATE Users SET FKCatalogStatusId = 2 WHERE UserId = {0}", UserId);
                    string res = web.Insertar(VariablesGlobales.TOKEN, sql, VariablesGlobales.HOST_NAME);

                    return (res == "\"\"");
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            public static bool getValidaSesionActiva(long UserId, string Hostname)
            {
                bool salida = true;
                try
                {
                    string sql = string.Format("SELECT LIVE_FLAG AS SESIONACTIVA, LOGIN_SESSION_ID AS SESSIONID, CHECK_TIME  FROM LOGIN_SESSION X WHERE X.USER_ID = {0} AND X.LOGIN_TIME = (SELECT MAX(LOGIN_TIME) FROM LOGIN_SESSION WHERE USER_ID = X.USER_ID)", UserId);
                    JArray post = new JArray();
                    post.Add((JObject)JToken.FromObject(new JsonItem("Sesion", sql)));
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, Hostname);
                    dynamic ar = JArray.Parse(res)[0];

                    if (ar.Id == "Sesion")
                    {
                        JArray items = ar.Obj;
                        if (items.Count > 0)
                        {
                            foreach (JObject job in items)
                            {
                                salida = (job.GetValue("SESIONACTIVA").ToString().Equals("0")) ? false : true;
                                if (salida)
                                {
                                    VariablesGlobales.sessionActiveId = Convert.ToInt32(job.GetValue("SESSIONID"));
                                    VariablesGlobales.sessionLastTime = Convert.ToDateTime(job.GetValue("CHECK_TIME"));
                                }

                            }

                        }
                        else
                            salida = false;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return salida;
            }

            public static object getItemFromJArray(JArray array, string ItemName)
            {
                object dato = null;
                foreach (JObject obj in array)
                    dato = (obj != null) ? obj.GetValue(ItemName) : null;
                return dato;
            }

            public static string getIdFromJArray(JArray array, string modulo, string accion)
            {
                string dato = null;
                foreach (JObject obj in array)
                {
                    string id = obj.GetValue("ROLE_ACCION_ID").ToString();
                    string val = obj.GetValue("NOMBRE").ToString();
                    string padre = obj.GetValue("XNOMBRE").ToString();
                    if (val.ToString().Equals(accion) && padre.ToString().Equals(modulo))
                        dato = id;
                }
                return dato;
            }

            public static bool setLogoutSession(string UserName)
            {
                bool salida = false;
                try
                {
                    string res = "\"\""; //web.ProcesarSP_LogoutSession(VariablesGlobales.Token, UserName);
                    if (res == "\"\"")
                        salida = true;
                }
                catch
                {
                    return salida;
                }
                return salida;
            }

            public static bool setRegisterUserSync(string UserName, string Hostname)
            {
                bool salida = false;
                try
                {
                    string sql = String.Format("INSERT INTO USERS_SYNC (USER_ID, LAST_INSERT_DATE) VALUES ((SELECT USER_ID FROM USERS WHERE LOGIN_NAME='{0}'), SYSDATE)", UserName);
                    string res = web.Insertar(VariablesGlobales.TOKEN, sql, Hostname);
                    if (res == "\"\"")
                        salida = true;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return salida;
            }




            //Nuevos métodos
            public static JArray getUserInfo(string username, string hostname)
            {
                try
                {
                    StringBuilder sql = new StringBuilder();
                    sql.Append("SELECT U.UserId, U.Name, U.Password, U.CreationDate, U.DaysOfAccess, U.StartTime, U.EndTime, U.CreationDate, U.ModifyDate, U.CreateUserId, U.FKPersonId, U.FKUserRolesId, U.FKCatalogStatusId, U.FKOfficeId, U.ResetPassword, U.PasswordChangeDate ");
                    sql.Append("FROM Users U ");
                    sql.Append(string.Format("WHERE U.Name='{0}'", username));
                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("Users", sql.ToString()));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, hostname);
                    dynamic ar = JArray.Parse(res)[0];

                    if (ar.Id == "Users")
                        return ar.Obj;
                    return null;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            public static JArray EjecutarLoginFileLog(int UserId, string hostname)
            {
                try
                {

                    string sql = string.Format("exec dbo.sp_LoginFail {0}", UserId);
                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("Users", sql.ToString()));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, hostname);
                    dynamic ar = JArray.Parse(res)[0];

                    if (ar.Id == "Users")
                        return ar.Obj;
                    return null;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            public static JArray getLoginFailLog(int userId, string hostname)
            {
                try
                {
                    StringBuilder sql = new StringBuilder();
                    sql.Append("SELECT FailCount, LastFailTime, FKUserId ");
                    sql.Append("FROM LoginFailLog  ");
                    sql.Append(string.Format("WHERE FKUserId={0}", userId));
                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("Users", sql.ToString()));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, hostname);
                    dynamic ar = JArray.Parse(res)[0];

                    if (ar.Id == "Users")
                        return ar.Obj;
                    return null;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            public static JArray getUserFailCount(int UserId, string hostname)
            {
                try
                {
                    StringBuilder sql = new StringBuilder();
                    sql.Append("SELECT L.FKUserId, L.FailCount, L.LastFailTime ");
                    sql.Append("FROM LoginFailLog L ");
                    sql.Append(string.Format("WHERE L.FKUserId= {0}", UserId));
                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("LoginFailLog", sql.ToString()));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, hostname);
                    dynamic ar = JArray.Parse(res)[0];

                    if (ar.Id == "LoginFailLog")
                        return ar.Obj;
                    return null;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            public static JArray getSessionActiva(long userId, string hostname)
            {

                try
                {
                    string sql = string.Format("SELECT FKCatalogStatusId , LoginSessionId , LogoutTime  FROM LoginSession X WHERE X.FKUserId = {0} AND X.LoginTime = (SELECT MAX(LoginTime) FROM LoginSession WHERE FKUserId = X.FKUserId)", userId);
                    JArray post = new JArray();
                    post.Add((JObject)JToken.FromObject(new JsonItem("LoginSession", sql)));
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, hostname);
                    dynamic ar = JArray.Parse(res)[0];

                    if (ar.Id == "LoginSession")
                        return ar.Obj;
                    return null;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }


        }

        public class Nist
        {
            public static string NIST_ORI = "GVISION";

            public static bool EmpaquetarNistFID(string NoTransaccion, string sourceFiles, string firstName, string lastName, string fullName, string sex, string nationality, DateTime dateOfBirth, string NIST_ORI)
            {
                try
                {
                    string folder = Path.Combine(sourceFiles);
                    if (!Directory.Exists(folder)) Directory.CreateDirectory(folder);

                    if (Directory.GetFiles(folder).Length > 0)
                    {
                        var NistCtrl = new AXNISTNEWLib.eSortNistCtrl();
                        string cfgpath = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                        NistCtrl.DataDir = sourceFiles;
                        NistCtrl.CfgFile = cfgpath + "\\AxNist.cfg";
                        NistCtrl.TOT = "FID";
                        NistCtrl.TransNo = NoTransaccion;
                        NistCtrl.NistFileName = string.Format(@"{0}\{1}.tdf", sourceFiles, NoTransaccion);
                        NistCtrl.SetDemoInfo("SID", NoTransaccion);
                        NistCtrl.SetDemoInfo("VER", "0201");
                        NistCtrl.SetDemoInfo("TCN", NoTransaccion);
                        NistCtrl.SetDemoInfo("PRY", "1");
                        NistCtrl.SetDemoInfo("NSR", "19.69");
                        NistCtrl.SetDemoInfo("NTR", "19.69");
                        NistCtrl.SetDemoInfo("ORI", NIST_ORI);
                        NistCtrl.SetDemoInfo("DAT", DateTime.Now.ToString("yyyyMMdd"));
                        NistCtrl.SetDemoInfo("NAM", fullName);
                        NistCtrl.SetDemoInfo("FNAM", firstName);
                        NistCtrl.SetDemoInfo("LNAM", lastName);
                        NistCtrl.SetDemoInfo("SEX", sex);
                        NistCtrl.SetDemoInfo("POB", nationality);
                        NistCtrl.SetDemoInfo("DOB", Convert.ToDateTime(dateOfBirth).ToString("yyyyMMdd"));
                        NistCtrl.SetType15(1);
                        NistCtrl.Pack();
                        if (NistCtrl.ErrCode == 0)
                        {
                            Utilidades.Log.InsertarLog(Utilidades.Log.ErrorType.Informacion, "Enrolamiento::EmpaquetarNistFID", "Nist FID Empaquetado", VariablesGlobales.PathDataLog);
                            return true;
                        }
                        else return false;
                    }
                    else return false;
                }
                catch (Exception ex)
                {

                    Log.InsertarLog(Log.ErrorType.Error, "Enrolamiento::EmpaquetarNistFID", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return false;
                }
            }

            public static bool EmpaquetarNist121Ant(string NoTransaccion, string personId, string sourceFiles, string firstName, string lastName, string fullName, string sex, string nationality, DateTime dateOfBirth, string NIST_ORI)
            {
                try
                {
                    string folder = Path.Combine(sourceFiles);
                    if (!Directory.Exists(folder)) Directory.CreateDirectory(folder);

                    if (Directory.GetFiles(folder).Length > 0)
                    {
                        var NistCtrl = new AXNISTNEWLib.eSortNistCtrl();
                        string cfgpath = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                        NistCtrl.DataDir = sourceFiles;
                        NistCtrl.CfgFile = cfgpath + "\\AxNist.cfg";
                        NistCtrl.TOT = "121";
                        NistCtrl.TransNo = NoTransaccion;
                        NistCtrl.NistFileName = string.Format(@"{0}\{1}.tdf", sourceFiles, NoTransaccion);
                        NistCtrl.SetDemoInfo("SID", personId);
                        NistCtrl.SetDemoInfo("VER", "0201");
                        NistCtrl.SetDemoInfo("TCN", NoTransaccion);
                        NistCtrl.SetDemoInfo("PRY", "1");
                        NistCtrl.SetDemoInfo("NSR", "19.69");
                        NistCtrl.SetDemoInfo("NTR", "19.69");
                        NistCtrl.SetDemoInfo("ORI", NIST_ORI);
                        NistCtrl.SetDemoInfo("DAT", DateTime.Now.ToString("yyyyMMdd"));
                        NistCtrl.SetDemoInfo("NAM", fullName);
                        NistCtrl.SetDemoInfo("FNAM", firstName);
                        NistCtrl.SetDemoInfo("LNAM", lastName);
                        NistCtrl.SetDemoInfo("SEX", sex);
                        NistCtrl.SetDemoInfo("POB", nationality);
                        NistCtrl.SetDemoInfo("DOB", Convert.ToDateTime(dateOfBirth).ToString("yyyyMMdd"));
                        NistCtrl.SetType15(1);
                        NistCtrl.Pack();
                        return true;
                    }
                    else return false;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            public static bool EmpaquetarNist121(string NoTransaccion, string personId, string sourceFiles, string firstName, string lastName, string fullName, string sex, string nationality, DateTime dateOfBirth, string NIST_ORI)
            {
                try
                {
                    string folder = Path.Combine(sourceFiles);
                    if (!Directory.Exists(folder)) Directory.CreateDirectory(folder);

                    if (Directory.GetFiles(folder).Length > 0)
                    {
                        var NistCtrl = new AXNISTNEWLib.eSortNistCtrl();
                        string cfgpath = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                        NistCtrl.DataDir = sourceFiles;
                        NistCtrl.CfgFile = cfgpath + "\\AxNist.cfg";
                        NistCtrl.TOT = "FingerPrintWTVerify";
                        NistCtrl.TransNo = NoTransaccion;
                        NistCtrl.NistFileName = string.Format(@"{0}\{1}.tdf", sourceFiles, NoTransaccion);
                        NistCtrl.SetDemoInfo("Transaction-Number", NoTransaccion);
                        NistCtrl.SetDemoInfo("Transaction-Type", "FingerPrintWTVerify");
                        NistCtrl.SetDemoInfo("ENROLMENTID", personId);
                        NistCtrl.SetType15(1);
                        NistCtrl.Pack();
                        return (NistCtrl.ErrCode == 0);
                    }
                    else return false;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            public static bool EmpaquetarNistSID(string NoTransaccion, string sourceFiles, string firstName, string lastName, string fullName, string sex, string nationality, DateTime dateOfBirth, string NIST_ORI)
            {
                try
                {
                    string folder = Path.Combine(sourceFiles);
                    if (!Directory.Exists(folder)) Directory.CreateDirectory(folder);

                    if (Directory.GetFiles(sourceFiles).Length > 0)
                    {
                        var NistCtrl = new AXNISTNEWLib.eSortNistCtrl();
                        string cfgpath = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                        NistCtrl.DataDir = sourceFiles;
                        NistCtrl.CfgFile = cfgpath + "\\AxNist.cfg";
                        NistCtrl.TOT = "SID";
                        NistCtrl.TransNo = NoTransaccion;
                        NistCtrl.NistFileName = string.Format(@"{0}\{1}.tdf", sourceFiles, NoTransaccion);
                        NistCtrl.SetDemoInfo("SID", NoTransaccion);
                        NistCtrl.SetDemoInfo("VER", "0201");
                        NistCtrl.SetDemoInfo("TCN", NoTransaccion);
                        NistCtrl.SetDemoInfo("PRY", "1");
                        NistCtrl.SetDemoInfo("NSR", "19.69");
                        NistCtrl.SetDemoInfo("NTR", "19.69");
                        NistCtrl.SetDemoInfo("ORI", "GVISION");
                        NistCtrl.SetDemoInfo("DAT", DateTime.Now.ToString("yyyyMMdd"));
                        NistCtrl.SetDemoInfo("NAM", fullName);
                        NistCtrl.SetDemoInfo("FNAM", firstName);
                        NistCtrl.SetDemoInfo("LNAM", lastName);
                        NistCtrl.SetDemoInfo("SEX", sex);
                        NistCtrl.SetDemoInfo("POB", nationality);
                        NistCtrl.SetDemoInfo("DOB", Convert.ToDateTime(dateOfBirth).ToString("yyyyMMdd"));
                        NistCtrl.PhotoWidth = 400;
                        NistCtrl.PhotoHeight = 760;
                        string ruta = string.Format(@"{0}\{1}.jpg", folder, NoTransaccion);
                        NistCtrl.SetPhotoInfo(ruta, "FF");
                        NistCtrl.SetType15(1);
                        NistCtrl.Pack();
                        return true;
                    }
                    else return false;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public class SearchPersonas
        {


            public static JArray getEnrolled(long travelId, string hostname)
            {
                try
                {
                    string sql = string.Format("SELECT ENROLLED_TRAVELER_ID AS ENROLLED_ID FROM TRAVELER_EVENT_LINK WHERE TRAVEL_EVENT_ID={0}", travelId);
                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("Id", sql));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, hostname);
                    dynamic ar = JArray.Parse(res)[0];
                    if (ar.Id == "Id")
                        return ar.Obj;
                    return null;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            public static JArray getAll(string officeCode, string hostname, char tipo)
            {
                try
                {
                    string sql0 = string.Format("SELECT TE.*, TP.TRAVELER_PHOTO AS TRAVEL_PHOTO FROM TRAVEL_EVENT TE LEFT OUTER JOIN TRAVELER_PHOTO TP ON TP.TRAVEL_EVENT_ID = TE.TRAVEL_EVENT_ID WHERE TE.FIRST_INSPECT_DECISION_CODE = 'S' AND TE.FIRST_INSPECT_OFFICE_CODE = '{0}' AND TRAVEL_EVENT_TYPE_CODE='{1}' AND TE.TRAVEL_EVENT_ID NOT IN (SELECT B.TRAVEL_EVENT_ID FROM SECOND_INSPECTION B WHERE B.SECOND_INSPECT_DECISION_CODE = 'P' AND B.INVESTIGATION_FLAG = 1)", officeCode, tipo);
                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("SecondInspection", sql0));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, hostname);
                    dynamic ar = JArray.Parse(res)[0];
                    if (ar.Id == "SecondInspection")
                        return ar.Obj;
                    return null;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }


            public static JArray getAllInvestigacion(string officeCode, string hostname)
            {
                try
                {
                    string sql0 = string.Format("SELECT TE.*, TP.TRAVELER_PHOTO AS TRAVEL_PHOTO  FROM TRAVEL_EVENT TE LEFT OUTER JOIN TRAVELER_PHOTO TP ON TP.TRAVEL_EVENT_ID = TE.TRAVEL_EVENT_ID  INNER JOIN SECOND_INSPECTION SI ON TE.TRAVEL_EVENT_ID = SI.TRAVEL_EVENT_ID LEFT JOIN TRAVEL_EVENT_INVESTIGATION TI ON TE.TRAVEL_EVENT_ID = TI.TRAVEL_EVENT_ID WHERE TI.TRAVEL_EVENT_ID IS NULL AND SI.INVESTIGATION_FLAG=1 AND SI.SECOND_INSPECT_DECISION_CODE='P'AND TE.FIRST_INSPECT_OFFICE_CODE = '{0}'", officeCode);
                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("SecondInspection", sql0));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, hostname);
                    dynamic ar = JArray.Parse(res)[0];
                    if (ar.Id == "SecondInspection")
                        return ar.Obj;
                    return null;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            public static JArray getFotografiaMovimiento(long travelEventId, string hostname)
            {
                try
                {
                    string sql0 = string.Format("SELECT TE.*, TP.TRAVELER_PHOTO AS TRAVEL_PHOTO  FROM TRAVEL_EVENT TE LEFT OUTER JOIN TRAVELER_PHOTO TP ON TP.TRAVEL_EVENT_ID = TE.TRAVEL_EVENT_ID WHERE TE.TRAVEL_EVENT_ID = '{0}'", travelEventId);
                    JArray post = new JArray();
                    JObject ob0 = (JObject)JToken.FromObject(new JsonItem("SecondInspection", sql0));
                    post.Add(ob0);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, hostname);
                    return JArray.Parse(res);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            public static JArray getToday(string officeCode, string hostname, char tipo)
            {
                try
                {
                    string sql0 = string.Format("SELECT TE.*, TP.TRAVELER_PHOTO AS TRAVEL_PHOTO FROM TRAVEL_EVENT TE LEFT OUTER JOIN TRAVELER_PHOTO TP ON TP.TRAVEL_EVENT_ID = TE.TRAVEL_EVENT_ID WHERE TRUNC(TE.FIRST_INSPECT_BEGIN_TIME) = TRUNC(SYSDATE) AND TE.FIRST_INSPECT_DECISION_CODE = 'S' AND TE.FIRST_INSPECT_OFFICE_CODE = '{0}' AND TE.TRAVEL_EVENT_TYPE_CODE = '{1}' AND TE.TRAVEL_EVENT_ID NOT IN (SELECT B.TRAVEL_EVENT_ID FROM SECOND_INSPECTION B WHERE B.SECOND_INSPECT_DECISION_CODE='P' AND B.INVESTIGATION_FLAG=1)", officeCode, tipo);
                    string sql1 = string.Format("SELECT COUNT(*) AS CANTIDAD FROM TRAVEL_EVENT TE WHERE TE.FIRST_INSPECT_DECISION_CODE = 'S' AND TE.FIRST_INSPECT_OFFICE_CODE = '{0}' AND TE.TRAVEL_EVENT_TYPE_CODE = '{1}' AND TE.TRAVEL_EVENT_ID NOT IN (SELECT B.TRAVEL_EVENT_ID FROM SECOND_INSPECTION B WHERE B.SECOND_INSPECT_DECISION_CODE = 'P' AND B.INVESTIGATION_FLAG = 1)", officeCode, tipo);
                    JArray post = new JArray();
                    JObject ob0 = (JObject)JToken.FromObject(new JsonItem("SecondInspection", sql0));
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("SecondInspectionCount", sql1));
                    post.Add(ob0);
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, hostname);
                    return JArray.Parse(res);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            public static JArray getInvestigacion(string officeCode, string hostname)
            {
                try
                {
                    string sql0 = string.Format("SELECT TE.*, TP.TRAVELER_PHOTO AS TRAVEL_PHOTO  FROM TRAVEL_EVENT TE LEFT OUTER JOIN TRAVELER_PHOTO TP ON TP.TRAVEL_EVENT_ID = TE.TRAVEL_EVENT_ID  INNER JOIN SECOND_INSPECTION SI ON TE.TRAVEL_EVENT_ID = SI.TRAVEL_EVENT_ID LEFT JOIN TRAVEL_EVENT_INVESTIGATION TI ON TE.TRAVEL_EVENT_ID = TI.TRAVEL_EVENT_ID WHERE TI.TRAVEL_EVENT_ID IS NULL AND SI.INVESTIGATION_FLAG=1 AND TRUNC(TE.FIRST_INSPECT_BEGIN_TIME) = TRUNC(SYSDATE) AND TE.FIRST_INSPECT_OFFICE_CODE = '{0}'", officeCode);
                    string sql1 = string.Format("SELECT COUNT(*) AS CANTIDAD FROM TRAVEL_EVENT TE INNER JOIN SECOND_INSPECTION SI ON TE.TRAVEL_EVENT_ID = SI.TRAVEL_EVENT_ID LEFT JOIN TRAVEL_EVENT_INVESTIGATION TI ON TE.TRAVEL_EVENT_ID = TI.TRAVEL_EVENT_ID WHERE TI.TRAVEL_EVENT_ID IS NULL AND SI.INVESTIGATION_FLAG=1 AND TE.FIRST_INSPECT_OFFICE_CODE = '{0}'", officeCode);
                    JArray post = new JArray();
                    JObject ob0 = (JObject)JToken.FromObject(new JsonItem("SecondInspection", sql0));
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("SecondInspectionCount", sql1));
                    post.Add(ob0);
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, hostname);
                    return JArray.Parse(res);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            public static JArray getMovimientosIncompletos(string officeCode, string usuario, string fechaInicial, string fechaFinal, string hostname)
            {

                try
                {
                    string where = "";
                    if (officeCode.Trim() != string.Empty)
                        where += string.Format(" AND TRAVEL_EVENT. FIRST_INSPECT_OFFICE_CODE = '{0}'", officeCode);

                    if (usuario.Trim() != string.Empty)
                        where += string.Format(" AND TRAVEL_EVENT.FIRST_INSPECT_USER_ID = '{0}'", usuario);

                    if (fechaInicial.Trim() != string.Empty && fechaFinal.Trim() != string.Empty)
                        where += string.Format(" AND TRUNC(TRAVEL_EVENT.FIRST_INSPECT_BEGIN_TIME) BETWEEN TO_DATE('{0}', 'dd/mm/yyyy') AND TO_DATE('{1}', 'dd/mm/yyyy')", fechaInicial, fechaFinal);

                    string sql = string.Format("SELECT TRAVEL_EVENT.FIRST_INSPECT_USER_ID AS USERID, USERS.LOGIN_NAME AS USUARIO, UPPER(USERS.FIRST_NAME) AS NOMBRE, UPPER (USERS.LAST_NAME) AS APELLIDO, Count(*) as TOTAL FROM TRAVEL_EVENT INNER JOIN USERS ON TRAVEL_EVENT.FIRST_INSPECT_USER_ID = USERS.USER_ID WHERE FIRST_INSPECT_DECISION_CODE ='I' AND USERS.ACTIVE_FLAG = 1") + where + "GROUP BY TRAVEL_EVENT.FIRST_INSPECT_USER_ID, USERS.LOGIN_NAME, USERS.FIRST_NAME, USERS.LAST_NAME ORDER BY 4 DESC";
                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("MovimientosIncompletos", sql));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, hostname);
                    dynamic ar = JArray.Parse(res)[0];

                    if (ar.Id == "MovimientosIncompletos")
                        return ar.Obj;
                    return null;
                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }
            public static JArray getMovimientosIncompletosPorUsuario(string usuario, string hostname)
            {
                try
                {
                    string sql0 = string.Format("SELECT * FROM TRAVEL_EVENT TE WHERE FIRST_INSPECT_DECISION_CODE ='I' AND TE.FIRST_INSPECT_USER_ID= {0}", usuario);
                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("MovimientosIncompletosPorUsuario", sql0));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, hostname);
                    dynamic ar = JArray.Parse(res)[0];
                    if (ar.Id == "MovimientosIncompletosPorUsuario")
                        return ar.Obj;
                    return null;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            public static JArray getPersonaRegistrada(int tipoDocumento, string NumeroDocumento, string primerNombre, string segundoNombre, string fechaNacimiento, string hostname)
            {
                try
                {

                    string sql0 = string.Format("Select P.PersonId, P.FirstName, P.SecondName, P.FirstLastName, P.SecondLastName, E.DocumentNumber, PP.Image  from Persons P inner join Enrolments E on p.PersonId=E.FKPersonId inner join PersonPictures PP on P.PersonId= PP.FKPersonId where e.DocumentNumber ='{0}' or p.FKCatalogId={1} or p.FirstName ='{2}' or p.SecondName='{3}' or p.DateOfBirth='{4}'", NumeroDocumento, tipoDocumento, primerNombre, segundoNombre, fechaNacimiento);
                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("PersonaRegistrada", sql0));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, hostname);
                    dynamic ar = JArray.Parse(res)[0];
                    if (ar.Id == "PersonaRegistrada")
                        return ar.Obj;
                    return null;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            public static JArray getPersonaRegistradaPorNombres(string primerNombre, string segundoNombre, string fechaNacimiento, string hostname)
            {
                try
                {
                    string sql0 = string.Format("Select P.PersonId, P.FirstName, P.SecondName, P.FirstLastName, P.SecondLastName, E.DocumentNumber, PP.Image  from Persons P inner join Enrolments E on p.PersonId=E.FKPersonId inner join PersonPictures PP on P.PersonId= PP.FKPersonId where (p.FirstName ='{0}' and p.SecondName='{1}') and p.DateOfBirth='{2}'", primerNombre, segundoNombre, fechaNacimiento);
                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("PersonaRegistrada", sql0));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, hostname);
                    dynamic ar = JArray.Parse(res)[0];
                    if (ar.Id == "PersonaRegistrada")
                        return ar.Obj;
                    return null;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            public static JArray getPersonabyPersonaId(int PersonaId, string hostname)
            {
                try
                {
                    string sql0 = string.Format("Select P.PersonId, concat (P.FirstName, P.SecondName) as Nombres, concat (P.FirstLastName, P.SecondLastName) as Apellidos,  PP.Image, C.Name as Sexo, N.Name as Nacionalidad  from Persons P inner join Catalogs C on p.FKSexId=C.CatalogsId inner join PersonPictures PP on P.PersonId= PP.FKPersonId  inner join Nationality N on P.FKNationalityId= N.NationalityId  where P.PersonId={0}", PersonaId);
                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("PersonaRegistrada", sql0));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, hostname);
                    dynamic ar = JArray.Parse(res)[0];
                    if (ar.Id == "PersonaRegistrada")
                        return ar.Obj;
                    return null;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            public static JArray getEnrolledKeyInfo(long travelId, string hostname)
            {
                try
                {
                    string sql0 = string.Format("SELECT * FROM ENROLLED_TRAVELER_KEY_INFO WHERE ENROLLED_TRAVELER_ID={0}", travelId);
                    JArray post = new JArray();
                    JObject ob0 = (JObject)JToken.FromObject(new JsonItem("KEY_INFO", sql0));
                    post.Add(ob0);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, hostname);
                    return JArray.Parse(res);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            public static JArray getFilter(string tipoDocumento, string paisEmisor, string numeroDocumento, string viajeroId, string primerNombre, string primerApellido, string nacionalidad, string fechaInicial, string fechaFinal, string officeCode, string hostname, char tipo)
            {
                try
                {
                    string where = "";
                    if (tipoDocumento.Trim() != string.Empty)
                        where += string.Format(" AND TE.TRAVEL_DOC_TYPE_CODE = '{0}'", tipoDocumento);

                    if (paisEmisor.Trim() != string.Empty)
                        where += string.Format(" AND TE.TRAVEL_DOC_ISSUE_COUNTRY = '{0}'", paisEmisor);

                    if (numeroDocumento.Trim() != string.Empty)
                        where += string.Format(" AND TE.TRAVEL_DOC_NO = '{0}'", numeroDocumento);

                    if (primerNombre.Trim() != string.Empty)
                        where += string.Format(" AND TE.FIRST_NAME = '{0}'", primerNombre);

                    if (primerApellido.Trim() != string.Empty)
                        where += string.Format(" AND TE.LAST_NAME = '{0}'", primerApellido);

                    if (nacionalidad.Trim() != string.Empty)
                        where += string.Format(" AND TE.NATIONALITY_CODE = '{0}'", nacionalidad);

                    if (fechaInicial.Trim() != string.Empty && fechaFinal.Trim() != string.Empty)
                        where += string.Format(" AND TRUNC(TE.FIRST_INSPECT_BEGIN_TIME) BETWEEN TO_DATE('{0}', 'dd/mm/yyyy') AND TO_DATE('{1}', 'dd/mm/yyyy')", fechaInicial, fechaFinal);

                    string sql = string.Format("SELECT TE.*, TP.TRAVELER_PHOTO AS TRAVEL_PHOTO FROM TRAVEL_EVENT TE LEFT OUTER JOIN TRAVELER_PHOTO TP ON TP.TRAVEL_EVENT_ID = TE.TRAVEL_EVENT_ID WHERE TE.FIRST_INSPECT_DECISION_CODE = 'S' AND TE.FIRST_INSPECT_OFFICE_CODE = '{0}' AND TE.TRAVEL_EVENT_TYPE_CODE = '{1}' AND TE.TRAVEL_EVENT_ID NOT IN (SELECT B.TRAVEL_EVENT_ID FROM SECOND_INSPECTION B WHERE B.SECOND_INSPECT_DECISION_CODE = 'P' AND B.INVESTIGATION_FLAG = 1)", officeCode, tipo) + where;
                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("SecondInspection", sql));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, hostname);
                    dynamic ar = JArray.Parse(res)[0];

                    if (ar.Id == "SecondInspection")
                        return ar.Obj;
                    return null;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            public static JArray getFilterInvestigacion(string tipoDocumento, string paisEmisor, string numeroDocumento, string viajeroId, string primerNombre, string primerApellido, string nacionalidad, string fechaInicial, string fechaFinal, string officeCode, string hostname)
            {
                try
                {
                    string where = "";
                    if (tipoDocumento.Trim() != string.Empty)
                        where += string.Format(" AND TE.TRAVEL_DOC_TYPE_CODE = '{0}'", tipoDocumento);

                    if (paisEmisor.Trim() != string.Empty)
                        where += string.Format(" AND TE.TRAVEL_DOC_ISSUE_COUNTRY = '{0}'", paisEmisor);

                    if (numeroDocumento.Trim() != string.Empty)
                        where += string.Format(" AND TE.TRAVEL_DOC_NO = '{0}'", numeroDocumento);

                    if (primerNombre.Trim() != string.Empty)
                        where += string.Format(" AND TE.FIRST_NAME = '{0}'", primerNombre);

                    if (primerApellido.Trim() != string.Empty)
                        where += string.Format(" AND TE.LAST_NAME = '{0}'", primerApellido);

                    if (nacionalidad.Trim() != string.Empty)
                        where += string.Format(" AND TE.NATIONALITY_CODE = '{0}'", nacionalidad);

                    if (fechaInicial.Trim() != string.Empty && fechaFinal.Trim() != string.Empty)
                        where += string.Format(" AND TRUNC(TE.FIRST_INSPECT_BEGIN_TIME) BETWEEN TO_DATE('{0}', 'dd/mm/yyyy') AND TO_DATE('{1}', 'dd/mm/yyyy')", fechaInicial, fechaFinal);

                    string sql = string.Format("SELECT TE.*, TP.TRAVELER_PHOTO AS TRAVEL_PHOTO  FROM TRAVEL_EVENT TE LEFT OUTER JOIN TRAVELER_PHOTO TP ON TP.TRAVEL_EVENT_ID = TE.TRAVEL_EVENT_ID  INNER JOIN SECOND_INSPECTION SI ON TE.TRAVEL_EVENT_ID = SI.TRAVEL_EVENT_ID LEFT JOIN TRAVEL_EVENT_INVESTIGATION TI ON TE.TRAVEL_EVENT_ID = TI.TRAVEL_EVENT_ID WHERE TI.TRAVEL_EVENT_ID IS NULL AND TE.FIRST_INSPECT_DECISION_CODE='S' AND SI.INVESTIGATION_FLAG=1 AND SI.SECOND_INSPECT_DECISION_CODE='P' AND TE.FIRST_INSPECT_OFFICE_CODE = '{0}'", officeCode) + where;
                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("SecondInspection", sql));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, hostname);
                    dynamic ar = JArray.Parse(res)[0];

                    if (ar.Id == "SecondInspection")
                        return ar.Obj;
                    return null;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            public static JArray getRedFlagsFirstInspection(long travelId, string hostname)
            {
                try
                {
                    string sql = string.Format("SELECT * FROM FIRST_INSPECT_RED_FLAG WHERE TRAVEL_EVENT_ID={0}", travelId);
                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("ListFlags", sql));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, hostname);
                    dynamic ar = JArray.Parse(res)[0];

                    if (ar.Id == "ListFlags")
                        return ar.Obj;
                    return null;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            public static JArray getCafisTrans(long travelId)
            {
                try
                {
                    string sql = string.Format("SELECT * FROM CAFIS_TRANS_QUEUE WHERE TRAVEL_EVENT_ID={0}", travelId);
                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("Cafis", sql));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, VariablesGlobales.HOST_NAME);
                    dynamic ar = JArray.Parse(res)[0];
                    if (ar.Id == "Cafis")
                        return ar.Obj;
                    return null;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            public static JArray getControlMinorSelected(long travelId, string hostname)
            {
                try
                {
                    string sql = string.Format("SELECT * FROM AUTHORIZED_ADULT_TRAVEL WHERE TRAVEL_EVENT_ID={0}", travelId);
                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("Adults", sql));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, hostname);
                    dynamic ar = JArray.Parse(res)[0];

                    if (ar.Id == "Adults")
                        return ar.Obj;
                    return null;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            public static JArray getAlertRecord(long travelId, string hostname)
            {
                try
                {
                    string sql = string.Format("SELECT * FROM ALERT_RECORD WHERE TRAVEL_EVENT_ID={0} AND UPPER(ISSUING_INSTITUTION) != 'INTERPOL'", travelId);
                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("Alerts", sql));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, hostname);
                    dynamic ar = JArray.Parse(res)[0];

                    if (ar.Id == "Alerts")
                        return ar.Obj;
                    return null;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            public static JArray getAlertRecordInterpol(long travelId, string hostname)
            {
                try
                {
                    string sql = string.Format("SELECT * FROM ALERT_RECORD WHERE TRAVEL_EVENT_ID={0} AND UPPER(ISSUING_INSTITUTION)='INTERPOL'", travelId);
                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("Alerts", sql));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, hostname);
                    dynamic ar = JArray.Parse(res)[0];

                    if (ar.Id == "Alerts")
                        return ar.Obj;
                    return null;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            public static JArray getControlArchivos(long travelId)
            {
                try
                {
                    string sql = string.Format("SELECT * FROM OFFICE_GV_FILE WHERE TRAVEL_EVENT_ID={0}", travelId);
                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("File", sql));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, VariablesGlobales.HOST_NAME);
                    dynamic ar = JArray.Parse(res)[0];

                    if (ar.Id == "File")
                        return ar.Obj;
                    return null;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            public static JArray getImagenesPasaporte(long travelId)
            {
                try
                {
                    string sql = string.Format("SELECT TYPE_IMG, PASSPORT_PHOTO, LAST_UPDATE_TIME FROM SECOND_INSPECTION_PASSPORT WHERE TRAVEL_EVENT_ID={0}", travelId);
                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("Pasaporte", sql));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, VariablesGlobales.HOST_NAME);
                    dynamic ar = JArray.Parse(res)[0];

                    if (ar.Id == "Pasaporte")
                        return ar.Obj;
                    return null;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }





            public static bool existeMovimiento(long travelId)
            {
                try
                {
                    string sql = String.Format("SELECT * FROM SECOND_INSPECTION WHERE TRAVEL_EVENT_ID={0}", travelId);
                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("SecondInspect", sql));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, VariablesGlobales.HOST_NAME);
                    dynamic ar = JArray.Parse(res)[0];
                    if (ar.Id == "SecondInspect")
                    {
                        JArray array = ar.Obj;
                        return (array.Count > 0);
                    }
                    else return false;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            public static bool guardar(long travelId, char decisionCode, int investigation)
            {
                try
                {
                    string sql = String.Format("INSERT INTO SECOND_INSPECTION (TRAVEL_EVENT_ID, SECOND_INSPECT_OFFICE_CODE, SECOND_INSPECT_USER_ID, SECOND_INSPECT_WS_ID, SECOND_INSPECT_BEGIN_TIME, SECOND_INSPECT_DECISION_CODE, INVESTIGATION_FLAG, LAST_UPDATE_USER) VALUES ({0},'{1}',{2},{3},SYSDATE,'{4}',{5},{6})", travelId, VariablesGlobales.OFFICE_CODE, VariablesGlobales.USER_ID, VariablesGlobales.WS_ID, decisionCode, investigation, VariablesGlobales.USER_ID);
                    string res = web.Insertar(VariablesGlobales.TOKEN, sql, VariablesGlobales.HOST_NAME);
                    return (res == "\"\"");
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            public static bool guardarInvestigacion(long travelId, string comentario)
            {
                try
                {
                    string sql = String.Format("INSERT INTO TRAVEL_EVENT_INVESTIGATION (TRAVEL_EVENT_ID, LAST_UPDATE_USER, LAST_UPDATE_TIME, INVESTIGATION_NOTE) VALUES ({0}, {1}, SYSDATE,'{2}')", travelId, VariablesGlobales.USER_ID, comentario);
                    string res = web.Insertar(VariablesGlobales.TOKEN, sql, VariablesGlobales.HOST_NAME);
                    return (res == "\"\"");
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            public static bool actualizarSecondInspect(long travelId, string comentario, char decisionCode, int investigation)
            {
                try
                {
                    string sql = String.Format("UPDATE SECOND_INSPECTION SET SECOND_INSPECT_OFFICE_CODE='{0}', SECOND_INSPECT_USER_ID={1}, SECOND_INSPECT_WS_ID={2}, SECOND_INSPECT_END_TIME=SYSDATE, SECOND_INSPECT_NOTE='{3}', SECOND_INSPECT_DECISION_CODE='{4}', INVESTIGATION_FLAG={5}, LAST_UPDATE_USER={6} WHERE TRAVEL_EVENT_ID={7}", VariablesGlobales.OFFICE_CODE, VariablesGlobales.USER_ID, VariablesGlobales.WS_ID, comentario, decisionCode, investigation, VariablesGlobales.USER_ID, travelId);
                    string res = web.Insertar(VariablesGlobales.TOKEN, sql, VariablesGlobales.HOST_NAME);
                    return (res == "\"\"");
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }





            public class SearchHistoryInformation
            {
                private static ExodusBcService web = new ExodusBcService();

                public static JArray getHistorialViajes(long enrolledId, string hostname)
                {
                    try
                    {
                        string sql = string.Format("SELECT * FROM (SELECT LM.*,ROW_NUMBER() OVER (ORDER BY LM.LAST_UPDATE_TIME DESC) AS fila FROM DNII_LISTADO_MOVIMIENTOS LM WHERE LM.ENROLLED_TRAVELER_ID = {0}) WHERE fila > 0 ORDER BY LAST_UPDATE_TIME DESC", enrolledId);
                        JArray post = new JArray();
                        JObject ob1 = (JObject)JToken.FromObject(new JsonItem("ListMov", sql));
                        post.Add(ob1);
                        string pre = post.ToString().Substring(0, post.ToString().Length);
                        string res = web.GetJson(VariablesGlobales.TOKEN, pre, hostname);
                        dynamic ar = JArray.Parse(res)[0];

                        if (ar.Id == "ListMov")
                            return ar.Obj;
                        return null;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

                public static JArray getHistorialViajesTop5(long enrolledId, string hostname)
                {
                    try
                    {
                        string sql = string.Format("SELECT * FROM (SELECT LM.*,ROW_NUMBER() OVER (ORDER BY LM.LAST_UPDATE_TIME DESC) AS fila FROM DNII_LISTADO_MOVIMIENTOS LM WHERE LM.ENROLLED_TRAVELER_ID = {0}) WHERE fila <= 5 ORDER BY LAST_UPDATE_TIME DESC", enrolledId);
                        JArray post = new JArray();
                        JObject ob1 = (JObject)JToken.FromObject(new JsonItem("ListMov", sql));
                        post.Add(ob1);
                        string pre = post.ToString().Substring(0, post.ToString().Length);
                        string res = web.GetJson(VariablesGlobales.TOKEN, pre, hostname);
                        dynamic ar = JArray.Parse(res)[0];

                        if (ar.Id == "ListMov")
                            return ar.Obj;
                        return null;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

                public static JArray getInformacionAdicional(long enrolledId, string hostname)
                {
                    try
                    {
                        JArray post = new JArray();
                        string[] keys = {
                        "Informacion",
                        "CantidadES",
                        "CantidadDocs",
                        "UltEntradas",
                        "UltSalidas",
                        "Decisiones",
                        "SecondIns",
                        "Fotos",
                        "CantidadObs",
                        "AlertaViajes"
                    };
                        string[] script = {
                        string.Format("SELECT MIDDLE_NAME, LAST_NAME2, PROFESSION_CODE, BIRTH_COUNTRY_CODE, RESIDENCE_COUNTRY_CODE, UPPER(DESTINATION_ADDRESS) AS DESTINATION_ADDRESS FROM (SELECT LM.*, ROW_NUMBER() OVER(ORDER BY LM.LAST_UPDATE_TIME DESC) as fila FROM TRAVEL_EVENT LM INNER JOIN TRAVELER_EVENT_LINK TL ON TL.TRAVEL_EVENT_ID=LM.TRAVEL_EVENT_ID INNER JOIN ENROLLED_TRAVELER ET ON ET.ENROLLED_TRAVELER_ID=TL.ENROLLED_TRAVELER_ID  WHERE TL.ENROLLED_TRAVELER_ID = {0}) WHERE fila = 1", enrolledId),
                        string.Format("SELECT TRAVEL_EVENT_TYPE_CODE AS TIPO, COUNT(TRAVEL_EVENT_TYPE_CODE) AS CANTIDAD FROM DNII_LISTADO_MOVIMIENTOS WHERE ENROLLED_TRAVELER_ID = {0} GROUP BY TRAVEL_EVENT_TYPE_CODE", enrolledId),
                        string.Format("SELECT COUNT(TRAVEL_DOC_TYPE_CODE) AS CANT_DOCUMENTOS FROM ENROLLED_TRAVELER_KEY_INFO WHERE ENROLLED_TRAVELER_ID = {0}", enrolledId),
                        string.Format("SELECT PAIS FROM (SELECT LM.ORIGIN_COUNTRY_CODE AS PAIS, ROW_NUMBER() OVER (ORDER BY LM.LAST_UPDATE_TIME DESC) AS FILA FROM DNII_LISTADO_MOVIMIENTOS LM WHERE LM.TRAVEL_EVENT_TYPE_CODE='ENTRADA' AND LM.ENROLLED_TRAVELER_ID =  {0}) WHERE FILA <= 3", enrolledId),
                        string.Format("SELECT PAIS FROM (SELECT LM.DESTINATION_COUNTRY_CODE AS PAIS, ROW_NUMBER() OVER (ORDER BY LM.LAST_UPDATE_TIME DESC) AS FILA FROM DNII_LISTADO_MOVIMIENTOS LM WHERE LM.TRAVEL_EVENT_TYPE_CODE='SALIDA' AND LM.ENROLLED_TRAVELER_ID =  {0}) WHERE FILA <= 3", enrolledId),
                        string.Format("SELECT FIRST_INSPECT_DECISION_CODE AS DECISION, COUNT(FIRST_INSPECT_DECISION_CODE) AS CANTIDAD FROM TRAVEL_EVENT TE INNER JOIN TRAVELER_EVENT_LINK TL ON TL.TRAVEL_EVENT_ID = TE.TRAVEL_EVENT_ID INNER JOIN ENROLLED_TRAVELER ET ON ET.ENROLLED_TRAVELER_ID = TL.ENROLLED_TRAVELER_ID WHERE ET.ENROLLED_TRAVELER_ID = {0} GROUP BY FIRST_INSPECT_DECISION_CODE", enrolledId),
                        string.Format("SELECT COUNT( TE.TRAVEL_EVENT_ID ) AS CANTIDAD FROM TRAVEL_EVENT TE INNER JOIN SECOND_INSPECTION SI ON SI.TRAVEL_EVENT_ID = TE.TRAVEL_EVENT_ID INNER JOIN TRAVELER_EVENT_LINK TL ON TL.TRAVEL_EVENT_ID = TE.TRAVEL_EVENT_ID INNER JOIN ENROLLED_TRAVELER ET ON ET.ENROLLED_TRAVELER_ID = TL.ENROLLED_TRAVELER_ID WHERE ET.ENROLLED_TRAVELER_ID = {0}", enrolledId),
                        string.Format("SELECT COUNT( TP.TRAVEL_EVENT_ID ) AS CANTIDAD FROM TRAVEL_EVENT TE INNER JOIN TRAVELER_PHOTO TP ON TP.TRAVEL_EVENT_ID = TE.TRAVEL_EVENT_ID INNER JOIN TRAVELER_EVENT_LINK TL ON TL.TRAVEL_EVENT_ID = TE.TRAVEL_EVENT_ID INNER JOIN ENROLLED_TRAVELER ET ON ET.ENROLLED_TRAVELER_ID = TL.ENROLLED_TRAVELER_ID WHERE ET.ENROLLED_TRAVELER_ID = {0}", enrolledId),
                        string.Format("SELECT COUNT(*) AS OBSERVACIONES FROM DNII_LISTADO_COMENTARIOS LC WHERE LC.ENROLLED_TRAVELER_ID = {0}", enrolledId),
                        string.Format("SELECT 1 AS FLAG FROM DNII_LISTADO_MOVIMIENTOS LM WHERE LM.ENROLLED_TRAVELER_ID = {0} AND LM.LAST_UPDATE_TIME >= (SELECT SYSDATE-(SELECT CONFIG_NUM_VALUE FROM SYSTEM_CONFIG WHERE CONFIG_NAME='ALERT_TRAVEL_DAYS') AS FECHA_INICIAL FROM DUAL) GROUP BY LM.ENROLLED_TRAVELER_ID HAVING COUNT(LM.TRAVEL_EVENT_ID) >= (SELECT CONFIG_NUM_VALUE FROM SYSTEM_CONFIG WHERE CONFIG_NAME='ALERT_TRAVEL_AMOUNT')", enrolledId)
                    };
                        for (int i = 0; i < keys.Length; i++)
                            post.Add((JObject)JToken.FromObject(new JsonItem(keys[i], script[i])));

                        string pre = post.ToString().Substring(0, post.ToString().Length);
                        string res = web.GetJson(VariablesGlobales.TOKEN, pre, hostname);
                        return JArray.Parse(res);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

                public static JArray getHistorialObservaciones(long enrolledId, string hostname)
                {
                    try
                    {
                        string sql = string.Format("SELECT * FROM (SELECT LC.*,ROW_NUMBER() OVER (ORDER BY LC.LAST_UPDATE_TIME DESC) AS fila FROM DNII_LISTADO_COMENTARIOS LC WHERE LC.ENROLLED_TRAVELER_ID = {0} AND LC.COMMENTS IS NOT NULL) WHERE fila > 0 ORDER BY LAST_UPDATE_TIME DESC", enrolledId);
                        JArray post = new JArray();
                        JObject ob1 = (JObject)JToken.FromObject(new JsonItem("ListObs", sql));
                        post.Add(ob1);
                        string pre = post.ToString().Substring(0, post.ToString().Length);
                        string res = web.GetJson(VariablesGlobales.TOKEN, pre, hostname);
                        dynamic ar = JArray.Parse(res)[0];

                        if (ar.Id == "ListObs")
                            return ar.Obj;
                        return null;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

                public static JArray getHistorialObservacionesTop5(long enrolledId, string hostname)
                {
                    try
                    {
                        string sql = string.Format("SELECT * FROM (SELECT LC.*,ROW_NUMBER() OVER (ORDER BY LC.LAST_UPDATE_TIME DESC) AS fila FROM DNII_LISTADO_COMENTARIOS LC WHERE LC.ENROLLED_TRAVELER_ID = {0} AND LC.COMMENTS IS NOT NULL) WHERE fila <= 5 ORDER BY LAST_UPDATE_TIME DESC", enrolledId);
                        JArray post = new JArray();
                        JObject ob1 = (JObject)JToken.FromObject(new JsonItem("ListObs", sql));
                        post.Add(ob1);
                        string pre = post.ToString().Substring(0, post.ToString().Length);
                        string res = web.GetJson(VariablesGlobales.TOKEN, pre, hostname);
                        dynamic ar = JArray.Parse(res)[0];

                        if (ar.Id == "ListObs")
                            return ar.Obj;
                        return null;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

                public static JArray getHistorialDocumentos(long enrolledId, string hostname)
                {
                    try
                    {
                        string sql = string.Format("SELECT * FROM DNII_LISTADO_DOCUMENTOS WHERE ENROLLED_TRAVELER_ID = {0} ORDER BY LAST_UPDATE_TIME DESC", enrolledId);
                        JArray post = new JArray();
                        JObject ob1 = (JObject)JToken.FromObject(new JsonItem("ListDocs", sql));
                        post.Add(ob1);
                        string pre = post.ToString().Substring(0, post.ToString().Length);
                        string res = web.GetJson(VariablesGlobales.TOKEN, pre, hostname);
                        dynamic ar = JArray.Parse(res)[0];
                        if (ar.Id == "ListDocs")
                            return ar.Obj;
                        return null;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

                public static JArray getHistorialFotografias(long enrolledId, string hostname)
                {
                    try
                    {
                        string sql = string.Format("SELECT TP.*, TE.FIRST_INSPECT_DECISION_CODE FROM TRAVELER_PHOTO TP INNER JOIN TRAVEL_EVENT TE ON TE.TRAVEL_EVENT_ID = TP.TRAVEL_EVENT_ID INNER JOIN TRAVELER_EVENT_LINK TL ON TL.TRAVEL_EVENT_ID = TE.TRAVEL_EVENT_ID INNER JOIN ENROLLED_TRAVELER ET ON ET.ENROLLED_TRAVELER_ID = TL.ENROLLED_TRAVELER_ID WHERE ET.ENROLLED_TRAVELER_ID = {0} AND TE.FIRST_INSPECT_DECISION_CODE != 'S' ORDER BY TP.LAST_UPDATE_TIME DESC", enrolledId);
                        JArray post = new JArray();
                        JObject ob1 = (JObject)JToken.FromObject(new JsonItem("Pictures", sql));
                        post.Add(ob1);
                        string pre = post.ToString().Substring(0, post.ToString().Length);
                        string res = web.GetJson(VariablesGlobales.TOKEN, pre, hostname);
                        dynamic ar = JArray.Parse(res)[0];
                        if (ar.Id == "Pictures")
                            return ar.Obj;
                        return null;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

                public static JArray getUltimaFotografia(long enrolledId, string hostname)
                {
                    try
                    {
                        string sql = string.Format("SELECT * FROM (SELECT TP.*, TE.FIRST_INSPECT_DECISION_CODE, ROW_NUMBER() OVER(ORDER BY TP.LAST_UPDATE_TIME DESC) AS fila FROM TRAVELER_PHOTO TP INNER JOIN TRAVEL_EVENT TE ON TE.TRAVEL_EVENT_ID = TP.TRAVEL_EVENT_ID INNER JOIN TRAVELER_EVENT_LINK TL ON TL.TRAVEL_EVENT_ID = TE.TRAVEL_EVENT_ID INNER JOIN ENROLLED_TRAVELER ET ON ET.ENROLLED_TRAVELER_ID = TL.ENROLLED_TRAVELER_ID WHERE ET.ENROLLED_TRAVELER_ID = {0} AND TE.FIRST_INSPECT_DECISION_CODE != 'S' ORDER BY TP.LAST_UPDATE_TIME DESC) WHERE fila = 1", enrolledId);
                        JArray post = new JArray();
                        JObject ob1 = (JObject)JToken.FromObject(new JsonItem("UltFoto", sql));
                        post.Add(ob1);
                        string pre = post.ToString().Substring(0, post.ToString().Length);
                        string res = web.GetJson(VariablesGlobales.TOKEN, pre, hostname);
                        dynamic ar = JArray.Parse(res)[0];
                        if (ar.Id == "UltFoto")
                            return ar.Obj;
                        return null;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

                public static JArray getUltimaEntrada(long enrolledId, string hostname)
                {
                    try
                    {
                        string sql = string.Format("SELECT TO_CHAR(FIRST_INSPECT_BEGIN_TIME, 'YYYY-MM-DD HH24:Mi:SS') AS FIRST_INSPECT_BEGIN_TIME,TRAVEL_EVENT_ID,STAY_OR_OVERSTAY_DURATION FROM TRAVEL_EVENT WHERE FIRST_INSPECT_BEGIN_TIME =(SELECT MAX(FIRST_INSPECT_BEGIN_TIME) as UltimaEntrada FROM TRAVELER_EVENT_LINK TEL INNER JOIN TRAVEL_EVENT TE ON  TEL.TRAVEL_EVENT_ID = TE.TRAVEL_EVENT_ID WHERE TEL.ENROLLED_TRAVELER_ID = {0} AND TE.TRAVEL_EVENT_TYPE_CODE = 'E')", enrolledId);
                        JArray post = new JArray();
                        JObject ob1 = (JObject)JToken.FromObject(new JsonItem("UltimaEntrada", sql));
                        post.Add(ob1);
                        string pre = post.ToString().Substring(0, post.ToString().Length);
                        string res = web.GetJson(VariablesGlobales.TOKEN, pre, hostname);
                        dynamic ar = JArray.Parse(res)[0];
                        if (ar.Id == "UltimaEntrada")
                            return ar.Obj;
                        return null;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

                public static JArray getUltimoMovimiento(long enrolledId, string hostname)
                {
                    try
                    {
                        string sql = string.Format("SELECT * FROM (SELECT LM.*, ROW_NUMBER() OVER (ORDER BY LM.LAST_UPDATE_TIME DESC) AS fila FROM DNII_LISTADO_MOVIMIENTOS LM WHERE LM.ENROLLED_TRAVELER_ID = {0}) WHERE fila = 1 ORDER BY LAST_UPDATE_TIME DESC", enrolledId);
                        JArray post = new JArray();
                        JObject ob1 = (JObject)JToken.FromObject(new JsonItem("UltimoMovimiento", sql));
                        post.Add(ob1);
                        string pre = post.ToString().Substring(0, post.ToString().Length);
                        string res = web.GetJson(VariablesGlobales.TOKEN, pre, hostname);
                        dynamic ar = JArray.Parse(res)[0];
                        if (ar.Id == "UltimoMovimiento")
                            return ar.Obj;
                        return null;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

                public static JArray getEnrolled(string primerNombre, string primerApellido, string fechaNacimiento, string sexo, string nacionalidad)
                {
                    try
                    {
                        string sql = string.Format("SELECT ET.ENROLLED_TRAVELER_ID FROM ENROLLED_TRAVELER ET WHERE ET.FIRST_NAME = '{0}' AND ET.LAST_NAME = '{1}' AND TRUNC(ET.DATE_OF_BIRTH) = TO_DATE('{2}', 'dd-MM-yyyy') AND ET.SEX_CODE = '{3}' AND ET.NATIONALITY_CODE = '{4}'", primerNombre, primerApellido, fechaNacimiento, sexo, nacionalidad);
                        JArray post = new JArray();
                        JObject ob1 = (JObject)JToken.FromObject(new JsonItem("Id", sql));
                        post.Add(ob1);
                        string pre = post.ToString().Substring(0, post.ToString().Length);
                        string res = web.GetJson(VariablesGlobales.TOKEN, pre, VariablesGlobales.HOST_NAME);
                        dynamic ar = JArray.Parse(res)[0];
                        if (ar.Id == "Id")
                            return ar.Obj;
                        return null;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }
        }


        #region CONSULTAS Y TRAMITES DE CERTIFICADOS
        public class RequestCertificate
        {
            public enum TipoEvento : int
            {
                InicioSesion = 1,
                CerrarSesion = 2,
                Insertar = 3,
                Modificar = 4,
                Eliminar = 5,
                Consultar = 6
            }

            public enum Modulos : int
            {
                Enrolamiento = 1,
                Certificados = 2,
                Registro = 3
            }

            public static object GetItemFromJArray(JArray array, string ItemName)
            {
                object dato = null;
                foreach (JObject obj in array)
                    dato = (obj != null) ? obj.GetValue(ItemName) : null;
                return dato;
            }

            public static JArray GetPersonByDocument(int documentType, string documentNumber, string primerNombre = "", string primerApellido = "",
                DateTime? FechaNacimiento = null, int sexo = 0)
            {
                try
                {
                    StringBuilder sql = new StringBuilder();
                    string nacimiento = string.Empty;
                    string Where = string.Empty;
                    sql.Append("SELECT dc.DocumentTypeId, dc.Description AS DocumentType, er.DocumentNumber, pr.PersonalMovilPhone, pr.WorkPhone, pr.HomePhone, ");
                    sql.Append("Concat(pr.FirstName,' ', pr.SecondName) as Names, pr.FirstName, pr.FirstLastName, er.EnrolmentId, ");
                    sql.Append("Concat(pr.FirstLastName,' ', pr.SecondLastName) as LastNames, pr.FirstLastName, FORMAT(CONVERT(date, pr.DateOfBirth, 201), 'dd/MM/yyyy') as FechaNacimiento, pr.FKSexId, pp.Name as Profesion, cat.Name as ESTADOCIVIL, ");
                    sql.Append("pr.PersonalEmail, er.FKDocumentType, er.EnrolmentId, pr.PersonId, pr.FKCountryId, pr.FKCountryResidenceId, nb.Name as Colonia, pr.Address,");
                    sql.Append("pr.FKStateCountryId, pr.FKStateCityId, pr.FKNeighborhoodId, ct.Alpha2, dc.Code, st.Name as City, nt.Name as Nacionality ");
                    sql.Append("FROM Persons pr ");
                    sql.Append("INNER JOIN Enrolments er ON pr.PersonId = er.FKPersonId ");
                    sql.Append("INNER JOIN DocumentType dc ON er.FKDocumentType = dc.DocumentTypeId ");
                    sql.Append("INNER JOIN Country ct ON pr.FKCountryId = ct.CountryId ");
                    sql.Append("INNER JOIN StateCity st ON pr.FKStateCityId = st.StateCityId ");
                    sql.Append("INNER JOIN Catalogs cat ON pr.FKCivilStatusId = cat.CatalogsId ");
                    sql.Append("INNER JOIN Nationality nt ON pr.FKNationalityId = nt.NationalityId ");
                    sql.Append("INNER JOIN Neighborhood nb ON pr.FKNeighborhoodId = nb.NeighborhoodId ");
                    sql.Append("INNER JOIN PersonProfessions pp ON pr.FKPersonProfessionsId = pp.PersonProfessionId ");

                    if (documentType != 0 & documentNumber != string.Empty)
                        Where = " WHERE dc.DocumentTypeId = " + documentType + " And er.DocumentNumber = '" + documentNumber + "' ";
                    else
                    {
                        if (primerNombre != string.Empty) Where = "Where pr.FirstName = '" + primerNombre + "' ";
                        if (primerApellido != string.Empty) Where += " And pr.FirstLastName = '" + primerApellido + "' ";
                        if (FechaNacimiento != null & FechaNacimiento != Convert.ToDateTime("01/01/0001"))
                        {
                            nacimiento = string.Format("{0:yyyy/MM/dd}", FechaNacimiento.Value.Date);
                            Where += " And pr.DateOfBirth = '" + nacimiento + "' ";
                        }
                        if (sexo != 0) Where += " And pr.FKSexId = " + sexo + " ";
                    }

                    sql.Append(Where);
                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("Person", sql.ToString()));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, "");
                    dynamic ar = JArray.Parse(res)[0];

                    if (ar.Id == "Person")
                        return ar.Obj;
                    else return null;
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::GetPersonByDocument", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    throw ex;
                }
            }

            public static JArray GetPersonByNombres(string primerNombre, string primerApellido, DateTime? FechaNacimiento, int sexo)
            {
                try
                {
                    StringBuilder sql = new StringBuilder();
                    string nacimiento = string.Empty;
                    string Where = string.Empty;
                    sql.Append("SELECT dc.DocumentTypeId, dc.Description AS DocumentType, er.DocumentNumber, pr.PersonalMovilPhone, pr.WorkPhone, pr.HomePhone, ");
                    sql.Append("Concat(pr.FirstName,' ', pr.SecondName) as Names, pr.FirstName, pr.FirstLastName, er.EnrolmentId, ");
                    sql.Append("Concat(pr.FirstLastName,' ', pr.SecondLastName) as LastNames, FORMAT(CONVERT(date, pr.DateOfBirth, 201), 'dd/MM/yyyy') as FechaNacimiento, pr.FKSexId, pp.Name as Profesion, cat.Name as ESTADOCIVIL, ");
                    sql.Append("pr.PersonalEmail, er.FKDocumentType, er.EnrolmentId, pr.PersonId, pr.FKCountryId, pr.FKCountryResidenceId, nb.Name as Colonia, pr.Address,");
                    sql.Append("pr.FKStateCountryId, pr.FKStateCityId, pr.FKNeighborhoodId, ct.Alpha2, dc.Code, st.Name as City, nt.Name as Nacionality ");
                    sql.Append("FROM Persons pr ");
                    sql.Append("INNER JOIN Enrolments er ON pr.PersonId = er.FKPersonId ");
                    sql.Append("LEFT JOIN DocumentType dc ON er.FKDocumentType = dc.DocumentTypeId ");
                    sql.Append("INNER JOIN Country ct ON pr.FKCountryId = ct.CountryId ");
                    sql.Append("INNER JOIN StateCity st ON pr.FKStateCityId = st.StateCityId ");
                    sql.Append("INNER JOIN Catalogs cat ON pr.FKCivilStatusId = cat.FKCatalogTypesId ");
                    sql.Append("INNER JOIN Nationality nt ON pr.FKNationalityId = nt.NationalityId ");
                    sql.Append("INNER JOIN Neighborhood nb ON pr.FKNeighborhoodId = nb.NeighborhoodId ");
                    sql.Append("INNER JOIN PersonProfessions pp ON pr.FKPersonProfessionsId = pp.PersonProfessionId ");

                    if (primerNombre != string.Empty) Where = "Where pr.FirstName = '" + primerNombre + "' ";
                    if (primerApellido != string.Empty) Where += " And pr.FirstLastName = '" + primerApellido + "' ";
                    if (FechaNacimiento != null & FechaNacimiento != Convert.ToDateTime("01/01/0001"))
                    {
                        nacimiento = string.Format("{0:yyyy-MM-dd}", FechaNacimiento.Value.Date);
                        Where += " And pr.DateOfBirth = '" + nacimiento + "' ";
                    }
                    if (sexo != 0) Where += " And pr.FKSexId = " + sexo + " ";

                    sql.Append(Where);

                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("Person", sql.ToString()));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, "");
                    dynamic ar = JArray.Parse(res)[0];

                    if (ar.Id == "Person")
                        return ar.Obj;
                    else return null;
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::GetPersonByDocument", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    throw ex;
                }
            }

            public static JArray GetExistCustomer(string docId)
            {
                try
                {
                    string sql = string.Empty;
                    sql = "SELECT * FROM Customers WHERE DocumentNumber = '" + docId + "' ";

                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("Customers", sql.ToString()));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, "");
                    dynamic ar = JArray.Parse(res)[0];

                    if (ar.Id == "Customers") return ar.Obj;
                    else return null;
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::GetPersonByDocument", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    throw ex;
                }
            }

            public static JArray GetPersonById(int prsId)
            {
                try
                {
                    StringBuilder sql = new StringBuilder();
                    string nacimiento = string.Empty;
                    string Where = string.Empty;
                    sql.Append("SELECT dc.DocumentTypeId, dc.Description AS DocumentType, er.DocumentNumber, pr.PersonalMovilPhone, pr.WorkPhone, ");
                    sql.Append("Concat(pr.FirstName,' ', pr.SecondName) as Names, ");
                    sql.Append("Concat(pr.FirstLastName,' ', pr.SecondLastName) as LastNames, ");
                    sql.Append("pr.PersonalEmail, er.FKDocumentType, er.EnrolmentId, pr.PersonId, pr.FKCountryId,");
                    sql.Append("pr.FKStateCountryId, pr.FKStateCityId, pr.FKNeighborhoodId, pr.Address, ct.Alpha2, dc.Code, st.Name as City ");
                    sql.Append("FROM Persons pr ");
                    sql.Append("INNER JOIN Enrolments er ON pr.PersonId = er.FKPersonId ");
                    sql.Append("INNER JOIN DocumentType dc ON er.FKDocumentType = dc.DocumentTypeId ");
                    sql.Append("INNER JOIN Country ct ON pr.FKCountryId = ct.CountryId ");
                    sql.Append("INNER JOIN StateCity st ON pr.FKStateCityId = st.StateCityId ");
                    sql.Append("WHERE pr.PersonId = " + prsId + " ");

                    sql.Append(Where);
                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("Person", sql.ToString()));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, "");
                    dynamic ar = JArray.Parse(res)[0];

                    if (ar.Id == "Person")
                        return ar.Obj;
                    else return null;
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::GetPersonByDocument", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    throw ex;
                }
            }

            public static JArray GetRepresentanteByPersonaId(string docIdPrsJuridica, string namePrsJuridica)
            {
                try
                {
                    StringBuilder sql = new StringBuilder();
                    string nacimiento = string.Empty;
                    string Where = string.Empty;
                    sql.Append("SELECT rp.RepresentativeRequestId, ");
                    sql.Append("Concat(pr.FirstName, ' ', pr.FirstLastName) as Nombres,  ");
                    sql.Append("Concat(pr.FirstLastName, ' ', pr.SecondLastName) as Apellidos, ");
                    sql.Append("nt.Name as Nacionalidad  ");
                    sql.Append("FROM RepresentativeRequest rp ");
                    sql.Append("INNER JOIN Request rq ON rp.FKRequestId = rq.RequestId ");
                    sql.Append("INNER JOIN Enrolments er ON rp.FKEnrolmentId = er.EnrolmentId ");
                    sql.Append("INNER JOIN Persons pr ON rp.FKPersonId = pr.PersonId ");
                    sql.Append("INNER JOIN Nationality nt ON pr.FKNationalityId = nt.NationalityId ");
                    sql.Append("WHERE rp.DocumentNumberLegalPerson = '" + docIdPrsJuridica + "' And  rp.NameLegalPeson = '" + namePrsJuridica + "' ");

                    sql.Append(Where);
                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("PersonRepresentative", sql.ToString()));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, "");
                    dynamic ar = JArray.Parse(res)[0];

                    if (ar.Id == "PersonRepresentative")
                        return ar.Obj;
                    else return null;
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::GetPersonByDocument", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    throw ex;
                }
            }

            public static bool InsertNewRequestPJ(Request rq)
            {
                try
                {
                    string sql = string.Format("INSERT INTO Request (" +
                        "TicketNumber, DocumentNumberNaturalPerson, Names, LastNames, " +
                        "Email, ZipCode, CellPhonePersonal, PhoneWork, NameLegalPerson, " +
                        "DocumentNumberLegalPerson, CreationDate, ModifyDate, PasswordClient, " +
                        "FKDocumentTypeNaturalPerson, FKDocumentTypeLegalPerson, " +
                        "FKCountryId, FKStateCountryId, FKStateCityId, FKNeighborhoodId, " +
                        "FKEnrolmentId, FKPersonId, FKCatalogStatusId, FKCreateUserId, " +
                        "FKTransactionStatusId, FKProductId, FKOfficeId, FKCertificateOfficeId, " +
                        "FKRegistrationAuthorityId, FKRegistrationAgentId, LegalPersonPhone, LegalPersonEmail, " +
                        "DocumentNumberCustomer, FKWorkStationId, FKPersonTypeId, NumeroFacturaManual)" +
                        "VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', GETDATE(), GETDATE(), " +
                        "'{12}', {13}, {14}, {15}, {16}, {17}, {18}, {19}, {20}, {21}, {22}, {23}, {24}, {25}, {26}, {27}, {28}, '{29}', '{30}','{31}', {32}, {33}, '{34}' )",
                        rq.TicketNumber, rq.DocumentNumberNaturalPerson, rq.Names, rq.LastName,
                        rq.Email, rq.ZipCode, rq.CellPhonePersonal, rq.PhoneWork, rq.NameLegalPerson,
                        rq.DocumentNumberLegalPerson, rq.CreationDate, rq.ModifyDate, rq.PasswordClient,
                        rq.FKDocumentTypeNaturalPerson, rq.FKDocumentTypeLegalPerson,
                        rq.FKCountryId, rq.FKStateCountryId, rq.FKStateCityId, rq.FKNeighborhoodId,
                        rq.FKEnrolmentId, rq.FKPersonId, rq.FKCatalogStatusId, rq.FKCreateUserId,
                        rq.FKTransactionStatusId, rq.FKProductId, rq.FKOfficeId, rq.FKCertificateOfficeId,
                        rq.FKRegistrationAuthorityId, rq.FKRegistrationAgentId, rq.LegalPersonPhone, rq.LegalPersonEmail, rq.DocumentNumberCustomer, rq.FKWorkStationId, rq.FKPersonTypeId, rq.FacturaManual);

                    string res = web.Insertar(VariablesGlobales.TOKEN, sql, VariablesGlobales.HOST_NAME);
                    return (res == "\"\"");
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::InsertNewRequest", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return false;
                }
            }

            public static bool InsertNewRequestPN(Request rq)
            {
                try
                {
                    string sql = string.Format("INSERT INTO Request (" +
                        "TicketNumber, DocumentNumberNaturalPerson, Names, LastNames, " +
                        "Email, ZipCode, CellPhonePersonal, PhoneWork, " +
                        "CreationDate, ModifyDate, PasswordClient, " +
                        "FKDocumentTypeNaturalPerson, " +
                        "FKCountryId, FKCountryResidenceId, FKStateCountryId, FKStateCityId, FKNeighborhoodId, " +
                        "FKEnrolmentId, FKPersonId, FKCatalogStatusId, FKCreateUserId, " +
                        "FKTransactionStatusId, FKProductId, FKOfficeId, FKCertificateOfficeId, " +
                        "FKRegistrationAuthorityId, FKRegistrationAgentId, LegalPersonPhone, " +
                        "LegalPersonEmail, DocumentNumberCustomer, FKWorkStationId, FKPersonTypeId, " +
                        "NumeroFacturaManual, Profession, RegistrationNumber, InstitutionIssues) " +
                        "VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', GETDATE(), GETDATE(), " +
                        "'{10}', {11}, {12}, {13}, {14}, {15}, {16}, {17}, {18}, {19}, {20}, {21}, {22}, {23}, " +
                        "{24}, {25}, {26}, '{27}', '{28}', '{29}', {30}, {31}, '{32}', '{33}', '{34}', '{35}' )",
                        rq.TicketNumber, rq.DocumentNumberNaturalPerson, rq.Names, rq.LastName,
                        rq.Email, rq.ZipCode, rq.CellPhonePersonal, rq.PhoneWork,
                        rq.CreationDate, rq.ModifyDate, rq.PasswordClient, rq.FKDocumentTypeNaturalPerson,
                        rq.FKCountryId, rq.FKCountryResidenceId, rq.FKStateCountryId, rq.FKStateCityId, rq.FKNeighborhoodId,
                        rq.FKEnrolmentId, rq.FKPersonId, rq.FKCatalogStatusId, rq.FKCreateUserId,
                        rq.FKTransactionStatusId, rq.FKProductId, rq.FKOfficeId, rq.FKCertificateOfficeId,
                        rq.FKRegistrationAuthorityId, rq.FKRegistrationAgentId, rq.LegalPersonPhone, rq.LegalPersonEmail,
                        rq.DocumentNumberCustomer, rq.FKWorkStationId, rq.FKPersonTypeId, rq.FacturaManual, rq.Profession, rq.RegistrationNumber, rq.InstitutionIssues);

                    string res = web.Insertar(VariablesGlobales.TOKEN, sql, VariablesGlobales.HOST_NAME);
                    return (res == "\"\"");
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::InsertNewRequest", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return false;
                }
            }

            public static bool InsertNewRepresentativeRequest(RepresentativeRequest rpRq)
            {
                try
                {
                    string sql = string.Format("INSERT INTO RepresentativeRequest (" +
                        "DocumentNumber, Names, LastNames, Nacionality, CreationDate, FKRequestId, FKCreateUserId )" +
                        "VALUES ('{0}', '{1}', '{2}', '{3}', GETDATE(), {5}, {6} )",
                        rpRq.DocumentNumber, rpRq.Names, rpRq.LastNames, rpRq.Nationality,
                        rpRq.CreationDate, rpRq.FKRequestId, rpRq.FKCreateUserId);

                    string res = web.Insertar(VariablesGlobales.TOKEN, sql, VariablesGlobales.HOST_NAME);
                    return (res == "\"\"");
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::InsertNewRepresentativeRequest", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return false;
                }
            }

            public static bool InsertNewRequestAttach(RequestAttach rqa)
            {
                try
                {
                    string sql = string.Format("INSERT INTO RequestAttach (" +
                        "Name, Folder, Type, Size, CreationDate, TipoPersona, " +
                        "FKRequestId, FKDocumentType, FKCreateUserId )" +
                        "VALUES ('{0}', '{1}', '{2}', '{3}', GETDATE(), {5}, {6}, {7}, {8} )",
                        rqa.NameDocument, rqa.Folder, rqa.Type, rqa.Size, rqa.CreationDate,
                        rqa.TipoPersona, rqa.FKRequestId, rqa.FKDocumentType, rqa.FKCreateUserId);

                    string res = web.Insertar(VariablesGlobales.TOKEN, sql, VariablesGlobales.HOST_NAME);
                    return (res == "\"\"");
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::InsertNewRequestAttach", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return false;
                }
            }

            public static bool InsertNewRequestAttachImg(string strBase64, string path, string fileName)
            {
                try
                {
                    int res = web.RequestAttached(strBase64, path, fileName);
                    if (res == 1) return true;
                    else return false;
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::InsertNewRequestAttach", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return false;
                }
            }

            public static bool InsertNewRCustomer(Customers clt)
            {
                try
                {
                    string sql = string.Format("INSERT INTO Customers (" +
                        "DocumentNumber, Names, LastNames, City, Address, PersonalMovilPhone, WorkPhone, CreationDate, FKPersonType, FKCreateUserId )" +
                        "VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', GETDATE(), {8}, {9} )",
                        clt.DocumentNumber, clt.Names, clt.LastNames, clt.City, clt.Address,
                        clt.PersonalMovilPhone, clt.WorkPhone, clt.CreationDate, clt.FKPersonType, clt.FKCreateUserId);

                    string res = web.Insertar(VariablesGlobales.TOKEN, sql, VariablesGlobales.HOST_NAME);
                    return (res == "\"\"");
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::InsertNewRepresentativeRequest", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return false;
                }
            }


            //**BITÁCORAS** ==================================================================================
            public static bool InsertarRegistro(int eventoId, int usuarioId, int moduloId, string detalle)
            {
                try
                {
                    Bitacoras bt = new Bitacoras();
                    bt.Detalle = detalle;
                    bt.FKEventoId = eventoId;
                    bt.FKUsuarioId = usuarioId;
                    bt.FKModuloId = moduloId;

                    string sql = string.Format("INSERT INTO Bitacoras (" +
                        "FechaRegistro, Detalle, FKEventoId, FKUsuarioId, FKModuloId )" +
                        "VALUES (GETDATE(), '{1}', {2}, {3}, {4})", bt.FechaRegistro, bt.Detalle, bt.FKEventoId, bt.FKUsuarioId, bt.FKModuloId);

                    string res = web.Insertar(VariablesGlobales.TOKEN, sql, VariablesGlobales.HOST_NAME);
                    return (res == "\"\"");
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::InsertarBitacoras", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return false;
                }
            }

            public static bool ModificarRegistro(int eventoId, int usuarioId, int moduloId, string detalle,
                                                 object objetoAnterior, Type TipoAnterior, object objetoNuevo,
                                                 Type TipoNuevo, bool guardarImagenArchivo = false)
            {
                try
                {
                    string strXmlAnterior = string.Empty;
                    string strXmlNuevo = string.Empty;

                    //**VALORES ANTES DE MODIFICAR**
                    //===============================================================================
                    strXmlAnterior = string.Format("<{0}>", TipoAnterior.Name);
                    foreach (PropertyInfo pr in objetoAnterior.GetType().GetProperties())
                    {
                        if (pr.CanRead)
                        {
                            if (pr.PropertyType.FullName.Contains("System.String") == true
                                | pr.PropertyType.FullName.Contains("System.Int32") == true
                                | pr.PropertyType.FullName.Contains("System.Boolean") == true
                                | pr.PropertyType.FullName.Contains("System.DateTime") == true
                                | pr.PropertyType.FullName.Contains("System.Byte[]") == true)
                            {
                                if (pr.PropertyType.FullName == "System.Byte[]")
                                {
                                    if (guardarImagenArchivo == true & pr.GetValue(objetoAnterior, null) != null)
                                        strXmlAnterior = string.Format("{0}<{1}>{2}</{1}>", strXmlAnterior, pr.Name, Convert.ToBase64String((byte[])pr.GetValue(objetoAnterior, null)));
                                }
                                else if (pr.PropertyType.FullName == "System.DateTime")
                                    strXmlAnterior = string.Format("{0}<{1}>{2}</{1}>", strXmlAnterior, pr.Name, Convert.ToDateTime(pr.GetValue(objetoAnterior, null)).ToString("yyyy/MM/dd HH:mm:ss"));
                                else
                                    strXmlAnterior = string.Format("{0}<{1}>{2}</{1}>", strXmlAnterior, pr.Name, Convert.ToString(pr.GetValue(objetoAnterior, null)));
                            }
                        }
                    }
                    strXmlAnterior = string.Format("{0}</{1}>", strXmlAnterior, TipoAnterior.Name);
                    //====================================================================================



                    //**VALORES DESPUES DE MODIFICAR**
                    //====================================================================================
                    strXmlNuevo = string.Format("<{0}>", TipoNuevo.Name);
                    foreach (PropertyInfo pr in objetoNuevo.GetType().GetProperties())
                    {
                        if (pr.CanRead)
                        {
                            if (pr.PropertyType.FullName.Contains("System.String") == true
                                | pr.PropertyType.FullName.Contains("System.Int32") == true
                                | pr.PropertyType.FullName.Contains("System.Boolean") == true
                                | pr.PropertyType.FullName.Contains("System.DateTime") == true
                                | pr.PropertyType.FullName.Contains("System.Byte[]") == true)
                            {
                                if (pr.PropertyType.FullName == "System.Byte[]")
                                {
                                    if (guardarImagenArchivo == true & pr.GetValue(objetoNuevo, null) != null)
                                        strXmlNuevo = string.Format("{0}<{1}>{2}</{1}>", strXmlNuevo, pr.Name, Convert.ToBase64String((byte[])pr.GetValue(objetoNuevo, null)));
                                }
                                else if (pr.PropertyType.FullName == "System.DateTime")
                                    strXmlNuevo = string.Format("{0}<{1}>{2}</{1}>", strXmlNuevo, pr.Name, Convert.ToDateTime(pr.GetValue(objetoNuevo, null)).ToString("yyyy/MM/dd HH:mm:ss"));
                                else
                                    strXmlNuevo = string.Format("{0}<{1}>{2}</{1}>", strXmlNuevo, pr.Name, Convert.ToString(pr.GetValue(objetoNuevo, null)));
                            }
                        }
                    }
                    strXmlNuevo = string.Format("{0}</{1}>", strXmlNuevo, TipoNuevo.Name);
                    //====================================================================================


                    //**INSERTAR REGISTRO**
                    //====================================================================================
                    try
                    {
                        Bitacoras bt = new Bitacoras();
                        bt.Detalle = detalle;
                        bt.RegistroXmlOld = strXmlAnterior;
                        bt.RegistroXmlNew = strXmlNuevo;
                        bt.FKEventoId = eventoId;
                        bt.FKUsuarioId = usuarioId;
                        bt.FKModuloId = moduloId;

                        string sql = string.Format("INSERT INTO Bitacoras (" +
                            "FechaRegistro, Detalle, RegistroXmlOld, RegistroXmlOld, FKEventoId, FKUsuarioId, FKModuloId )" +
                            "VALUES (GETDATE(), '{1}', '{2}', '{3}', {4}, {5}, {6})", bt.FechaRegistro, bt.Detalle, bt.RegistroXmlOld, bt.RegistroXmlNew, bt.FKEventoId, bt.FKUsuarioId, bt.FKModuloId);

                        string res = web.Insertar(VariablesGlobales.TOKEN, sql, VariablesGlobales.HOST_NAME);
                        return (res == "\"\"");
                    }
                    catch (Exception ex)
                    {
                        Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::ModificarBitacoras", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                        return false;
                    }
                    //====================================================================================

                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::InsertarBitacoras", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return false;
                }
            }

            public static bool EliminarRegistro(int eventoId, int usuarioId, int moduloId, string detalle,
                                                object objetoAnterior, Type TipoAnterior, bool guardarImagenArchivo = false)
            {
                try
                {
                    //**VALOR ANTERIOR**
                    //===============================================================================
                    string strXml = string.Empty;
                    strXml = string.Format("<{0}>", TipoAnterior.Name);
                    foreach (PropertyInfo pr in objetoAnterior.GetType().GetProperties())
                    {
                        if (pr.CanRead)
                        {
                            if (pr.PropertyType.FullName.Contains("System.String") == true | pr.PropertyType.FullName.Contains("System.Int32") == true | pr.PropertyType.FullName.Contains("System.Boolean") == true | pr.PropertyType.FullName.Contains("System.DateTime") == true | pr.PropertyType.FullName.Contains("System.Byte[]") == true)
                            {
                                if (pr.PropertyType.FullName == "System.Byte[]")
                                {
                                    if (guardarImagenArchivo == true & pr.GetValue(objetoAnterior, null) != null)
                                        strXml = string.Format("{0}<{1}>{2}</{1}>", strXml, pr.Name, Convert.ToBase64String((byte[])pr.GetValue(objetoAnterior, null)));
                                }
                                else if (pr.PropertyType.FullName == "System.DateTime")
                                    strXml = string.Format("{0}<{1}>{2}</{1}>", strXml, pr.Name, Convert.ToDateTime(pr.GetValue(objetoAnterior, null)).ToString("yyyy/MM/dd HH:mm:ss"));
                                else
                                    strXml = string.Format("{0}<{1}>{2}</{1}>", strXml, pr.Name, Convert.ToString(pr.GetValue(objetoAnterior, null)));
                            }
                        }
                    }
                    strXml = string.Format("{0}</{1}>", strXml, TipoAnterior.Name);


                    //**INSERTAR REGISTRO**
                    //====================================================================================
                    try
                    {
                        Bitacoras bt = new Bitacoras();
                        bt.Detalle = detalle;
                        bt.RegistroXmlOld = strXml;
                        bt.FKEventoId = eventoId;
                        bt.FKUsuarioId = usuarioId;
                        bt.FKModuloId = moduloId;

                        string sql = string.Format("INSERT INTO Bitacoras (" +
                            "FechaRegistro, Detalle, RegistroXmlOld, FKEventoId, FKUsuarioId, FKModuloId )" +
                            "VALUES (GETDATE(), '{1}', '{2}', {3}, {4}, {5})", bt.FechaRegistro, bt.Detalle, bt.RegistroXmlOld, bt.FKEventoId, bt.FKUsuarioId, bt.FKModuloId);

                        string res = web.Insertar(VariablesGlobales.TOKEN, sql, VariablesGlobales.HOST_NAME);
                        return (res == "\"\"");
                    }
                    catch (Exception ex)
                    {
                        Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::EliminarBitacoras", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                        return false;
                    }
                    //====================================================================================
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::EliminarBitacoras", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return false;
                }
            }
            //============================================================================================
            //**FIN BITÁCORAS**


            public static bool InsertNewRequestFollow(RequestFollow rsf)
            {
                try
                {
                    string sql = string.Format("INSERT INTO RequestFollow (" +
                        "CreationDate, Observation, FKRequestId, FKTransactionEstatusId, FKCreateUserId, FKOfficeId, FKTransactionStatusPrevious, FKWorkstationId )" +
                        "VALUES (GETDATE(), '{0}', {1}, {2}, {3}, {4}, {5}, {6} )",
                         rsf.Observation, rsf.FKRequestId, rsf.FKTransactionEstatusId, rsf.FKCreateUserId,
                        rsf.FKOfficeId, rsf.FKTransactionStatusPrevious, rsf.FKWorkstationId);

                    string res = web.Insertar(VariablesGlobales.TOKEN, sql, VariablesGlobales.HOST_NAME);
                    return (res == "\"\"");
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::InsertNewRequestFollow", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return false;
                }
            }

            public static bool InsertNewRequestFollowValid(RequestFollowValid rsVd)
            {
                try
                {
                    string sql = string.Format("INSERT INTO RequestFollowValid (" +
                        "CreationDate, Observation, FKRequestId, FKRequestStatusId, FKCreateUserId, FKOfficeId, FKRequestStatusPreviousId, FKWorkstationId )" +
                        "VALUES (GETDATE(), '{0}', {1}, {2}, {3}, {4}, {5}, {6} )",
                         rsVd.Observation, rsVd.FKRequestId, rsVd.FKRequestStatusId, rsVd.FKCreateUserId,
                        rsVd.FKOfficeId, rsVd.FKRequestStatusPreviousId, rsVd.FKWorkstationId);

                    string res = web.Insertar(VariablesGlobales.TOKEN, sql, VariablesGlobales.HOST_NAME);
                    return (res == "\"\"");
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::InsertNewRequestFollow", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return false;
                }
            }

            public static bool UpdateRequestStatus(int rqId, int StatusId, int OfficeApproved)
            {
                try
                {
                    string sql = "Update Request Set FKTransactionStatusId = " + StatusId + ", FKOfficeApproved = " + OfficeApproved + "  WHERE RequestId = " + rqId + " ";
                    string res = web.Insertar(VariablesGlobales.TOKEN, sql, VariablesGlobales.HOST_NAME);
                    return (res == "\"\"");
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::UpdateRequestStatus", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return false;
                }
            }

            public static bool UpdateRequestStatusValid(int rqId, int statusId)
            {
                try
                {
                    string sql = "Update Request Set FKRequestStatusId = " + statusId + " WHERE RequestId = " + rqId + " ";
                    string res = web.Insertar(VariablesGlobales.TOKEN, sql, VariablesGlobales.HOST_NAME);
                    return (res == "\"\"");
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::UpdateRequestStatusValid", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return false;
                }
            }

            public static bool UpdateRequestStatusFacturaPagada(int rqId, int StatusId, string factura)
            {
                try
                {
                    string sql = string.Format("Update Request Set FKTransactionStatusId = {0}, NumeroFactura = '{1}', FechaPago = GETDATE() WHERE RequestId = {2}", StatusId, factura, rqId);
                    string res = web.Insertar(VariablesGlobales.TOKEN, sql, VariablesGlobales.HOST_NAME);
                    return (res == "\"\"");
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::UpdateRequestStatus", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return false;
                }
            }

            public static bool UpdateRequestSend(int statusTransacctionId, int statusRequestId, int rqId)
            {
                try
                {
                    string sql = "Update Request Set FKTransactionStatusId = " + statusTransacctionId + ", FKRequestStatusId = " + statusRequestId + " WHERE RequestId = " + rqId + " ";
                    string res = web.Insertar(VariablesGlobales.TOKEN, sql, VariablesGlobales.HOST_NAME);
                    return (res == "\"\"");
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::InsertNewRequestAttach", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return false;
                }
            }

            public static bool UpdateStatusRequestValid(int statusRequestId, int rqId)
            {
                try
                {
                    string sql = "Update Request Set FKRequestStatusId = " + statusRequestId + " WHERE RequestId = " + rqId + " ";
                    string res = web.Insertar(VariablesGlobales.TOKEN, sql, VariablesGlobales.HOST_NAME);
                    return (res == "\"\"");
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::InsertNewRequestAttach", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return false;
                }
            }

            public static bool UpdateRequestPasswordClient(int rqId, string pass)
            {
                try
                {
                    string sql = "Update Request Set PasswordClient = " + pass + " WHERE RequestId = " + rqId + " ";
                    string res = web.Insertar(VariablesGlobales.TOKEN, sql, VariablesGlobales.HOST_NAME);
                    return (res == "\"\"");
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::InsertNewRequestAttach", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return false;
                }
            }

            public static bool UpdateRequestStatusTicket(int rqId, string ticket, int StatusId)
            {
                try
                {
                    string sql = "Update Request Set FKRequestStatusId = " + StatusId + ", TicketNumber = '" + ticket + "' WHERE RequestId = " + rqId + " ";
                    string res = web.Insertar(VariablesGlobales.TOKEN, sql, VariablesGlobales.HOST_NAME);
                    return (res == "\"\"");
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::InsertNewRequestAttach", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return false;
                }
            }

            public static bool UpdateRequestOrderNumber(int rqId, string NumeroPedido, int statusId)
            {
                try
                {
                    string sql = "Update Request Set NumeroPedido = '" + NumeroPedido + "', FKTransactionStatusId = " + statusId + " WHERE RequestId = " + rqId + " ";
                    string res = web.Insertar(VariablesGlobales.TOKEN, sql, VariablesGlobales.HOST_NAME);
                    return (res == "\"\"");
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::InsertNewRequestAttach", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return false;
                }
            }

            public static bool UpdateUserPasswordUser(int usrId, string pass, int reset = 0)
            {
                try
                {
                    string sql = "Update Users Set Password = '" + pass + "', ResetPassword = " + reset + " WHERE UserId = " + usrId + " ";
                    string res = web.Insertar(VariablesGlobales.TOKEN, sql, VariablesGlobales.HOST_NAME);
                    return (res == "\"\"");
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::UpdateRequestStatus", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return false;
                }
            }

            public static bool UpdateRequestReview(int statusTransacctionId, int rqId, Request rq)
            {
                try
                {
                    string sql = string.Format("UPDATE Request SET " +
                       "DocumentNumberNaturalPerson = '{0}', Names = '{1}', LastNames = '{2}',  Email = '{3}', CellPhonePersonal = '{4}', " +
                       "PhoneWork = '{5}', FKStateCountryId = {6}, FKStateCityId = {7}, FKNeighborhoodId = {8}, FKProductId = {9}, " +
                       "NameLegalPerson = '{10}', DocumentNumberLegalPerson = '{11}', LegalPersonEmail = '{12}', LegalPersonPhone = '{13}', " +
                       "FKTransactionStatusId = {14}, ModifyDate = GETDATE(), RegistrationNumber = '{15}' WHERE RequestId = {16} ",
                       rq.DocumentNumberNaturalPerson, rq.Names, rq.LastName, rq.Email, rq.CellPhonePersonal,
                       rq.PhoneWork, rq.FKStateCountryId, rq.FKStateCityId, rq.FKNeighborhoodId, rq.FKProductId,
                       rq.NameLegalPerson, rq.DocumentNumberLegalPerson, rq.LegalPersonEmail, rq.LegalPersonPhone,
                       statusTransacctionId, rq.RegistrationNumber, rqId);

                    string res = web.Insertar(VariablesGlobales.TOKEN, sql, VariablesGlobales.HOST_NAME);
                    return (res == "\"\"");
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::InsertNewRequestAttach", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return false;
                }
            }

            public static bool DeleteAttachRequestFolder(int rqId, string folder)
            {
                try
                {
                    string sql = "Delete from RequestAttach WHERE FKRequestId = '" + rqId + "'And Folder = '" + folder + "' ";
                    string res = web.Insertar(VariablesGlobales.TOKEN, sql, VariablesGlobales.HOST_NAME);
                    return (res == "\"\"");
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::DeleteAttachRequestFolder", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return false;
                }
            }

            public static bool DeleteAttachRequestDirectory(string folder)
            {
                try
                {
                    bool res = web.DeleteRequestAttached(folder);
                    if (res) return res;
                    else return false;
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::DeleteAttachRequestDirectory", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return false;
                }
            }

            public static bool UpdateAttachRequestDirectory(string Base64, string folder, string fileName)
            {
                try
                {
                    bool res = web.UpdateRequestAttached(Base64, folder, fileName);
                    if (res) return res;
                    else return false;
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::UpdateAttachRequestDirectory", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return false;
                }
            }


            public static bool DeleteRepresentativeRequestById(int rqId, string docNumber)
            {
                try
                {
                    string sql = "Delete from RepresentativeRequest WHERE FKRequestId = '" + rqId + "'And DocumentNumber = '" + docNumber + "' ";
                    string res = web.Insertar(VariablesGlobales.TOKEN, sql, VariablesGlobales.HOST_NAME);
                    return (res == "\"\"");
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::DeleteAttachRequestFolder", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return false;
                }
            }

            public static string GetRequestAttachImg(string path, string fileName)
            {
                try
                {
                    string res = web.GetRequestAttached(path, fileName);
                    if (res != null) return res;
                    else return null;
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::InsertNewRequestAttach", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return null;
                }
            }

            public static JArray GetRequestAttachById(int rqaId)
            {
                try
                {
                    StringBuilder sql = new StringBuilder();
                    sql.Append("SELECT FKRequestId FROM RequestAttach ");
                    sql.Append("WHERE FKRequestId = " + rqaId + " ");

                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("RequestAttach", sql.ToString()));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, "");
                    dynamic ar = JArray.Parse(res)[0];

                    if (ar.Id == "RequestAttach")
                        return ar.Obj;
                    else return null;
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::GetRequestTrayByUserId", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return null;
                }
            }

            public static JArray GetRequestById(int rqId)
            {
                try
                {
                    StringBuilder sql = new StringBuilder();
                    sql.Append("SELECT DocumentNumberNaturalPerson as IDENTIFICACIONPN, DocumentNumberLegalPerson as IDENTIFICACIONPJ, ");
                    sql.Append("rq.Names as NOMBRES, rq.LastNames as APELLIDOS, rq.NameLegalPerson as NOMBREPJ, pd.Code as CODIGOPRODUCTO, ");
                    sql.Append("pd.Description as DESCRIPCIONPRODUCTO, rq.CellPhonePersonal, rq.RequestId, rq.FKProductId, rq.Email, pr.DateOfBirth, rq.FKTransactionStatusId, ");
                    sql.Append("ct.Name as Sexo, ct1.Name as EstadoCivil, nc.Name as Nacionalidad, st.Name as Departamento, stc.Name as Municipio, DocumentNumberCustomer, ");
                    sql.Append("pr.Address as Direccion, pr.PersonId, LegalPersonEmail, LegalPersonPhone, FORMAT(CONVERT(datetime2, rq.CreationDate, 201), 'dd/MM/yyyy HH:mm') as Fecha, us.Name as Usuario, ofc.Name as Oficina, ofc.OfficeId, rq.FKPersonTypeId, rq.RegistrationNumber as NUMEROCOLEGIO ");
                    sql.Append("FROM Request rq ");
                    sql.Append("INNER JOIN Products pd ON rq.FKProductId = pd.ProductId ");
                    sql.Append("INNER JOIN Persons pr ON rq.FKPersonId = pr.PersonId ");
                    sql.Append("INNER JOIN StateCountry st on pr.FKStateCountryId = st.StateCountryId ");
                    sql.Append("INNER JOIN StateCity stc ON pr.FKStateCityId = stc.StateCityId ");
                    sql.Append("INNER JOIN Nationality nc ON pr.FKNationalityId = nc.NationalityId ");
                    sql.Append("INNER JOIN Catalogs ct ON pr.FKSexId = ct.CatalogsId ");
                    sql.Append("INNER JOIN Catalogs ct1 ON pr.FKCivilStatusId = ct1.CatalogsId ");
                    sql.Append("INNER JOIN Users us ON rq.FKCreateUserId = us.UserId ");
                    sql.Append("INNER JOIN Office ofc ON rq.FKOfficeId = ofc.OfficeId ");
                    sql.Append("WHERE rq.RequestId = " + rqId + " ");

                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("Request", sql.ToString()));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, "");
                    dynamic ar = JArray.Parse(res)[0];

                    if (ar.Id == "Request")
                        return ar.Obj;
                    else return null;
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::GetRequestById", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return null;
                }
            }

            public static JArray GetRequestByIdCertificados(int rqId, int ofcId)
            {
                try
                {
                    StringBuilder sql = new StringBuilder();
                    sql.Append("SELECT UPPER(pr.Description) as PRODUCTO, rq.TicketNumber as TICKET, cr.Description as LOCALIDAD, ag.Description as AGENTE, rs.Name ESTADOACTUAL, rs.RequestStatusId, ");
                    sql.Append("CASE WHEN dcpj.DocumentTypeId IS NOT NULL THEN 'PERSONA JURIDICA' ELSE 'PERSONA NATURAL' END AS TIPOPERSONA, ");
                    sql.Append("CASE WHEN dcpj.DocumentTypeId IS NOT NULL THEN dcpj.Description ELSE dcpn.Description END AS TIPODOCUMENTO, ");
                    sql.Append("CASE WHEN dcpj.DocumentTypeId IS NOT NULL THEN rq.DocumentNumberLegalPerson else rq.DocumentNumberNaturalPerson END AS DOCUMENTO, ");
                    sql.Append("CASE WHEN dcpj.DocumentTypeId IS NOT NULL THEN rq.NameLegalPerson else rq.Names END AS NOMBRES, ");
                    sql.Append("CASE WHEN dcpj.DocumentTypeId IS NOT NULL THEN '' else  rq.LastNames END AS APELLIDOS, ");
                    sql.Append("UPPER(ofc.Name) as OFICINA, UPPER(us.Name) as USUARIO, FORMAT(CONVERT(datetime2, rq.CreationDate, 201), 'dd/MM/yyyy HH:mm') as FECHA, ");
                    sql.Append("rq.NumeroFactura AS FACTURA, FORMAT(CONVERT(datetime2, rq.FechaPago, 201), 'dd/MM/yyyy HH:mm') as FECHAPAGO ");
                    sql.Append("FROM Request rq ");
                    sql.Append("INNER JOIN Products pr ON rq.FKProductId = pr.ProductId ");
                    sql.Append("INNER JOIN CertificateOffice cr ON rq.FKCertificateOfficeId = cr.CertificateOfficeId ");
                    sql.Append("INNER JOIN RegistrationAgent ag ON rq.FKRegistrationAgentId = ag.RegistrationAgentId ");
                    sql.Append("LEFT JOIN RequestStatus rs ON rq.FKRequestStatusId = rs.RequestStatusId ");
                    sql.Append("LEFT JOIN DocumentType dcpn ON rq.FKDocumentTypeNaturalPerson = dcpn.DocumentTypeId ");
                    sql.Append("LEFT JOIN DocumentType dcpj ON rq.FKDocumentTypeLegalPerson = dcpj.DocumentTypeId ");
                    sql.Append("INNER JOIN Office ofc ON rq.FKOfficeId = ofc.OfficeId ");
                    sql.Append("INNER JOIN Users us ON rq.FKCreateUserId = us.UserId ");
                    sql.Append("WHERE rq.RequestId = " + rqId + " And ofc.OfficeId = " + ofcId + " ");

                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("Request", sql.ToString()));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, "");
                    dynamic ar = JArray.Parse(res)[0];
                    if (ar.Id == "Request") return ar.Obj;
                    else return null;
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::GetRequestByIdCertificados", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return null;
                }
            }

            public static JArray GetSeguimientoCertificados(int rqId, int ofcId)
            {
                try
                {
                    StringBuilder sql = new StringBuilder();
                    sql.Append("SELECT FORMAT(CONVERT(datetime2, rf.CreationDate, 201), 'dd/MM/yyyy HH:mm') as FECHAREGISTRO, ");
                    sql.Append("UPPER(us.Name) AS USUARIO, ofc.Name as OFICINA, wk.Details as ESTACION, rq.Name as ESTADO ");
                    sql.Append("FROM RequestFollowValid rf ");
                    sql.Append("LEFT JOIN RequestStatus rq ON rf.FKRequestStatusId = rq.RequestStatusId ");
                    sql.Append("INNER JOIN Users us ON rf.FKCreateUserId = us.UserId ");
                    sql.Append("INNER JOIN Office ofc ON rf.FKOfficeId = ofc.OfficeId ");
                    sql.Append("INNER JOIN WorkStations wk ON rf.FKWorkstationId = wk.WorkStationsId ");
                    sql.Append("WHERE rf.FKRequestId = " + rqId + " And rf.FKOfficeId = " + ofcId + " ORDER BY rf.CreationDate Desc ");

                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("Request", sql.ToString()));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, "");
                    dynamic ar = JArray.Parse(res)[0];
                    if (ar.Id == "Request") return ar.Obj;
                    else return null;
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::GetRequestByIdCertificados", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return null;
                }
            }

            public static JArray GetSeguimientoSolicitudes(int rqId, int ofcId)
            {
                try
                {
                    StringBuilder sql = new StringBuilder();
                    sql.Append("SELECT DISTINCT ts.TransactionStatusId, FORMAT(CONVERT(datetime2, rf.CreationDate, 201), 'dd/MM/yyyy HH:mm') as FECHAREGISTRO, ");
                    sql.Append("UPPER(us.Name) AS USUARIO, ofc.Name as OFICINA, wk.Details as ESTACION, ts.Name as ESTADO, rf.CreationDate ");
                    sql.Append("FROM RequestFollow rf ");
                    sql.Append("LEFT JOIN TransactionStatus ts ON rf.FKTransactionEstatusId = ts.TransactionStatusId ");
                    sql.Append("INNER JOIN Users us ON rf.FKCreateUserId = us.UserId ");
                    sql.Append("INNER JOIN Office ofc ON rf.FKOfficeId = ofc.OfficeId ");
                    sql.Append("INNER JOIN WorkStations wk ON rf.FKWorkstationId = wk.WorkStationsId ");
                    sql.Append("WHERE rf.FKRequestId = " + rqId + " And rf.FKOfficeId = " + ofcId + " ");
                    sql.Append("GROUP BY ts.TransactionStatusId, rf.CreationDate, us.Name, ofc.Name, wk.Details, ts.Name ");
                    sql.Append("ORDER BY rf.CreationDate desc ");

                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("Request", sql.ToString()));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, "");
                    dynamic ar = JArray.Parse(res)[0];
                    if (ar.Id == "Request") return ar.Obj;
                    else return null;
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::GetSeguimientoSolicitudes", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return null;
                }
            }

            public static JArray GetSolicitudesRechazadas(int rqId, int ofcId)
            {
                try
                {
                    StringBuilder sql = new StringBuilder();
                    sql.Append("SELECT DISTINCT ts.TransactionStatusId, ");
                    sql.Append("FORMAT(CONVERT(datetime2, rf.CreationDate, 201), 'dd/MM/yyyy HH:mm') as FECHAREGISTRO, UPPER(us.Name) AS USUARIO, ");
                    sql.Append("ofc.Name as OFICINA, wk.Details as ESTACION, ts.Name as ESTADO, rf.Observation as OBSERVACION ");
                    sql.Append("FROM RequestFollow rf ");
                    sql.Append("LEFT JOIN TransactionStatus ts ON rf.FKTransactionEstatusId = ts.TransactionStatusId ");
                    sql.Append("INNER JOIN Users us ON rf.FKCreateUserId = us.UserId ");
                    sql.Append("INNER JOIN Office ofc ON rf.FKOfficeId = ofc.OfficeId ");
                    sql.Append("INNER JOIN WorkStations wk ON rf.FKWorkstationId = wk.WorkStationsId ");
                    sql.Append("WHERE rf.FKRequestId = " + rqId + " And rf.FKOfficeId = " + ofcId + " ");
                    sql.Append("GROUP BY ts.TransactionStatusId, rf.CreationDate, us.Name, ofc.Name, wk.Details, ts.Name, rf.Observation ");
                    sql.Append("Order by ts.TransactionStatusId ASC ");

                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("Request", sql.ToString()));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, "");
                    dynamic ar = JArray.Parse(res)[0];
                    if (ar.Id == "Request") return ar.Obj;
                    else return null;
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::GetSolicitudesRechazadas", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return null;
                }
            }

            public static JArray GetSolicitudesEnRevision(int rqId, int ofcId)
            {
                try
                {
                    StringBuilder sql = new StringBuilder();
                    sql.Append("SELECT rf.FKTransactionEstatusId, ");
                    sql.Append("FORMAT(CONVERT(datetime2, rf.CreationDate, 201), 'dd/MM/yyyy HH:mm') as FECHAREGISTRO, UPPER(us.Name) AS USUARIO, ");
                    sql.Append("ofc.Name as OFICINA, wk.Details as ESTACION, ts.Name as ESTADO, rf.Observation as OBSERVACION ");
                    sql.Append("FROM RequestFollow rf ");
                    sql.Append("INNER JOIN TransactionStatus ts ON rf.FKTransactionEstatusId = ts.TransactionStatusId ");
                    sql.Append("INNER JOIN Users us ON rf.FKCreateUserId = us.UserId ");
                    sql.Append("INNER JOIN Office ofc ON rf.FKOfficeId = ofc.OfficeId ");
                    sql.Append("INNER JOIN WorkStations wk ON rf.FKWorkstationId = wk.WorkStationsId ");
                    sql.Append("WHERE rf.FKRequestId = " + rqId + " And rf.FKOfficeId = " + ofcId + " ");
                    sql.Append("GROUP BY rf.FKTransactionEstatusId, rf.CreationDate, us.Name, ofc.Name, wk.Details, ts.Name, rf.Observation ");
                    sql.Append("Order by rf.CreationDate ASC ");

                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("Request", sql.ToString()));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, "");
                    dynamic ar = JArray.Parse(res)[0];
                    if (ar.Id == "Request") return ar.Obj;
                    else return null;
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::GetSolicitudesRechazadas", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return null;
                }
            }

            public static JArray GetRequestByIdRevision(int rqId)
            {
                try
                {
                    StringBuilder sql = new StringBuilder();
                    sql.Append("SELECT er.DocumentNumber as IDENTIFICACIONPN, DocumentNumberLegalPerson as IDENTIFICACIONPJ, ");
                    sql.Append("CONCAT(pr.FirstName, ' ', pr.SecondName) as NOMBRES, Concat(pr.FirstLastName, ' ', pr.SecondLastName) as APELLIDOS, rq.NameLegalPerson as NOMBREPJ, pd.Code as CODIGOPRODUCTO, pd.ProductId as PRODUCTOID, pr.WorkPhone as TelefonoOficina, pr.PersonalMovilPhone, pr.HomePhone, ");
                    sql.Append("pd.Description as DESCRIPCIONPRODUCTO, rq.RequestId, rq.FKProductId, pr.PersonalEmail, pr.DateOfBirth, ");
                    sql.Append("ct.Name as Sexo, ct1.Name as EstadoCivil, nc.Name as Nacionalidad, st.Name as Departamento, st.StateCountryId, stc.Name as Municipio, stc.StateCityId, DocumentNumberCustomer, ");
                    sql.Append("pr.Address as Direccion, pr.PersonId, rq.LegalPersonEmail, rq.LegalPersonPhone, FORMAT(CONVERT(datetime2, rq.CreationDate, 201), 'dd/MM/yyyy HH:mm') as Fecha, us.Name as Usuario, ofc.Name as Oficina, nb.Name as Colonia, nb.NeighborhoodId, rq.FKPersonTypeId as TipoPersona, rq.RegistrationNumber as NUMEROCOLEGIO ");
                    sql.Append("FROM Request rq ");
                    sql.Append("INNER JOIN Products pd ON rq.FKProductId = pd.ProductId ");
                    sql.Append("INNER JOIN Persons pr ON rq.FKPersonId = pr.PersonId ");
                    sql.Append("INNER JOIN Enrolments er ON rq.FKEnrolmentId = er.EnrolmentId ");
                    sql.Append("INNER JOIN StateCountry st on pr.FKStateCountryId = st.StateCountryId ");
                    sql.Append("INNER JOIN StateCity stc ON pr.FKStateCityId = stc.StateCityId ");
                    sql.Append("INNER JOIN Neighborhood nb ON pr.FKNeighborhoodId = nb.NeighborhoodId ");
                    sql.Append("INNER JOIN Nationality nc ON pr.FKNationalityId = nc.NationalityId ");
                    sql.Append("INNER JOIN Catalogs ct ON pr.FKSexId = ct.CatalogsId ");
                    sql.Append("INNER JOIN Catalogs ct1 ON pr.FKCivilStatusId = ct1.CatalogsId ");
                    sql.Append("INNER JOIN Users us ON rq.FKCreateUserId = us.UserId ");
                    sql.Append("INNER JOIN Office ofc ON rq.FKOfficeId = ofc.OfficeId ");
                    sql.Append("WHERE rq.RequestId = " + rqId + " ");

                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("Request", sql.ToString()));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, "");
                    dynamic ar = JArray.Parse(res)[0];

                    if (ar.Id == "Request")
                        return ar.Obj;
                    else return null;
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::GetRequestById", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return null;
                }
            }

            public static JArray GetRequestByStatusId(int ofcId)
            {
                try
                {
                    StringBuilder sql = new StringBuilder();
                    sql.Append("SELECT DocumentNumberNaturalPerson as IDENTIDAD, DocumentNumberLegalPerson, NameLegalPerson, TicketNumber as TICKET, pd.Description AS PRODUCTO, ");
                    sql.Append("rq.Names as NOMBRES, rq.LastNames as APELLIDOS, trs.Name as SEGUIMIENTO, ");
                    sql.Append("rq.NumeroFactura as FACTURA, trs.TransactionStatusId, rq.RequestId ");
                    sql.Append("FROM Request rq ");
                    sql.Append("INNER JOIN Products pd ON rq.FKProductId = pd.ProductId ");
                    sql.Append("INNER JOIN TransactionStatus trs ON rq.FKTransactionStatusId = trs.TransactionStatusId ");
                    sql.Append("WHERE rq.FKTransactionStatusId in (1, 9) And rq.FKOfficeId = " + ofcId + " ");

                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("Request", sql.ToString()));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, "");
                    dynamic ar = JArray.Parse(res)[0];

                    if (ar.Id == "Request")
                        return ar.Obj;
                    else return null;
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::GetRequestTrayByUserId", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return null;
                }
            }

            public static JArray GetRequestByStatusRevision(int statusId, int ofcId)
            {
                try
                {
                    StringBuilder sql = new StringBuilder();
                    sql.Append("SELECT  rq.DocumentNumberNaturalPerson as IDENTIDAD, DocumentNumberLegalPerson, NameLegalPerson, pd.Description AS PRODUCTO, rq.FKPersonId, ");
                    sql.Append("rq.Names as NOMBRES, rq.LastNames as APELLIDOS, trs.Name as SEGUIMIENTO, ");
                    sql.Append("rq.NumeroFactura as FACTURA, trs.TransactionStatusId, rq.RequestId,(select rf.Observation from RequestFollow rf where (rf.CreationDate=(select max(CreationDate) from RequestFollow where FKRequestId=rf.FKRequestId)) and rf.FKTransactionEstatusId=4 ) as Observacion ");
                    sql.Append("FROM Request rq ");
                    sql.Append("INNER JOIN Products pd ON rq.FKProductId = pd.ProductId ");
                    sql.Append("INNER JOIN TransactionStatus trs ON rq.FKTransactionStatusId = trs.TransactionStatusId ");
                    sql.Append("WHERE rq.FKTransactionStatusId = " + statusId + " And rq.FKOfficeId = " + ofcId + " ");

                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("Request", sql.ToString()));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, "");
                    dynamic ar = JArray.Parse(res)[0];

                    if (ar.Id == "Request")
                        return ar.Obj;
                    else return null;
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::GetRequestTrayByUserId", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return null;
                }
            }

            public static JArray GetRequestByStatusIdRequestId(int statusId, int rqId)
            {
                try
                {
                    StringBuilder sql = new StringBuilder();
                    sql.Append("SELECT DocumentNumberNaturalPerson as IDENTIDAD, DocumentNumberLegalPerson, TicketNumber as TICKET, pd.Description AS PRODUCTO, ");
                    sql.Append("rq.Names as NOMBRES, rq.LastNames as APELLIDOS, rs.Name as SEGUIMIENTO, ");
                    sql.Append("rq.NumeroFactura as FACTURA, rs.RequestStatusId, rq.RequestId, FKRequestStatusId  ");
                    sql.Append("FROM Request rq ");
                    sql.Append("INNER JOIN Products pd ON rq.FKProductId = pd.ProductId ");
                    sql.Append("INNER JOIN RequestStatus rs ON rq.FKRequestStatusId = rs.RequestStatusId ");
                    sql.Append("INNER JOIN TransactionStatus ts ON rq.FKTransactionStatusId = ts.TransactionStatusId ");
                    sql.Append("WHERE rq.FKTransactionStatusId = " + statusId + " And rq.RequestId = " + rqId + " ");

                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("Request", sql.ToString()));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, "");
                    dynamic ar = JArray.Parse(res)[0];

                    if (ar.Id == "Request")
                        return ar.Obj;
                    else return null;
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::GetRequestTrayByUserId", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return null;
                }
            }

            public static JArray GetRequestBandeja(int userId, int oficeId, DateTime Inicio, DateTime Final)
            {
                try
                {
                    string fechaInicio = Inicio.ToString("yyyyMMdd 00:00");
                    string fechaFinal = Final.ToString("yyyyMMdd 23:59:59");
                    StringBuilder sql = new StringBuilder();
                    sql.Append("SELECT TicketNumber as TICKET, pd.Description AS PRODUCTO, ");
                    sql.Append("rq.Names as NOMBRES, rq.LastNames as APELLIDOS, rs.Name as SOLICITUD, ts.Name as TRANSACCION, ");
                    sql.Append("rq.NumeroFactura as FACTURA, rq.FKRequestStatusId, rq.CreationDate, rq.FKTransactionStatusId, rq.RequestId, DocumentNumberLegalPerson, NameLegalPerson ");
                    sql.Append("FROM Request rq ");
                    sql.Append("INNER JOIN Products pd ON rq.FKProductId = pd.ProductId ");
                    sql.Append("LEFT JOIN RequestStatus rs ON rq.FKRequestStatusId = rs.RequestStatusId ");
                    sql.Append("LEFT JOIN TransactionStatus ts ON rq.FKTransactionStatusId = ts.TransactionStatusId ");
                    sql.Append("WHERE rq.FKCreateUserId = " + userId + " And rq.FKOfficeId = " + oficeId + " ");
                    sql.Append("And (FKRequestStatusId in (1,2) or FKRequestStatusId is null) And rq.FKTransactionStatusId in (2, 3, 4, 6, 7) ");
                    sql.Append("and cast(rq.CreationDate as DATE) between '" + fechaInicio + "' and '" + fechaFinal + "' order by rq.CreationDate Asc  ");

                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("Request", sql.ToString()));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, "");
                    dynamic ar = JArray.Parse(res)[0];

                    if (ar.Id == "Request")
                        return ar.Obj;
                    else return null;
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::GetRequestBandeja", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return null;
                }
            }

            public static JArray GetRequestBandejaTodas(DateTime Inicio, DateTime Final)
            {
                try
                {
                    string fechaInicio = Inicio.ToString("yyyyMMdd 00:00");
                    string fechaFinal = Final.ToString("yyyyMMdd 23:59:59");
                    StringBuilder sql = new StringBuilder();
                    sql.Append("SELECT TicketNumber as TICKET, pd.Description AS PRODUCTO, ");
                    sql.Append("rq.Names as NOMBRES, rq.LastNames as APELLIDOS, rs.Name as SOLICITUD, ts.Name as TRANSACCION, ");
                    sql.Append("rq.NumeroFactura as FACTURA, rq.FKRequestStatusId, rq.CreationDate, rq.FKTransactionStatusId, rq.RequestId, DocumentNumberLegalPerson, NameLegalPerson ");
                    sql.Append("FROM Request rq ");
                    sql.Append("INNER JOIN Products pd ON rq.FKProductId = pd.ProductId ");
                    sql.Append("LEFT JOIN RequestStatus rs ON rq.FKRequestStatusId = rs.RequestStatusId ");
                    sql.Append("LEFT JOIN TransactionStatus ts ON rq.FKTransactionStatusId = ts.TransactionStatusId ");
                    sql.Append("WHERE (FKRequestStatusId in (1,2) or FKRequestStatusId is null) And rq.FKTransactionStatusId in (2, 3, 4, 6, 7) ");
                    sql.Append("and cast(rq.CreationDate as DATE) between '" + fechaInicio + "' and '" + fechaFinal + "' order by rq.CreationDate Asc  ");

                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("Request", sql.ToString()));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, "");
                    dynamic ar = JArray.Parse(res)[0];

                    if (ar.Id == "Request")
                        return ar.Obj;
                    else return null;
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::GetRequestTrayByUserId", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return null;
                }
            }

            public static JArray GetRequestBandejaCertificados(int userId, int oficeId, DateTime Inicio, DateTime Final)
            {
                try
                {
                    string fechaInicio = Inicio.ToString("yyyyMMdd 00:00");
                    string fechaFinal = Final.ToString("yyyyMMdd 23:59:59");
                    StringBuilder sql = new StringBuilder();
                    sql.Append("SELECT TicketNumber as TICKET, pd.Description AS PRODUCTO, ");
                    sql.Append("rq.Names as NOMBRES, rq.LastNames as APELLIDOS, rs.Name as SOLICITUD, ts.Name as TRANSACCION, ");
                    sql.Append("rq.NumeroFactura as FACTURA, rq.FKRequestStatusId, rq.CreationDate, rq.FKTransactionStatusId, rq.RequestId, DocumentNumberLegalPerson, NameLegalPerson ");
                    sql.Append("FROM Request rq ");
                    sql.Append("INNER JOIN Products pd ON rq.FKProductId = pd.ProductId ");
                    sql.Append("LEFT JOIN RequestStatus rs ON rq.FKRequestStatusId = rs.RequestStatusId ");
                    sql.Append("LEFT JOIN TransactionStatus ts ON rq.FKTransactionStatusId = ts.TransactionStatusId ");
                    sql.Append("WHERE rq.FKCreateUserId = " + userId + " And rq.FKOfficeId = " + oficeId + "  and cast(rq.CreationDate as DATE) between '" + fechaInicio + "' and '" + fechaFinal + "' ");
                    sql.Append("And rq.FKTransactionStatusId IN (7) And rq.FKRequestStatusId IN (4,5,6,8,12,13,15,16) order by rq.CreationDate Asc ");

                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("Request", sql.ToString()));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, "");
                    dynamic ar = JArray.Parse(res)[0];

                    if (ar.Id == "Request")
                        return ar.Obj;
                    else return null;
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::GetRequestTrayByUserId", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return null;
                }
            }

            public static JArray GetRequestBandejaCertificadosTodas(DateTime Inicio, DateTime Final)
            {
                try
                {
                    string fechaInicio = Inicio.ToString("yyyyMMdd 00:00");
                    string fechaFinal = Final.ToString("yyyyMMdd 23:59:59");
                    StringBuilder sql = new StringBuilder();
                    sql.Append("SELECT TicketNumber as TICKET, pd.Description AS PRODUCTO, ");
                    sql.Append("rq.Names as NOMBRES, rq.LastNames as APELLIDOS, rs.Name as SOLICITUD, ts.Name as TRANSACCION, ");
                    sql.Append("rq.NumeroFactura as FACTURA, rq.FKRequestStatusId, rq.CreationDate, rq.FKTransactionStatusId, rq.RequestId, DocumentNumberLegalPerson, NameLegalPerson ");
                    sql.Append("FROM Request rq ");
                    sql.Append("INNER JOIN Products pd ON rq.FKProductId = pd.ProductId ");
                    sql.Append("LEFT JOIN RequestStatus rs ON rq.FKRequestStatusId = rs.RequestStatusId ");
                    sql.Append("LEFT JOIN TransactionStatus ts ON rq.FKTransactionStatusId = ts.TransactionStatusId ");
                    sql.Append("WHERE cast(rq.CreationDate as DATE) between '" + fechaInicio + "' and '" + fechaFinal + "' ");
                    sql.Append("And rq.FKTransactionStatusId IN (7) And rq.FKRequestStatusId IN (4,5,6,8,12,13,15,16) order by rq.CreationDate Asc ");

                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("Request", sql.ToString()));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, "");
                    dynamic ar = JArray.Parse(res)[0];

                    if (ar.Id == "Request")
                        return ar.Obj;
                    else return null;
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::GetRequestTrayByUserId", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return null;
                }
            }

            public static JArray GetRequestBandejaStatusValid(int userId, int oficeId, int RequestStatusId)
            {
                try
                {
                    StringBuilder sql = new StringBuilder();
                    sql.Append("SELECT TicketNumber as TICKET, pd.Description AS PRODUCTO, ");
                    sql.Append("rq.Names as NOMBRES, rq.LastNames as APELLIDOS, rs.Name as SOLICITUD, ts.Name as Transaccion, ");
                    sql.Append("rq.NumeroFactura as FACTURA, rq.FKRequestStatusId, rq.CreationDate, rq.FKTransactionStatusId, rq.RequestId, DocumentNumberLegalPerson, NameLegalPerson ");
                    sql.Append("FROM Request rq ");
                    sql.Append("INNER JOIN Products pd ON rq.FKProductId = pd.ProductId ");
                    sql.Append("LEFT JOIN RequestStatus rs ON rq.FKRequestStatusId = rs.RequestStatusId ");
                    sql.Append("LEFT JOIN TransactionStatus ts ON rq.FKTransactionStatusId = ts.TransactionStatusId ");
                    sql.Append("WHERE rq.FKCreateUserId = " + userId + " And rq.FKOfficeId = " + oficeId + " ");
                    sql.Append("And rq.FKRequestStatusId = " + RequestStatusId + " order by rq.CreationDate Asc ");

                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("Request", sql.ToString()));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, "");
                    dynamic ar = JArray.Parse(res)[0];

                    if (ar.Id == "Request")
                        return ar.Obj;
                    else return null;
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::GetRequestTrayByUserId", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return null;
                }
            }

            public static JArray GetRequestBandejaStatusCertificados(int oficeId, DateTime _fechaInicio, DateTime _fechaFinal)
            {
                try
                {
                    string fi = _fechaInicio.ToString("yyyyMMdd 00:00");
                    string ff = _fechaFinal.ToString("yyyyMMdd 23:59:59");
                    StringBuilder sql = new StringBuilder();
                    sql.Append("SELECT rq.RequestId,CAST(rq.CreationDate AS DATE) as FECHACREACION, rq.TicketNumber as NUMEROTICKET, pr.Description AS PRODUCTO, ");
                    sql.Append("rq.Names as NOMBRES, rq.LastNames as APELLIDOS, rs.Name as STATUSVALID, ts.Name as STATUSTRANSACCION, ofi.Name as OFICINA, rq.FKTransactionStatusId ");
                    sql.Append("FROM Request rq ");
                    sql.Append("INNER JOIN PRODUCTS pr on rq.FKProductId = pr.ProductId ");
                    sql.Append("INNER JOIN Office ofi on rq.FKOfficeId = ofi.OfficeId ");
                    sql.Append("LEFT JOIN RequestStatus rs ON rq.FKRequestStatusId = rs.RequestStatusId ");
                    sql.Append("LEFT JOIN TransactionStatus ts ON rq.FKTransactionStatusId = ts.TransactionStatusId ");
                    sql.Append("WHERE rq.FKOfficeId = " + oficeId + " and cast(rq.CreationDate as DATE) between '" + fi + "' and '" + ff + "' ");

                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("Request", sql.ToString()));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, "");
                    dynamic ar = JArray.Parse(res)[0];

                    if (ar.Id == "Request") return ar.Obj;
                    else return null;
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::GetRequestTrayByUserId", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return null;
                }
            }

            public static JArray GetRequestByNumberTicket(string numberTicket)
            {
                try
                {
                    StringBuilder sql = new StringBuilder();
                    sql.Append("SELECT case when r.FKPersonTypeId = 1 then r.DocumentNumberNaturalPerson ELSE DocumentNumberLegalPerson END AS IDENTIFICACION, ");
                    sql.Append("case when r.FKPersonTypeId = 1 then concat(r.Names, ' ', r.LastNames) else r.NameLegalPerson end as NOMBRES,  ");
                    sql.Append("case when r.FKPersonTypeId = 1 then r.Email else r.LegalPersonEmail end as CORREO, ");
                    sql.Append("case when r.FKPersonTypeId = 1 then r.CellPhonePersonal else r.LegalPersonPhone end as TELEFONO, ");
                    sql.Append("t.Description as TIPOPERSONA, r.TicketNumber, r.RequestId ");
                    sql.Append("FROM Request r ");
                    sql.Append("INNER JOIN PersonType t ON r.FKPersonTypeId = t.PersonTypeId ");
                    sql.Append("WHERE TicketNumber = '" + numberTicket + "' ");

                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("Request", sql.ToString()));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, "");
                    dynamic ar = JArray.Parse(res)[0];

                    if (ar.Id == "Request") return ar.Obj;
                    else return null;
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::GetRequestByNumberTicket", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return null;
                }
            }

            public static JArray GetNumeroPedidoByTicket(string numberTicket)
            {
                try
                {
                    StringBuilder sql = new StringBuilder();
                    sql.Append("SELECT rq.NumeroPedido, rq.RequestId ");
                    sql.Append("FROM Request rq ");
                    sql.Append("WHERE TicketNumber = '" + numberTicket + "' ");

                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("Request", sql.ToString()));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, "");
                    dynamic ar = JArray.Parse(res)[0];

                    if (ar.Id == "Request")
                        return ar.Obj;
                    else return null;
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::GetRequestByNumberTicket", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return null;
                }
            }

            public static JArray GetNumeroFacturaManualByTicket(int rqId)
            {
                try
                {
                    StringBuilder sql = new StringBuilder();
                    sql.Append("SELECT rq.NumeroFacturaManual, rq.RequestId ");
                    sql.Append("FROM Request rq ");
                    sql.Append("WHERE RequestId = " + rqId + " ");

                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("Request", sql.ToString()));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, "");
                    dynamic ar = JArray.Parse(res)[0];

                    if (ar.Id == "Request") return ar.Obj;
                    else return null;
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::GetRequestByNumberTicket", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return null;
                }
            }

            public static JArray GetRequestByIdNoTicket(int rqId)
            {
                try
                {
                    StringBuilder sql = new StringBuilder();
                    sql.Append("SELECT ct.Alpha2, rq.FKCountryResidenceId, rq.FKStateCountryId, rq.FKStateCityId, rq.TicketNumber, rq.NumeroPedido, rq.NumeroFacturaManual, ");
                    sql.Append("prs.Address, rq.CellPhonePersonal, rq.PhoneWork, prs.PersonId, ");
                    sql.Append("pr.Code as CodigoProducto, pr.Description as DescripcionProducto, rq.Email, rq.Names, rq.LastNames, PasswordClient, ");
                    sql.Append("rq.DocumentNumberNaturalPerson, rq.LegalPersonPhone, rq.LegalPersonEmail, dc.Description as TipoDocumento, rq.FKRequestStatusId, dc.Code as CodigoDocumento, rq.Profession, rq.RegistrationNumber, rq.InstitutionIssues, ");
                    sql.Append("rq.NameLegalPerson, rq.NumeroFactura, rq.DocumentNumberLegalPerson, FKDocumentTypeLegalPerson, dc1.Code as CodigoDocumentoLegal ");
                    sql.Append("FROM Request rq ");
                    sql.Append("INNER JOIN Products pr ON rq.FKProductId = pr.ProductId ");
                    sql.Append("INNER JOIN Country ct ON rq.FKCountryId = ct.CountryId ");
                    sql.Append("INNER JOIN Persons prs ON rq.FKPersonId = prs.PersonId ");
                    sql.Append("LEFT JOIN DocumentType dc ON rq.FKDocumentTypeNaturalPerson = dc.DocumentTypeId ");
                    sql.Append("LEFT JOIN DocumentType dc1 ON rq.FKDocumentTypeLegalPerson = dc1.DocumentTypeId ");
                    sql.Append("WHERE rq.RequestId = " + rqId + " AND rq.FKTransactionStatusId <> 3 ");

                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("Request", sql.ToString()));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, "");
                    dynamic ar = JArray.Parse(res)[0];

                    if (ar.Id == "Request")
                        return ar.Obj;
                    else return null;
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::GetRequestByNumberTicket", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return null;
                }
            }

            public static JArray GetRequestByRejection(int rqId)
            {
                try
                {
                    StringBuilder sql = new StringBuilder();
                    sql.Append("SELECT pr.Description as PRODUCTO, pt.Description AS TIPOPERSONA, dc.Description as TIPODOCUMENTO, ");
                    sql.Append("rq.Names AS NOMBRES, rq.LastNames AS APELLIDOS, rq.DocumentNumberNaturalPerson AS DOCUMENTONATURAL, ");
                    sql.Append("rq.NameLegalPerson AS NOMBREJURIDICA, rq.DocumentNumberLegalPerson AS DOCUMENTOJURIDICA, FKOfficeId ");
                    sql.Append("FROM Request rq ");
                    sql.Append("INNER JOIN Products pr ON rq.FKProductId = pr.ProductId ");
                    sql.Append("INNER JOIN Persons prs ON rq.FKPersonId = prs.PersonId ");
                    sql.Append("INNER JOIN PersonType pt ON RQ.FKPersonTypeId = PT.PersonTypeId ");
                    sql.Append("LEFT JOIN DocumentType dc ON rq.FKDocumentTypeNaturalPerson = dc.DocumentTypeId ");
                    sql.Append("LEFT JOIN DocumentType dc1 ON rq.FKDocumentTypeLegalPerson = dc1.DocumentTypeId ");
                    sql.Append("WHERE rq.RequestId = " + rqId + " AND rq.FKTransactionStatusId = 3 ");

                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("Request", sql.ToString()));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, "");
                    dynamic ar = JArray.Parse(res)[0];

                    if (ar.Id == "Request")
                        return ar.Obj;
                    else return null;
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::GetRequestByNumberTicket", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return null;
                }
            }

            public static JArray GetRequestByReview(int rqId)
            {
                try
                {
                    StringBuilder sql = new StringBuilder();
                    sql.Append("SELECT pr.Description as PRODUCTO, pt.Description AS TIPOPERSONA, dc.Description as TIPODOCUMENTO, ");
                    sql.Append("rq.Names AS NOMBRES, rq.LastNames AS APELLIDOS, rq.DocumentNumberNaturalPerson AS DOCUMENTONATURAL, ");
                    sql.Append("rq.NameLegalPerson AS NOMBREJURIDICA, rq.DocumentNumberLegalPerson AS DOCUMENTOJURIDICA, FKOfficeId ");
                    sql.Append("FROM Request rq ");
                    sql.Append("INNER JOIN Products pr ON rq.FKProductId = pr.ProductId ");
                    sql.Append("INNER JOIN Persons prs ON rq.FKPersonId = prs.PersonId ");
                    sql.Append("INNER JOIN PersonType pt ON RQ.FKPersonTypeId = PT.PersonTypeId ");
                    sql.Append("LEFT JOIN DocumentType dc ON rq.FKDocumentTypeNaturalPerson = dc.DocumentTypeId ");
                    sql.Append("LEFT JOIN DocumentType dc1 ON rq.FKDocumentTypeLegalPerson = dc1.DocumentTypeId ");
                    sql.Append("WHERE rq.RequestId = " + rqId + " AND rq.FKTransactionStatusId = 4 ");

                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("Request", sql.ToString()));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, "");
                    dynamic ar = JArray.Parse(res)[0];

                    if (ar.Id == "Request")
                        return ar.Obj;
                    else return null;
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::GetRequestByNumberTicket", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return null;
                }
            }

            public static JArray GetRequestByIdDocument(string documentNumber)
            {
                try
                {
                    string sql = "SELECT TOP(1) RequestId, DocumentNumberNaturalPerson FROM Request WHERE DocumentNumberNaturalPerson = '" + documentNumber + "' ORDER BY CreationDate Desc ";

                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("Request", sql.ToString()));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, "");
                    dynamic ar = JArray.Parse(res)[0];

                    if (ar.Id == "Request")
                        return ar.Obj;
                    else return null;
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::GetRequestById", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return null;
                }
            }

            public static JArray GetLastPhoto(int personId)
            {
                try
                {
                    string sql = string.Format("SELECT TOP(1) prs.Image FROM PersonPictures prs where prs.FKPersonId = {0} ORDER BY CreationDate DESC", personId);
                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("UltFoto", sql));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, "");
                    dynamic ar = JArray.Parse(res)[0];
                    if (ar.Id == "UltFoto")
                        return ar.Obj;
                    return null;
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::GetLastPhoto", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return null;
                }
            }

            public static JArray GetLastPhotoRepresentative(string numeroDocumento)
            {
                try
                {
                    StringBuilder sql = new StringBuilder();
                    sql.Append("SELECT TOP(1) pps.Image ");
                    sql.Append("FROM PersonPictures pps ");
                    sql.Append("INNER JOIN Enrolments er ON pps.FKPersonId = er.FKPersonId ");
                    sql.AppendFormat("WHERE er.DocumentNumber = '{0}' ORDER BY pps.CreationDate DESC", numeroDocumento);

                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("UltFoto", sql.ToString()));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, "");
                    dynamic ar = JArray.Parse(res)[0];
                    if (ar.Id == "UltFoto")
                        return ar.Obj;
                    return null;
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::GetLastPhoto", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return null;
                }
            }

            public static JArray GetLastSignature(int personId)
            {
                try
                {
                    StringBuilder sql = new StringBuilder();
                    sql.Append("SELECT prs.Imagen FROM Persons pr ");
                    sql.Append("INNER JOIN Enrolments er ON pr.PersonId = er.FKPersonId ");
                    sql.Append("INNER JOIN PersonSignatures prs ON er.EnrolmentId = prs.FKEnrolmentId ");
                    sql.Append("WHERE pr.PersonId = " + personId + " ");

                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("UltSignature", sql.ToString()));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, "");
                    dynamic ar = JArray.Parse(res)[0];
                    if (ar.Id == "UltSignature")
                        return ar.Obj;
                    return null;
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::GetLastPhoto", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return null;
                }
            }

            public static JArray GetAdjuntoByRequestId(int RequestId)
            {
                try
                {
                    string sql = string.Format("SELECT Name as NombreDocumento, Folder, Type, Size, CreationDate, TipoPersona, FKDocumentType, FKRequestId FROM RequestAttach where FKRequestId = {0}", RequestId);
                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("UltFoto", sql));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, "");
                    dynamic ar = JArray.Parse(res)[0];
                    if (ar.Id == "UltFoto")
                        return ar.Obj;
                    return null;
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::GetAdjuntoByRequestId", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return null;
                }
            }

            public static JArray GetARepresentativeRequestByRequestId(int RequestId)
            {
                try
                {
                    StringBuilder sql = new StringBuilder();
                    sql.Append("SELECT er.DocumentNumber, Concat(rp.Names, ' ', rp.LastNames)as Nombres, rp.Nacionality, er.FKPersonId ");
                    sql.Append("FROM RepresentativeRequest rp ");
                    sql.Append("INNER JOIN Enrolments er ON rp.DocumentNumber = er.DocumentNumber ");
                    sql.Append(string.Format("WHERE RP.FKRequestId = {0}", RequestId));

                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("Representative", sql.ToString()));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, "");
                    dynamic ar = JArray.Parse(res)[0];
                    if (ar.Id == "Representative")
                        return ar.Obj;
                    return null;
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::GetLastPhoto", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return null;
                }
            }

            public static JArray GetRepresentativeRequestById(int rqId)
            {
                try
                {
                    StringBuilder sql = new StringBuilder();
                    sql.Append("SELECT er.DocumentNumber as IDENTIFICACION, rp.Names as NOMBRES, rp.LastNames as APELLIDOS, rp.Nacionality as NACIONALIDAD, er.FKPersonId, rp.FKRequestId, rp.CreationDate, rp.FKCreateUserId ");
                    sql.Append("FROM RepresentativeRequest rp ");
                    sql.Append("INNER JOIN Enrolments er ON rp.DocumentNumber = er.DocumentNumber ");
                    sql.Append(string.Format("WHERE rp.FKRequestId = {0}", rqId));

                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("Representative", sql.ToString()));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, "");
                    dynamic ar = JArray.Parse(res)[0];
                    if (ar.Id == "Representative") return ar.Obj;
                    return null;
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::GetLastPhoto", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return null;
                }
            }

            public static JArray GetRepresentativeRequestByDocument(string documento, int rqId = 0)
            {
                try
                {
                    StringBuilder sql = new StringBuilder();
                    sql.Append("SELECT DocumentNumber as IDENTIFICACION, FKRequestId ");
                    sql.Append("FROM RepresentativeRequest ");
                    sql.Append(string.Format("WHERE DocumentNumber = '{0}' And FKRequestId = {1} ", documento, rqId));

                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("Representative", sql.ToString()));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, "");
                    dynamic ar = JArray.Parse(res)[0];
                    if (ar.Id == "Representative") return ar.Obj;
                    return null;
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::GetLastPhoto", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return null;
                }
            }

            public static JArray GetCustomer()
            {
                try
                {
                    string sql = "SELECT  *FROM Customers";
                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("Customers", sql));
                    post.Add(ob1);
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, "");
                    dynamic ar = JArray.Parse(res)[0];
                    if (ar.Id == "Customers")
                        return ar.Obj;
                    return null;
                }
                catch (Exception ex)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "GenesysOneValidBase::GetCustomer", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return null;
                }
            }

             public class certOf
            {
                public string code { get; set; }
            }

            public class AgentAut
            {
                public string code { get; set; }
            }

            public class raOperator
            {
                public string code { get; set; }
            }

            public class Product
            {
                public string sku { set; get; }
            }

            public class ApplicantDataGlobal
            {
                public string name { get; set; }
                public string documentType { get; set; }
                public string documentNumber { get; set; }
                public string email { get; set; }
                public string phoneDDD { get; set; }
                public string phoneNumber { get; set; }

            }

            public class Client
            {
                public string email { get; set; }
                public string emailConfirmation { get; set; }
                public string zipCode { get; set; }
                public string country { get; set; }
                public string state { get; set; }
                public string city { get; set; }
                public string address { get; set; }
                public string addressNumber { get; set; }
                public string addressAdditionalInfo { get; set; }
                public string neighborhood { get; set; }
                public string cellPhoneDDD { get; set; }
                public string cellPhoneNumber { get; set; }
                public string phoneDDD { get; set; }
                public string phoneNumber { get; set; }
                public JArray requestClientInfo { get; set; }
                public ClientPj requestClientPj { get; set; }
            }

            public class ClientInfo
            {
                public string name { get; set; }
                public string document { get; set; }
                public string documentType { get; set; }
                public string email { get; set; }
                public string profession { get; set; }
                public string institutionAwardedDegress { get; set; }
                public string professionalCardNumber { get; set; }
            }

            public class ClientPj
            {
                public string name { get; set; }
                public string document { get; set; }
                public string documentType { get; set; }
            }

            public class DossiePrincipal
            {
                public JArray dossie { get; set; }
            }

            public class Dossie
            {
                public string dateCreate { get; set; }
                public string description { set; get; }
                public string docto { get; set; }
                public string hash { get; set; }
            }

            public static string EncodeJson(string value)
            {
                SHA1 sha1 = new SHA1CryptoServiceProvider();
                byte[] inputBytes = new UnicodeEncoding().GetBytes(value);
                byte[] hash = sha1.ComputeHash(inputBytes);
                return Convert.ToBase64String(hash);

            }

            public static string EncodeJsonString(string value)
            {
                StringBuilder sb = new StringBuilder();
                foreach (byte b in GetHash(value))
                    sb.Append(b.ToString("x2"));

                return sb.ToString();
            }

            public static byte[] GetHash(string inputString)
            {
                HashAlgorithm algorithm = SHA1.Create();
                return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
            }

            public static string EncodeJsonBytes(byte[] value)
            {
                StringBuilder sb = new StringBuilder();
                foreach (byte b in GetHashBytes(value))
                    sb.Append(b.ToString("x2"));

                return sb.ToString();
            }

            public static byte[] GetHashBytes(byte[] inputString)
            {
                HashAlgorithm algorithm = SHA1.Create();
                return algorithm.ComputeHash(inputString);
            }

            public static string GeneratePasswordRequest(int LongPassMin, int LongPassMax)
            {
                char[] ValueAfanumeric = { 'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'z', 'x', 'c', 'v', 'b', 'n', 'm', 'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', 'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'Z', 'X', 'C', 'V', 'B', 'N', 'M', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
                Random ram = new Random();
                int LongitudPass = ram.Next(LongPassMin, LongPassMax);
                string Password = string.Empty;

                for (int i = 0; i < LongitudPass; i++)
                {
                    int rm = ram.Next(0, 2);

                    if (rm == 0) Password += ram.Next(0, 10);
                    else Password += ValueAfanumeric[ram.Next(0, 59)];
                }
                return Password;
            }

            public static void SetGridRequest(GridControl gridReques, GridView visorRequest)
            {
                visorRequest.Columns[0].Caption = "TICKET";
                visorRequest.Columns[0].AppearanceCell.TextOptions.HAlignment = HorzAlignment.Near;
                visorRequest.Columns[0].AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center;
                visorRequest.Columns[0].AppearanceHeader.BackColor = Color.FromArgb(55, 71, 79);

                visorRequest.Columns[1].Caption = "CERTIFICADO";
                visorRequest.Columns[1].AppearanceCell.TextOptions.HAlignment = HorzAlignment.Near;
                visorRequest.Columns[1].AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center;
                visorRequest.Columns[1].AppearanceHeader.ForeColor = Color.FromArgb(55, 71, 79);

                visorRequest.Columns[2].Caption = "NOMBRES";
                visorRequest.Columns[2].AppearanceCell.TextOptions.HAlignment = HorzAlignment.Center;
                visorRequest.Columns[2].AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center;
                visorRequest.Columns[2].AppearanceHeader.ForeColor = Color.FromArgb(55, 71, 79);

                visorRequest.Columns[3].Caption = "APELLIDOS";
                visorRequest.Columns[3].AppearanceCell.TextOptions.HAlignment = HorzAlignment.Near;
                visorRequest.Columns[3].AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center;
                visorRequest.Columns[3].AppearanceHeader.ForeColor = Color.FromArgb(55, 71, 79);

                visorRequest.Columns[4].Caption = "SEGUIMIENTO";
                visorRequest.Columns[4].AppearanceCell.TextOptions.HAlignment = HorzAlignment.Near;
                visorRequest.Columns[4].AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center;
                visorRequest.Columns[4].AppearanceHeader.ForeColor = Color.FromArgb(55, 71, 79);

                visorRequest.Columns[5].Caption = "PAGO REALIZADO";
                visorRequest.Columns[5].AppearanceCell.TextOptions.HAlignment = HorzAlignment.Near;
                visorRequest.Columns[5].AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center;
                visorRequest.Columns[5].AppearanceHeader.ForeColor = Color.FromArgb(55, 71, 79);
            }

            public static string VerificarCaracteresEspeciales(string textoOrigen)
            {
                if (textoOrigen.Length > 0)
                {
                    textoOrigen.ToUpper();
                    char[] car = textoOrigen.ToCharArray();
                    foreach (char c in car)
                    {
                        if (c.ToString() == "Â" | c.ToString() == "À" | c.ToString() == "Á" | c.ToString() == "Ä" | c.ToString() == "Ã")
                            textoOrigen = textoOrigen.Replace(c.ToString(), "A");
                        else if (c.ToString() == "Ê" | c.ToString() == "È" | c.ToString() == "É" | c.ToString() == "Ë")
                            textoOrigen = textoOrigen.Replace(c.ToString(), "E");
                        else if (c.ToString() == "Î" | c.ToString() == "Í" | c.ToString() == "Ì" | c.ToString() == "Ï")
                            textoOrigen = textoOrigen.Replace(c.ToString(), "I");
                        else if (c.ToString() == "Ô" | c.ToString() == "Õ" | c.ToString() == "Ò" | c.ToString() == "Ó" | c.ToString() == "Ö")
                            textoOrigen = textoOrigen.Replace(c.ToString(), "O");
                        else if (c.ToString() == "Û" | c.ToString() == "Ù" | c.ToString() == "Ú" | c.ToString() == "Ü")
                            textoOrigen = textoOrigen.Replace(c.ToString(), "U");
                        else if (c.ToString() == "Ý" | c.ToString() == "Ÿ") textoOrigen = textoOrigen.Replace(c.ToString(), "Y");
                        else if (c.ToString() == "Ç") textoOrigen = textoOrigen.Replace(c.ToString(), "C");
                        else if (c.ToString() == "Ñ") textoOrigen = textoOrigen.Replace(c.ToString(), "N");
                        else if (c.ToString() == "<>" | c.ToString() == "\\" | c.ToString() == "|" | c.ToString() == "/" | c.ToString() == ".")
                            textoOrigen = textoOrigen.Replace(c.ToString(), "");
                    }
                }
                return textoOrigen;
            }
          
            public static bool VerificarConexionAlservidor()
            {
                //***Direccion IP servidor de BD***
                bool exito;
                IPAddress ip = IPAddress.Parse("192.168.1.128");
                Ping ping = new Ping();
                PingReply pr = null;
                for (int i = 0; i < 4; i++)
                {
                    pr = ping.Send(ip);
                    //pr.Address, pr.Buffer.Length, pr.RoundtripTime, pr.Status.ToString();
                }
                if (pr.Status.ToString() == "Success") exito = true;
                else exito = false;

                return exito;
            }


            //***CLASES***
           public class NewPerson
            {
                public class PersonsRoot { public JArray PersonsName { get; set; } }
                public class persons
                {
                    public int PersonId { set; get; }
                    public string FirstName { get; set; }
                    public string SecondName { get; set; }
                    public string FirstLastName { get; set; }
                    public string SecondLastName { get; set; }
                    public DateTime DateOfBirth { get; set; }
                    public DateTime CreationDate { get; set; }
                    public DateTime ModifyDate { set; get; }
                    public string PersonalMovilPhone { set; get; }
                    public string HomePhone { set; get; }
                    public string Address { set; get; }
                    public string WorkPhone { set; get; }
                    public string WorkAddress { set; get; }
                    public string PersonalEmail { set; get; }
                    public string WorkEmail { set; get; }
                    public int FKSexId { set; get; }
                    public int FKCivilStatusId { set; get; }
                    public int FKPersonProfessionsId { set; get; }
                    public int FKEducationLevelId { set; get; }
                    public int FKStateCityId { set; get; }
                    public int FKStateCountryId { set; get; }
                    public int FKNeighborhoodId { set; get; }
                    public int FKNationalityId { set; get; }
                    public int FKCatalogStatusId { set; get; }
                    public int FKCountryId { set; get; }
                    public int FKCountryResidenceId { set; get; }
                    public int FKCreateUserId { set; get; }
                }

                public class EnrolmentsRoot { public JArray EnrolmentsName { get; set; } }
                public class enrolments
                {
                    public int EnrolmentId { set; get; }
                    public string DocumentNumber { get; set; }
                    public int FPCaptured { set; get; }
                    public int Attachments { set; get; }
                    public bool RegisteredAfis { get; set; }
                    public DateTime CreationDate { get; set; }
                    public DateTime ModifyDate { set; get; }
                    public bool NoPresential { set; get; }
                    public int FKDocumentType { set; get; }
                    public int FKPersonId { set; get; }
                    public int FKWorkStationId { set; get; }
                    public int FKCreationUserId { set; get; }
                    public int FKOfficeId { set; get; }

                }

                public class PersonPicturesRoot { public JArray PersonPicturesName { get; set; } }
                public class personPictures
                {
                    public string Image { get; set; }
                    public DateTime CreationDate { get; set; }
                }

                public class PersonSignaturesRoot { public JArray PersonSignaturesName { get; set; } }
                public class personSignatures
                {
                    public string Image { get; set; }
                    public int FKCatalogStatusId { get; set; }
                    public DateTime CreationDate { get; set; }
                    public int FKCreateUserId { get; set; }
                }

                public class HuellasEnrolmentRoot { public JArray HuellasEnrolmentName { get; set; } }
                public class huellasEnrolment
                {
                    public int Dedo { set; get; }
                    public int Estado { set; get; }
                    public int Calidad { set; get; }
                    public string WSQ { set; get; }
                }
            }


        }
        #endregion

        #region CLASE PARA CONSULTA AL AFIS
        public class AxCls
        {
            private AXCLSLib.AxClsCtrl ax = null;

            //Estas variables son las mismas de la clase CS500e
            public int cs500eInicializado = -1;
            public int TotalDedos = 0;
            public bool nistGenerado = false;
            private bool SeCapturaHuella = true;
            private int captura = -1;
            //private int finalizado = 0;
            //---------------------------------------------------------

            string maskType = "1111111111111111";

            int n_TaskID;
            int n_TenPrintType = 1;
            int n_PlainType = 0;

            public enum CLS_FP_CAPTURE_FINGER
            {
                RIGHT_THUMB = 1,
                RIGHT_INDEX = 2,
                RIGHT_MIDDLE = 3,
                RIGHT_RING = 4,
                RIGHT_LITTLE = 5,
                LEFT_THUMB = 6,
                LEFT_INDEX = 7,
                LEFT_MIDDLE = 8,
                LEFT_RING = 9,
                LEFT_LITTLE = 10,
                PLAIN_RIGHT_THUMB = 11,
                PLAIN_LEFT_THUMB = 12,
                PLAIN_RIGHT_SLAP = 13,
                PLAIN_LEFT_SLAP = 14,
                PLAIN_TWO_THUMBS = 15,
                PLAIN_SEPARATE_THUMBS = 16,
                RIGHT_FULL_PALM = 21,
                RIGHT_WRITERS_PALM = 22,
                LEFT_FULL_PALM = 23,
                LEFT_WRITERS_PALM = 24,
                RIGHT_LOWER_PALM = 25,
                RIGHT_UPPER_PALM = 26,
                LEFT_LOWER_PALM = 27,
                LEFT_UPPER_PALM = 28
            }

            public AxCls()
            {
                ax = new AXCLSLib.AxClsCtrl();
            }
            /// <summary>
            /// Comprueba si el dispositivo está conectado
            /// </summary>
            public void ComprobarEscaner()
            {
                try
                {
                    cs500eInicializado = ax.ConnectStatus;

                    if (cs500eInicializado != 1)
                    {
                        ax.Init("");
                        cs500eInicializado = ax.ConnectStatus;
                    }
                    else
                    {
                        if (ax.ErrCode != 0)
                        {
                            ax.Init("");
                            cs500eInicializado = ax.ConnectStatus;
                        }
                    }
                }
                catch (Exception ex)
                {
                    cs500eInicializado = -1;
                }
            }

            private void KillProcessCs500e()
            {
                try
                {
                    var prs = from item in System.Diagnostics.Process.GetProcesses()
                              where item.ProcessName.ToUpper() == "CLSCaptureDll2Server".ToUpper()
                              select item;

                    if (prs == null) return;

                    foreach (var item in prs.ToList())
                    {
                        item.Kill();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            public void PrepararCapturaHuellas()
            {
                if (ax.ConnectStatus == 0)
                    ax.Connect();

                cs500eInicializado = ax.ConnectStatus;
                ax.Clear();
            }
            public int capturarHuellas(string numeroTransaccion)
            {
                try
                {
                    ax.TransNo = numeroTransaccion;

                    n_TenPrintType = 0;
                    n_PlainType = 0;

                    if (n_TenPrintType == 1)
                    {
                        n_TaskID = 89;
                    }
                    else
                    {
                        n_TaskID = 26;
                    }

                    ax.CaptFP(26, 0, 0, maskType, 1, 1); // No genera bkp
                    return ax.ErrCode;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            public int CapturarHuellasDerecha(string numeroTransaccion)
            {
                ax.TransNo = numeroTransaccion;
                ax.CaptFP(71, 0, 0, maskType, 1, 1);
                captura = ax.ErrCode;
                return ax.ErrCode;

            }
            public int capturarHuellasIzquierda(string numeroTransaccion)
            {
                ax.TransNo = numeroTransaccion;
                ax.CaptFP(72, 0, 0, maskType, 1, 1);
                captura = ax.ErrCode;
                return ax.ErrCode;
            }
            public int capturarHuellasPulgares(string numeroTransaccion)
            {
                ax.TransNo = numeroTransaccion;
                ax.CaptFP(99, 0, 0, maskType, 0, 1);
                captura = ax.ErrCode;
                return ax.ErrCode;
            }
            public bool validarHuellasCapturadas()
            {
                return (captura == 0);
            }
            public List<EN.EnrolmentTemplate> getHuellasCapturas(string numeroTransaccion, string path, string metodo)
            {
                List<EN.EnrolmentTemplate> fingers = new List<EN.EnrolmentTemplate>();
                try
                {
                    string dirWSQ = Path.Combine(path, numeroTransaccion + "\\" + metodo);
                    if (Directory.Exists(dirWSQ))
                    {
                        string[] archivoImagenes = Directory.GetFiles(dirWSQ);

                        int dedo = 0;
                        for (int i = 0; i < archivoImagenes.Length - 1; i++)
                        {
                            //CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_IMG_EXP estado = CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_IMG_EXP.NO_EXCEPTION;


                            int calidad = 0;

                            string fileWSQ = archivoImagenes[i + 1];

                            bool archivoWSQ = fileWSQ.EndsWith(".wsq");
                            if (File.Exists(fileWSQ) && archivoWSQ)
                            {
                                //CLSFPCaptureDllWrapper.CLS_GetImageException(dedos[dedo], ref estado);
                                //CLSFPCaptureDllWrapper.CLS_GetImageNFIQ(dedos[dedo], ref calidad, CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_IMPRESSION_TYPE.PLAIN);
                                string[] fileSplit = fileWSQ.Replace(dirWSQ, "").Replace(".wsq", "").ToString().Split('_');
                                if (fileSplit.Count() != 2)
                                    continue;

                                byte[] wsqBytes = File.ReadAllBytes(fileWSQ);

                                EN.EnrolmentTemplate fp = new EN.EnrolmentTemplate();
                                fp.index = Convert.ToInt32(fileSplit[1]);
                                fp.score = calidad;
                                fp.status = 1;
                                fp.wsq = Convert.ToBase64String(wsqBytes);
                                fingers.Add(fp);
                                dedo++;
                            }
                        }
                    }
                    else fingers = null;
                }
                catch (Exception ex)
                {
                    fingers = null;
                    throw ex;
                }
                return fingers;
            }
            public List<EN.EnrolmentTemplate> getHuellasCapturas(string path)
            {
                List<EN.EnrolmentTemplate> fingers = new List<EN.EnrolmentTemplate>();
                try
                {
                    string dirWSQ = path;
                    if (Directory.Exists(dirWSQ))
                    {
                        string[] archivoImagenes = Directory.GetFiles(dirWSQ);
                        CLS_FP_CAPTURE_FINGER[] dedos = {
                        CLS_FP_CAPTURE_FINGER.RIGHT_THUMB,
                        CLS_FP_CAPTURE_FINGER.RIGHT_INDEX,
                        CLS_FP_CAPTURE_FINGER.RIGHT_MIDDLE,
                        CLS_FP_CAPTURE_FINGER.RIGHT_RING,
                        CLS_FP_CAPTURE_FINGER.RIGHT_LITTLE,
                        CLS_FP_CAPTURE_FINGER.LEFT_THUMB,
                        CLS_FP_CAPTURE_FINGER.LEFT_INDEX,
                        CLS_FP_CAPTURE_FINGER.LEFT_MIDDLE,
                        CLS_FP_CAPTURE_FINGER.LEFT_RING,
                        CLS_FP_CAPTURE_FINGER.LEFT_LITTLE
                    };
                        int dedo = 0;
                        for (int i = 0; i < archivoImagenes.Length - 1; i++)
                        {
                            //CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_IMG_EXP estado = CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_IMG_EXP.NO_EXCEPTION;
                            int calidad = 0;
                            string fileWSQ = archivoImagenes[i + 1];
                            bool archivoWSQ = fileWSQ.EndsWith(".wsq");
                            if (File.Exists(fileWSQ) && archivoWSQ)
                            {
                                //CLSFPCaptureDllWrapper.CLS_GetImageException(dedos[dedo], ref estado);
                                //CLSFPCaptureDllWrapper.CLS_GetImageNFIQ(dedos[dedo], ref calidad, CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_IMPRESSION_TYPE.PLAIN);
                                byte[] wsqBytes = File.ReadAllBytes(fileWSQ);

                                EN.EnrolmentTemplate fp = new EN.EnrolmentTemplate();
                                fp.index = dedo + 1;
                                fp.score = calidad;
                                fp.status = 1;
                                fp.wsq = Convert.ToBase64String(wsqBytes);
                                fingers.Add(fp);
                                dedo++;
                            }
                        }
                    }
                    else fingers = null;
                }
                catch (Exception ex)
                {
                    fingers = null;
                    throw ex;
                }
                return fingers;
            }
            public void DesconectarEscaner()
            {
                if (cs500eInicializado == 1)
                {
                    ax.Term();
                    //comprobar que no exista el proceso del cs500e corriendo
                    KillProcessCs500e();
                }
            }
            public int SalvarHuellasCapturadas(string numeroTransaccion, string wsqPath, string bkpMano) //devolverá el numero de huellas capturadas
            {
                try
                {
                    if (!Directory.Exists(wsqPath))
                        Directory.CreateDirectory(wsqPath);

                    string filename = wsqPath + ax.TransNo + "_" + 1 + ".wsq";
                    ax.SaveWsqImage(ax.TransNo, wsqPath, filename, 2, 0, maskType);

                    if (bkpMano != "")
                    {
                        if (bkpMano == "2OFF") //Cuando venga de un offline hay que generar los bkp de cada mano
                        {
                            string bkpL = ax.TransNo + "L.bkp";
                            string bkpR = ax.TransNo + "R.bkp";
                            ax.SaveMntRightFlatCutBkp(ax.TransNo, wsqPath, bkpR);
                            ax.SaveMntLeftFlatCutBkp(ax.TransNo, wsqPath, bkpL);
                        }
                        else
                        {
                            //eliminar el wsq # 13 y 14 ya que no se requiere para el empaquetado ni para el bkp
                            if (bkpMano == "R" || bkpMano == "L")
                            {
                                string pathWSQ13 = Path.Combine(wsqPath, ax.TransNo + "_" + 13 + ".wsq");
                                if (File.Exists(pathWSQ13))
                                    File.Delete(pathWSQ13);

                                string pathWSQ14 = Path.Combine(wsqPath, ax.TransNo + "_" + 14 + ".wsq");
                                if (File.Exists(pathWSQ14))
                                    File.Delete(pathWSQ14);
                            }

                            string filename2 = ax.TransNo + ".bkp";
                            //ax.SaveFlatBkp(ax.TransNo, dirr, filename);
                            if (bkpMano == "R")
                                ax.SaveMntRightFlatCutBkp(ax.TransNo, wsqPath, filename2);
                            else
                                ax.SaveMntLeftFlatCutBkp(ax.TransNo, wsqPath, filename2);
                        }
                    }


                    int respSaveWSQ = ax.ErrCode;

                    if (respSaveWSQ == 0)
                    {
                        string[] archivoImagenes = Directory.GetFiles(wsqPath, "*.wsq");
                        int huellasCapturadas = archivoImagenes.Count();

                        return (huellasCapturadas > 0) ? huellasCapturadas : -1;
                    }
                    else return -1;

                }
                catch (Exception)
                {
                    return -1;
                }

            }

            public void getImagenesBmp(string pathwsq, string pathBmp)
            {
                List<EN.EnrolmentTemplate> fingers = new List<EN.EnrolmentTemplate>();
                try
                {
                    string dirWSQ = pathwsq;
                    if (Directory.Exists(dirWSQ))
                    {
                        string[] archivoImagenes = Directory.GetFiles(dirWSQ);
                        CLS_FP_CAPTURE_FINGER[] dedos = {
                        CLS_FP_CAPTURE_FINGER.RIGHT_THUMB,
                        CLS_FP_CAPTURE_FINGER.RIGHT_INDEX,
                        CLS_FP_CAPTURE_FINGER.RIGHT_MIDDLE,
                        CLS_FP_CAPTURE_FINGER.RIGHT_RING,
                        CLS_FP_CAPTURE_FINGER.RIGHT_LITTLE,
                        CLS_FP_CAPTURE_FINGER.LEFT_THUMB,
                        CLS_FP_CAPTURE_FINGER.LEFT_INDEX,
                        CLS_FP_CAPTURE_FINGER.LEFT_MIDDLE,
                        CLS_FP_CAPTURE_FINGER.LEFT_RING,
                        CLS_FP_CAPTURE_FINGER.LEFT_LITTLE
                    };
                        int dedo = 0;
                        for (int i = 0; i < archivoImagenes.Length - 1; i++)
                        {

                            string fileWSQ = archivoImagenes[i + 1];
                            int indice = i + 1;
                            string filebmp = pathBmp + "\\" + ax.TransNo + "_" + indice + ".bmp";
                            bool archivoWSQ = fileWSQ.EndsWith(".wsq");
                            if (File.Exists(fileWSQ) && archivoWSQ)
                            {
                                WSQaImagen(fileWSQ, filebmp);
                            }
                        }
                    }

                }
                catch (Exception ex)
                {

                    throw ex;
                }

            }
            public void WSQaImagen(string rutawsq, string rutabmp)
            {
                FileStream fs = File.OpenRead(rutawsq);
                byte[] fileData = new byte[fs.Length];
                fs.Read(fileData, 0, fileData.Length);

                WsqDecoder decoder = new WsqDecoder();
                Bitmap bmp = decoder.Decode(fileData);
                bmp.Save(rutabmp);
            }

        }
        #endregion


    }
}
