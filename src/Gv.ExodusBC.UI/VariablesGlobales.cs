﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gv.ExodusBc.EN;
using Gv.ExodusBc.LN;
using System.Windows.Forms;
using System.Xml;
using Gv.Seguridad;
using Gv.ServerUtility;
using System.IO;
using static Gv.ExodusBc.UI.ExodusBcBase;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Gv.ExodusBc.UI.WebReference;
using System.Net.NetworkInformation;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.Net;

namespace Gv.ExodusBc.UI
{
    public class VariablesGlobales
    {
        public static string WsValidacionesIP = "192.168.52.74";
        public static string TOKEN = "";
        public static string NameDB = "genesysonevalid.db";
        public static string AppVersion = "1.0.0.0";
        public static string PathDataDBInstall = System.IO.Path.Combine(Application.StartupPath, "Data", NameDB);
        public static string PathDataDB = @"C:\GenesysOne\DATA_DB\";
        public static string PathDataBackup = @"C:\GenesysOne\DATA_BACKUP\";
        public static string PathDataLog = @"C:\GenesysOne\DATA_LOG\GenesysOneValidApp\";
        public static string PathDataLocal = @"C:\GenesysOne\DATA_LOCAL\";
        public static string PathDataSystem = @"C:\GenesysOne\SYSTEM\";
        public static string PathWSQ = @"C:\GenesysOne\WSQ\";
        public static string PathWSQFID = @"C:\GenesysOne\WSQ\FID\";
        public static string PathHuellas = @"C:\GenesysOne\Huellas\";
        public static string PathPdf = @"C:\GenesysOne\SYSTEM\PDF\";
        public static long AdjuntoTamanioMaximo = 0;
        public static int AdjuntoCantidadMaximaPN = 0;
        public static int AdjuntoCantidadMaximaPJ = 0;
        public static string EscanerCPNombre = "";
        public static int EscanerCPDPI = 0;
        public static bool SeCapturaFirma = true; //para que por defecto sea requerida
        public static int ENVIOTRANSACCION = 0;
        public static string PathDataSystemTemp = @"C:\GenesysOne\SYSTEM\TEMP\";
        public static string MessageBoxDevExpressStyle = "Office 2016 Colorful";

        public static List<Settings> lstConfiguraciones = null;
        public static List<ProfessionalAssociations> lstColegios = null;
        public static List<DocumentType> lstDocumentoPN = null;
        public static List<DocumentType> lstDocumentoPJ = null;
        public static List<Products> lstProductos = null;
        public static int IDPRODUCTO = 0;

        public static int CANTMINUSUARIO = 0;
        public static int CONTRASENIAMIN = 0;
        public static int EDADMINIMA = 0;
        public static int EDADMAXIMA = 0;
      
        public static int DIASEXPIRACION = 0;
        public static DateTime FechaHora = DateTime.Now;
        public static string WY_UPDATE_IP = "";
        public static string WY_UPDATE_URL = "";
        public static string HOST_NAME = "";
        public static int CANTIDADMAXIMAINTENTOS = 0;
        public static long updateLiveTime = 0; //indicarlo en minutos

        //DATOS PARA HACER LA LLAMADA CON VALID
        public static string SKINCODE = string.Empty; // Identificación visual de TECNISIGN 
        public static string IDIOMA = string.Empty; //Valor fijo en el encabezado de archivo JSON
        public static string PEDIDO = string.Empty; //Valor fijo en el encabezado de archivo JSON
        public static int LONGITUDMINPASSRSQT = 0; //Longitud mininam pass request certificados
        public static int LONGITUDMAXPASSRSQT = 0;//Longitud maxima pass request certificados
        public static string CERTOFFICE = string.Empty;
        public static string RGAURORITY = string.Empty;
        public static string AGAUTORITY = string.Empty;
        public static int REGISTRATION_AGENT_ID = 0;
        public static int OFFICE_ID = 0;
        public static int OFFICE_CERTIFICATE_ID = 0;
        public static int OFFICE_REGISTRATIONAUTHORITY_ID = 0;
        public static string VIGENCIACERTIFICADOS = string.Empty;

        //DATOS PARA HACER LA LLAMADA CON GP
        public static string OFFICEIDGP = string.Empty;
        public static string OFFICECODEGP = string.Empty;
        public static string OFFICEDESCRIPTIONGP = string.Empty;


        //
        public static AxCls cs500e = null;
        public static ExodusBcService ws = new ExodusBcService();

        //SERVIDOR DE BBDD
        public static string ServerDominio = string.Empty;
        public static string ServerIp = string.Empty;
        public static string ServerUser = string.Empty;
        public static string ServerPassword = string.Empty;
        public static string PathWSQ2 = string.Empty;
        public static RemoteServer ServerRemote { get; set; }

        public static string OFFICENAME = "";
        public static string MODOTRABAJO = "";
        public static bool LocalTimeSynchronized;
        public static string NOMBRECAMARA = "";
        public static int USER_ID = 0;
        public static string USER_NAME = "";
        public static string USER_PASSWORD = "";
        public static long USER_ROL_ID = 0;
        public static int sessionActiveId = 0;
        public static DateTime sessionLastTime = DateTime.MinValue;
        public static string OFFICE_CODE = "";
        public static string OFFICEZIPCODE = string.Empty;
        public static long WS_ID = 0;
        public static int AUTO_LOGOUT_TIME = 0;
        //Inactividad del sistema
        public static bool IsLocked;
        public static string CodigoMaquina = "";
        public static bool EscanerInicializado = false;
        public static bool SeCapturaHuella = true; //para que por defecto sea requerida
        public static int CS500eInicializado = -1; //0= OK

        //Permisos del usuario
        public static bool PERMISO_Enrolamiento;
        public static bool PERMISO_Certificados;
        public static bool PERMISO_Aprobacion;
        public static bool PERMISO_Solicitudes;
        public static bool PERMISO_EstadoSolicitudes;
        public static bool PERMISO_Emisiones;
        public static bool PERMISO_Revocaciones;
        public static bool PERMISO_VerOficinasAprobacion;
        public static bool PERMISO_VerOficinasSeguimiento;
        public static bool PERMISO_ContratoFirmado;

        public static long DOC_TYPE_DEFAULT = Convert.ToInt64(1);
        public static long COUNTRY_DEFAULT = Convert.ToInt64(102);
        public static long NATIONALITY_DEFAULT = Convert.ToInt64(102);
        public static long COUNTRYRESIDENCE_DEFAULT = Convert.ToInt64(102);

        public static string RutaDB = string.Empty;


        public static DateTime UltimaSincronizacion { get; set; }

        public static void Inicializar()
        {
            try
            {
                //iniciar scaner
                //cs500e = new AxCls();

                AppVersion = Helper.GetVersion();
                RutaDB = ConfigurationManager.AppSettings["RutaDB"];
                string password = "G3n3$1$0n3";
                ConfiguracionLN.GetConnecionString($"data source={RutaDB};password={password}");

                var tblSystemConfig = LN.SETTINGSLN.GetSettings();
                lstColegios = LN.PROFESSIONALASSOCIATIONSLN.GetAll();
                lstDocumentoPN = LN.DOCUMENTTYPELN.GetDocumentByTypeDocumentId();
                lstDocumentoPJ = LN.DOCUMENTTYPELN.GetDocumentById();

                if (DateTime.TryParseExact(LN.SETTINGSLN.GetValue("ULTIMASINCRONIZACION"), "dd-MM-yyyy HH:mm:ss", null, DateTimeStyles.None, out DateTime ultimaSincronizacion))
                    UltimaSincronizacion = ultimaSincronizacion;
                else UltimaSincronizacion = DateTime.Now;


                //Crear Directorios
                if (!Directory.Exists(PathDataLog)) Directory.CreateDirectory(PathDataLog);
                if (!Directory.Exists(PathHuellas)) Directory.CreateDirectory(PathHuellas);
                if (!Directory.Exists(PathWSQ)) Directory.CreateDirectory(PathWSQ);
                if (!Directory.Exists(PathWSQFID)) Directory.CreateDirectory(PathWSQFID);
                if (!Directory.Exists(PathDataLocal)) Directory.CreateDirectory(PathDataLocal);

                var tblWebServiceToken = WEBSERVICETOKENSLN.GetWebServiceTokenbyConsumidor("APP-CLIENTE");
                if (tblSystemConfig != null)
                {
                    lstConfiguraciones = new List<Settings>();
                    foreach (var item in tblSystemConfig)
                    lstConfiguraciones.Add(item);
                    if (lstConfiguraciones != null)
                    {
                        if (lstConfiguraciones.Where(x => x.Name.Trim().ToUpper() == "NOMBREUSUARIO-MIN").FirstOrDefault().Value != null)
                            CANTMINUSUARIO = Convert.ToInt32(lstConfiguraciones.Where(x => x.Name.Trim().ToUpper() == "NOMBREUSUARIO-MIN").FirstOrDefault().Value);

                        if (lstConfiguraciones.Where(x => x.Name.Trim().ToUpper() == "CONTRASENIA-MIN").FirstOrDefault().Value != null)
                            CONTRASENIAMIN = Convert.ToInt32(lstConfiguraciones.Where(x => x.Name.Trim().ToUpper() == "CONTRASENIA-MIN").FirstOrDefault().Value);

                        if (lstConfiguraciones.Where(x => x.Name.Trim().ToUpper() == "EDAD-MINIMA").FirstOrDefault().Value != null)
                            EDADMINIMA = Convert.ToInt32(lstConfiguraciones.Where(x => x.Name.Trim().ToUpper() == "EDAD-MINIMA").FirstOrDefault().Value);

                        if (lstConfiguraciones.Where(x => x.Name.Trim().ToUpper() == "EDAD-MAXIMA").FirstOrDefault().Value != null)
                            EDADMAXIMA = Convert.ToInt32(lstConfiguraciones.Where(x => x.Name.Trim().ToUpper() == "EDAD-MAXIMA").FirstOrDefault().Value);

                        if (lstConfiguraciones.Where(x => x.Name.Trim().ToUpper() == "DIAS-EXPIRACION-CONTRASENIA").FirstOrDefault().Value != null)
                            DIASEXPIRACION = Convert.ToInt32(lstConfiguraciones.Where(x => x.Name.Trim().ToUpper() == "DIAS-EXPIRACION-CONTRASENIA").FirstOrDefault().Value);

                        if (lstConfiguraciones.Where(x => x.Name.Trim().ToUpper() == "CANTIDAD-MAX-INTENTOS").FirstOrDefault().Value != null)
                            CANTIDADMAXIMAINTENTOS = Convert.ToInt32(lstConfiguraciones.Where(x => x.Name.Trim().ToUpper() == "CANTIDAD-MAX-INTENTOS").FirstOrDefault().Value);

                        if (lstConfiguraciones.Where(x => x.Name.Trim().ToUpper() == "UPDATELIVETIME").FirstOrDefault().Value != null)
                            updateLiveTime = Convert.ToInt32(lstConfiguraciones.Where(x => x.Name.Trim().ToUpper() == "UPDATELIVETIME").FirstOrDefault().Value);

                        if (lstConfiguraciones.Where(x => x.Name.Trim().ToUpper() == "ADJUNTO-CANTIDAD-MAX-PN").FirstOrDefault().Value != null)
                            AdjuntoCantidadMaximaPN = Convert.ToInt32(lstConfiguraciones.Where(x => x.Name.Trim().ToUpper() == "ADJUNTO-CANTIDAD-MAX-PN").FirstOrDefault().Value);

                        if (lstConfiguraciones.Where(x => x.Name.Trim().ToUpper() == "ADJUNTO-CANTIDAD-MAX-PJ").FirstOrDefault().Value != null)
                            AdjuntoCantidadMaximaPJ = Convert.ToInt32(lstConfiguraciones.Where(x => x.Name.Trim().ToUpper() == "ADJUNTO-CANTIDAD-MAX-PJ").FirstOrDefault().Value);

                        if (lstConfiguraciones.Where(x => x.Name.Trim().ToUpper() == "ADJUNTO-TAMANIO-MAX").FirstOrDefault().Value != null)
                            AdjuntoTamanioMaximo = Convert.ToInt32(lstConfiguraciones.Where(x => x.Name.Trim().ToUpper() == "ADJUNTO-TAMANIO-MAX").FirstOrDefault().Value);

                        if (lstConfiguraciones.Where(x => x.Name.Trim().ToUpper() == "SKINCODE").FirstOrDefault().Value != null)
                            SKINCODE = Convert.ToString(lstConfiguraciones.Where(x => x.Name.Trim().ToUpper() == "SKINCODE").FirstOrDefault().Value);

                        if (lstConfiguraciones.Where(x => x.Name.Trim().ToUpper() == "IDIOMA").FirstOrDefault().Value != null)
                            IDIOMA = Convert.ToString(lstConfiguraciones.Where(x => x.Name.Trim().ToUpper() == "IDIOMA").FirstOrDefault().Value);

                        if (lstConfiguraciones.Where(x => x.Name.Trim().ToUpper() == "PEDIDO").FirstOrDefault().Value != null)
                            PEDIDO = Convert.ToString(lstConfiguraciones.Where(x => x.Name.Trim().ToUpper() == "PEDIDO").FirstOrDefault().Value);

                        if (lstConfiguraciones.Where(x => x.Name.Trim().ToUpper() == "VIGENCIA-CERTIFICADOS").FirstOrDefault().Value != null)
                            VIGENCIACERTIFICADOS = Convert.ToString(lstConfiguraciones.Where(x => x.Name.Trim().ToUpper() == "VIGENCIA-CERTIFICADOS").FirstOrDefault().Value);

                        if (lstConfiguraciones.Where(x => x.Name.Trim().ToUpper() == "LONGITUD-MIN-PASSREQUEST").FirstOrDefault().Value != null)
                            LONGITUDMINPASSRSQT = Convert.ToInt32(lstConfiguraciones.Where(x => x.Name.Trim().ToUpper() == "LONGITUD-MIN-PASSREQUEST").FirstOrDefault().Value);

                        if (lstConfiguraciones.Where(x => x.Name.Trim().ToUpper() == "LONGITUD-MAX-PASSREQUEST").FirstOrDefault().Value != null)
                            LONGITUDMAXPASSRSQT = Convert.ToInt32(lstConfiguraciones.Where(x => x.Name.Trim().ToUpper() == "LONGITUD-MAX-PASSREQUEST").FirstOrDefault().Value);

                        if (lstConfiguraciones.Where(x => x.Name.Trim().ToUpper() == "ESCANER-CP-NOMBRE").FirstOrDefault().Value != null)
                            EscanerCPNombre = Convert.ToString(lstConfiguraciones.Where(x => x.Name.Trim().ToUpper() == "ESCANER-CP-NOMBRE").FirstOrDefault().Value);

                        if (lstConfiguraciones.Where(x => x.Name.Trim().ToUpper() == "ESCANER-CP-DPI").FirstOrDefault().Value != null)
                            EscanerCPDPI = Convert.ToInt32(lstConfiguraciones.Where(x => x.Name.Trim().ToUpper() == "ESCANER-CP-DPI").FirstOrDefault().Value);

                        if (lstConfiguraciones.Where(x => x.Name.Trim().ToUpper() == "ESCANER-CP-DPI").FirstOrDefault().Value != null)
                            EscanerCPDPI = Convert.ToInt32(lstConfiguraciones.Where(x => x.Name.Trim().ToUpper() == "ESCANER-CP-DPI").FirstOrDefault().Value);

                        var capturarFirma = Convert.ToInt32(lstConfiguraciones.Where(x => x.Name.Trim().ToUpper() == "CAPTURAR-FIRMA").FirstOrDefault().Value);
                        if (capturarFirma == 0) SeCapturaFirma = false;

                        ENVIOTRANSACCION = Convert.ToInt32(lstConfiguraciones.Where(x => x.Name.Trim().ToUpper() == "ENVIOTRANSACCION").FirstOrDefault().Value);

                        NOMBRECAMARA = lstConfiguraciones.Where(x => x.Name.Trim().ToUpper().Contains("NOMBRE-CAMARA")).FirstOrDefault().Value;

                    }
                }
                if (tblWebServiceToken != null)  TOKEN = tblWebServiceToken.Token;

            }
            catch (Exception ex)
            {
                Utilidades.Log.InsertarLog(Utilidades.Log.ErrorType.Error, "VariablesGlobales::Inicializar", ex.StackTrace, PathDataLog);
            }

        }

        public static void CargarPermisosUsuarioEnLinea(int rolUsuario)
        {
            //Por default todos los permisos estaran como falsos
            PERMISO_Enrolamiento = false;
            PERMISO_Certificados = false;
            PERMISO_Aprobacion = false;
            PERMISO_Solicitudes = false;
            PERMISO_Emisiones = false;
            PERMISO_Revocaciones = false;
            PERMISO_EstadoSolicitudes = false;
            PERMISO_VerOficinasAprobacion = false;
            PERMISO_VerOficinasSeguimiento = false;
            PERMISO_ContratoFirmado = false;

            //Permisos
            var lstPermisos = USERSPERMISSIONSLN.GetPermisosUsers(rolUsuario);
            if (lstPermisos != null)
            {
                if (lstPermisos.Count > 0)
                {
                    foreach (var item in lstPermisos)
                    {
                        switch (item.FKModuleOptionsId)
                        {
                            case 1: PERMISO_Enrolamiento = true; break;
                            case 2: PERMISO_Certificados = true; break;
                            case 3: PERMISO_Aprobacion = true; break;
                            case 4: PERMISO_Solicitudes = true; break;
                            case 5: PERMISO_Emisiones = true; break;
                            case 6: PERMISO_Revocaciones = true; break;
                            case 7: PERMISO_EstadoSolicitudes = true; break;
                            case 8: PERMISO_VerOficinasAprobacion = true; break;
                            case 9: PERMISO_VerOficinasSeguimiento = true; break;
                            case 15: PERMISO_ContratoFirmado = true; break;
                        }
                    }
                }
            }
        }

        public static void CargarPermisosUsuarioFueraDeLinea(int rolUsuario)
        {
            //Por default todos los permisos estaran como falsos
            PERMISO_Enrolamiento = false;
            PERMISO_Certificados = false;
            PERMISO_Aprobacion = false;
            PERMISO_Solicitudes = false;
            PERMISO_Emisiones = false;
            PERMISO_Revocaciones = false;
            PERMISO_EstadoSolicitudes = false;
            PERMISO_ContratoFirmado = false;
            //Permisos
            var lstPermisos = USERSPERMISSIONSLN.GetPermisosUsers(rolUsuario);
            if (lstPermisos != null)
            {
                if (lstPermisos.Count > 0)
                {
                    foreach (var item in lstPermisos)
                    {
                        switch (item.FKModuleOptionsId)
                        {
                            case 1: PERMISO_Enrolamiento = true; break;
                        }
                    }
                }
            }
        }

        public static async Task<bool> SincronizarCatalogos()
        {
            var sw = new Stopwatch();
            RutaDB = ConfigurationManager.AppSettings["RutaDB"];
            string password = "G3n3$1$0n3";
            ConfiguracionLN.GetConnecionString($"data source={RutaDB};password={password}");

            try
            {
                sw.Start();

                string json = ws.GetCatalogsJSON(TOKEN, Utilities.getHostName());

                if(json != null)
                {
                    var catalogos = JsonConvert.DeserializeObject<CatalogosJSON>(json, new JsonSerializerSettings
                    { NullValueHandling = NullValueHandling.Ignore });

                   GenesysOneDBLN.SincronizarCatalogos(
                   catalogos.DetailLicense,
                   catalogos.License,
                   catalogos.LoginFailLog,
                   catalogos.ModuleOptions,
                   catalogos.Modules,
                   catalogos.Office,
                   catalogos.RegistrationAgent,
                   catalogos.Settings,
                   catalogos.UserPermissions,
                   catalogos.UserRoles,
                   catalogos.Users,
                   catalogos.WebServiceTokens,
                   catalogos.WorkStations,
                   catalogos.Catalogs,
                   catalogos.CatalogStatus,
                   catalogos.CatalogTypes,
                   catalogos.CertificateOffice,
                   catalogos.Country,
                   catalogos.DocumentType,
                   catalogos.DocumentTypePerson,
                   catalogos.Nationality,
                   catalogos.Neighborhood,
                   catalogos.OfficeGP,
                   catalogos.PersonProfessions,
                   catalogos.PersonType,
                   catalogos.Products,
                   catalogos.ProfessionalAssociations,
                   catalogos.ReasonRejection,
                   catalogos.ReasonReview,
                   catalogos.RegistrationAuthority,
                   catalogos.RequestStatus,
                   catalogos.StateCity,
                   catalogos.StateCountry,
                   catalogos.TransactionStatus,
                   catalogos.ZipCodes
                   );

                    GenesysOneDBLN.ActualizarFechaUltimaSincronizacion();
                    UltimaSincronizacion = DateTime.Now;

                    sw.Stop();

                    Utilidades.Log.InsertarLog(Utilidades.Log.ErrorType.Informacion, "VariablesGlobales::SincronizarCatalogos", $"Completada sincronización de catálogos en {sw.ElapsedMilliseconds}ms.", PathDataLog);

                    return true;
                }

                return false;

            }
            catch (Exception ex)
            {
                Utilidades.Log.InsertarLog(Utilidades.Log.ErrorType.Error, "VariablesGlobales::SincronizarCatalogos", ex.StackTrace, PathDataLog);
                return false;
            }
        }

        public static bool NecesitaSincronizar
        {
            get => UltimaSincronizacion < DateTime.Now.AddSeconds(-60);
        }

        public static bool HayConexionRed
        {
            get => NetworkInterface.GetIsNetworkAvailable();
        }

        public static bool VerificarConexionAlservidor()
        {
            //***Direccion IP servidor de BD***
            bool exito;
            IPAddress ip = IPAddress.Parse("192.168.1.128");
            Ping ping = new Ping();
            PingReply pr = null;
            for (int i = 0; i < 4; i++)
            {
                pr = ping.Send(ip);
                //pr.Address, pr.Buffer.Length, pr.RoundtripTime, pr.Status.ToString();
            }
            if (pr.Status.ToString() == "Success") exito = true;
            else exito = false;

            return exito;
        }

    }
}