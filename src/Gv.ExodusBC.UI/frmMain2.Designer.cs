﻿namespace Gv.ExodusBc.UI
{
    partial class frmMain2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            this.pnlSideBar = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit4 = new DevExpress.XtraEditors.PictureEdit();
            this.flpMenu = new System.Windows.Forms.FlowLayoutPanel();
            this.btnEnrolamiento = new DevExpress.XtraEditors.SimpleButton();
            this.btnCertificados = new DevExpress.XtraEditors.SimpleButton();
            this.btnBandejaAprobacion = new DevExpress.XtraEditors.SimpleButton();
            this.btnBandeja = new DevExpress.XtraEditors.SimpleButton();
            this.btnBandejaCertificados = new DevExpress.XtraEditors.SimpleButton();
            this.btnConsultas = new DevExpress.XtraEditors.SimpleButton();
            this.btnReportes = new DevExpress.XtraEditors.SimpleButton();
            this.btnContratoFirmado = new DevExpress.XtraEditors.SimpleButton();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.pnlHeader = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit6 = new DevExpress.XtraEditors.PictureEdit();
            this.picCambiarPassword = new DevExpress.XtraEditors.PictureEdit();
            this.lblTituloCliente = new System.Windows.Forms.Label();
            this.perfilToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mniCambiarPassword = new System.Windows.Forms.ToolStripMenuItem();
            this.mniBloquear = new System.Windows.Forms.ToolStripMenuItem();
            this.mniSalir = new System.Windows.Forms.ToolStripMenuItem();
            this.lblUsuario = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.lblDelegacion = new DevExpress.XtraEditors.LabelControl();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.lblIP = new DevExpress.XtraEditors.LabelControl();
            this.lblIPDescripcion = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.lblEstacion = new DevExpress.XtraEditors.LabelControl();
            this.lblEstacionDescripcion = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.lblVersionApp = new DevExpress.XtraEditors.LabelControl();
            this.lblVersionDescripcion = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.lblConexionTitulo = new DevExpress.XtraEditors.LabelControl();
            this.lblEstadoConexion = new DevExpress.XtraEditors.LabelControl();
            this.lblServicioEnvio = new DevExpress.XtraEditors.LabelControl();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.ssForm = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::Gv.ExodusBc.UI.frmWaitForm), true, true);
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.pictureEdit2 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit3 = new DevExpress.XtraEditors.PictureEdit();
            this.lblRol = new DevExpress.XtraEditors.LabelControl();
            this.tlcRepVuelos = new DevExpress.XtraEditors.TileItem();
            this.tileItem1 = new DevExpress.XtraEditors.TileItem();
            this.pnlContenedor = new System.Windows.Forms.Panel();
            this.pnlBienvenida = new System.Windows.Forms.Panel();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit5 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblDia = new DevExpress.XtraEditors.LabelControl();
            this.lblMes = new DevExpress.XtraEditors.LabelControl();
            this.lblDiaNumero = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSideBar)).BeginInit();
            this.pnlSideBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).BeginInit();
            this.flpMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlHeader)).BeginInit();
            this.pnlHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCambiarPassword.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).BeginInit();
            this.pnlContenedor.SuspendLayout();
            this.pnlBienvenida.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit5.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlSideBar
            // 
            this.pnlSideBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pnlSideBar.Appearance.BackColor = System.Drawing.Color.Gray;
            this.pnlSideBar.Appearance.Options.UseBackColor = true;
            this.pnlSideBar.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.pnlSideBar.Controls.Add(this.pictureEdit4);
            this.pnlSideBar.Controls.Add(this.flpMenu);
            this.pnlSideBar.Location = new System.Drawing.Point(0, 58);
            this.pnlSideBar.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.pnlSideBar.Name = "pnlSideBar";
            this.pnlSideBar.Size = new System.Drawing.Size(233, 691);
            this.pnlSideBar.TabIndex = 7;
            // 
            // pictureEdit4
            // 
            this.pictureEdit4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit4.EditValue = global::Gv.ExodusBc.UI.Properties.Resources.lg_gv_trans;
            this.pictureEdit4.Location = new System.Drawing.Point(76, 7);
            this.pictureEdit4.Margin = new System.Windows.Forms.Padding(2);
            this.pictureEdit4.Name = "pictureEdit4";
            this.pictureEdit4.Properties.AllowFocused = false;
            this.pictureEdit4.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit4.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit4.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit4.Properties.ShowEditMenuItem = DevExpress.Utils.DefaultBoolean.False;
            this.pictureEdit4.Properties.ShowMenu = false;
            this.pictureEdit4.Properties.ShowZoomSubMenu = DevExpress.Utils.DefaultBoolean.False;
            this.pictureEdit4.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.pictureEdit4.Size = new System.Drawing.Size(64, 72);
            this.pictureEdit4.TabIndex = 48;
            // 
            // flpMenu
            // 
            this.flpMenu.BackColor = System.Drawing.Color.Transparent;
            this.flpMenu.Controls.Add(this.btnEnrolamiento);
            this.flpMenu.Controls.Add(this.btnCertificados);
            this.flpMenu.Controls.Add(this.btnBandejaAprobacion);
            this.flpMenu.Controls.Add(this.btnBandeja);
            this.flpMenu.Controls.Add(this.btnBandejaCertificados);
            this.flpMenu.Controls.Add(this.btnConsultas);
            this.flpMenu.Controls.Add(this.btnReportes);
            this.flpMenu.Controls.Add(this.btnContratoFirmado);
            this.flpMenu.Location = new System.Drawing.Point(7, 87);
            this.flpMenu.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.flpMenu.Name = "flpMenu";
            this.flpMenu.Size = new System.Drawing.Size(217, 463);
            this.flpMenu.TabIndex = 14;
            // 
            // btnEnrolamiento
            // 
            this.btnEnrolamiento.AllowFocus = false;
            this.btnEnrolamiento.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.btnEnrolamiento.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEnrolamiento.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnEnrolamiento.Appearance.Options.UseBackColor = true;
            this.btnEnrolamiento.Appearance.Options.UseFont = true;
            this.btnEnrolamiento.Appearance.Options.UseForeColor = true;
            this.btnEnrolamiento.Appearance.Options.UseTextOptions = true;
            this.btnEnrolamiento.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.btnEnrolamiento.AppearanceHovered.BackColor = System.Drawing.Color.Silver;
            this.btnEnrolamiento.AppearanceHovered.Options.UseBackColor = true;
            this.btnEnrolamiento.AppearancePressed.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEnrolamiento.AppearancePressed.Options.UseBackColor = true;
            this.btnEnrolamiento.AppearancePressed.Options.UseFont = true;
            this.btnEnrolamiento.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnEnrolamiento.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnEnrolamiento.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_mano_captura_32x32;
            this.btnEnrolamiento.Location = new System.Drawing.Point(2, 4);
            this.btnEnrolamiento.LookAndFeel.SkinName = "Visual Studio 2013 Light";
            this.btnEnrolamiento.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.btnEnrolamiento.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnEnrolamiento.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.btnEnrolamiento.Name = "btnEnrolamiento";
            this.btnEnrolamiento.Padding = new System.Windows.Forms.Padding(12, 0, 0, 0);
            this.btnEnrolamiento.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnEnrolamiento.Size = new System.Drawing.Size(210, 45);
            this.btnEnrolamiento.TabIndex = 0;
            this.btnEnrolamiento.Text = "&Enrolamiento";
            this.btnEnrolamiento.Click += new System.EventHandler(this.btnEnrolamiento_Click);
            // 
            // btnCertificados
            // 
            this.btnCertificados.AllowFocus = false;
            this.btnCertificados.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnCertificados.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCertificados.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.btnCertificados.Appearance.Options.UseBackColor = true;
            this.btnCertificados.Appearance.Options.UseFont = true;
            this.btnCertificados.Appearance.Options.UseForeColor = true;
            this.btnCertificados.Appearance.Options.UseTextOptions = true;
            this.btnCertificados.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.btnCertificados.AppearanceHovered.BackColor = System.Drawing.Color.Silver;
            this.btnCertificados.AppearanceHovered.Options.UseBackColor = true;
            this.btnCertificados.AppearancePressed.BackColor = System.Drawing.Color.Silver;
            this.btnCertificados.AppearancePressed.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnCertificados.AppearancePressed.Options.UseBackColor = true;
            this.btnCertificados.AppearancePressed.Options.UseFont = true;
            this.btnCertificados.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnCertificados.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCertificados.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_certificate_32x32;
            this.btnCertificados.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftTop;
            this.btnCertificados.Location = new System.Drawing.Point(2, 57);
            this.btnCertificados.LookAndFeel.SkinName = "Visual Studio 2013 Light";
            this.btnCertificados.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.btnCertificados.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnCertificados.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.btnCertificados.Name = "btnCertificados";
            this.btnCertificados.Padding = new System.Windows.Forms.Padding(12, 0, 0, 0);
            this.btnCertificados.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnCertificados.Size = new System.Drawing.Size(210, 45);
            this.btnCertificados.TabIndex = 1;
            this.btnCertificados.Text = "&Certificados";
            this.btnCertificados.Click += new System.EventHandler(this.btnCertificados_Click);
            // 
            // btnBandejaAprobacion
            // 
            this.btnBandejaAprobacion.AllowFocus = false;
            this.btnBandejaAprobacion.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnBandejaAprobacion.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBandejaAprobacion.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.btnBandejaAprobacion.Appearance.Options.UseBackColor = true;
            this.btnBandejaAprobacion.Appearance.Options.UseFont = true;
            this.btnBandejaAprobacion.Appearance.Options.UseForeColor = true;
            this.btnBandejaAprobacion.Appearance.Options.UseTextOptions = true;
            this.btnBandejaAprobacion.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.btnBandejaAprobacion.AppearanceHovered.BackColor = System.Drawing.Color.Silver;
            this.btnBandejaAprobacion.AppearanceHovered.Options.UseBackColor = true;
            this.btnBandejaAprobacion.AppearancePressed.BackColor = System.Drawing.Color.Silver;
            this.btnBandejaAprobacion.AppearancePressed.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnBandejaAprobacion.AppearancePressed.Options.UseBackColor = true;
            this.btnBandejaAprobacion.AppearancePressed.Options.UseFont = true;
            this.btnBandejaAprobacion.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnBandejaAprobacion.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnBandejaAprobacion.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_ticketnone_32x32;
            this.btnBandejaAprobacion.Location = new System.Drawing.Point(2, 110);
            this.btnBandejaAprobacion.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.btnBandejaAprobacion.Name = "btnBandejaAprobacion";
            this.btnBandejaAprobacion.Padding = new System.Windows.Forms.Padding(12, 0, 0, 0);
            this.btnBandejaAprobacion.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnBandejaAprobacion.Size = new System.Drawing.Size(210, 45);
            this.btnBandejaAprobacion.TabIndex = 2;
            this.btnBandejaAprobacion.Text = "&Bandeja aprobación";
            this.btnBandejaAprobacion.Click += new System.EventHandler(this.btnBandejaAprobacion_Click);
            // 
            // btnBandeja
            // 
            this.btnBandeja.AllowFocus = false;
            this.btnBandeja.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnBandeja.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBandeja.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.btnBandeja.Appearance.Options.UseBackColor = true;
            this.btnBandeja.Appearance.Options.UseFont = true;
            this.btnBandeja.Appearance.Options.UseForeColor = true;
            this.btnBandeja.Appearance.Options.UseTextOptions = true;
            this.btnBandeja.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.btnBandeja.AppearanceHovered.BackColor = System.Drawing.Color.Silver;
            this.btnBandeja.AppearanceHovered.Options.UseBackColor = true;
            this.btnBandeja.AppearancePressed.BackColor = System.Drawing.Color.Silver;
            this.btnBandeja.AppearancePressed.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnBandeja.AppearancePressed.Options.UseBackColor = true;
            this.btnBandeja.AppearancePressed.Options.UseFont = true;
            this.btnBandeja.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnBandeja.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnBandeja.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_tray_32x32;
            this.btnBandeja.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftTop;
            this.btnBandeja.Location = new System.Drawing.Point(2, 163);
            this.btnBandeja.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.btnBandeja.Name = "btnBandeja";
            this.btnBandeja.Padding = new System.Windows.Forms.Padding(12, 0, 0, 0);
            this.btnBandeja.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnBandeja.Size = new System.Drawing.Size(210, 45);
            this.btnBandeja.TabIndex = 3;
            this.btnBandeja.Text = "&Bandeja solicitudes";
            this.btnBandeja.Click += new System.EventHandler(this.btnBandeja_Click);
            // 
            // btnBandejaCertificados
            // 
            this.btnBandejaCertificados.AllowFocus = false;
            this.btnBandejaCertificados.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnBandejaCertificados.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBandejaCertificados.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.btnBandejaCertificados.Appearance.Options.UseBackColor = true;
            this.btnBandejaCertificados.Appearance.Options.UseFont = true;
            this.btnBandejaCertificados.Appearance.Options.UseForeColor = true;
            this.btnBandejaCertificados.Appearance.Options.UseTextOptions = true;
            this.btnBandejaCertificados.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.btnBandejaCertificados.AppearanceHovered.BackColor = System.Drawing.Color.Silver;
            this.btnBandejaCertificados.AppearanceHovered.Options.UseBackColor = true;
            this.btnBandejaCertificados.AppearancePressed.BackColor = System.Drawing.Color.Silver;
            this.btnBandejaCertificados.AppearancePressed.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnBandejaCertificados.AppearancePressed.Options.UseBackColor = true;
            this.btnBandejaCertificados.AppearancePressed.Options.UseFont = true;
            this.btnBandejaCertificados.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnBandejaCertificados.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnBandejaCertificados.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_sendfile_32x32;
            this.btnBandejaCertificados.Location = new System.Drawing.Point(2, 216);
            this.btnBandejaCertificados.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.btnBandejaCertificados.Name = "btnBandejaCertificados";
            this.btnBandejaCertificados.Padding = new System.Windows.Forms.Padding(12, 0, 0, 0);
            this.btnBandejaCertificados.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnBandejaCertificados.Size = new System.Drawing.Size(210, 45);
            this.btnBandejaCertificados.TabIndex = 5;
            this.btnBandejaCertificados.Text = "&Bandeja seguimiento";
            this.btnBandejaCertificados.Click += new System.EventHandler(this.btnBandejaCertificados_Click);
            // 
            // btnConsultas
            // 
            this.btnConsultas.AllowFocus = false;
            this.btnConsultas.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnConsultas.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConsultas.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.btnConsultas.Appearance.Options.UseBackColor = true;
            this.btnConsultas.Appearance.Options.UseFont = true;
            this.btnConsultas.Appearance.Options.UseForeColor = true;
            this.btnConsultas.Appearance.Options.UseTextOptions = true;
            this.btnConsultas.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.btnConsultas.AppearanceHovered.BackColor = System.Drawing.Color.Silver;
            this.btnConsultas.AppearanceHovered.Options.UseBackColor = true;
            this.btnConsultas.AppearancePressed.BackColor = System.Drawing.Color.Silver;
            this.btnConsultas.AppearancePressed.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnConsultas.AppearancePressed.Options.UseBackColor = true;
            this.btnConsultas.AppearancePressed.Options.UseFont = true;
            this.btnConsultas.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnConsultas.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnConsultas.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_ticketnone_32x32;
            this.btnConsultas.Location = new System.Drawing.Point(2, 269);
            this.btnConsultas.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.btnConsultas.Name = "btnConsultas";
            this.btnConsultas.Padding = new System.Windows.Forms.Padding(12, 0, 0, 0);
            this.btnConsultas.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnConsultas.Size = new System.Drawing.Size(210, 45);
            this.btnConsultas.TabIndex = 7;
            this.btnConsultas.Text = "&Valid emisiones";
            this.btnConsultas.Click += new System.EventHandler(this.btnConsultas_Click);
            // 
            // btnReportes
            // 
            this.btnReportes.AllowFocus = false;
            this.btnReportes.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnReportes.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReportes.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.btnReportes.Appearance.Options.UseBackColor = true;
            this.btnReportes.Appearance.Options.UseFont = true;
            this.btnReportes.Appearance.Options.UseForeColor = true;
            this.btnReportes.Appearance.Options.UseTextOptions = true;
            this.btnReportes.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.btnReportes.AppearanceHovered.BackColor = System.Drawing.Color.Silver;
            this.btnReportes.AppearanceHovered.Options.UseBackColor = true;
            this.btnReportes.AppearancePressed.BackColor = System.Drawing.Color.Silver;
            this.btnReportes.AppearancePressed.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnReportes.AppearancePressed.Options.UseBackColor = true;
            this.btnReportes.AppearancePressed.Options.UseFont = true;
            this.btnReportes.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnReportes.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnReportes.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.cancel_form;
            this.btnReportes.Location = new System.Drawing.Point(2, 322);
            this.btnReportes.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.btnReportes.Name = "btnReportes";
            this.btnReportes.Padding = new System.Windows.Forms.Padding(12, 0, 0, 0);
            this.btnReportes.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnReportes.Size = new System.Drawing.Size(210, 45);
            this.btnReportes.TabIndex = 8;
            this.btnReportes.Text = "&Valid revocaciones";
            this.btnReportes.Click += new System.EventHandler(this.btnReportes_Click);
            // 
            // btnContratoFirmado
            // 
            this.btnContratoFirmado.AllowFocus = false;
            this.btnContratoFirmado.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnContratoFirmado.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnContratoFirmado.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.btnContratoFirmado.Appearance.Options.UseBackColor = true;
            this.btnContratoFirmado.Appearance.Options.UseFont = true;
            this.btnContratoFirmado.Appearance.Options.UseForeColor = true;
            this.btnContratoFirmado.Appearance.Options.UseTextOptions = true;
            this.btnContratoFirmado.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.btnContratoFirmado.AppearanceHovered.BackColor = System.Drawing.Color.Silver;
            this.btnContratoFirmado.AppearanceHovered.Options.UseBackColor = true;
            this.btnContratoFirmado.AppearancePressed.BackColor = System.Drawing.Color.Silver;
            this.btnContratoFirmado.AppearancePressed.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnContratoFirmado.AppearancePressed.Options.UseBackColor = true;
            this.btnContratoFirmado.AppearancePressed.Options.UseFont = true;
            this.btnContratoFirmado.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnContratoFirmado.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnContratoFirmado.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.contract3;
            this.btnContratoFirmado.Location = new System.Drawing.Point(2, 375);
            this.btnContratoFirmado.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.btnContratoFirmado.Name = "btnContratoFirmado";
            this.btnContratoFirmado.Padding = new System.Windows.Forms.Padding(12, 0, 0, 0);
            this.btnContratoFirmado.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnContratoFirmado.Size = new System.Drawing.Size(210, 45);
            this.btnContratoFirmado.TabIndex = 6;
            this.btnContratoFirmado.Text = "&Contrato firmado";
            this.btnContratoFirmado.Click += new System.EventHandler(this.btnContratoFirmado_Click);
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit1.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit1.EditValue = global::Gv.ExodusBc.UI.Properties.Resources.Logo_Tecnisign;
            this.pictureEdit1.Location = new System.Drawing.Point(1412, 10);
            this.pictureEdit1.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.AllowFocused = false;
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit1.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit1.Size = new System.Drawing.Size(195, 39);
            this.pictureEdit1.TabIndex = 34;
            // 
            // pnlHeader
            // 
            this.pnlHeader.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.pnlHeader.Appearance.Options.UseBackColor = true;
            this.pnlHeader.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlHeader.Controls.Add(this.pictureEdit6);
            this.pnlHeader.Controls.Add(this.picCambiarPassword);
            this.pnlHeader.Controls.Add(this.pictureEdit1);
            this.pnlHeader.Controls.Add(this.lblTituloCliente);
            this.pnlHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlHeader.Location = new System.Drawing.Point(0, 0);
            this.pnlHeader.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.pnlHeader.LookAndFeel.UseDefaultLookAndFeel = false;
            this.pnlHeader.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.pnlHeader.Name = "pnlHeader";
            this.pnlHeader.Size = new System.Drawing.Size(1701, 58);
            this.pnlHeader.TabIndex = 9;
            this.pnlHeader.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlHeader_MouseDown);
            // 
            // pictureEdit6
            // 
            this.pictureEdit6.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit6.EditValue = global::Gv.ExodusBc.UI.Properties.Resources.G1;
            this.pictureEdit6.Location = new System.Drawing.Point(5, 7);
            this.pictureEdit6.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.pictureEdit6.Name = "pictureEdit6";
            this.pictureEdit6.Properties.AllowFocused = false;
            this.pictureEdit6.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit6.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit6.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit6.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit6.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit6.Size = new System.Drawing.Size(231, 43);
            this.pictureEdit6.TabIndex = 33;
            // 
            // picCambiarPassword
            // 
            this.picCambiarPassword.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.picCambiarPassword.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picCambiarPassword.EditValue = global::Gv.ExodusBc.UI.Properties.Resources.Login4;
            this.picCambiarPassword.Location = new System.Drawing.Point(1644, 5);
            this.picCambiarPassword.Margin = new System.Windows.Forms.Padding(2);
            this.picCambiarPassword.Name = "picCambiarPassword";
            this.picCambiarPassword.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picCambiarPassword.Properties.Appearance.Options.UseBackColor = true;
            this.picCambiarPassword.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.picCambiarPassword.Properties.ShowMenu = false;
            this.picCambiarPassword.Properties.ShowZoomSubMenu = DevExpress.Utils.DefaultBoolean.False;
            this.picCambiarPassword.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.picCambiarPassword.Size = new System.Drawing.Size(50, 50);
            this.picCambiarPassword.TabIndex = 47;
            this.picCambiarPassword.ToolTip = "Cambiar contraseña";
            this.picCambiarPassword.Click += new System.EventHandler(this.picCambiarPassword_Click);
            // 
            // lblTituloCliente
            // 
            this.lblTituloCliente.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTituloCliente.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTituloCliente.ForeColor = System.Drawing.Color.White;
            this.lblTituloCliente.Location = new System.Drawing.Point(0, 0);
            this.lblTituloCliente.Name = "lblTituloCliente";
            this.lblTituloCliente.Size = new System.Drawing.Size(1701, 58);
            this.lblTituloCliente.TabIndex = 48;
            this.lblTituloCliente.Text = "Emisión de Certificados Digitales";
            this.lblTituloCliente.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // perfilToolStripMenuItem
            // 
            this.perfilToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mniCambiarPassword,
            this.mniBloquear,
            this.mniSalir});
            this.perfilToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.perfilToolStripMenuItem.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_profile48x48;
            this.perfilToolStripMenuItem.Name = "perfilToolStripMenuItem";
            this.perfilToolStripMenuItem.Size = new System.Drawing.Size(81, 27);
            this.perfilToolStripMenuItem.Text = "Perfil";
            // 
            // mniCambiarPassword
            // 
            this.mniCambiarPassword.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.mniCambiarPassword.ForeColor = System.Drawing.Color.White;
            this.mniCambiarPassword.Name = "mniCambiarPassword";
            this.mniCambiarPassword.Size = new System.Drawing.Size(249, 28);
            this.mniCambiarPassword.Text = "Cambiar Contraseña";
            this.mniCambiarPassword.Click += new System.EventHandler(this.mniCambiarPassword_Click);
            // 
            // mniBloquear
            // 
            this.mniBloquear.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.mniBloquear.ForeColor = System.Drawing.Color.White;
            this.mniBloquear.Name = "mniBloquear";
            this.mniBloquear.Size = new System.Drawing.Size(249, 28);
            this.mniBloquear.Text = "Bloquear";
            this.mniBloquear.Click += new System.EventHandler(this.mniBloquear_Click);
            // 
            // mniSalir
            // 
            this.mniSalir.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.mniSalir.ForeColor = System.Drawing.Color.White;
            this.mniSalir.Name = "mniSalir";
            this.mniSalir.Size = new System.Drawing.Size(249, 28);
            this.mniSalir.Text = "Salir";
            this.mniSalir.Click += new System.EventHandler(this.mniSalir_Click);
            // 
            // lblUsuario
            // 
            this.lblUsuario.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsuario.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lblUsuario.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblUsuario.Appearance.Options.UseFont = true;
            this.lblUsuario.Appearance.Options.UseForeColor = true;
            this.lblUsuario.Appearance.Options.UseImageAlign = true;
            this.lblUsuario.Appearance.Options.UseTextOptions = true;
            this.lblUsuario.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lblUsuario.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftTop;
            this.lblUsuario.Location = new System.Drawing.Point(36, 4);
            this.lblUsuario.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblUsuario.Name = "lblUsuario";
            this.lblUsuario.Size = new System.Drawing.Size(8, 17);
            this.lblUsuario.TabIndex = 16;
            this.lblUsuario.Text = "[]";
            this.lblUsuario.ToolTip = "Usuario";
            // 
            // labelControl10
            // 
            this.labelControl10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl10.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl10.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl10.LineLocation = DevExpress.XtraEditors.LineLocation.Top;
            this.labelControl10.LineVisible = true;
            this.labelControl10.Location = new System.Drawing.Point(250, 122);
            this.labelControl10.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(1439, 23);
            this.labelControl10.TabIndex = 17;
            // 
            // lblDelegacion
            // 
            this.lblDelegacion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDelegacion.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDelegacion.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lblDelegacion.Appearance.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_office_24x24;
            this.lblDelegacion.Appearance.Options.UseFont = true;
            this.lblDelegacion.Appearance.Options.UseForeColor = true;
            this.lblDelegacion.Appearance.Options.UseImage = true;
            this.lblDelegacion.Appearance.Options.UseTextOptions = true;
            this.lblDelegacion.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lblDelegacion.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblDelegacion.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.lblDelegacion.Location = new System.Drawing.Point(1332, 92);
            this.lblDelegacion.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblDelegacion.Name = "lblDelegacion";
            this.lblDelegacion.Size = new System.Drawing.Size(349, 34);
            this.lblDelegacion.TabIndex = 25;
            this.lblDelegacion.Text = "[]";
            this.lblDelegacion.ToolTip = "Oficina";
            // 
            // panelControl7
            // 
            this.panelControl7.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.panelControl7.Appearance.Options.UseBackColor = true;
            this.panelControl7.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl7.Controls.Add(this.flowLayoutPanel2);
            this.panelControl7.Controls.Add(this.lblServicioEnvio);
            this.panelControl7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl7.Location = new System.Drawing.Point(0, 745);
            this.panelControl7.LookAndFeel.SkinName = "Metropolis";
            this.panelControl7.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.panelControl7.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelControl7.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(1701, 32);
            this.panelControl7.TabIndex = 27;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel2.Controls.Add(this.lblIP);
            this.flowLayoutPanel2.Controls.Add(this.lblIPDescripcion);
            this.flowLayoutPanel2.Controls.Add(this.labelControl9);
            this.flowLayoutPanel2.Controls.Add(this.lblEstacion);
            this.flowLayoutPanel2.Controls.Add(this.lblEstacionDescripcion);
            this.flowLayoutPanel2.Controls.Add(this.labelControl1);
            this.flowLayoutPanel2.Controls.Add(this.lblVersionApp);
            this.flowLayoutPanel2.Controls.Add(this.lblVersionDescripcion);
            this.flowLayoutPanel2.Controls.Add(this.labelControl8);
            this.flowLayoutPanel2.Controls.Add(this.lblConexionTitulo);
            this.flowLayoutPanel2.Controls.Add(this.lblEstadoConexion);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(9, 2);
            this.flowLayoutPanel2.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(752, 28);
            this.flowLayoutPanel2.TabIndex = 16;
            // 
            // lblIP
            // 
            this.lblIP.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIP.Appearance.ForeColor = System.Drawing.Color.LightGray;
            this.lblIP.Appearance.Image = global::Gv.ExodusBc.UI.Properties.Resources.documentmap_16x16;
            this.lblIP.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblIP.Appearance.Options.UseFont = true;
            this.lblIP.Appearance.Options.UseForeColor = true;
            this.lblIP.Appearance.Options.UseImage = true;
            this.lblIP.Appearance.Options.UseImageAlign = true;
            this.lblIP.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.lblIP.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ips_white;
            this.lblIP.Location = new System.Drawing.Point(2, 4);
            this.lblIP.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblIP.Name = "lblIP";
            this.lblIP.Size = new System.Drawing.Size(36, 20);
            this.lblIP.TabIndex = 12;
            this.lblIP.Text = "IP:";
            this.toolTip1.SetToolTip(this.lblIP, "Dirección IP");
            // 
            // lblIPDescripcion
            // 
            this.lblIPDescripcion.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIPDescripcion.Appearance.ForeColor = System.Drawing.Color.White;
            this.lblIPDescripcion.Appearance.Options.UseFont = true;
            this.lblIPDescripcion.Appearance.Options.UseForeColor = true;
            this.lblIPDescripcion.Location = new System.Drawing.Point(42, 4);
            this.lblIPDescripcion.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblIPDescripcion.Name = "lblIPDescripcion";
            this.lblIPDescripcion.Size = new System.Drawing.Size(18, 20);
            this.lblIPDescripcion.TabIndex = 37;
            this.lblIPDescripcion.Text = "  []";
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl9.Appearance.ForeColor = System.Drawing.Color.DimGray;
            this.labelControl9.Appearance.Options.UseFont = true;
            this.labelControl9.Appearance.Options.UseForeColor = true;
            this.labelControl9.Location = new System.Drawing.Point(64, 4);
            this.labelControl9.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(25, 22);
            this.labelControl9.TabIndex = 32;
            this.labelControl9.Text = "  |  ";
            // 
            // lblEstacion
            // 
            this.lblEstacion.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstacion.Appearance.ForeColor = System.Drawing.Color.LightGray;
            this.lblEstacion.Appearance.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_EstacionDeTrabajo1_16x16;
            this.lblEstacion.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblEstacion.Appearance.Options.UseFont = true;
            this.lblEstacion.Appearance.Options.UseForeColor = true;
            this.lblEstacion.Appearance.Options.UseImage = true;
            this.lblEstacion.Appearance.Options.UseImageAlign = true;
            this.lblEstacion.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.lblEstacion.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.statios_white;
            this.lblEstacion.Location = new System.Drawing.Point(93, 4);
            this.lblEstacion.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblEstacion.Name = "lblEstacion";
            this.lblEstacion.Size = new System.Drawing.Size(79, 20);
            this.lblEstacion.TabIndex = 13;
            this.lblEstacion.Text = "Estación:";
            this.toolTip1.SetToolTip(this.lblEstacion, "Estación de Trabajo");
            // 
            // lblEstacionDescripcion
            // 
            this.lblEstacionDescripcion.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstacionDescripcion.Appearance.ForeColor = System.Drawing.Color.White;
            this.lblEstacionDescripcion.Appearance.Options.UseFont = true;
            this.lblEstacionDescripcion.Appearance.Options.UseForeColor = true;
            this.lblEstacionDescripcion.Location = new System.Drawing.Point(176, 4);
            this.lblEstacionDescripcion.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblEstacionDescripcion.Name = "lblEstacionDescripcion";
            this.lblEstacionDescripcion.Size = new System.Drawing.Size(18, 20);
            this.lblEstacionDescripcion.TabIndex = 38;
            this.lblEstacionDescripcion.Text = "  []";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.DimGray;
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Appearance.Options.UseForeColor = true;
            this.labelControl1.Location = new System.Drawing.Point(198, 4);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(25, 22);
            this.labelControl1.TabIndex = 33;
            this.labelControl1.Text = "  |  ";
            // 
            // lblVersionApp
            // 
            this.lblVersionApp.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVersionApp.Appearance.ForeColor = System.Drawing.Color.LightGray;
            this.lblVersionApp.Appearance.Image = global::Gv.ExodusBc.UI.Properties.Resources.groupfieldcollection_16x16;
            this.lblVersionApp.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblVersionApp.Appearance.Options.UseFont = true;
            this.lblVersionApp.Appearance.Options.UseForeColor = true;
            this.lblVersionApp.Appearance.Options.UseImage = true;
            this.lblVersionApp.Appearance.Options.UseImageAlign = true;
            this.lblVersionApp.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.lblVersionApp.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.windows_white;
            this.lblVersionApp.Location = new System.Drawing.Point(227, 4);
            this.lblVersionApp.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblVersionApp.Name = "lblVersionApp";
            this.lblVersionApp.Size = new System.Drawing.Size(73, 20);
            this.lblVersionApp.TabIndex = 34;
            this.lblVersionApp.Text = "Versión:";
            this.toolTip1.SetToolTip(this.lblVersionApp, "Versión del sistema");
            // 
            // lblVersionDescripcion
            // 
            this.lblVersionDescripcion.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVersionDescripcion.Appearance.ForeColor = System.Drawing.Color.White;
            this.lblVersionDescripcion.Appearance.Options.UseFont = true;
            this.lblVersionDescripcion.Appearance.Options.UseForeColor = true;
            this.lblVersionDescripcion.Location = new System.Drawing.Point(304, 4);
            this.lblVersionDescripcion.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblVersionDescripcion.Name = "lblVersionDescripcion";
            this.lblVersionDescripcion.Size = new System.Drawing.Size(18, 20);
            this.lblVersionDescripcion.TabIndex = 39;
            this.lblVersionDescripcion.Text = "  []";
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.labelControl8.Appearance.ForeColor = System.Drawing.Color.DimGray;
            this.labelControl8.Appearance.Options.UseFont = true;
            this.labelControl8.Appearance.Options.UseForeColor = true;
            this.labelControl8.Location = new System.Drawing.Point(326, 4);
            this.labelControl8.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(25, 22);
            this.labelControl8.TabIndex = 42;
            this.labelControl8.Text = "  |  ";
            // 
            // lblConexionTitulo
            // 
            this.lblConexionTitulo.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConexionTitulo.Appearance.ForeColor = System.Drawing.Color.LightGray;
            this.lblConexionTitulo.Appearance.Image = global::Gv.ExodusBc.UI.Properties.Resources.groupfieldcollection_16x16;
            this.lblConexionTitulo.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblConexionTitulo.Appearance.Options.UseFont = true;
            this.lblConexionTitulo.Appearance.Options.UseForeColor = true;
            this.lblConexionTitulo.Appearance.Options.UseImage = true;
            this.lblConexionTitulo.Appearance.Options.UseImageAlign = true;
            this.lblConexionTitulo.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.lblConexionTitulo.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.conection_white;
            this.lblConexionTitulo.Location = new System.Drawing.Point(355, 4);
            this.lblConexionTitulo.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblConexionTitulo.Name = "lblConexionTitulo";
            this.lblConexionTitulo.Size = new System.Drawing.Size(86, 20);
            this.lblConexionTitulo.TabIndex = 40;
            this.lblConexionTitulo.Text = "Conexión:";
            this.toolTip1.SetToolTip(this.lblConexionTitulo, "Estado de la conexión");
            // 
            // lblEstadoConexion
            // 
            this.lblEstadoConexion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEstadoConexion.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstadoConexion.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(204)))), ((int)(((byte)(113)))));
            this.lblEstadoConexion.Appearance.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_online26x26;
            this.lblEstadoConexion.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblEstadoConexion.Appearance.Options.UseFont = true;
            this.lblEstadoConexion.Appearance.Options.UseForeColor = true;
            this.lblEstadoConexion.Appearance.Options.UseImage = true;
            this.lblEstadoConexion.Appearance.Options.UseImageAlign = true;
            this.lblEstadoConexion.Appearance.Options.UseTextOptions = true;
            this.lblEstadoConexion.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblEstadoConexion.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblEstadoConexion.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.lblEstadoConexion.Location = new System.Drawing.Point(445, 4);
            this.lblEstadoConexion.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblEstadoConexion.Name = "lblEstadoConexion";
            this.lblEstadoConexion.Size = new System.Drawing.Size(150, 20);
            this.lblEstadoConexion.TabIndex = 34;
            this.lblEstadoConexion.Text = "[]";
            // 
            // lblServicioEnvio
            // 
            this.lblServicioEnvio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblServicioEnvio.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblServicioEnvio.Appearance.ForeColor = System.Drawing.Color.White;
            this.lblServicioEnvio.Appearance.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_uploadgreen16x16;
            this.lblServicioEnvio.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblServicioEnvio.Appearance.Options.UseFont = true;
            this.lblServicioEnvio.Appearance.Options.UseForeColor = true;
            this.lblServicioEnvio.Appearance.Options.UseImage = true;
            this.lblServicioEnvio.Appearance.Options.UseImageAlign = true;
            this.lblServicioEnvio.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.lblServicioEnvio.Location = new System.Drawing.Point(1500, 5);
            this.lblServicioEnvio.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblServicioEnvio.Name = "lblServicioEnvio";
            this.lblServicioEnvio.Size = new System.Drawing.Size(171, 20);
            this.lblServicioEnvio.TabIndex = 14;
            this.lblServicioEnvio.Text = "Envío de transacciones";
            this.toolTip1.SetToolTip(this.lblServicioEnvio, "Estado del servicio de Envío de Transacciones");
            this.lblServicioEnvio.DoubleClick += new System.EventHandler(this.lblServicioEnvio_DoubleClick);
            // 
            // ssForm
            // 
            this.ssForm.ClosingDelay = 500;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel1.Controls.Add(this.pictureEdit2);
            this.flowLayoutPanel1.Controls.Add(this.lblUsuario);
            this.flowLayoutPanel1.Controls.Add(this.labelControl7);
            this.flowLayoutPanel1.Controls.Add(this.pictureEdit3);
            this.flowLayoutPanel1.Controls.Add(this.lblRol);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(248, 94);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1079, 36);
            this.flowLayoutPanel1.TabIndex = 30;
            // 
            // pictureEdit2
            // 
            this.pictureEdit2.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit2.EditValue = global::Gv.ExodusBc.UI.Properties.Resources.ic_user_24x24;
            this.pictureEdit2.Location = new System.Drawing.Point(2, 2);
            this.pictureEdit2.Margin = new System.Windows.Forms.Padding(2);
            this.pictureEdit2.Name = "pictureEdit2";
            this.pictureEdit2.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit2.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit2.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit2.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit2.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.pictureEdit2.Size = new System.Drawing.Size(30, 30);
            this.pictureEdit2.TabIndex = 33;
            this.pictureEdit2.ToolTip = "Usuario";
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.labelControl7.Appearance.ForeColor = System.Drawing.Color.DimGray;
            this.labelControl7.Appearance.Options.UseFont = true;
            this.labelControl7.Appearance.Options.UseForeColor = true;
            this.labelControl7.Location = new System.Drawing.Point(48, 4);
            this.labelControl7.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(25, 22);
            this.labelControl7.TabIndex = 31;
            this.labelControl7.Text = "  |  ";
            // 
            // pictureEdit3
            // 
            this.pictureEdit3.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit3.EditValue = global::Gv.ExodusBc.UI.Properties.Resources.ic_rol_24x24;
            this.pictureEdit3.Location = new System.Drawing.Point(77, 2);
            this.pictureEdit3.Margin = new System.Windows.Forms.Padding(2);
            this.pictureEdit3.Name = "pictureEdit3";
            this.pictureEdit3.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit3.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit3.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit3.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit3.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.pictureEdit3.Size = new System.Drawing.Size(25, 25);
            this.pictureEdit3.TabIndex = 34;
            this.pictureEdit3.ToolTip = "Rol ";
            // 
            // lblRol
            // 
            this.lblRol.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRol.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lblRol.Appearance.Options.UseFont = true;
            this.lblRol.Appearance.Options.UseForeColor = true;
            this.lblRol.Location = new System.Drawing.Point(106, 4);
            this.lblRol.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblRol.Name = "lblRol";
            this.lblRol.Size = new System.Drawing.Size(8, 17);
            this.lblRol.TabIndex = 32;
            this.lblRol.Text = "[]";
            this.lblRol.ToolTip = "Rol ";
            // 
            // tlcRepVuelos
            // 
            this.tlcRepVuelos.AppearanceItem.Normal.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tlcRepVuelos.AppearanceItem.Normal.BorderColor = System.Drawing.Color.Silver;
            this.tlcRepVuelos.AppearanceItem.Normal.Font = new System.Drawing.Font("Tahoma", 12F);
            this.tlcRepVuelos.AppearanceItem.Normal.ForeColor = System.Drawing.Color.Black;
            this.tlcRepVuelos.AppearanceItem.Normal.Options.UseBackColor = true;
            this.tlcRepVuelos.AppearanceItem.Normal.Options.UseBorderColor = true;
            this.tlcRepVuelos.AppearanceItem.Normal.Options.UseFont = true;
            this.tlcRepVuelos.AppearanceItem.Normal.Options.UseForeColor = true;
            this.tlcRepVuelos.AppearanceItem.Pressed.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(152)))), ((int)(((byte)(219)))));
            this.tlcRepVuelos.AppearanceItem.Pressed.Options.UseForeColor = true;
            tileItemElement3.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_report3_blue_48x48;
            tileItemElement3.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.TopCenter;
            tileItemElement3.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Top;
            tileItemElement3.Text = "Vuelos";
            this.tlcRepVuelos.Elements.Add(tileItemElement3);
            this.tlcRepVuelos.Id = 27;
            this.tlcRepVuelos.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide;
            this.tlcRepVuelos.Name = "tlcRepVuelos";
            // 
            // tileItem1
            // 
            this.tileItem1.AppearanceItem.Normal.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tileItem1.AppearanceItem.Normal.BorderColor = System.Drawing.Color.Silver;
            this.tileItem1.AppearanceItem.Normal.Font = new System.Drawing.Font("Tahoma", 12F);
            this.tileItem1.AppearanceItem.Normal.ForeColor = System.Drawing.Color.Black;
            this.tileItem1.AppearanceItem.Normal.Options.UseBackColor = true;
            this.tileItem1.AppearanceItem.Normal.Options.UseBorderColor = true;
            this.tileItem1.AppearanceItem.Normal.Options.UseFont = true;
            this.tileItem1.AppearanceItem.Normal.Options.UseForeColor = true;
            this.tileItem1.AppearanceItem.Pressed.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(152)))), ((int)(((byte)(219)))));
            this.tileItem1.AppearanceItem.Pressed.Options.UseForeColor = true;
            tileItemElement4.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_configblue48x48;
            tileItemElement4.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.TopCenter;
            tileItemElement4.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Top;
            tileItemElement4.Text = "Sistema";
            this.tileItem1.Elements.Add(tileItemElement4);
            this.tileItem1.Id = 22;
            this.tileItem1.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide;
            this.tileItem1.Name = "tileItem1";
            // 
            // pnlContenedor
            // 
            this.pnlContenedor.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlContenedor.Controls.Add(this.pnlBienvenida);
            this.pnlContenedor.Location = new System.Drawing.Point(250, 145);
            this.pnlContenedor.Margin = new System.Windows.Forms.Padding(2);
            this.pnlContenedor.Name = "pnlContenedor";
            this.pnlContenedor.Size = new System.Drawing.Size(1445, 594);
            this.pnlContenedor.TabIndex = 32;
            // 
            // pnlBienvenida
            // 
            this.pnlBienvenida.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlBienvenida.BackColor = System.Drawing.Color.White;
            this.pnlBienvenida.Controls.Add(this.panelControl3);
            this.pnlBienvenida.Controls.Add(this.panel2);
            this.pnlBienvenida.Location = new System.Drawing.Point(9, 8);
            this.pnlBienvenida.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.pnlBienvenida.Name = "pnlBienvenida";
            this.pnlBienvenida.Size = new System.Drawing.Size(1426, 581);
            this.pnlBienvenida.TabIndex = 32;
            // 
            // panelControl3
            // 
            this.panelControl3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl3.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.panelControl3.Appearance.Options.UseBackColor = true;
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.pictureEdit5);
            this.panelControl3.Controls.Add(this.labelControl2);
            this.panelControl3.Controls.Add(this.labelControl5);
            this.panelControl3.Location = new System.Drawing.Point(20, 4);
            this.panelControl3.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(1403, 70);
            this.panelControl3.TabIndex = 19;
            // 
            // pictureEdit5
            // 
            this.pictureEdit5.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit5.EditValue = global::Gv.ExodusBc.UI.Properties.Resources.ic_fingerprint100x100;
            this.pictureEdit5.Location = new System.Drawing.Point(6, 7);
            this.pictureEdit5.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.pictureEdit5.Name = "pictureEdit5";
            this.pictureEdit5.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit5.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit5.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit5.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit5.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit5.Size = new System.Drawing.Size(54, 59);
            this.pictureEdit5.TabIndex = 29;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Appearance.Options.UseForeColor = true;
            this.labelControl2.Appearance.Options.UseTextOptions = true;
            this.labelControl2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl2.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.labelControl2.Location = new System.Drawing.Point(66, 32);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(898, 32);
            this.labelControl2.TabIndex = 16;
            this.labelControl2.Text = "Sistema desarrollado por Grupo Visión, para la captura de información biométrica " +
    "e información personal.";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Appearance.Options.UseForeColor = true;
            this.labelControl5.Appearance.Options.UseTextOptions = true;
            this.labelControl5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl5.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.labelControl5.Location = new System.Drawing.Point(66, 7);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(898, 28);
            this.labelControl5.TabIndex = 15;
            this.labelControl5.Text = "Sistema de enrolamiento biométrico";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(76)))), ((int)(((byte)(70)))));
            this.panel2.Controls.Add(this.lblDia);
            this.panel2.Controls.Add(this.lblMes);
            this.panel2.Controls.Add(this.lblDiaNumero);
            this.panel2.Location = new System.Drawing.Point(20, 86);
            this.panel2.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(232, 173);
            this.panel2.TabIndex = 9;
            // 
            // lblDia
            // 
            this.lblDia.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDia.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.lblDia.Appearance.Options.UseFont = true;
            this.lblDia.Appearance.Options.UseForeColor = true;
            this.lblDia.Appearance.Options.UseTextOptions = true;
            this.lblDia.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblDia.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblDia.Location = new System.Drawing.Point(2, 4);
            this.lblDia.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblDia.Name = "lblDia";
            this.lblDia.Size = new System.Drawing.Size(228, 25);
            this.lblDia.TabIndex = 4;
            this.lblDia.Text = "Lunes";
            // 
            // lblMes
            // 
            this.lblMes.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMes.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.lblMes.Appearance.Options.UseFont = true;
            this.lblMes.Appearance.Options.UseForeColor = true;
            this.lblMes.Appearance.Options.UseTextOptions = true;
            this.lblMes.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblMes.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblMes.Location = new System.Drawing.Point(4, 144);
            this.lblMes.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblMes.Name = "lblMes";
            this.lblMes.Size = new System.Drawing.Size(227, 28);
            this.lblMes.TabIndex = 6;
            this.lblMes.Text = "JULIO 2017";
            // 
            // lblDiaNumero
            // 
            this.lblDiaNumero.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiaNumero.Appearance.ForeColor = System.Drawing.Color.White;
            this.lblDiaNumero.Appearance.Options.UseFont = true;
            this.lblDiaNumero.Appearance.Options.UseForeColor = true;
            this.lblDiaNumero.Appearance.Options.UseTextOptions = true;
            this.lblDiaNumero.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblDiaNumero.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblDiaNumero.Location = new System.Drawing.Point(2, 51);
            this.lblDiaNumero.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblDiaNumero.Name = "lblDiaNumero";
            this.lblDiaNumero.Size = new System.Drawing.Size(228, 75);
            this.lblDiaNumero.TabIndex = 5;
            this.lblDiaNumero.Text = "17";
            // 
            // frmMain2
            // 
            this.Appearance.BackColor = System.Drawing.Color.White;
            this.Appearance.ForeColor = System.Drawing.Color.White;
            this.Appearance.Options.UseBackColor = true;
            this.Appearance.Options.UseForeColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1701, 777);
            this.Controls.Add(this.labelControl10);
            this.Controls.Add(this.panelControl7);
            this.Controls.Add(this.pnlContenedor);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.lblDelegacion);
            this.Controls.Add(this.pnlSideBar);
            this.Controls.Add(this.pnlHeader);
            this.DoubleBuffered = true;
            this.FormBorderEffect = DevExpress.XtraEditors.FormBorderEffect.Shadow;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.Name = "frmMain2";
            this.Text = "GenesysOne";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmMain2_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmMain2_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.pnlSideBar)).EndInit();
            this.pnlSideBar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).EndInit();
            this.flpMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlHeader)).EndInit();
            this.pnlHeader.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCambiarPassword.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            this.panelControl7.PerformLayout();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).EndInit();
            this.pnlContenedor.ResumeLayout(false);
            this.pnlBienvenida.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit5.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraEditors.PanelControl pnlSideBar;
        private DevExpress.XtraEditors.SimpleButton btnCertificados;
        private DevExpress.XtraEditors.SimpleButton btnReportes;
        private DevExpress.XtraEditors.PanelControl pnlHeader;
        private DevExpress.XtraEditors.LabelControl lblUsuario;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl lblDelegacion;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraEditors.LabelControl lblIP;
        private DevExpress.XtraEditors.LabelControl lblEstacion;
        private DevExpress.XtraEditors.LabelControl lblServicioEnvio;
        private DevExpress.XtraEditors.SimpleButton btnEnrolamiento;
     

        private System.Windows.Forms.ToolTip toolTip1;
        private DevExpress.XtraSplashScreen.SplashScreenManager ssForm;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.LabelControl lblRol;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private System.Windows.Forms.ToolStripMenuItem perfilToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mniCambiarPassword;
        private System.Windows.Forms.ToolStripMenuItem mniBloquear;
        private System.Windows.Forms.ToolStripMenuItem mniSalir;
        private System.Windows.Forms.FlowLayoutPanel flpMenu;
        private DevExpress.XtraEditors.TileItem tlcRepVuelos;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl lblVersionApp;
        private DevExpress.XtraEditors.SimpleButton btnConsultas;
        private DevExpress.XtraEditors.TileItem tileItem1;
        private System.Windows.Forms.Panel pnlContenedor;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PictureEdit pictureEdit5;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private System.Windows.Forms.Panel panel2;
        private DevExpress.XtraEditors.LabelControl lblDia;
        private DevExpress.XtraEditors.LabelControl lblMes;
        private DevExpress.XtraEditors.LabelControl lblDiaNumero;
        private System.Windows.Forms.Panel pnlBienvenida;
        private DevExpress.XtraEditors.SimpleButton btnBandeja;
        private DevExpress.XtraEditors.SimpleButton btnBandejaAprobacion;
        private DevExpress.XtraEditors.PictureEdit pictureEdit2;
        private DevExpress.XtraEditors.PictureEdit pictureEdit3;
        private DevExpress.XtraEditors.SimpleButton btnBandejaCertificados;
        private DevExpress.XtraEditors.SimpleButton btnContratoFirmado;
        private DevExpress.XtraEditors.PictureEdit pictureEdit6;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.LabelControl lblIPDescripcion;
        private DevExpress.XtraEditors.LabelControl lblEstacionDescripcion;
        private DevExpress.XtraEditors.LabelControl lblVersionDescripcion;
        private DevExpress.XtraEditors.LabelControl lblConexionTitulo;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl lblEstadoConexion;
        private DevExpress.XtraEditors.PictureEdit picCambiarPassword;
        private System.Windows.Forms.Label lblTituloCliente;
        private DevExpress.XtraEditors.PictureEdit pictureEdit4;
    }
}