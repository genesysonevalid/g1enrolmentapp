﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using AForge.Video.DirectShow;
using Gv.ExodusBc.UI.Properties;
using Newtonsoft.Json.Linq;
using Gv.ExodusBc.EN;
using Gv.ExodusBc.UI.WebReference;
using System.IO;
using Gv.Logitech.SDK;
using Gv.ExodusBc.LN;
using DevExpress.XtraEditors.Mask;
using static Gv.ExodusBc.UI.ExodusBcBase;

namespace Gv.ExodusBc.UI.Modulos.InspeccionPrimaria
{

    public partial class frmEnrolamientoActualizacion : DevExpress.XtraEditors.XtraForm
    {
        bool estadoBiograficos = false;
        bool estadoHuellas = false;
        bool estadoFoto = false;
        bool estadoFirma = false;
        bool estadoOtros = false;
        public bool resFoto;
        public bool resFirma;
        Image firmaCapturada = null;
        string ValoresIcao = string.Empty;
        int TotalErroresIcao = 0;
        long _EnrolmentId = 0;
        PictureBox picTemporal = new PictureBox();
        public int PersonaId { get; set; }
        public static FilterInfoCollection DispositivosDeVideo;
        public static List<CamarasDisponibles> lstCamaras;
        private static ExodusBcService web = new ExodusBcService();
        bool ExisteCamara;
        bool ExistePadFirmas;
        public frmEnrolamientoActualizacion()
        {
            InitializeComponent();
            Init();
        }

        private void frmEnrolamientoActualizacion_Load(object sender, EventArgs e)
        {
            //Llenar combos
            ExodusBcBase.Helper.Combobox.setCombo(lueSexo, Convert.ToInt32(LN.LISTCATALOGSTYPES.ListTypesCatalogs.Sexo));
            ExodusBcBase.Helper.Combobox.setComboTipoDocumento(lueTipoDocumento);
            ExodusBcBase.Helper.Combobox.setCombo(lueEstadoCivil, Convert.ToInt32(LN.LISTCATALOGSTYPES.ListTypesCatalogs.EstadoCivil));
            ExodusBcBase.Helper.Combobox.setComboProfesiones(lueProfesion);
            ExodusBcBase.Helper.Combobox.setCombo(lueNivelEducativo, Convert.ToInt32(LN.LISTCATALOGSTYPES.ListTypesCatalogs.NivelEducativo));
            ExodusBcBase.Helper.Combobox.setComboCountry(luePaisNacimiento);

            ExodusBcBase.Helper.Combobox.setComboNationality(lueNacionalidad);
            ExodusBcBase.Helper.Combobox.setComboStateCountry(lueDepartamentos);
            //ssForm.ShowWaitForm();
            comprobarCamara();
            comprobarPad();
            CargarDatosEditar();
            //if (ssForm.IsSplashFormVisible)
            //    ssForm.CloseWaitForm();

        }

        private void Init()
        {
           


        }

        public void comprobarCamara()
        {
            //Iniciar Camara
            DispositivosDeVideo = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            CargarCamara(DispositivosDeVideo);
            ////Temporal	
            //ExisteCamara = true;
            if (ExisteCamara)
            {
                picCamara.Image = Resources.camera1;
            }
            else
            {
                picCamara.Image = Resources.camera2;
            }
        }

        public class CamarasDisponibles
        {
            public int Index { get; set; }
            public string Nombre { get; set; }
        }

        private void CargarCamara(FilterInfoCollection Dispositivos)
        {
            lstCamaras = new List<CamarasDisponibles>();
            for (int item = 0; item <= Dispositivos.Count - 1; item++)
            {
                CamarasDisponibles camara = new CamarasDisponibles() { Index = item, Nombre = Dispositivos[item].Name };
                lstCamaras.Add(camara);
            }
            foreach (var camara in lstCamaras)
            {
                if (camara.Nombre.ToString().Contains(VariablesGlobales.NOMBRECAMARA)) ExisteCamara = true;
                else ExisteCamara = false;
            }
        }

        public void comprobarPad()
        {

            if (VariablesGlobales.SeCapturaFirma)
            {
                //Iniciar PadFirmas
                try
                {
                    ExistePadFirmas = sigPlusNET1.TabletConnectQuery();
                    if (ExistePadFirmas) picPadFirma.Image = Resources.dspad1;
                    else picPadFirma.Image = Resources.dspad2;
                }
                catch (Exception)
                {
                    picPadFirma.Image = Resources.dspad2;
                }
            }
            else picPadFirma.Visible = false;

        }

        public void CargarDatosEditar()
        {
            string sql0 = string.Format("select P.PersonId,E.EnrolmentId, E.FKDocumentType, E.DocumentNumber, P.FirstName, P.SecondName, P.FirstLastName, P.SecondLastName, P.FKSexId, P.FKCountryId, P.FKNationalityId, P.DateOfBirth, P.FKCivilStatusId, P.FKStateCountryId, P.FKStateCityId, P.FKNeighborhoodId, P.Address, P.PersonalMovilPhone, P.HomePhone, P.PersonalEmail, P.FKPersonProfessionsId, P.FKEducationLevelId,P.WorkPhone, P.WorkAddress, P.WorkEmail, PP.Image, PS.Imagen as Firma from Persons P inner join Enrolments E on P.PersonId = E.FKPersonId left join PersonPictures PP on P.PersonId = PP.FKPersonId left join PersonSignatures PS on E.EnrolmentId = PS.FKEnrolmentId where p.PersonId ='{0}'", PersonaId);
            JArray post = new JArray();
            JObject ob1 = (JObject)JToken.FromObject(new JsonItem("PersonaRegistrada", sql0));
            post.Add(ob1);
           
            string pre = post.ToString().Substring(0, post.ToString().Length);
            string res = web.GetJson(VariablesGlobales.TOKEN, pre, VariablesGlobales.HOST_NAME);
            JArray items;
            dynamic dyarr = JArray.Parse(res)[0];

            if (dyarr.Id == "PersonaRegistrada")
            {
                items = dyarr.Obj;
                var lstPersonas = new List<ActualizarInformacionEN>();
                foreach (JObject p in items)
                {
                    var prs = new ActualizarInformacionEN();
                    prs.EnrolmentId = (long)p["EnrolmentId"];
                    prs.PersonId = int.Parse(p["PersonId"].ToString());
                    prs.TipoDocumento = p["FKDocumentType"].ToString();
                    prs.NumeroDocumento = p["DocumentNumber"].ToString();
                    prs.PrimerNombre = p["FirstName"].ToString();
                    prs.SegundoNombre = p["SecondName"].ToString();
                    prs.PrimerApellido = p["FirstLastName"].ToString();
                    prs.SegundoApellido = p["SecondLastName"].ToString();
                    prs.Sexo = p["FKSexId"].ToString();
                    prs.Pais = p["FKCountryId"].ToString();
                    prs.Nacionalidad = p["FKNationalityId"].ToString();
                    prs.FechaNacimiento = Convert.ToDateTime(p["DateOfBirth"].ToString());
                    prs.EstadoCivil = p["FKCivilStatusId"].ToString();
                    prs.Departamento = p["FKStateCountryId"].ToString();
                    prs.Municipio = p["FKStateCityId"].ToString();
                    prs.Colonia = p["FKNeighborhoodId"].ToString();
                    prs.DireccionPersonal = p["Address"].ToString();
                    prs.TelefonoCasa = p["PersonalMovilPhone"].ToString();
                    prs.Movil = p["HomePhone"].ToString();
                    prs.Correo = p["PersonalEmail"].ToString();
                    prs.Profesion = p["FKPersonProfessionsId"].ToString();
                    prs.NivelEducativo = p["FKEducationLevelId"].ToString();
                    prs.TelefonoTrabajo = p["WorkPhone"].ToString();
                    prs.DireccionTrabajo = p["WorkAddress"].ToString();
                    prs.CorreoTrabajo = p["WorkEmail"].ToString();
                    prs.Foto = p["Image"].ToString() != "" ? (byte[])p["Image"] : null;
                    prs.Firma = p["Firma"].ToString() != "" ? (byte[])p["Firma"] : null;
                    lstPersonas.Add(prs);
                }

                foreach (var item in lstPersonas)
                {
                    _EnrolmentId = item.EnrolmentId;
                    lueTipoDocumento.EditValue = Convert.ToInt64(item.TipoDocumento);
                    txtIdentificacion.Text = item.NumeroDocumento;
                    txtPrimerNombre.Text = item.PrimerNombre;
                    txtSegundoNombre.Text = item.SegundoNombre;
                    txtPrimerApellido.Text = item.PrimerApellido;
                    txtSegundoApellido.Text = item.SegundoApellido;
                    lueSexo.EditValue = Convert.ToInt64(item.Sexo);
                    luePaisNacimiento.EditValue = Convert.ToInt64(item.Pais);
                    lueNacionalidad.EditValue = Convert.ToInt64(item.Nacionalidad);
                    dtmFechaNacimiento.EditValue = item.FechaNacimiento;
                    lueEstadoCivil.EditValue = Convert.ToInt64(item.EstadoCivil);
                    lueDepartamentos.EditValue = Convert.ToInt64(item.Departamento);
                    lueMunicipios.EditValue = Convert.ToInt64(item.Municipio);
                    lueColonia.EditValue = Convert.ToInt64(item.Colonia);
                    meDireccionCompleta.Text = item.DireccionPersonal;
                    txtTelefono.Text = item.TelefonoCasa;
                    txtMovil.Text = item.Movil;
                    txtCorreo.Text = item.Correo;
                    lueProfesion.EditValue = Convert.ToInt64(item.Profesion);
                    lueNivelEducativo.EditValue = Convert.ToInt64(item.NivelEducativo);
                    txtTelefonoTrabajo.Text = item.TelefonoTrabajo;
                    meDireccionTrabajo.Text = item.DireccionTrabajo;
                    txtCorreoTrabajo.Text = item.CorreoTrabajo;
                    picFotoDB.Image = ExodusBcBase.Utilities.ByteArrayToImage(item.Foto);
                    picFirmaDB.Image = ExodusBcBase.Utilities.ByteArrayToImage(item.Firma);
                }

            }
        }

        private void sbtnSiguiente_Click(object sender, EventArgs e)
        {
            switch (tabEnrolamiento.SelectedPageIndex)
            {
                case 0: //Biograficos
                    {
                        if (ValidarTabBiograficos())
                        {
                            tabEnrolamiento.SelectNextPage();
                            if (picFotoDB.Image != null)
                            {
                                pnlFotoDB.Visible = true;
                            }
                            sbtnAnterior.Enabled = true;
                        }
                        break;
                    }
                case 1://Foto
                    {
                        if (ValidarTabFoto())
                        {
                            sigPlusNET1.SetTabletState(1);
                            tabEnrolamiento.SelectNextPage();
                        }
                        break;
                    }
                case 2://Firma
                    {

                        if (ValidarTabFirma())
                        {
                            CargarInformacionFinal();
                            sbtnSiguiente.Enabled = false;
                            sbtnAnterior.Focus();
                            btnGuardar.Enabled = true;
                            tabEnrolamiento.SelectNextPage();
                        }
                        break;
                    }
                case 3://Adjuntos
                    {
                        break;
                    }
            }
            SetTitulos();
        }

        private bool ValidarTabBiograficos()
        {
            if (lueTipoDocumento.EditValue == null)
            {
                XtraMessageBox.Show("Tipo de Documento esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                lueTipoDocumento.Select();
                return false;
            }

            if (txtIdentificacion.Text.Trim().Length == 0)
            {
                XtraMessageBox.Show("Número de Identificación esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtIdentificacion.Select();
                return false;
            }

            if (txtPrimerNombre.Text.Trim().Length == 0)
            {
                XtraMessageBox.Show("Primer Nombre esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtPrimerNombre.Select();
                return false;
            }

            if (txtPrimerApellido.Text.Trim().Length == 0)
            {
                XtraMessageBox.Show("Primer Apellido esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                txtPrimerApellido.Select();
                return false;
            }

            if (lueSexo.Text.Trim().Length == 0)
            {
                XtraMessageBox.Show("Sexo esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                lueSexo.Select();
                return false;
            }

            if (luePaisNacimiento.Text.Trim().Length == 0)
            {
                XtraMessageBox.Show("País de nacimiento esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                luePaisNacimiento.Select();
                return false;
            }

            if (lueNacionalidad.Text.Trim().Length == 0)
            {
                XtraMessageBox.Show("Nacionalidad esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                lueNacionalidad.Select();
                return false;
            }

            if (dtmFechaNacimiento.DateTime == DateTime.MinValue)
            {
                XtraMessageBox.Show("Fecha de nacimiento esta vacía, este campo es nesesario", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                dtmFechaNacimiento.Select();
                return false;
            }
            else
            {
                dtmFechaNacimiento.Properties.MaxValue = ExodusBcBase.Utilities.getFechaActual();
                if (VariablesGlobales.EDADMINIMA >= 0)
                {
                    string calculoEdad = ExodusBcBase.Utilities.getEdad(dtmFechaNacimiento.DateTime).ToString();
                    if (Convert.ToInt32(calculoEdad) < VariablesGlobales.EDADMINIMA)
                    {
                        XtraMessageBox.Show(string.Format("La edad permitida para enrolar es {0} años.", VariablesGlobales.EDADMINIMA), "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                        dtmFechaNacimiento.Select();
                        return false;
                    }
                }

                if (lueEstadoCivil.Text.Trim().Length == 0)
                {
                    XtraMessageBox.Show("Estado Civil esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    lueEstadoCivil.Select();
                    return false;
                }

                if (lueDepartamentos.Text.Trim().Length == 0)
                {
                    XtraMessageBox.Show("Departamento de Nacimiento esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    lueDepartamentos.Select();
                    return false;
                }

                if (lueMunicipios.Text.Trim().Length == 0)
                {
                    XtraMessageBox.Show("Municipio de Nacimiento esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    lueMunicipios.Select();
                    return false;
                }

                if (lueColonia.Text.Trim().Length == 0)
                {
                    XtraMessageBox.Show("Colonia esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    lueMunicipios.Select();
                    return false;
                }

                if (meDireccionCompleta.Text.Trim().Length == 0)
                {
                    XtraMessageBox.Show("Dirección particular esta vacía, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    meDireccionCompleta.Select();
                    return false;
                }

                if (txtTelefono.Text.Trim().Length == 0 && txtMovil.Text.Trim().Length == 0)
                {
                    XtraMessageBox.Show("Debe agregar al menos un número telefónico", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtTelefono.Select();
                    return false;
                }

                if (lueProfesion.Text.Trim().Length == 0)
                {
                    XtraMessageBox.Show("Profesión  esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    lueProfesion.Select();
                    return false;
                }

                if (lueNivelEducativo.Text.Trim().Length == 0)
                {
                    XtraMessageBox.Show("Nivel educativo esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    lueNivelEducativo.Select();
                    return false;
                }
                estadoBiograficos = true;
                return true;
            }
        }

        private bool ValidarTabFoto()
        {
            if (picFotoCapturada.Image == null & picFotoDB.EditValue == null) 
            {
                XtraMessageBox.Show("Foto esta vacía, esta imagen es requerida.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                sbtnIniciarCamara.Select();
                return false;
            }
            estadoFoto = true;
            return true;
        }

        private bool ValidarTabFirma()
        {
            if (picFirmaDB.Image == null)
            {
                if (sigPlusNET1.NumberOfTabletPoints() == 0 && !chknoPuedeFirmar.Checked)
                {
                    XtraMessageBox.Show("La firma esta vacia, esta campo es requerido.", "Información",
                                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
            }
            if (sigPlusNET1.NumberOfTabletPoints() > 0)
            {
                if (firmaCapturada == null)
                {
                    sigPlusNET1.SetImageXSize(500);
                    sigPlusNET1.SetImageYSize(150);
                    sigPlusNET1.SetJustifyMode(5);
                    var firma = sigPlusNET1.GetSigImage();
                    firmaCapturada = firma;
                }
            }

            return true;
        }

        void CargarInformacionFinal()
        {
            //BIOGRAFICOS
            lblTipoDocumento.Text = lueTipoDocumento.Text;
            lblIdentificacion.Text = txtIdentificacion.Text;

            lblNombres.Text = string.Format("{0} {1}", txtPrimerNombre.Text, txtSegundoNombre.Text);
            lblApellidos.Text = string.Format("{0} {1}", txtPrimerApellido.Text, txtSegundoApellido.Text);

            lblSexo.Text = lueSexo.Text;

            lblEstadoCivil.Text = lueEstadoCivil.Text;

            lblProfesionOficio.Text = lueProfesion.Text;

            if (txtCorreo.Text != String.Empty)
                lblCorreoElectronico.Text = txtCorreo.Text;
            else
                lblCorreoElectronico.Text = String.Empty;

            lblNacionalidad.Text = lueNacionalidad.Text;

            lblPaisNacimiento.Text = luePaisNacimiento.Text;
            lblNivelEducativo.Text = lueNivelEducativo.Text;

            lblNacimiento.Text = Convert.ToString(String.Format("{0:dd/MM/yyyy}", dtmFechaNacimiento.EditValue));

            lblDepartamento.Text = lueDepartamentos.Text;

            lblMunicipio.Text = lueMunicipios.Text;

            lblColoniaCasa.Text = lueColonia.Text;

            lblTelefono.Text = txtTelefono.Text;

            lblMovil.Text = txtMovil.Text;

            lblDireccionResidencia.Text = meDireccionCompleta.Text;

            if (picFotoDB.Image!=null && picTemporal.Image ==null)
            {
                picFotoFinal.Image = picFotoDB.Image;
            }
            else
            {
                picFotoFinal.Image = picFotoCapturada.Image;
            }
            if (firmaCapturada!= null)
                reviewSignature.Image = firmaCapturada;
            else
                reviewSignature.Image = picFirmaDB.Image;
           
        }

        void SetTitulos()
        {
            if (tabEnrolamiento.SelectedPageIndex == 0)
            {
                lblTituloAccion.Text = "Edición de Información Biográfica";
                lblDetalles.Text = "Datos Personales.";
                picNumeros.Image = Resources.Circulo;
            }
            else if (tabEnrolamiento.SelectedPageIndex == 1)
            {
                lblTituloAccion.Text = "Edición de Fotografía";
                lblDetalles.Text = "Captura aplicando foto vertical.";
                picNumeros.Image = Resources.Circulo2;
            }
          
            else if (tabEnrolamiento.SelectedPageIndex == 2)
            {
                lblTituloAccion.Text = "Edición de Firma";
                lblDetalles.Text = "Actualización de firma.";
                picNumeros.Image = Resources.Circulo3;
            }
            else if (tabEnrolamiento.SelectedPageIndex == 3)
            {
                lblTituloAccion.Text = "Revisión de Información";
                lblDetalles.Text = "Por favór revise los datos editados.";
                picNumeros.Image = Resources.Circulo4;
            }

        }

        private void sbtnBorrarFirma_Click(object sender, EventArgs e)
        {
            sigPlusNET1.ClearTablet();
            sigPlusNET1.BackgroundImage = null;
            sigPlusNET1.SetJustifyMode(0);
        }

        private void sbtnIniciarCamara_Click(object sender, EventArgs e)
        {
             WrapperCamara CamSDK = new WrapperCamara();
            CamSDK.SetNombreCamara(VariablesGlobales.NOMBRECAMARA);
            CamSDK.SetPictureBoxDestino(picTemporal);
            try
            {
                CamSDK.Inicializar();
                ValoresIcao = CamSDK.MensajeResultado;
                TotalErroresIcao = CamSDK.TotalErrores;
                if (picTemporal.Image != null)
                {
                    picFotoCapturada.Image = picTemporal.Image;
                    //picFotoDB.Image = picTemporal.Image;
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime fechaNac = Convert.ToDateTime(dtmFechaNacimiento.EditValue).Date;
                string FechaNacimientoFinal = fechaNac.ToString("yyyy-MM-dd");
                meDireccionCompleta.Text = RequestCertificate.VerificarCaracteresEspeciales(meDireccionCompleta.Text.Trim());
                
                string sql = string.Format("update P set P.FirstName ='{0}', P.SecondName ='{1}', P.FirstLastName='{2}', P.SecondLastName='{3}', P.FKSexId={4}, P.FKCountryId={5}, P.FKNationalityId={6}, P.DateOfBirth='{7}', P.FKCivilStatusId={8}, P.FKStateCountryId={9}, P.FKStateCityId={10}, P.FKNeighborhoodId={11}, P.Address='{12}', P.PersonalMovilPhone='{13}', P.HomePhone='{14}', P.PersonalEmail='{15}', P.FKPersonProfessionsId={16}, P.FKEducationLevelId={17}, P.WorkPhone='{18}', P.WorkAddress='{19}', P.WorkEmail='{20}', P.ModifyDate= GETDATE() from Persons P WHERE P.PersonId={21} ",
                    txtPrimerNombre.Text, txtSegundoNombre.Text, txtPrimerApellido.Text, txtSegundoApellido.Text, lueSexo.EditValue, luePaisNacimiento.EditValue, lueNacionalidad.EditValue, FechaNacimientoFinal, lueEstadoCivil.EditValue, lueDepartamentos.EditValue, lueMunicipios.EditValue, lueColonia.EditValue, meDireccionCompleta.Text, txtMovil.Text, txtTelefono.Text, txtCorreo.Text, lueProfesion.EditValue, lueNivelEducativo.EditValue, txtTelefonoTrabajo.Text, meDireccionTrabajo.Text, txtCorreoTrabajo.Text, PersonaId);

                string res = web.Insertar(VariablesGlobales.TOKEN, sql, VariablesGlobales.HOST_NAME);

                if (picTemporal.Image !=null) resFoto = guardarFotografía(ExodusBcBase.Utilities.ImageToBase64(picFotoCapturada.Image), PersonaId);
                if (firmaCapturada != null)
                    resFirma = guardarFirma(ExodusBcBase.Utilities.ImageToBase64(firmaCapturada), PersonaId, meObservacionFirma.Text, _EnrolmentId, VariablesGlobales.USER_ID);
            
                if (res == "\"\"" || resFoto || resFirma)
                {
                    XtraMessageBox.Show("Registro editado satisfactoriamente", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Limpiar();
                    frmMain2 changeValue = (frmMain2)System.Windows.Forms.Application.OpenForms["frmMain2"];
                    changeValue.OpenForms("frmEnrolamientoActualizacion");
                    Close();
                }
            }
            catch (Exception ex) { throw ex; }
          
        }

        public static bool guardarFotografía(string base64, int _PersonId)
        {
            try
            {
                if (base64 != string.Empty)
                   return web.InsertarFotografía(base64, VariablesGlobales.TOKEN, _PersonId);
                else return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool guardarFirma(string base64, int _PersonId, string Observacion, long EnrolmentId, int UserId)
        {
            try
            {
                if (base64 != string.Empty)
                    return web.InsertarFirma(base64, 1, Observacion, EnrolmentId, UserId, VariablesGlobales.TOKEN);
                else return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void Limpiar()
        {
           

            //Limpiar page captura
            txtIdentificacion.Text = "";
            lueTipoDocumento.EditValue = null;
            txtPrimerNombre.Text = "";
            txtSegundoNombre.Text = "";
            txtPrimerApellido.Text = "";
            txtSegundoApellido.Text = "";
            lueSexo.EditValue = null;
            luePaisNacimiento.EditValue = null;
            lueNacionalidad.EditValue = null;
            dtmFechaNacimiento.Text = "";
            lueSexo.EditValue = null;
            lueDepartamentos.EditValueChanged -= lueDepartamentos_EditValueChanged;
            lueDepartamentos.EditValue = null;
            lueMunicipios.EditValue = null;
            lueMunicipios.Properties.DataSource = null;
            lueMunicipios.EditValueChanged -= lueMunicipios_EditValueChanged;
            lueColonia.EditValue = null;
            meDireccionCompleta.Text = "";
            txtTelefono.Text = "";
            txtTelefonoTrabajo.Text = "";
            txtCorreo.Text = "";
            txtCorreoTrabajo.Text = "";
            lueProfesion.EditValue = null;
            lueNivelEducativo.EditValue = null;
            meDireccionTrabajo.Text = "";
            txtCorreoTrabajo.Text = "";

            //Limpiar page Foto
            picFotoCapturada.Image = null;

            //Limpiar page Firma
            sigPlusNET1.ClearTablet();
            sigPlusNET1.BackgroundImage = null;
            sigPlusNET1.SetJustifyMode(0);
            firmaCapturada = null;
            meObservacionFirma.Text = "";
            chknoPuedeFirmar.Checked = false;

            //Limpiar page Review
            lblTipoDocumento.Text = "";
            lblIdentificacion.Text = "";

            lblNombres.Text = "";
            lblApellidos.Text = "";
            lblSexo.Text = "";
            lblEstadoCivil.Text = "";
            lblProfesionOficio.Text = "";
            lblCorreoElectronico.Text = "";
            lblNacionalidad.Text = "";
            lblPaisNacimiento.Text = "";
            lblNivelEducativo.Text = "";
            lblNacimiento.Text = "";
            lblDepartamento.Text = "";
            lblMunicipio.Text = "";
            lblColoniaCasa.Text = "";
            lblTelefono.Text = "";
            lblMovil.Text = "";
            lblDireccionResidencia.Text = "";
         
            picFotoCapturada.Image = null;
            picFotoFinal.Image = null;

            frmEnrolamientoLectora.objLectura = null;
        }

        private void lueDepartamentos_EditValueChanged(object sender, EventArgs e)
        {
            if (lueDepartamentos.EditValue != null)
            {

                ExodusBcBase.Helper.Combobox.setComboStateCity(lueMunicipios, Convert.ToInt32(lueDepartamentos.EditValue));
                lueMunicipios.EditValueChanged += lueMunicipios_EditValueChanged;
            }
        }

        private void lueMunicipios_EditValueChanged(object sender, EventArgs e)
        {
            if (lueMunicipios.EditValue != null && lueDepartamentos.EditValue != null)
            {
                ExodusBcBase.Helper.Combobox.setComboNeighborhood(lueColonia, Convert.ToInt32(lueMunicipios.EditValue));
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            frmMain2 changeValue = (frmMain2)System.Windows.Forms.Application.OpenForms["frmMain2"];
            changeValue.OpenForms("frmEnrolamientoActualizacion");
            Limpiar();
            Close();
        }

        private void sbtnAnterior_Click(object sender, EventArgs e)
        {
            btnGuardar.Enabled = false;

            if (tabEnrolamiento.SelectedPageIndex > 0)
            {
                tabEnrolamiento.SelectPrevPage();
                SetTitulos();
            }
            else if (tabEnrolamiento.SelectedPageIndex == 0)
                sbtnAnterior.Enabled = false;

            if (sbtnSiguiente.Enabled == false)
                sbtnSiguiente.Enabled = true;
        }

        private void lueTipoDocumento_EditValueChanged(object sender, EventArgs e)
        {
            txtIdentificacion.Text = null;
            if (Convert.ToInt32(lueTipoDocumento.EditValue) == (int)DOCUMENTTYPELN.DocumentType.IDENTIDAD)
            {
                txtIdentificacion.Properties.Mask.EditMask = "0000-0000-00000";
                txtIdentificacion.Properties.Mask.MaskType = MaskType.Simple;
            }
            else if (Convert.ToInt32(lueTipoDocumento.EditValue) == (int)DOCUMENTTYPELN.DocumentType.RTN)
            {
                txtIdentificacion.Properties.Mask.EditMask = "0000-0000-000000"; 
                txtIdentificacion.Properties.Mask.MaskType = MaskType.Simple;
            }
            else
            {
                txtIdentificacion.Properties.Mask.MaskType = MaskType.None;
                txtIdentificacion.Properties.CharacterCasing = CharacterCasing.Upper;
            }
            txtIdentificacion.Focus();
        }

        private void btnRotar_Click(object sender, EventArgs e)
        {
            if (picFotoDB != null)
            {
                int rotacion = 0;
                rotacion += 1;

                if (rotacion == 1)
                {
                    Image image = picFotoDB.Image.Clone() as Image;
                    image.RotateFlip(RotateFlipType.Rotate90FlipNone);
                    picFotoDB.Image.Dispose();
                    picFotoDB.Image = image;
                }
                else if (rotacion == 2)
                {
                    Image image = picFotoDB.Image.Clone() as Image;
                    image.RotateFlip(RotateFlipType.Rotate270FlipNone);
                    picFotoDB.Image.Dispose();
                    picFotoDB.Image = image;
                }
                else if (rotacion == 3)
                {
                    Image image = picFotoDB.Image.Clone() as Image;
                    image.RotateFlip(RotateFlipType.Rotate180FlipNone);
                    picFotoDB.Image.Dispose();
                    picFotoDB.Image = image;
                }
                else if (rotacion == 4)
                {
                    Image image = picFotoDB.Image.Clone() as Image;
                    image.RotateFlip(RotateFlipType.Rotate90FlipNone);
                    picFotoDB.Image.Dispose();
                    picFotoDB.Image = image;
                    rotacion = 0;
                }

                picTemporal.Image = picFotoDB.Image;

            }
        }

        private void labelControl17_Click(object sender, EventArgs e)
        {

        }
    }
}