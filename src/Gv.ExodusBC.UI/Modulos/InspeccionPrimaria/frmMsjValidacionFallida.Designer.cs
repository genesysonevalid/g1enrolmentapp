﻿namespace Gv.ExodusBc.UI.Modulos.InspeccionPrimaria
{
    partial class frmMsjValidacionFallida
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ptbIcono = new System.Windows.Forms.PictureBox();
            this.lblRechazaVerificacion = new System.Windows.Forms.Label();
            this.lblMensaje = new System.Windows.Forms.Label();
            this.lblMensaje2 = new System.Windows.Forms.Label();
            this.btnAceptar = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.ptbIcono)).BeginInit();
            this.SuspendLayout();
            // 
            // ptbIcono
            // 
            this.ptbIcono.BackColor = System.Drawing.Color.Transparent;
            this.ptbIcono.Image = global::Gv.ExodusBc.UI.Properties.Resources.Verificacion_Rechazada;
            this.ptbIcono.Location = new System.Drawing.Point(38, 48);
            this.ptbIcono.Name = "ptbIcono";
            this.ptbIcono.Size = new System.Drawing.Size(85, 85);
            this.ptbIcono.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbIcono.TabIndex = 10;
            this.ptbIcono.TabStop = false;
            // 
            // lblRechazaVerificacion
            // 
            this.lblRechazaVerificacion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.lblRechazaVerificacion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblRechazaVerificacion.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblRechazaVerificacion.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRechazaVerificacion.ForeColor = System.Drawing.Color.White;
            this.lblRechazaVerificacion.Location = new System.Drawing.Point(0, 223);
            this.lblRechazaVerificacion.Name = "lblRechazaVerificacion";
            this.lblRechazaVerificacion.Size = new System.Drawing.Size(572, 39);
            this.lblRechazaVerificacion.TabIndex = 11;
            this.lblRechazaVerificacion.Text = "VERICACION CON  4 DEDOS DE LA MANO DERECHA";
            this.lblRechazaVerificacion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMensaje
            // 
            this.lblMensaje.BackColor = System.Drawing.Color.Transparent;
            this.lblMensaje.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMensaje.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.lblMensaje.Location = new System.Drawing.Point(133, 39);
            this.lblMensaje.Name = "lblMensaje";
            this.lblMensaje.Size = new System.Drawing.Size(398, 45);
            this.lblMensaje.TabIndex = 9;
            this.lblMensaje.Text = "Verificación Fallida";
            this.lblMensaje.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMensaje2
            // 
            this.lblMensaje2.BackColor = System.Drawing.Color.Transparent;
            this.lblMensaje2.Font = new System.Drawing.Font("Century Gothic", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMensaje2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblMensaje2.Location = new System.Drawing.Point(129, 92);
            this.lblMensaje2.Name = "lblMensaje2";
            this.lblMensaje2.Size = new System.Drawing.Size(432, 59);
            this.lblMensaje2.TabIndex = 12;
            this.lblMensaje2.Text = "Se intentará nuevamente usando \r\nla mano izquierda";
            this.lblMensaje2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnAceptar
            // 
            this.btnAceptar.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.btnAceptar.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnAceptar.Appearance.Options.UseFont = true;
            this.btnAceptar.Appearance.Options.UseForeColor = true;
            this.btnAceptar.AppearanceHovered.BackColor = System.Drawing.Color.Silver;
            this.btnAceptar.AppearanceHovered.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnAceptar.AppearanceHovered.Options.UseBackColor = true;
            this.btnAceptar.AppearanceHovered.Options.UseFont = true;
            this.btnAceptar.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnAceptar.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_enter;
            this.btnAceptar.Location = new System.Drawing.Point(224, 161);
            this.btnAceptar.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnAceptar.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(124, 44);
            this.btnAceptar.TabIndex = 0;
            this.btnAceptar.Text = "Aceptar";
            this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // frmMsjValidacionFallida
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(572, 262);
            this.Controls.Add(this.btnAceptar);
            this.Controls.Add(this.lblMensaje2);
            this.Controls.Add(this.ptbIcono);
            this.Controls.Add(this.lblRechazaVerificacion);
            this.Controls.Add(this.lblMensaje);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmMsjValidacionFallida";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Validación Fallida";
            ((System.ComponentModel.ISupportInitialize)(this.ptbIcono)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.PictureBox ptbIcono;
        internal System.Windows.Forms.Label lblRechazaVerificacion;
        internal System.Windows.Forms.Label lblMensaje;
        internal System.Windows.Forms.Label lblMensaje2;
        private DevExpress.XtraEditors.SimpleButton btnAceptar;
    }
}