﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Newtonsoft.Json.Linq;
using Gv.ExodusBc.EN;
using Gv.ExodusBc.UI.WebReference;
using DevExpress.Utils;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using Gv.Utilidades;
using Gv.ExodusBc.LN;
using DevExpress.XtraEditors.Mask;
using static Gv.ExodusBc.UI.ExodusBcBase;

namespace Gv.ExodusBc.UI.Modulos.InspeccionPrimaria
{
    public partial class frmBusquedaPersonas : DevExpress.XtraEditors.XtraForm
    {
        public int coincidenciaRegistro= 0;
        public EN.RegistryPersons _registryPerson = new EN.RegistryPersons();
        private static ExodusBcService web = new ExodusBcService();
        public static Form frmCert;
        public string NoIdentidad = "";
        public Int64 DocumentoTipo = 0;

        public object ft { get; private set; }

        public frmBusquedaPersonas()
        {
            InitializeComponent();
            Init();
        }

        private void frmBusquedaPersonas_Load(object sender, EventArgs e)
        {
            if (lueDocumentoTipo.Text.Length > 0)
            {
                txtIdentificacion.Focus();
                txtIdentificacion.Select();
            }
            else lueDocumentoTipo.Focus();
        }
        private void Init()
        {
            //Llenar combos
            ExodusBcBase.Helper.Combobox.setComboTipoDocumento(lueDocumentoTipo);
            lueDocumentoTipo.EditValue = VariablesGlobales.DOC_TYPE_DEFAULT;
        }

        private void btnBuscarPersona_Click(object sender, EventArgs e)
        {
            try
            {
                if (lueDocumentoTipo.EditValue == null)
                {
                    XtraMessageBox.Show("Seleccione el tipo de documento.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    lueDocumentoTipo.Focus();
                }
                else if (txtIdentificacion.Text == string.Empty)
                {
                    XtraMessageBox.Show("Ingrese el número de identificación", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtIdentificacion.Focus();
                }
                else
                {
                    string whr = "";
                    string sql0 = string.Format("Select P.PersonId, DT.Description as TipoDocumento, E.DocumentNumber as NumeroDocumento, Concat(P.FirstName,' ', P.SecondName) as Nombres, Concat(P.FirstLastName,' ',P.SecondLastName) as Apellidos, P.DateOfBirth as FechaNacimiento, (Select top 1 PP.Image from PersonPictures PP LEFT JOIN Persons on Persons.PersonId = PP.FKPersonId where PP.FKPersonId=P.PersonId order by PP.CreationDate desc) as Foto from Persons P inner join Enrolments E on p.PersonId=E.FKPersonId inner join DocumentType DT on E.FKDocumentType=DT.DocumentTypeId where e.DocumentNumber ='{0}' and E.FKDocumentType={1}", txtIdentificacion.Text, lueDocumentoTipo.EditValue);
                    
                    if (txtPrimerNombre.Text != string.Empty) whr += " and p.FirstName ='" + txtPrimerNombre.Text + "' ";
                    if (txtPrimerApellido.Text != string.Empty) whr += " and p.FirstLastName='" + txtPrimerApellido.Text + "' ";
                    if (dtmFechaNacimiento.EditValue != null) whr += " and p.DateOfBirth= '" + dtmFechaNacimiento.Text + "' ";

                    JArray post = new JArray();
                    JObject ob1 = (JObject)JToken.FromObject(new JsonItem("PersonaRegistrada", string.Concat(sql0,whr)));
                    post.Add(ob1);

                    ssForm.ShowWaitForm();
                    string pre = post.ToString().Substring(0, post.ToString().Length);
                    string res = web.GetJson(VariablesGlobales.TOKEN, pre, VariablesGlobales.HOST_NAME);
                    JArray items;
                    dynamic dyarr = JArray.Parse(res)[0];

                    if (dyarr.Id == "PersonaRegistrada")
                    {
                        items = dyarr.Obj;
                        if (items.Count > 0)
                        {
                            var lstPersonas = new List<gridRegistroPersonas>();
                            foreach (JObject p in items)
                            {
                                var prs = new gridRegistroPersonas();
                                prs.PersonId = Convert.ToInt32(p["PersonId"]);
                                prs.Foto = p["Foto"].ToString() != "" ? (byte[])p["Foto"] : null;
                                prs.TipoDocumento = p["TipoDocumento"].ToString();
                                prs.NumeroDocumento = p["NumeroDocumento"].ToString();
                                prs.Nombres = p["Nombres"].ToString();
                                prs.Apellidos = p["Apellidos"].ToString();
                                prs.FechaNacimiento = Convert.ToDateTime(p["FechaNacimiento"].ToString());
                                lstPersonas.Add(prs);
                            }
                            gcPersonas.DataSource = lstPersonas.ToList();
                            
                        }
                        else
                        {
                            if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                            frmInsPrimariaEntrada.NoIdentidad = txtIdentificacion.Text;
                            frmInsPrimariaEntrada.TipoDocumento = Convert.ToInt64(lueDocumentoTipo.EditValue);
                            this.DialogResult = DialogResult.OK;
                            Close();
                        }
                    }
                    if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                }
            }
            catch (Exception ex)
            {
                if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm(); 
                XtraMessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmBusquedaPersonas::btnBuscarPersona_Click", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                this.DialogResult = DialogResult.Abort;
            }
           
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            lueDocumentoTipo.EditValue = null;
            txtIdentificacion.Text = "";
            txtPrimerApellido.Text = "";
            txtPrimerNombre.Text = "";
            dtmFechaNacimiento.Text = "";
            gcPersonas.DataSource = null;
        }

        private void btnLeerIdentidad_Click(object sender, EventArgs e)
        {
            frmEnrolamientoLectora frmLector = new frmEnrolamientoLectora();
            frmLector.ShowDialog();

            if (frmEnrolamientoLectora.LecturaOK)
            {
              
                txtIdentificacion.Text = frmEnrolamientoLectora.objLectura.NoIdentidad;
                lueDocumentoTipo.EditValue = Convert.ToInt64(1);
                btnBuscarPersona_Click(sender, e);
            }

        }
 
       private void ribeVer_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            try
            {
              
                frmCert = Application.OpenForms.OfType<Form>().Where(c => c.Name == "frmEnrolamientoActualizacion").SingleOrDefault();
                if (frmCert == null)
                {
                    frmEnrolamientoActualizacion frm1 = new frmEnrolamientoActualizacion();
                    frmMain2.pnlActualizacionBienvenida.Visible = false;
                    frm1.TopLevel = false;
                    frm1.Dock = DockStyle.Fill;
                    frm1.Parent = MdiParent;
                    frmMain2.pnlActualizacion.Controls.Add(frm1);
                    var IdPerson = gvPersonas.GetFocusedRowCellValue("PersonId").ToString();
                    frm1.PersonaId = Convert.ToInt32(IdPerson);
                    frm1.Show();

                }

                this.Close();
                //frm1.Show();
            }
            catch (Exception ex)
            {

                XtraMessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmBusquedaPersonas::ribeVer_ButtonClick", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
           
            
        }

        private void frmBusquedaPersonas_FormClosing(object sender, FormClosingEventArgs e)
        {
          
        }

        private void frmBusquedaPersonas_FormClosed(object sender, FormClosedEventArgs e)
        {
            //coincidenciaRegistro = -1;
            this.Close();
        }

        private void lueDocumentoTipo_EditValueChanged(object sender, EventArgs e)
        {
            txtIdentificacion.Text = null;
            if (Convert.ToInt32(lueDocumentoTipo.EditValue) == (int)DOCUMENTTYPELN.DocumentType.IDENTIDAD)
            {
                txtIdentificacion.Properties.Mask.EditMask = "0000-0000-00000";
                txtIdentificacion.Properties.Mask.MaskType = MaskType.Simple;
            }
            else if (Convert.ToInt32(lueDocumentoTipo.EditValue) == (int)DOCUMENTTYPELN.DocumentType.RTN)
            {
                txtIdentificacion.Properties.Mask.EditMask = "0000-0000-000000";
                txtIdentificacion.Properties.Mask.MaskType = MaskType.Simple;
            }
            else 
            {
                txtIdentificacion.Properties.Mask.MaskType = MaskType.None;
                txtIdentificacion.Properties.CharacterCasing = CharacterCasing.Upper;
            }
            txtIdentificacion.Focus();
        }
    }
}
