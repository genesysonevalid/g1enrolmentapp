﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Newtonsoft.Json.Linq;
using Gv.ExodusBc.EN;
using System.IO;
using Gv.ExodusBc.UI.WebReference;

namespace Gv.ExodusBc.UI.Modulos.InspeccionPrimaria
{
    public partial class frmEnrolamientoVisualizarHit : DevExpress.XtraEditors.XtraForm
    {
        private static ExodusBcService web = new ExodusBcService();
        public int PersonaId { get; set; }
        public string NoIdentidad { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Sexo { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public string Nacionalidad { get; set; }

        public Image Foto { get; set; }

        public frmEnrolamientoVisualizarHit()
        {
            InitializeComponent();
        }

        private void frmEnrolamientoVisualizarHit_Load(object sender, EventArgs e)
        {
            string PersonaIdHit = string.Empty;
            this.lblIdentificacionActivo.Text = this.NoIdentidad;
            this.lblNombresActivo.Text = this.Nombre;
            this.lblApellidosActivo.Text = this.Apellido;
            this.lblSexoActivo.Text = this.Sexo;
            this.lblNacimientoActivo.Text = this.FechaNacimiento.ToString("dd/MM/yyyy");
            this.lblNacionalidadActivo.Text = this.Nacionalidad;
            this.picRegistroActivo.Image = this.Foto;

            //JArray _personaRegistrada = ExodusBcBase.SearchPersonas.getPersonabyPersonaId(this.PersonaId, VariablesGlobales.HOST_NAME);
            string sql0 = string.Format("Select E.DocumentNumber, P.PersonId, concat (P.FirstName, P.SecondName) as Nombres, concat (P.FirstLastName, P.SecondLastName) as Apellidos, C.Name as Sexo, N.Name as Nacionalidad, P.DateOfBirth from Persons P inner join Enrolments E on P.PersonId=E.FKPersonId inner join Catalogs C on P.FKSexId=C.CatalogsId inner join Nationality N on P.FKNationalityId= N.NationalityId  where E.EnrolmentId={0}", PersonaId);
            JArray post = new JArray();
            JObject ob1 = (JObject)JToken.FromObject(new JsonItem("PersonaRegistrada", sql0));
            post.Add(ob1);
            string pre = post.ToString().Substring(0, post.ToString().Length);
            string res = web.GetJson(VariablesGlobales.TOKEN, pre, VariablesGlobales.HOST_NAME);
           
                dynamic dyarr = JArray.Parse(res)[0];
                if (dyarr.Id == "PersonaRegistrada")
                {

                try
                {
                    JArray items = dyarr.Obj;
                    var data = items.Select(p => new Hit
                    {
                        DocumentNumber = p["DocumentNumber"].ToString(),
                        Nombres = p["Nombres"].ToString(),
                        Apellidos = p["Apellidos"].ToString(),
                        Sexo = p["Sexo"].ToString(),
                        Nacionalidad = p["Nacionalidad"].ToString(),
                        DateOfBirth = Convert.ToDateTime(p["DateOfBirth"].ToString()),
                        PersonId = p["PersonId"].ToString(),
                    }).ToList();

                    foreach (var item in data)
                    {
                        this.lblIdentificacionExistente.Text = item.DocumentNumber;
                        this.lblNombresExistente.Text = item.Nombres;
                        this.lblApellidosExistente.Text = item.Apellidos;
                        this.lblSexoExistente.Text = item.Sexo;
                        this.lblNacionalidadExistente.Text = item.Nacionalidad;
                        this.lblNacimientoExistente.Text = Convert.ToString(item.DateOfBirth);
                        PersonaIdHit = item.PersonId;
                    }

                    string sqlFotografia = string.Format("Select top 1 PP.Image from Persons P inner join PersonPictures PP on P.PersonId= PP.FKPersonId where P.PersonId={0} order by PP.CreationDate desc", Convert.ToInt32(PersonaIdHit));
                    JArray postFotografia = new JArray();
                    JObject ob1Fotografia = (JObject)JToken.FromObject(new JsonItem("PersonaFotografia", sqlFotografia));
                    postFotografia.Add(ob1Fotografia);
                    string preFotografia = postFotografia.ToString().Substring(0, postFotografia.ToString().Length);
                    string resFotografia = web.GetJson(VariablesGlobales.TOKEN, preFotografia, VariablesGlobales.HOST_NAME);
                    dynamic dyarrFotografia = JArray.Parse(resFotografia)[0];
                    if (dyarrFotografia.Id == "PersonaFotografia")
                    {
                        JArray itemsFotografia = dyarrFotografia.Obj;
                        var datafoto = itemsFotografia.Select(p => new Fotografia
                        {
                            Foto = (byte[])p["Image"],
                        });
                        foreach (var item in datafoto)
                        {
                            byte[] fotografia = item.Foto;
                            MemoryStream ms = new MemoryStream(item.Foto, 0, item.Foto.Length);
                            ms.Write(item.Foto, 0, item.Foto.Length);
                            Image img = Image.FromStream(ms, true);
                            picRegistroExistente.Image = img;
                        }

           
                    }
                }
                catch (Exception)
                {

                    throw;
                }
                
                  

                }

        }
        public class Hit
        {
            public string DocumentNumber { get; set; }
            public string Nombres { get; set; }
            public string Apellidos { get; set; }
            public string Sexo { get; set; }
            public string Nacionalidad { get; set; }
            public DateTime DateOfBirth { get; set; }
            public string PersonId { get; set; }
        }
        public class Fotografia
        {
            public byte[] Foto { get; set; }
        }
            private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}