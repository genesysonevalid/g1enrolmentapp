﻿namespace Gv.ExodusBc.UI.Modulos.InspeccionPrimaria
{
    partial class frmEnrolamientoActualizacion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEnrolamientoActualizacion));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.lblNombreFormulario = new DevExpress.XtraEditors.LabelControl();
            this.btnSalir = new DevExpress.XtraEditors.SimpleButton();
            this.lblTituloAccion = new DevExpress.XtraEditors.LabelControl();
            this.sbtnSiguiente = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnAnterior = new DevExpress.XtraEditors.SimpleButton();
            this.lblDetalles = new DevExpress.XtraEditors.LabelControl();
            this.picNumeros = new DevExpress.XtraEditors.PictureEdit();
            this.picManoDerecha = new DevExpress.XtraEditors.PictureEdit();
            this.picManoIzquierda = new DevExpress.XtraEditors.PictureEdit();
            this.picIndiceDerecho = new DevExpress.XtraEditors.PictureEdit();
            this.picPulgarDerecho = new DevExpress.XtraEditors.PictureEdit();
            this.picMedioDerecho = new DevExpress.XtraEditors.PictureEdit();
            this.picAnularDerecho = new DevExpress.XtraEditors.PictureEdit();
            this.picAnularIzquierdo = new DevExpress.XtraEditors.PictureEdit();
            this.picMeniequeIzquierdo = new DevExpress.XtraEditors.PictureEdit();
            this.picMedioIzquierdo = new DevExpress.XtraEditors.PictureEdit();
            this.picIndiceIzquierdo = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.tabEnrolamiento = new DevExpress.XtraBars.Navigation.NavigationFrame();
            this.tabBiograficos = new DevExpress.XtraBars.Navigation.NavigationPage();
            this.labelControl61 = new DevExpress.XtraEditors.LabelControl();
            this.label22 = new System.Windows.Forms.Label();
            this.labelControl57 = new DevExpress.XtraEditors.LabelControl();
            this.label15 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl69 = new DevExpress.XtraEditors.LabelControl();
            this.label2 = new System.Windows.Forms.Label();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl62 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl59 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl60 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl58 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl54 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl53 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl37 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.btnTest = new DevExpress.XtraEditors.SimpleButton();
            this.txtCorreoTrabajo = new DevExpress.XtraEditors.TextEdit();
            this.lueColonia = new DevExpress.XtraEditors.LookUpEdit();
            this.lueMunicipios = new DevExpress.XtraEditors.LookUpEdit();
            this.lueDepartamentos = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl42 = new DevExpress.XtraEditors.LabelControl();
            this.label28 = new System.Windows.Forms.Label();
            this.meDireccionTrabajo = new DevExpress.XtraEditors.MemoEdit();
            this.txtTelefonoTrabajo = new DevExpress.XtraEditors.TextEdit();
            this.lueProfesion = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.label29 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lueNivelEducativo = new DevExpress.XtraEditors.LookUpEdit();
            this.luePaisNacimiento = new DevExpress.XtraEditors.LookUpEdit();
            this.meDireccionCompleta = new DevExpress.XtraEditors.MemoEdit();
            this.txtMovil = new DevExpress.XtraEditors.TextEdit();
            this.txtTelefono = new DevExpress.XtraEditors.TextEdit();
            this.lueSexo = new DevExpress.XtraEditors.LookUpEdit();
            this.dtmFechaNacimiento = new DevExpress.XtraEditors.DateEdit();
            this.txtCorreo = new DevExpress.XtraEditors.TextEdit();
            this.lueNacionalidad = new DevExpress.XtraEditors.LookUpEdit();
            this.lueEstadoCivil = new DevExpress.XtraEditors.LookUpEdit();
            this.lueTipoDocumento = new DevExpress.XtraEditors.LookUpEdit();
            this.txtIdentificacion = new DevExpress.XtraEditors.TextEdit();
            this.txtPrimerNombre = new DevExpress.XtraEditors.TextEdit();
            this.txtSegundoNombre = new DevExpress.XtraEditors.TextEdit();
            this.txtPrimerApellido = new DevExpress.XtraEditors.TextEdit();
            this.txtSegundoApellido = new DevExpress.XtraEditors.TextEdit();
            this.navigationPage1 = new DevExpress.XtraBars.Navigation.NavigationPage();
            this.btnRotar = new DevExpress.XtraEditors.SimpleButton();
            this.pnlFotoDB = new DevExpress.XtraEditors.PanelControl();
            this.picFotoDB = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.picFotoCapturada = new DevExpress.XtraEditors.PictureEdit();
            this.sbtnIniciarCamara = new DevExpress.XtraEditors.SimpleButton();
            this.navigationPage2 = new DevExpress.XtraBars.Navigation.NavigationPage();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.lblDepartamento = new DevExpress.XtraEditors.LabelControl();
            this.labelControl47 = new DevExpress.XtraEditors.LabelControl();
            this.lblTipoDocumento = new DevExpress.XtraEditors.LabelControl();
            this.labelControl44 = new DevExpress.XtraEditors.LabelControl();
            this.picFotoFinal = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl41 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl63 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl56 = new DevExpress.XtraEditors.LabelControl();
            this.reviewSignature = new DevExpress.XtraEditors.PictureEdit();
            this.lblNivelEducativo = new DevExpress.XtraEditors.LabelControl();
            this.labelControl135 = new DevExpress.XtraEditors.LabelControl();
            this.lblProfesionOficio = new DevExpress.XtraEditors.LabelControl();
            this.labelControl100 = new DevExpress.XtraEditors.LabelControl();
            this.lblPaisNacimiento = new DevExpress.XtraEditors.LabelControl();
            this.labelControl132 = new DevExpress.XtraEditors.LabelControl();
            this.lblDireccionResidencia = new DevExpress.XtraEditors.LabelControl();
            this.lblMovil = new DevExpress.XtraEditors.LabelControl();
            this.labelControl110 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl108 = new DevExpress.XtraEditors.LabelControl();
            this.lblTelefono = new DevExpress.XtraEditors.LabelControl();
            this.lblColoniaCasa = new DevExpress.XtraEditors.LabelControl();
            this.labelControl104 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl103 = new DevExpress.XtraEditors.LabelControl();
            this.lblCorreoElectronico = new DevExpress.XtraEditors.LabelControl();
            this.labelControl101 = new DevExpress.XtraEditors.LabelControl();
            this.lblNacionalidad = new DevExpress.XtraEditors.LabelControl();
            this.lblNacimiento = new DevExpress.XtraEditors.LabelControl();
            this.lblSexo = new DevExpress.XtraEditors.LabelControl();
            this.lblIdentificacion = new DevExpress.XtraEditors.LabelControl();
            this.lblEstadoCivil = new DevExpress.XtraEditors.LabelControl();
            this.lblMunicipio = new DevExpress.XtraEditors.LabelControl();
            this.lblApellidos = new DevExpress.XtraEditors.LabelControl();
            this.lblNombres = new DevExpress.XtraEditors.LabelControl();
            this.labelControl49 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl50 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl51 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl52 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl45 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl46 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl48 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl55 = new DevExpress.XtraEditors.LabelControl();
            this.navigationPage5 = new DevExpress.XtraBars.Navigation.NavigationPage();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.picFirmaDB = new DevExpress.XtraEditors.PictureEdit();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.meObservacionFirma = new DevExpress.XtraEditors.MemoEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.labelControl65 = new DevExpress.XtraEditors.LabelControl();
            this.sbtnBorrarFirma = new DevExpress.XtraEditors.SimpleButton();
            this.chknoPuedeFirmar = new DevExpress.XtraEditors.CheckEdit();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.sigPlusNET1 = new Topaz.SigPlusNET();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.picPadFirma = new DevExpress.XtraEditors.PictureEdit();
            this.picCamara = new DevExpress.XtraEditors.PictureEdit();
            this.btnGuardar = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picNumeros.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picManoDerecha.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picManoIzquierda.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picIndiceDerecho.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPulgarDerecho.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMedioDerecho.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAnularDerecho.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAnularIzquierdo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMeniequeIzquierdo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMedioIzquierdo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picIndiceIzquierdo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabEnrolamiento)).BeginInit();
            this.tabEnrolamiento.SuspendLayout();
            this.tabBiograficos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCorreoTrabajo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueColonia.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueMunicipios.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueDepartamentos.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meDireccionTrabajo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTelefonoTrabajo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueProfesion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueNivelEducativo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luePaisNacimiento.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meDireccionCompleta.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMovil.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTelefono.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueSexo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtmFechaNacimiento.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtmFechaNacimiento.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCorreo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueNacionalidad.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueEstadoCivil.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTipoDocumento.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdentificacion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrimerNombre.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSegundoNombre.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrimerApellido.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSegundoApellido.Properties)).BeginInit();
            this.navigationPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlFotoDB)).BeginInit();
            this.pnlFotoDB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picFotoDB.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFotoCapturada.Properties)).BeginInit();
            this.navigationPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picFotoFinal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reviewSignature.Properties)).BeginInit();
            this.navigationPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picFirmaDB.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.meObservacionFirma.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chknoPuedeFirmar.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picPadFirma.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCamara.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.panelControl1.Appearance.Options.UseBackColor = true;
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.lblNombreFormulario);
            this.panelControl1.Controls.Add(this.btnSalir);
            this.panelControl1.Controls.Add(this.lblTituloAccion);
            this.panelControl1.Controls.Add(this.sbtnSiguiente);
            this.panelControl1.Controls.Add(this.sbtnAnterior);
            this.panelControl1.Controls.Add(this.lblDetalles);
            this.panelControl1.Controls.Add(this.picNumeros);
            this.panelControl1.Location = new System.Drawing.Point(0, -1);
            this.panelControl1.LookAndFeel.SkinName = "Blue";
            this.panelControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelControl1.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1366, 59);
            this.panelControl1.TabIndex = 117;
            // 
            // lblNombreFormulario
            // 
            this.lblNombreFormulario.Appearance.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombreFormulario.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lblNombreFormulario.Appearance.Options.UseFont = true;
            this.lblNombreFormulario.Appearance.Options.UseForeColor = true;
            this.lblNombreFormulario.Appearance.Options.UseTextOptions = true;
            this.lblNombreFormulario.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblNombreFormulario.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblNombreFormulario.Location = new System.Drawing.Point(459, 7);
            this.lblNombreFormulario.Margin = new System.Windows.Forms.Padding(2);
            this.lblNombreFormulario.Name = "lblNombreFormulario";
            this.lblNombreFormulario.Size = new System.Drawing.Size(536, 45);
            this.lblNombreFormulario.TabIndex = 72;
            this.lblNombreFormulario.Text = "ACTUALIZACIÓN DE INFORMACIÓN";
            // 
            // btnSalir
            // 
            this.btnSalir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSalir.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.btnSalir.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalir.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.btnSalir.Appearance.Options.UseBackColor = true;
            this.btnSalir.Appearance.Options.UseFont = true;
            this.btnSalir.Appearance.Options.UseForeColor = true;
            this.btnSalir.Appearance.Options.UseTextOptions = true;
            this.btnSalir.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnSalir.AppearanceHovered.BackColor = System.Drawing.Color.Crimson;
            this.btnSalir.AppearanceHovered.Options.UseBackColor = true;
            this.btnSalir.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btnSalir.Location = new System.Drawing.Point(1315, 2);
            this.btnSalir.Margin = new System.Windows.Forms.Padding(2);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(50, 27);
            this.btnSalir.TabIndex = 71;
            this.btnSalir.Text = "X";
            this.btnSalir.ToolTip = "Salir";
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // lblTituloAccion
            // 
            this.lblTituloAccion.Appearance.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTituloAccion.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lblTituloAccion.Appearance.Options.UseFont = true;
            this.lblTituloAccion.Appearance.Options.UseForeColor = true;
            this.lblTituloAccion.Appearance.Options.UseTextOptions = true;
            this.lblTituloAccion.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblTituloAccion.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblTituloAccion.Location = new System.Drawing.Point(111, 5);
            this.lblTituloAccion.Margin = new System.Windows.Forms.Padding(4);
            this.lblTituloAccion.Name = "lblTituloAccion";
            this.lblTituloAccion.Size = new System.Drawing.Size(296, 30);
            this.lblTituloAccion.TabIndex = 64;
            this.lblTituloAccion.Text = "Información Biográfica";
            // 
            // sbtnSiguiente
            // 
            this.sbtnSiguiente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.sbtnSiguiente.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.sbtnSiguiente.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sbtnSiguiente.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.sbtnSiguiente.Appearance.Options.UseBackColor = true;
            this.sbtnSiguiente.Appearance.Options.UseFont = true;
            this.sbtnSiguiente.Appearance.Options.UseForeColor = true;
            this.sbtnSiguiente.Appearance.Options.UseTextOptions = true;
            this.sbtnSiguiente.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.sbtnSiguiente.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.sbtnSiguiente.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("sbtnSiguiente.ImageOptions.Image")));
            this.sbtnSiguiente.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.sbtnSiguiente.Location = new System.Drawing.Point(1149, 10);
            this.sbtnSiguiente.LookAndFeel.SkinName = "Glass Oceans";
            this.sbtnSiguiente.LookAndFeel.UseDefaultLookAndFeel = false;
            this.sbtnSiguiente.Margin = new System.Windows.Forms.Padding(4);
            this.sbtnSiguiente.Name = "sbtnSiguiente";
            this.sbtnSiguiente.Size = new System.Drawing.Size(132, 44);
            this.sbtnSiguiente.TabIndex = 70;
            this.sbtnSiguiente.Text = "&Siguiente";
            this.sbtnSiguiente.Click += new System.EventHandler(this.sbtnSiguiente_Click);
            // 
            // sbtnAnterior
            // 
            this.sbtnAnterior.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.sbtnAnterior.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.sbtnAnterior.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sbtnAnterior.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.sbtnAnterior.Appearance.Options.UseBackColor = true;
            this.sbtnAnterior.Appearance.Options.UseFont = true;
            this.sbtnAnterior.Appearance.Options.UseForeColor = true;
            this.sbtnAnterior.Appearance.Options.UseTextOptions = true;
            this.sbtnAnterior.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.sbtnAnterior.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.sbtnAnterior.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("sbtnAnterior.ImageOptions.Image")));
            this.sbtnAnterior.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.sbtnAnterior.Location = new System.Drawing.Point(1015, 10);
            this.sbtnAnterior.LookAndFeel.SkinName = "Glass Oceans";
            this.sbtnAnterior.LookAndFeel.UseDefaultLookAndFeel = false;
            this.sbtnAnterior.Margin = new System.Windows.Forms.Padding(4);
            this.sbtnAnterior.Name = "sbtnAnterior";
            this.sbtnAnterior.Size = new System.Drawing.Size(125, 44);
            this.sbtnAnterior.TabIndex = 69;
            this.sbtnAnterior.Text = "&Anterior";
            this.sbtnAnterior.Click += new System.EventHandler(this.sbtnAnterior_Click);
            // 
            // lblDetalles
            // 
            this.lblDetalles.Appearance.Font = new System.Drawing.Font("Segoe UI", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDetalles.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lblDetalles.Appearance.Options.UseFont = true;
            this.lblDetalles.Appearance.Options.UseForeColor = true;
            this.lblDetalles.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblDetalles.Location = new System.Drawing.Point(111, 32);
            this.lblDetalles.Margin = new System.Windows.Forms.Padding(4);
            this.lblDetalles.Name = "lblDetalles";
            this.lblDetalles.Size = new System.Drawing.Size(325, 21);
            this.lblDetalles.TabIndex = 66;
            this.lblDetalles.Text = "Datos Personales.";
            // 
            // picNumeros
            // 
            this.picNumeros.Cursor = System.Windows.Forms.Cursors.Default;
            this.picNumeros.EditValue = ((object)(resources.GetObject("picNumeros.EditValue")));
            this.picNumeros.Location = new System.Drawing.Point(35, 4);
            this.picNumeros.Margin = new System.Windows.Forms.Padding(4);
            this.picNumeros.Name = "picNumeros";
            this.picNumeros.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picNumeros.Properties.Appearance.Options.UseBackColor = true;
            this.picNumeros.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.picNumeros.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picNumeros.Properties.ShowMenu = false;
            this.picNumeros.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.picNumeros.Size = new System.Drawing.Size(70, 52);
            this.picNumeros.TabIndex = 65;
            // 
            // picManoDerecha
            // 
            this.picManoDerecha.Cursor = System.Windows.Forms.Cursors.Default;
            this.picManoDerecha.EditValue = ((object)(resources.GetObject("picManoDerecha.EditValue")));
            this.picManoDerecha.Location = new System.Drawing.Point(535, 34);
            this.picManoDerecha.Name = "picManoDerecha";
            this.picManoDerecha.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picManoDerecha.Properties.Appearance.Options.UseBackColor = true;
            this.picManoDerecha.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.picManoDerecha.Properties.PictureAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.picManoDerecha.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picManoDerecha.Properties.ShowMenu = false;
            this.picManoDerecha.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.picManoDerecha.Size = new System.Drawing.Size(61, 56);
            this.picManoDerecha.TabIndex = 264;
            // 
            // picManoIzquierda
            // 
            this.picManoIzquierda.Cursor = System.Windows.Forms.Cursors.Default;
            this.picManoIzquierda.EditValue = ((object)(resources.GetObject("picManoIzquierda.EditValue")));
            this.picManoIzquierda.Location = new System.Drawing.Point(452, 34);
            this.picManoIzquierda.Name = "picManoIzquierda";
            this.picManoIzquierda.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picManoIzquierda.Properties.Appearance.Options.UseBackColor = true;
            this.picManoIzquierda.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.picManoIzquierda.Properties.PictureAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.picManoIzquierda.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picManoIzquierda.Properties.ShowMenu = false;
            this.picManoIzquierda.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.picManoIzquierda.Size = new System.Drawing.Size(61, 56);
            this.picManoIzquierda.TabIndex = 270;
            // 
            // picIndiceDerecho
            // 
            this.picIndiceDerecho.Cursor = System.Windows.Forms.Cursors.Default;
            this.picIndiceDerecho.Location = new System.Drawing.Point(686, 22);
            this.picIndiceDerecho.Name = "picIndiceDerecho";
            this.picIndiceDerecho.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.picIndiceDerecho.Properties.Appearance.BorderColor = System.Drawing.Color.SteelBlue;
            this.picIndiceDerecho.Properties.Appearance.Font = new System.Drawing.Font("Arial Black", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.picIndiceDerecho.Properties.Appearance.ForeColor = System.Drawing.Color.DarkGray;
            this.picIndiceDerecho.Properties.Appearance.Options.UseBackColor = true;
            this.picIndiceDerecho.Properties.Appearance.Options.UseBorderColor = true;
            this.picIndiceDerecho.Properties.Appearance.Options.UseFont = true;
            this.picIndiceDerecho.Properties.Appearance.Options.UseForeColor = true;
            this.picIndiceDerecho.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.picIndiceDerecho.Properties.NullText = "2";
            this.picIndiceDerecho.Properties.ShowMenu = false;
            this.picIndiceDerecho.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.picIndiceDerecho.Size = new System.Drawing.Size(86, 78);
            this.picIndiceDerecho.TabIndex = 261;
            this.picIndiceDerecho.ToolTip = "Indice Derecho";
            // 
            // picPulgarDerecho
            // 
            this.picPulgarDerecho.Cursor = System.Windows.Forms.Cursors.Default;
            this.picPulgarDerecho.Location = new System.Drawing.Point(596, 22);
            this.picPulgarDerecho.Name = "picPulgarDerecho";
            this.picPulgarDerecho.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.picPulgarDerecho.Properties.Appearance.BorderColor = System.Drawing.Color.SteelBlue;
            this.picPulgarDerecho.Properties.Appearance.Font = new System.Drawing.Font("Arial Black", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.picPulgarDerecho.Properties.Appearance.ForeColor = System.Drawing.Color.DarkGray;
            this.picPulgarDerecho.Properties.Appearance.Options.UseBackColor = true;
            this.picPulgarDerecho.Properties.Appearance.Options.UseBorderColor = true;
            this.picPulgarDerecho.Properties.Appearance.Options.UseFont = true;
            this.picPulgarDerecho.Properties.Appearance.Options.UseForeColor = true;
            this.picPulgarDerecho.Properties.Appearance.Options.UseTextOptions = true;
            this.picPulgarDerecho.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.picPulgarDerecho.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.picPulgarDerecho.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.picPulgarDerecho.Properties.NullText = "1";
            this.picPulgarDerecho.Properties.ShowMenu = false;
            this.picPulgarDerecho.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.picPulgarDerecho.Size = new System.Drawing.Size(86, 78);
            this.picPulgarDerecho.TabIndex = 260;
            this.picPulgarDerecho.ToolTip = "Pulgar Derecho";
            // 
            // picMedioDerecho
            // 
            this.picMedioDerecho.Cursor = System.Windows.Forms.Cursors.Default;
            this.picMedioDerecho.Location = new System.Drawing.Point(775, 22);
            this.picMedioDerecho.Name = "picMedioDerecho";
            this.picMedioDerecho.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.picMedioDerecho.Properties.Appearance.BorderColor = System.Drawing.Color.SteelBlue;
            this.picMedioDerecho.Properties.Appearance.Font = new System.Drawing.Font("Arial Black", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.picMedioDerecho.Properties.Appearance.ForeColor = System.Drawing.Color.DarkGray;
            this.picMedioDerecho.Properties.Appearance.Options.UseBackColor = true;
            this.picMedioDerecho.Properties.Appearance.Options.UseBorderColor = true;
            this.picMedioDerecho.Properties.Appearance.Options.UseFont = true;
            this.picMedioDerecho.Properties.Appearance.Options.UseForeColor = true;
            this.picMedioDerecho.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.picMedioDerecho.Properties.NullText = "3";
            this.picMedioDerecho.Properties.ShowMenu = false;
            this.picMedioDerecho.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.picMedioDerecho.Size = new System.Drawing.Size(86, 78);
            this.picMedioDerecho.TabIndex = 262;
            this.picMedioDerecho.ToolTip = "Medio Derecho";
            // 
            // picAnularDerecho
            // 
            this.picAnularDerecho.Cursor = System.Windows.Forms.Cursors.Default;
            this.picAnularDerecho.Location = new System.Drawing.Point(864, 22);
            this.picAnularDerecho.Name = "picAnularDerecho";
            this.picAnularDerecho.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.picAnularDerecho.Properties.Appearance.BorderColor = System.Drawing.Color.SteelBlue;
            this.picAnularDerecho.Properties.Appearance.Font = new System.Drawing.Font("Arial Black", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.picAnularDerecho.Properties.Appearance.ForeColor = System.Drawing.Color.DarkGray;
            this.picAnularDerecho.Properties.Appearance.Options.UseBackColor = true;
            this.picAnularDerecho.Properties.Appearance.Options.UseBorderColor = true;
            this.picAnularDerecho.Properties.Appearance.Options.UseFont = true;
            this.picAnularDerecho.Properties.Appearance.Options.UseForeColor = true;
            this.picAnularDerecho.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.picAnularDerecho.Properties.NullText = "4";
            this.picAnularDerecho.Properties.ShowMenu = false;
            this.picAnularDerecho.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.picAnularDerecho.Size = new System.Drawing.Size(86, 78);
            this.picAnularDerecho.TabIndex = 263;
            this.picAnularDerecho.ToolTip = "Anular Derecho";
            // 
            // picAnularIzquierdo
            // 
            this.picAnularIzquierdo.Cursor = System.Windows.Forms.Cursors.Default;
            this.picAnularIzquierdo.Location = new System.Drawing.Point(102, 22);
            this.picAnularIzquierdo.Name = "picAnularIzquierdo";
            this.picAnularIzquierdo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.picAnularIzquierdo.Properties.Appearance.BorderColor = System.Drawing.Color.SteelBlue;
            this.picAnularIzquierdo.Properties.Appearance.Font = new System.Drawing.Font("Arial Black", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.picAnularIzquierdo.Properties.Appearance.ForeColor = System.Drawing.Color.DarkGray;
            this.picAnularIzquierdo.Properties.Appearance.Options.UseBackColor = true;
            this.picAnularIzquierdo.Properties.Appearance.Options.UseBorderColor = true;
            this.picAnularIzquierdo.Properties.Appearance.Options.UseFont = true;
            this.picAnularIzquierdo.Properties.Appearance.Options.UseForeColor = true;
            this.picAnularIzquierdo.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.picAnularIzquierdo.Properties.NullText = "9";
            this.picAnularIzquierdo.Properties.ShowMenu = false;
            this.picAnularIzquierdo.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.picAnularIzquierdo.Size = new System.Drawing.Size(86, 78);
            this.picAnularIzquierdo.TabIndex = 266;
            this.picAnularIzquierdo.ToolTip = "Anular Izquierdo";
            // 
            // picMeniequeIzquierdo
            // 
            this.picMeniequeIzquierdo.Cursor = System.Windows.Forms.Cursors.Default;
            this.picMeniequeIzquierdo.Location = new System.Drawing.Point(13, 22);
            this.picMeniequeIzquierdo.Name = "picMeniequeIzquierdo";
            this.picMeniequeIzquierdo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.picMeniequeIzquierdo.Properties.Appearance.BorderColor = System.Drawing.Color.SteelBlue;
            this.picMeniequeIzquierdo.Properties.Appearance.Font = new System.Drawing.Font("Arial Black", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.picMeniequeIzquierdo.Properties.Appearance.ForeColor = System.Drawing.Color.DarkGray;
            this.picMeniequeIzquierdo.Properties.Appearance.Options.UseBackColor = true;
            this.picMeniequeIzquierdo.Properties.Appearance.Options.UseBorderColor = true;
            this.picMeniequeIzquierdo.Properties.Appearance.Options.UseFont = true;
            this.picMeniequeIzquierdo.Properties.Appearance.Options.UseForeColor = true;
            this.picMeniequeIzquierdo.Properties.Appearance.Options.UseTextOptions = true;
            this.picMeniequeIzquierdo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.picMeniequeIzquierdo.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.picMeniequeIzquierdo.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.picMeniequeIzquierdo.Properties.NullText = "10";
            this.picMeniequeIzquierdo.Properties.ShowMenu = false;
            this.picMeniequeIzquierdo.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.picMeniequeIzquierdo.Size = new System.Drawing.Size(86, 78);
            this.picMeniequeIzquierdo.TabIndex = 265;
            this.picMeniequeIzquierdo.ToolTip = "Meñique Izquierdo";
            // 
            // picMedioIzquierdo
            // 
            this.picMedioIzquierdo.Cursor = System.Windows.Forms.Cursors.Default;
            this.picMedioIzquierdo.Location = new System.Drawing.Point(192, 22);
            this.picMedioIzquierdo.Name = "picMedioIzquierdo";
            this.picMedioIzquierdo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.picMedioIzquierdo.Properties.Appearance.BorderColor = System.Drawing.Color.SteelBlue;
            this.picMedioIzquierdo.Properties.Appearance.Font = new System.Drawing.Font("Arial Black", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.picMedioIzquierdo.Properties.Appearance.ForeColor = System.Drawing.Color.DarkGray;
            this.picMedioIzquierdo.Properties.Appearance.Options.UseBackColor = true;
            this.picMedioIzquierdo.Properties.Appearance.Options.UseBorderColor = true;
            this.picMedioIzquierdo.Properties.Appearance.Options.UseFont = true;
            this.picMedioIzquierdo.Properties.Appearance.Options.UseForeColor = true;
            this.picMedioIzquierdo.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.picMedioIzquierdo.Properties.NullText = "8";
            this.picMedioIzquierdo.Properties.ShowMenu = false;
            this.picMedioIzquierdo.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.picMedioIzquierdo.Size = new System.Drawing.Size(86, 78);
            this.picMedioIzquierdo.TabIndex = 267;
            this.picMedioIzquierdo.ToolTip = "Medio Izquierdo";
            // 
            // picIndiceIzquierdo
            // 
            this.picIndiceIzquierdo.AllowDrop = true;
            this.picIndiceIzquierdo.AllowHtmlTextInToolTip = DevExpress.Utils.DefaultBoolean.True;
            this.picIndiceIzquierdo.Cursor = System.Windows.Forms.Cursors.Default;
            this.picIndiceIzquierdo.Location = new System.Drawing.Point(282, 22);
            this.picIndiceIzquierdo.Name = "picIndiceIzquierdo";
            this.picIndiceIzquierdo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.picIndiceIzquierdo.Properties.Appearance.BorderColor = System.Drawing.Color.SteelBlue;
            this.picIndiceIzquierdo.Properties.Appearance.Font = new System.Drawing.Font("Arial Black", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.picIndiceIzquierdo.Properties.Appearance.ForeColor = System.Drawing.Color.DarkGray;
            this.picIndiceIzquierdo.Properties.Appearance.Options.UseBackColor = true;
            this.picIndiceIzquierdo.Properties.Appearance.Options.UseBorderColor = true;
            this.picIndiceIzquierdo.Properties.Appearance.Options.UseFont = true;
            this.picIndiceIzquierdo.Properties.Appearance.Options.UseForeColor = true;
            this.picIndiceIzquierdo.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.picIndiceIzquierdo.Properties.NullText = "7";
            this.picIndiceIzquierdo.Properties.ShowMenu = false;
            this.picIndiceIzquierdo.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.picIndiceIzquierdo.Size = new System.Drawing.Size(86, 78);
            this.picIndiceIzquierdo.TabIndex = 269;
            this.picIndiceIzquierdo.ToolTip = "Indice Izquierdo";
            // 
            // labelControl18
            // 
            this.labelControl18.Location = new System.Drawing.Point(463, 86);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(0, 16);
            this.labelControl18.TabIndex = 271;
            // 
            // labelControl13
            // 
            this.labelControl13.Location = new System.Drawing.Point(547, 86);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(0, 16);
            this.labelControl13.TabIndex = 272;
            // 
            // tabEnrolamiento
            // 
            this.tabEnrolamiento.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabEnrolamiento.Controls.Add(this.tabBiograficos);
            this.tabEnrolamiento.Controls.Add(this.navigationPage1);
            this.tabEnrolamiento.Controls.Add(this.navigationPage2);
            this.tabEnrolamiento.Controls.Add(this.navigationPage5);
            this.tabEnrolamiento.Location = new System.Drawing.Point(0, 90);
            this.tabEnrolamiento.Margin = new System.Windows.Forms.Padding(4);
            this.tabEnrolamiento.Name = "tabEnrolamiento";
            this.tabEnrolamiento.Pages.AddRange(new DevExpress.XtraBars.Navigation.NavigationPageBase[] {
            this.tabBiograficos,
            this.navigationPage1,
            this.navigationPage5,
            this.navigationPage2});
            this.tabEnrolamiento.SelectedPage = this.tabBiograficos;
            this.tabEnrolamiento.Size = new System.Drawing.Size(1325, 676);
            this.tabEnrolamiento.TabIndex = 118;
            this.tabEnrolamiento.Text = "navigationFrame1";
            // 
            // tabBiograficos
            // 
            this.tabBiograficos.Caption = "tabBiograficos";
            this.tabBiograficos.Controls.Add(this.labelControl59);
            this.tabBiograficos.Controls.Add(this.labelControl61);
            this.tabBiograficos.Controls.Add(this.label22);
            this.tabBiograficos.Controls.Add(this.labelControl57);
            this.tabBiograficos.Controls.Add(this.label15);
            this.tabBiograficos.Controls.Add(this.label1);
            this.tabBiograficos.Controls.Add(this.labelControl4);
            this.tabBiograficos.Controls.Add(this.labelControl15);
            this.tabBiograficos.Controls.Add(this.labelControl12);
            this.tabBiograficos.Controls.Add(this.labelControl11);
            this.tabBiograficos.Controls.Add(this.labelControl69);
            this.tabBiograficos.Controls.Add(this.label2);
            this.tabBiograficos.Controls.Add(this.labelControl10);
            this.tabBiograficos.Controls.Add(this.labelControl62);
            this.tabBiograficos.Controls.Add(this.labelControl21);
            this.tabBiograficos.Controls.Add(this.labelControl17);
            this.tabBiograficos.Controls.Add(this.labelControl8);
            this.tabBiograficos.Controls.Add(this.labelControl2);
            this.tabBiograficos.Controls.Add(this.labelControl60);
            this.tabBiograficos.Controls.Add(this.labelControl14);
            this.tabBiograficos.Controls.Add(this.labelControl7);
            this.tabBiograficos.Controls.Add(this.labelControl19);
            this.tabBiograficos.Controls.Add(this.labelControl58);
            this.tabBiograficos.Controls.Add(this.labelControl54);
            this.tabBiograficos.Controls.Add(this.labelControl53);
            this.tabBiograficos.Controls.Add(this.labelControl37);
            this.tabBiograficos.Controls.Add(this.labelControl24);
            this.tabBiograficos.Controls.Add(this.labelControl16);
            this.tabBiograficos.Controls.Add(this.labelControl9);
            this.tabBiograficos.Controls.Add(this.labelControl6);
            this.tabBiograficos.Controls.Add(this.labelControl3);
            this.tabBiograficos.Controls.Add(this.btnTest);
            this.tabBiograficos.Controls.Add(this.txtCorreoTrabajo);
            this.tabBiograficos.Controls.Add(this.lueColonia);
            this.tabBiograficos.Controls.Add(this.lueMunicipios);
            this.tabBiograficos.Controls.Add(this.lueDepartamentos);
            this.tabBiograficos.Controls.Add(this.labelControl42);
            this.tabBiograficos.Controls.Add(this.label28);
            this.tabBiograficos.Controls.Add(this.meDireccionTrabajo);
            this.tabBiograficos.Controls.Add(this.txtTelefonoTrabajo);
            this.tabBiograficos.Controls.Add(this.lueProfesion);
            this.tabBiograficos.Controls.Add(this.labelControl5);
            this.tabBiograficos.Controls.Add(this.label29);
            this.tabBiograficos.Controls.Add(this.label20);
            this.tabBiograficos.Controls.Add(this.label19);
            this.tabBiograficos.Controls.Add(this.label18);
            this.tabBiograficos.Controls.Add(this.label16);
            this.tabBiograficos.Controls.Add(this.label14);
            this.tabBiograficos.Controls.Add(this.label13);
            this.tabBiograficos.Controls.Add(this.label12);
            this.tabBiograficos.Controls.Add(this.label11);
            this.tabBiograficos.Controls.Add(this.label10);
            this.tabBiograficos.Controls.Add(this.label9);
            this.tabBiograficos.Controls.Add(this.label8);
            this.tabBiograficos.Controls.Add(this.label7);
            this.tabBiograficos.Controls.Add(this.label6);
            this.tabBiograficos.Controls.Add(this.lueNivelEducativo);
            this.tabBiograficos.Controls.Add(this.luePaisNacimiento);
            this.tabBiograficos.Controls.Add(this.meDireccionCompleta);
            this.tabBiograficos.Controls.Add(this.txtMovil);
            this.tabBiograficos.Controls.Add(this.txtTelefono);
            this.tabBiograficos.Controls.Add(this.lueSexo);
            this.tabBiograficos.Controls.Add(this.dtmFechaNacimiento);
            this.tabBiograficos.Controls.Add(this.txtCorreo);
            this.tabBiograficos.Controls.Add(this.lueNacionalidad);
            this.tabBiograficos.Controls.Add(this.lueEstadoCivil);
            this.tabBiograficos.Controls.Add(this.lueTipoDocumento);
            this.tabBiograficos.Controls.Add(this.txtIdentificacion);
            this.tabBiograficos.Controls.Add(this.txtPrimerNombre);
            this.tabBiograficos.Controls.Add(this.txtSegundoNombre);
            this.tabBiograficos.Controls.Add(this.txtPrimerApellido);
            this.tabBiograficos.Controls.Add(this.txtSegundoApellido);
            this.tabBiograficos.FireScrollEventOnMouseWheel = true;
            this.tabBiograficos.Margin = new System.Windows.Forms.Padding(4);
            this.tabBiograficos.Name = "tabBiograficos";
            this.tabBiograficos.Size = new System.Drawing.Size(1325, 676);
            // 
            // labelControl61
            // 
            this.labelControl61.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl61.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl61.Appearance.Options.UseFont = true;
            this.labelControl61.Appearance.Options.UseForeColor = true;
            this.labelControl61.Location = new System.Drawing.Point(796, 40);
            this.labelControl61.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl61.Name = "labelControl61";
            this.labelControl61.Size = new System.Drawing.Size(62, 20);
            this.labelControl61.TabIndex = 473;
            this.labelControl61.Text = "Profesión";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.ForeColor = System.Drawing.Color.Red;
            this.label22.Location = new System.Drawing.Point(781, 44);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(16, 17);
            this.label22.TabIndex = 397;
            this.label22.Text = "*";
            // 
            // labelControl57
            // 
            this.labelControl57.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl57.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl57.Appearance.Options.UseFont = true;
            this.labelControl57.Appearance.Options.UseForeColor = true;
            this.labelControl57.Location = new System.Drawing.Point(413, 44);
            this.labelControl57.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl57.Name = "labelControl57";
            this.labelControl57.Size = new System.Drawing.Size(168, 20);
            this.labelControl57.TabIndex = 466;
            this.labelControl57.Text = "Departamento residencia";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.ForeColor = System.Drawing.Color.Red;
            this.label15.Location = new System.Drawing.Point(400, 46);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(16, 17);
            this.label15.TabIndex = 380;
            this.label15.Text = "*";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(24, 43);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(16, 17);
            this.label1.TabIndex = 457;
            this.label1.Text = "*";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Appearance.Options.UseForeColor = true;
            this.labelControl4.Location = new System.Drawing.Point(39, 40);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(133, 20);
            this.labelControl4.TabIndex = 408;
            this.labelControl4.Text = "Tipo de Documento";
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl15.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl15.Appearance.Options.UseFont = true;
            this.labelControl15.Appearance.Options.UseForeColor = true;
            this.labelControl15.Appearance.Options.UseTextOptions = true;
            this.labelControl15.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl15.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl15.LineColor = System.Drawing.Color.Gainsboro;
            this.labelControl15.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.labelControl15.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.labelControl15.LineVisible = true;
            this.labelControl15.Location = new System.Drawing.Point(785, 4);
            this.labelControl15.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(224, 24);
            this.labelControl15.TabIndex = 456;
            this.labelControl15.Text = "Información Laboral";
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl12.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl12.Appearance.Options.UseFont = true;
            this.labelControl12.Appearance.Options.UseForeColor = true;
            this.labelControl12.Appearance.Options.UseTextOptions = true;
            this.labelControl12.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl12.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl12.LineColor = System.Drawing.Color.Gainsboro;
            this.labelControl12.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.labelControl12.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.labelControl12.LineVisible = true;
            this.labelControl12.Location = new System.Drawing.Point(402, 6);
            this.labelControl12.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(269, 24);
            this.labelControl12.TabIndex = 455;
            this.labelControl12.Text = "Información de Residencia";
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl11.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl11.Appearance.Options.UseFont = true;
            this.labelControl11.Appearance.Options.UseForeColor = true;
            this.labelControl11.Appearance.Options.UseTextOptions = true;
            this.labelControl11.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl11.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl11.LineColor = System.Drawing.Color.Gainsboro;
            this.labelControl11.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.labelControl11.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.labelControl11.LineVisible = true;
            this.labelControl11.Location = new System.Drawing.Point(27, 3);
            this.labelControl11.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(224, 24);
            this.labelControl11.TabIndex = 454;
            this.labelControl11.Text = "Información Personal";
            // 
            // labelControl69
            // 
            this.labelControl69.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl69.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl69.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.labelControl69.LineLocation = DevExpress.XtraEditors.LineLocation.Top;
            this.labelControl69.LineVisible = true;
            this.labelControl69.Location = new System.Drawing.Point(27, 28);
            this.labelControl69.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.labelControl69.Name = "labelControl69";
            this.labelControl69.Size = new System.Drawing.Size(1296, 24);
            this.labelControl69.TabIndex = 569;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(397, 486);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(16, 17);
            this.label2.TabIndex = 480;
            this.label2.Text = "*";
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl10.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl10.Appearance.Options.UseFont = true;
            this.labelControl10.Appearance.Options.UseForeColor = true;
            this.labelControl10.Appearance.Options.UseTextOptions = true;
            this.labelControl10.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl10.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl10.LineColor = System.Drawing.Color.Gainsboro;
            this.labelControl10.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.labelControl10.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.labelControl10.LineVisible = true;
            this.labelControl10.Location = new System.Drawing.Point(957, 573);
            this.labelControl10.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(140, 24);
            this.labelControl10.TabIndex = 479;
            this.labelControl10.Text = "Campos requeridos";
            // 
            // labelControl62
            // 
            this.labelControl62.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl62.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl62.Appearance.Options.UseFont = true;
            this.labelControl62.Appearance.Options.UseForeColor = true;
            this.labelControl62.Appearance.Options.UseTextOptions = true;
            this.labelControl62.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl62.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl62.LineColor = System.Drawing.Color.Gainsboro;
            this.labelControl62.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.labelControl62.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.labelControl62.LineVisible = true;
            this.labelControl62.Location = new System.Drawing.Point(430, 569);
            this.labelControl62.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.labelControl62.Name = "labelControl62";
            this.labelControl62.Size = new System.Drawing.Size(365, 24);
            this.labelControl62.TabIndex = 478;
            this.labelControl62.Text = "Al menos un número telefónico es requerido";
            // 
            // labelControl21
            // 
            this.labelControl21.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl21.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl21.Appearance.Options.UseFont = true;
            this.labelControl21.Appearance.Options.UseForeColor = true;
            this.labelControl21.Location = new System.Drawing.Point(786, 355);
            this.labelControl21.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(123, 20);
            this.labelControl21.TabIndex = 477;
            this.labelControl21.Text = "Correo electrónico";
            // 
            // labelControl17
            // 
            this.labelControl17.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl17.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl17.Appearance.Options.UseFont = true;
            this.labelControl17.Appearance.Options.UseForeColor = true;
            this.labelControl17.Location = new System.Drawing.Point(787, 228);
            this.labelControl17.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(136, 20);
            this.labelControl17.TabIndex = 476;
            this.labelControl17.Text = "Dirección de trabajo";
            this.labelControl17.Click += new System.EventHandler(this.labelControl17_Click);
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl8.Appearance.Options.UseFont = true;
            this.labelControl8.Appearance.Options.UseForeColor = true;
            this.labelControl8.Location = new System.Drawing.Point(788, 165);
            this.labelControl8.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(135, 20);
            this.labelControl8.TabIndex = 475;
            this.labelControl8.Text = "Número de teléfono";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Appearance.Options.UseForeColor = true;
            this.labelControl2.Location = new System.Drawing.Point(796, 99);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(103, 20);
            this.labelControl2.TabIndex = 474;
            this.labelControl2.Text = "Nivel educativo";
            // 
            // labelControl59
            // 
            this.labelControl59.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl59.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl59.Appearance.Options.UseFont = true;
            this.labelControl59.Appearance.Options.UseForeColor = true;
            this.labelControl59.Location = new System.Drawing.Point(413, 483);
            this.labelControl59.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl59.Name = "labelControl59";
            this.labelControl59.Size = new System.Drawing.Size(45, 20);
            this.labelControl59.TabIndex = 472;
            this.labelControl59.Text = "Correo";
            // 
            // labelControl60
            // 
            this.labelControl60.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl60.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl60.Appearance.Options.UseFont = true;
            this.labelControl60.Appearance.Options.UseForeColor = true;
            this.labelControl60.Location = new System.Drawing.Point(424, 425);
            this.labelControl60.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl60.Name = "labelControl60";
            this.labelControl60.Size = new System.Drawing.Size(123, 20);
            this.labelControl60.TabIndex = 471;
            this.labelControl60.Text = "Número de celular";
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl14.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl14.Appearance.Options.UseFont = true;
            this.labelControl14.Appearance.Options.UseForeColor = true;
            this.labelControl14.Location = new System.Drawing.Point(424, 359);
            this.labelControl14.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(135, 20);
            this.labelControl14.TabIndex = 470;
            this.labelControl14.Text = "Número de teléfono";
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl7.Appearance.Options.UseFont = true;
            this.labelControl7.Appearance.Options.UseForeColor = true;
            this.labelControl7.Location = new System.Drawing.Point(413, 227);
            this.labelControl7.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(67, 20);
            this.labelControl7.TabIndex = 469;
            this.labelControl7.Text = "Dirección ";
            // 
            // labelControl19
            // 
            this.labelControl19.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl19.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl19.Appearance.Options.UseFont = true;
            this.labelControl19.Appearance.Options.UseForeColor = true;
            this.labelControl19.Location = new System.Drawing.Point(413, 170);
            this.labelControl19.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(122, 20);
            this.labelControl19.TabIndex = 468;
            this.labelControl19.Text = "Colonia residencia";
            // 
            // labelControl58
            // 
            this.labelControl58.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl58.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl58.Appearance.Options.UseFont = true;
            this.labelControl58.Appearance.Options.UseForeColor = true;
            this.labelControl58.Location = new System.Drawing.Point(413, 107);
            this.labelControl58.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl58.Name = "labelControl58";
            this.labelControl58.Size = new System.Drawing.Size(137, 20);
            this.labelControl58.TabIndex = 467;
            this.labelControl58.Text = "Municipio residencia";
            // 
            // labelControl54
            // 
            this.labelControl54.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl54.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl54.Appearance.Options.UseFont = true;
            this.labelControl54.Appearance.Options.UseForeColor = true;
            this.labelControl54.Location = new System.Drawing.Point(39, 540);
            this.labelControl54.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl54.Name = "labelControl54";
            this.labelControl54.Size = new System.Drawing.Size(75, 20);
            this.labelControl54.TabIndex = 465;
            this.labelControl54.Text = "Estado civil";
            // 
            // labelControl53
            // 
            this.labelControl53.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl53.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl53.Appearance.Options.UseFont = true;
            this.labelControl53.Appearance.Options.UseForeColor = true;
            this.labelControl53.Location = new System.Drawing.Point(39, 480);
            this.labelControl53.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl53.Name = "labelControl53";
            this.labelControl53.Size = new System.Drawing.Size(137, 20);
            this.labelControl53.TabIndex = 464;
            this.labelControl53.Text = "Fecha de nacimiento";
            // 
            // labelControl37
            // 
            this.labelControl37.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl37.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl37.Appearance.Options.UseFont = true;
            this.labelControl37.Appearance.Options.UseForeColor = true;
            this.labelControl37.Location = new System.Drawing.Point(39, 418);
            this.labelControl37.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl37.Name = "labelControl37";
            this.labelControl37.Size = new System.Drawing.Size(89, 20);
            this.labelControl37.TabIndex = 463;
            this.labelControl37.Text = "Nacionalidad";
            // 
            // labelControl24
            // 
            this.labelControl24.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl24.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl24.Appearance.Options.UseFont = true;
            this.labelControl24.Appearance.Options.UseForeColor = true;
            this.labelControl24.Location = new System.Drawing.Point(39, 352);
            this.labelControl24.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(125, 20);
            this.labelControl24.TabIndex = 462;
            this.labelControl24.Text = "Pais de nacimiento";
            // 
            // labelControl16
            // 
            this.labelControl16.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl16.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl16.Appearance.Options.UseFont = true;
            this.labelControl16.Appearance.Options.UseForeColor = true;
            this.labelControl16.Location = new System.Drawing.Point(39, 292);
            this.labelControl16.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(32, 20);
            this.labelControl16.TabIndex = 461;
            this.labelControl16.Text = "Sexo";
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl9.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl9.Appearance.Options.UseFont = true;
            this.labelControl9.Appearance.Options.UseForeColor = true;
            this.labelControl9.Location = new System.Drawing.Point(39, 229);
            this.labelControl9.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(174, 20);
            this.labelControl9.TabIndex = 460;
            this.labelControl9.Text = "Primer y segundo apellido";
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl6.Appearance.Options.UseFont = true;
            this.labelControl6.Appearance.Options.UseForeColor = true;
            this.labelControl6.Location = new System.Drawing.Point(39, 160);
            this.labelControl6.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(171, 20);
            this.labelControl6.TabIndex = 459;
            this.labelControl6.Text = "Primer y segundo nombre";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Appearance.Options.UseForeColor = true;
            this.labelControl3.Location = new System.Drawing.Point(39, 99);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(169, 20);
            this.labelControl3.TabIndex = 458;
            this.labelControl3.Text = "Número de Identificación";
            // 
            // btnTest
            // 
            this.btnTest.Location = new System.Drawing.Point(1004, 448);
            this.btnTest.Margin = new System.Windows.Forms.Padding(4);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(94, 29);
            this.btnTest.TabIndex = 415;
            this.btnTest.Text = "test";
            this.btnTest.Visible = false;
            // 
            // txtCorreoTrabajo
            // 
            this.txtCorreoTrabajo.Location = new System.Drawing.Point(784, 382);
            this.txtCorreoTrabajo.Margin = new System.Windows.Forms.Padding(4);
            this.txtCorreoTrabajo.Name = "txtCorreoTrabajo";
            this.txtCorreoTrabajo.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCorreoTrabajo.Properties.Appearance.Options.UseFont = true;
            this.txtCorreoTrabajo.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtCorreoTrabajo.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtCorreoTrabajo.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtCorreoTrabajo.Properties.Mask.EditMask = "\\w+([-+.\']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
            this.txtCorreoTrabajo.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtCorreoTrabajo.Properties.MaxLength = 100;
            this.txtCorreoTrabajo.Size = new System.Drawing.Size(311, 26);
            this.txtCorreoTrabajo.TabIndex = 329;
            // 
            // lueColonia
            // 
            this.lueColonia.Location = new System.Drawing.Point(403, 191);
            this.lueColonia.Margin = new System.Windows.Forms.Padding(4);
            this.lueColonia.Name = "lueColonia";
            this.lueColonia.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueColonia.Properties.Appearance.Options.UseFont = true;
            this.lueColonia.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueColonia.Properties.NullText = "";
            this.lueColonia.Properties.PopupFilterMode = DevExpress.XtraEditors.PopupFilterMode.Contains;
            this.lueColonia.Size = new System.Drawing.Size(312, 26);
            this.lueColonia.TabIndex = 320;
            // 
            // lueMunicipios
            // 
            this.lueMunicipios.Location = new System.Drawing.Point(402, 128);
            this.lueMunicipios.Margin = new System.Windows.Forms.Padding(4);
            this.lueMunicipios.Name = "lueMunicipios";
            this.lueMunicipios.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueMunicipios.Properties.Appearance.Options.UseFont = true;
            this.lueMunicipios.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueMunicipios.Properties.NullText = "";
            this.lueMunicipios.Size = new System.Drawing.Size(311, 26);
            this.lueMunicipios.TabIndex = 319;
            this.lueMunicipios.EditValueChanged += new System.EventHandler(this.lueMunicipios_EditValueChanged);
            // 
            // lueDepartamentos
            // 
            this.lueDepartamentos.Location = new System.Drawing.Point(402, 65);
            this.lueDepartamentos.Margin = new System.Windows.Forms.Padding(4);
            this.lueDepartamentos.Name = "lueDepartamentos";
            this.lueDepartamentos.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueDepartamentos.Properties.Appearance.Options.UseFont = true;
            this.lueDepartamentos.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueDepartamentos.Properties.NullText = "";
            this.lueDepartamentos.Size = new System.Drawing.Size(311, 26);
            this.lueDepartamentos.TabIndex = 318;
            this.lueDepartamentos.EditValueChanged += new System.EventHandler(this.lueDepartamentos_EditValueChanged);
            // 
            // labelControl42
            // 
            this.labelControl42.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl42.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl42.Appearance.Options.UseFont = true;
            this.labelControl42.Appearance.Options.UseForeColor = true;
            this.labelControl42.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl42.Location = new System.Drawing.Point(937, 576);
            this.labelControl42.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl42.Name = "labelControl42";
            this.labelControl42.Size = new System.Drawing.Size(20, 16);
            this.labelControl42.TabIndex = 344;
            this.labelControl42.Text = "(*)\r\n";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.ForeColor = System.Drawing.Color.Red;
            this.label28.Location = new System.Drawing.Point(399, 170);
            this.label28.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(16, 17);
            this.label28.TabIndex = 407;
            this.label28.Text = "*";
            // 
            // meDireccionTrabajo
            // 
            this.meDireccionTrabajo.Location = new System.Drawing.Point(786, 254);
            this.meDireccionTrabajo.Margin = new System.Windows.Forms.Padding(4);
            this.meDireccionTrabajo.Name = "meDireccionTrabajo";
            this.meDireccionTrabajo.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.meDireccionTrabajo.Properties.Appearance.Options.UseFont = true;
            this.meDireccionTrabajo.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.meDireccionTrabajo.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.meDireccionTrabajo.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.meDireccionTrabajo.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.meDireccionTrabajo.Properties.MaxLength = 1000;
            this.meDireccionTrabajo.Size = new System.Drawing.Size(311, 96);
            this.meDireccionTrabajo.TabIndex = 328;
            // 
            // txtTelefonoTrabajo
            // 
            this.txtTelefonoTrabajo.Location = new System.Drawing.Point(786, 191);
            this.txtTelefonoTrabajo.Margin = new System.Windows.Forms.Padding(4);
            this.txtTelefonoTrabajo.Name = "txtTelefonoTrabajo";
            this.txtTelefonoTrabajo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtTelefonoTrabajo.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTelefonoTrabajo.Properties.Appearance.Options.UseBackColor = true;
            this.txtTelefonoTrabajo.Properties.Appearance.Options.UseFont = true;
            this.txtTelefonoTrabajo.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTelefonoTrabajo.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTelefonoTrabajo.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtTelefonoTrabajo.Properties.Mask.BeepOnError = true;
            this.txtTelefonoTrabajo.Properties.Mask.EditMask = "[0-9,()-]+";
            this.txtTelefonoTrabajo.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtTelefonoTrabajo.Properties.MaxLength = 20;
            this.txtTelefonoTrabajo.Size = new System.Drawing.Size(311, 26);
            this.txtTelefonoTrabajo.TabIndex = 327;
            // 
            // lueProfesion
            // 
            this.lueProfesion.Location = new System.Drawing.Point(784, 66);
            this.lueProfesion.Margin = new System.Windows.Forms.Padding(4);
            this.lueProfesion.Name = "lueProfesion";
            this.lueProfesion.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueProfesion.Properties.Appearance.Options.UseFont = true;
            this.lueProfesion.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lueProfesion.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lueProfesion.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.lueProfesion.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueProfesion.Properties.NullText = "";
            this.lueProfesion.Size = new System.Drawing.Size(312, 26);
            this.lueProfesion.TabIndex = 325;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Appearance.Options.UseForeColor = true;
            this.labelControl5.Location = new System.Drawing.Point(398, 573);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(24, 20);
            this.labelControl5.TabIndex = 393;
            this.labelControl5.Text = "(**)\r\n";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.ForeColor = System.Drawing.Color.Red;
            this.label29.Location = new System.Drawing.Point(782, 103);
            this.label29.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(16, 17);
            this.label29.TabIndex = 390;
            this.label29.Text = "*";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.ForeColor = System.Drawing.Color.Red;
            this.label20.Location = new System.Drawing.Point(400, 425);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(24, 17);
            this.label20.TabIndex = 385;
            this.label20.Text = "**";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.ForeColor = System.Drawing.Color.Red;
            this.label19.Location = new System.Drawing.Point(400, 361);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(24, 17);
            this.label19.TabIndex = 384;
            this.label19.Text = "**";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.ForeColor = System.Drawing.Color.Red;
            this.label18.Location = new System.Drawing.Point(399, 234);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(16, 17);
            this.label18.TabIndex = 383;
            this.label18.Text = "*";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.ForeColor = System.Drawing.Color.Red;
            this.label16.Location = new System.Drawing.Point(400, 107);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(16, 17);
            this.label16.TabIndex = 381;
            this.label16.Text = "*";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Location = new System.Drawing.Point(24, 544);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(16, 17);
            this.label14.TabIndex = 378;
            this.label14.Text = "*";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Location = new System.Drawing.Point(25, 483);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(16, 17);
            this.label13.TabIndex = 377;
            this.label13.Text = "*";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(26, 421);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(16, 17);
            this.label12.TabIndex = 376;
            this.label12.Text = "*";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Location = new System.Drawing.Point(25, 355);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(16, 17);
            this.label11.TabIndex = 375;
            this.label11.Text = "*";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(25, 293);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(16, 17);
            this.label10.TabIndex = 374;
            this.label10.Text = "*";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(25, 228);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(16, 17);
            this.label9.TabIndex = 373;
            this.label9.Text = "*";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(25, 163);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(16, 17);
            this.label8.TabIndex = 372;
            this.label8.Text = "*";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(25, 103);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(16, 17);
            this.label7.TabIndex = 371;
            this.label7.Text = "*";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(156, 38);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(16, 17);
            this.label6.TabIndex = 370;
            this.label6.Text = "*";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lueNivelEducativo
            // 
            this.lueNivelEducativo.Location = new System.Drawing.Point(784, 124);
            this.lueNivelEducativo.Margin = new System.Windows.Forms.Padding(4);
            this.lueNivelEducativo.Name = "lueNivelEducativo";
            this.lueNivelEducativo.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueNivelEducativo.Properties.Appearance.Options.UseFont = true;
            this.lueNivelEducativo.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lueNivelEducativo.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lueNivelEducativo.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.lueNivelEducativo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueNivelEducativo.Properties.NullText = "";
            this.lueNivelEducativo.Size = new System.Drawing.Size(312, 26);
            this.lueNivelEducativo.TabIndex = 326;
            // 
            // luePaisNacimiento
            // 
            this.luePaisNacimiento.Location = new System.Drawing.Point(27, 377);
            this.luePaisNacimiento.Margin = new System.Windows.Forms.Padding(4);
            this.luePaisNacimiento.Name = "luePaisNacimiento";
            this.luePaisNacimiento.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.luePaisNacimiento.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.luePaisNacimiento.Properties.Appearance.Options.UseBackColor = true;
            this.luePaisNacimiento.Properties.Appearance.Options.UseFont = true;
            this.luePaisNacimiento.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.luePaisNacimiento.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.luePaisNacimiento.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.luePaisNacimiento.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luePaisNacimiento.Properties.NullText = "";
            this.luePaisNacimiento.Size = new System.Drawing.Size(322, 26);
            this.luePaisNacimiento.TabIndex = 314;
            // 
            // meDireccionCompleta
            // 
            this.meDireccionCompleta.Location = new System.Drawing.Point(402, 254);
            this.meDireccionCompleta.Margin = new System.Windows.Forms.Padding(4);
            this.meDireccionCompleta.Name = "meDireccionCompleta";
            this.meDireccionCompleta.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.meDireccionCompleta.Properties.Appearance.Options.UseFont = true;
            this.meDireccionCompleta.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.meDireccionCompleta.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.meDireccionCompleta.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.meDireccionCompleta.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.meDireccionCompleta.Properties.MaxLength = 1000;
            this.meDireccionCompleta.Size = new System.Drawing.Size(314, 95);
            this.meDireccionCompleta.TabIndex = 321;
            // 
            // txtMovil
            // 
            this.txtMovil.Location = new System.Drawing.Point(399, 446);
            this.txtMovil.Margin = new System.Windows.Forms.Padding(4);
            this.txtMovil.Name = "txtMovil";
            this.txtMovil.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtMovil.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMovil.Properties.Appearance.Options.UseBackColor = true;
            this.txtMovil.Properties.Appearance.Options.UseFont = true;
            this.txtMovil.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtMovil.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtMovil.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtMovil.Properties.Mask.BeepOnError = true;
            this.txtMovil.Properties.Mask.EditMask = "[0-9,()-]+";
            this.txtMovil.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtMovil.Properties.MaxLength = 20;
            this.txtMovil.Size = new System.Drawing.Size(311, 26);
            this.txtMovil.TabIndex = 323;
            // 
            // txtTelefono
            // 
            this.txtTelefono.Location = new System.Drawing.Point(402, 382);
            this.txtTelefono.Margin = new System.Windows.Forms.Padding(4);
            this.txtTelefono.Name = "txtTelefono";
            this.txtTelefono.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtTelefono.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTelefono.Properties.Appearance.Options.UseBackColor = true;
            this.txtTelefono.Properties.Appearance.Options.UseFont = true;
            this.txtTelefono.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTelefono.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTelefono.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtTelefono.Properties.Mask.BeepOnError = true;
            this.txtTelefono.Properties.Mask.EditMask = "[0-9,()-]+";
            this.txtTelefono.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtTelefono.Properties.MaxLength = 20;
            this.txtTelefono.Size = new System.Drawing.Size(311, 26);
            this.txtTelefono.TabIndex = 322;
            // 
            // lueSexo
            // 
            this.lueSexo.Location = new System.Drawing.Point(29, 314);
            this.lueSexo.Margin = new System.Windows.Forms.Padding(4);
            this.lueSexo.Name = "lueSexo";
            this.lueSexo.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueSexo.Properties.Appearance.Options.UseFont = true;
            this.lueSexo.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lueSexo.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lueSexo.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.lueSexo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueSexo.Properties.NullText = "";
            this.lueSexo.Size = new System.Drawing.Size(320, 26);
            this.lueSexo.TabIndex = 313;
            // 
            // dtmFechaNacimiento
            // 
            this.dtmFechaNacimiento.EditValue = null;
            this.dtmFechaNacimiento.Location = new System.Drawing.Point(27, 504);
            this.dtmFechaNacimiento.Margin = new System.Windows.Forms.Padding(4);
            this.dtmFechaNacimiento.Name = "dtmFechaNacimiento";
            this.dtmFechaNacimiento.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.dtmFechaNacimiento.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtmFechaNacimiento.Properties.Appearance.Options.UseBackColor = true;
            this.dtmFechaNacimiento.Properties.Appearance.Options.UseFont = true;
            this.dtmFechaNacimiento.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dtmFechaNacimiento.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dtmFechaNacimiento.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.dtmFechaNacimiento.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtmFechaNacimiento.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtmFechaNacimiento.Size = new System.Drawing.Size(322, 26);
            this.dtmFechaNacimiento.TabIndex = 316;
            // 
            // txtCorreo
            // 
            this.txtCorreo.Location = new System.Drawing.Point(399, 508);
            this.txtCorreo.Margin = new System.Windows.Forms.Padding(4);
            this.txtCorreo.Name = "txtCorreo";
            this.txtCorreo.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCorreo.Properties.Appearance.Options.UseFont = true;
            this.txtCorreo.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtCorreo.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtCorreo.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtCorreo.Properties.Mask.EditMask = "\\w+([-+.\']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
            this.txtCorreo.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtCorreo.Properties.MaxLength = 100;
            this.txtCorreo.Size = new System.Drawing.Size(311, 26);
            this.txtCorreo.TabIndex = 324;
            // 
            // lueNacionalidad
            // 
            this.lueNacionalidad.Location = new System.Drawing.Point(28, 442);
            this.lueNacionalidad.Margin = new System.Windows.Forms.Padding(4);
            this.lueNacionalidad.Name = "lueNacionalidad";
            this.lueNacionalidad.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.lueNacionalidad.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueNacionalidad.Properties.Appearance.Options.UseBackColor = true;
            this.lueNacionalidad.Properties.Appearance.Options.UseFont = true;
            this.lueNacionalidad.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lueNacionalidad.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lueNacionalidad.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.lueNacionalidad.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueNacionalidad.Properties.NullText = "";
            this.lueNacionalidad.Size = new System.Drawing.Size(321, 26);
            this.lueNacionalidad.TabIndex = 315;
            // 
            // lueEstadoCivil
            // 
            this.lueEstadoCivil.Location = new System.Drawing.Point(27, 563);
            this.lueEstadoCivil.Margin = new System.Windows.Forms.Padding(4);
            this.lueEstadoCivil.Name = "lueEstadoCivil";
            this.lueEstadoCivil.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueEstadoCivil.Properties.Appearance.Options.UseFont = true;
            this.lueEstadoCivil.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lueEstadoCivil.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lueEstadoCivil.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.lueEstadoCivil.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueEstadoCivil.Properties.NullText = "";
            this.lueEstadoCivil.Size = new System.Drawing.Size(322, 26);
            this.lueEstadoCivil.TabIndex = 317;
            // 
            // lueTipoDocumento
            // 
            this.lueTipoDocumento.Enabled = false;
            this.lueTipoDocumento.Location = new System.Drawing.Point(28, 65);
            this.lueTipoDocumento.Margin = new System.Windows.Forms.Padding(4);
            this.lueTipoDocumento.Name = "lueTipoDocumento";
            this.lueTipoDocumento.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.lueTipoDocumento.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueTipoDocumento.Properties.Appearance.Options.UseBackColor = true;
            this.lueTipoDocumento.Properties.Appearance.Options.UseFont = true;
            this.lueTipoDocumento.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lueTipoDocumento.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lueTipoDocumento.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.lueTipoDocumento.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueTipoDocumento.Properties.NullText = "";
            this.lueTipoDocumento.Size = new System.Drawing.Size(321, 26);
            this.lueTipoDocumento.TabIndex = 307;
            this.lueTipoDocumento.EditValueChanged += new System.EventHandler(this.lueTipoDocumento_EditValueChanged);
            // 
            // txtIdentificacion
            // 
            this.txtIdentificacion.Enabled = false;
            this.txtIdentificacion.Location = new System.Drawing.Point(28, 124);
            this.txtIdentificacion.Margin = new System.Windows.Forms.Padding(4);
            this.txtIdentificacion.Name = "txtIdentificacion";
            this.txtIdentificacion.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtIdentificacion.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIdentificacion.Properties.Appearance.Options.UseBackColor = true;
            this.txtIdentificacion.Properties.Appearance.Options.UseFont = true;
            this.txtIdentificacion.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtIdentificacion.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtIdentificacion.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtIdentificacion.Properties.Mask.EditMask = "0000-0000-00000";
            this.txtIdentificacion.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
            this.txtIdentificacion.Properties.Mask.SaveLiteral = false;
            this.txtIdentificacion.Properties.MaxLength = 20;
            this.txtIdentificacion.Size = new System.Drawing.Size(321, 26);
            this.txtIdentificacion.TabIndex = 308;
            // 
            // txtPrimerNombre
            // 
            this.txtPrimerNombre.Location = new System.Drawing.Point(28, 188);
            this.txtPrimerNombre.Margin = new System.Windows.Forms.Padding(4);
            this.txtPrimerNombre.Name = "txtPrimerNombre";
            this.txtPrimerNombre.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtPrimerNombre.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrimerNombre.Properties.Appearance.Options.UseBackColor = true;
            this.txtPrimerNombre.Properties.Appearance.Options.UseFont = true;
            this.txtPrimerNombre.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtPrimerNombre.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtPrimerNombre.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtPrimerNombre.Properties.Mask.BeepOnError = true;
            this.txtPrimerNombre.Properties.Mask.EditMask = "[A-ZÑ]+";
            this.txtPrimerNombre.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtPrimerNombre.Properties.MaxLength = 100;
            this.txtPrimerNombre.Size = new System.Drawing.Size(156, 26);
            this.txtPrimerNombre.TabIndex = 309;
            // 
            // txtSegundoNombre
            // 
            this.txtSegundoNombre.Location = new System.Drawing.Point(190, 187);
            this.txtSegundoNombre.Margin = new System.Windows.Forms.Padding(4);
            this.txtSegundoNombre.Name = "txtSegundoNombre";
            this.txtSegundoNombre.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtSegundoNombre.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSegundoNombre.Properties.Appearance.Options.UseBackColor = true;
            this.txtSegundoNombre.Properties.Appearance.Options.UseFont = true;
            this.txtSegundoNombre.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtSegundoNombre.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtSegundoNombre.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtSegundoNombre.Properties.Mask.EditMask = "[A-Z Ñ]+";
            this.txtSegundoNombre.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtSegundoNombre.Properties.MaxLength = 100;
            this.txtSegundoNombre.Size = new System.Drawing.Size(159, 26);
            this.txtSegundoNombre.TabIndex = 310;
            // 
            // txtPrimerApellido
            // 
            this.txtPrimerApellido.Location = new System.Drawing.Point(28, 252);
            this.txtPrimerApellido.Margin = new System.Windows.Forms.Padding(4);
            this.txtPrimerApellido.Name = "txtPrimerApellido";
            this.txtPrimerApellido.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtPrimerApellido.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrimerApellido.Properties.Appearance.Options.UseBackColor = true;
            this.txtPrimerApellido.Properties.Appearance.Options.UseFont = true;
            this.txtPrimerApellido.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtPrimerApellido.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtPrimerApellido.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtPrimerApellido.Properties.Mask.EditMask = "[A-Z Ñ]+";
            this.txtPrimerApellido.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtPrimerApellido.Properties.MaxLength = 100;
            this.txtPrimerApellido.Size = new System.Drawing.Size(156, 26);
            this.txtPrimerApellido.TabIndex = 311;
            // 
            // txtSegundoApellido
            // 
            this.txtSegundoApellido.Location = new System.Drawing.Point(190, 252);
            this.txtSegundoApellido.Margin = new System.Windows.Forms.Padding(4);
            this.txtSegundoApellido.Name = "txtSegundoApellido";
            this.txtSegundoApellido.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtSegundoApellido.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSegundoApellido.Properties.Appearance.Options.UseBackColor = true;
            this.txtSegundoApellido.Properties.Appearance.Options.UseFont = true;
            this.txtSegundoApellido.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtSegundoApellido.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtSegundoApellido.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtSegundoApellido.Properties.Mask.EditMask = "[A-Z Ñ]+";
            this.txtSegundoApellido.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtSegundoApellido.Properties.MaxLength = 100;
            this.txtSegundoApellido.Size = new System.Drawing.Size(159, 26);
            this.txtSegundoApellido.TabIndex = 312;
            // 
            // navigationPage1
            // 
            this.navigationPage1.Caption = "navigationPage1";
            this.navigationPage1.Controls.Add(this.btnRotar);
            this.navigationPage1.Controls.Add(this.pnlFotoDB);
            this.navigationPage1.Controls.Add(this.picFotoCapturada);
            this.navigationPage1.Controls.Add(this.sbtnIniciarCamara);
            this.navigationPage1.Margin = new System.Windows.Forms.Padding(4);
            this.navigationPage1.Name = "navigationPage1";
            this.navigationPage1.Size = new System.Drawing.Size(1325, 676);
            // 
            // btnRotar
            // 
            this.btnRotar.Appearance.BackColor = System.Drawing.Color.AliceBlue;
            this.btnRotar.Appearance.Font = new System.Drawing.Font("Trebuchet MS", 9.75F);
            this.btnRotar.Appearance.Options.UseBackColor = true;
            this.btnRotar.Appearance.Options.UseFont = true;
            this.btnRotar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnRotar.ImageOptions.Image")));
            this.btnRotar.Location = new System.Drawing.Point(395, 61);
            this.btnRotar.LookAndFeel.SkinName = "Office 2016 Colorful";
            this.btnRotar.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnRotar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnRotar.Name = "btnRotar";
            this.btnRotar.Size = new System.Drawing.Size(101, 33);
            this.btnRotar.TabIndex = 310;
            this.btnRotar.Text = "Rotar";
            this.btnRotar.Visible = false;
            this.btnRotar.Click += new System.EventHandler(this.btnRotar_Click);
            // 
            // pnlFotoDB
            // 
            this.pnlFotoDB.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pnlFotoDB.Appearance.Options.UseBackColor = true;
            this.pnlFotoDB.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlFotoDB.Controls.Add(this.picFotoDB);
            this.pnlFotoDB.Controls.Add(this.labelControl1);
            this.pnlFotoDB.Location = new System.Drawing.Point(395, 92);
            this.pnlFotoDB.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.pnlFotoDB.LookAndFeel.UseDefaultLookAndFeel = false;
            this.pnlFotoDB.Margin = new System.Windows.Forms.Padding(4);
            this.pnlFotoDB.Name = "pnlFotoDB";
            this.pnlFotoDB.Size = new System.Drawing.Size(155, 232);
            this.pnlFotoDB.TabIndex = 309;
            this.pnlFotoDB.Visible = false;
            // 
            // picFotoDB
            // 
            this.picFotoDB.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.picFotoDB.Cursor = System.Windows.Forms.Cursors.Default;
            this.picFotoDB.EditValue = global::Gv.ExodusBc.UI.Properties.Resources.Login4;
            this.picFotoDB.Location = new System.Drawing.Point(8, 35);
            this.picFotoDB.Margin = new System.Windows.Forms.Padding(4);
            this.picFotoDB.Name = "picFotoDB";
            this.picFotoDB.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picFotoDB.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.picFotoDB.Properties.Appearance.Options.UseBackColor = true;
            this.picFotoDB.Properties.Appearance.Options.UseBorderColor = true;
            this.picFotoDB.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.picFotoDB.Properties.NullText = " ";
            this.picFotoDB.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picFotoDB.Properties.ShowMenu = false;
            this.picFotoDB.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.picFotoDB.Size = new System.Drawing.Size(138, 194);
            this.picFotoDB.TabIndex = 293;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl1.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Appearance.Options.UseForeColor = true;
            this.labelControl1.Appearance.Options.UseImageAlign = true;
            this.labelControl1.Appearance.Options.UseTextOptions = true;
            this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.labelControl1.Location = new System.Drawing.Point(8, 10);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(72, 18);
            this.labelControl1.TabIndex = 307;
            this.labelControl1.Text = "Foto actual";
            // 
            // picFotoCapturada
            // 
            this.picFotoCapturada.Cursor = System.Windows.Forms.Cursors.Default;
            this.picFotoCapturada.EditValue = ((object)(resources.GetObject("picFotoCapturada.EditValue")));
            this.picFotoCapturada.Location = new System.Drawing.Point(616, 92);
            this.picFotoCapturada.Margin = new System.Windows.Forms.Padding(4);
            this.picFotoCapturada.Name = "picFotoCapturada";
            this.picFotoCapturada.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.picFotoCapturada.Properties.Appearance.Options.UseBorderColor = true;
            this.picFotoCapturada.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.picFotoCapturada.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picFotoCapturada.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.picFotoCapturada.Size = new System.Drawing.Size(275, 375);
            this.picFotoCapturada.TabIndex = 295;
            // 
            // sbtnIniciarCamara
            // 
            this.sbtnIniciarCamara.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.sbtnIniciarCamara.Appearance.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sbtnIniciarCamara.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.sbtnIniciarCamara.Appearance.Options.UseBackColor = true;
            this.sbtnIniciarCamara.Appearance.Options.UseFont = true;
            this.sbtnIniciarCamara.Appearance.Options.UseForeColor = true;
            this.sbtnIniciarCamara.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.sbtnIniciarCamara.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.sbtnIniciarCamara.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.sbtnIniciarCamara.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("sbtnIniciarCamara.ImageOptions.SvgImage")));
            this.sbtnIniciarCamara.Location = new System.Drawing.Point(616, 475);
            this.sbtnIniciarCamara.Margin = new System.Windows.Forms.Padding(4);
            this.sbtnIniciarCamara.Name = "sbtnIniciarCamara";
            this.sbtnIniciarCamara.Size = new System.Drawing.Size(275, 35);
            this.sbtnIniciarCamara.TabIndex = 292;
            this.sbtnIniciarCamara.Text = "Iniciar Cámara";
            this.sbtnIniciarCamara.Click += new System.EventHandler(this.sbtnIniciarCamara_Click);
            // 
            // navigationPage2
            // 
            this.navigationPage2.Caption = "navigationPage2";
            this.navigationPage2.Controls.Add(this.labelControl22);
            this.navigationPage2.Controls.Add(this.labelControl25);
            this.navigationPage2.Controls.Add(this.lblDepartamento);
            this.navigationPage2.Controls.Add(this.labelControl47);
            this.navigationPage2.Controls.Add(this.lblTipoDocumento);
            this.navigationPage2.Controls.Add(this.labelControl44);
            this.navigationPage2.Controls.Add(this.picFotoFinal);
            this.navigationPage2.Controls.Add(this.labelControl23);
            this.navigationPage2.Controls.Add(this.labelControl41);
            this.navigationPage2.Controls.Add(this.labelControl63);
            this.navigationPage2.Controls.Add(this.labelControl56);
            this.navigationPage2.Controls.Add(this.reviewSignature);
            this.navigationPage2.Controls.Add(this.lblNivelEducativo);
            this.navigationPage2.Controls.Add(this.labelControl135);
            this.navigationPage2.Controls.Add(this.lblProfesionOficio);
            this.navigationPage2.Controls.Add(this.labelControl100);
            this.navigationPage2.Controls.Add(this.lblPaisNacimiento);
            this.navigationPage2.Controls.Add(this.labelControl132);
            this.navigationPage2.Controls.Add(this.lblDireccionResidencia);
            this.navigationPage2.Controls.Add(this.lblMovil);
            this.navigationPage2.Controls.Add(this.labelControl110);
            this.navigationPage2.Controls.Add(this.labelControl108);
            this.navigationPage2.Controls.Add(this.lblTelefono);
            this.navigationPage2.Controls.Add(this.lblColoniaCasa);
            this.navigationPage2.Controls.Add(this.labelControl104);
            this.navigationPage2.Controls.Add(this.labelControl103);
            this.navigationPage2.Controls.Add(this.lblCorreoElectronico);
            this.navigationPage2.Controls.Add(this.labelControl101);
            this.navigationPage2.Controls.Add(this.lblNacionalidad);
            this.navigationPage2.Controls.Add(this.lblNacimiento);
            this.navigationPage2.Controls.Add(this.lblSexo);
            this.navigationPage2.Controls.Add(this.lblIdentificacion);
            this.navigationPage2.Controls.Add(this.lblEstadoCivil);
            this.navigationPage2.Controls.Add(this.lblMunicipio);
            this.navigationPage2.Controls.Add(this.lblApellidos);
            this.navigationPage2.Controls.Add(this.lblNombres);
            this.navigationPage2.Controls.Add(this.labelControl49);
            this.navigationPage2.Controls.Add(this.labelControl50);
            this.navigationPage2.Controls.Add(this.labelControl51);
            this.navigationPage2.Controls.Add(this.labelControl52);
            this.navigationPage2.Controls.Add(this.labelControl45);
            this.navigationPage2.Controls.Add(this.labelControl46);
            this.navigationPage2.Controls.Add(this.labelControl48);
            this.navigationPage2.Controls.Add(this.labelControl55);
            this.navigationPage2.Margin = new System.Windows.Forms.Padding(4);
            this.navigationPage2.Name = "navigationPage2";
            this.navigationPage2.Size = new System.Drawing.Size(1325, 676);
            // 
            // labelControl22
            // 
            this.labelControl22.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl22.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl22.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl22.Appearance.Options.UseFont = true;
            this.labelControl22.Appearance.Options.UseForeColor = true;
            this.labelControl22.Appearance.Options.UseTextOptions = true;
            this.labelControl22.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl22.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl22.LineColor = System.Drawing.Color.Gainsboro;
            this.labelControl22.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.labelControl22.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.labelControl22.LineVisible = true;
            this.labelControl22.Location = new System.Drawing.Point(19, 346);
            this.labelControl22.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(1288, 35);
            this.labelControl22.TabIndex = 556;
            this.labelControl22.Text = "Firma";
            // 
            // labelControl25
            // 
            this.labelControl25.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl25.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl25.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.labelControl25.LineLocation = DevExpress.XtraEditors.LineLocation.Top;
            this.labelControl25.LineVisible = true;
            this.labelControl25.Location = new System.Drawing.Point(18, 343);
            this.labelControl25.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(1289, 24);
            this.labelControl25.TabIndex = 570;
            // 
            // lblDepartamento
            // 
            this.lblDepartamento.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDepartamento.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDepartamento.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lblDepartamento.Appearance.Options.UseFont = true;
            this.lblDepartamento.Appearance.Options.UseForeColor = true;
            this.lblDepartamento.Location = new System.Drawing.Point(883, 57);
            this.lblDepartamento.Margin = new System.Windows.Forms.Padding(4);
            this.lblDepartamento.Name = "lblDepartamento";
            this.lblDepartamento.Size = new System.Drawing.Size(131, 20);
            this.lblDepartamento.TabIndex = 503;
            this.lblDepartamento.Text = "[lblDepartamento]";
            // 
            // labelControl47
            // 
            this.labelControl47.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl47.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.labelControl47.Appearance.Options.UseFont = true;
            this.labelControl47.Appearance.Options.UseForeColor = true;
            this.labelControl47.Location = new System.Drawing.Point(766, 57);
            this.labelControl47.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl47.Name = "labelControl47";
            this.labelControl47.Size = new System.Drawing.Size(103, 20);
            this.labelControl47.TabIndex = 491;
            this.labelControl47.Text = "Departamento:";
            // 
            // lblTipoDocumento
            // 
            this.lblTipoDocumento.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTipoDocumento.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipoDocumento.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lblTipoDocumento.Appearance.Options.UseFont = true;
            this.lblTipoDocumento.Appearance.Options.UseForeColor = true;
            this.lblTipoDocumento.Location = new System.Drawing.Point(433, 57);
            this.lblTipoDocumento.Margin = new System.Windows.Forms.Padding(4);
            this.lblTipoDocumento.Name = "lblTipoDocumento";
            this.lblTipoDocumento.Size = new System.Drawing.Size(142, 20);
            this.lblTipoDocumento.TabIndex = 499;
            this.lblTipoDocumento.Text = "[lblTipoDocumento]";
            // 
            // labelControl44
            // 
            this.labelControl44.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl44.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl44.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.labelControl44.Appearance.Options.UseFont = true;
            this.labelControl44.Appearance.Options.UseForeColor = true;
            this.labelControl44.Location = new System.Drawing.Point(282, 57);
            this.labelControl44.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl44.Name = "labelControl44";
            this.labelControl44.Size = new System.Drawing.Size(139, 20);
            this.labelControl44.TabIndex = 494;
            this.labelControl44.Text = "Tipo de Documento:";
            // 
            // picFotoFinal
            // 
            this.picFotoFinal.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picFotoFinal.Cursor = System.Windows.Forms.Cursors.Default;
            this.picFotoFinal.Location = new System.Drawing.Point(19, 59);
            this.picFotoFinal.Margin = new System.Windows.Forms.Padding(4);
            this.picFotoFinal.Name = "picFotoFinal";
            this.picFotoFinal.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picFotoFinal.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.picFotoFinal.Properties.Appearance.Options.UseBackColor = true;
            this.picFotoFinal.Properties.Appearance.Options.UseBorderColor = true;
            this.picFotoFinal.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.picFotoFinal.Properties.NullText = " ";
            this.picFotoFinal.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picFotoFinal.Properties.ShowMenu = false;
            this.picFotoFinal.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.picFotoFinal.Size = new System.Drawing.Size(168, 205);
            this.picFotoFinal.TabIndex = 488;
            // 
            // labelControl23
            // 
            this.labelControl23.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl23.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl23.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.labelControl23.LineLocation = DevExpress.XtraEditors.LineLocation.Top;
            this.labelControl23.LineVisible = true;
            this.labelControl23.Location = new System.Drawing.Point(18, 36);
            this.labelControl23.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(1289, 24);
            this.labelControl23.TabIndex = 569;
            // 
            // labelControl41
            // 
            this.labelControl41.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl41.Appearance.ForeColor = System.Drawing.Color.DarkGray;
            this.labelControl41.Appearance.Options.UseFont = true;
            this.labelControl41.Appearance.Options.UseForeColor = true;
            this.labelControl41.Appearance.Options.UseTextOptions = true;
            this.labelControl41.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl41.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl41.LineColor = System.Drawing.Color.Gainsboro;
            this.labelControl41.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.labelControl41.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.labelControl41.Location = new System.Drawing.Point(18, 4);
            this.labelControl41.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.labelControl41.Name = "labelControl41";
            this.labelControl41.Size = new System.Drawing.Size(169, 39);
            this.labelControl41.TabIndex = 554;
            this.labelControl41.Text = "Fotografía";
            // 
            // labelControl63
            // 
            this.labelControl63.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl63.Appearance.ForeColor = System.Drawing.Color.DarkGray;
            this.labelControl63.Appearance.Options.UseFont = true;
            this.labelControl63.Appearance.Options.UseForeColor = true;
            this.labelControl63.Appearance.Options.UseTextOptions = true;
            this.labelControl63.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl63.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl63.LineColor = System.Drawing.Color.WhiteSmoke;
            this.labelControl63.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.labelControl63.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.labelControl63.Location = new System.Drawing.Point(703, 8);
            this.labelControl63.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.labelControl63.Name = "labelControl63";
            this.labelControl63.Size = new System.Drawing.Size(488, 35);
            this.labelControl63.TabIndex = 551;
            this.labelControl63.Text = "Información Biográfica";
            // 
            // labelControl56
            // 
            this.labelControl56.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl56.Appearance.ForeColor = System.Drawing.Color.DarkGray;
            this.labelControl56.Appearance.Options.UseFont = true;
            this.labelControl56.Appearance.Options.UseForeColor = true;
            this.labelControl56.Appearance.Options.UseTextOptions = true;
            this.labelControl56.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl56.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl56.LineColor = System.Drawing.Color.WhiteSmoke;
            this.labelControl56.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.labelControl56.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.labelControl56.Location = new System.Drawing.Point(233, 8);
            this.labelControl56.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.labelControl56.Name = "labelControl56";
            this.labelControl56.Size = new System.Drawing.Size(342, 35);
            this.labelControl56.TabIndex = 550;
            this.labelControl56.Text = "Información Personal";
            // 
            // reviewSignature
            // 
            this.reviewSignature.Cursor = System.Windows.Forms.Cursors.Default;
            this.reviewSignature.Location = new System.Drawing.Point(19, 388);
            this.reviewSignature.Margin = new System.Windows.Forms.Padding(4);
            this.reviewSignature.Name = "reviewSignature";
            this.reviewSignature.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.reviewSignature.Properties.Appearance.Options.UseBorderColor = true;
            this.reviewSignature.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.reviewSignature.Properties.NullText = "No signature";
            this.reviewSignature.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.reviewSignature.Properties.ShowMenu = false;
            this.reviewSignature.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.reviewSignature.Size = new System.Drawing.Size(485, 88);
            this.reviewSignature.TabIndex = 555;
            // 
            // lblNivelEducativo
            // 
            this.lblNivelEducativo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblNivelEducativo.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNivelEducativo.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lblNivelEducativo.Appearance.Options.UseFont = true;
            this.lblNivelEducativo.Appearance.Options.UseForeColor = true;
            this.lblNivelEducativo.Location = new System.Drawing.Point(883, 314);
            this.lblNivelEducativo.Margin = new System.Windows.Forms.Padding(4);
            this.lblNivelEducativo.Name = "lblNivelEducativo";
            this.lblNivelEducativo.Size = new System.Drawing.Size(133, 20);
            this.lblNivelEducativo.TabIndex = 548;
            this.lblNivelEducativo.Text = "[lblNivelEducativo]";
            // 
            // labelControl135
            // 
            this.labelControl135.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl135.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.labelControl135.Appearance.Options.UseFont = true;
            this.labelControl135.Appearance.Options.UseForeColor = true;
            this.labelControl135.Location = new System.Drawing.Point(758, 315);
            this.labelControl135.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl135.Name = "labelControl135";
            this.labelControl135.Size = new System.Drawing.Size(111, 20);
            this.labelControl135.TabIndex = 547;
            this.labelControl135.Text = "Nivel Educativo:";
            // 
            // lblProfesionOficio
            // 
            this.lblProfesionOficio.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblProfesionOficio.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProfesionOficio.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lblProfesionOficio.Appearance.Options.UseFont = true;
            this.lblProfesionOficio.Appearance.Options.UseForeColor = true;
            this.lblProfesionOficio.Location = new System.Drawing.Point(883, 283);
            this.lblProfesionOficio.Margin = new System.Windows.Forms.Padding(4);
            this.lblProfesionOficio.Name = "lblProfesionOficio";
            this.lblProfesionOficio.Size = new System.Drawing.Size(137, 20);
            this.lblProfesionOficio.TabIndex = 544;
            this.lblProfesionOficio.Text = "[lblProfesionOficio]";
            // 
            // labelControl100
            // 
            this.labelControl100.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl100.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.labelControl100.Appearance.Options.UseFont = true;
            this.labelControl100.Appearance.Options.UseForeColor = true;
            this.labelControl100.Location = new System.Drawing.Point(743, 283);
            this.labelControl100.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl100.Name = "labelControl100";
            this.labelControl100.Size = new System.Drawing.Size(126, 20);
            this.labelControl100.TabIndex = 543;
            this.labelControl100.Text = "Profesión u Oficio:";
            // 
            // lblPaisNacimiento
            // 
            this.lblPaisNacimiento.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPaisNacimiento.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPaisNacimiento.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lblPaisNacimiento.Appearance.Options.UseFont = true;
            this.lblPaisNacimiento.Appearance.Options.UseForeColor = true;
            this.lblPaisNacimiento.Location = new System.Drawing.Point(433, 209);
            this.lblPaisNacimiento.Margin = new System.Windows.Forms.Padding(4);
            this.lblPaisNacimiento.Name = "lblPaisNacimiento";
            this.lblPaisNacimiento.Size = new System.Drawing.Size(138, 20);
            this.lblPaisNacimiento.TabIndex = 534;
            this.lblPaisNacimiento.Text = "[lblPaisNacimiento]";
            // 
            // labelControl132
            // 
            this.labelControl132.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl132.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.labelControl132.Appearance.Options.UseFont = true;
            this.labelControl132.Appearance.Options.UseForeColor = true;
            this.labelControl132.Location = new System.Drawing.Point(286, 209);
            this.labelControl132.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl132.Name = "labelControl132";
            this.labelControl132.Size = new System.Drawing.Size(135, 20);
            this.labelControl132.TabIndex = 533;
            this.labelControl132.Text = "País de Nacimiento:";
            // 
            // lblDireccionResidencia
            // 
            this.lblDireccionResidencia.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDireccionResidencia.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblDireccionResidencia.Appearance.Options.UseFont = true;
            this.lblDireccionResidencia.Appearance.Options.UseForeColor = true;
            this.lblDireccionResidencia.Appearance.Options.UseTextOptions = true;
            this.lblDireccionResidencia.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.lblDireccionResidencia.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lblDireccionResidencia.AutoEllipsis = true;
            this.lblDireccionResidencia.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblDireccionResidencia.Location = new System.Drawing.Point(883, 150);
            this.lblDireccionResidencia.Margin = new System.Windows.Forms.Padding(4);
            this.lblDireccionResidencia.Name = "lblDireccionResidencia";
            this.lblDireccionResidencia.Padding = new System.Windows.Forms.Padding(2);
            this.lblDireccionResidencia.Size = new System.Drawing.Size(308, 40);
            this.lblDireccionResidencia.TabIndex = 529;
            this.lblDireccionResidencia.Text = "[lblDireccionResidencia]";
            // 
            // lblMovil
            // 
            this.lblMovil.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMovil.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMovil.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lblMovil.Appearance.Options.UseFont = true;
            this.lblMovil.Appearance.Options.UseForeColor = true;
            this.lblMovil.Location = new System.Drawing.Point(883, 220);
            this.lblMovil.Margin = new System.Windows.Forms.Padding(4);
            this.lblMovil.Name = "lblMovil";
            this.lblMovil.Size = new System.Drawing.Size(68, 20);
            this.lblMovil.TabIndex = 528;
            this.lblMovil.Text = "[lblMovil]";
            // 
            // labelControl110
            // 
            this.labelControl110.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl110.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.labelControl110.Appearance.Options.UseFont = true;
            this.labelControl110.Appearance.Options.UseForeColor = true;
            this.labelControl110.Location = new System.Drawing.Point(826, 220);
            this.labelControl110.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl110.Name = "labelControl110";
            this.labelControl110.Size = new System.Drawing.Size(43, 20);
            this.labelControl110.TabIndex = 525;
            this.labelControl110.Text = "Móvil:";
            // 
            // labelControl108
            // 
            this.labelControl108.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl108.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.labelControl108.Appearance.Options.UseFont = true;
            this.labelControl108.Appearance.Options.UseForeColor = true;
            this.labelControl108.Location = new System.Drawing.Point(703, 150);
            this.labelControl108.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl108.Name = "labelControl108";
            this.labelControl108.Size = new System.Drawing.Size(166, 20);
            this.labelControl108.TabIndex = 523;
            this.labelControl108.Text = "Dirección de Residencia:";
            // 
            // lblTelefono
            // 
            this.lblTelefono.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTelefono.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTelefono.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lblTelefono.Appearance.Options.UseFont = true;
            this.lblTelefono.Appearance.Options.UseForeColor = true;
            this.lblTelefono.Location = new System.Drawing.Point(883, 189);
            this.lblTelefono.Margin = new System.Windows.Forms.Padding(4);
            this.lblTelefono.Name = "lblTelefono";
            this.lblTelefono.Size = new System.Drawing.Size(91, 20);
            this.lblTelefono.TabIndex = 520;
            this.lblTelefono.Text = "[lblTelefono]";
            // 
            // lblColoniaCasa
            // 
            this.lblColoniaCasa.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblColoniaCasa.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblColoniaCasa.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lblColoniaCasa.Appearance.Options.UseFont = true;
            this.lblColoniaCasa.Appearance.Options.UseForeColor = true;
            this.lblColoniaCasa.Location = new System.Drawing.Point(883, 119);
            this.lblColoniaCasa.Margin = new System.Windows.Forms.Padding(4);
            this.lblColoniaCasa.Name = "lblColoniaCasa";
            this.lblColoniaCasa.Size = new System.Drawing.Size(113, 20);
            this.lblColoniaCasa.TabIndex = 519;
            this.lblColoniaCasa.Text = "[lblColoniaCasa]";
            // 
            // labelControl104
            // 
            this.labelControl104.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl104.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.labelControl104.Appearance.Options.UseFont = true;
            this.labelControl104.Appearance.Options.UseForeColor = true;
            this.labelControl104.Location = new System.Drawing.Point(805, 189);
            this.labelControl104.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl104.Name = "labelControl104";
            this.labelControl104.Size = new System.Drawing.Size(64, 20);
            this.labelControl104.TabIndex = 515;
            this.labelControl104.Text = "Teléfono:";
            // 
            // labelControl103
            // 
            this.labelControl103.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl103.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.labelControl103.Appearance.Options.UseFont = true;
            this.labelControl103.Appearance.Options.UseForeColor = true;
            this.labelControl103.Location = new System.Drawing.Point(813, 119);
            this.labelControl103.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl103.Name = "labelControl103";
            this.labelControl103.Size = new System.Drawing.Size(56, 20);
            this.labelControl103.TabIndex = 514;
            this.labelControl103.Text = "Colonia:";
            // 
            // lblCorreoElectronico
            // 
            this.lblCorreoElectronico.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCorreoElectronico.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCorreoElectronico.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lblCorreoElectronico.Appearance.Options.UseFont = true;
            this.lblCorreoElectronico.Appearance.Options.UseForeColor = true;
            this.lblCorreoElectronico.Location = new System.Drawing.Point(883, 251);
            this.lblCorreoElectronico.Margin = new System.Windows.Forms.Padding(4);
            this.lblCorreoElectronico.Name = "lblCorreoElectronico";
            this.lblCorreoElectronico.Size = new System.Drawing.Size(153, 20);
            this.lblCorreoElectronico.TabIndex = 513;
            this.lblCorreoElectronico.Text = "[lblCorreoElectronico]";
            // 
            // labelControl101
            // 
            this.labelControl101.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl101.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.labelControl101.Appearance.Options.UseFont = true;
            this.labelControl101.Appearance.Options.UseForeColor = true;
            this.labelControl101.Location = new System.Drawing.Point(738, 251);
            this.labelControl101.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl101.Name = "labelControl101";
            this.labelControl101.Size = new System.Drawing.Size(131, 20);
            this.labelControl101.TabIndex = 512;
            this.labelControl101.Text = "Correo Electrónico:";
            // 
            // lblNacionalidad
            // 
            this.lblNacionalidad.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblNacionalidad.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNacionalidad.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lblNacionalidad.Appearance.Options.UseFont = true;
            this.lblNacionalidad.Appearance.Options.UseForeColor = true;
            this.lblNacionalidad.Location = new System.Drawing.Point(433, 240);
            this.lblNacionalidad.Margin = new System.Windows.Forms.Padding(4);
            this.lblNacionalidad.Name = "lblNacionalidad";
            this.lblNacionalidad.Size = new System.Drawing.Size(120, 20);
            this.lblNacionalidad.TabIndex = 509;
            this.lblNacionalidad.Text = "[lblNacionalidad]";
            // 
            // lblNacimiento
            // 
            this.lblNacimiento.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblNacimiento.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNacimiento.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lblNacimiento.Appearance.Options.UseFont = true;
            this.lblNacimiento.Appearance.Options.UseForeColor = true;
            this.lblNacimiento.Location = new System.Drawing.Point(433, 271);
            this.lblNacimiento.Margin = new System.Windows.Forms.Padding(4);
            this.lblNacimiento.Name = "lblNacimiento";
            this.lblNacimiento.Size = new System.Drawing.Size(110, 20);
            this.lblNacimiento.TabIndex = 508;
            this.lblNacimiento.Text = "[lblNacimiento]";
            // 
            // lblSexo
            // 
            this.lblSexo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSexo.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSexo.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lblSexo.Appearance.Options.UseFont = true;
            this.lblSexo.Appearance.Options.UseForeColor = true;
            this.lblSexo.Location = new System.Drawing.Point(433, 181);
            this.lblSexo.Margin = new System.Windows.Forms.Padding(4);
            this.lblSexo.Name = "lblSexo";
            this.lblSexo.Size = new System.Drawing.Size(62, 20);
            this.lblSexo.TabIndex = 507;
            this.lblSexo.Text = "[lblSexo]";
            // 
            // lblIdentificacion
            // 
            this.lblIdentificacion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblIdentificacion.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIdentificacion.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lblIdentificacion.Appearance.Options.UseFont = true;
            this.lblIdentificacion.Appearance.Options.UseForeColor = true;
            this.lblIdentificacion.Location = new System.Drawing.Point(433, 88);
            this.lblIdentificacion.Margin = new System.Windows.Forms.Padding(4);
            this.lblIdentificacion.Name = "lblIdentificacion";
            this.lblIdentificacion.Size = new System.Drawing.Size(124, 20);
            this.lblIdentificacion.TabIndex = 506;
            this.lblIdentificacion.Text = "[lblIdentificacion]";
            // 
            // lblEstadoCivil
            // 
            this.lblEstadoCivil.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEstadoCivil.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstadoCivil.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lblEstadoCivil.Appearance.Options.UseFont = true;
            this.lblEstadoCivil.Appearance.Options.UseForeColor = true;
            this.lblEstadoCivil.Location = new System.Drawing.Point(433, 304);
            this.lblEstadoCivil.Margin = new System.Windows.Forms.Padding(4);
            this.lblEstadoCivil.Name = "lblEstadoCivil";
            this.lblEstadoCivil.Size = new System.Drawing.Size(105, 20);
            this.lblEstadoCivil.TabIndex = 505;
            this.lblEstadoCivil.Text = "[lblEstadoCivil]";
            // 
            // lblMunicipio
            // 
            this.lblMunicipio.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMunicipio.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMunicipio.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lblMunicipio.Appearance.Options.UseFont = true;
            this.lblMunicipio.Appearance.Options.UseForeColor = true;
            this.lblMunicipio.Location = new System.Drawing.Point(883, 88);
            this.lblMunicipio.Margin = new System.Windows.Forms.Padding(4);
            this.lblMunicipio.Name = "lblMunicipio";
            this.lblMunicipio.Size = new System.Drawing.Size(98, 20);
            this.lblMunicipio.TabIndex = 504;
            this.lblMunicipio.Text = "[lblMunicipio]";
            // 
            // lblApellidos
            // 
            this.lblApellidos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblApellidos.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblApellidos.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lblApellidos.Appearance.Options.UseFont = true;
            this.lblApellidos.Appearance.Options.UseForeColor = true;
            this.lblApellidos.Location = new System.Drawing.Point(433, 150);
            this.lblApellidos.Margin = new System.Windows.Forms.Padding(4);
            this.lblApellidos.Name = "lblApellidos";
            this.lblApellidos.Size = new System.Drawing.Size(94, 20);
            this.lblApellidos.TabIndex = 502;
            this.lblApellidos.Text = "[lblApellidos]";
            // 
            // lblNombres
            // 
            this.lblNombres.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblNombres.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombres.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lblNombres.Appearance.Options.UseFont = true;
            this.lblNombres.Appearance.Options.UseForeColor = true;
            this.lblNombres.Location = new System.Drawing.Point(433, 119);
            this.lblNombres.Margin = new System.Windows.Forms.Padding(4);
            this.lblNombres.Name = "lblNombres";
            this.lblNombres.Size = new System.Drawing.Size(77, 20);
            this.lblNombres.TabIndex = 500;
            this.lblNombres.Text = "[Nombres]";
            // 
            // labelControl49
            // 
            this.labelControl49.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl49.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.labelControl49.Appearance.Options.UseFont = true;
            this.labelControl49.Appearance.Options.UseForeColor = true;
            this.labelControl49.Appearance.Options.UseTextOptions = true;
            this.labelControl49.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl49.Location = new System.Drawing.Point(273, 271);
            this.labelControl49.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl49.Name = "labelControl49";
            this.labelControl49.Size = new System.Drawing.Size(148, 20);
            this.labelControl49.TabIndex = 498;
            this.labelControl49.Text = "Fecha de Nacimiento:";
            // 
            // labelControl50
            // 
            this.labelControl50.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl50.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.labelControl50.Appearance.Options.UseFont = true;
            this.labelControl50.Appearance.Options.UseForeColor = true;
            this.labelControl50.Location = new System.Drawing.Point(326, 240);
            this.labelControl50.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl50.Name = "labelControl50";
            this.labelControl50.Size = new System.Drawing.Size(95, 20);
            this.labelControl50.TabIndex = 497;
            this.labelControl50.Text = "Nacionalidad:";
            // 
            // labelControl51
            // 
            this.labelControl51.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl51.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.labelControl51.Appearance.Options.UseFont = true;
            this.labelControl51.Appearance.Options.UseForeColor = true;
            this.labelControl51.Location = new System.Drawing.Point(384, 178);
            this.labelControl51.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl51.Name = "labelControl51";
            this.labelControl51.Size = new System.Drawing.Size(37, 20);
            this.labelControl51.TabIndex = 496;
            this.labelControl51.Text = "Sexo:";
            // 
            // labelControl52
            // 
            this.labelControl52.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl52.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl52.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.labelControl52.Appearance.Options.UseFont = true;
            this.labelControl52.Appearance.Options.UseForeColor = true;
            this.labelControl52.Location = new System.Drawing.Point(325, 88);
            this.labelControl52.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl52.Name = "labelControl52";
            this.labelControl52.Size = new System.Drawing.Size(96, 20);
            this.labelControl52.TabIndex = 495;
            this.labelControl52.Text = "Identificación:";
            // 
            // labelControl45
            // 
            this.labelControl45.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl45.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.labelControl45.Appearance.Options.UseFont = true;
            this.labelControl45.Appearance.Options.UseForeColor = true;
            this.labelControl45.Location = new System.Drawing.Point(339, 303);
            this.labelControl45.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl45.Name = "labelControl45";
            this.labelControl45.Size = new System.Drawing.Size(82, 20);
            this.labelControl45.TabIndex = 493;
            this.labelControl45.Text = "Estado Civil:";
            // 
            // labelControl46
            // 
            this.labelControl46.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl46.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.labelControl46.Appearance.Options.UseFont = true;
            this.labelControl46.Appearance.Options.UseForeColor = true;
            this.labelControl46.Location = new System.Drawing.Point(796, 88);
            this.labelControl46.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl46.Name = "labelControl46";
            this.labelControl46.Size = new System.Drawing.Size(73, 20);
            this.labelControl46.TabIndex = 492;
            this.labelControl46.Text = "Municipio:";
            // 
            // labelControl48
            // 
            this.labelControl48.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl48.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl48.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.labelControl48.Appearance.Options.UseFont = true;
            this.labelControl48.Appearance.Options.UseForeColor = true;
            this.labelControl48.Location = new System.Drawing.Point(233, 119);
            this.labelControl48.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl48.Name = "labelControl48";
            this.labelControl48.Size = new System.Drawing.Size(188, 20);
            this.labelControl48.TabIndex = 489;
            this.labelControl48.Text = "Primer y Segundo Nombre:";
            // 
            // labelControl55
            // 
            this.labelControl55.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl55.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl55.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.labelControl55.Appearance.Options.UseFont = true;
            this.labelControl55.Appearance.Options.UseForeColor = true;
            this.labelControl55.Location = new System.Drawing.Point(233, 150);
            this.labelControl55.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl55.Name = "labelControl55";
            this.labelControl55.Size = new System.Drawing.Size(188, 20);
            this.labelControl55.TabIndex = 490;
            this.labelControl55.Text = "Primer y Segundo Apellido:";
            // 
            // navigationPage5
            // 
            this.navigationPage5.Caption = "navigationPage5";
            this.navigationPage5.Controls.Add(this.labelControl20);
            this.navigationPage5.Controls.Add(this.picFirmaDB);
            this.navigationPage5.Controls.Add(this.panelControl3);
            this.navigationPage5.Controls.Add(this.sbtnBorrarFirma);
            this.navigationPage5.Controls.Add(this.chknoPuedeFirmar);
            this.navigationPage5.Controls.Add(this.panelControl2);
            this.navigationPage5.Margin = new System.Windows.Forms.Padding(4);
            this.navigationPage5.Name = "navigationPage5";
            this.navigationPage5.Size = new System.Drawing.Size(1325, 676);
            // 
            // labelControl20
            // 
            this.labelControl20.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl20.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl20.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelControl20.Appearance.Options.UseFont = true;
            this.labelControl20.Appearance.Options.UseForeColor = true;
            this.labelControl20.Appearance.Options.UseImageAlign = true;
            this.labelControl20.Appearance.Options.UseTextOptions = true;
            this.labelControl20.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl20.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.labelControl20.Location = new System.Drawing.Point(386, 84);
            this.labelControl20.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(81, 20);
            this.labelControl20.TabIndex = 307;
            this.labelControl20.Text = "Firma actual";
            // 
            // picFirmaDB
            // 
            this.picFirmaDB.Cursor = System.Windows.Forms.Cursors.Default;
            this.picFirmaDB.Location = new System.Drawing.Point(386, 104);
            this.picFirmaDB.Margin = new System.Windows.Forms.Padding(4);
            this.picFirmaDB.Name = "picFirmaDB";
            this.picFirmaDB.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.picFirmaDB.Properties.Appearance.Options.UseBorderColor = true;
            this.picFirmaDB.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.picFirmaDB.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picFirmaDB.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.picFirmaDB.Size = new System.Drawing.Size(638, 100);
            this.picFirmaDB.TabIndex = 306;
            // 
            // panelControl3
            // 
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.meObservacionFirma);
            this.panelControl3.Controls.Add(this.label3);
            this.panelControl3.Controls.Add(this.labelControl65);
            this.panelControl3.Location = new System.Drawing.Point(309, 446);
            this.panelControl3.Margin = new System.Windows.Forms.Padding(4);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(818, 125);
            this.panelControl3.TabIndex = 295;
            this.panelControl3.Visible = false;
            // 
            // meObservacionFirma
            // 
            this.meObservacionFirma.Location = new System.Drawing.Point(6, 25);
            this.meObservacionFirma.Margin = new System.Windows.Forms.Padding(4);
            this.meObservacionFirma.Name = "meObservacionFirma";
            this.meObservacionFirma.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.meObservacionFirma.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.meObservacionFirma.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.meObservacionFirma.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.meObservacionFirma.Size = new System.Drawing.Size(805, 96);
            this.meObservacionFirma.TabIndex = 287;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(141, 5);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(16, 17);
            this.label3.TabIndex = 289;
            this.label3.Text = "*";
            // 
            // labelControl65
            // 
            this.labelControl65.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl65.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl65.Appearance.Options.UseFont = true;
            this.labelControl65.Appearance.Options.UseForeColor = true;
            this.labelControl65.Location = new System.Drawing.Point(6, 5);
            this.labelControl65.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl65.Name = "labelControl65";
            this.labelControl65.Size = new System.Drawing.Size(101, 20);
            this.labelControl65.TabIndex = 288;
            this.labelControl65.Text = "Observaciones";
            // 
            // sbtnBorrarFirma
            // 
            this.sbtnBorrarFirma.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.sbtnBorrarFirma.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sbtnBorrarFirma.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.sbtnBorrarFirma.Appearance.Options.UseBackColor = true;
            this.sbtnBorrarFirma.Appearance.Options.UseFont = true;
            this.sbtnBorrarFirma.Appearance.Options.UseForeColor = true;
            this.sbtnBorrarFirma.Appearance.Options.UseTextOptions = true;
            this.sbtnBorrarFirma.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.sbtnBorrarFirma.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.sbtnBorrarFirma.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_eraser_32x32;
            this.sbtnBorrarFirma.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.sbtnBorrarFirma.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.sbtnBorrarFirma.Location = new System.Drawing.Point(388, 391);
            this.sbtnBorrarFirma.Margin = new System.Windows.Forms.Padding(4);
            this.sbtnBorrarFirma.Name = "sbtnBorrarFirma";
            this.sbtnBorrarFirma.Size = new System.Drawing.Size(134, 35);
            this.sbtnBorrarFirma.TabIndex = 239;
            this.sbtnBorrarFirma.Text = "Borrar";
            this.sbtnBorrarFirma.Click += new System.EventHandler(this.sbtnBorrarFirma_Click);
            // 
            // chknoPuedeFirmar
            // 
            this.chknoPuedeFirmar.Location = new System.Drawing.Point(881, 392);
            this.chknoPuedeFirmar.Margin = new System.Windows.Forms.Padding(4);
            this.chknoPuedeFirmar.Name = "chknoPuedeFirmar";
            this.chknoPuedeFirmar.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chknoPuedeFirmar.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.chknoPuedeFirmar.Properties.Appearance.Options.UseFont = true;
            this.chknoPuedeFirmar.Properties.Appearance.Options.UseForeColor = true;
            this.chknoPuedeFirmar.Properties.Caption = "No puede firmar";
            this.chknoPuedeFirmar.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.SvgToggle1;
            this.chknoPuedeFirmar.Properties.CheckBoxOptions.SvgColorChecked = DevExpress.LookAndFeel.DXSkinColors.ForeColors.Information;
            this.chknoPuedeFirmar.Properties.CheckBoxOptions.SvgColorUnchecked = DevExpress.LookAndFeel.DXSkinColors.ForeColors.DisabledText;
            this.chknoPuedeFirmar.Size = new System.Drawing.Size(149, 27);
            this.chknoPuedeFirmar.TabIndex = 240;
            // 
            // panelControl2
            // 
            this.panelControl2.Appearance.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.panelControl2.Appearance.Options.UseBackColor = true;
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.panelControl2.Controls.Add(this.sigPlusNET1);
            this.panelControl2.Location = new System.Drawing.Point(388, 215);
            this.panelControl2.LookAndFeel.SkinName = "Blue";
            this.panelControl2.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelControl2.Margin = new System.Windows.Forms.Padding(4);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(636, 169);
            this.panelControl2.TabIndex = 236;
            // 
            // sigPlusNET1
            // 
            this.sigPlusNET1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sigPlusNET1.BackColor = System.Drawing.Color.White;
            this.sigPlusNET1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.sigPlusNET1.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.sigPlusNET1.ForeColor = System.Drawing.Color.Black;
            this.sigPlusNET1.Location = new System.Drawing.Point(29, 19);
            this.sigPlusNET1.Margin = new System.Windows.Forms.Padding(4);
            this.sigPlusNET1.Name = "sigPlusNET1";
            this.sigPlusNET1.Size = new System.Drawing.Size(586, 131);
            this.sigPlusNET1.TabIndex = 14;
            this.sigPlusNET1.Text = "sigPlusNET1";
            // 
            // panelControl4
            // 
            this.panelControl4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl4.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.panelControl4.Appearance.Options.UseBackColor = true;
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Controls.Add(this.picPadFirma);
            this.panelControl4.Controls.Add(this.picCamara);
            this.panelControl4.Controls.Add(this.btnGuardar);
            this.panelControl4.Location = new System.Drawing.Point(0, 787);
            this.panelControl4.LookAndFeel.SkinName = "Blue";
            this.panelControl4.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelControl4.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(1366, 52);
            this.panelControl4.TabIndex = 118;
            // 
            // picPadFirma
            // 
            this.picPadFirma.Cursor = System.Windows.Forms.Cursors.Default;
            this.picPadFirma.Location = new System.Drawing.Point(53, 7);
            this.picPadFirma.Margin = new System.Windows.Forms.Padding(4);
            this.picPadFirma.Name = "picPadFirma";
            this.picPadFirma.Properties.AllowFocused = false;
            this.picPadFirma.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picPadFirma.Properties.Appearance.Options.UseBackColor = true;
            this.picPadFirma.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.picPadFirma.Properties.NullText = " ";
            this.picPadFirma.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picPadFirma.Properties.ShowMenu = false;
            this.picPadFirma.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.picPadFirma.Size = new System.Drawing.Size(38, 38);
            this.picPadFirma.TabIndex = 420;
            // 
            // picCamara
            // 
            this.picCamara.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.picCamara.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.picCamara.Cursor = System.Windows.Forms.Cursors.Default;
            this.picCamara.Location = new System.Drawing.Point(7, 8);
            this.picCamara.Margin = new System.Windows.Forms.Padding(4);
            this.picCamara.Name = "picCamara";
            this.picCamara.Properties.AllowFocused = false;
            this.picCamara.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picCamara.Properties.Appearance.Options.UseBackColor = true;
            this.picCamara.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.picCamara.Properties.NullText = "Cámara Inicializada";
            this.picCamara.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picCamara.Properties.ShowMenu = false;
            this.picCamara.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.picCamara.Size = new System.Drawing.Size(38, 38);
            this.picCamara.TabIndex = 419;
            // 
            // btnGuardar
            // 
            this.btnGuardar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGuardar.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardar.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.btnGuardar.Appearance.Options.UseFont = true;
            this.btnGuardar.Appearance.Options.UseForeColor = true;
            this.btnGuardar.AppearanceHovered.Font = new System.Drawing.Font("Tahoma", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.btnGuardar.AppearanceHovered.Options.UseFont = true;
            this.btnGuardar.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnGuardar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGuardar.Enabled = false;
            this.btnGuardar.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.save_32x32;
            this.btnGuardar.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnGuardar.Location = new System.Drawing.Point(1160, 5);
            this.btnGuardar.LookAndFeel.SkinName = "Office 2016 Colorful";
            this.btnGuardar.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnGuardar.Margin = new System.Windows.Forms.Padding(4);
            this.btnGuardar.MaximumSize = new System.Drawing.Size(165, 41);
            this.btnGuardar.MinimumSize = new System.Drawing.Size(165, 41);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(165, 41);
            this.btnGuardar.TabIndex = 418;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // frmEnrolamientoActualizacion
            // 
            this.Appearance.BackColor = System.Drawing.Color.White;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(1366, 839);
            this.Controls.Add(this.panelControl4);
            this.Controls.Add(this.tabEnrolamiento);
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEnrolamientoActualizacion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmEnrolamientoActualizacion";
            this.Load += new System.EventHandler(this.frmEnrolamientoActualizacion_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picNumeros.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picManoDerecha.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picManoIzquierda.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picIndiceDerecho.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPulgarDerecho.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMedioDerecho.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAnularDerecho.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAnularIzquierdo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMeniequeIzquierdo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMedioIzquierdo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picIndiceIzquierdo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabEnrolamiento)).EndInit();
            this.tabEnrolamiento.ResumeLayout(false);
            this.tabBiograficos.ResumeLayout(false);
            this.tabBiograficos.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCorreoTrabajo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueColonia.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueMunicipios.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueDepartamentos.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meDireccionTrabajo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTelefonoTrabajo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueProfesion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueNivelEducativo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luePaisNacimiento.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meDireccionCompleta.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMovil.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTelefono.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueSexo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtmFechaNacimiento.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtmFechaNacimiento.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCorreo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueNacionalidad.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueEstadoCivil.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTipoDocumento.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdentificacion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrimerNombre.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSegundoNombre.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrimerApellido.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSegundoApellido.Properties)).EndInit();
            this.navigationPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlFotoDB)).EndInit();
            this.pnlFotoDB.ResumeLayout(false);
            this.pnlFotoDB.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picFotoDB.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFotoCapturada.Properties)).EndInit();
            this.navigationPage2.ResumeLayout(false);
            this.navigationPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picFotoFinal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reviewSignature.Properties)).EndInit();
            this.navigationPage5.ResumeLayout(false);
            this.navigationPage5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picFirmaDB.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.meObservacionFirma.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chknoPuedeFirmar.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picPadFirma.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCamara.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl lblNombreFormulario;
        private DevExpress.XtraEditors.SimpleButton btnSalir;
        private DevExpress.XtraEditors.LabelControl lblTituloAccion;
        private DevExpress.XtraEditors.SimpleButton sbtnSiguiente;
        private DevExpress.XtraEditors.SimpleButton sbtnAnterior;
        private DevExpress.XtraEditors.LabelControl lblDetalles;
        private DevExpress.XtraEditors.PictureEdit picNumeros;
        private DevExpress.XtraEditors.PictureEdit picManoDerecha;
        private DevExpress.XtraEditors.PictureEdit picManoIzquierda;
        private DevExpress.XtraEditors.PictureEdit picIndiceDerecho;
        private DevExpress.XtraEditors.PictureEdit picPulgarDerecho;
        private DevExpress.XtraEditors.PictureEdit picMedioDerecho;
        private DevExpress.XtraEditors.PictureEdit picAnularDerecho;
        private DevExpress.XtraEditors.PictureEdit picAnularIzquierdo;
        private DevExpress.XtraEditors.PictureEdit picMeniequeIzquierdo;
        private DevExpress.XtraEditors.PictureEdit picMedioIzquierdo;
        private DevExpress.XtraEditors.PictureEdit picIndiceIzquierdo;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraBars.Navigation.NavigationFrame tabEnrolamiento;
        private DevExpress.XtraBars.Navigation.NavigationPage tabBiograficos;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl62;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl61;
        private DevExpress.XtraEditors.LabelControl labelControl59;
        private DevExpress.XtraEditors.LabelControl labelControl60;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.LabelControl labelControl58;
        private DevExpress.XtraEditors.LabelControl labelControl57;
        private DevExpress.XtraEditors.LabelControl labelControl54;
        private DevExpress.XtraEditors.LabelControl labelControl53;
        private DevExpress.XtraEditors.LabelControl labelControl37;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.SimpleButton btnTest;
        private DevExpress.XtraEditors.TextEdit txtCorreoTrabajo;
        private DevExpress.XtraEditors.LookUpEdit lueColonia;
        private DevExpress.XtraEditors.LookUpEdit lueMunicipios;
        private DevExpress.XtraEditors.LookUpEdit lueDepartamentos;
        private DevExpress.XtraEditors.LabelControl labelControl42;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private System.Windows.Forms.Label label28;
        private DevExpress.XtraEditors.MemoEdit meDireccionTrabajo;
        private DevExpress.XtraEditors.TextEdit txtTelefonoTrabajo;
        private System.Windows.Forms.Label label22;
        private DevExpress.XtraEditors.LookUpEdit lueProfesion;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.LookUpEdit lueNivelEducativo;
        private DevExpress.XtraEditors.LookUpEdit luePaisNacimiento;
        private DevExpress.XtraEditors.MemoEdit meDireccionCompleta;
        private DevExpress.XtraEditors.TextEdit txtMovil;
        private DevExpress.XtraEditors.TextEdit txtTelefono;
        private DevExpress.XtraEditors.LookUpEdit lueSexo;
        private DevExpress.XtraEditors.DateEdit dtmFechaNacimiento;
        private DevExpress.XtraEditors.TextEdit txtCorreo;
        private DevExpress.XtraEditors.LookUpEdit lueNacionalidad;
        private DevExpress.XtraEditors.LookUpEdit lueEstadoCivil;
        private DevExpress.XtraEditors.LookUpEdit lueTipoDocumento;
        private DevExpress.XtraEditors.TextEdit txtIdentificacion;
        private DevExpress.XtraEditors.TextEdit txtPrimerNombre;
        private DevExpress.XtraEditors.TextEdit txtSegundoNombre;
        private DevExpress.XtraEditors.TextEdit txtPrimerApellido;
        private DevExpress.XtraEditors.TextEdit txtSegundoApellido;
        private DevExpress.XtraBars.Navigation.NavigationPage navigationPage1;
        private DevExpress.XtraEditors.PanelControl pnlFotoDB;
        private DevExpress.XtraEditors.PictureEdit picFotoDB;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.PictureEdit picFotoCapturada;
        private DevExpress.XtraEditors.SimpleButton sbtnIniciarCamara;
        private DevExpress.XtraBars.Navigation.NavigationPage navigationPage2;
        private DevExpress.XtraEditors.LabelControl labelControl41;
        private DevExpress.XtraEditors.LabelControl labelControl63;
        private DevExpress.XtraEditors.LabelControl labelControl56;
        private DevExpress.XtraEditors.LabelControl lblNivelEducativo;
        private DevExpress.XtraEditors.LabelControl labelControl135;
        private DevExpress.XtraEditors.LabelControl lblProfesionOficio;
        private DevExpress.XtraEditors.LabelControl labelControl100;
        private DevExpress.XtraEditors.LabelControl lblPaisNacimiento;
        private DevExpress.XtraEditors.LabelControl labelControl132;
        private DevExpress.XtraEditors.LabelControl lblDireccionResidencia;
        private DevExpress.XtraEditors.LabelControl lblMovil;
        private DevExpress.XtraEditors.LabelControl labelControl110;
        private DevExpress.XtraEditors.LabelControl labelControl108;
        private DevExpress.XtraEditors.LabelControl lblTelefono;
        private DevExpress.XtraEditors.LabelControl lblColoniaCasa;
        private DevExpress.XtraEditors.LabelControl labelControl104;
        private DevExpress.XtraEditors.LabelControl labelControl103;
        private DevExpress.XtraEditors.LabelControl lblCorreoElectronico;
        private DevExpress.XtraEditors.LabelControl labelControl101;
        private DevExpress.XtraEditors.LabelControl lblNacionalidad;
        private DevExpress.XtraEditors.LabelControl lblNacimiento;
        private DevExpress.XtraEditors.LabelControl lblSexo;
        private DevExpress.XtraEditors.LabelControl lblIdentificacion;
        private DevExpress.XtraEditors.LabelControl lblEstadoCivil;
        private DevExpress.XtraEditors.LabelControl lblMunicipio;
        private DevExpress.XtraEditors.LabelControl lblDepartamento;
        private DevExpress.XtraEditors.LabelControl lblApellidos;
        private DevExpress.XtraEditors.LabelControl lblNombres;
        private DevExpress.XtraEditors.LabelControl lblTipoDocumento;
        private DevExpress.XtraEditors.LabelControl labelControl49;
        private DevExpress.XtraEditors.LabelControl labelControl50;
        private DevExpress.XtraEditors.LabelControl labelControl51;
        private DevExpress.XtraEditors.LabelControl labelControl52;
        private DevExpress.XtraEditors.LabelControl labelControl44;
        private DevExpress.XtraEditors.LabelControl labelControl45;
        private DevExpress.XtraEditors.LabelControl labelControl46;
        private DevExpress.XtraEditors.LabelControl labelControl47;
        private DevExpress.XtraEditors.LabelControl labelControl48;
        private DevExpress.XtraEditors.LabelControl labelControl55;
        private DevExpress.XtraEditors.PictureEdit picFotoFinal;
        private DevExpress.XtraBars.Navigation.NavigationPage navigationPage5;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.PictureEdit picFirmaDB;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.MemoEdit meObservacionFirma;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.LabelControl labelControl65;
        private DevExpress.XtraEditors.SimpleButton sbtnBorrarFirma;
        private DevExpress.XtraEditors.CheckEdit chknoPuedeFirmar;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private Topaz.SigPlusNET sigPlusNET1;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.PictureEdit picCamara;
        private DevExpress.XtraEditors.SimpleButton btnGuardar;
        private DevExpress.XtraSplashScreen.SplashScreenManager ssForm;
        private DevExpress.XtraEditors.PictureEdit picPadFirma;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.PictureEdit reviewSignature;
        private DevExpress.XtraEditors.SimpleButton btnRotar;
        private DevExpress.XtraEditors.LabelControl labelControl69;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.LabelControl labelControl25;
    }
}