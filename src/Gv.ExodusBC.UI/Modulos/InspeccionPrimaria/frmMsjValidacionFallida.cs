﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gv.ExodusBc.UI.Modulos.InspeccionPrimaria
{
   
    public partial class frmMsjValidacionFallida : Form
    {

        public frmMsjValidacionFallida(string mensaje, string footer)
        {
            InitializeComponent();

            //this.ShowHeaderMaxButton = false;
            //this.ShowHeaderMinButton = false;
            //this.ShowHeaderCloseButton = false;
            //this.AllowWindowsResizing = false;

            this.lblMensaje2.Text = mensaje;
            this.lblRechazaVerificacion.Text = footer;

        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
