﻿namespace Gv.ExodusBc.UI.Modulos.InspeccionPrimaria
{
    partial class frmEnrolamientoVistaPrevia
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEnrolamientoVistaPrevia));
            this.pnlBotones = new System.Windows.Forms.Panel();
            this.btnTamanioReal = new DevExpress.XtraEditors.SimpleButton();
            this.btnAjustarTamanio = new DevExpress.XtraEditors.SimpleButton();
            this.btnRotarIzquierda = new DevExpress.XtraEditors.SimpleButton();
            this.btnRotarDerecha = new DevExpress.XtraEditors.SimpleButton();
            this.lblZoom = new System.Windows.Forms.Label();
            this.tbZoomBar = new System.Windows.Forms.TrackBar();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.ptbPrevia = new DevExpress.XtraEditors.PictureEdit();
            this.pnlBotones.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbZoomBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ptbPrevia.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlBotones
            // 
            this.pnlBotones.Controls.Add(this.btnTamanioReal);
            this.pnlBotones.Controls.Add(this.btnAjustarTamanio);
            this.pnlBotones.Controls.Add(this.btnRotarIzquierda);
            this.pnlBotones.Controls.Add(this.btnRotarDerecha);
            this.pnlBotones.Controls.Add(this.lblZoom);
            this.pnlBotones.Controls.Add(this.tbZoomBar);
            this.pnlBotones.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlBotones.Location = new System.Drawing.Point(2, 2);
            this.pnlBotones.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pnlBotones.Name = "pnlBotones";
            this.pnlBotones.Size = new System.Drawing.Size(1140, 88);
            this.pnlBotones.TabIndex = 2;
            // 
            // btnTamanioReal
            // 
            this.btnTamanioReal.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btnTamanioReal.Appearance.Options.UseForeColor = true;
            this.btnTamanioReal.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnTamanioReal.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.selectall_16x16;
            this.btnTamanioReal.Location = new System.Drawing.Point(173, 48);
            this.btnTamanioReal.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnTamanioReal.Name = "btnTamanioReal";
            this.btnTamanioReal.Size = new System.Drawing.Size(35, 36);
            this.btnTamanioReal.TabIndex = 290;
            this.btnTamanioReal.Click += new System.EventHandler(this.btnTamanioReal_Click);
            // 
            // btnAjustarTamanio
            // 
            this.btnAjustarTamanio.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btnAjustarTamanio.Appearance.Options.UseForeColor = true;
            this.btnAjustarTamanio.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnAjustarTamanio.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_zoommore24x24;
            this.btnAjustarTamanio.Location = new System.Drawing.Point(131, 48);
            this.btnAjustarTamanio.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnAjustarTamanio.Name = "btnAjustarTamanio";
            this.btnAjustarTamanio.Size = new System.Drawing.Size(35, 36);
            this.btnAjustarTamanio.TabIndex = 289;
            this.btnAjustarTamanio.Click += new System.EventHandler(this.btnAjustarTamanio_Click);
            // 
            // btnRotarIzquierda
            // 
            this.btnRotarIzquierda.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btnRotarIzquierda.Appearance.Options.UseForeColor = true;
            this.btnRotarIzquierda.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnRotarIzquierda.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.redo_16x16;
            this.btnRotarIzquierda.Location = new System.Drawing.Point(57, 48);
            this.btnRotarIzquierda.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnRotarIzquierda.Name = "btnRotarIzquierda";
            this.btnRotarIzquierda.Size = new System.Drawing.Size(35, 36);
            this.btnRotarIzquierda.TabIndex = 288;
            this.btnRotarIzquierda.Click += new System.EventHandler(this.btnRotarIzquierda_Click);
            // 
            // btnRotarDerecha
            // 
            this.btnRotarDerecha.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btnRotarDerecha.Appearance.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_rotarizquierda_32x32;
            this.btnRotarDerecha.Appearance.Options.UseForeColor = true;
            this.btnRotarDerecha.Appearance.Options.UseImage = true;
            this.btnRotarDerecha.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnRotarDerecha.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.undo_16x162;
            this.btnRotarDerecha.Location = new System.Drawing.Point(15, 48);
            this.btnRotarDerecha.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnRotarDerecha.Name = "btnRotarDerecha";
            this.btnRotarDerecha.Size = new System.Drawing.Size(35, 36);
            this.btnRotarDerecha.TabIndex = 287;
            this.btnRotarDerecha.Click += new System.EventHandler(this.btnRotarDerecha_Click);
            // 
            // lblZoom
            // 
            this.lblZoom.AutoSize = true;
            this.lblZoom.Location = new System.Drawing.Point(712, 44);
            this.lblZoom.Name = "lblZoom";
            this.lblZoom.Size = new System.Drawing.Size(30, 17);
            this.lblZoom.TabIndex = 48;
            this.lblZoom.Text = "0%";
            // 
            // tbZoomBar
            // 
            this.tbZoomBar.Location = new System.Drawing.Point(451, 25);
            this.tbZoomBar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbZoomBar.Maximum = 1000;
            this.tbZoomBar.Name = "tbZoomBar";
            this.tbZoomBar.Size = new System.Drawing.Size(234, 56);
            this.tbZoomBar.TabIndex = 47;
            this.tbZoomBar.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.tbZoomBar.Value = 100;
            this.tbZoomBar.Scroll += new System.EventHandler(this.tbZoomBar_Scroll);
            // 
            // panelControl1
            // 
            this.panelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl1.Controls.Add(this.pnlBotones);
            this.panelControl1.Location = new System.Drawing.Point(1, 1);
            this.panelControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1144, 92);
            this.panelControl1.TabIndex = 3;
            // 
            // panelControl2
            // 
            this.panelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl2.Controls.Add(this.ptbPrevia);
            this.panelControl2.Location = new System.Drawing.Point(3, 98);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1140, 597);
            this.panelControl2.TabIndex = 4;
            // 
            // ptbPrevia
            // 
            this.ptbPrevia.Cursor = System.Windows.Forms.Cursors.Default;
            this.ptbPrevia.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ptbPrevia.Location = new System.Drawing.Point(2, 2);
            this.ptbPrevia.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ptbPrevia.Name = "ptbPrevia";
            this.ptbPrevia.Properties.AllowScrollViaMouseDrag = true;
            this.ptbPrevia.Properties.AllowZoomOnMouseWheel = DevExpress.Utils.DefaultBoolean.True;
            this.ptbPrevia.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.ptbPrevia.Properties.ShowScrollBars = true;
            this.ptbPrevia.Properties.ShowZoomSubMenu = DevExpress.Utils.DefaultBoolean.True;
            this.ptbPrevia.Size = new System.Drawing.Size(1136, 593);
            this.ptbPrevia.TabIndex = 1;
            // 
            // frmEnrolamientoVistaPrevia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1144, 695);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmEnrolamientoVistaPrevia";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Vista previa de documentos";
            this.Load += new System.EventHandler(this.frmEnrolamientoVistaPrevia_Load);
            this.pnlBotones.ResumeLayout(false);
            this.pnlBotones.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbZoomBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ptbPrevia.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Panel pnlBotones;
        internal DevExpress.XtraEditors.SimpleButton btnTamanioReal;
        internal DevExpress.XtraEditors.SimpleButton btnAjustarTamanio;
        internal DevExpress.XtraEditors.SimpleButton btnRotarIzquierda;
        internal DevExpress.XtraEditors.SimpleButton btnRotarDerecha;
        private System.Windows.Forms.Label lblZoom;
        private System.Windows.Forms.TrackBar tbZoomBar;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        internal DevExpress.XtraEditors.PictureEdit ptbPrevia;
    }
}