﻿namespace Gv.ExodusBc.UI.Modulos.InspeccionPrimaria
{
    partial class frmInsPrimariaEntrada
    {
        private System.ComponentModel.IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null)){components.Dispose();}
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmInsPrimariaEntrada));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnSalir = new DevExpress.XtraEditors.SimpleButton();
            this.lblTituloAccion = new DevExpress.XtraEditors.LabelControl();
            this.sbtnSiguiente = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnAnterior = new DevExpress.XtraEditors.SimpleButton();
            this.lblDetalles = new DevExpress.XtraEditors.LabelControl();
            this.picNumeros = new DevExpress.XtraEditors.PictureEdit();
            this.lblNombreFormulario = new DevExpress.XtraEditors.LabelControl();
            this.xtraScrollableControl1 = new DevExpress.XtraEditors.XtraScrollableControl();
            this.picAtencion = new DevExpress.XtraEditors.PictureEdit();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.btnSyncCatalogos = new DevExpress.XtraEditors.SimpleButton();
            this.picPadFirma = new DevExpress.XtraEditors.PictureEdit();
            this.picCS500e = new DevExpress.XtraEditors.PictureEdit();
            this.picCamara = new DevExpress.XtraEditors.PictureEdit();
            this.pnlVerificacionHuellas = new DevExpress.XtraEditors.PanelControl();
            this.lblGifValidando = new DevExpress.XtraEditors.LabelControl();
            this.lblVerificarHuella = new DevExpress.XtraEditors.LabelControl();
            this.btnGuardar = new DevExpress.XtraEditors.SimpleButton();
            this.lblEstadoValidacion = new DevExpress.XtraEditors.LabelControl();
            this.tabEnrolamiento = new DevExpress.XtraBars.Navigation.NavigationFrame();
            this.navigationPage4 = new DevExpress.XtraBars.Navigation.NavigationPage();
            this.btnReiniciarEscaner = new DevExpress.XtraEditors.SimpleButton();
            this.pnlObservacionesPersonaHuellaEstado = new DevExpress.XtraEditors.PanelControl();
            this.label17 = new System.Windows.Forms.Label();
            this.meObservacionHuellas = new DevExpress.XtraEditors.MemoEdit();
            this.lblObservacionesHuellas = new DevExpress.XtraEditors.LabelControl();
            this.luePersonaHuellaEstado = new DevExpress.XtraEditors.LookUpEdit();
            this.lblNumeroTransaccion = new DevExpress.XtraEditors.LabelControl();
            this.labelControl98 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl78 = new DevExpress.XtraEditors.LabelControl();
            this.sbtnCapturaHuellas = new DevExpress.XtraEditors.SimpleButton();
            this.lblDedo5 = new DevExpress.XtraEditors.LabelControl();
            this.prgbDedo5 = new DevExpress.XtraEditors.ProgressBarControl();
            this.lblDedo4 = new DevExpress.XtraEditors.LabelControl();
            this.prgbDedo4 = new DevExpress.XtraEditors.ProgressBarControl();
            this.lblDedo3 = new DevExpress.XtraEditors.LabelControl();
            this.prgbDedo3 = new DevExpress.XtraEditors.ProgressBarControl();
            this.lblDedo2 = new DevExpress.XtraEditors.LabelControl();
            this.prgbDedo2 = new DevExpress.XtraEditors.ProgressBarControl();
            this.lblDedo1 = new DevExpress.XtraEditors.LabelControl();
            this.prgbDedo1 = new DevExpress.XtraEditors.ProgressBarControl();
            this.lblDedo10 = new DevExpress.XtraEditors.LabelControl();
            this.prgbDedo10 = new DevExpress.XtraEditors.ProgressBarControl();
            this.lblDedo9 = new DevExpress.XtraEditors.LabelControl();
            this.prgbDedo9 = new DevExpress.XtraEditors.ProgressBarControl();
            this.lblDedo8 = new DevExpress.XtraEditors.LabelControl();
            this.prgbDedo8 = new DevExpress.XtraEditors.ProgressBarControl();
            this.lblDedo7 = new DevExpress.XtraEditors.LabelControl();
            this.prgbDedo7 = new DevExpress.XtraEditors.ProgressBarControl();
            this.picSeleccionDerecha = new DevExpress.XtraEditors.PictureEdit();
            this.pnlManoDerecha = new DevExpress.XtraEditors.PanelControl();
            this.lblDedosDerecha = new DevExpress.XtraEditors.LabelControl();
            this.labelControl38 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl39 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit13 = new DevExpress.XtraEditors.PictureEdit();
            this.prgbDerechoGrande = new DevExpress.XtraEditors.ProgressBarControl();
            this.picSeleccionIzquierda = new DevExpress.XtraEditors.PictureEdit();
            this.pnlManoIzquierda = new DevExpress.XtraEditors.PanelControl();
            this.lblDedosIzquierda = new DevExpress.XtraEditors.LabelControl();
            this.labelControl36 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl29 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit12 = new DevExpress.XtraEditors.PictureEdit();
            this.prgbIzquierdoGrande = new DevExpress.XtraEditors.ProgressBarControl();
            this.labelControl35 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl34 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl33 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl32 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl31 = new DevExpress.XtraEditors.LabelControl();
            this.lblDedo6 = new DevExpress.XtraEditors.LabelControl();
            this.prgbDedo6 = new DevExpress.XtraEditors.ProgressBarControl();
            this.picPulgarIzquierdo = new DevExpress.XtraEditors.PictureEdit();
            this.picMeniequeDerecho = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl30 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl28 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl27 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl26 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.picIndiceIzquierdo = new DevExpress.XtraEditors.PictureEdit();
            this.picMedioIzquierdo = new DevExpress.XtraEditors.PictureEdit();
            this.picMeniequeIzquierdo = new DevExpress.XtraEditors.PictureEdit();
            this.picAnularIzquierdo = new DevExpress.XtraEditors.PictureEdit();
            this.picAnularDerecho = new DevExpress.XtraEditors.PictureEdit();
            this.picMedioDerecho = new DevExpress.XtraEditors.PictureEdit();
            this.picPulgarDerecho = new DevExpress.XtraEditors.PictureEdit();
            this.picIndiceDerecho = new DevExpress.XtraEditors.PictureEdit();
            this.picManoIzquierda = new DevExpress.XtraEditors.PictureEdit();
            this.picManoDerecha = new DevExpress.XtraEditors.PictureEdit();
            this.navigationPage1 = new DevExpress.XtraBars.Navigation.NavigationPage();
            this.btnRotar = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnIniciarCamara = new DevExpress.XtraEditors.SimpleButton();
            this.btnAdjuntarFoto = new DevExpress.XtraEditors.SimpleButton();
            this.picFotoCapturada = new DevExpress.XtraEditors.PictureEdit();
            this.pnlObservacionFoto = new DevExpress.XtraEditors.PanelControl();
            this.mmObservacionFoto = new DevExpress.XtraEditors.MemoEdit();
            this.label25 = new System.Windows.Forms.Label();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.navigationPage2 = new DevExpress.XtraBars.Navigation.NavigationPage();
            this.labelControl72 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl73 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl71 = new DevExpress.XtraEditors.LabelControl();
            this.lblDepartamento = new DevExpress.XtraEditors.LabelControl();
            this.labelControl46 = new DevExpress.XtraEditors.LabelControl();
            this.lblTipoDocumento = new DevExpress.XtraEditors.LabelControl();
            this.labelControl43 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl70 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl68 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl64 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit2 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl40 = new DevExpress.XtraEditors.LabelControl();
            this.picFotoFinal = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl67 = new DevExpress.XtraEditors.LabelControl();
            this.reviewSignature = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl41 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl63 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl56 = new DevExpress.XtraEditors.LabelControl();
            this.picDedo2Final = new DevExpress.XtraEditors.PictureEdit();
            this.lblNivelEducativo = new DevExpress.XtraEditors.LabelControl();
            this.labelControl135 = new DevExpress.XtraEditors.LabelControl();
            this.lblProfesionOficio = new DevExpress.XtraEditors.LabelControl();
            this.labelControl100 = new DevExpress.XtraEditors.LabelControl();
            this.lblPaisNacimiento = new DevExpress.XtraEditors.LabelControl();
            this.labelControl132 = new DevExpress.XtraEditors.LabelControl();
            this.lblDireccionResidencia = new DevExpress.XtraEditors.LabelControl();
            this.lblMovil = new DevExpress.XtraEditors.LabelControl();
            this.labelControl110 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl108 = new DevExpress.XtraEditors.LabelControl();
            this.lblTelefono = new DevExpress.XtraEditors.LabelControl();
            this.lblColoniaCasa = new DevExpress.XtraEditors.LabelControl();
            this.labelControl104 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl103 = new DevExpress.XtraEditors.LabelControl();
            this.lblCorreoElectronico = new DevExpress.XtraEditors.LabelControl();
            this.labelControl101 = new DevExpress.XtraEditors.LabelControl();
            this.lblNacionalidad = new DevExpress.XtraEditors.LabelControl();
            this.lblNacimiento = new DevExpress.XtraEditors.LabelControl();
            this.lblSexo = new DevExpress.XtraEditors.LabelControl();
            this.lblIdentificacion = new DevExpress.XtraEditors.LabelControl();
            this.lblEstadoCivil = new DevExpress.XtraEditors.LabelControl();
            this.lblMunicipio = new DevExpress.XtraEditors.LabelControl();
            this.lblApellidos = new DevExpress.XtraEditors.LabelControl();
            this.lblNombres = new DevExpress.XtraEditors.LabelControl();
            this.labelControl49 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl50 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl51 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl52 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl44 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl45 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl47 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl48 = new DevExpress.XtraEditors.LabelControl();
            this.picDedo6Final = new DevExpress.XtraEditors.PictureEdit();
            this.picDedo5Final = new DevExpress.XtraEditors.PictureEdit();
            this.picDedo7Final = new DevExpress.XtraEditors.PictureEdit();
            this.picDedo8Final = new DevExpress.XtraEditors.PictureEdit();
            this.picDedo10Final = new DevExpress.XtraEditors.PictureEdit();
            this.picDedo9Final = new DevExpress.XtraEditors.PictureEdit();
            this.picDedo4Final = new DevExpress.XtraEditors.PictureEdit();
            this.picDedo3Final = new DevExpress.XtraEditors.PictureEdit();
            this.picDedo1Final = new DevExpress.XtraEditors.PictureEdit();
            this.navigationPage5 = new DevExpress.XtraBars.Navigation.NavigationPage();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.sigPlusNET1 = new Topaz.SigPlusNET();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.meObservacionFirma = new DevExpress.XtraEditors.MemoEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.labelControl55 = new DevExpress.XtraEditors.LabelControl();
            this.sbtnBorrarFirma = new DevExpress.XtraEditors.SimpleButton();
            this.chknoPuedeFirmar = new DevExpress.XtraEditors.CheckEdit();
            this.tabBiograficos = new DevExpress.XtraBars.Navigation.NavigationPage();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl61 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl59 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl54 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl37 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.label1 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.labelControl66 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl69 = new DevExpress.XtraEditors.LabelControl();
            this.luePaisResidencia = new DevExpress.XtraEditors.LookUpEdit();
            this.chkNoPresencial = new DevExpress.XtraEditors.CheckEdit();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.label7 = new System.Windows.Forms.Label();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.txtCorreoTrabajo = new DevExpress.XtraEditors.TextEdit();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl42 = new DevExpress.XtraEditors.LabelControl();
            this.meDireccionTrabajo = new DevExpress.XtraEditors.MemoEdit();
            this.txtTelefonoTrabajo = new DevExpress.XtraEditors.TextEdit();
            this.lueProfesion = new DevExpress.XtraEditors.LookUpEdit();
            this.lueNivelEducativo = new DevExpress.XtraEditors.LookUpEdit();
            this.meDireccionCompleta = new DevExpress.XtraEditors.MemoEdit();
            this.txtTelefono = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.labelControl62 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl60 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl58 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl57 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.lueColonia = new DevExpress.XtraEditors.LookUpEdit();
            this.lueMunicipios = new DevExpress.XtraEditors.LookUpEdit();
            this.lueDepartamentos = new DevExpress.XtraEditors.LookUpEdit();
            this.label28 = new System.Windows.Forms.Label();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtMovil = new DevExpress.XtraEditors.TextEdit();
            this.txtCorreo = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lueEstadoCivil = new DevExpress.XtraEditors.LookUpEdit();
            this.lueSexo = new DevExpress.XtraEditors.LookUpEdit();
            this.label23 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.labelControl53 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.luePaisNacimiento = new DevExpress.XtraEditors.LookUpEdit();
            this.dtmFechaNacimiento = new DevExpress.XtraEditors.DateEdit();
            this.lueNacionalidad = new DevExpress.XtraEditors.LookUpEdit();
            this.lueTipoDocumento = new DevExpress.XtraEditors.LookUpEdit();
            this.txtIdentificacion = new DevExpress.XtraEditors.TextEdit();
            this.txtPrimerNombre = new DevExpress.XtraEditors.TextEdit();
            this.txtSegundoNombre = new DevExpress.XtraEditors.TextEdit();
            this.txtPrimerApellido = new DevExpress.XtraEditors.TextEdit();
            this.txtSegundoApellido = new DevExpress.XtraEditors.TextEdit();
            this.labelControl65 = new DevExpress.XtraEditors.LabelControl();
            this.ssForm = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::Gv.ExodusBc.UI.frmWaitForm), true, true);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picNumeros.Properties)).BeginInit();
            this.xtraScrollableControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picAtencion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picPadFirma.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCS500e.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCamara.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlVerificacionHuellas)).BeginInit();
            this.pnlVerificacionHuellas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabEnrolamiento)).BeginInit();
            this.tabEnrolamiento.SuspendLayout();
            this.navigationPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlObservacionesPersonaHuellaEstado)).BeginInit();
            this.pnlObservacionesPersonaHuellaEstado.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.meObservacionHuellas.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luePersonaHuellaEstado.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prgbDedo5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prgbDedo4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prgbDedo3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prgbDedo2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prgbDedo1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prgbDedo10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prgbDedo9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prgbDedo8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prgbDedo7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSeleccionDerecha.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlManoDerecha)).BeginInit();
            this.pnlManoDerecha.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prgbDerechoGrande.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSeleccionIzquierda.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlManoIzquierda)).BeginInit();
            this.pnlManoIzquierda.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prgbIzquierdoGrande.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prgbDedo6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPulgarIzquierdo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMeniequeDerecho.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picIndiceIzquierdo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMedioIzquierdo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMeniequeIzquierdo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAnularIzquierdo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAnularDerecho.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMedioDerecho.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPulgarDerecho.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picIndiceDerecho.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picManoIzquierda.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picManoDerecha.Properties)).BeginInit();
            this.navigationPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picFotoCapturada.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlObservacionFoto)).BeginInit();
            this.pnlObservacionFoto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mmObservacionFoto.Properties)).BeginInit();
            this.navigationPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFotoFinal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reviewSignature.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDedo2Final.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDedo6Final.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDedo5Final.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDedo7Final.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDedo8Final.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDedo10Final.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDedo9Final.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDedo4Final.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDedo3Final.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDedo1Final.Properties)).BeginInit();
            this.navigationPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.meObservacionFirma.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chknoPuedeFirmar.Properties)).BeginInit();
            this.tabBiograficos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.luePaisResidencia.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNoPresencial.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCorreoTrabajo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meDireccionTrabajo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTelefonoTrabajo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueProfesion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueNivelEducativo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meDireccionCompleta.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTelefono.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueColonia.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueMunicipios.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueDepartamentos.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMovil.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCorreo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueEstadoCivil.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueSexo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luePaisNacimiento.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtmFechaNacimiento.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtmFechaNacimiento.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueNacionalidad.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTipoDocumento.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdentificacion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrimerNombre.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSegundoNombre.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrimerApellido.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSegundoApellido.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.panelControl1.Appearance.Options.UseBackColor = true;
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.btnSalir);
            this.panelControl1.Controls.Add(this.lblTituloAccion);
            this.panelControl1.Controls.Add(this.sbtnSiguiente);
            this.panelControl1.Controls.Add(this.sbtnAnterior);
            this.panelControl1.Controls.Add(this.lblDetalles);
            this.panelControl1.Controls.Add(this.picNumeros);
            this.panelControl1.Controls.Add(this.lblNombreFormulario);
            this.panelControl1.Location = new System.Drawing.Point(-4, 0);
            this.panelControl1.LookAndFeel.SkinName = "Blue";
            this.panelControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelControl1.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1392, 59);
            this.panelControl1.TabIndex = 116;
            // 
            // btnSalir
            // 
            this.btnSalir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSalir.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.btnSalir.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalir.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.btnSalir.Appearance.Options.UseBackColor = true;
            this.btnSalir.Appearance.Options.UseFont = true;
            this.btnSalir.Appearance.Options.UseForeColor = true;
            this.btnSalir.Appearance.Options.UseTextOptions = true;
            this.btnSalir.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnSalir.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btnSalir.Location = new System.Drawing.Point(1333, 1);
            this.btnSalir.Margin = new System.Windows.Forms.Padding(2);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(50, 27);
            this.btnSalir.TabIndex = 71;
            this.btnSalir.Text = "X";
            this.btnSalir.ToolTip = "Salir";
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // lblTituloAccion
            // 
            this.lblTituloAccion.Appearance.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTituloAccion.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lblTituloAccion.Appearance.Options.UseFont = true;
            this.lblTituloAccion.Appearance.Options.UseForeColor = true;
            this.lblTituloAccion.Appearance.Options.UseTextOptions = true;
            this.lblTituloAccion.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblTituloAccion.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblTituloAccion.Location = new System.Drawing.Point(108, 4);
            this.lblTituloAccion.Margin = new System.Windows.Forms.Padding(4);
            this.lblTituloAccion.Name = "lblTituloAccion";
            this.lblTituloAccion.Size = new System.Drawing.Size(266, 30);
            this.lblTituloAccion.TabIndex = 64;
            this.lblTituloAccion.Text = "Información Biográfica";
            // 
            // sbtnSiguiente
            // 
            this.sbtnSiguiente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.sbtnSiguiente.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.sbtnSiguiente.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sbtnSiguiente.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.sbtnSiguiente.Appearance.Options.UseBackColor = true;
            this.sbtnSiguiente.Appearance.Options.UseFont = true;
            this.sbtnSiguiente.Appearance.Options.UseForeColor = true;
            this.sbtnSiguiente.Appearance.Options.UseTextOptions = true;
            this.sbtnSiguiente.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.sbtnSiguiente.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.sbtnSiguiente.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("sbtnSiguiente.ImageOptions.Image")));
            this.sbtnSiguiente.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.sbtnSiguiente.Location = new System.Drawing.Point(1079, 7);
            this.sbtnSiguiente.LookAndFeel.SkinName = "Glass Oceans";
            this.sbtnSiguiente.LookAndFeel.UseDefaultLookAndFeel = false;
            this.sbtnSiguiente.Margin = new System.Windows.Forms.Padding(4);
            this.sbtnSiguiente.Name = "sbtnSiguiente";
            this.sbtnSiguiente.Size = new System.Drawing.Size(132, 44);
            this.sbtnSiguiente.TabIndex = 70;
            this.sbtnSiguiente.Text = "&Siguiente";
            this.sbtnSiguiente.Click += new System.EventHandler(this.sbtnSiguiente_Click);
            // 
            // sbtnAnterior
            // 
            this.sbtnAnterior.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.sbtnAnterior.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.sbtnAnterior.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sbtnAnterior.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.sbtnAnterior.Appearance.Options.UseBackColor = true;
            this.sbtnAnterior.Appearance.Options.UseFont = true;
            this.sbtnAnterior.Appearance.Options.UseForeColor = true;
            this.sbtnAnterior.Appearance.Options.UseTextOptions = true;
            this.sbtnAnterior.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.sbtnAnterior.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.sbtnAnterior.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("sbtnAnterior.ImageOptions.Image")));
            this.sbtnAnterior.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.sbtnAnterior.Location = new System.Drawing.Point(946, 8);
            this.sbtnAnterior.LookAndFeel.SkinName = "Glass Oceans";
            this.sbtnAnterior.LookAndFeel.UseDefaultLookAndFeel = false;
            this.sbtnAnterior.Margin = new System.Windows.Forms.Padding(4);
            this.sbtnAnterior.Name = "sbtnAnterior";
            this.sbtnAnterior.Size = new System.Drawing.Size(125, 44);
            this.sbtnAnterior.TabIndex = 69;
            this.sbtnAnterior.Text = "&Anterior";
            this.sbtnAnterior.Click += new System.EventHandler(this.sbtnAnterior_Click);
            // 
            // lblDetalles
            // 
            this.lblDetalles.Appearance.Font = new System.Drawing.Font("Segoe UI", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDetalles.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lblDetalles.Appearance.Options.UseFont = true;
            this.lblDetalles.Appearance.Options.UseForeColor = true;
            this.lblDetalles.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblDetalles.Location = new System.Drawing.Point(108, 34);
            this.lblDetalles.Margin = new System.Windows.Forms.Padding(4);
            this.lblDetalles.Name = "lblDetalles";
            this.lblDetalles.Size = new System.Drawing.Size(325, 21);
            this.lblDetalles.TabIndex = 66;
            this.lblDetalles.Text = "Datos Personales.";
            // 
            // picNumeros
            // 
            this.picNumeros.Cursor = System.Windows.Forms.Cursors.Default;
            this.picNumeros.EditValue = ((object)(resources.GetObject("picNumeros.EditValue")));
            this.picNumeros.Location = new System.Drawing.Point(33, 6);
            this.picNumeros.Margin = new System.Windows.Forms.Padding(4);
            this.picNumeros.Name = "picNumeros";
            this.picNumeros.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picNumeros.Properties.Appearance.Options.UseBackColor = true;
            this.picNumeros.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.picNumeros.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picNumeros.Properties.ShowMenu = false;
            this.picNumeros.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.picNumeros.Size = new System.Drawing.Size(70, 52);
            this.picNumeros.TabIndex = 65;
            // 
            // lblNombreFormulario
            // 
            this.lblNombreFormulario.Appearance.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombreFormulario.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lblNombreFormulario.Appearance.Options.UseFont = true;
            this.lblNombreFormulario.Appearance.Options.UseForeColor = true;
            this.lblNombreFormulario.Appearance.Options.UseTextOptions = true;
            this.lblNombreFormulario.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblNombreFormulario.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblNombreFormulario.Location = new System.Drawing.Point(8, 6);
            this.lblNombreFormulario.Margin = new System.Windows.Forms.Padding(2);
            this.lblNombreFormulario.Name = "lblNombreFormulario";
            this.lblNombreFormulario.Size = new System.Drawing.Size(1374, 45);
            this.lblNombreFormulario.TabIndex = 72;
            this.lblNombreFormulario.Text = "ENROLAMIENTO";
            // 
            // xtraScrollableControl1
            // 
            this.xtraScrollableControl1.AllowTouchScroll = true;
            this.xtraScrollableControl1.Appearance.BackColor = System.Drawing.Color.White;
            this.xtraScrollableControl1.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.xtraScrollableControl1.Appearance.Options.UseBackColor = true;
            this.xtraScrollableControl1.Appearance.Options.UseBorderColor = true;
            this.xtraScrollableControl1.Controls.Add(this.picAtencion);
            this.xtraScrollableControl1.Controls.Add(this.panelControl4);
            this.xtraScrollableControl1.Controls.Add(this.lblEstadoValidacion);
            this.xtraScrollableControl1.Controls.Add(this.tabEnrolamiento);
            this.xtraScrollableControl1.Controls.Add(this.panelControl1);
            this.xtraScrollableControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraScrollableControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraScrollableControl1.Margin = new System.Windows.Forms.Padding(4);
            this.xtraScrollableControl1.Name = "xtraScrollableControl1";
            this.xtraScrollableControl1.Padding = new System.Windows.Forms.Padding(2);
            this.xtraScrollableControl1.Size = new System.Drawing.Size(1380, 766);
            this.xtraScrollableControl1.TabIndex = 41;
            // 
            // picAtencion
            // 
            this.picAtencion.Cursor = System.Windows.Forms.Cursors.Default;
            this.picAtencion.EditValue = global::Gv.ExodusBc.UI.Properties.Resources.error;
            this.picAtencion.Location = new System.Drawing.Point(5, 662);
            this.picAtencion.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.picAtencion.Name = "picAtencion";
            this.picAtencion.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picAtencion.Properties.Appearance.Options.UseBackColor = true;
            this.picAtencion.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.picAtencion.Properties.PictureAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.picAtencion.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picAtencion.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.picAtencion.Size = new System.Drawing.Size(22, 22);
            this.picAtencion.TabIndex = 477;
            this.picAtencion.Visible = false;
            // 
            // panelControl4
            // 
            this.panelControl4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl4.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.panelControl4.Appearance.Options.UseBackColor = true;
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Controls.Add(this.btnSyncCatalogos);
            this.panelControl4.Controls.Add(this.picPadFirma);
            this.panelControl4.Controls.Add(this.picCS500e);
            this.panelControl4.Controls.Add(this.picCamara);
            this.panelControl4.Controls.Add(this.pnlVerificacionHuellas);
            this.panelControl4.Controls.Add(this.btnGuardar);
            this.panelControl4.Location = new System.Drawing.Point(-1, 688);
            this.panelControl4.LookAndFeel.SkinName = "Blue";
            this.panelControl4.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelControl4.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(1381, 78);
            this.panelControl4.TabIndex = 117;
            // 
            // btnSyncCatalogos
            // 
            this.btnSyncCatalogos.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.btnSyncCatalogos.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.btnSyncCatalogos.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.btnSyncCatalogos.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSyncCatalogos.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.btnSyncCatalogos.Appearance.Options.UseBackColor = true;
            this.btnSyncCatalogos.Appearance.Options.UseBorderColor = true;
            this.btnSyncCatalogos.Appearance.Options.UseFont = true;
            this.btnSyncCatalogos.Appearance.Options.UseForeColor = true;
            this.btnSyncCatalogos.Appearance.Options.UseTextOptions = true;
            this.btnSyncCatalogos.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnSyncCatalogos.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btnSyncCatalogos.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.sync;
            this.btnSyncCatalogos.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnSyncCatalogos.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnSyncCatalogos.Location = new System.Drawing.Point(642, 30);
            this.btnSyncCatalogos.LookAndFeel.SkinName = "The Bezier";
            this.btnSyncCatalogos.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnSyncCatalogos.Margin = new System.Windows.Forms.Padding(4);
            this.btnSyncCatalogos.Name = "btnSyncCatalogos";
            this.btnSyncCatalogos.Size = new System.Drawing.Size(74, 44);
            this.btnSyncCatalogos.TabIndex = 568;
            this.btnSyncCatalogos.ToolTip = "Sincronizar manualmente";
            this.btnSyncCatalogos.Click += new System.EventHandler(this.btnSyncCatalogos_Click);
            // 
            // picPadFirma
            // 
            this.picPadFirma.Cursor = System.Windows.Forms.Cursors.Default;
            this.picPadFirma.Location = new System.Drawing.Point(110, 30);
            this.picPadFirma.Margin = new System.Windows.Forms.Padding(4);
            this.picPadFirma.Name = "picPadFirma";
            this.picPadFirma.Properties.AllowFocused = false;
            this.picPadFirma.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picPadFirma.Properties.Appearance.Options.UseBackColor = true;
            this.picPadFirma.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.picPadFirma.Properties.NullText = " ";
            this.picPadFirma.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picPadFirma.Properties.ShowMenu = false;
            this.picPadFirma.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.picPadFirma.Size = new System.Drawing.Size(38, 38);
            this.picPadFirma.TabIndex = 418;
            // 
            // picCS500e
            // 
            this.picCS500e.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.picCS500e.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.picCS500e.Cursor = System.Windows.Forms.Cursors.Default;
            this.picCS500e.Location = new System.Drawing.Point(52, 35);
            this.picCS500e.Margin = new System.Windows.Forms.Padding(4);
            this.picCS500e.Name = "picCS500e";
            this.picCS500e.Properties.AllowFocused = false;
            this.picCS500e.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picCS500e.Properties.Appearance.Options.UseBackColor = true;
            this.picCS500e.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.picCS500e.Properties.NullText = "Escaner Inicializado";
            this.picCS500e.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picCS500e.Properties.ShowMenu = false;
            this.picCS500e.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.picCS500e.Size = new System.Drawing.Size(38, 38);
            this.picCS500e.TabIndex = 417;
            // 
            // picCamara
            // 
            this.picCamara.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.picCamara.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.picCamara.Cursor = System.Windows.Forms.Cursors.Default;
            this.picCamara.Location = new System.Drawing.Point(8, 35);
            this.picCamara.Margin = new System.Windows.Forms.Padding(4);
            this.picCamara.Name = "picCamara";
            this.picCamara.Properties.AllowFocused = false;
            this.picCamara.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picCamara.Properties.Appearance.Options.UseBackColor = true;
            this.picCamara.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.picCamara.Properties.NullText = "Cámara Inicializada";
            this.picCamara.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picCamara.Properties.ShowMenu = false;
            this.picCamara.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.picCamara.Size = new System.Drawing.Size(38, 38);
            this.picCamara.TabIndex = 416;
            // 
            // pnlVerificacionHuellas
            // 
            this.pnlVerificacionHuellas.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlVerificacionHuellas.Appearance.BackColor = System.Drawing.Color.MintCream;
            this.pnlVerificacionHuellas.Appearance.Options.UseBackColor = true;
            this.pnlVerificacionHuellas.Appearance.Options.UseBorderColor = true;
            this.pnlVerificacionHuellas.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlVerificacionHuellas.Controls.Add(this.lblGifValidando);
            this.pnlVerificacionHuellas.Controls.Add(this.lblVerificarHuella);
            this.pnlVerificacionHuellas.Location = new System.Drawing.Point(0, 0);
            this.pnlVerificacionHuellas.LookAndFeel.SkinName = "Foggy";
            this.pnlVerificacionHuellas.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.pnlVerificacionHuellas.LookAndFeel.UseDefaultLookAndFeel = false;
            this.pnlVerificacionHuellas.Margin = new System.Windows.Forms.Padding(4);
            this.pnlVerificacionHuellas.Name = "pnlVerificacionHuellas";
            this.pnlVerificacionHuellas.Size = new System.Drawing.Size(1381, 28);
            this.pnlVerificacionHuellas.TabIndex = 413;
            // 
            // lblGifValidando
            // 
            this.lblGifValidando.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lblGifValidando.Appearance.BackColor2 = System.Drawing.Color.Transparent;
            this.lblGifValidando.Appearance.Image = global::Gv.ExodusBc.UI.Properties.Resources.loading_small;
            this.lblGifValidando.Appearance.Options.UseBackColor = true;
            this.lblGifValidando.Appearance.Options.UseImage = true;
            this.lblGifValidando.Location = new System.Drawing.Point(163, 8);
            this.lblGifValidando.Margin = new System.Windows.Forms.Padding(4);
            this.lblGifValidando.Name = "lblGifValidando";
            this.lblGifValidando.Size = new System.Drawing.Size(16, 16);
            this.lblGifValidando.TabIndex = 2;
            this.lblGifValidando.Visible = false;
            // 
            // lblVerificarHuella
            // 
            this.lblVerificarHuella.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVerificarHuella.Appearance.Options.UseFont = true;
            this.lblVerificarHuella.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblVerificarHuella.Location = new System.Drawing.Point(6, 4);
            this.lblVerificarHuella.Margin = new System.Windows.Forms.Padding(4);
            this.lblVerificarHuella.Name = "lblVerificarHuella";
            this.lblVerificarHuella.Size = new System.Drawing.Size(149, 22);
            this.lblVerificarHuella.TabIndex = 1;
            this.lblVerificarHuella.Text = "Verificación de huellas:";
            // 
            // btnGuardar
            // 
            this.btnGuardar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGuardar.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardar.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.btnGuardar.Appearance.Options.UseFont = true;
            this.btnGuardar.Appearance.Options.UseForeColor = true;
            this.btnGuardar.AppearanceHovered.Font = new System.Drawing.Font("Tahoma", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.btnGuardar.AppearanceHovered.Options.UseFont = true;
            this.btnGuardar.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnGuardar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGuardar.Enabled = false;
            this.btnGuardar.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.save_32x32;
            this.btnGuardar.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnGuardar.Location = new System.Drawing.Point(1185, 31);
            this.btnGuardar.LookAndFeel.SkinName = "Office 2016 Colorful";
            this.btnGuardar.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnGuardar.Margin = new System.Windows.Forms.Padding(4);
            this.btnGuardar.MaximumSize = new System.Drawing.Size(165, 41);
            this.btnGuardar.MinimumSize = new System.Drawing.Size(165, 41);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(165, 41);
            this.btnGuardar.TabIndex = 70;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // lblEstadoValidacion
            // 
            this.lblEstadoValidacion.Appearance.Font = new System.Drawing.Font("Segoe UI", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstadoValidacion.Appearance.Options.UseFont = true;
            this.lblEstadoValidacion.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblEstadoValidacion.Location = new System.Drawing.Point(34, 662);
            this.lblEstadoValidacion.Margin = new System.Windows.Forms.Padding(4);
            this.lblEstadoValidacion.Name = "lblEstadoValidacion";
            this.lblEstadoValidacion.Size = new System.Drawing.Size(1045, 22);
            this.lblEstadoValidacion.TabIndex = 0;
            this.lblEstadoValidacion.Text = "No Iniciada";
            // 
            // tabEnrolamiento
            // 
            this.tabEnrolamiento.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabEnrolamiento.Controls.Add(this.navigationPage4);
            this.tabEnrolamiento.Controls.Add(this.navigationPage1);
            this.tabEnrolamiento.Controls.Add(this.navigationPage2);
            this.tabEnrolamiento.Controls.Add(this.navigationPage5);
            this.tabEnrolamiento.Controls.Add(this.tabBiograficos);
            this.tabEnrolamiento.Location = new System.Drawing.Point(4, 70);
            this.tabEnrolamiento.Margin = new System.Windows.Forms.Padding(4);
            this.tabEnrolamiento.Name = "tabEnrolamiento";
            this.tabEnrolamiento.Pages.AddRange(new DevExpress.XtraBars.Navigation.NavigationPageBase[] {
            this.tabBiograficos,
            this.navigationPage4,
            this.navigationPage1,
            this.navigationPage5,
            this.navigationPage2});
            this.tabEnrolamiento.SelectedPage = this.tabBiograficos;
            this.tabEnrolamiento.Size = new System.Drawing.Size(1359, 591);
            this.tabEnrolamiento.TabIndex = 64;
            this.tabEnrolamiento.Text = "navigationFrame1";
            this.tabEnrolamiento.Click += new System.EventHandler(this.tabEnrolamiento_Click);
            // 
            // navigationPage4
            // 
            this.navigationPage4.Caption = "navigationPage4";
            this.navigationPage4.Controls.Add(this.btnReiniciarEscaner);
            this.navigationPage4.Controls.Add(this.pnlObservacionesPersonaHuellaEstado);
            this.navigationPage4.Controls.Add(this.luePersonaHuellaEstado);
            this.navigationPage4.Controls.Add(this.lblNumeroTransaccion);
            this.navigationPage4.Controls.Add(this.labelControl98);
            this.navigationPage4.Controls.Add(this.labelControl78);
            this.navigationPage4.Controls.Add(this.sbtnCapturaHuellas);
            this.navigationPage4.Controls.Add(this.lblDedo5);
            this.navigationPage4.Controls.Add(this.prgbDedo5);
            this.navigationPage4.Controls.Add(this.lblDedo4);
            this.navigationPage4.Controls.Add(this.prgbDedo4);
            this.navigationPage4.Controls.Add(this.lblDedo3);
            this.navigationPage4.Controls.Add(this.prgbDedo3);
            this.navigationPage4.Controls.Add(this.lblDedo2);
            this.navigationPage4.Controls.Add(this.prgbDedo2);
            this.navigationPage4.Controls.Add(this.lblDedo1);
            this.navigationPage4.Controls.Add(this.prgbDedo1);
            this.navigationPage4.Controls.Add(this.lblDedo10);
            this.navigationPage4.Controls.Add(this.prgbDedo10);
            this.navigationPage4.Controls.Add(this.lblDedo9);
            this.navigationPage4.Controls.Add(this.prgbDedo9);
            this.navigationPage4.Controls.Add(this.lblDedo8);
            this.navigationPage4.Controls.Add(this.prgbDedo8);
            this.navigationPage4.Controls.Add(this.lblDedo7);
            this.navigationPage4.Controls.Add(this.prgbDedo7);
            this.navigationPage4.Controls.Add(this.picSeleccionDerecha);
            this.navigationPage4.Controls.Add(this.pnlManoDerecha);
            this.navigationPage4.Controls.Add(this.picSeleccionIzquierda);
            this.navigationPage4.Controls.Add(this.pnlManoIzquierda);
            this.navigationPage4.Controls.Add(this.labelControl35);
            this.navigationPage4.Controls.Add(this.labelControl34);
            this.navigationPage4.Controls.Add(this.labelControl33);
            this.navigationPage4.Controls.Add(this.labelControl32);
            this.navigationPage4.Controls.Add(this.labelControl31);
            this.navigationPage4.Controls.Add(this.lblDedo6);
            this.navigationPage4.Controls.Add(this.prgbDedo6);
            this.navigationPage4.Controls.Add(this.picPulgarIzquierdo);
            this.navigationPage4.Controls.Add(this.picMeniequeDerecho);
            this.navigationPage4.Controls.Add(this.labelControl30);
            this.navigationPage4.Controls.Add(this.labelControl28);
            this.navigationPage4.Controls.Add(this.labelControl27);
            this.navigationPage4.Controls.Add(this.labelControl26);
            this.navigationPage4.Controls.Add(this.labelControl25);
            this.navigationPage4.Controls.Add(this.labelControl13);
            this.navigationPage4.Controls.Add(this.labelControl18);
            this.navigationPage4.Controls.Add(this.picIndiceIzquierdo);
            this.navigationPage4.Controls.Add(this.picMedioIzquierdo);
            this.navigationPage4.Controls.Add(this.picMeniequeIzquierdo);
            this.navigationPage4.Controls.Add(this.picAnularIzquierdo);
            this.navigationPage4.Controls.Add(this.picAnularDerecho);
            this.navigationPage4.Controls.Add(this.picMedioDerecho);
            this.navigationPage4.Controls.Add(this.picPulgarDerecho);
            this.navigationPage4.Controls.Add(this.picIndiceDerecho);
            this.navigationPage4.Controls.Add(this.picManoIzquierda);
            this.navigationPage4.Controls.Add(this.picManoDerecha);
            this.navigationPage4.Margin = new System.Windows.Forms.Padding(4);
            this.navigationPage4.Name = "navigationPage4";
            this.navigationPage4.Size = new System.Drawing.Size(1359, 591);
            // 
            // btnReiniciarEscaner
            // 
            this.btnReiniciarEscaner.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.btnReiniciarEscaner.Appearance.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReiniciarEscaner.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.btnReiniciarEscaner.Appearance.Options.UseBackColor = true;
            this.btnReiniciarEscaner.Appearance.Options.UseFont = true;
            this.btnReiniciarEscaner.Appearance.Options.UseForeColor = true;
            this.btnReiniciarEscaner.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnReiniciarEscaner.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_refresh_32x32;
            this.btnReiniciarEscaner.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnReiniciarEscaner.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnReiniciarEscaner.Location = new System.Drawing.Point(668, 456);
            this.btnReiniciarEscaner.Margin = new System.Windows.Forms.Padding(4);
            this.btnReiniciarEscaner.Name = "btnReiniciarEscaner";
            this.btnReiniciarEscaner.Size = new System.Drawing.Size(185, 38);
            this.btnReiniciarEscaner.TabIndex = 312;
            this.btnReiniciarEscaner.Text = "Reiniciar Escáner";
            this.btnReiniciarEscaner.Click += new System.EventHandler(this.btnReiniciarEscaner_Click);
            // 
            // pnlObservacionesPersonaHuellaEstado
            // 
            this.pnlObservacionesPersonaHuellaEstado.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlObservacionesPersonaHuellaEstado.Controls.Add(this.label17);
            this.pnlObservacionesPersonaHuellaEstado.Controls.Add(this.meObservacionHuellas);
            this.pnlObservacionesPersonaHuellaEstado.Controls.Add(this.lblObservacionesHuellas);
            this.pnlObservacionesPersonaHuellaEstado.Location = new System.Drawing.Point(114, 499);
            this.pnlObservacionesPersonaHuellaEstado.Margin = new System.Windows.Forms.Padding(4);
            this.pnlObservacionesPersonaHuellaEstado.Name = "pnlObservacionesPersonaHuellaEstado";
            this.pnlObservacionesPersonaHuellaEstado.Size = new System.Drawing.Size(924, 81);
            this.pnlObservacionesPersonaHuellaEstado.TabIndex = 311;
            this.pnlObservacionesPersonaHuellaEstado.Visible = false;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.ForeColor = System.Drawing.Color.Red;
            this.label17.Location = new System.Drawing.Point(109, 13);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(15, 16);
            this.label17.TabIndex = 286;
            this.label17.Text = "*";
            // 
            // meObservacionHuellas
            // 
            this.meObservacionHuellas.Location = new System.Drawing.Point(4, 32);
            this.meObservacionHuellas.Margin = new System.Windows.Forms.Padding(4);
            this.meObservacionHuellas.Name = "meObservacionHuellas";
            this.meObservacionHuellas.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.meObservacionHuellas.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.meObservacionHuellas.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.meObservacionHuellas.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.meObservacionHuellas.Size = new System.Drawing.Size(872, 44);
            this.meObservacionHuellas.TabIndex = 233;
            // 
            // lblObservacionesHuellas
            // 
            this.lblObservacionesHuellas.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblObservacionesHuellas.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lblObservacionesHuellas.Appearance.Options.UseFont = true;
            this.lblObservacionesHuellas.Appearance.Options.UseForeColor = true;
            this.lblObservacionesHuellas.Location = new System.Drawing.Point(4, 7);
            this.lblObservacionesHuellas.Margin = new System.Windows.Forms.Padding(4);
            this.lblObservacionesHuellas.Name = "lblObservacionesHuellas";
            this.lblObservacionesHuellas.Size = new System.Drawing.Size(101, 20);
            this.lblObservacionesHuellas.TabIndex = 254;
            this.lblObservacionesHuellas.Text = "Observaciones";
            // 
            // luePersonaHuellaEstado
            // 
            this.luePersonaHuellaEstado.Location = new System.Drawing.Point(283, 471);
            this.luePersonaHuellaEstado.Margin = new System.Windows.Forms.Padding(4);
            this.luePersonaHuellaEstado.Name = "luePersonaHuellaEstado";
            this.luePersonaHuellaEstado.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.luePersonaHuellaEstado.Properties.Appearance.Options.UseBackColor = true;
            this.luePersonaHuellaEstado.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.luePersonaHuellaEstado.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.luePersonaHuellaEstado.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.luePersonaHuellaEstado.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luePersonaHuellaEstado.Properties.NullText = "";
            this.luePersonaHuellaEstado.Size = new System.Drawing.Size(355, 22);
            this.luePersonaHuellaEstado.TabIndex = 310;
            this.luePersonaHuellaEstado.EditValueChanged += new System.EventHandler(this.luePersonaHuellaEstado_EditValueChanged);
            // 
            // lblNumeroTransaccion
            // 
            this.lblNumeroTransaccion.Appearance.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumeroTransaccion.Appearance.ForeColor = System.Drawing.Color.Gray;
            this.lblNumeroTransaccion.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblNumeroTransaccion.Appearance.Options.UseFont = true;
            this.lblNumeroTransaccion.Appearance.Options.UseForeColor = true;
            this.lblNumeroTransaccion.Appearance.Options.UseImageAlign = true;
            this.lblNumeroTransaccion.Appearance.Options.UseTextOptions = true;
            this.lblNumeroTransaccion.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblNumeroTransaccion.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblNumeroTransaccion.Location = new System.Drawing.Point(570, 185);
            this.lblNumeroTransaccion.Margin = new System.Windows.Forms.Padding(4);
            this.lblNumeroTransaccion.Name = "lblNumeroTransaccion";
            this.lblNumeroTransaccion.Size = new System.Drawing.Size(218, 22);
            this.lblNumeroTransaccion.TabIndex = 259;
            this.lblNumeroTransaccion.Text = "[lblNumeroTransaccion]";
            // 
            // labelControl98
            // 
            this.labelControl98.Appearance.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl98.Appearance.ForeColor = System.Drawing.Color.Gray;
            this.labelControl98.Appearance.Options.UseFont = true;
            this.labelControl98.Appearance.Options.UseForeColor = true;
            this.labelControl98.Appearance.Options.UseTextOptions = true;
            this.labelControl98.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl98.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl98.Location = new System.Drawing.Point(570, 155);
            this.labelControl98.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl98.Name = "labelControl98";
            this.labelControl98.Size = new System.Drawing.Size(196, 22);
            this.labelControl98.TabIndex = 258;
            this.labelControl98.Text = "Número de Transacción";
            // 
            // labelControl78
            // 
            this.labelControl78.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl78.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl78.Appearance.Options.UseFont = true;
            this.labelControl78.Appearance.Options.UseForeColor = true;
            this.labelControl78.Location = new System.Drawing.Point(114, 469);
            this.labelControl78.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl78.Name = "labelControl78";
            this.labelControl78.Size = new System.Drawing.Size(144, 20);
            this.labelControl78.TabIndex = 291;
            this.labelControl78.Text = "Estado de las huellas";
            // 
            // sbtnCapturaHuellas
            // 
            this.sbtnCapturaHuellas.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.sbtnCapturaHuellas.Appearance.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sbtnCapturaHuellas.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.sbtnCapturaHuellas.Appearance.Options.UseBackColor = true;
            this.sbtnCapturaHuellas.Appearance.Options.UseFont = true;
            this.sbtnCapturaHuellas.Appearance.Options.UseForeColor = true;
            this.sbtnCapturaHuellas.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.sbtnCapturaHuellas.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("sbtnCapturaHuellas.ImageOptions.Image")));
            this.sbtnCapturaHuellas.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.sbtnCapturaHuellas.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.sbtnCapturaHuellas.Location = new System.Drawing.Point(581, 250);
            this.sbtnCapturaHuellas.Margin = new System.Windows.Forms.Padding(4);
            this.sbtnCapturaHuellas.Name = "sbtnCapturaHuellas";
            this.sbtnCapturaHuellas.Size = new System.Drawing.Size(185, 38);
            this.sbtnCapturaHuellas.TabIndex = 273;
            this.sbtnCapturaHuellas.Text = "Iniciar Captura";
            this.sbtnCapturaHuellas.Click += new System.EventHandler(this.sbtnCapturaHuellas_Click);
            // 
            // lblDedo5
            // 
            this.lblDedo5.Appearance.BackColor = System.Drawing.Color.Black;
            this.lblDedo5.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDedo5.Appearance.ForeColor = System.Drawing.Color.White;
            this.lblDedo5.Appearance.Options.UseBackColor = true;
            this.lblDedo5.Appearance.Options.UseFont = true;
            this.lblDedo5.Appearance.Options.UseForeColor = true;
            this.lblDedo5.Appearance.Options.UseTextOptions = true;
            this.lblDedo5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblDedo5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblDedo5.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.lblDedo5.Location = new System.Drawing.Point(1199, 32);
            this.lblDedo5.Margin = new System.Windows.Forms.Padding(4);
            this.lblDedo5.Name = "lblDedo5";
            this.lblDedo5.Size = new System.Drawing.Size(12, 16);
            this.lblDedo5.TabIndex = 309;
            this.lblDedo5.Text = "[]";
            // 
            // prgbDedo5
            // 
            this.prgbDedo5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.prgbDedo5.EditValue = 5;
            this.prgbDedo5.Location = new System.Drawing.Point(1200, 52);
            this.prgbDedo5.Margin = new System.Windows.Forms.Padding(4);
            this.prgbDedo5.Name = "prgbDedo5";
            this.prgbDedo5.Properties.Appearance.BorderColor = System.Drawing.Color.Black;
            this.prgbDedo5.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.prgbDedo5.Properties.EndColor = System.Drawing.Color.Black;
            this.prgbDedo5.Properties.LookAndFeel.SkinMaskColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.prgbDedo5.Properties.LookAndFeel.SkinMaskColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.prgbDedo5.Properties.LookAndFeel.SkinName = "Visual Studio 2013 Blue";
            this.prgbDedo5.Properties.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.prgbDedo5.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.prgbDedo5.Properties.Maximum = 5;
            this.prgbDedo5.Properties.Minimum = 1;
            this.prgbDedo5.Properties.ProgressKind = DevExpress.XtraEditors.Controls.ProgressKind.Vertical;
            this.prgbDedo5.Properties.ProgressViewStyle = DevExpress.XtraEditors.Controls.ProgressViewStyle.Solid;
            this.prgbDedo5.Properties.StartColor = System.Drawing.Color.Black;
            this.prgbDedo5.Properties.Step = 5;
            this.prgbDedo5.Properties.TextOrientation = DevExpress.Utils.Drawing.TextOrientation.VerticalDownwards;
            this.prgbDedo5.Size = new System.Drawing.Size(8, 68);
            this.prgbDedo5.TabIndex = 308;
            // 
            // lblDedo4
            // 
            this.lblDedo4.Appearance.BackColor = System.Drawing.Color.Black;
            this.lblDedo4.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDedo4.Appearance.ForeColor = System.Drawing.Color.White;
            this.lblDedo4.Appearance.Options.UseBackColor = true;
            this.lblDedo4.Appearance.Options.UseFont = true;
            this.lblDedo4.Appearance.Options.UseForeColor = true;
            this.lblDedo4.Appearance.Options.UseTextOptions = true;
            this.lblDedo4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblDedo4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblDedo4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.lblDedo4.Location = new System.Drawing.Point(1084, 32);
            this.lblDedo4.Margin = new System.Windows.Forms.Padding(4);
            this.lblDedo4.Name = "lblDedo4";
            this.lblDedo4.Size = new System.Drawing.Size(12, 16);
            this.lblDedo4.TabIndex = 307;
            this.lblDedo4.Text = "[]";
            // 
            // prgbDedo4
            // 
            this.prgbDedo4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.prgbDedo4.EditValue = 5;
            this.prgbDedo4.Location = new System.Drawing.Point(1085, 52);
            this.prgbDedo4.Margin = new System.Windows.Forms.Padding(4);
            this.prgbDedo4.Name = "prgbDedo4";
            this.prgbDedo4.Properties.Appearance.BorderColor = System.Drawing.Color.Black;
            this.prgbDedo4.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.prgbDedo4.Properties.EndColor = System.Drawing.Color.Black;
            this.prgbDedo4.Properties.LookAndFeel.SkinMaskColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.prgbDedo4.Properties.LookAndFeel.SkinMaskColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.prgbDedo4.Properties.LookAndFeel.SkinName = "Visual Studio 2013 Blue";
            this.prgbDedo4.Properties.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.prgbDedo4.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.prgbDedo4.Properties.Maximum = 5;
            this.prgbDedo4.Properties.Minimum = 1;
            this.prgbDedo4.Properties.ProgressKind = DevExpress.XtraEditors.Controls.ProgressKind.Vertical;
            this.prgbDedo4.Properties.ProgressViewStyle = DevExpress.XtraEditors.Controls.ProgressViewStyle.Solid;
            this.prgbDedo4.Properties.StartColor = System.Drawing.Color.Black;
            this.prgbDedo4.Properties.Step = 5;
            this.prgbDedo4.Properties.TextOrientation = DevExpress.Utils.Drawing.TextOrientation.VerticalDownwards;
            this.prgbDedo4.Size = new System.Drawing.Size(8, 68);
            this.prgbDedo4.TabIndex = 306;
            // 
            // lblDedo3
            // 
            this.lblDedo3.Appearance.BackColor = System.Drawing.Color.Black;
            this.lblDedo3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDedo3.Appearance.ForeColor = System.Drawing.Color.White;
            this.lblDedo3.Appearance.Options.UseBackColor = true;
            this.lblDedo3.Appearance.Options.UseFont = true;
            this.lblDedo3.Appearance.Options.UseForeColor = true;
            this.lblDedo3.Appearance.Options.UseTextOptions = true;
            this.lblDedo3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblDedo3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblDedo3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.lblDedo3.Location = new System.Drawing.Point(972, 32);
            this.lblDedo3.Margin = new System.Windows.Forms.Padding(4);
            this.lblDedo3.Name = "lblDedo3";
            this.lblDedo3.Size = new System.Drawing.Size(12, 16);
            this.lblDedo3.TabIndex = 305;
            this.lblDedo3.Text = "[]";
            // 
            // prgbDedo3
            // 
            this.prgbDedo3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.prgbDedo3.EditValue = 5;
            this.prgbDedo3.Location = new System.Drawing.Point(975, 52);
            this.prgbDedo3.Margin = new System.Windows.Forms.Padding(4);
            this.prgbDedo3.Name = "prgbDedo3";
            this.prgbDedo3.Properties.Appearance.BorderColor = System.Drawing.Color.Black;
            this.prgbDedo3.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.prgbDedo3.Properties.EndColor = System.Drawing.Color.Black;
            this.prgbDedo3.Properties.LookAndFeel.SkinMaskColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.prgbDedo3.Properties.LookAndFeel.SkinMaskColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.prgbDedo3.Properties.LookAndFeel.SkinName = "Visual Studio 2013 Blue";
            this.prgbDedo3.Properties.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.prgbDedo3.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.prgbDedo3.Properties.Maximum = 5;
            this.prgbDedo3.Properties.Minimum = 1;
            this.prgbDedo3.Properties.ProgressKind = DevExpress.XtraEditors.Controls.ProgressKind.Vertical;
            this.prgbDedo3.Properties.ProgressViewStyle = DevExpress.XtraEditors.Controls.ProgressViewStyle.Solid;
            this.prgbDedo3.Properties.StartColor = System.Drawing.Color.Black;
            this.prgbDedo3.Properties.Step = 5;
            this.prgbDedo3.Properties.TextOrientation = DevExpress.Utils.Drawing.TextOrientation.VerticalDownwards;
            this.prgbDedo3.Size = new System.Drawing.Size(8, 68);
            this.prgbDedo3.TabIndex = 304;
            // 
            // lblDedo2
            // 
            this.lblDedo2.Appearance.BackColor = System.Drawing.Color.Black;
            this.lblDedo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDedo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.lblDedo2.Appearance.Options.UseBackColor = true;
            this.lblDedo2.Appearance.Options.UseFont = true;
            this.lblDedo2.Appearance.Options.UseForeColor = true;
            this.lblDedo2.Appearance.Options.UseTextOptions = true;
            this.lblDedo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblDedo2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblDedo2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.lblDedo2.Location = new System.Drawing.Point(861, 32);
            this.lblDedo2.Margin = new System.Windows.Forms.Padding(4);
            this.lblDedo2.Name = "lblDedo2";
            this.lblDedo2.Size = new System.Drawing.Size(12, 16);
            this.lblDedo2.TabIndex = 303;
            this.lblDedo2.Text = "[]";
            // 
            // prgbDedo2
            // 
            this.prgbDedo2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.prgbDedo2.EditValue = 5;
            this.prgbDedo2.Location = new System.Drawing.Point(862, 52);
            this.prgbDedo2.Margin = new System.Windows.Forms.Padding(4);
            this.prgbDedo2.Name = "prgbDedo2";
            this.prgbDedo2.Properties.Appearance.BorderColor = System.Drawing.Color.Black;
            this.prgbDedo2.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.prgbDedo2.Properties.EndColor = System.Drawing.Color.Black;
            this.prgbDedo2.Properties.LookAndFeel.SkinMaskColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.prgbDedo2.Properties.LookAndFeel.SkinMaskColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.prgbDedo2.Properties.LookAndFeel.SkinName = "Visual Studio 2013 Blue";
            this.prgbDedo2.Properties.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.prgbDedo2.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.prgbDedo2.Properties.Maximum = 5;
            this.prgbDedo2.Properties.Minimum = 1;
            this.prgbDedo2.Properties.ProgressKind = DevExpress.XtraEditors.Controls.ProgressKind.Vertical;
            this.prgbDedo2.Properties.ProgressViewStyle = DevExpress.XtraEditors.Controls.ProgressViewStyle.Solid;
            this.prgbDedo2.Properties.StartColor = System.Drawing.Color.Black;
            this.prgbDedo2.Properties.Step = 5;
            this.prgbDedo2.Properties.TextOrientation = DevExpress.Utils.Drawing.TextOrientation.VerticalDownwards;
            this.prgbDedo2.Size = new System.Drawing.Size(8, 68);
            this.prgbDedo2.TabIndex = 302;
            // 
            // lblDedo1
            // 
            this.lblDedo1.Appearance.BackColor = System.Drawing.Color.Black;
            this.lblDedo1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDedo1.Appearance.ForeColor = System.Drawing.Color.White;
            this.lblDedo1.Appearance.Options.UseBackColor = true;
            this.lblDedo1.Appearance.Options.UseFont = true;
            this.lblDedo1.Appearance.Options.UseForeColor = true;
            this.lblDedo1.Appearance.Options.UseTextOptions = true;
            this.lblDedo1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblDedo1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblDedo1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.lblDedo1.Location = new System.Drawing.Point(749, 32);
            this.lblDedo1.Margin = new System.Windows.Forms.Padding(4);
            this.lblDedo1.Name = "lblDedo1";
            this.lblDedo1.Size = new System.Drawing.Size(12, 16);
            this.lblDedo1.TabIndex = 301;
            this.lblDedo1.Text = "[]";
            // 
            // prgbDedo1
            // 
            this.prgbDedo1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.prgbDedo1.EditValue = 5;
            this.prgbDedo1.Location = new System.Drawing.Point(751, 52);
            this.prgbDedo1.Margin = new System.Windows.Forms.Padding(4);
            this.prgbDedo1.Name = "prgbDedo1";
            this.prgbDedo1.Properties.Appearance.BorderColor = System.Drawing.Color.Black;
            this.prgbDedo1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.prgbDedo1.Properties.EndColor = System.Drawing.Color.Black;
            this.prgbDedo1.Properties.LookAndFeel.SkinMaskColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.prgbDedo1.Properties.LookAndFeel.SkinMaskColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.prgbDedo1.Properties.LookAndFeel.SkinName = "Visual Studio 2013 Blue";
            this.prgbDedo1.Properties.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.prgbDedo1.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.prgbDedo1.Properties.Maximum = 5;
            this.prgbDedo1.Properties.ProgressKind = DevExpress.XtraEditors.Controls.ProgressKind.Vertical;
            this.prgbDedo1.Properties.ProgressViewStyle = DevExpress.XtraEditors.Controls.ProgressViewStyle.Solid;
            this.prgbDedo1.Properties.StartColor = System.Drawing.Color.Black;
            this.prgbDedo1.Properties.Step = 5;
            this.prgbDedo1.Properties.TextOrientation = DevExpress.Utils.Drawing.TextOrientation.VerticalDownwards;
            this.prgbDedo1.Size = new System.Drawing.Size(8, 68);
            this.prgbDedo1.TabIndex = 300;
            // 
            // lblDedo10
            // 
            this.lblDedo10.Appearance.BackColor = System.Drawing.Color.Black;
            this.lblDedo10.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDedo10.Appearance.ForeColor = System.Drawing.Color.White;
            this.lblDedo10.Appearance.Options.UseBackColor = true;
            this.lblDedo10.Appearance.Options.UseFont = true;
            this.lblDedo10.Appearance.Options.UseForeColor = true;
            this.lblDedo10.Appearance.Options.UseTextOptions = true;
            this.lblDedo10.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblDedo10.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblDedo10.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.lblDedo10.Location = new System.Drawing.Point(20, 31);
            this.lblDedo10.Margin = new System.Windows.Forms.Padding(4);
            this.lblDedo10.Name = "lblDedo10";
            this.lblDedo10.Size = new System.Drawing.Size(12, 16);
            this.lblDedo10.TabIndex = 299;
            this.lblDedo10.Text = "[]";
            // 
            // prgbDedo10
            // 
            this.prgbDedo10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.prgbDedo10.EditValue = 5;
            this.prgbDedo10.Location = new System.Drawing.Point(21, 52);
            this.prgbDedo10.Margin = new System.Windows.Forms.Padding(4);
            this.prgbDedo10.Name = "prgbDedo10";
            this.prgbDedo10.Properties.Appearance.BorderColor = System.Drawing.Color.Black;
            this.prgbDedo10.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.prgbDedo10.Properties.EndColor = System.Drawing.Color.Black;
            this.prgbDedo10.Properties.LookAndFeel.SkinMaskColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.prgbDedo10.Properties.LookAndFeel.SkinMaskColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.prgbDedo10.Properties.LookAndFeel.SkinName = "Visual Studio 2013 Blue";
            this.prgbDedo10.Properties.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.prgbDedo10.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.prgbDedo10.Properties.Maximum = 5;
            this.prgbDedo10.Properties.Minimum = 1;
            this.prgbDedo10.Properties.ProgressKind = DevExpress.XtraEditors.Controls.ProgressKind.Vertical;
            this.prgbDedo10.Properties.ProgressViewStyle = DevExpress.XtraEditors.Controls.ProgressViewStyle.Solid;
            this.prgbDedo10.Properties.StartColor = System.Drawing.Color.Black;
            this.prgbDedo10.Properties.Step = 5;
            this.prgbDedo10.Properties.TextOrientation = DevExpress.Utils.Drawing.TextOrientation.VerticalDownwards;
            this.prgbDedo10.Size = new System.Drawing.Size(8, 68);
            this.prgbDedo10.TabIndex = 298;
            // 
            // lblDedo9
            // 
            this.lblDedo9.Appearance.BackColor = System.Drawing.Color.Black;
            this.lblDedo9.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDedo9.Appearance.ForeColor = System.Drawing.Color.White;
            this.lblDedo9.Appearance.Options.UseBackColor = true;
            this.lblDedo9.Appearance.Options.UseFont = true;
            this.lblDedo9.Appearance.Options.UseForeColor = true;
            this.lblDedo9.Appearance.Options.UseTextOptions = true;
            this.lblDedo9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblDedo9.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblDedo9.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.lblDedo9.Location = new System.Drawing.Point(132, 31);
            this.lblDedo9.Margin = new System.Windows.Forms.Padding(4);
            this.lblDedo9.Name = "lblDedo9";
            this.lblDedo9.Size = new System.Drawing.Size(12, 16);
            this.lblDedo9.TabIndex = 297;
            this.lblDedo9.Text = "[]";
            // 
            // prgbDedo9
            // 
            this.prgbDedo9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.prgbDedo9.EditValue = 5;
            this.prgbDedo9.Location = new System.Drawing.Point(135, 52);
            this.prgbDedo9.Margin = new System.Windows.Forms.Padding(4);
            this.prgbDedo9.Name = "prgbDedo9";
            this.prgbDedo9.Properties.Appearance.BorderColor = System.Drawing.Color.Black;
            this.prgbDedo9.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.prgbDedo9.Properties.EndColor = System.Drawing.Color.Black;
            this.prgbDedo9.Properties.LookAndFeel.SkinMaskColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.prgbDedo9.Properties.LookAndFeel.SkinMaskColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.prgbDedo9.Properties.LookAndFeel.SkinName = "Visual Studio 2013 Blue";
            this.prgbDedo9.Properties.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.prgbDedo9.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.prgbDedo9.Properties.Maximum = 5;
            this.prgbDedo9.Properties.Minimum = 1;
            this.prgbDedo9.Properties.ProgressKind = DevExpress.XtraEditors.Controls.ProgressKind.Vertical;
            this.prgbDedo9.Properties.ProgressViewStyle = DevExpress.XtraEditors.Controls.ProgressViewStyle.Solid;
            this.prgbDedo9.Properties.StartColor = System.Drawing.Color.Black;
            this.prgbDedo9.Properties.Step = 5;
            this.prgbDedo9.Properties.TextOrientation = DevExpress.Utils.Drawing.TextOrientation.VerticalDownwards;
            this.prgbDedo9.Size = new System.Drawing.Size(8, 68);
            this.prgbDedo9.TabIndex = 296;
            // 
            // lblDedo8
            // 
            this.lblDedo8.Appearance.BackColor = System.Drawing.Color.Black;
            this.lblDedo8.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDedo8.Appearance.ForeColor = System.Drawing.Color.White;
            this.lblDedo8.Appearance.Options.UseBackColor = true;
            this.lblDedo8.Appearance.Options.UseFont = true;
            this.lblDedo8.Appearance.Options.UseForeColor = true;
            this.lblDedo8.Appearance.Options.UseTextOptions = true;
            this.lblDedo8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblDedo8.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblDedo8.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.lblDedo8.Location = new System.Drawing.Point(244, 31);
            this.lblDedo8.Margin = new System.Windows.Forms.Padding(4);
            this.lblDedo8.Name = "lblDedo8";
            this.lblDedo8.Size = new System.Drawing.Size(12, 16);
            this.lblDedo8.TabIndex = 295;
            this.lblDedo8.Text = "[]";
            // 
            // prgbDedo8
            // 
            this.prgbDedo8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.prgbDedo8.EditValue = 5;
            this.prgbDedo8.Location = new System.Drawing.Point(245, 52);
            this.prgbDedo8.Margin = new System.Windows.Forms.Padding(4);
            this.prgbDedo8.Name = "prgbDedo8";
            this.prgbDedo8.Properties.Appearance.BorderColor = System.Drawing.Color.Black;
            this.prgbDedo8.Properties.Appearance.ForeColor = System.Drawing.SystemColors.WindowText;
            this.prgbDedo8.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.prgbDedo8.Properties.EndColor = System.Drawing.Color.Black;
            this.prgbDedo8.Properties.LookAndFeel.SkinMaskColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.prgbDedo8.Properties.LookAndFeel.SkinMaskColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.prgbDedo8.Properties.LookAndFeel.SkinName = "Visual Studio 2013 Blue";
            this.prgbDedo8.Properties.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.prgbDedo8.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.prgbDedo8.Properties.Maximum = 5;
            this.prgbDedo8.Properties.Minimum = 1;
            this.prgbDedo8.Properties.ProgressKind = DevExpress.XtraEditors.Controls.ProgressKind.Vertical;
            this.prgbDedo8.Properties.ProgressViewStyle = DevExpress.XtraEditors.Controls.ProgressViewStyle.Solid;
            this.prgbDedo8.Properties.StartColor = System.Drawing.Color.Black;
            this.prgbDedo8.Properties.Step = 5;
            this.prgbDedo8.Properties.TextOrientation = DevExpress.Utils.Drawing.TextOrientation.VerticalDownwards;
            this.prgbDedo8.Size = new System.Drawing.Size(8, 68);
            this.prgbDedo8.TabIndex = 294;
            // 
            // lblDedo7
            // 
            this.lblDedo7.Appearance.BackColor = System.Drawing.Color.Black;
            this.lblDedo7.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDedo7.Appearance.ForeColor = System.Drawing.Color.White;
            this.lblDedo7.Appearance.Options.UseBackColor = true;
            this.lblDedo7.Appearance.Options.UseFont = true;
            this.lblDedo7.Appearance.Options.UseForeColor = true;
            this.lblDedo7.Appearance.Options.UseTextOptions = true;
            this.lblDedo7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblDedo7.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblDedo7.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.lblDedo7.Location = new System.Drawing.Point(358, 31);
            this.lblDedo7.Margin = new System.Windows.Forms.Padding(4);
            this.lblDedo7.Name = "lblDedo7";
            this.lblDedo7.Size = new System.Drawing.Size(12, 16);
            this.lblDedo7.TabIndex = 293;
            this.lblDedo7.Text = "[]";
            // 
            // prgbDedo7
            // 
            this.prgbDedo7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.prgbDedo7.EditValue = 5;
            this.prgbDedo7.Location = new System.Drawing.Point(359, 52);
            this.prgbDedo7.Margin = new System.Windows.Forms.Padding(4);
            this.prgbDedo7.Name = "prgbDedo7";
            this.prgbDedo7.Properties.Appearance.BorderColor = System.Drawing.Color.Black;
            this.prgbDedo7.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.prgbDedo7.Properties.EndColor = System.Drawing.Color.Black;
            this.prgbDedo7.Properties.LookAndFeel.SkinMaskColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.prgbDedo7.Properties.LookAndFeel.SkinMaskColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.prgbDedo7.Properties.LookAndFeel.SkinName = "Visual Studio 2013 Blue";
            this.prgbDedo7.Properties.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.prgbDedo7.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.prgbDedo7.Properties.Maximum = 5;
            this.prgbDedo7.Properties.Minimum = 1;
            this.prgbDedo7.Properties.ProgressKind = DevExpress.XtraEditors.Controls.ProgressKind.Vertical;
            this.prgbDedo7.Properties.ProgressViewStyle = DevExpress.XtraEditors.Controls.ProgressViewStyle.Solid;
            this.prgbDedo7.Properties.StartColor = System.Drawing.Color.Black;
            this.prgbDedo7.Properties.Step = 5;
            this.prgbDedo7.Properties.TextOrientation = DevExpress.Utils.Drawing.TextOrientation.VerticalDownwards;
            this.prgbDedo7.Size = new System.Drawing.Size(8, 68);
            this.prgbDedo7.TabIndex = 292;
            // 
            // picSeleccionDerecha
            // 
            this.picSeleccionDerecha.Cursor = System.Windows.Forms.Cursors.Default;
            this.picSeleccionDerecha.EditValue = ((object)(resources.GetObject("picSeleccionDerecha.EditValue")));
            this.picSeleccionDerecha.Location = new System.Drawing.Point(876, 173);
            this.picSeleccionDerecha.Margin = new System.Windows.Forms.Padding(4);
            this.picSeleccionDerecha.Name = "picSeleccionDerecha";
            this.picSeleccionDerecha.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.picSeleccionDerecha.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.picSeleccionDerecha.Properties.Appearance.Font = new System.Drawing.Font("Arial Black", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.picSeleccionDerecha.Properties.Appearance.ForeColor = System.Drawing.Color.DarkGray;
            this.picSeleccionDerecha.Properties.Appearance.Options.UseBackColor = true;
            this.picSeleccionDerecha.Properties.Appearance.Options.UseBorderColor = true;
            this.picSeleccionDerecha.Properties.Appearance.Options.UseFont = true;
            this.picSeleccionDerecha.Properties.Appearance.Options.UseForeColor = true;
            this.picSeleccionDerecha.Properties.Appearance.Options.UseTextOptions = true;
            this.picSeleccionDerecha.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.picSeleccionDerecha.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.picSeleccionDerecha.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.picSeleccionDerecha.Properties.ShowMenu = false;
            this.picSeleccionDerecha.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.picSeleccionDerecha.Size = new System.Drawing.Size(315, 269);
            this.picSeleccionDerecha.TabIndex = 275;
            // 
            // pnlManoDerecha
            // 
            this.pnlManoDerecha.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.pnlManoDerecha.Appearance.Options.UseBackColor = true;
            this.pnlManoDerecha.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlManoDerecha.Controls.Add(this.lblDedosDerecha);
            this.pnlManoDerecha.Controls.Add(this.labelControl38);
            this.pnlManoDerecha.Controls.Add(this.labelControl39);
            this.pnlManoDerecha.Controls.Add(this.pictureEdit13);
            this.pnlManoDerecha.Controls.Add(this.prgbDerechoGrande);
            this.pnlManoDerecha.Location = new System.Drawing.Point(876, 144);
            this.pnlManoDerecha.LookAndFeel.SkinName = "Blue";
            this.pnlManoDerecha.LookAndFeel.UseDefaultLookAndFeel = false;
            this.pnlManoDerecha.Margin = new System.Windows.Forms.Padding(4);
            this.pnlManoDerecha.Name = "pnlManoDerecha";
            this.pnlManoDerecha.Size = new System.Drawing.Size(315, 28);
            this.pnlManoDerecha.TabIndex = 290;
            // 
            // lblDedosDerecha
            // 
            this.lblDedosDerecha.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDedosDerecha.Appearance.Options.UseFont = true;
            this.lblDedosDerecha.Appearance.Options.UseTextOptions = true;
            this.lblDedosDerecha.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblDedosDerecha.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblDedosDerecha.Location = new System.Drawing.Point(42, 4);
            this.lblDedosDerecha.Margin = new System.Windows.Forms.Padding(4);
            this.lblDedosDerecha.Name = "lblDedosDerecha";
            this.lblDedosDerecha.Size = new System.Drawing.Size(72, 18);
            this.lblDedosDerecha.TabIndex = 233;
            this.lblDedosDerecha.Text = "[]";
            // 
            // labelControl38
            // 
            this.labelControl38.Appearance.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.labelControl38.Appearance.Options.UseFont = true;
            this.labelControl38.Appearance.Options.UseTextOptions = true;
            this.labelControl38.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl38.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl38.Location = new System.Drawing.Point(1, 4);
            this.labelControl38.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl38.Name = "labelControl38";
            this.labelControl38.Size = new System.Drawing.Size(39, 18);
            this.labelControl38.TabIndex = 233;
            this.labelControl38.Text = "Dedo:";
            // 
            // labelControl39
            // 
            this.labelControl39.Appearance.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.labelControl39.Appearance.Options.UseFont = true;
            this.labelControl39.Appearance.Options.UseTextOptions = true;
            this.labelControl39.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl39.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl39.Location = new System.Drawing.Point(113, 4);
            this.labelControl39.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl39.Name = "labelControl39";
            this.labelControl39.Size = new System.Drawing.Size(59, 18);
            this.labelControl39.TabIndex = 232;
            this.labelControl39.Text = "Calidad:";
            // 
            // pictureEdit13
            // 
            this.pictureEdit13.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit13.EditValue = ((object)(resources.GetObject("pictureEdit13.EditValue")));
            this.pictureEdit13.Location = new System.Drawing.Point(289, 2);
            this.pictureEdit13.Margin = new System.Windows.Forms.Padding(4);
            this.pictureEdit13.Name = "pictureEdit13";
            this.pictureEdit13.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit13.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit13.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit13.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit13.Properties.ShowMenu = false;
            this.pictureEdit13.Size = new System.Drawing.Size(20, 22);
            this.pictureEdit13.TabIndex = 232;
            // 
            // prgbDerechoGrande
            // 
            this.prgbDerechoGrande.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.prgbDerechoGrande.EditValue = 5;
            this.prgbDerechoGrande.Location = new System.Drawing.Point(176, 6);
            this.prgbDerechoGrande.Margin = new System.Windows.Forms.Padding(4);
            this.prgbDerechoGrande.Name = "prgbDerechoGrande";
            this.prgbDerechoGrande.Properties.Appearance.BorderColor = System.Drawing.Color.Black;
            this.prgbDerechoGrande.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.prgbDerechoGrande.Properties.EndColor = System.Drawing.Color.Black;
            this.prgbDerechoGrande.Properties.LookAndFeel.SkinMaskColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.prgbDerechoGrande.Properties.LookAndFeel.SkinMaskColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.prgbDerechoGrande.Properties.LookAndFeel.SkinName = "Visual Studio 2013 Blue";
            this.prgbDerechoGrande.Properties.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.prgbDerechoGrande.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.prgbDerechoGrande.Properties.Maximum = 5;
            this.prgbDerechoGrande.Properties.Minimum = 1;
            this.prgbDerechoGrande.Properties.ProgressViewStyle = DevExpress.XtraEditors.Controls.ProgressViewStyle.Solid;
            this.prgbDerechoGrande.Properties.StartColor = System.Drawing.Color.Black;
            this.prgbDerechoGrande.Properties.Step = 5;
            this.prgbDerechoGrande.Properties.TextOrientation = DevExpress.Utils.Drawing.TextOrientation.VerticalDownwards;
            this.prgbDerechoGrande.Size = new System.Drawing.Size(109, 12);
            this.prgbDerechoGrande.TabIndex = 232;
            // 
            // picSeleccionIzquierda
            // 
            this.picSeleccionIzquierda.Cursor = System.Windows.Forms.Cursors.Default;
            this.picSeleccionIzquierda.EditValue = ((object)(resources.GetObject("picSeleccionIzquierda.EditValue")));
            this.picSeleccionIzquierda.Location = new System.Drawing.Point(117, 173);
            this.picSeleccionIzquierda.Margin = new System.Windows.Forms.Padding(4);
            this.picSeleccionIzquierda.Name = "picSeleccionIzquierda";
            this.picSeleccionIzquierda.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.picSeleccionIzquierda.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.picSeleccionIzquierda.Properties.Appearance.Font = new System.Drawing.Font("Arial Black", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.picSeleccionIzquierda.Properties.Appearance.ForeColor = System.Drawing.Color.DarkGray;
            this.picSeleccionIzquierda.Properties.Appearance.Options.UseBackColor = true;
            this.picSeleccionIzquierda.Properties.Appearance.Options.UseBorderColor = true;
            this.picSeleccionIzquierda.Properties.Appearance.Options.UseFont = true;
            this.picSeleccionIzquierda.Properties.Appearance.Options.UseForeColor = true;
            this.picSeleccionIzquierda.Properties.Appearance.Options.UseTextOptions = true;
            this.picSeleccionIzquierda.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.picSeleccionIzquierda.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.picSeleccionIzquierda.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.picSeleccionIzquierda.Properties.ShowMenu = false;
            this.picSeleccionIzquierda.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.picSeleccionIzquierda.Size = new System.Drawing.Size(315, 269);
            this.picSeleccionIzquierda.TabIndex = 274;
            // 
            // pnlManoIzquierda
            // 
            this.pnlManoIzquierda.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.pnlManoIzquierda.Appearance.Options.UseBackColor = true;
            this.pnlManoIzquierda.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlManoIzquierda.Controls.Add(this.lblDedosIzquierda);
            this.pnlManoIzquierda.Controls.Add(this.labelControl36);
            this.pnlManoIzquierda.Controls.Add(this.labelControl29);
            this.pnlManoIzquierda.Controls.Add(this.pictureEdit12);
            this.pnlManoIzquierda.Controls.Add(this.prgbIzquierdoGrande);
            this.pnlManoIzquierda.Location = new System.Drawing.Point(117, 145);
            this.pnlManoIzquierda.LookAndFeel.SkinName = "Blue";
            this.pnlManoIzquierda.LookAndFeel.UseDefaultLookAndFeel = false;
            this.pnlManoIzquierda.Margin = new System.Windows.Forms.Padding(4);
            this.pnlManoIzquierda.Name = "pnlManoIzquierda";
            this.pnlManoIzquierda.Size = new System.Drawing.Size(315, 28);
            this.pnlManoIzquierda.TabIndex = 289;
            // 
            // lblDedosIzquierda
            // 
            this.lblDedosIzquierda.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDedosIzquierda.Appearance.Options.UseFont = true;
            this.lblDedosIzquierda.Appearance.Options.UseTextOptions = true;
            this.lblDedosIzquierda.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblDedosIzquierda.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblDedosIzquierda.Location = new System.Drawing.Point(47, 4);
            this.lblDedosIzquierda.Margin = new System.Windows.Forms.Padding(4);
            this.lblDedosIzquierda.Name = "lblDedosIzquierda";
            this.lblDedosIzquierda.Size = new System.Drawing.Size(72, 18);
            this.lblDedosIzquierda.TabIndex = 233;
            this.lblDedosIzquierda.Text = "[]";
            // 
            // labelControl36
            // 
            this.labelControl36.Appearance.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.labelControl36.Appearance.Options.UseFont = true;
            this.labelControl36.Appearance.Options.UseTextOptions = true;
            this.labelControl36.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl36.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl36.Location = new System.Drawing.Point(4, 4);
            this.labelControl36.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl36.Name = "labelControl36";
            this.labelControl36.Size = new System.Drawing.Size(39, 18);
            this.labelControl36.TabIndex = 233;
            this.labelControl36.Text = "Dedo:";
            // 
            // labelControl29
            // 
            this.labelControl29.Appearance.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.labelControl29.Appearance.Options.UseFont = true;
            this.labelControl29.Appearance.Options.UseTextOptions = true;
            this.labelControl29.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl29.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl29.Location = new System.Drawing.Point(116, 4);
            this.labelControl29.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl29.Name = "labelControl29";
            this.labelControl29.Size = new System.Drawing.Size(59, 18);
            this.labelControl29.TabIndex = 232;
            this.labelControl29.Text = "Calidad:";
            // 
            // pictureEdit12
            // 
            this.pictureEdit12.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit12.EditValue = ((object)(resources.GetObject("pictureEdit12.EditValue")));
            this.pictureEdit12.Location = new System.Drawing.Point(289, 2);
            this.pictureEdit12.Margin = new System.Windows.Forms.Padding(4);
            this.pictureEdit12.Name = "pictureEdit12";
            this.pictureEdit12.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit12.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit12.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit12.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit12.Properties.ShowMenu = false;
            this.pictureEdit12.Size = new System.Drawing.Size(25, 22);
            this.pictureEdit12.TabIndex = 232;
            // 
            // prgbIzquierdoGrande
            // 
            this.prgbIzquierdoGrande.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.prgbIzquierdoGrande.EditValue = 5;
            this.prgbIzquierdoGrande.Location = new System.Drawing.Point(179, 8);
            this.prgbIzquierdoGrande.Margin = new System.Windows.Forms.Padding(4);
            this.prgbIzquierdoGrande.Name = "prgbIzquierdoGrande";
            this.prgbIzquierdoGrande.Properties.Appearance.BorderColor = System.Drawing.Color.Black;
            this.prgbIzquierdoGrande.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.prgbIzquierdoGrande.Properties.EndColor = System.Drawing.Color.Black;
            this.prgbIzquierdoGrande.Properties.LookAndFeel.SkinMaskColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.prgbIzquierdoGrande.Properties.LookAndFeel.SkinMaskColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.prgbIzquierdoGrande.Properties.LookAndFeel.SkinName = "Visual Studio 2013 Blue";
            this.prgbIzquierdoGrande.Properties.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.prgbIzquierdoGrande.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.prgbIzquierdoGrande.Properties.Maximum = 5;
            this.prgbIzquierdoGrande.Properties.Minimum = 1;
            this.prgbIzquierdoGrande.Properties.ProgressViewStyle = DevExpress.XtraEditors.Controls.ProgressViewStyle.Solid;
            this.prgbIzquierdoGrande.Properties.StartColor = System.Drawing.Color.Black;
            this.prgbIzquierdoGrande.Properties.Step = 5;
            this.prgbIzquierdoGrande.Properties.TextOrientation = DevExpress.Utils.Drawing.TextOrientation.VerticalDownwards;
            this.prgbIzquierdoGrande.Size = new System.Drawing.Size(109, 12);
            this.prgbIzquierdoGrande.TabIndex = 232;
            // 
            // labelControl35
            // 
            this.labelControl35.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl35.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl35.Appearance.Options.UseFont = true;
            this.labelControl35.Appearance.Options.UseForeColor = true;
            this.labelControl35.Location = new System.Drawing.Point(1219, 4);
            this.labelControl35.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl35.Name = "labelControl35";
            this.labelControl35.Size = new System.Drawing.Size(68, 23);
            this.labelControl35.TabIndex = 288;
            this.labelControl35.Text = "Meñique";
            // 
            // labelControl34
            // 
            this.labelControl34.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl34.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl34.Appearance.Options.UseFont = true;
            this.labelControl34.Appearance.Options.UseForeColor = true;
            this.labelControl34.Location = new System.Drawing.Point(1102, 4);
            this.labelControl34.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl34.Name = "labelControl34";
            this.labelControl34.Size = new System.Drawing.Size(50, 23);
            this.labelControl34.TabIndex = 287;
            this.labelControl34.Text = "Anular";
            // 
            // labelControl33
            // 
            this.labelControl33.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl33.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl33.Appearance.Options.UseFont = true;
            this.labelControl33.Appearance.Options.UseForeColor = true;
            this.labelControl33.Location = new System.Drawing.Point(1001, 4);
            this.labelControl33.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl33.Name = "labelControl33";
            this.labelControl33.Size = new System.Drawing.Size(49, 23);
            this.labelControl33.TabIndex = 286;
            this.labelControl33.Text = "Medio";
            // 
            // labelControl32
            // 
            this.labelControl32.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl32.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl32.Appearance.Options.UseFont = true;
            this.labelControl32.Appearance.Options.UseForeColor = true;
            this.labelControl32.Location = new System.Drawing.Point(886, 2);
            this.labelControl32.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl32.Name = "labelControl32";
            this.labelControl32.Size = new System.Drawing.Size(46, 23);
            this.labelControl32.TabIndex = 285;
            this.labelControl32.Text = "Indice";
            // 
            // labelControl31
            // 
            this.labelControl31.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl31.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl31.Appearance.Options.UseFont = true;
            this.labelControl31.Appearance.Options.UseForeColor = true;
            this.labelControl31.Location = new System.Drawing.Point(774, 5);
            this.labelControl31.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl31.Name = "labelControl31";
            this.labelControl31.Size = new System.Drawing.Size(49, 23);
            this.labelControl31.TabIndex = 284;
            this.labelControl31.Text = "Pulgar";
            // 
            // lblDedo6
            // 
            this.lblDedo6.Appearance.BackColor = System.Drawing.Color.Black;
            this.lblDedo6.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDedo6.Appearance.ForeColor = System.Drawing.Color.White;
            this.lblDedo6.Appearance.Options.UseBackColor = true;
            this.lblDedo6.Appearance.Options.UseFont = true;
            this.lblDedo6.Appearance.Options.UseForeColor = true;
            this.lblDedo6.Appearance.Options.UseTextOptions = true;
            this.lblDedo6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblDedo6.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblDedo6.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.lblDedo6.Location = new System.Drawing.Point(469, 31);
            this.lblDedo6.Margin = new System.Windows.Forms.Padding(4);
            this.lblDedo6.Name = "lblDedo6";
            this.lblDedo6.Size = new System.Drawing.Size(12, 16);
            this.lblDedo6.TabIndex = 283;
            this.lblDedo6.Text = "[]";
            // 
            // prgbDedo6
            // 
            this.prgbDedo6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.prgbDedo6.EditValue = 5;
            this.prgbDedo6.Location = new System.Drawing.Point(471, 52);
            this.prgbDedo6.Margin = new System.Windows.Forms.Padding(4);
            this.prgbDedo6.Name = "prgbDedo6";
            this.prgbDedo6.Properties.Appearance.BorderColor = System.Drawing.Color.Black;
            this.prgbDedo6.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.prgbDedo6.Properties.EndColor = System.Drawing.Color.Black;
            this.prgbDedo6.Properties.LookAndFeel.SkinMaskColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.prgbDedo6.Properties.LookAndFeel.SkinMaskColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.prgbDedo6.Properties.LookAndFeel.SkinName = "Visual Studio 2013 Blue";
            this.prgbDedo6.Properties.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.prgbDedo6.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.prgbDedo6.Properties.Maximum = 5;
            this.prgbDedo6.Properties.Minimum = 1;
            this.prgbDedo6.Properties.ProgressKind = DevExpress.XtraEditors.Controls.ProgressKind.Vertical;
            this.prgbDedo6.Properties.ProgressViewStyle = DevExpress.XtraEditors.Controls.ProgressViewStyle.Solid;
            this.prgbDedo6.Properties.StartColor = System.Drawing.Color.Black;
            this.prgbDedo6.Properties.Step = 5;
            this.prgbDedo6.Properties.TextOrientation = DevExpress.Utils.Drawing.TextOrientation.VerticalDownwards;
            this.prgbDedo6.Size = new System.Drawing.Size(8, 68);
            this.prgbDedo6.TabIndex = 282;
            // 
            // picPulgarIzquierdo
            // 
            this.picPulgarIzquierdo.Cursor = System.Windows.Forms.Cursors.Default;
            this.picPulgarIzquierdo.Location = new System.Drawing.Point(465, 28);
            this.picPulgarIzquierdo.Margin = new System.Windows.Forms.Padding(4);
            this.picPulgarIzquierdo.Name = "picPulgarIzquierdo";
            this.picPulgarIzquierdo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.picPulgarIzquierdo.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.picPulgarIzquierdo.Properties.Appearance.Font = new System.Drawing.Font("Arial Black", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.picPulgarIzquierdo.Properties.Appearance.ForeColor = System.Drawing.Color.DarkGray;
            this.picPulgarIzquierdo.Properties.Appearance.Options.UseBackColor = true;
            this.picPulgarIzquierdo.Properties.Appearance.Options.UseBorderColor = true;
            this.picPulgarIzquierdo.Properties.Appearance.Options.UseFont = true;
            this.picPulgarIzquierdo.Properties.Appearance.Options.UseForeColor = true;
            this.picPulgarIzquierdo.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.picPulgarIzquierdo.Properties.NullText = "6";
            this.picPulgarIzquierdo.Properties.ShowMenu = false;
            this.picPulgarIzquierdo.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.picPulgarIzquierdo.Size = new System.Drawing.Size(108, 98);
            this.picPulgarIzquierdo.TabIndex = 268;
            this.picPulgarIzquierdo.ToolTip = "Pulgar Izquierdo";
            // 
            // picMeniequeDerecho
            // 
            this.picMeniequeDerecho.Cursor = System.Windows.Forms.Cursors.Default;
            this.picMeniequeDerecho.Location = new System.Drawing.Point(1195, 28);
            this.picMeniequeDerecho.Margin = new System.Windows.Forms.Padding(4);
            this.picMeniequeDerecho.Name = "picMeniequeDerecho";
            this.picMeniequeDerecho.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.picMeniequeDerecho.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.picMeniequeDerecho.Properties.Appearance.Font = new System.Drawing.Font("Arial Black", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.picMeniequeDerecho.Properties.Appearance.ForeColor = System.Drawing.Color.DarkGray;
            this.picMeniequeDerecho.Properties.Appearance.Options.UseBackColor = true;
            this.picMeniequeDerecho.Properties.Appearance.Options.UseBorderColor = true;
            this.picMeniequeDerecho.Properties.Appearance.Options.UseFont = true;
            this.picMeniequeDerecho.Properties.Appearance.Options.UseForeColor = true;
            this.picMeniequeDerecho.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.picMeniequeDerecho.Properties.NullText = "5";
            this.picMeniequeDerecho.Properties.ShowMenu = false;
            this.picMeniequeDerecho.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.picMeniequeDerecho.Size = new System.Drawing.Size(108, 98);
            this.picMeniequeDerecho.TabIndex = 281;
            this.picMeniequeDerecho.ToolTip = "Meñique Derecho";
            // 
            // labelControl30
            // 
            this.labelControl30.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl30.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl30.Appearance.Options.UseFont = true;
            this.labelControl30.Appearance.Options.UseForeColor = true;
            this.labelControl30.Location = new System.Drawing.Point(32, 2);
            this.labelControl30.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl30.Name = "labelControl30";
            this.labelControl30.Size = new System.Drawing.Size(68, 23);
            this.labelControl30.TabIndex = 280;
            this.labelControl30.Text = "Meñique";
            // 
            // labelControl28
            // 
            this.labelControl28.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl28.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl28.Appearance.Options.UseFont = true;
            this.labelControl28.Appearance.Options.UseForeColor = true;
            this.labelControl28.Location = new System.Drawing.Point(156, 2);
            this.labelControl28.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl28.Name = "labelControl28";
            this.labelControl28.Size = new System.Drawing.Size(50, 23);
            this.labelControl28.TabIndex = 279;
            this.labelControl28.Text = "Anular";
            // 
            // labelControl27
            // 
            this.labelControl27.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl27.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl27.Appearance.Options.UseFont = true;
            this.labelControl27.Appearance.Options.UseForeColor = true;
            this.labelControl27.Location = new System.Drawing.Point(270, 4);
            this.labelControl27.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl27.Name = "labelControl27";
            this.labelControl27.Size = new System.Drawing.Size(49, 23);
            this.labelControl27.TabIndex = 278;
            this.labelControl27.Text = "Medio";
            // 
            // labelControl26
            // 
            this.labelControl26.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl26.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl26.Appearance.Options.UseFont = true;
            this.labelControl26.Appearance.Options.UseForeColor = true;
            this.labelControl26.Location = new System.Drawing.Point(384, 2);
            this.labelControl26.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl26.Name = "labelControl26";
            this.labelControl26.Size = new System.Drawing.Size(46, 23);
            this.labelControl26.TabIndex = 277;
            this.labelControl26.Text = "Indice";
            // 
            // labelControl25
            // 
            this.labelControl25.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl25.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl25.Appearance.Options.UseFont = true;
            this.labelControl25.Appearance.Options.UseForeColor = true;
            this.labelControl25.Location = new System.Drawing.Point(501, 2);
            this.labelControl25.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(49, 23);
            this.labelControl25.TabIndex = 276;
            this.labelControl25.Text = "Pulgar";
            // 
            // labelControl13
            // 
            this.labelControl13.Location = new System.Drawing.Point(684, 108);
            this.labelControl13.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(47, 16);
            this.labelControl13.TabIndex = 272;
            this.labelControl13.Text = "Derecha";
            // 
            // labelControl18
            // 
            this.labelControl18.Location = new System.Drawing.Point(579, 108);
            this.labelControl18.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(53, 16);
            this.labelControl18.TabIndex = 271;
            this.labelControl18.Text = "Izquierda";
            // 
            // picIndiceIzquierdo
            // 
            this.picIndiceIzquierdo.AllowDrop = true;
            this.picIndiceIzquierdo.AllowHtmlTextInToolTip = DevExpress.Utils.DefaultBoolean.True;
            this.picIndiceIzquierdo.Cursor = System.Windows.Forms.Cursors.Default;
            this.picIndiceIzquierdo.Location = new System.Drawing.Point(352, 28);
            this.picIndiceIzquierdo.Margin = new System.Windows.Forms.Padding(4);
            this.picIndiceIzquierdo.Name = "picIndiceIzquierdo";
            this.picIndiceIzquierdo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.picIndiceIzquierdo.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.picIndiceIzquierdo.Properties.Appearance.Font = new System.Drawing.Font("Arial Black", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.picIndiceIzquierdo.Properties.Appearance.ForeColor = System.Drawing.Color.DarkGray;
            this.picIndiceIzquierdo.Properties.Appearance.Options.UseBackColor = true;
            this.picIndiceIzquierdo.Properties.Appearance.Options.UseBorderColor = true;
            this.picIndiceIzquierdo.Properties.Appearance.Options.UseFont = true;
            this.picIndiceIzquierdo.Properties.Appearance.Options.UseForeColor = true;
            this.picIndiceIzquierdo.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.picIndiceIzquierdo.Properties.NullText = "7";
            this.picIndiceIzquierdo.Properties.ShowMenu = false;
            this.picIndiceIzquierdo.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.picIndiceIzquierdo.Size = new System.Drawing.Size(108, 98);
            this.picIndiceIzquierdo.TabIndex = 269;
            this.picIndiceIzquierdo.ToolTip = "Indice Izquierdo";
            // 
            // picMedioIzquierdo
            // 
            this.picMedioIzquierdo.Cursor = System.Windows.Forms.Cursors.Default;
            this.picMedioIzquierdo.Location = new System.Drawing.Point(240, 28);
            this.picMedioIzquierdo.Margin = new System.Windows.Forms.Padding(4);
            this.picMedioIzquierdo.Name = "picMedioIzquierdo";
            this.picMedioIzquierdo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.picMedioIzquierdo.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.picMedioIzquierdo.Properties.Appearance.Font = new System.Drawing.Font("Arial Black", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.picMedioIzquierdo.Properties.Appearance.ForeColor = System.Drawing.Color.DarkGray;
            this.picMedioIzquierdo.Properties.Appearance.Options.UseBackColor = true;
            this.picMedioIzquierdo.Properties.Appearance.Options.UseBorderColor = true;
            this.picMedioIzquierdo.Properties.Appearance.Options.UseFont = true;
            this.picMedioIzquierdo.Properties.Appearance.Options.UseForeColor = true;
            this.picMedioIzquierdo.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.picMedioIzquierdo.Properties.NullText = "8";
            this.picMedioIzquierdo.Properties.ShowMenu = false;
            this.picMedioIzquierdo.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.picMedioIzquierdo.Size = new System.Drawing.Size(108, 98);
            this.picMedioIzquierdo.TabIndex = 267;
            this.picMedioIzquierdo.ToolTip = "Medio Izquierdo";
            // 
            // picMeniequeIzquierdo
            // 
            this.picMeniequeIzquierdo.Cursor = System.Windows.Forms.Cursors.Default;
            this.picMeniequeIzquierdo.Location = new System.Drawing.Point(16, 28);
            this.picMeniequeIzquierdo.Margin = new System.Windows.Forms.Padding(4);
            this.picMeniequeIzquierdo.Name = "picMeniequeIzquierdo";
            this.picMeniequeIzquierdo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.picMeniequeIzquierdo.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.picMeniequeIzquierdo.Properties.Appearance.Font = new System.Drawing.Font("Arial Black", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.picMeniequeIzquierdo.Properties.Appearance.ForeColor = System.Drawing.Color.DarkGray;
            this.picMeniequeIzquierdo.Properties.Appearance.Options.UseBackColor = true;
            this.picMeniequeIzquierdo.Properties.Appearance.Options.UseBorderColor = true;
            this.picMeniequeIzquierdo.Properties.Appearance.Options.UseFont = true;
            this.picMeniequeIzquierdo.Properties.Appearance.Options.UseForeColor = true;
            this.picMeniequeIzquierdo.Properties.Appearance.Options.UseTextOptions = true;
            this.picMeniequeIzquierdo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.picMeniequeIzquierdo.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.picMeniequeIzquierdo.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.picMeniequeIzquierdo.Properties.NullText = "10";
            this.picMeniequeIzquierdo.Properties.ShowMenu = false;
            this.picMeniequeIzquierdo.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.picMeniequeIzquierdo.Size = new System.Drawing.Size(108, 98);
            this.picMeniequeIzquierdo.TabIndex = 265;
            this.picMeniequeIzquierdo.ToolTip = "Meñique Izquierdo";
            // 
            // picAnularIzquierdo
            // 
            this.picAnularIzquierdo.Cursor = System.Windows.Forms.Cursors.Default;
            this.picAnularIzquierdo.Location = new System.Drawing.Point(128, 28);
            this.picAnularIzquierdo.Margin = new System.Windows.Forms.Padding(4);
            this.picAnularIzquierdo.Name = "picAnularIzquierdo";
            this.picAnularIzquierdo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.picAnularIzquierdo.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.picAnularIzquierdo.Properties.Appearance.Font = new System.Drawing.Font("Arial Black", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.picAnularIzquierdo.Properties.Appearance.ForeColor = System.Drawing.Color.DarkGray;
            this.picAnularIzquierdo.Properties.Appearance.Options.UseBackColor = true;
            this.picAnularIzquierdo.Properties.Appearance.Options.UseBorderColor = true;
            this.picAnularIzquierdo.Properties.Appearance.Options.UseFont = true;
            this.picAnularIzquierdo.Properties.Appearance.Options.UseForeColor = true;
            this.picAnularIzquierdo.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.picAnularIzquierdo.Properties.NullText = "9";
            this.picAnularIzquierdo.Properties.ShowMenu = false;
            this.picAnularIzquierdo.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.picAnularIzquierdo.Size = new System.Drawing.Size(108, 98);
            this.picAnularIzquierdo.TabIndex = 266;
            this.picAnularIzquierdo.ToolTip = "Anular Izquierdo";
            // 
            // picAnularDerecho
            // 
            this.picAnularDerecho.Cursor = System.Windows.Forms.Cursors.Default;
            this.picAnularDerecho.Location = new System.Drawing.Point(1080, 28);
            this.picAnularDerecho.Margin = new System.Windows.Forms.Padding(4);
            this.picAnularDerecho.Name = "picAnularDerecho";
            this.picAnularDerecho.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.picAnularDerecho.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.picAnularDerecho.Properties.Appearance.Font = new System.Drawing.Font("Arial Black", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.picAnularDerecho.Properties.Appearance.ForeColor = System.Drawing.Color.DarkGray;
            this.picAnularDerecho.Properties.Appearance.Options.UseBackColor = true;
            this.picAnularDerecho.Properties.Appearance.Options.UseBorderColor = true;
            this.picAnularDerecho.Properties.Appearance.Options.UseFont = true;
            this.picAnularDerecho.Properties.Appearance.Options.UseForeColor = true;
            this.picAnularDerecho.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.picAnularDerecho.Properties.NullText = "4";
            this.picAnularDerecho.Properties.ShowMenu = false;
            this.picAnularDerecho.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.picAnularDerecho.Size = new System.Drawing.Size(108, 98);
            this.picAnularDerecho.TabIndex = 263;
            this.picAnularDerecho.ToolTip = "Anular Derecho";
            // 
            // picMedioDerecho
            // 
            this.picMedioDerecho.Cursor = System.Windows.Forms.Cursors.Default;
            this.picMedioDerecho.Location = new System.Drawing.Point(969, 28);
            this.picMedioDerecho.Margin = new System.Windows.Forms.Padding(4);
            this.picMedioDerecho.Name = "picMedioDerecho";
            this.picMedioDerecho.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.picMedioDerecho.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.picMedioDerecho.Properties.Appearance.Font = new System.Drawing.Font("Arial Black", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.picMedioDerecho.Properties.Appearance.ForeColor = System.Drawing.Color.DarkGray;
            this.picMedioDerecho.Properties.Appearance.Options.UseBackColor = true;
            this.picMedioDerecho.Properties.Appearance.Options.UseBorderColor = true;
            this.picMedioDerecho.Properties.Appearance.Options.UseFont = true;
            this.picMedioDerecho.Properties.Appearance.Options.UseForeColor = true;
            this.picMedioDerecho.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.picMedioDerecho.Properties.NullText = "3";
            this.picMedioDerecho.Properties.ShowMenu = false;
            this.picMedioDerecho.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.picMedioDerecho.Size = new System.Drawing.Size(108, 98);
            this.picMedioDerecho.TabIndex = 262;
            this.picMedioDerecho.ToolTip = "Medio Derecho";
            // 
            // picPulgarDerecho
            // 
            this.picPulgarDerecho.Cursor = System.Windows.Forms.Cursors.Default;
            this.picPulgarDerecho.Location = new System.Drawing.Point(745, 28);
            this.picPulgarDerecho.Margin = new System.Windows.Forms.Padding(4);
            this.picPulgarDerecho.Name = "picPulgarDerecho";
            this.picPulgarDerecho.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.picPulgarDerecho.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.picPulgarDerecho.Properties.Appearance.Font = new System.Drawing.Font("Arial Black", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.picPulgarDerecho.Properties.Appearance.ForeColor = System.Drawing.Color.DarkGray;
            this.picPulgarDerecho.Properties.Appearance.Options.UseBackColor = true;
            this.picPulgarDerecho.Properties.Appearance.Options.UseBorderColor = true;
            this.picPulgarDerecho.Properties.Appearance.Options.UseFont = true;
            this.picPulgarDerecho.Properties.Appearance.Options.UseForeColor = true;
            this.picPulgarDerecho.Properties.Appearance.Options.UseTextOptions = true;
            this.picPulgarDerecho.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.picPulgarDerecho.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.picPulgarDerecho.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.picPulgarDerecho.Properties.NullText = "1";
            this.picPulgarDerecho.Properties.ShowMenu = false;
            this.picPulgarDerecho.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.picPulgarDerecho.Size = new System.Drawing.Size(108, 98);
            this.picPulgarDerecho.TabIndex = 260;
            this.picPulgarDerecho.ToolTip = "Pulgar Derecho";
            // 
            // picIndiceDerecho
            // 
            this.picIndiceDerecho.Cursor = System.Windows.Forms.Cursors.Default;
            this.picIndiceDerecho.Location = new System.Drawing.Point(858, 28);
            this.picIndiceDerecho.Margin = new System.Windows.Forms.Padding(4);
            this.picIndiceDerecho.Name = "picIndiceDerecho";
            this.picIndiceDerecho.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.picIndiceDerecho.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.picIndiceDerecho.Properties.Appearance.Font = new System.Drawing.Font("Arial Black", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.picIndiceDerecho.Properties.Appearance.ForeColor = System.Drawing.Color.DarkGray;
            this.picIndiceDerecho.Properties.Appearance.Options.UseBackColor = true;
            this.picIndiceDerecho.Properties.Appearance.Options.UseBorderColor = true;
            this.picIndiceDerecho.Properties.Appearance.Options.UseFont = true;
            this.picIndiceDerecho.Properties.Appearance.Options.UseForeColor = true;
            this.picIndiceDerecho.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.picIndiceDerecho.Properties.NullText = "2";
            this.picIndiceDerecho.Properties.ShowMenu = false;
            this.picIndiceDerecho.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.picIndiceDerecho.Size = new System.Drawing.Size(108, 98);
            this.picIndiceDerecho.TabIndex = 261;
            this.picIndiceDerecho.ToolTip = "Indice Derecho";
            // 
            // picManoIzquierda
            // 
            this.picManoIzquierda.Cursor = System.Windows.Forms.Cursors.Default;
            this.picManoIzquierda.EditValue = ((object)(resources.GetObject("picManoIzquierda.EditValue")));
            this.picManoIzquierda.Location = new System.Drawing.Point(565, 42);
            this.picManoIzquierda.Margin = new System.Windows.Forms.Padding(4);
            this.picManoIzquierda.Name = "picManoIzquierda";
            this.picManoIzquierda.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picManoIzquierda.Properties.Appearance.Options.UseBackColor = true;
            this.picManoIzquierda.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.picManoIzquierda.Properties.PictureAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.picManoIzquierda.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picManoIzquierda.Properties.ShowMenu = false;
            this.picManoIzquierda.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.picManoIzquierda.Size = new System.Drawing.Size(76, 70);
            this.picManoIzquierda.TabIndex = 270;
            // 
            // picManoDerecha
            // 
            this.picManoDerecha.Cursor = System.Windows.Forms.Cursors.Default;
            this.picManoDerecha.EditValue = ((object)(resources.GetObject("picManoDerecha.EditValue")));
            this.picManoDerecha.Location = new System.Drawing.Point(669, 42);
            this.picManoDerecha.Margin = new System.Windows.Forms.Padding(4);
            this.picManoDerecha.Name = "picManoDerecha";
            this.picManoDerecha.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picManoDerecha.Properties.Appearance.Options.UseBackColor = true;
            this.picManoDerecha.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.picManoDerecha.Properties.PictureAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.picManoDerecha.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picManoDerecha.Properties.ShowMenu = false;
            this.picManoDerecha.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.picManoDerecha.Size = new System.Drawing.Size(76, 70);
            this.picManoDerecha.TabIndex = 264;
            // 
            // navigationPage1
            // 
            this.navigationPage1.Caption = "navigationPage1";
            this.navigationPage1.Controls.Add(this.btnRotar);
            this.navigationPage1.Controls.Add(this.sbtnIniciarCamara);
            this.navigationPage1.Controls.Add(this.btnAdjuntarFoto);
            this.navigationPage1.Controls.Add(this.picFotoCapturada);
            this.navigationPage1.Controls.Add(this.pnlObservacionFoto);
            this.navigationPage1.Margin = new System.Windows.Forms.Padding(4);
            this.navigationPage1.Name = "navigationPage1";
            this.navigationPage1.Size = new System.Drawing.Size(1359, 591);
            this.navigationPage1.Paint += new System.Windows.Forms.PaintEventHandler(this.navigationPage1_Paint);
            // 
            // btnRotar
            // 
            this.btnRotar.Appearance.BackColor = System.Drawing.Color.AliceBlue;
            this.btnRotar.Appearance.Font = new System.Drawing.Font("Trebuchet MS", 9.75F);
            this.btnRotar.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.btnRotar.Appearance.Options.UseBackColor = true;
            this.btnRotar.Appearance.Options.UseFont = true;
            this.btnRotar.Appearance.Options.UseForeColor = true;
            this.btnRotar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnRotar.ImageOptions.Image")));
            this.btnRotar.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnRotar.Location = new System.Drawing.Point(558, 14);
            this.btnRotar.LookAndFeel.SkinName = "Office 2016 Colorful";
            this.btnRotar.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnRotar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnRotar.Name = "btnRotar";
            this.btnRotar.Size = new System.Drawing.Size(101, 33);
            this.btnRotar.TabIndex = 301;
            this.btnRotar.Text = "Rotar";
            this.btnRotar.Visible = false;
            this.btnRotar.Click += new System.EventHandler(this.btnRotar_Click);
            // 
            // sbtnIniciarCamara
            // 
            this.sbtnIniciarCamara.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.sbtnIniciarCamara.Appearance.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sbtnIniciarCamara.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.sbtnIniciarCamara.Appearance.Options.UseBackColor = true;
            this.sbtnIniciarCamara.Appearance.Options.UseFont = true;
            this.sbtnIniciarCamara.Appearance.Options.UseForeColor = true;
            this.sbtnIniciarCamara.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.sbtnIniciarCamara.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.sbtnIniciarCamara.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.sbtnIniciarCamara.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("sbtnIniciarCamara.ImageOptions.SvgImage")));
            this.sbtnIniciarCamara.Location = new System.Drawing.Point(557, 409);
            this.sbtnIniciarCamara.Margin = new System.Windows.Forms.Padding(4);
            this.sbtnIniciarCamara.Name = "sbtnIniciarCamara";
            this.sbtnIniciarCamara.Size = new System.Drawing.Size(276, 37);
            this.sbtnIniciarCamara.TabIndex = 292;
            this.sbtnIniciarCamara.Text = "Iniciar Cámara";
            this.sbtnIniciarCamara.Click += new System.EventHandler(this.sbtnIniciarCamara_Click);
            // 
            // btnAdjuntarFoto
            // 
            this.btnAdjuntarFoto.Appearance.BackColor = System.Drawing.Color.AliceBlue;
            this.btnAdjuntarFoto.Appearance.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdjuntarFoto.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btnAdjuntarFoto.Appearance.Options.UseBackColor = true;
            this.btnAdjuntarFoto.Appearance.Options.UseFont = true;
            this.btnAdjuntarFoto.Appearance.Options.UseForeColor = true;
            this.btnAdjuntarFoto.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnAdjuntarFoto.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.identity_;
            this.btnAdjuntarFoto.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnAdjuntarFoto.Location = new System.Drawing.Point(557, 409);
            this.btnAdjuntarFoto.Margin = new System.Windows.Forms.Padding(4);
            this.btnAdjuntarFoto.Name = "btnAdjuntarFoto";
            this.btnAdjuntarFoto.Size = new System.Drawing.Size(276, 37);
            this.btnAdjuntarFoto.TabIndex = 300;
            this.btnAdjuntarFoto.Text = "Adjuntar Fotografía";
            this.btnAdjuntarFoto.Click += new System.EventHandler(this.btnAdjuntarFoto_Click);
            // 
            // picFotoCapturada
            // 
            this.picFotoCapturada.BackgroundImage = global::Gv.ExodusBc.UI.Properties.Resources.Login4;
            this.picFotoCapturada.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.picFotoCapturada.Cursor = System.Windows.Forms.Cursors.Default;
            this.picFotoCapturada.Location = new System.Drawing.Point(557, 51);
            this.picFotoCapturada.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.picFotoCapturada.Name = "picFotoCapturada";
            this.picFotoCapturada.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picFotoCapturada.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.picFotoCapturada.Properties.Appearance.Options.UseBackColor = true;
            this.picFotoCapturada.Properties.Appearance.Options.UseBorderColor = true;
            this.picFotoCapturada.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.picFotoCapturada.Properties.NullText = " ";
            this.picFotoCapturada.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picFotoCapturada.Properties.ShowMenu = false;
            this.picFotoCapturada.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.picFotoCapturada.Size = new System.Drawing.Size(276, 350);
            this.picFotoCapturada.TabIndex = 299;
            // 
            // pnlObservacionFoto
            // 
            this.pnlObservacionFoto.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlObservacionFoto.Controls.Add(this.mmObservacionFoto);
            this.pnlObservacionFoto.Controls.Add(this.label25);
            this.pnlObservacionFoto.Controls.Add(this.labelControl22);
            this.pnlObservacionFoto.Location = new System.Drawing.Point(284, 484);
            this.pnlObservacionFoto.Margin = new System.Windows.Forms.Padding(4);
            this.pnlObservacionFoto.Name = "pnlObservacionFoto";
            this.pnlObservacionFoto.Size = new System.Drawing.Size(818, 75);
            this.pnlObservacionFoto.TabIndex = 294;
            this.pnlObservacionFoto.Visible = false;
            // 
            // mmObservacionFoto
            // 
            this.mmObservacionFoto.Location = new System.Drawing.Point(6, 25);
            this.mmObservacionFoto.Margin = new System.Windows.Forms.Padding(4);
            this.mmObservacionFoto.Name = "mmObservacionFoto";
            this.mmObservacionFoto.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.mmObservacionFoto.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.mmObservacionFoto.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.mmObservacionFoto.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.mmObservacionFoto.Size = new System.Drawing.Size(805, 45);
            this.mmObservacionFoto.TabIndex = 287;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.ForeColor = System.Drawing.Color.Red;
            this.label25.Location = new System.Drawing.Point(113, 6);
            this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(15, 16);
            this.label25.TabIndex = 289;
            this.label25.Text = "*";
            // 
            // labelControl22
            // 
            this.labelControl22.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl22.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl22.Appearance.Options.UseFont = true;
            this.labelControl22.Appearance.Options.UseForeColor = true;
            this.labelControl22.Location = new System.Drawing.Point(6, 3);
            this.labelControl22.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(101, 20);
            this.labelControl22.TabIndex = 288;
            this.labelControl22.Text = "Observaciones";
            // 
            // navigationPage2
            // 
            this.navigationPage2.Caption = "navigationPage2";
            this.navigationPage2.Controls.Add(this.labelControl72);
            this.navigationPage2.Controls.Add(this.labelControl73);
            this.navigationPage2.Controls.Add(this.labelControl71);
            this.navigationPage2.Controls.Add(this.lblDepartamento);
            this.navigationPage2.Controls.Add(this.labelControl46);
            this.navigationPage2.Controls.Add(this.lblTipoDocumento);
            this.navigationPage2.Controls.Add(this.labelControl43);
            this.navigationPage2.Controls.Add(this.labelControl70);
            this.navigationPage2.Controls.Add(this.labelControl68);
            this.navigationPage2.Controls.Add(this.pictureEdit1);
            this.navigationPage2.Controls.Add(this.labelControl64);
            this.navigationPage2.Controls.Add(this.pictureEdit2);
            this.navigationPage2.Controls.Add(this.labelControl1);
            this.navigationPage2.Controls.Add(this.labelControl40);
            this.navigationPage2.Controls.Add(this.picFotoFinal);
            this.navigationPage2.Controls.Add(this.labelControl67);
            this.navigationPage2.Controls.Add(this.reviewSignature);
            this.navigationPage2.Controls.Add(this.labelControl41);
            this.navigationPage2.Controls.Add(this.labelControl63);
            this.navigationPage2.Controls.Add(this.labelControl56);
            this.navigationPage2.Controls.Add(this.picDedo2Final);
            this.navigationPage2.Controls.Add(this.lblNivelEducativo);
            this.navigationPage2.Controls.Add(this.labelControl135);
            this.navigationPage2.Controls.Add(this.lblProfesionOficio);
            this.navigationPage2.Controls.Add(this.labelControl100);
            this.navigationPage2.Controls.Add(this.lblPaisNacimiento);
            this.navigationPage2.Controls.Add(this.labelControl132);
            this.navigationPage2.Controls.Add(this.lblDireccionResidencia);
            this.navigationPage2.Controls.Add(this.lblMovil);
            this.navigationPage2.Controls.Add(this.labelControl110);
            this.navigationPage2.Controls.Add(this.labelControl108);
            this.navigationPage2.Controls.Add(this.lblTelefono);
            this.navigationPage2.Controls.Add(this.lblColoniaCasa);
            this.navigationPage2.Controls.Add(this.labelControl104);
            this.navigationPage2.Controls.Add(this.labelControl103);
            this.navigationPage2.Controls.Add(this.lblCorreoElectronico);
            this.navigationPage2.Controls.Add(this.labelControl101);
            this.navigationPage2.Controls.Add(this.lblNacionalidad);
            this.navigationPage2.Controls.Add(this.lblNacimiento);
            this.navigationPage2.Controls.Add(this.lblSexo);
            this.navigationPage2.Controls.Add(this.lblIdentificacion);
            this.navigationPage2.Controls.Add(this.lblEstadoCivil);
            this.navigationPage2.Controls.Add(this.lblMunicipio);
            this.navigationPage2.Controls.Add(this.lblApellidos);
            this.navigationPage2.Controls.Add(this.lblNombres);
            this.navigationPage2.Controls.Add(this.labelControl49);
            this.navigationPage2.Controls.Add(this.labelControl50);
            this.navigationPage2.Controls.Add(this.labelControl51);
            this.navigationPage2.Controls.Add(this.labelControl52);
            this.navigationPage2.Controls.Add(this.labelControl44);
            this.navigationPage2.Controls.Add(this.labelControl45);
            this.navigationPage2.Controls.Add(this.labelControl47);
            this.navigationPage2.Controls.Add(this.labelControl48);
            this.navigationPage2.Controls.Add(this.picDedo6Final);
            this.navigationPage2.Controls.Add(this.picDedo5Final);
            this.navigationPage2.Controls.Add(this.picDedo7Final);
            this.navigationPage2.Controls.Add(this.picDedo8Final);
            this.navigationPage2.Controls.Add(this.picDedo10Final);
            this.navigationPage2.Controls.Add(this.picDedo9Final);
            this.navigationPage2.Controls.Add(this.picDedo4Final);
            this.navigationPage2.Controls.Add(this.picDedo3Final);
            this.navigationPage2.Controls.Add(this.picDedo1Final);
            this.navigationPage2.Margin = new System.Windows.Forms.Padding(4);
            this.navigationPage2.Name = "navigationPage2";
            this.navigationPage2.Size = new System.Drawing.Size(1359, 591);
            // 
            // labelControl72
            // 
            this.labelControl72.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl72.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl72.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.labelControl72.LineLocation = DevExpress.XtraEditors.LineLocation.Top;
            this.labelControl72.LineVisible = true;
            this.labelControl72.Location = new System.Drawing.Point(0, 552);
            this.labelControl72.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.labelControl72.Name = "labelControl72";
            this.labelControl72.Size = new System.Drawing.Size(1322, 23);
            this.labelControl72.TabIndex = 563;
            // 
            // labelControl73
            // 
            this.labelControl73.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl73.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.labelControl73.LineLocation = DevExpress.XtraEditors.LineLocation.Center;
            this.labelControl73.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Vertical;
            this.labelControl73.LineVisible = true;
            this.labelControl73.Location = new System.Drawing.Point(959, 396);
            this.labelControl73.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.labelControl73.Name = "labelControl73";
            this.labelControl73.Size = new System.Drawing.Size(10, 160);
            this.labelControl73.TabIndex = 564;
            // 
            // labelControl71
            // 
            this.labelControl71.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl71.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.labelControl71.LineLocation = DevExpress.XtraEditors.LineLocation.Center;
            this.labelControl71.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Vertical;
            this.labelControl71.LineVisible = true;
            this.labelControl71.Location = new System.Drawing.Point(477, 396);
            this.labelControl71.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.labelControl71.Name = "labelControl71";
            this.labelControl71.Size = new System.Drawing.Size(10, 165);
            this.labelControl71.TabIndex = 562;
            // 
            // lblDepartamento
            // 
            this.lblDepartamento.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDepartamento.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDepartamento.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lblDepartamento.Appearance.Options.UseFont = true;
            this.lblDepartamento.Appearance.Options.UseForeColor = true;
            this.lblDepartamento.Location = new System.Drawing.Point(878, 48);
            this.lblDepartamento.Margin = new System.Windows.Forms.Padding(4);
            this.lblDepartamento.Name = "lblDepartamento";
            this.lblDepartamento.Size = new System.Drawing.Size(131, 20);
            this.lblDepartamento.TabIndex = 503;
            this.lblDepartamento.Text = "[lblDepartamento]";
            // 
            // labelControl46
            // 
            this.labelControl46.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl46.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.labelControl46.Appearance.Options.UseFont = true;
            this.labelControl46.Appearance.Options.UseForeColor = true;
            this.labelControl46.Location = new System.Drawing.Point(762, 48);
            this.labelControl46.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl46.Name = "labelControl46";
            this.labelControl46.Size = new System.Drawing.Size(103, 20);
            this.labelControl46.TabIndex = 491;
            this.labelControl46.Text = "Departamento:";
            // 
            // lblTipoDocumento
            // 
            this.lblTipoDocumento.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTipoDocumento.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipoDocumento.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lblTipoDocumento.Appearance.Options.UseFont = true;
            this.lblTipoDocumento.Appearance.Options.UseForeColor = true;
            this.lblTipoDocumento.Location = new System.Drawing.Point(420, 48);
            this.lblTipoDocumento.Margin = new System.Windows.Forms.Padding(4);
            this.lblTipoDocumento.Name = "lblTipoDocumento";
            this.lblTipoDocumento.Size = new System.Drawing.Size(142, 20);
            this.lblTipoDocumento.TabIndex = 499;
            this.lblTipoDocumento.Text = "[lblTipoDocumento]";
            // 
            // labelControl43
            // 
            this.labelControl43.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl43.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl43.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.labelControl43.Appearance.Options.UseFont = true;
            this.labelControl43.Appearance.Options.UseForeColor = true;
            this.labelControl43.Location = new System.Drawing.Point(268, 48);
            this.labelControl43.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl43.Name = "labelControl43";
            this.labelControl43.Size = new System.Drawing.Size(139, 20);
            this.labelControl43.TabIndex = 494;
            this.labelControl43.Text = "Tipo de Documento:";
            // 
            // labelControl70
            // 
            this.labelControl70.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl70.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl70.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.labelControl70.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.labelControl70.LineVisible = true;
            this.labelControl70.Location = new System.Drawing.Point(7, 373);
            this.labelControl70.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.labelControl70.Name = "labelControl70";
            this.labelControl70.Size = new System.Drawing.Size(1314, 23);
            this.labelControl70.TabIndex = 561;
            // 
            // labelControl68
            // 
            this.labelControl68.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl68.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl68.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.labelControl68.LineLocation = DevExpress.XtraEditors.LineLocation.Top;
            this.labelControl68.LineVisible = true;
            this.labelControl68.Location = new System.Drawing.Point(8, 425);
            this.labelControl68.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.labelControl68.Name = "labelControl68";
            this.labelControl68.Size = new System.Drawing.Size(1314, 23);
            this.labelControl68.TabIndex = 558;
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit1.EditValue = ((object)(resources.GetObject("pictureEdit1.EditValue")));
            this.pictureEdit1.Location = new System.Drawing.Point(154, 396);
            this.pictureEdit1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit1.Properties.PictureAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.pictureEdit1.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit1.Properties.ShowMenu = false;
            this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.pictureEdit1.Size = new System.Drawing.Size(30, 30);
            this.pictureEdit1.TabIndex = 560;
            // 
            // labelControl64
            // 
            this.labelControl64.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl64.Appearance.ForeColor = System.Drawing.Color.DimGray;
            this.labelControl64.Appearance.Options.UseFont = true;
            this.labelControl64.Appearance.Options.UseForeColor = true;
            this.labelControl64.Appearance.Options.UseTextOptions = true;
            this.labelControl64.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl64.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl64.LineColor = System.Drawing.Color.Gainsboro;
            this.labelControl64.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.labelControl64.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.labelControl64.Location = new System.Drawing.Point(12, 387);
            this.labelControl64.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.labelControl64.Name = "labelControl64";
            this.labelControl64.Size = new System.Drawing.Size(459, 52);
            this.labelControl64.TabIndex = 552;
            this.labelControl64.Text = "Mano Izquierda";
            // 
            // pictureEdit2
            // 
            this.pictureEdit2.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit2.EditValue = ((object)(resources.GetObject("pictureEdit2.EditValue")));
            this.pictureEdit2.Location = new System.Drawing.Point(777, 397);
            this.pictureEdit2.Margin = new System.Windows.Forms.Padding(4);
            this.pictureEdit2.Name = "pictureEdit2";
            this.pictureEdit2.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit2.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit2.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit2.Properties.PictureAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.pictureEdit2.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit2.Properties.ShowMenu = false;
            this.pictureEdit2.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.pictureEdit2.Size = new System.Drawing.Size(30, 30);
            this.pictureEdit2.TabIndex = 559;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.DimGray;
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Appearance.Options.UseForeColor = true;
            this.labelControl1.Appearance.Options.UseTextOptions = true;
            this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.LineColor = System.Drawing.Color.Gainsboro;
            this.labelControl1.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.labelControl1.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.labelControl1.Location = new System.Drawing.Point(972, 390);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(350, 52);
            this.labelControl1.TabIndex = 556;
            this.labelControl1.Text = "Firma";
            // 
            // labelControl40
            // 
            this.labelControl40.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl40.Appearance.ForeColor = System.Drawing.Color.DimGray;
            this.labelControl40.Appearance.Options.UseFont = true;
            this.labelControl40.Appearance.Options.UseForeColor = true;
            this.labelControl40.Appearance.Options.UseTextOptions = true;
            this.labelControl40.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl40.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl40.LineColor = System.Drawing.Color.Gainsboro;
            this.labelControl40.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.labelControl40.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.labelControl40.Location = new System.Drawing.Point(495, 388);
            this.labelControl40.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.labelControl40.Name = "labelControl40";
            this.labelControl40.Size = new System.Drawing.Size(463, 52);
            this.labelControl40.TabIndex = 553;
            this.labelControl40.Text = "Mano Derecha";
            // 
            // picFotoFinal
            // 
            this.picFotoFinal.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picFotoFinal.Cursor = System.Windows.Forms.Cursors.Default;
            this.picFotoFinal.Location = new System.Drawing.Point(24, 50);
            this.picFotoFinal.Margin = new System.Windows.Forms.Padding(4);
            this.picFotoFinal.Name = "picFotoFinal";
            this.picFotoFinal.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picFotoFinal.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.picFotoFinal.Properties.Appearance.Options.UseBackColor = true;
            this.picFotoFinal.Properties.Appearance.Options.UseBorderColor = true;
            this.picFotoFinal.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.picFotoFinal.Properties.NullText = " ";
            this.picFotoFinal.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picFotoFinal.Properties.ShowMenu = false;
            this.picFotoFinal.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.picFotoFinal.Size = new System.Drawing.Size(168, 205);
            this.picFotoFinal.TabIndex = 488;
            // 
            // labelControl67
            // 
            this.labelControl67.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl67.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl67.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.labelControl67.LineLocation = DevExpress.XtraEditors.LineLocation.Top;
            this.labelControl67.LineVisible = true;
            this.labelControl67.Location = new System.Drawing.Point(24, 31);
            this.labelControl67.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.labelControl67.Name = "labelControl67";
            this.labelControl67.Size = new System.Drawing.Size(1284, 23);
            this.labelControl67.TabIndex = 557;
            // 
            // reviewSignature
            // 
            this.reviewSignature.Cursor = System.Windows.Forms.Cursors.Default;
            this.reviewSignature.Location = new System.Drawing.Point(972, 450);
            this.reviewSignature.Margin = new System.Windows.Forms.Padding(4);
            this.reviewSignature.Name = "reviewSignature";
            this.reviewSignature.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.reviewSignature.Properties.Appearance.Options.UseBorderColor = true;
            this.reviewSignature.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.reviewSignature.Properties.NullText = "No signature";
            this.reviewSignature.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.reviewSignature.Properties.ShowMenu = false;
            this.reviewSignature.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.reviewSignature.Size = new System.Drawing.Size(350, 88);
            this.reviewSignature.TabIndex = 555;
            // 
            // labelControl41
            // 
            this.labelControl41.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl41.Appearance.ForeColor = System.Drawing.Color.DimGray;
            this.labelControl41.Appearance.Options.UseFont = true;
            this.labelControl41.Appearance.Options.UseForeColor = true;
            this.labelControl41.Appearance.Options.UseTextOptions = true;
            this.labelControl41.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl41.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl41.LineColor = System.Drawing.Color.WhiteSmoke;
            this.labelControl41.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.labelControl41.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.labelControl41.Location = new System.Drawing.Point(24, 3);
            this.labelControl41.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.labelControl41.Name = "labelControl41";
            this.labelControl41.Size = new System.Drawing.Size(168, 34);
            this.labelControl41.TabIndex = 554;
            this.labelControl41.Text = "Fotografía";
            // 
            // labelControl63
            // 
            this.labelControl63.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl63.Appearance.ForeColor = System.Drawing.Color.DimGray;
            this.labelControl63.Appearance.Options.UseFont = true;
            this.labelControl63.Appearance.Options.UseForeColor = true;
            this.labelControl63.Appearance.Options.UseTextOptions = true;
            this.labelControl63.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl63.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl63.LineColor = System.Drawing.Color.WhiteSmoke;
            this.labelControl63.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.labelControl63.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.labelControl63.Location = new System.Drawing.Point(754, -1);
            this.labelControl63.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.labelControl63.Name = "labelControl63";
            this.labelControl63.Size = new System.Drawing.Size(367, 40);
            this.labelControl63.TabIndex = 551;
            this.labelControl63.Text = "Información Biográfica";
            // 
            // labelControl56
            // 
            this.labelControl56.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl56.Appearance.ForeColor = System.Drawing.Color.DimGray;
            this.labelControl56.Appearance.Options.UseFont = true;
            this.labelControl56.Appearance.Options.UseForeColor = true;
            this.labelControl56.Appearance.Options.UseTextOptions = true;
            this.labelControl56.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl56.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl56.LineColor = System.Drawing.Color.WhiteSmoke;
            this.labelControl56.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.labelControl56.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.labelControl56.Location = new System.Drawing.Point(272, 3);
            this.labelControl56.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.labelControl56.Name = "labelControl56";
            this.labelControl56.Size = new System.Drawing.Size(378, 36);
            this.labelControl56.TabIndex = 550;
            this.labelControl56.Text = "Información Personal";
            // 
            // picDedo2Final
            // 
            this.picDedo2Final.Cursor = System.Windows.Forms.Cursors.Default;
            this.picDedo2Final.Location = new System.Drawing.Point(588, 451);
            this.picDedo2Final.Margin = new System.Windows.Forms.Padding(4);
            this.picDedo2Final.Name = "picDedo2Final";
            this.picDedo2Final.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.picDedo2Final.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.picDedo2Final.Properties.Appearance.Font = new System.Drawing.Font("Arial Black", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.picDedo2Final.Properties.Appearance.ForeColor = System.Drawing.Color.DarkGray;
            this.picDedo2Final.Properties.Appearance.Options.UseBackColor = true;
            this.picDedo2Final.Properties.Appearance.Options.UseBorderColor = true;
            this.picDedo2Final.Properties.Appearance.Options.UseFont = true;
            this.picDedo2Final.Properties.Appearance.Options.UseForeColor = true;
            this.picDedo2Final.Properties.Appearance.Options.UseTextOptions = true;
            this.picDedo2Final.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.picDedo2Final.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.picDedo2Final.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.picDedo2Final.Properties.NullText = "2";
            this.picDedo2Final.Properties.ShowMenu = false;
            this.picDedo2Final.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.picDedo2Final.Size = new System.Drawing.Size(88, 88);
            this.picDedo2Final.TabIndex = 549;
            // 
            // lblNivelEducativo
            // 
            this.lblNivelEducativo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblNivelEducativo.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNivelEducativo.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lblNivelEducativo.Appearance.Options.UseFont = true;
            this.lblNivelEducativo.Appearance.Options.UseForeColor = true;
            this.lblNivelEducativo.Location = new System.Drawing.Point(878, 305);
            this.lblNivelEducativo.Margin = new System.Windows.Forms.Padding(4);
            this.lblNivelEducativo.Name = "lblNivelEducativo";
            this.lblNivelEducativo.Size = new System.Drawing.Size(133, 20);
            this.lblNivelEducativo.TabIndex = 548;
            this.lblNivelEducativo.Text = "[lblNivelEducativo]";
            // 
            // labelControl135
            // 
            this.labelControl135.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl135.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.labelControl135.Appearance.Options.UseFont = true;
            this.labelControl135.Appearance.Options.UseForeColor = true;
            this.labelControl135.Location = new System.Drawing.Point(754, 306);
            this.labelControl135.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl135.Name = "labelControl135";
            this.labelControl135.Size = new System.Drawing.Size(111, 20);
            this.labelControl135.TabIndex = 547;
            this.labelControl135.Text = "Nivel Educativo:";
            // 
            // lblProfesionOficio
            // 
            this.lblProfesionOficio.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblProfesionOficio.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProfesionOficio.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lblProfesionOficio.Appearance.Options.UseFont = true;
            this.lblProfesionOficio.Appearance.Options.UseForeColor = true;
            this.lblProfesionOficio.Location = new System.Drawing.Point(878, 274);
            this.lblProfesionOficio.Margin = new System.Windows.Forms.Padding(4);
            this.lblProfesionOficio.Name = "lblProfesionOficio";
            this.lblProfesionOficio.Size = new System.Drawing.Size(137, 20);
            this.lblProfesionOficio.TabIndex = 544;
            this.lblProfesionOficio.Text = "[lblProfesionOficio]";
            // 
            // labelControl100
            // 
            this.labelControl100.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl100.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.labelControl100.Appearance.Options.UseFont = true;
            this.labelControl100.Appearance.Options.UseForeColor = true;
            this.labelControl100.Location = new System.Drawing.Point(739, 274);
            this.labelControl100.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl100.Name = "labelControl100";
            this.labelControl100.Size = new System.Drawing.Size(126, 20);
            this.labelControl100.TabIndex = 543;
            this.labelControl100.Text = "Profesión u Oficio:";
            // 
            // lblPaisNacimiento
            // 
            this.lblPaisNacimiento.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPaisNacimiento.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPaisNacimiento.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lblPaisNacimiento.Appearance.Options.UseFont = true;
            this.lblPaisNacimiento.Appearance.Options.UseForeColor = true;
            this.lblPaisNacimiento.Location = new System.Drawing.Point(420, 200);
            this.lblPaisNacimiento.Margin = new System.Windows.Forms.Padding(4);
            this.lblPaisNacimiento.Name = "lblPaisNacimiento";
            this.lblPaisNacimiento.Size = new System.Drawing.Size(138, 20);
            this.lblPaisNacimiento.TabIndex = 534;
            this.lblPaisNacimiento.Text = "[lblPaisNacimiento]";
            // 
            // labelControl132
            // 
            this.labelControl132.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl132.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.labelControl132.Appearance.Options.UseFont = true;
            this.labelControl132.Appearance.Options.UseForeColor = true;
            this.labelControl132.Location = new System.Drawing.Point(272, 200);
            this.labelControl132.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl132.Name = "labelControl132";
            this.labelControl132.Size = new System.Drawing.Size(135, 20);
            this.labelControl132.TabIndex = 533;
            this.labelControl132.Text = "País de Nacimiento:";
            // 
            // lblDireccionResidencia
            // 
            this.lblDireccionResidencia.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDireccionResidencia.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDireccionResidencia.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lblDireccionResidencia.Appearance.Options.UseFont = true;
            this.lblDireccionResidencia.Appearance.Options.UseForeColor = true;
            this.lblDireccionResidencia.Appearance.Options.UseTextOptions = true;
            this.lblDireccionResidencia.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.lblDireccionResidencia.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lblDireccionResidencia.AutoEllipsis = true;
            this.lblDireccionResidencia.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblDireccionResidencia.Location = new System.Drawing.Point(878, 141);
            this.lblDireccionResidencia.Margin = new System.Windows.Forms.Padding(4);
            this.lblDireccionResidencia.Name = "lblDireccionResidencia";
            this.lblDireccionResidencia.Padding = new System.Windows.Forms.Padding(2);
            this.lblDireccionResidencia.Size = new System.Drawing.Size(264, 40);
            this.lblDireccionResidencia.TabIndex = 529;
            this.lblDireccionResidencia.Text = "[lblDireccionResidencia]";
            // 
            // lblMovil
            // 
            this.lblMovil.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMovil.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMovil.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lblMovil.Appearance.Options.UseFont = true;
            this.lblMovil.Appearance.Options.UseForeColor = true;
            this.lblMovil.Location = new System.Drawing.Point(878, 211);
            this.lblMovil.Margin = new System.Windows.Forms.Padding(4);
            this.lblMovil.Name = "lblMovil";
            this.lblMovil.Size = new System.Drawing.Size(68, 20);
            this.lblMovil.TabIndex = 528;
            this.lblMovil.Text = "[lblMovil]";
            // 
            // labelControl110
            // 
            this.labelControl110.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl110.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.labelControl110.Appearance.Options.UseFont = true;
            this.labelControl110.Appearance.Options.UseForeColor = true;
            this.labelControl110.Location = new System.Drawing.Point(822, 211);
            this.labelControl110.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl110.Name = "labelControl110";
            this.labelControl110.Size = new System.Drawing.Size(43, 20);
            this.labelControl110.TabIndex = 525;
            this.labelControl110.Text = "Móvil:";
            // 
            // labelControl108
            // 
            this.labelControl108.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl108.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.labelControl108.Appearance.Options.UseFont = true;
            this.labelControl108.Appearance.Options.UseForeColor = true;
            this.labelControl108.Location = new System.Drawing.Point(699, 141);
            this.labelControl108.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl108.Name = "labelControl108";
            this.labelControl108.Size = new System.Drawing.Size(166, 20);
            this.labelControl108.TabIndex = 523;
            this.labelControl108.Text = "Direccion de Residencia:";
            // 
            // lblTelefono
            // 
            this.lblTelefono.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTelefono.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTelefono.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lblTelefono.Appearance.Options.UseFont = true;
            this.lblTelefono.Appearance.Options.UseForeColor = true;
            this.lblTelefono.Location = new System.Drawing.Point(878, 180);
            this.lblTelefono.Margin = new System.Windows.Forms.Padding(4);
            this.lblTelefono.Name = "lblTelefono";
            this.lblTelefono.Size = new System.Drawing.Size(91, 20);
            this.lblTelefono.TabIndex = 520;
            this.lblTelefono.Text = "[lblTelefono]";
            // 
            // lblColoniaCasa
            // 
            this.lblColoniaCasa.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblColoniaCasa.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblColoniaCasa.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lblColoniaCasa.Appearance.Options.UseFont = true;
            this.lblColoniaCasa.Appearance.Options.UseForeColor = true;
            this.lblColoniaCasa.Location = new System.Drawing.Point(878, 110);
            this.lblColoniaCasa.Margin = new System.Windows.Forms.Padding(4);
            this.lblColoniaCasa.Name = "lblColoniaCasa";
            this.lblColoniaCasa.Size = new System.Drawing.Size(113, 20);
            this.lblColoniaCasa.TabIndex = 519;
            this.lblColoniaCasa.Text = "[lblColoniaCasa]";
            // 
            // labelControl104
            // 
            this.labelControl104.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl104.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.labelControl104.Appearance.Options.UseFont = true;
            this.labelControl104.Appearance.Options.UseForeColor = true;
            this.labelControl104.Location = new System.Drawing.Point(801, 180);
            this.labelControl104.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl104.Name = "labelControl104";
            this.labelControl104.Size = new System.Drawing.Size(64, 20);
            this.labelControl104.TabIndex = 515;
            this.labelControl104.Text = "Teléfono:";
            // 
            // labelControl103
            // 
            this.labelControl103.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl103.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.labelControl103.Appearance.Options.UseFont = true;
            this.labelControl103.Appearance.Options.UseForeColor = true;
            this.labelControl103.Location = new System.Drawing.Point(809, 110);
            this.labelControl103.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl103.Name = "labelControl103";
            this.labelControl103.Size = new System.Drawing.Size(56, 20);
            this.labelControl103.TabIndex = 514;
            this.labelControl103.Text = "Colonia:";
            // 
            // lblCorreoElectronico
            // 
            this.lblCorreoElectronico.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCorreoElectronico.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCorreoElectronico.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lblCorreoElectronico.Appearance.Options.UseFont = true;
            this.lblCorreoElectronico.Appearance.Options.UseForeColor = true;
            this.lblCorreoElectronico.Location = new System.Drawing.Point(878, 242);
            this.lblCorreoElectronico.Margin = new System.Windows.Forms.Padding(4);
            this.lblCorreoElectronico.Name = "lblCorreoElectronico";
            this.lblCorreoElectronico.Size = new System.Drawing.Size(153, 20);
            this.lblCorreoElectronico.TabIndex = 513;
            this.lblCorreoElectronico.Text = "[lblCorreoElectronico]";
            // 
            // labelControl101
            // 
            this.labelControl101.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl101.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.labelControl101.Appearance.Options.UseFont = true;
            this.labelControl101.Appearance.Options.UseForeColor = true;
            this.labelControl101.Location = new System.Drawing.Point(734, 242);
            this.labelControl101.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl101.Name = "labelControl101";
            this.labelControl101.Size = new System.Drawing.Size(131, 20);
            this.labelControl101.TabIndex = 512;
            this.labelControl101.Text = "Correo Electrónico:";
            // 
            // lblNacionalidad
            // 
            this.lblNacionalidad.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblNacionalidad.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNacionalidad.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lblNacionalidad.Appearance.Options.UseFont = true;
            this.lblNacionalidad.Appearance.Options.UseForeColor = true;
            this.lblNacionalidad.Location = new System.Drawing.Point(420, 231);
            this.lblNacionalidad.Margin = new System.Windows.Forms.Padding(4);
            this.lblNacionalidad.Name = "lblNacionalidad";
            this.lblNacionalidad.Size = new System.Drawing.Size(120, 20);
            this.lblNacionalidad.TabIndex = 509;
            this.lblNacionalidad.Text = "[lblNacionalidad]";
            // 
            // lblNacimiento
            // 
            this.lblNacimiento.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblNacimiento.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNacimiento.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lblNacimiento.Appearance.Options.UseFont = true;
            this.lblNacimiento.Appearance.Options.UseForeColor = true;
            this.lblNacimiento.Location = new System.Drawing.Point(420, 262);
            this.lblNacimiento.Margin = new System.Windows.Forms.Padding(4);
            this.lblNacimiento.Name = "lblNacimiento";
            this.lblNacimiento.Size = new System.Drawing.Size(110, 20);
            this.lblNacimiento.TabIndex = 508;
            this.lblNacimiento.Text = "[lblNacimiento]";
            // 
            // lblSexo
            // 
            this.lblSexo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSexo.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSexo.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lblSexo.Appearance.Options.UseFont = true;
            this.lblSexo.Appearance.Options.UseForeColor = true;
            this.lblSexo.Location = new System.Drawing.Point(420, 172);
            this.lblSexo.Margin = new System.Windows.Forms.Padding(4);
            this.lblSexo.Name = "lblSexo";
            this.lblSexo.Size = new System.Drawing.Size(62, 20);
            this.lblSexo.TabIndex = 507;
            this.lblSexo.Text = "[lblSexo]";
            // 
            // lblIdentificacion
            // 
            this.lblIdentificacion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblIdentificacion.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIdentificacion.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lblIdentificacion.Appearance.Options.UseFont = true;
            this.lblIdentificacion.Appearance.Options.UseForeColor = true;
            this.lblIdentificacion.Location = new System.Drawing.Point(420, 79);
            this.lblIdentificacion.Margin = new System.Windows.Forms.Padding(4);
            this.lblIdentificacion.Name = "lblIdentificacion";
            this.lblIdentificacion.Size = new System.Drawing.Size(124, 20);
            this.lblIdentificacion.TabIndex = 506;
            this.lblIdentificacion.Text = "[lblIdentificacion]";
            // 
            // lblEstadoCivil
            // 
            this.lblEstadoCivil.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEstadoCivil.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstadoCivil.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lblEstadoCivil.Appearance.Options.UseFont = true;
            this.lblEstadoCivil.Appearance.Options.UseForeColor = true;
            this.lblEstadoCivil.Location = new System.Drawing.Point(420, 295);
            this.lblEstadoCivil.Margin = new System.Windows.Forms.Padding(4);
            this.lblEstadoCivil.Name = "lblEstadoCivil";
            this.lblEstadoCivil.Size = new System.Drawing.Size(105, 20);
            this.lblEstadoCivil.TabIndex = 505;
            this.lblEstadoCivil.Text = "[lblEstadoCivil]";
            // 
            // lblMunicipio
            // 
            this.lblMunicipio.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMunicipio.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMunicipio.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lblMunicipio.Appearance.Options.UseFont = true;
            this.lblMunicipio.Appearance.Options.UseForeColor = true;
            this.lblMunicipio.Location = new System.Drawing.Point(878, 79);
            this.lblMunicipio.Margin = new System.Windows.Forms.Padding(4);
            this.lblMunicipio.Name = "lblMunicipio";
            this.lblMunicipio.Size = new System.Drawing.Size(98, 20);
            this.lblMunicipio.TabIndex = 504;
            this.lblMunicipio.Text = "[lblMunicipio]";
            // 
            // lblApellidos
            // 
            this.lblApellidos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblApellidos.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblApellidos.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lblApellidos.Appearance.Options.UseFont = true;
            this.lblApellidos.Appearance.Options.UseForeColor = true;
            this.lblApellidos.Location = new System.Drawing.Point(420, 141);
            this.lblApellidos.Margin = new System.Windows.Forms.Padding(4);
            this.lblApellidos.Name = "lblApellidos";
            this.lblApellidos.Size = new System.Drawing.Size(94, 20);
            this.lblApellidos.TabIndex = 502;
            this.lblApellidos.Text = "[lblApellidos]";
            // 
            // lblNombres
            // 
            this.lblNombres.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblNombres.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombres.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lblNombres.Appearance.Options.UseFont = true;
            this.lblNombres.Appearance.Options.UseForeColor = true;
            this.lblNombres.Location = new System.Drawing.Point(420, 110);
            this.lblNombres.Margin = new System.Windows.Forms.Padding(4);
            this.lblNombres.Name = "lblNombres";
            this.lblNombres.Size = new System.Drawing.Size(77, 20);
            this.lblNombres.TabIndex = 500;
            this.lblNombres.Text = "[Nombres]";
            // 
            // labelControl49
            // 
            this.labelControl49.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl49.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.labelControl49.Appearance.Options.UseFont = true;
            this.labelControl49.Appearance.Options.UseForeColor = true;
            this.labelControl49.Appearance.Options.UseTextOptions = true;
            this.labelControl49.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl49.Location = new System.Drawing.Point(259, 262);
            this.labelControl49.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl49.Name = "labelControl49";
            this.labelControl49.Size = new System.Drawing.Size(148, 20);
            this.labelControl49.TabIndex = 498;
            this.labelControl49.Text = "Fecha de Nacimiento:";
            // 
            // labelControl50
            // 
            this.labelControl50.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl50.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.labelControl50.Appearance.Options.UseFont = true;
            this.labelControl50.Appearance.Options.UseForeColor = true;
            this.labelControl50.Location = new System.Drawing.Point(312, 231);
            this.labelControl50.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl50.Name = "labelControl50";
            this.labelControl50.Size = new System.Drawing.Size(95, 20);
            this.labelControl50.TabIndex = 497;
            this.labelControl50.Text = "Nacionalidad:";
            // 
            // labelControl51
            // 
            this.labelControl51.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl51.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.labelControl51.Appearance.Options.UseFont = true;
            this.labelControl51.Appearance.Options.UseForeColor = true;
            this.labelControl51.Location = new System.Drawing.Point(370, 169);
            this.labelControl51.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl51.Name = "labelControl51";
            this.labelControl51.Size = new System.Drawing.Size(37, 20);
            this.labelControl51.TabIndex = 496;
            this.labelControl51.Text = "Sexo:";
            // 
            // labelControl52
            // 
            this.labelControl52.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl52.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl52.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.labelControl52.Appearance.Options.UseFont = true;
            this.labelControl52.Appearance.Options.UseForeColor = true;
            this.labelControl52.Location = new System.Drawing.Point(311, 79);
            this.labelControl52.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl52.Name = "labelControl52";
            this.labelControl52.Size = new System.Drawing.Size(96, 20);
            this.labelControl52.TabIndex = 495;
            this.labelControl52.Text = "Identificación:";
            // 
            // labelControl44
            // 
            this.labelControl44.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl44.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.labelControl44.Appearance.Options.UseFont = true;
            this.labelControl44.Appearance.Options.UseForeColor = true;
            this.labelControl44.Location = new System.Drawing.Point(325, 294);
            this.labelControl44.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl44.Name = "labelControl44";
            this.labelControl44.Size = new System.Drawing.Size(82, 20);
            this.labelControl44.TabIndex = 493;
            this.labelControl44.Text = "Estado Civil:";
            // 
            // labelControl45
            // 
            this.labelControl45.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl45.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.labelControl45.Appearance.Options.UseFont = true;
            this.labelControl45.Appearance.Options.UseForeColor = true;
            this.labelControl45.Location = new System.Drawing.Point(792, 79);
            this.labelControl45.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl45.Name = "labelControl45";
            this.labelControl45.Size = new System.Drawing.Size(73, 20);
            this.labelControl45.TabIndex = 492;
            this.labelControl45.Text = "Municipio:";
            // 
            // labelControl47
            // 
            this.labelControl47.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl47.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl47.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.labelControl47.Appearance.Options.UseFont = true;
            this.labelControl47.Appearance.Options.UseForeColor = true;
            this.labelControl47.Location = new System.Drawing.Point(219, 110);
            this.labelControl47.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl47.Name = "labelControl47";
            this.labelControl47.Size = new System.Drawing.Size(188, 20);
            this.labelControl47.TabIndex = 489;
            this.labelControl47.Text = "Primer y Segundo Nombre:";
            // 
            // labelControl48
            // 
            this.labelControl48.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl48.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl48.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.labelControl48.Appearance.Options.UseFont = true;
            this.labelControl48.Appearance.Options.UseForeColor = true;
            this.labelControl48.Location = new System.Drawing.Point(219, 141);
            this.labelControl48.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl48.Name = "labelControl48";
            this.labelControl48.Size = new System.Drawing.Size(188, 20);
            this.labelControl48.TabIndex = 490;
            this.labelControl48.Text = "Primer y Segundo Apellido:";
            // 
            // picDedo6Final
            // 
            this.picDedo6Final.Cursor = System.Windows.Forms.Cursors.Default;
            this.picDedo6Final.Location = new System.Drawing.Point(383, 450);
            this.picDedo6Final.Margin = new System.Windows.Forms.Padding(4);
            this.picDedo6Final.Name = "picDedo6Final";
            this.picDedo6Final.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.picDedo6Final.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.picDedo6Final.Properties.Appearance.Font = new System.Drawing.Font("Arial Black", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.picDedo6Final.Properties.Appearance.ForeColor = System.Drawing.Color.DarkGray;
            this.picDedo6Final.Properties.Appearance.Options.UseBackColor = true;
            this.picDedo6Final.Properties.Appearance.Options.UseBorderColor = true;
            this.picDedo6Final.Properties.Appearance.Options.UseFont = true;
            this.picDedo6Final.Properties.Appearance.Options.UseForeColor = true;
            this.picDedo6Final.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.picDedo6Final.Properties.NullText = "6";
            this.picDedo6Final.Properties.ShowMenu = false;
            this.picDedo6Final.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.picDedo6Final.Size = new System.Drawing.Size(88, 88);
            this.picDedo6Final.TabIndex = 481;
            // 
            // picDedo5Final
            // 
            this.picDedo5Final.Cursor = System.Windows.Forms.Cursors.Default;
            this.picDedo5Final.Location = new System.Drawing.Point(870, 451);
            this.picDedo5Final.Margin = new System.Windows.Forms.Padding(4);
            this.picDedo5Final.Name = "picDedo5Final";
            this.picDedo5Final.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.picDedo5Final.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.picDedo5Final.Properties.Appearance.Font = new System.Drawing.Font("Arial Black", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.picDedo5Final.Properties.Appearance.ForeColor = System.Drawing.Color.DarkGray;
            this.picDedo5Final.Properties.Appearance.Options.UseBackColor = true;
            this.picDedo5Final.Properties.Appearance.Options.UseBorderColor = true;
            this.picDedo5Final.Properties.Appearance.Options.UseFont = true;
            this.picDedo5Final.Properties.Appearance.Options.UseForeColor = true;
            this.picDedo5Final.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.picDedo5Final.Properties.NullText = "5";
            this.picDedo5Final.Properties.ShowMenu = false;
            this.picDedo5Final.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.picDedo5Final.Size = new System.Drawing.Size(88, 88);
            this.picDedo5Final.TabIndex = 483;
            // 
            // picDedo7Final
            // 
            this.picDedo7Final.Cursor = System.Windows.Forms.Cursors.Default;
            this.picDedo7Final.Location = new System.Drawing.Point(289, 450);
            this.picDedo7Final.Margin = new System.Windows.Forms.Padding(4);
            this.picDedo7Final.Name = "picDedo7Final";
            this.picDedo7Final.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.picDedo7Final.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.picDedo7Final.Properties.Appearance.Font = new System.Drawing.Font("Arial Black", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.picDedo7Final.Properties.Appearance.ForeColor = System.Drawing.Color.DarkGray;
            this.picDedo7Final.Properties.Appearance.Options.UseBackColor = true;
            this.picDedo7Final.Properties.Appearance.Options.UseBorderColor = true;
            this.picDedo7Final.Properties.Appearance.Options.UseFont = true;
            this.picDedo7Final.Properties.Appearance.Options.UseForeColor = true;
            this.picDedo7Final.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.picDedo7Final.Properties.NullText = "7";
            this.picDedo7Final.Properties.ShowMenu = false;
            this.picDedo7Final.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.picDedo7Final.Size = new System.Drawing.Size(88, 88);
            this.picDedo7Final.TabIndex = 482;
            // 
            // picDedo8Final
            // 
            this.picDedo8Final.Cursor = System.Windows.Forms.Cursors.Default;
            this.picDedo8Final.Location = new System.Drawing.Point(195, 450);
            this.picDedo8Final.Margin = new System.Windows.Forms.Padding(4);
            this.picDedo8Final.Name = "picDedo8Final";
            this.picDedo8Final.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.picDedo8Final.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.picDedo8Final.Properties.Appearance.Font = new System.Drawing.Font("Arial Black", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.picDedo8Final.Properties.Appearance.ForeColor = System.Drawing.Color.DarkGray;
            this.picDedo8Final.Properties.Appearance.Options.UseBackColor = true;
            this.picDedo8Final.Properties.Appearance.Options.UseBorderColor = true;
            this.picDedo8Final.Properties.Appearance.Options.UseFont = true;
            this.picDedo8Final.Properties.Appearance.Options.UseForeColor = true;
            this.picDedo8Final.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.picDedo8Final.Properties.NullText = "8";
            this.picDedo8Final.Properties.ShowMenu = false;
            this.picDedo8Final.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.picDedo8Final.Size = new System.Drawing.Size(88, 88);
            this.picDedo8Final.TabIndex = 480;
            // 
            // picDedo10Final
            // 
            this.picDedo10Final.Cursor = System.Windows.Forms.Cursors.Default;
            this.picDedo10Final.Location = new System.Drawing.Point(8, 450);
            this.picDedo10Final.Margin = new System.Windows.Forms.Padding(4);
            this.picDedo10Final.Name = "picDedo10Final";
            this.picDedo10Final.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.picDedo10Final.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.picDedo10Final.Properties.Appearance.Font = new System.Drawing.Font("Arial Black", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.picDedo10Final.Properties.Appearance.ForeColor = System.Drawing.Color.DarkGray;
            this.picDedo10Final.Properties.Appearance.Options.UseBackColor = true;
            this.picDedo10Final.Properties.Appearance.Options.UseBorderColor = true;
            this.picDedo10Final.Properties.Appearance.Options.UseFont = true;
            this.picDedo10Final.Properties.Appearance.Options.UseForeColor = true;
            this.picDedo10Final.Properties.Appearance.Options.UseTextOptions = true;
            this.picDedo10Final.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.picDedo10Final.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.picDedo10Final.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.picDedo10Final.Properties.NullText = "10";
            this.picDedo10Final.Properties.ShowMenu = false;
            this.picDedo10Final.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.picDedo10Final.Size = new System.Drawing.Size(88, 88);
            this.picDedo10Final.TabIndex = 478;
            // 
            // picDedo9Final
            // 
            this.picDedo9Final.Cursor = System.Windows.Forms.Cursors.Default;
            this.picDedo9Final.Location = new System.Drawing.Point(101, 450);
            this.picDedo9Final.Margin = new System.Windows.Forms.Padding(4);
            this.picDedo9Final.Name = "picDedo9Final";
            this.picDedo9Final.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.picDedo9Final.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.picDedo9Final.Properties.Appearance.Font = new System.Drawing.Font("Arial Black", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.picDedo9Final.Properties.Appearance.ForeColor = System.Drawing.Color.DarkGray;
            this.picDedo9Final.Properties.Appearance.Options.UseBackColor = true;
            this.picDedo9Final.Properties.Appearance.Options.UseBorderColor = true;
            this.picDedo9Final.Properties.Appearance.Options.UseFont = true;
            this.picDedo9Final.Properties.Appearance.Options.UseForeColor = true;
            this.picDedo9Final.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.picDedo9Final.Properties.NullText = "9";
            this.picDedo9Final.Properties.ShowMenu = false;
            this.picDedo9Final.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.picDedo9Final.Size = new System.Drawing.Size(88, 88);
            this.picDedo9Final.TabIndex = 479;
            // 
            // picDedo4Final
            // 
            this.picDedo4Final.Cursor = System.Windows.Forms.Cursors.Default;
            this.picDedo4Final.Location = new System.Drawing.Point(776, 451);
            this.picDedo4Final.Margin = new System.Windows.Forms.Padding(4);
            this.picDedo4Final.Name = "picDedo4Final";
            this.picDedo4Final.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.picDedo4Final.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.picDedo4Final.Properties.Appearance.Font = new System.Drawing.Font("Arial Black", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.picDedo4Final.Properties.Appearance.ForeColor = System.Drawing.Color.DarkGray;
            this.picDedo4Final.Properties.Appearance.Options.UseBackColor = true;
            this.picDedo4Final.Properties.Appearance.Options.UseBorderColor = true;
            this.picDedo4Final.Properties.Appearance.Options.UseFont = true;
            this.picDedo4Final.Properties.Appearance.Options.UseForeColor = true;
            this.picDedo4Final.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.picDedo4Final.Properties.NullText = "4";
            this.picDedo4Final.Properties.ShowMenu = false;
            this.picDedo4Final.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.picDedo4Final.Size = new System.Drawing.Size(88, 88);
            this.picDedo4Final.TabIndex = 477;
            // 
            // picDedo3Final
            // 
            this.picDedo3Final.Cursor = System.Windows.Forms.Cursors.Default;
            this.picDedo3Final.Location = new System.Drawing.Point(682, 451);
            this.picDedo3Final.Margin = new System.Windows.Forms.Padding(4);
            this.picDedo3Final.Name = "picDedo3Final";
            this.picDedo3Final.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.picDedo3Final.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.picDedo3Final.Properties.Appearance.Font = new System.Drawing.Font("Arial Black", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.picDedo3Final.Properties.Appearance.ForeColor = System.Drawing.Color.DarkGray;
            this.picDedo3Final.Properties.Appearance.Options.UseBackColor = true;
            this.picDedo3Final.Properties.Appearance.Options.UseBorderColor = true;
            this.picDedo3Final.Properties.Appearance.Options.UseFont = true;
            this.picDedo3Final.Properties.Appearance.Options.UseForeColor = true;
            this.picDedo3Final.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.picDedo3Final.Properties.NullText = "3";
            this.picDedo3Final.Properties.ShowMenu = false;
            this.picDedo3Final.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.picDedo3Final.Size = new System.Drawing.Size(88, 88);
            this.picDedo3Final.TabIndex = 476;
            // 
            // picDedo1Final
            // 
            this.picDedo1Final.Cursor = System.Windows.Forms.Cursors.Default;
            this.picDedo1Final.Location = new System.Drawing.Point(495, 451);
            this.picDedo1Final.Margin = new System.Windows.Forms.Padding(4);
            this.picDedo1Final.Name = "picDedo1Final";
            this.picDedo1Final.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.picDedo1Final.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.picDedo1Final.Properties.Appearance.Font = new System.Drawing.Font("Arial Black", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.picDedo1Final.Properties.Appearance.ForeColor = System.Drawing.Color.DarkGray;
            this.picDedo1Final.Properties.Appearance.Options.UseBackColor = true;
            this.picDedo1Final.Properties.Appearance.Options.UseBorderColor = true;
            this.picDedo1Final.Properties.Appearance.Options.UseFont = true;
            this.picDedo1Final.Properties.Appearance.Options.UseForeColor = true;
            this.picDedo1Final.Properties.Appearance.Options.UseTextOptions = true;
            this.picDedo1Final.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.picDedo1Final.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.picDedo1Final.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.picDedo1Final.Properties.NullText = "1";
            this.picDedo1Final.Properties.ShowMenu = false;
            this.picDedo1Final.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.picDedo1Final.Size = new System.Drawing.Size(88, 88);
            this.picDedo1Final.TabIndex = 474;
            // 
            // navigationPage5
            // 
            this.navigationPage5.Caption = "navigationPage5";
            this.navigationPage5.Controls.Add(this.panelControl2);
            this.navigationPage5.Controls.Add(this.panelControl3);
            this.navigationPage5.Controls.Add(this.sbtnBorrarFirma);
            this.navigationPage5.Controls.Add(this.chknoPuedeFirmar);
            this.navigationPage5.Margin = new System.Windows.Forms.Padding(4);
            this.navigationPage5.Name = "navigationPage5";
            this.navigationPage5.Size = new System.Drawing.Size(1359, 591);
            // 
            // panelControl2
            // 
            this.panelControl2.Appearance.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.panelControl2.Appearance.Options.UseBackColor = true;
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.panelControl2.Controls.Add(this.sigPlusNET1);
            this.panelControl2.Location = new System.Drawing.Point(332, 124);
            this.panelControl2.LookAndFeel.SkinName = "Blue";
            this.panelControl2.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelControl2.Margin = new System.Windows.Forms.Padding(4);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(636, 169);
            this.panelControl2.TabIndex = 296;
            // 
            // sigPlusNET1
            // 
            this.sigPlusNET1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sigPlusNET1.BackColor = System.Drawing.Color.White;
            this.sigPlusNET1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.sigPlusNET1.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.sigPlusNET1.ForeColor = System.Drawing.Color.Black;
            this.sigPlusNET1.Location = new System.Drawing.Point(25, 19);
            this.sigPlusNET1.Margin = new System.Windows.Forms.Padding(4);
            this.sigPlusNET1.Name = "sigPlusNET1";
            this.sigPlusNET1.Size = new System.Drawing.Size(586, 131);
            this.sigPlusNET1.TabIndex = 15;
            this.sigPlusNET1.Text = "sigPlusNET1";
            // 
            // panelControl3
            // 
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.meObservacionFirma);
            this.panelControl3.Controls.Add(this.label3);
            this.panelControl3.Controls.Add(this.labelControl55);
            this.panelControl3.Location = new System.Drawing.Point(332, 368);
            this.panelControl3.Margin = new System.Windows.Forms.Padding(4);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(636, 125);
            this.panelControl3.TabIndex = 295;
            this.panelControl3.Visible = false;
            // 
            // meObservacionFirma
            // 
            this.meObservacionFirma.Location = new System.Drawing.Point(5, 25);
            this.meObservacionFirma.Margin = new System.Windows.Forms.Padding(4);
            this.meObservacionFirma.Name = "meObservacionFirma";
            this.meObservacionFirma.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.meObservacionFirma.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.meObservacionFirma.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.meObservacionFirma.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.meObservacionFirma.Size = new System.Drawing.Size(626, 96);
            this.meObservacionFirma.TabIndex = 287;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(116, 5);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(15, 16);
            this.label3.TabIndex = 289;
            this.label3.Text = "*";
            // 
            // labelControl55
            // 
            this.labelControl55.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl55.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl55.Appearance.Options.UseFont = true;
            this.labelControl55.Appearance.Options.UseForeColor = true;
            this.labelControl55.Location = new System.Drawing.Point(6, 2);
            this.labelControl55.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl55.Name = "labelControl55";
            this.labelControl55.Size = new System.Drawing.Size(101, 20);
            this.labelControl55.TabIndex = 288;
            this.labelControl55.Text = "Observaciones";
            // 
            // sbtnBorrarFirma
            // 
            this.sbtnBorrarFirma.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.sbtnBorrarFirma.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sbtnBorrarFirma.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.sbtnBorrarFirma.Appearance.Options.UseBackColor = true;
            this.sbtnBorrarFirma.Appearance.Options.UseFont = true;
            this.sbtnBorrarFirma.Appearance.Options.UseForeColor = true;
            this.sbtnBorrarFirma.Appearance.Options.UseTextOptions = true;
            this.sbtnBorrarFirma.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.sbtnBorrarFirma.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.sbtnBorrarFirma.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_eraser_32x32;
            this.sbtnBorrarFirma.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.sbtnBorrarFirma.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.sbtnBorrarFirma.Location = new System.Drawing.Point(332, 300);
            this.sbtnBorrarFirma.Margin = new System.Windows.Forms.Padding(4);
            this.sbtnBorrarFirma.Name = "sbtnBorrarFirma";
            this.sbtnBorrarFirma.Size = new System.Drawing.Size(142, 37);
            this.sbtnBorrarFirma.TabIndex = 239;
            this.sbtnBorrarFirma.Text = "Borrar";
            this.sbtnBorrarFirma.Click += new System.EventHandler(this.sbtnBorrarFirma_Click);
            // 
            // chknoPuedeFirmar
            // 
            this.chknoPuedeFirmar.Location = new System.Drawing.Point(820, 302);
            this.chknoPuedeFirmar.Margin = new System.Windows.Forms.Padding(4);
            this.chknoPuedeFirmar.Name = "chknoPuedeFirmar";
            this.chknoPuedeFirmar.Properties.Appearance.Font = new System.Drawing.Font("Trebuchet MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chknoPuedeFirmar.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.chknoPuedeFirmar.Properties.Appearance.Options.UseFont = true;
            this.chknoPuedeFirmar.Properties.Appearance.Options.UseForeColor = true;
            this.chknoPuedeFirmar.Properties.AutoHeight = false;
            this.chknoPuedeFirmar.Properties.Caption = "No puede firmar";
            this.chknoPuedeFirmar.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.SvgToggle1;
            this.chknoPuedeFirmar.Properties.CheckBoxOptions.SvgColorChecked = DevExpress.LookAndFeel.DXSkinColors.ForeColors.Information;
            this.chknoPuedeFirmar.Properties.CheckBoxOptions.SvgColorUnchecked = DevExpress.LookAndFeel.DXSkinColors.ForeColors.DisabledText;
            this.chknoPuedeFirmar.Properties.CheckBoxOptions.SvgImageSize = new System.Drawing.Size(22, 22);
            this.chknoPuedeFirmar.Size = new System.Drawing.Size(148, 26);
            this.chknoPuedeFirmar.TabIndex = 240;
            this.chknoPuedeFirmar.CheckedChanged += new System.EventHandler(this.chknoPuedeFirmar_CheckedChanged);
            // 
            // tabBiograficos
            // 
            this.tabBiograficos.Caption = "tabBiograficos";
            this.tabBiograficos.Controls.Add(this.labelControl2);
            this.tabBiograficos.Controls.Add(this.labelControl61);
            this.tabBiograficos.Controls.Add(this.labelControl59);
            this.tabBiograficos.Controls.Add(this.labelControl20);
            this.tabBiograficos.Controls.Add(this.labelControl54);
            this.tabBiograficos.Controls.Add(this.labelControl37);
            this.tabBiograficos.Controls.Add(this.labelControl24);
            this.tabBiograficos.Controls.Add(this.labelControl3);
            this.tabBiograficos.Controls.Add(this.labelControl4);
            this.tabBiograficos.Controls.Add(this.labelControl23);
            this.tabBiograficos.Controls.Add(this.label1);
            this.tabBiograficos.Controls.Add(this.label8);
            this.tabBiograficos.Controls.Add(this.label6);
            this.tabBiograficos.Controls.Add(this.labelControl66);
            this.tabBiograficos.Controls.Add(this.labelControl69);
            this.tabBiograficos.Controls.Add(this.luePaisResidencia);
            this.tabBiograficos.Controls.Add(this.chkNoPresencial);
            this.tabBiograficos.Controls.Add(this.simpleButton1);
            this.tabBiograficos.Controls.Add(this.label7);
            this.tabBiograficos.Controls.Add(this.labelControl10);
            this.tabBiograficos.Controls.Add(this.txtCorreoTrabajo);
            this.tabBiograficos.Controls.Add(this.labelControl21);
            this.tabBiograficos.Controls.Add(this.labelControl17);
            this.tabBiograficos.Controls.Add(this.labelControl8);
            this.tabBiograficos.Controls.Add(this.labelControl15);
            this.tabBiograficos.Controls.Add(this.labelControl42);
            this.tabBiograficos.Controls.Add(this.meDireccionTrabajo);
            this.tabBiograficos.Controls.Add(this.txtTelefonoTrabajo);
            this.tabBiograficos.Controls.Add(this.lueProfesion);
            this.tabBiograficos.Controls.Add(this.lueNivelEducativo);
            this.tabBiograficos.Controls.Add(this.meDireccionCompleta);
            this.tabBiograficos.Controls.Add(this.txtTelefono);
            this.tabBiograficos.Controls.Add(this.label2);
            this.tabBiograficos.Controls.Add(this.labelControl62);
            this.tabBiograficos.Controls.Add(this.labelControl60);
            this.tabBiograficos.Controls.Add(this.labelControl14);
            this.tabBiograficos.Controls.Add(this.labelControl7);
            this.tabBiograficos.Controls.Add(this.labelControl19);
            this.tabBiograficos.Controls.Add(this.labelControl58);
            this.tabBiograficos.Controls.Add(this.labelControl57);
            this.tabBiograficos.Controls.Add(this.labelControl12);
            this.tabBiograficos.Controls.Add(this.lueColonia);
            this.tabBiograficos.Controls.Add(this.lueMunicipios);
            this.tabBiograficos.Controls.Add(this.lueDepartamentos);
            this.tabBiograficos.Controls.Add(this.label28);
            this.tabBiograficos.Controls.Add(this.labelControl5);
            this.tabBiograficos.Controls.Add(this.label20);
            this.tabBiograficos.Controls.Add(this.label19);
            this.tabBiograficos.Controls.Add(this.label18);
            this.tabBiograficos.Controls.Add(this.label16);
            this.tabBiograficos.Controls.Add(this.txtMovil);
            this.tabBiograficos.Controls.Add(this.txtCorreo);
            this.tabBiograficos.Controls.Add(this.label14);
            this.tabBiograficos.Controls.Add(this.label13);
            this.tabBiograficos.Controls.Add(this.label12);
            this.tabBiograficos.Controls.Add(this.label10);
            this.tabBiograficos.Controls.Add(this.lueEstadoCivil);
            this.tabBiograficos.Controls.Add(this.lueSexo);
            this.tabBiograficos.Controls.Add(this.label23);
            this.tabBiograficos.Controls.Add(this.label21);
            this.tabBiograficos.Controls.Add(this.label5);
            this.tabBiograficos.Controls.Add(this.label4);
            this.tabBiograficos.Controls.Add(this.labelControl53);
            this.tabBiograficos.Controls.Add(this.labelControl16);
            this.tabBiograficos.Controls.Add(this.labelControl9);
            this.tabBiograficos.Controls.Add(this.labelControl6);
            this.tabBiograficos.Controls.Add(this.labelControl11);
            this.tabBiograficos.Controls.Add(this.luePaisNacimiento);
            this.tabBiograficos.Controls.Add(this.dtmFechaNacimiento);
            this.tabBiograficos.Controls.Add(this.lueNacionalidad);
            this.tabBiograficos.Controls.Add(this.lueTipoDocumento);
            this.tabBiograficos.Controls.Add(this.txtIdentificacion);
            this.tabBiograficos.Controls.Add(this.txtPrimerNombre);
            this.tabBiograficos.Controls.Add(this.txtSegundoNombre);
            this.tabBiograficos.Controls.Add(this.txtPrimerApellido);
            this.tabBiograficos.Controls.Add(this.txtSegundoApellido);
            this.tabBiograficos.Controls.Add(this.labelControl65);
            this.tabBiograficos.Name = "tabBiograficos";
            this.tabBiograficos.Size = new System.Drawing.Size(1359, 591);
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Appearance.Options.UseForeColor = true;
            this.labelControl2.Location = new System.Drawing.Point(743, 94);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(103, 20);
            this.labelControl2.TabIndex = 549;
            this.labelControl2.Text = "Nivel educativo";
            // 
            // labelControl61
            // 
            this.labelControl61.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl61.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl61.Appearance.Options.UseFont = true;
            this.labelControl61.Appearance.Options.UseForeColor = true;
            this.labelControl61.Location = new System.Drawing.Point(743, 36);
            this.labelControl61.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl61.Name = "labelControl61";
            this.labelControl61.Size = new System.Drawing.Size(62, 20);
            this.labelControl61.TabIndex = 548;
            this.labelControl61.Text = "Profesión";
            // 
            // labelControl59
            // 
            this.labelControl59.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl59.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl59.Appearance.Options.UseFont = true;
            this.labelControl59.Appearance.Options.UseForeColor = true;
            this.labelControl59.Location = new System.Drawing.Point(392, 514);
            this.labelControl59.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl59.Name = "labelControl59";
            this.labelControl59.Size = new System.Drawing.Size(45, 20);
            this.labelControl59.TabIndex = 536;
            this.labelControl59.Text = "Correo";
            // 
            // labelControl20
            // 
            this.labelControl20.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl20.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl20.Appearance.Options.UseFont = true;
            this.labelControl20.Appearance.Options.UseForeColor = true;
            this.labelControl20.Location = new System.Drawing.Point(392, 34);
            this.labelControl20.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(118, 20);
            this.labelControl20.TabIndex = 566;
            this.labelControl20.Text = "País de residencia";
            // 
            // labelControl54
            // 
            this.labelControl54.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl54.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl54.Appearance.Options.UseFont = true;
            this.labelControl54.Appearance.Options.UseForeColor = true;
            this.labelControl54.Location = new System.Drawing.Point(35, 514);
            this.labelControl54.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl54.Name = "labelControl54";
            this.labelControl54.Size = new System.Drawing.Size(75, 20);
            this.labelControl54.TabIndex = 506;
            this.labelControl54.Text = "Estado civil";
            // 
            // labelControl37
            // 
            this.labelControl37.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl37.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl37.Appearance.Options.UseFont = true;
            this.labelControl37.Appearance.Options.UseForeColor = true;
            this.labelControl37.Location = new System.Drawing.Point(35, 396);
            this.labelControl37.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl37.Name = "labelControl37";
            this.labelControl37.Size = new System.Drawing.Size(89, 20);
            this.labelControl37.TabIndex = 504;
            this.labelControl37.Text = "Nacionalidad";
            // 
            // labelControl24
            // 
            this.labelControl24.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl24.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl24.Appearance.Options.UseFont = true;
            this.labelControl24.Appearance.Options.UseForeColor = true;
            this.labelControl24.Location = new System.Drawing.Point(33, 337);
            this.labelControl24.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(125, 20);
            this.labelControl24.TabIndex = 503;
            this.labelControl24.Text = "Pais de nacimiento";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Appearance.Options.UseForeColor = true;
            this.labelControl3.Location = new System.Drawing.Point(35, 92);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(169, 20);
            this.labelControl3.TabIndex = 499;
            this.labelControl3.Text = "Número de Identificación";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Appearance.Options.UseForeColor = true;
            this.labelControl4.Location = new System.Drawing.Point(33, 36);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(133, 20);
            this.labelControl4.TabIndex = 496;
            this.labelControl4.Text = "Tipo de Documento";
            // 
            // labelControl23
            // 
            this.labelControl23.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl23.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl23.Appearance.Options.UseFont = true;
            this.labelControl23.Appearance.Options.UseForeColor = true;
            this.labelControl23.Location = new System.Drawing.Point(1083, 33);
            this.labelControl23.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(144, 20);
            this.labelControl23.TabIndex = 561;
            this.labelControl23.Text = "Tipo de enrolamiento";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(20, 39);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(15, 16);
            this.label1.TabIndex = 498;
            this.label1.Text = "*";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(378, 37);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(15, 16);
            this.label8.TabIndex = 565;
            this.label8.Text = "*";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(728, 39);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(15, 16);
            this.label6.TabIndex = 554;
            this.label6.Text = "*";
            // 
            // labelControl66
            // 
            this.labelControl66.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl66.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl66.Appearance.Options.UseFont = true;
            this.labelControl66.Appearance.Options.UseForeColor = true;
            this.labelControl66.Appearance.Options.UseTextOptions = true;
            this.labelControl66.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl66.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl66.LineColor = System.Drawing.Color.Gainsboro;
            this.labelControl66.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.labelControl66.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.labelControl66.LineVisible = true;
            this.labelControl66.Location = new System.Drawing.Point(1083, 4);
            this.labelControl66.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.labelControl66.Name = "labelControl66";
            this.labelControl66.Size = new System.Drawing.Size(262, 24);
            this.labelControl66.TabIndex = 563;
            this.labelControl66.Text = "Información Adicional";
            // 
            // labelControl69
            // 
            this.labelControl69.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl69.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl69.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.labelControl69.LineLocation = DevExpress.XtraEditors.LineLocation.Top;
            this.labelControl69.LineVisible = true;
            this.labelControl69.Location = new System.Drawing.Point(25, 27);
            this.labelControl69.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.labelControl69.Name = "labelControl69";
            this.labelControl69.Size = new System.Drawing.Size(1296, 24);
            this.labelControl69.TabIndex = 568;
            // 
            // luePaisResidencia
            // 
            this.luePaisResidencia.Location = new System.Drawing.Point(380, 59);
            this.luePaisResidencia.Margin = new System.Windows.Forms.Padding(4);
            this.luePaisResidencia.Name = "luePaisResidencia";
            this.luePaisResidencia.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.luePaisResidencia.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.luePaisResidencia.Properties.Appearance.Options.UseBackColor = true;
            this.luePaisResidencia.Properties.Appearance.Options.UseFont = true;
            this.luePaisResidencia.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.luePaisResidencia.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.luePaisResidencia.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.luePaisResidencia.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luePaisResidencia.Properties.NullText = "";
            this.luePaisResidencia.Size = new System.Drawing.Size(311, 26);
            this.luePaisResidencia.TabIndex = 567;
            // 
            // chkNoPresencial
            // 
            this.chkNoPresencial.Location = new System.Drawing.Point(1083, 60);
            this.chkNoPresencial.Name = "chkNoPresencial";
            this.chkNoPresencial.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkNoPresencial.Properties.Appearance.Options.UseFont = true;
            this.chkNoPresencial.Properties.Appearance.Options.UseTextOptions = true;
            this.chkNoPresencial.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.chkNoPresencial.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.chkNoPresencial.Properties.AutoHeight = false;
            this.chkNoPresencial.Properties.Caption = "No presencial";
            this.chkNoPresencial.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.SvgToggle1;
            this.chkNoPresencial.Properties.CheckBoxOptions.SvgColorChecked = DevExpress.LookAndFeel.DXSkinColors.ForeColors.Information;
            this.chkNoPresencial.Properties.CheckBoxOptions.SvgColorUnchecked = DevExpress.LookAndFeel.DXSkinColors.ForeColors.DisabledText;
            this.chkNoPresencial.Properties.CheckBoxOptions.SvgImageSize = new System.Drawing.Size(23, 23);
            this.chkNoPresencial.Size = new System.Drawing.Size(198, 30);
            this.chkNoPresencial.TabIndex = 564;
            this.chkNoPresencial.CheckedChanged += new System.EventHandler(this.chkNoPresencial_CheckedChanged);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(1262, 558);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(94, 29);
            this.simpleButton1.TabIndex = 556;
            this.simpleButton1.Text = "Datos Prueba";
            this.simpleButton1.Visible = false;
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(730, 98);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(15, 16);
            this.label7.TabIndex = 555;
            this.label7.Text = "*";
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl10.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl10.Appearance.Options.UseFont = true;
            this.labelControl10.Appearance.Options.UseForeColor = true;
            this.labelControl10.Appearance.Options.UseTextOptions = true;
            this.labelControl10.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl10.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl10.LineColor = System.Drawing.Color.Gainsboro;
            this.labelControl10.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.labelControl10.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.labelControl10.LineVisible = true;
            this.labelControl10.Location = new System.Drawing.Point(750, 544);
            this.labelControl10.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(144, 24);
            this.labelControl10.TabIndex = 553;
            this.labelControl10.Text = "Campos requeridos";
            // 
            // txtCorreoTrabajo
            // 
            this.txtCorreoTrabajo.Location = new System.Drawing.Point(733, 361);
            this.txtCorreoTrabajo.Margin = new System.Windows.Forms.Padding(4);
            this.txtCorreoTrabajo.Name = "txtCorreoTrabajo";
            this.txtCorreoTrabajo.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCorreoTrabajo.Properties.Appearance.Options.UseFont = true;
            this.txtCorreoTrabajo.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtCorreoTrabajo.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtCorreoTrabajo.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtCorreoTrabajo.Properties.Mask.EditMask = "\\w+([-+.\']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
            this.txtCorreoTrabajo.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtCorreoTrabajo.Properties.MaxLength = 100;
            this.txtCorreoTrabajo.Size = new System.Drawing.Size(311, 26);
            this.txtCorreoTrabajo.TabIndex = 545;
            // 
            // labelControl21
            // 
            this.labelControl21.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl21.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl21.Appearance.Options.UseFont = true;
            this.labelControl21.Appearance.Options.UseForeColor = true;
            this.labelControl21.Location = new System.Drawing.Point(733, 337);
            this.labelControl21.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(123, 20);
            this.labelControl21.TabIndex = 552;
            this.labelControl21.Text = "Correo electrónico";
            // 
            // labelControl17
            // 
            this.labelControl17.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl17.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl17.Appearance.Options.UseFont = true;
            this.labelControl17.Appearance.Options.UseForeColor = true;
            this.labelControl17.Location = new System.Drawing.Point(732, 215);
            this.labelControl17.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(136, 20);
            this.labelControl17.TabIndex = 551;
            this.labelControl17.Text = "Dirección de trabajo";
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl8.Appearance.Options.UseFont = true;
            this.labelControl8.Appearance.Options.UseForeColor = true;
            this.labelControl8.Location = new System.Drawing.Point(733, 155);
            this.labelControl8.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(135, 20);
            this.labelControl8.TabIndex = 550;
            this.labelControl8.Text = "Número de teléfono";
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl15.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl15.Appearance.Options.UseFont = true;
            this.labelControl15.Appearance.Options.UseForeColor = true;
            this.labelControl15.Appearance.Options.UseTextOptions = true;
            this.labelControl15.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl15.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl15.LineColor = System.Drawing.Color.Gainsboro;
            this.labelControl15.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.labelControl15.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.labelControl15.LineVisible = true;
            this.labelControl15.Location = new System.Drawing.Point(732, 4);
            this.labelControl15.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(312, 24);
            this.labelControl15.TabIndex = 547;
            this.labelControl15.Text = "Información Laboral";
            // 
            // labelControl42
            // 
            this.labelControl42.Appearance.Font = new System.Drawing.Font("Segoe UI Emoji", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl42.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl42.Appearance.Options.UseFont = true;
            this.labelControl42.Appearance.Options.UseForeColor = true;
            this.labelControl42.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl42.Location = new System.Drawing.Point(724, 546);
            this.labelControl42.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl42.Name = "labelControl42";
            this.labelControl42.Size = new System.Drawing.Size(20, 16);
            this.labelControl42.TabIndex = 546;
            this.labelControl42.Text = "(*)\r\n";
            // 
            // meDireccionTrabajo
            // 
            this.meDireccionTrabajo.Location = new System.Drawing.Point(732, 239);
            this.meDireccionTrabajo.Margin = new System.Windows.Forms.Padding(4);
            this.meDireccionTrabajo.Name = "meDireccionTrabajo";
            this.meDireccionTrabajo.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.meDireccionTrabajo.Properties.Appearance.Options.UseFont = true;
            this.meDireccionTrabajo.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.meDireccionTrabajo.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.meDireccionTrabajo.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.meDireccionTrabajo.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.meDireccionTrabajo.Properties.MaxLength = 100;
            this.meDireccionTrabajo.Size = new System.Drawing.Size(311, 88);
            this.meDireccionTrabajo.TabIndex = 544;
            // 
            // txtTelefonoTrabajo
            // 
            this.txtTelefonoTrabajo.Location = new System.Drawing.Point(732, 182);
            this.txtTelefonoTrabajo.Margin = new System.Windows.Forms.Padding(4);
            this.txtTelefonoTrabajo.Name = "txtTelefonoTrabajo";
            this.txtTelefonoTrabajo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtTelefonoTrabajo.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTelefonoTrabajo.Properties.Appearance.Options.UseBackColor = true;
            this.txtTelefonoTrabajo.Properties.Appearance.Options.UseFont = true;
            this.txtTelefonoTrabajo.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTelefonoTrabajo.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTelefonoTrabajo.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtTelefonoTrabajo.Properties.Mask.BeepOnError = true;
            this.txtTelefonoTrabajo.Properties.Mask.EditMask = "[0-9,()-]+";
            this.txtTelefonoTrabajo.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtTelefonoTrabajo.Properties.MaxLength = 20;
            this.txtTelefonoTrabajo.Size = new System.Drawing.Size(311, 26);
            this.txtTelefonoTrabajo.TabIndex = 543;
            // 
            // lueProfesion
            // 
            this.lueProfesion.Location = new System.Drawing.Point(732, 60);
            this.lueProfesion.Margin = new System.Windows.Forms.Padding(4);
            this.lueProfesion.Name = "lueProfesion";
            this.lueProfesion.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueProfesion.Properties.Appearance.Options.UseFont = true;
            this.lueProfesion.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lueProfesion.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lueProfesion.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.lueProfesion.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueProfesion.Properties.NullText = "";
            this.lueProfesion.Size = new System.Drawing.Size(312, 26);
            this.lueProfesion.TabIndex = 541;
            // 
            // lueNivelEducativo
            // 
            this.lueNivelEducativo.Location = new System.Drawing.Point(732, 118);
            this.lueNivelEducativo.Margin = new System.Windows.Forms.Padding(4);
            this.lueNivelEducativo.Name = "lueNivelEducativo";
            this.lueNivelEducativo.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueNivelEducativo.Properties.Appearance.Options.UseFont = true;
            this.lueNivelEducativo.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lueNivelEducativo.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lueNivelEducativo.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.lueNivelEducativo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueNivelEducativo.Properties.NullText = "";
            this.lueNivelEducativo.Size = new System.Drawing.Size(312, 26);
            this.lueNivelEducativo.TabIndex = 542;
            // 
            // meDireccionCompleta
            // 
            this.meDireccionCompleta.Location = new System.Drawing.Point(380, 299);
            this.meDireccionCompleta.Margin = new System.Windows.Forms.Padding(4);
            this.meDireccionCompleta.Name = "meDireccionCompleta";
            this.meDireccionCompleta.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.meDireccionCompleta.Properties.Appearance.Options.UseFont = true;
            this.meDireccionCompleta.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.meDireccionCompleta.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.meDireccionCompleta.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.meDireccionCompleta.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.meDireccionCompleta.Properties.MaxLength = 100;
            this.meDireccionCompleta.Size = new System.Drawing.Size(311, 87);
            this.meDireccionCompleta.TabIndex = 539;
            // 
            // txtTelefono
            // 
            this.txtTelefono.Location = new System.Drawing.Point(380, 420);
            this.txtTelefono.Margin = new System.Windows.Forms.Padding(4);
            this.txtTelefono.Name = "txtTelefono";
            this.txtTelefono.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtTelefono.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTelefono.Properties.Appearance.Options.UseBackColor = true;
            this.txtTelefono.Properties.Appearance.Options.UseFont = true;
            this.txtTelefono.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTelefono.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTelefono.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtTelefono.Properties.Mask.BeepOnError = true;
            this.txtTelefono.Properties.Mask.EditMask = "[0-9]+";
            this.txtTelefono.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtTelefono.Properties.MaxLength = 20;
            this.txtTelefono.Size = new System.Drawing.Size(311, 26);
            this.txtTelefono.TabIndex = 540;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(378, 517);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(15, 16);
            this.label2.TabIndex = 538;
            this.label2.Text = "*";
            // 
            // labelControl62
            // 
            this.labelControl62.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl62.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl62.Appearance.Options.UseFont = true;
            this.labelControl62.Appearance.Options.UseForeColor = true;
            this.labelControl62.Appearance.Options.UseTextOptions = true;
            this.labelControl62.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl62.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl62.LineColor = System.Drawing.Color.Gainsboro;
            this.labelControl62.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.labelControl62.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.labelControl62.LineVisible = true;
            this.labelControl62.Location = new System.Drawing.Point(763, 451);
            this.labelControl62.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.labelControl62.Name = "labelControl62";
            this.labelControl62.Size = new System.Drawing.Size(322, 24);
            this.labelControl62.TabIndex = 537;
            this.labelControl62.Text = "Al menos un número telefónico es requerido";
            // 
            // labelControl60
            // 
            this.labelControl60.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl60.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl60.Appearance.Options.UseFont = true;
            this.labelControl60.Appearance.Options.UseForeColor = true;
            this.labelControl60.Location = new System.Drawing.Point(401, 453);
            this.labelControl60.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl60.Name = "labelControl60";
            this.labelControl60.Size = new System.Drawing.Size(123, 20);
            this.labelControl60.TabIndex = 535;
            this.labelControl60.Text = "Número de celular";
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl14.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl14.Appearance.Options.UseFont = true;
            this.labelControl14.Appearance.Options.UseForeColor = true;
            this.labelControl14.Location = new System.Drawing.Point(401, 396);
            this.labelControl14.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(135, 20);
            this.labelControl14.TabIndex = 534;
            this.labelControl14.Text = "Número de teléfono";
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl7.Appearance.Options.UseFont = true;
            this.labelControl7.Appearance.Options.UseForeColor = true;
            this.labelControl7.Location = new System.Drawing.Point(380, 274);
            this.labelControl7.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(67, 20);
            this.labelControl7.TabIndex = 533;
            this.labelControl7.Text = "Dirección ";
            // 
            // labelControl19
            // 
            this.labelControl19.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl19.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl19.Appearance.Options.UseFont = true;
            this.labelControl19.Appearance.Options.UseForeColor = true;
            this.labelControl19.Location = new System.Drawing.Point(381, 219);
            this.labelControl19.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(143, 20);
            this.labelControl19.TabIndex = 532;
            this.labelControl19.Text = "Colonia de residencia";
            // 
            // labelControl58
            // 
            this.labelControl58.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl58.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl58.Appearance.Options.UseFont = true;
            this.labelControl58.Appearance.Options.UseForeColor = true;
            this.labelControl58.Location = new System.Drawing.Point(392, 151);
            this.labelControl58.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl58.Name = "labelControl58";
            this.labelControl58.Size = new System.Drawing.Size(158, 20);
            this.labelControl58.TabIndex = 531;
            this.labelControl58.Text = "Municipio de residencia";
            // 
            // labelControl57
            // 
            this.labelControl57.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl57.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl57.Appearance.Options.UseFont = true;
            this.labelControl57.Appearance.Options.UseForeColor = true;
            this.labelControl57.Location = new System.Drawing.Point(392, 98);
            this.labelControl57.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl57.Name = "labelControl57";
            this.labelControl57.Size = new System.Drawing.Size(189, 20);
            this.labelControl57.TabIndex = 530;
            this.labelControl57.Text = "Departamento de residencia";
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl12.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl12.Appearance.Options.UseFont = true;
            this.labelControl12.Appearance.Options.UseForeColor = true;
            this.labelControl12.Appearance.Options.UseTextOptions = true;
            this.labelControl12.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl12.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl12.LineColor = System.Drawing.Color.Gainsboro;
            this.labelControl12.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.labelControl12.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.labelControl12.LineVisible = true;
            this.labelControl12.Location = new System.Drawing.Point(381, 4);
            this.labelControl12.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(310, 24);
            this.labelControl12.TabIndex = 529;
            this.labelControl12.Text = "Información de Residencia";
            // 
            // lueColonia
            // 
            this.lueColonia.Location = new System.Drawing.Point(380, 240);
            this.lueColonia.Margin = new System.Windows.Forms.Padding(4);
            this.lueColonia.Name = "lueColonia";
            this.lueColonia.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueColonia.Properties.Appearance.Options.UseFont = true;
            this.lueColonia.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueColonia.Properties.NullText = "";
            this.lueColonia.Properties.PopupFilterMode = DevExpress.XtraEditors.PopupFilterMode.Contains;
            this.lueColonia.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.lueColonia.Size = new System.Drawing.Size(311, 26);
            this.lueColonia.TabIndex = 520;
            // 
            // lueMunicipios
            // 
            this.lueMunicipios.Location = new System.Drawing.Point(380, 176);
            this.lueMunicipios.Margin = new System.Windows.Forms.Padding(4);
            this.lueMunicipios.Name = "lueMunicipios";
            this.lueMunicipios.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueMunicipios.Properties.Appearance.Options.UseFont = true;
            this.lueMunicipios.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueMunicipios.Properties.NullText = "";
            this.lueMunicipios.Size = new System.Drawing.Size(311, 26);
            this.lueMunicipios.TabIndex = 519;
            // 
            // lueDepartamentos
            // 
            this.lueDepartamentos.Location = new System.Drawing.Point(380, 119);
            this.lueDepartamentos.Margin = new System.Windows.Forms.Padding(4);
            this.lueDepartamentos.Name = "lueDepartamentos";
            this.lueDepartamentos.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueDepartamentos.Properties.Appearance.Options.UseFont = true;
            this.lueDepartamentos.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueDepartamentos.Properties.NullText = "";
            this.lueDepartamentos.Size = new System.Drawing.Size(311, 26);
            this.lueDepartamentos.TabIndex = 518;
            this.lueDepartamentos.EditValueChanged += new System.EventHandler(this.lueDepartamentos_EditValueChanged);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.ForeColor = System.Drawing.Color.Red;
            this.label28.Location = new System.Drawing.Point(377, 154);
            this.label28.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(15, 16);
            this.label28.TabIndex = 528;
            this.label28.Text = "*";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Appearance.Options.UseForeColor = true;
            this.labelControl5.Location = new System.Drawing.Point(731, 455);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(22, 20);
            this.labelControl5.TabIndex = 527;
            this.labelControl5.Text = "(**)\r\n";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.ForeColor = System.Drawing.Color.Red;
            this.label20.Location = new System.Drawing.Point(377, 457);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(23, 16);
            this.label20.TabIndex = 526;
            this.label20.Text = "**";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.ForeColor = System.Drawing.Color.Red;
            this.label19.Location = new System.Drawing.Point(374, 399);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(23, 16);
            this.label19.TabIndex = 525;
            this.label19.Text = "**";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.ForeColor = System.Drawing.Color.Red;
            this.label18.Location = new System.Drawing.Point(380, 279);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(15, 16);
            this.label18.TabIndex = 524;
            this.label18.Text = "*";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.ForeColor = System.Drawing.Color.Red;
            this.label16.Location = new System.Drawing.Point(378, 98);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(15, 16);
            this.label16.TabIndex = 523;
            this.label16.Text = "*";
            // 
            // txtMovil
            // 
            this.txtMovil.Location = new System.Drawing.Point(380, 478);
            this.txtMovil.Margin = new System.Windows.Forms.Padding(4);
            this.txtMovil.Name = "txtMovil";
            this.txtMovil.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtMovil.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMovil.Properties.Appearance.Options.UseBackColor = true;
            this.txtMovil.Properties.Appearance.Options.UseFont = true;
            this.txtMovil.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtMovil.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtMovil.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtMovil.Properties.Mask.BeepOnError = true;
            this.txtMovil.Properties.Mask.EditMask = "[0-9,()-]+";
            this.txtMovil.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtMovil.Properties.MaxLength = 20;
            this.txtMovil.Size = new System.Drawing.Size(311, 26);
            this.txtMovil.TabIndex = 521;
            // 
            // txtCorreo
            // 
            this.txtCorreo.Location = new System.Drawing.Point(380, 538);
            this.txtCorreo.Margin = new System.Windows.Forms.Padding(4);
            this.txtCorreo.Name = "txtCorreo";
            this.txtCorreo.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCorreo.Properties.Appearance.Options.UseFont = true;
            this.txtCorreo.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtCorreo.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtCorreo.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtCorreo.Properties.Mask.EditMask = "\\w+([-+.\']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
            this.txtCorreo.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtCorreo.Properties.MaxLength = 100;
            this.txtCorreo.Size = new System.Drawing.Size(311, 26);
            this.txtCorreo.TabIndex = 522;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Location = new System.Drawing.Point(22, 517);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(15, 16);
            this.label14.TabIndex = 517;
            this.label14.Text = "*";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Location = new System.Drawing.Point(20, 455);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(15, 16);
            this.label13.TabIndex = 516;
            this.label13.Text = "*";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(22, 399);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(15, 16);
            this.label12.TabIndex = 515;
            this.label12.Text = "*";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(20, 340);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(15, 16);
            this.label10.TabIndex = 514;
            this.label10.Text = "*";
            // 
            // lueEstadoCivil
            // 
            this.lueEstadoCivil.Location = new System.Drawing.Point(25, 538);
            this.lueEstadoCivil.Margin = new System.Windows.Forms.Padding(4);
            this.lueEstadoCivil.Name = "lueEstadoCivil";
            this.lueEstadoCivil.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueEstadoCivil.Properties.Appearance.Options.UseFont = true;
            this.lueEstadoCivil.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lueEstadoCivil.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lueEstadoCivil.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.lueEstadoCivil.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueEstadoCivil.Properties.NullText = "";
            this.lueEstadoCivil.Size = new System.Drawing.Size(322, 26);
            this.lueEstadoCivil.TabIndex = 495;
            // 
            // lueSexo
            // 
            this.lueSexo.Location = new System.Drawing.Point(25, 300);
            this.lueSexo.Margin = new System.Windows.Forms.Padding(4);
            this.lueSexo.Name = "lueSexo";
            this.lueSexo.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueSexo.Properties.Appearance.Options.UseFont = true;
            this.lueSexo.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lueSexo.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lueSexo.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.lueSexo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueSexo.Properties.NullText = "";
            this.lueSexo.Size = new System.Drawing.Size(320, 26);
            this.lueSexo.TabIndex = 491;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.ForeColor = System.Drawing.Color.Red;
            this.label23.Location = new System.Drawing.Point(20, 279);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(15, 16);
            this.label23.TabIndex = 510;
            this.label23.Text = "*";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.ForeColor = System.Drawing.Color.Red;
            this.label21.Location = new System.Drawing.Point(20, 219);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(15, 16);
            this.label21.TabIndex = 509;
            this.label21.Text = "*";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(20, 158);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(15, 16);
            this.label5.TabIndex = 508;
            this.label5.Text = "*";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(22, 96);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(15, 16);
            this.label4.TabIndex = 507;
            this.label4.Text = "*";
            // 
            // labelControl53
            // 
            this.labelControl53.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl53.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl53.Appearance.Options.UseFont = true;
            this.labelControl53.Appearance.Options.UseForeColor = true;
            this.labelControl53.Location = new System.Drawing.Point(35, 456);
            this.labelControl53.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl53.Name = "labelControl53";
            this.labelControl53.Size = new System.Drawing.Size(137, 20);
            this.labelControl53.TabIndex = 505;
            this.labelControl53.Text = "Fecha de nacimiento";
            // 
            // labelControl16
            // 
            this.labelControl16.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl16.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl16.Appearance.Options.UseFont = true;
            this.labelControl16.Appearance.Options.UseForeColor = true;
            this.labelControl16.Location = new System.Drawing.Point(35, 276);
            this.labelControl16.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(32, 20);
            this.labelControl16.TabIndex = 502;
            this.labelControl16.Text = "Sexo";
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl9.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl9.Appearance.Options.UseFont = true;
            this.labelControl9.Appearance.Options.UseForeColor = true;
            this.labelControl9.Location = new System.Drawing.Point(35, 216);
            this.labelControl9.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(174, 20);
            this.labelControl9.TabIndex = 501;
            this.labelControl9.Text = "Primer y segundo apellido";
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl6.Appearance.Options.UseFont = true;
            this.labelControl6.Appearance.Options.UseForeColor = true;
            this.labelControl6.Location = new System.Drawing.Point(35, 153);
            this.labelControl6.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(171, 20);
            this.labelControl6.TabIndex = 500;
            this.labelControl6.Text = "Primer y segundo nombre";
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl11.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl11.Appearance.Options.UseFont = true;
            this.labelControl11.Appearance.Options.UseForeColor = true;
            this.labelControl11.Appearance.Options.UseTextOptions = true;
            this.labelControl11.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl11.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl11.LineColor = System.Drawing.Color.Gainsboro;
            this.labelControl11.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.labelControl11.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.labelControl11.LineVisible = true;
            this.labelControl11.Location = new System.Drawing.Point(25, 4);
            this.labelControl11.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(322, 24);
            this.labelControl11.TabIndex = 497;
            this.labelControl11.Text = "Información Personal";
            // 
            // luePaisNacimiento
            // 
            this.luePaisNacimiento.Location = new System.Drawing.Point(25, 361);
            this.luePaisNacimiento.Margin = new System.Windows.Forms.Padding(4);
            this.luePaisNacimiento.Name = "luePaisNacimiento";
            this.luePaisNacimiento.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.luePaisNacimiento.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.luePaisNacimiento.Properties.Appearance.Options.UseBackColor = true;
            this.luePaisNacimiento.Properties.Appearance.Options.UseFont = true;
            this.luePaisNacimiento.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.luePaisNacimiento.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.luePaisNacimiento.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.luePaisNacimiento.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luePaisNacimiento.Properties.NullText = "";
            this.luePaisNacimiento.Size = new System.Drawing.Size(320, 26);
            this.luePaisNacimiento.TabIndex = 492;
            // 
            // dtmFechaNacimiento
            // 
            this.dtmFechaNacimiento.EditValue = null;
            this.dtmFechaNacimiento.Location = new System.Drawing.Point(25, 480);
            this.dtmFechaNacimiento.Margin = new System.Windows.Forms.Padding(4);
            this.dtmFechaNacimiento.Name = "dtmFechaNacimiento";
            this.dtmFechaNacimiento.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.dtmFechaNacimiento.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtmFechaNacimiento.Properties.Appearance.Options.UseBackColor = true;
            this.dtmFechaNacimiento.Properties.Appearance.Options.UseFont = true;
            this.dtmFechaNacimiento.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dtmFechaNacimiento.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dtmFechaNacimiento.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.dtmFechaNacimiento.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtmFechaNacimiento.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtmFechaNacimiento.Properties.CalendarView = DevExpress.XtraEditors.Repository.CalendarView.Vista;
            this.dtmFechaNacimiento.Properties.VistaDisplayMode = DevExpress.Utils.DefaultBoolean.True;
            this.dtmFechaNacimiento.Size = new System.Drawing.Size(322, 26);
            this.dtmFechaNacimiento.TabIndex = 494;
            // 
            // lueNacionalidad
            // 
            this.lueNacionalidad.Location = new System.Drawing.Point(25, 420);
            this.lueNacionalidad.Margin = new System.Windows.Forms.Padding(4);
            this.lueNacionalidad.Name = "lueNacionalidad";
            this.lueNacionalidad.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.lueNacionalidad.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueNacionalidad.Properties.Appearance.Options.UseBackColor = true;
            this.lueNacionalidad.Properties.Appearance.Options.UseFont = true;
            this.lueNacionalidad.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lueNacionalidad.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lueNacionalidad.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.lueNacionalidad.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueNacionalidad.Properties.NullText = "";
            this.lueNacionalidad.Size = new System.Drawing.Size(321, 26);
            this.lueNacionalidad.TabIndex = 493;
            // 
            // lueTipoDocumento
            // 
            this.lueTipoDocumento.Location = new System.Drawing.Point(25, 59);
            this.lueTipoDocumento.Margin = new System.Windows.Forms.Padding(4);
            this.lueTipoDocumento.Name = "lueTipoDocumento";
            this.lueTipoDocumento.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.lueTipoDocumento.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueTipoDocumento.Properties.Appearance.Options.UseBackColor = true;
            this.lueTipoDocumento.Properties.Appearance.Options.UseFont = true;
            this.lueTipoDocumento.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lueTipoDocumento.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lueTipoDocumento.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.lueTipoDocumento.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueTipoDocumento.Properties.NullText = "";
            this.lueTipoDocumento.Size = new System.Drawing.Size(322, 26);
            this.lueTipoDocumento.TabIndex = 485;
            // 
            // txtIdentificacion
            // 
            this.txtIdentificacion.Location = new System.Drawing.Point(25, 118);
            this.txtIdentificacion.Margin = new System.Windows.Forms.Padding(4);
            this.txtIdentificacion.Name = "txtIdentificacion";
            this.txtIdentificacion.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtIdentificacion.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIdentificacion.Properties.Appearance.Options.UseBackColor = true;
            this.txtIdentificacion.Properties.Appearance.Options.UseFont = true;
            this.txtIdentificacion.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtIdentificacion.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtIdentificacion.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtIdentificacion.Properties.Mask.EditMask = "0000-0000-00000";
            this.txtIdentificacion.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
            this.txtIdentificacion.Properties.Mask.SaveLiteral = false;
            this.txtIdentificacion.Properties.MaxLength = 20;
            this.txtIdentificacion.Size = new System.Drawing.Size(322, 26);
            this.txtIdentificacion.TabIndex = 486;
            // 
            // txtPrimerNombre
            // 
            this.txtPrimerNombre.Location = new System.Drawing.Point(25, 178);
            this.txtPrimerNombre.Margin = new System.Windows.Forms.Padding(4);
            this.txtPrimerNombre.Name = "txtPrimerNombre";
            this.txtPrimerNombre.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtPrimerNombre.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrimerNombre.Properties.Appearance.Options.UseBackColor = true;
            this.txtPrimerNombre.Properties.Appearance.Options.UseFont = true;
            this.txtPrimerNombre.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtPrimerNombre.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtPrimerNombre.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtPrimerNombre.Properties.Mask.BeepOnError = true;
            this.txtPrimerNombre.Properties.Mask.EditMask = "[A-ZÑ]+";
            this.txtPrimerNombre.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtPrimerNombre.Properties.MaxLength = 100;
            this.txtPrimerNombre.Size = new System.Drawing.Size(156, 26);
            this.txtPrimerNombre.TabIndex = 487;
            // 
            // txtSegundoNombre
            // 
            this.txtSegundoNombre.Location = new System.Drawing.Point(191, 178);
            this.txtSegundoNombre.Margin = new System.Windows.Forms.Padding(4);
            this.txtSegundoNombre.Name = "txtSegundoNombre";
            this.txtSegundoNombre.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtSegundoNombre.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSegundoNombre.Properties.Appearance.Options.UseBackColor = true;
            this.txtSegundoNombre.Properties.Appearance.Options.UseFont = true;
            this.txtSegundoNombre.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtSegundoNombre.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtSegundoNombre.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtSegundoNombre.Properties.Mask.EditMask = "[A-Z Ñ]+";
            this.txtSegundoNombre.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtSegundoNombre.Properties.MaxLength = 100;
            this.txtSegundoNombre.Size = new System.Drawing.Size(156, 26);
            this.txtSegundoNombre.TabIndex = 488;
            // 
            // txtPrimerApellido
            // 
            this.txtPrimerApellido.Location = new System.Drawing.Point(26, 242);
            this.txtPrimerApellido.Margin = new System.Windows.Forms.Padding(4);
            this.txtPrimerApellido.Name = "txtPrimerApellido";
            this.txtPrimerApellido.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtPrimerApellido.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrimerApellido.Properties.Appearance.Options.UseBackColor = true;
            this.txtPrimerApellido.Properties.Appearance.Options.UseFont = true;
            this.txtPrimerApellido.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtPrimerApellido.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtPrimerApellido.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtPrimerApellido.Properties.Mask.EditMask = "[A-Z Ñ]+";
            this.txtPrimerApellido.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtPrimerApellido.Properties.MaxLength = 100;
            this.txtPrimerApellido.Size = new System.Drawing.Size(151, 26);
            this.txtPrimerApellido.TabIndex = 489;
            // 
            // txtSegundoApellido
            // 
            this.txtSegundoApellido.Location = new System.Drawing.Point(191, 242);
            this.txtSegundoApellido.Margin = new System.Windows.Forms.Padding(4);
            this.txtSegundoApellido.Name = "txtSegundoApellido";
            this.txtSegundoApellido.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtSegundoApellido.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSegundoApellido.Properties.Appearance.Options.UseBackColor = true;
            this.txtSegundoApellido.Properties.Appearance.Options.UseFont = true;
            this.txtSegundoApellido.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtSegundoApellido.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtSegundoApellido.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtSegundoApellido.Properties.Mask.EditMask = "[A-Z Ñ]+";
            this.txtSegundoApellido.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtSegundoApellido.Properties.MaxLength = 100;
            this.txtSegundoApellido.Size = new System.Drawing.Size(156, 26);
            this.txtSegundoApellido.TabIndex = 490;
            // 
            // labelControl65
            // 
            this.labelControl65.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl65.LineColor = System.Drawing.Color.Gainsboro;
            this.labelControl65.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.labelControl65.LineVisible = true;
            this.labelControl65.Location = new System.Drawing.Point(1083, 31);
            this.labelControl65.Name = "labelControl65";
            this.labelControl65.Size = new System.Drawing.Size(262, 32);
            this.labelControl65.TabIndex = 562;
            // 
            // ssForm
            // 
            this.ssForm.ClosingDelay = 500;
            // 
            // frmInsPrimariaEntrada
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(1380, 766);
            this.Controls.Add(this.xtraScrollableControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmInsPrimariaEntrada";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Enrolamiento";
            this.Load += new System.EventHandler(this.frmInsPrimariaEntrada_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picNumeros.Properties)).EndInit();
            this.xtraScrollableControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picAtencion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picPadFirma.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCS500e.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCamara.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlVerificacionHuellas)).EndInit();
            this.pnlVerificacionHuellas.ResumeLayout(false);
            this.pnlVerificacionHuellas.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabEnrolamiento)).EndInit();
            this.tabEnrolamiento.ResumeLayout(false);
            this.navigationPage4.ResumeLayout(false);
            this.navigationPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlObservacionesPersonaHuellaEstado)).EndInit();
            this.pnlObservacionesPersonaHuellaEstado.ResumeLayout(false);
            this.pnlObservacionesPersonaHuellaEstado.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.meObservacionHuellas.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luePersonaHuellaEstado.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prgbDedo5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prgbDedo4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prgbDedo3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prgbDedo2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prgbDedo1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prgbDedo10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prgbDedo9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prgbDedo8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prgbDedo7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSeleccionDerecha.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlManoDerecha)).EndInit();
            this.pnlManoDerecha.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prgbDerechoGrande.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSeleccionIzquierda.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlManoIzquierda)).EndInit();
            this.pnlManoIzquierda.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prgbIzquierdoGrande.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prgbDedo6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPulgarIzquierdo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMeniequeDerecho.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picIndiceIzquierdo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMedioIzquierdo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMeniequeIzquierdo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAnularIzquierdo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAnularDerecho.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMedioDerecho.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPulgarDerecho.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picIndiceDerecho.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picManoIzquierda.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picManoDerecha.Properties)).EndInit();
            this.navigationPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picFotoCapturada.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlObservacionFoto)).EndInit();
            this.pnlObservacionFoto.ResumeLayout(false);
            this.pnlObservacionFoto.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mmObservacionFoto.Properties)).EndInit();
            this.navigationPage2.ResumeLayout(false);
            this.navigationPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFotoFinal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reviewSignature.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDedo2Final.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDedo6Final.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDedo5Final.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDedo7Final.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDedo8Final.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDedo10Final.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDedo9Final.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDedo4Final.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDedo3Final.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDedo1Final.Properties)).EndInit();
            this.navigationPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.meObservacionFirma.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chknoPuedeFirmar.Properties)).EndInit();
            this.tabBiograficos.ResumeLayout(false);
            this.tabBiograficos.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.luePaisResidencia.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNoPresencial.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCorreoTrabajo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meDireccionTrabajo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTelefonoTrabajo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueProfesion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueNivelEducativo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meDireccionCompleta.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTelefono.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueColonia.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueMunicipios.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueDepartamentos.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMovil.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCorreo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueEstadoCivil.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueSexo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luePaisNacimiento.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtmFechaNacimiento.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtmFechaNacimiento.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueNacionalidad.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTipoDocumento.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdentificacion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrimerNombre.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSegundoNombre.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrimerApellido.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSegundoApellido.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.XtraScrollableControl xtraScrollableControl1;
        private DevExpress.XtraSplashScreen.SplashScreenManager ssmProcesando;
        private DevExpress.XtraEditors.LabelControl lblDetalles;
        private DevExpress.XtraEditors.LabelControl lblTituloAccion;
        private DevExpress.XtraEditors.PictureEdit picNumeros;
        private DevExpress.XtraBars.Navigation.NavigationFrame tabEnrolamiento;
        private DevExpress.XtraBars.Navigation.NavigationPage navigationPage4;
        private DevExpress.XtraBars.Navigation.NavigationPage navigationPage1;
        private DevExpress.XtraEditors.SimpleButton sbtnSiguiente;
        private DevExpress.XtraEditors.SimpleButton sbtnAnterior;
        private DevExpress.XtraEditors.SimpleButton btnReiniciarEscaner;
        private DevExpress.XtraEditors.PanelControl pnlObservacionesPersonaHuellaEstado;
        private System.Windows.Forms.Label label17;
        private DevExpress.XtraEditors.MemoEdit meObservacionHuellas;
        private DevExpress.XtraEditors.LabelControl lblObservacionesHuellas;
        private DevExpress.XtraEditors.LookUpEdit luePersonaHuellaEstado;
        private DevExpress.XtraEditors.LabelControl lblNumeroTransaccion;
        private DevExpress.XtraEditors.LabelControl labelControl98;
        private DevExpress.XtraEditors.LabelControl labelControl78;
        private DevExpress.XtraEditors.SimpleButton sbtnCapturaHuellas;
        private DevExpress.XtraEditors.LabelControl lblDedo5;
        private DevExpress.XtraEditors.ProgressBarControl prgbDedo5;
        private DevExpress.XtraEditors.LabelControl lblDedo4;
        private DevExpress.XtraEditors.ProgressBarControl prgbDedo4;
        private DevExpress.XtraEditors.LabelControl lblDedo3;
        private DevExpress.XtraEditors.ProgressBarControl prgbDedo3;
        private DevExpress.XtraEditors.LabelControl lblDedo1;
        private DevExpress.XtraEditors.ProgressBarControl prgbDedo1;
        private DevExpress.XtraEditors.LabelControl lblDedo10;
        private DevExpress.XtraEditors.ProgressBarControl prgbDedo10;
        private DevExpress.XtraEditors.LabelControl lblDedo9;
        private DevExpress.XtraEditors.ProgressBarControl prgbDedo9;
        private DevExpress.XtraEditors.LabelControl lblDedo8;
        private DevExpress.XtraEditors.ProgressBarControl prgbDedo8;
        private DevExpress.XtraEditors.LabelControl lblDedo7;
        private DevExpress.XtraEditors.ProgressBarControl prgbDedo7;
        private DevExpress.XtraEditors.PictureEdit picSeleccionDerecha;
        private DevExpress.XtraEditors.PanelControl pnlManoDerecha;
        private DevExpress.XtraEditors.LabelControl lblDedosDerecha;
        private DevExpress.XtraEditors.LabelControl labelControl38;
        private DevExpress.XtraEditors.LabelControl labelControl39;
        private DevExpress.XtraEditors.PictureEdit pictureEdit13;
        private DevExpress.XtraEditors.ProgressBarControl prgbDerechoGrande;
        private DevExpress.XtraEditors.PictureEdit picSeleccionIzquierda;
        private DevExpress.XtraEditors.PanelControl pnlManoIzquierda;
        private DevExpress.XtraEditors.LabelControl lblDedosIzquierda;
        private DevExpress.XtraEditors.LabelControl labelControl36;
        private DevExpress.XtraEditors.LabelControl labelControl29;
        private DevExpress.XtraEditors.PictureEdit pictureEdit12;
        private DevExpress.XtraEditors.ProgressBarControl prgbIzquierdoGrande;
        private DevExpress.XtraEditors.LabelControl labelControl35;
        private DevExpress.XtraEditors.LabelControl labelControl34;
        private DevExpress.XtraEditors.LabelControl labelControl33;
        private DevExpress.XtraEditors.LabelControl labelControl32;
        private DevExpress.XtraEditors.LabelControl labelControl31;
        private DevExpress.XtraEditors.LabelControl lblDedo6;
        private DevExpress.XtraEditors.ProgressBarControl prgbDedo6;
        private DevExpress.XtraEditors.PictureEdit picPulgarIzquierdo;
        private DevExpress.XtraEditors.PictureEdit picMeniequeDerecho;
        private DevExpress.XtraEditors.LabelControl labelControl30;
        private DevExpress.XtraEditors.LabelControl labelControl28;
        private DevExpress.XtraEditors.LabelControl labelControl27;
        private DevExpress.XtraEditors.LabelControl labelControl26;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.PictureEdit picIndiceIzquierdo;
        private DevExpress.XtraEditors.PictureEdit picMedioIzquierdo;
        private DevExpress.XtraEditors.PictureEdit picMeniequeIzquierdo;
        private DevExpress.XtraEditors.PictureEdit picAnularIzquierdo;
        private DevExpress.XtraEditors.PictureEdit picAnularDerecho;
        private DevExpress.XtraEditors.PictureEdit picMedioDerecho;
        private DevExpress.XtraEditors.PictureEdit picPulgarDerecho;
        private DevExpress.XtraEditors.PictureEdit picManoIzquierda;
        private DevExpress.XtraEditors.PictureEdit picManoDerecha;
        private DevExpress.XtraBars.Navigation.NavigationPage navigationPage2;
        private DevExpress.XtraEditors.SimpleButton btnSalir;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.SimpleButton btnGuardar;
        private DevExpress.XtraEditors.LabelControl lblNombreFormulario;
        private DevExpress.XtraEditors.PanelControl pnlObservacionFoto;
        private DevExpress.XtraEditors.MemoEdit mmObservacionFoto;
        private System.Windows.Forms.Label label25;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.SimpleButton sbtnIniciarCamara;
        private DevExpress.XtraEditors.LabelControl lblPaisNacimiento;
        private DevExpress.XtraEditors.LabelControl labelControl132;
        private DevExpress.XtraEditors.LabelControl lblDireccionResidencia;
        private DevExpress.XtraEditors.LabelControl lblMovil;
        private DevExpress.XtraEditors.LabelControl labelControl110;
        private DevExpress.XtraEditors.LabelControl labelControl108;
        private DevExpress.XtraEditors.LabelControl lblTelefono;
        private DevExpress.XtraEditors.LabelControl lblColoniaCasa;
        private DevExpress.XtraEditors.LabelControl labelControl104;
        private DevExpress.XtraEditors.LabelControl labelControl103;
        private DevExpress.XtraEditors.LabelControl lblCorreoElectronico;
        private DevExpress.XtraEditors.LabelControl labelControl101;
        private DevExpress.XtraEditors.LabelControl lblNacionalidad;
        private DevExpress.XtraEditors.LabelControl lblNacimiento;
        private DevExpress.XtraEditors.LabelControl lblSexo;
        private DevExpress.XtraEditors.LabelControl lblIdentificacion;
        private DevExpress.XtraEditors.LabelControl lblEstadoCivil;
        private DevExpress.XtraEditors.LabelControl lblMunicipio;
        private DevExpress.XtraEditors.LabelControl lblDepartamento;
        private DevExpress.XtraEditors.LabelControl lblApellidos;
        private DevExpress.XtraEditors.LabelControl lblNombres;
        private DevExpress.XtraEditors.LabelControl lblTipoDocumento;
        private DevExpress.XtraEditors.LabelControl labelControl49;
        private DevExpress.XtraEditors.LabelControl labelControl50;
        private DevExpress.XtraEditors.LabelControl labelControl51;
        private DevExpress.XtraEditors.LabelControl labelControl52;
        private DevExpress.XtraEditors.LabelControl labelControl43;
        private DevExpress.XtraEditors.LabelControl labelControl44;
        private DevExpress.XtraEditors.LabelControl labelControl45;
        private DevExpress.XtraEditors.LabelControl labelControl46;
        private DevExpress.XtraEditors.LabelControl labelControl47;
        private DevExpress.XtraEditors.LabelControl labelControl48;
        private DevExpress.XtraEditors.PictureEdit picFotoFinal;
        private DevExpress.XtraEditors.PictureEdit picDedo6Final;
        private DevExpress.XtraEditors.PictureEdit picDedo5Final;
        private DevExpress.XtraEditors.PictureEdit picDedo7Final;
        private DevExpress.XtraEditors.PictureEdit picDedo8Final;
        private DevExpress.XtraEditors.PictureEdit picDedo10Final;
        private DevExpress.XtraEditors.PictureEdit picDedo9Final;
        private DevExpress.XtraEditors.PictureEdit picDedo4Final;
        private DevExpress.XtraEditors.PictureEdit picDedo3Final;
        private DevExpress.XtraEditors.PictureEdit picDedo1Final;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1;
        private DevExpress.XtraEditors.PanelControl pnlVerificacionHuellas;
        private DevExpress.XtraEditors.LabelControl lblGifValidando;
        private DevExpress.XtraEditors.LabelControl lblVerificarHuella;
        private DevExpress.XtraEditors.LabelControl lblEstadoValidacion;
        private DevExpress.XtraEditors.PictureEdit picCS500e;
        private DevExpress.XtraEditors.PictureEdit picCamara;
        private DevExpress.XtraEditors.LabelControl lblNivelEducativo;
        private DevExpress.XtraEditors.LabelControl labelControl135;
        private DevExpress.XtraEditors.LabelControl lblProfesionOficio;
        private DevExpress.XtraEditors.LabelControl labelControl100;
        private DevExpress.XtraEditors.PictureEdit picDedo2Final;
        private DevExpress.XtraEditors.LabelControl lblDedo2;
        private DevExpress.XtraEditors.ProgressBarControl prgbDedo2;
        private DevExpress.XtraEditors.PictureEdit picIndiceDerecho;
        private DevExpress.XtraEditors.LabelControl labelControl41;
        private DevExpress.XtraEditors.LabelControl labelControl40;
        private DevExpress.XtraEditors.LabelControl labelControl64;
        private DevExpress.XtraEditors.LabelControl labelControl63;
        private DevExpress.XtraEditors.LabelControl labelControl56;
        private DevExpress.XtraBars.Navigation.NavigationPage navigationPage5;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.MemoEdit meObservacionFirma;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.LabelControl labelControl55;
        private DevExpress.XtraEditors.SimpleButton sbtnBorrarFirma;
        private DevExpress.XtraEditors.CheckEdit chknoPuedeFirmar;
        private DevExpress.XtraEditors.PictureEdit picPadFirma;
        private DevExpress.XtraSplashScreen.SplashScreenManager ssForm;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.PictureEdit reviewSignature;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private Topaz.SigPlusNET sigPlusNET1;
        private DevExpress.XtraEditors.PictureEdit picFotoCapturada;
        private DevExpress.XtraEditors.PictureEdit picAtencion;
        private DevExpress.XtraBars.Navigation.NavigationPage tabBiograficos;
        private DevExpress.XtraEditors.MemoEdit meDireccionCompleta;
        private DevExpress.XtraEditors.TextEdit txtTelefono;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.LabelControl labelControl62;
        private DevExpress.XtraEditors.LabelControl labelControl59;
        private DevExpress.XtraEditors.LabelControl labelControl60;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.LabelControl labelControl58;
        private DevExpress.XtraEditors.LabelControl labelControl57;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LookUpEdit lueColonia;
        private DevExpress.XtraEditors.LookUpEdit lueMunicipios;
        private DevExpress.XtraEditors.LookUpEdit lueDepartamentos;
        private System.Windows.Forms.Label label28;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label16;
        private DevExpress.XtraEditors.TextEdit txtMovil;
        private DevExpress.XtraEditors.TextEdit txtCorreo;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private DevExpress.XtraEditors.LookUpEdit lueEstadoCivil;
        private DevExpress.XtraEditors.LookUpEdit lueSexo;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.LabelControl labelControl54;
        private DevExpress.XtraEditors.LabelControl labelControl53;
        private DevExpress.XtraEditors.LabelControl labelControl37;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LookUpEdit luePaisNacimiento;
        private DevExpress.XtraEditors.DateEdit dtmFechaNacimiento;
        private DevExpress.XtraEditors.LookUpEdit lueNacionalidad;
        private DevExpress.XtraEditors.LookUpEdit lueTipoDocumento;
        private DevExpress.XtraEditors.TextEdit txtIdentificacion;
        private DevExpress.XtraEditors.TextEdit txtPrimerNombre;
        private DevExpress.XtraEditors.TextEdit txtSegundoNombre;
        private DevExpress.XtraEditors.TextEdit txtPrimerApellido;
        private DevExpress.XtraEditors.TextEdit txtSegundoApellido;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.TextEdit txtCorreoTrabajo;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl61;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.LabelControl labelControl42;
        private DevExpress.XtraEditors.MemoEdit meDireccionTrabajo;
        private DevExpress.XtraEditors.TextEdit txtTelefonoTrabajo;
        private DevExpress.XtraEditors.LookUpEdit lueProfesion;
        private DevExpress.XtraEditors.LookUpEdit lueNivelEducativo;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton btnAdjuntarFoto;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.LabelControl labelControl65;
        private DevExpress.XtraEditors.LabelControl labelControl66;
        private DevExpress.XtraEditors.CheckEdit chkNoPresencial;
        private DevExpress.XtraEditors.SimpleButton btnRotar;
        private DevExpress.XtraEditors.LookUpEdit luePaisResidencia;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private System.Windows.Forms.Label label8;
        private DevExpress.XtraEditors.LabelControl labelControl68;
        private DevExpress.XtraEditors.LabelControl labelControl67;
        private DevExpress.XtraEditors.LabelControl labelControl69;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.PictureEdit pictureEdit2;
        private DevExpress.XtraEditors.LabelControl labelControl70;
        private DevExpress.XtraEditors.LabelControl labelControl72;
        private DevExpress.XtraEditors.LabelControl labelControl73;
        private DevExpress.XtraEditors.LabelControl labelControl71;
        private DevExpress.XtraEditors.SimpleButton btnSyncCatalogos;
    }
}