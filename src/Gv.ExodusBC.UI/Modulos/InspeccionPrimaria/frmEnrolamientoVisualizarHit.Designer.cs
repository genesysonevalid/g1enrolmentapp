﻿namespace Gv.ExodusBc.UI.Modulos.InspeccionPrimaria
{
    partial class frmEnrolamientoVisualizarHit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEnrolamientoVisualizarHit));
            this.btnCerrar = new DevExpress.XtraEditors.SimpleButton();
            this.label7 = new System.Windows.Forms.Label();
            this.pnlRegistroExistente = new DevExpress.XtraEditors.PanelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.lblIdentificacionExistente = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.lblNacionalidadExistente = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.lblSexoExistente = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.lblNacimientoExistente = new DevExpress.XtraEditors.LabelControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.lblApellidosExistente = new DevExpress.XtraEditors.LabelControl();
            this.lblNombresExistente = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.picRegistroExistente = new DevExpress.XtraEditors.PictureEdit();
            this.pnlRegistroActivo = new DevExpress.XtraEditors.PanelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.lblIdentificacionActivo = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.lblNacionalidadActivo = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.lblSexoActivo = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.lblNacimientoActivo = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.lblApellidosActivo = new DevExpress.XtraEditors.LabelControl();
            this.lblNombresActivo = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.picRegistroActivo = new DevExpress.XtraEditors.PictureEdit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlRegistroExistente)).BeginInit();
            this.pnlRegistroExistente.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picRegistroExistente.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlRegistroActivo)).BeginInit();
            this.pnlRegistroActivo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picRegistroActivo.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCerrar
            // 
            this.btnCerrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCerrar.Appearance.BorderColor = System.Drawing.Color.Transparent;
            this.btnCerrar.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCerrar.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(92)))), ((int)(((byte)(120)))));
            this.btnCerrar.Appearance.Options.UseBorderColor = true;
            this.btnCerrar.Appearance.Options.UseFont = true;
            this.btnCerrar.Appearance.Options.UseForeColor = true;
            this.btnCerrar.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnCerrar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCerrar.ImageOptions.Image")));
            this.btnCerrar.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnCerrar.Location = new System.Drawing.Point(644, 514);
            this.btnCerrar.LookAndFeel.SkinName = "Blue";
            this.btnCerrar.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(106, 36);
            this.btnCerrar.TabIndex = 14;
            this.btnCerrar.Text = "&Cerrar";
            this.btnCerrar.Click += new System.EventHandler(this.btnCerrar_Click);
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.LightPink;
            this.label7.Font = new System.Drawing.Font("Arial", 10F);
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(10, 509);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(628, 46);
            this.label7.TabIndex = 13;
            this.label7.Text = "Se cancelará la creación de la solicitud ya que las huellas de esta persona ya es" +
    "tán registradas y pertenecen a una persona diferente.";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlRegistroExistente
            // 
            this.pnlRegistroExistente.Appearance.BackColor = System.Drawing.Color.AliceBlue;
            this.pnlRegistroExistente.Appearance.Options.UseBackColor = true;
            this.pnlRegistroExistente.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlRegistroExistente.Controls.Add(this.labelControl14);
            this.pnlRegistroExistente.Controls.Add(this.lblIdentificacionExistente);
            this.pnlRegistroExistente.Controls.Add(this.labelControl8);
            this.pnlRegistroExistente.Controls.Add(this.labelControl9);
            this.pnlRegistroExistente.Controls.Add(this.lblNacionalidadExistente);
            this.pnlRegistroExistente.Controls.Add(this.labelControl11);
            this.pnlRegistroExistente.Controls.Add(this.lblSexoExistente);
            this.pnlRegistroExistente.Controls.Add(this.labelControl13);
            this.pnlRegistroExistente.Controls.Add(this.lblNacimientoExistente);
            this.pnlRegistroExistente.Controls.Add(this.labelControl15);
            this.pnlRegistroExistente.Controls.Add(this.lblApellidosExistente);
            this.pnlRegistroExistente.Controls.Add(this.lblNombresExistente);
            this.pnlRegistroExistente.Controls.Add(this.labelControl7);
            this.pnlRegistroExistente.Controls.Add(this.picRegistroExistente);
            this.pnlRegistroExistente.Location = new System.Drawing.Point(383, 9);
            this.pnlRegistroExistente.Name = "pnlRegistroExistente";
            this.pnlRegistroExistente.Size = new System.Drawing.Size(367, 495);
            this.pnlRegistroExistente.TabIndex = 12;
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl14.Appearance.ForeColor = System.Drawing.Color.SteelBlue;
            this.labelControl14.Appearance.Options.UseFont = true;
            this.labelControl14.Appearance.Options.UseForeColor = true;
            this.labelControl14.Appearance.Options.UseTextOptions = true;
            this.labelControl14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl14.Location = new System.Drawing.Point(24, 250);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(99, 13);
            this.labelControl14.TabIndex = 224;
            this.labelControl14.Text = "Número de identidad";
            // 
            // lblIdentificacionExistente
            // 
            this.lblIdentificacionExistente.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIdentificacionExistente.Appearance.Options.UseFont = true;
            this.lblIdentificacionExistente.Appearance.Options.UseTextOptions = true;
            this.lblIdentificacionExistente.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblIdentificacionExistente.Location = new System.Drawing.Point(24, 269);
            this.lblIdentificacionExistente.Name = "lblIdentificacionExistente";
            this.lblIdentificacionExistente.Size = new System.Drawing.Size(153, 13);
            this.lblIdentificacionExistente.TabIndex = 225;
            this.lblIdentificacionExistente.Text = "[lblIdentificacionExistente]";
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Appearance.ForeColor = System.Drawing.Color.SteelBlue;
            this.labelControl8.Appearance.Options.UseFont = true;
            this.labelControl8.Appearance.Options.UseForeColor = true;
            this.labelControl8.Appearance.Options.UseTextOptions = true;
            this.labelControl8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl8.Location = new System.Drawing.Point(24, 326);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(69, 13);
            this.labelControl8.TabIndex = 215;
            this.labelControl8.Text = "Primer apellido";
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl9.Appearance.ForeColor = System.Drawing.Color.SteelBlue;
            this.labelControl9.Appearance.Options.UseFont = true;
            this.labelControl9.Appearance.Options.UseForeColor = true;
            this.labelControl9.Appearance.Options.UseTextOptions = true;
            this.labelControl9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl9.Location = new System.Drawing.Point(24, 288);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(69, 13);
            this.labelControl9.TabIndex = 214;
            this.labelControl9.Text = "Primer nombre";
            // 
            // lblNacionalidadExistente
            // 
            this.lblNacionalidadExistente.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNacionalidadExistente.Appearance.Options.UseFont = true;
            this.lblNacionalidadExistente.Appearance.Options.UseTextOptions = true;
            this.lblNacionalidadExistente.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblNacionalidadExistente.Location = new System.Drawing.Point(24, 459);
            this.lblNacionalidadExistente.Name = "lblNacionalidadExistente";
            this.lblNacionalidadExistente.Size = new System.Drawing.Size(147, 13);
            this.lblNacionalidadExistente.TabIndex = 223;
            this.lblNacionalidadExistente.Text = "[lblNacionalidadExistente]";
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl11.Appearance.ForeColor = System.Drawing.Color.SteelBlue;
            this.labelControl11.Appearance.Options.UseFont = true;
            this.labelControl11.Appearance.Options.UseForeColor = true;
            this.labelControl11.Appearance.Options.UseTextOptions = true;
            this.labelControl11.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl11.Location = new System.Drawing.Point(24, 440);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(60, 13);
            this.labelControl11.TabIndex = 222;
            this.labelControl11.Text = "Nacionalidad";
            // 
            // lblSexoExistente
            // 
            this.lblSexoExistente.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSexoExistente.Appearance.Options.UseFont = true;
            this.lblSexoExistente.Appearance.Options.UseTextOptions = true;
            this.lblSexoExistente.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblSexoExistente.Location = new System.Drawing.Point(24, 421);
            this.lblSexoExistente.Name = "lblSexoExistente";
            this.lblSexoExistente.Size = new System.Drawing.Size(104, 13);
            this.lblSexoExistente.TabIndex = 221;
            this.lblSexoExistente.Text = "[lblSexoExistente]";
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl13.Appearance.ForeColor = System.Drawing.Color.SteelBlue;
            this.labelControl13.Appearance.Options.UseFont = true;
            this.labelControl13.Appearance.Options.UseForeColor = true;
            this.labelControl13.Appearance.Options.UseTextOptions = true;
            this.labelControl13.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl13.Location = new System.Drawing.Point(24, 402);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(24, 13);
            this.labelControl13.TabIndex = 220;
            this.labelControl13.Text = "Sexo";
            // 
            // lblNacimientoExistente
            // 
            this.lblNacimientoExistente.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNacimientoExistente.Appearance.Options.UseFont = true;
            this.lblNacimientoExistente.Appearance.Options.UseTextOptions = true;
            this.lblNacimientoExistente.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblNacimientoExistente.Location = new System.Drawing.Point(24, 383);
            this.lblNacimientoExistente.Name = "lblNacimientoExistente";
            this.lblNacimientoExistente.Size = new System.Drawing.Size(139, 13);
            this.lblNacimientoExistente.TabIndex = 219;
            this.lblNacimientoExistente.Text = "[lblNacimientoExistente]";
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl15.Appearance.ForeColor = System.Drawing.Color.SteelBlue;
            this.labelControl15.Appearance.Options.UseFont = true;
            this.labelControl15.Appearance.Options.UseForeColor = true;
            this.labelControl15.Appearance.Options.UseTextOptions = true;
            this.labelControl15.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl15.Location = new System.Drawing.Point(24, 364);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(98, 13);
            this.labelControl15.TabIndex = 218;
            this.labelControl15.Text = "Fecha de nacimiento";
            // 
            // lblApellidosExistente
            // 
            this.lblApellidosExistente.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblApellidosExistente.Appearance.Options.UseFont = true;
            this.lblApellidosExistente.Appearance.Options.UseTextOptions = true;
            this.lblApellidosExistente.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblApellidosExistente.Location = new System.Drawing.Point(24, 345);
            this.lblApellidosExistente.Name = "lblApellidosExistente";
            this.lblApellidosExistente.Size = new System.Drawing.Size(127, 13);
            this.lblApellidosExistente.TabIndex = 217;
            this.lblApellidosExistente.Text = "[lblApellidosExistente]";
            // 
            // lblNombresExistente
            // 
            this.lblNombresExistente.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombresExistente.Appearance.Options.UseFont = true;
            this.lblNombresExistente.Appearance.Options.UseTextOptions = true;
            this.lblNombresExistente.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblNombresExistente.Location = new System.Drawing.Point(24, 307);
            this.lblNombresExistente.Name = "lblNombresExistente";
            this.lblNombresExistente.Size = new System.Drawing.Size(115, 13);
            this.lblNombresExistente.TabIndex = 216;
            this.lblNombresExistente.Text = "lblNombreExistente]";
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.BackColor = System.Drawing.Color.LightSteelBlue;
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelControl7.Appearance.Options.UseBackColor = true;
            this.labelControl7.Appearance.Options.UseFont = true;
            this.labelControl7.Appearance.Options.UseImage = true;
            this.labelControl7.Appearance.Options.UseImageAlign = true;
            this.labelControl7.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl7.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelControl7.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControl7.Location = new System.Drawing.Point(0, 0);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.labelControl7.Size = new System.Drawing.Size(367, 36);
            this.labelControl7.TabIndex = 203;
            this.labelControl7.Text = "Persona ya registrada";
            // 
            // picRegistroExistente
            // 
            this.picRegistroExistente.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("picRegistroExistente.BackgroundImage")));
            this.picRegistroExistente.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picRegistroExistente.Cursor = System.Windows.Forms.Cursors.Default;
            this.picRegistroExistente.Location = new System.Drawing.Point(90, 41);
            this.picRegistroExistente.Name = "picRegistroExistente";
            this.picRegistroExistente.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picRegistroExistente.Properties.Appearance.BorderColor = System.Drawing.Color.SteelBlue;
            this.picRegistroExistente.Properties.Appearance.Options.UseBackColor = true;
            this.picRegistroExistente.Properties.Appearance.Options.UseBorderColor = true;
            this.picRegistroExistente.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.picRegistroExistente.Properties.NullText = " ";
            this.picRegistroExistente.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picRegistroExistente.Properties.ShowMenu = false;
            this.picRegistroExistente.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.picRegistroExistente.Size = new System.Drawing.Size(186, 205);
            this.picRegistroExistente.TabIndex = 200;
            // 
            // pnlRegistroActivo
            // 
            this.pnlRegistroActivo.Appearance.BackColor = System.Drawing.Color.AntiqueWhite;
            this.pnlRegistroActivo.Appearance.Options.UseBackColor = true;
            this.pnlRegistroActivo.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlRegistroActivo.Controls.Add(this.labelControl1);
            this.pnlRegistroActivo.Controls.Add(this.lblIdentificacionActivo);
            this.pnlRegistroActivo.Controls.Add(this.labelControl12);
            this.pnlRegistroActivo.Controls.Add(this.labelControl3);
            this.pnlRegistroActivo.Controls.Add(this.lblNacionalidadActivo);
            this.pnlRegistroActivo.Controls.Add(this.labelControl6);
            this.pnlRegistroActivo.Controls.Add(this.lblSexoActivo);
            this.pnlRegistroActivo.Controls.Add(this.labelControl5);
            this.pnlRegistroActivo.Controls.Add(this.lblNacimientoActivo);
            this.pnlRegistroActivo.Controls.Add(this.labelControl4);
            this.pnlRegistroActivo.Controls.Add(this.lblApellidosActivo);
            this.pnlRegistroActivo.Controls.Add(this.lblNombresActivo);
            this.pnlRegistroActivo.Controls.Add(this.labelControl2);
            this.pnlRegistroActivo.Controls.Add(this.picRegistroActivo);
            this.pnlRegistroActivo.Location = new System.Drawing.Point(10, 9);
            this.pnlRegistroActivo.Name = "pnlRegistroActivo";
            this.pnlRegistroActivo.Size = new System.Drawing.Size(367, 495);
            this.pnlRegistroActivo.TabIndex = 11;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Appearance.Options.UseForeColor = true;
            this.labelControl1.Appearance.Options.UseTextOptions = true;
            this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl1.Location = new System.Drawing.Point(17, 288);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(69, 13);
            this.labelControl1.TabIndex = 200;
            this.labelControl1.Text = "Primer nombre";
            // 
            // lblIdentificacionActivo
            // 
            this.lblIdentificacionActivo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIdentificacionActivo.Appearance.Options.UseFont = true;
            this.lblIdentificacionActivo.Appearance.Options.UseTextOptions = true;
            this.lblIdentificacionActivo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblIdentificacionActivo.Location = new System.Drawing.Point(17, 269);
            this.lblIdentificacionActivo.Name = "lblIdentificacionActivo";
            this.lblIdentificacionActivo.Size = new System.Drawing.Size(136, 13);
            this.lblIdentificacionActivo.TabIndex = 214;
            this.lblIdentificacionActivo.Text = "[lblIdentificacionActivo]";
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl12.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.labelControl12.Appearance.Options.UseFont = true;
            this.labelControl12.Appearance.Options.UseForeColor = true;
            this.labelControl12.Appearance.Options.UseTextOptions = true;
            this.labelControl12.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl12.Location = new System.Drawing.Point(17, 250);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(99, 13);
            this.labelControl12.TabIndex = 213;
            this.labelControl12.Text = "Número de identidad";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Appearance.Options.UseForeColor = true;
            this.labelControl3.Appearance.Options.UseTextOptions = true;
            this.labelControl3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl3.Location = new System.Drawing.Point(17, 326);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(69, 13);
            this.labelControl3.TabIndex = 204;
            this.labelControl3.Text = "Primer apellido";
            // 
            // lblNacionalidadActivo
            // 
            this.lblNacionalidadActivo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNacionalidadActivo.Appearance.Options.UseFont = true;
            this.lblNacionalidadActivo.Appearance.Options.UseTextOptions = true;
            this.lblNacionalidadActivo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblNacionalidadActivo.Location = new System.Drawing.Point(17, 459);
            this.lblNacionalidadActivo.Name = "lblNacionalidadActivo";
            this.lblNacionalidadActivo.Size = new System.Drawing.Size(130, 13);
            this.lblNacionalidadActivo.TabIndex = 212;
            this.lblNacionalidadActivo.Text = "[lblNacionalidadActivo]";
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.labelControl6.Appearance.Options.UseFont = true;
            this.labelControl6.Appearance.Options.UseForeColor = true;
            this.labelControl6.Appearance.Options.UseTextOptions = true;
            this.labelControl6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl6.Location = new System.Drawing.Point(17, 440);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(60, 13);
            this.labelControl6.TabIndex = 211;
            this.labelControl6.Text = "Nacionalidad";
            // 
            // lblSexoActivo
            // 
            this.lblSexoActivo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSexoActivo.Appearance.Options.UseFont = true;
            this.lblSexoActivo.Appearance.Options.UseTextOptions = true;
            this.lblSexoActivo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblSexoActivo.Location = new System.Drawing.Point(17, 421);
            this.lblSexoActivo.Name = "lblSexoActivo";
            this.lblSexoActivo.Size = new System.Drawing.Size(87, 13);
            this.lblSexoActivo.TabIndex = 210;
            this.lblSexoActivo.Text = "[lblSexoActivo]";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Appearance.Options.UseForeColor = true;
            this.labelControl5.Appearance.Options.UseTextOptions = true;
            this.labelControl5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl5.Location = new System.Drawing.Point(17, 402);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(24, 13);
            this.labelControl5.TabIndex = 209;
            this.labelControl5.Text = "Sexo";
            // 
            // lblNacimientoActivo
            // 
            this.lblNacimientoActivo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNacimientoActivo.Appearance.Options.UseFont = true;
            this.lblNacimientoActivo.Appearance.Options.UseTextOptions = true;
            this.lblNacimientoActivo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblNacimientoActivo.Location = new System.Drawing.Point(17, 383);
            this.lblNacimientoActivo.Name = "lblNacimientoActivo";
            this.lblNacimientoActivo.Size = new System.Drawing.Size(122, 13);
            this.lblNacimientoActivo.TabIndex = 208;
            this.lblNacimientoActivo.Text = "[lblNacimientoActivo]";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Appearance.Options.UseForeColor = true;
            this.labelControl4.Appearance.Options.UseTextOptions = true;
            this.labelControl4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl4.Location = new System.Drawing.Point(17, 364);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(98, 13);
            this.labelControl4.TabIndex = 207;
            this.labelControl4.Text = "Fecha de nacimiento";
            // 
            // lblApellidosActivo
            // 
            this.lblApellidosActivo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblApellidosActivo.Appearance.Options.UseFont = true;
            this.lblApellidosActivo.Appearance.Options.UseTextOptions = true;
            this.lblApellidosActivo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblApellidosActivo.Location = new System.Drawing.Point(17, 345);
            this.lblApellidosActivo.Name = "lblApellidosActivo";
            this.lblApellidosActivo.Size = new System.Drawing.Size(110, 13);
            this.lblApellidosActivo.TabIndex = 206;
            this.lblApellidosActivo.Text = "[lblApellidosActivo]";
            // 
            // lblNombresActivo
            // 
            this.lblNombresActivo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombresActivo.Appearance.Options.UseFont = true;
            this.lblNombresActivo.Appearance.Options.UseTextOptions = true;
            this.lblNombresActivo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblNombresActivo.Location = new System.Drawing.Point(17, 307);
            this.lblNombresActivo.Name = "lblNombresActivo";
            this.lblNombresActivo.Size = new System.Drawing.Size(109, 13);
            this.lblNombresActivo.TabIndex = 205;
            this.lblNombresActivo.Text = "[lblNombresActivo]";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelControl2.Appearance.Options.UseBackColor = true;
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Appearance.Options.UseImage = true;
            this.labelControl2.Appearance.Options.UseImageAlign = true;
            this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelControl2.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControl2.Location = new System.Drawing.Point(0, 0);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.labelControl2.Size = new System.Drawing.Size(367, 36);
            this.labelControl2.TabIndex = 202;
            this.labelControl2.Text = "Persona nueva";
            // 
            // picRegistroActivo
            // 
            this.picRegistroActivo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("picRegistroActivo.BackgroundImage")));
            this.picRegistroActivo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picRegistroActivo.Cursor = System.Windows.Forms.Cursors.Default;
            this.picRegistroActivo.Location = new System.Drawing.Point(90, 41);
            this.picRegistroActivo.Name = "picRegistroActivo";
            this.picRegistroActivo.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picRegistroActivo.Properties.Appearance.BorderColor = System.Drawing.Color.SteelBlue;
            this.picRegistroActivo.Properties.Appearance.Options.UseBackColor = true;
            this.picRegistroActivo.Properties.Appearance.Options.UseBorderColor = true;
            this.picRegistroActivo.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.picRegistroActivo.Properties.NullText = " ";
            this.picRegistroActivo.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picRegistroActivo.Properties.ShowMenu = false;
            this.picRegistroActivo.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.picRegistroActivo.Size = new System.Drawing.Size(186, 205);
            this.picRegistroActivo.TabIndex = 199;
            // 
            // frmEnrolamientoVisualizarHit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(761, 564);
            this.Controls.Add(this.btnCerrar);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.pnlRegistroExistente);
            this.Controls.Add(this.pnlRegistroActivo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmEnrolamientoVisualizarHit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Hit en huellas";
            this.Load += new System.EventHandler(this.frmEnrolamientoVisualizarHit_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pnlRegistroExistente)).EndInit();
            this.pnlRegistroExistente.ResumeLayout(false);
            this.pnlRegistroExistente.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picRegistroExistente.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlRegistroActivo)).EndInit();
            this.pnlRegistroActivo.ResumeLayout(false);
            this.pnlRegistroActivo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picRegistroActivo.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnCerrar;
        private System.Windows.Forms.Label label7;
        private DevExpress.XtraEditors.PanelControl pnlRegistroExistente;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.LabelControl lblIdentificacionExistente;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl lblNacionalidadExistente;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl lblSexoExistente;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl lblNacimientoExistente;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.LabelControl lblApellidosExistente;
        private DevExpress.XtraEditors.LabelControl lblNombresExistente;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.PictureEdit picRegistroExistente;
        private DevExpress.XtraEditors.PanelControl pnlRegistroActivo;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl lblIdentificacionActivo;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl lblNacionalidadActivo;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl lblSexoActivo;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl lblNacimientoActivo;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl lblApellidosActivo;
        private DevExpress.XtraEditors.LabelControl lblNombresActivo;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.PictureEdit picRegistroActivo;
    }
}