﻿using DevExpress.XtraEditors;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Gv.ExodusBc.UI.ExodusBcBase;

namespace Gv.ExodusBc.UI.Modulos.InspeccionPrimaria
{
    public partial class frmHitIdentidad : DevExpress.XtraEditors.XtraForm
    {

        public static long PersonaID { get; set; }

        public static JArray arPersona = null;


        public frmHitIdentidad()
        {
            InitializeComponent();
        }

        private void frmHitIdentidad_Load(object sender, EventArgs e)
        {
            foreach (JObject element in arPersona)
            {
                lblIdentificacionExistente.Text = element["PersonId"].ToString();
                lblNombresExistente.Text = string.Format("{0} {1}", element["PersonId"].ToString(), element["PersonId"].ToString());
                lblApellidosExistente.Text = string.Format("{0} {1}", element["FirstName"].ToString(), element["FirstLastName"].ToString());
                lblNacimientoExistente.Text = string.Format("{0:dd/MM/yyyy}", element["FechaNacimiento"].ToString());
                lblSexoExistente.Text = (element["FKSexId"].ToString() == "M") ? "MASCULINO" : lblSexoExistente.Text = "FEMENNO";

                JArray arrayFoto = new JArray();
                arrayFoto = RequestCertificate.GetLastPhoto(Convert.ToInt32(element["PersonId"].ToString()));
                foreach (JObject elementPhoto in arrayFoto)
                    if (elementPhoto["Image"].ToString() != null)
                    {
                        picIdentidad.EditValue = ExodusBcBase.Helper.byteToImage((byte[])elementPhoto["Image"]);
                    }
            }
        }
    }
}