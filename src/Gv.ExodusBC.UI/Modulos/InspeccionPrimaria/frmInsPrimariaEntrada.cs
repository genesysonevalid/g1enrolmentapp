﻿using System;
using System.Windows.Forms;
using System.Diagnostics;
using Gv.ExodusBc.UI.Properties;
using DevExpress.XtraEditors;
using System.Text;
using System.Collections.Generic;
using Gv.ExodusBc.LN;
using DevExpress.XtraRichEdit.API.RichTextBox;
using System.Drawing;
using System.IO;
using Cogent.Biometrics;
using Gv.Logitech.SDK;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.Images;
using DevExpress.Utils.Menu;
using AForge.Video.DirectShow;
using Newtonsoft.Json.Linq;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using GV.CameraAPI;
using System.Text.RegularExpressions;
using System.Net;
using System.Net.Sockets;
using static Gv.ExodusBc.UI.ExodusBcBase;
using Gv.ExodusBc.EN;
using Gv.Utilidades;
using DevExpress.XtraEditors.Mask;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using Log = Gv.Utilidades.Log;
using static Gv.ExodusBc.UI.ExodusBcBase.RequestCertificate;
using System.Threading.Tasks;

namespace Gv.ExodusBc.UI.Modulos.InspeccionPrimaria
{
    public partial class frmInsPrimariaEntrada : XtraForm
    {

        #region Variables_Formulario_Tabs

        bool estadoBiograficos = false;
        bool estadoHuellas = false;
        bool estadoFoto = false;
        bool estadoFirma = false;
        bool estadoOtros = false;
        int contador = 10;
        int intentos = 0;
        private bool validacionCompletada;
        AfisTransaccion afisRes = new AfisTransaccion();
        WsAFIS.NistService wsAfis = new WsAFIS.NistService();
        string PERSONA_ID_HIT;
        private bool HitenVivo;
        private bool ConsultaAfis;
        private int estadoNist;
        string TransaccionAfis = String.Empty;
        int formatoImagen = 0;
        string ValoresIcao = string.Empty;
        int TotalErroresIcao = 0;
        List<clsDocumento> Documentos = new List<clsDocumento>();
        string Dispositivo = null;
        int DPI = 0;
        //Twain
        GdPicture.GdPictureImaging gdTwain = new GdPicture.GdPictureImaging();
        public int TotalHuellasCapturadas { get; set; }
        int NumeroDocumentoTwain = 0;
        Image imgDoc = default(Image);
        int idDocumento = 0;
        //public static CS500E cs500e = new CS500E();
        int cs500eInicializado = -1; //-1=No, 0=Si
        public static FilterInfoCollection DispositivosDeVideo;
        public static List<CamarasDisponibles> lstCamaras;
        private bool estadoAdjuntos;
        private EN.EnrolmentPersons _enrolmentPersons = new EN.EnrolmentPersons();
        private EN.Enrolments _enrolments = new EN.Enrolments();
        private EN.EnrolmentPersonPictures _enrolmentPersonPictures = new EN.EnrolmentPersonPictures();
        private EN.EnrolmentSignature _enrolmentSignature = new EN.EnrolmentSignature();
        private WebReference.ExodusBcService ws = null;
        private string host = "";
        Image firmaCapturada = null;
        bool ExistePadFirmas;
        Camera camara = new Camera();
        bool omitirHuellas;
        #endregion

        #region Variables Globales
        StringBuilder directorio = new StringBuilder();
        StringBuilder numeroTransaccion = new StringBuilder();
        List<string> valoresCalidad = new List<string>();
        List<string> accesoTemporalWSQ = new List<string>();
        public delegate void SetLabelCallBack(LabelControl lbx, string texto, string tipo);
        public int _RegistroPersonas = 0;
        string correlativoConsulta = String.Empty;
        int transaccion;
        int captura;

        int imagenesSegmentadas;
        int calidadHuella;
        int guardar;
        int limpiarImagenesScanner;
        bool mensaje;
        string[] accesoTemporal;
        string[] AccesoTemporal2;
        const int dedos = 10;
        int totalDedos = 0;
        //bool sinHuellas = false;
        int RevisionPersona = -1;
        bool procesoValidacionIniciado;
        bool nistEnviado;
        private bool nistGenerado;
        string numeroConsultaAfis = "";
        ExodusBcBase.ClasesPersonalizadas.HuellasCapturar personaEnrolarHuellasCapturadas = null;
        string HitXML = string.Empty;
        AxCls cs500e = null;

        public static string ProcesadorMaquinaActual = String.Empty;
        System.Windows.Forms.Timer tmr1aN = new System.Windows.Forms.Timer();
        System.Windows.Forms.Timer tmr1a1 = new System.Windows.Forms.Timer();
        private enum ValidacionAfis
        {
            NO_INICIADO = 0,
            ENVIANDO_NIST = 1,
            NIST_ENVIADO = 2,
            ERROR_ENVIO_NIST = 3,
            ESPERANDO_RESPUESTA = 4,
            COMPLETADO = 5,
            TIMEOUT = 6
        }
        ValidacionAfis ValidacionHuellas = ValidacionAfis.NO_INICIADO;
        #endregion

        #region Variables_Formulario_Adjuntos


        public string AdjuntoTipo = String.Empty;
        public byte[] Archivo; 
        bool ExisteCamara;
        public static string NoIdentidad = "";
        public static Int64 TipoDocumento = 0;


        #endregion
        public frmInsPrimariaEntrada()
        {

            DialogResult resultado;

            using (frmBusquedaPersonas frmbusqueda = new frmBusquedaPersonas()) 
                resultado = frmbusqueda.ShowDialog();

            if (resultado == DialogResult.Abort || resultado == DialogResult.Cancel)
            {
                this.DialogResult = DialogResult.Cancel;
                this.Close();
                this.Dispose();
            }
            else
            {
                InitializeComponent();
                Init();

                comprobarEscaner();
                picPulgarDerecho.Click += new EventHandler(setHuellasZoom);
                picIndiceDerecho.Click += new EventHandler(setHuellasZoom);
                picMedioDerecho.Click += new EventHandler(setHuellasZoom);
                picAnularDerecho.Click += new EventHandler(setHuellasZoom);
                picMeniequeDerecho.Click += new EventHandler(setHuellasZoom);
                picPulgarIzquierdo.Click += new EventHandler(setHuellasZoom);
                picIndiceIzquierdo.Click += new EventHandler(setHuellasZoom);
                picMedioIzquierdo.Click += new EventHandler(setHuellasZoom);
                picAnularIzquierdo.Click += new EventHandler(setHuellasZoom);
                picMeniequeIzquierdo.Click += new EventHandler(setHuellasZoom);

                personaEnrolarHuellasCapturadas = new ClasesPersonalizadas.HuellasCapturar() { TotalDedos = 0 };

                ws = new WebReference.ExodusBcService();
                cs500e = new AxCls();

#if DEBUG
                //simpleButton1.Visible = true;
#endif
            }

        }

        private void setHuellasZoom(object sender, EventArgs e)
        {
            PictureEdit pic = (PictureEdit)sender;

            if (pic != null)
            {
                if (pic.Image != null)
                {
                    //derecho: 1,2,3,4,5  || izquierda: 6,7,8,9,10
                    if (pic.ToolTip.ToLower().Contains("derecho"))
                    {
                        pnlManoDerecha.Visible = true;
                        Utilities.PrepararProgressBar(prgbDerechoGrande, new LabelControl(), Convert.ToInt32(pic.Tag));
                        picSeleccionDerecha.Image = pic.Image;
                        lblDedosDerecha.Text = pic.ToolTip.Replace("Derecho", "").Trim();
                    }
                    else
                    {
                        pnlManoIzquierda.Visible = true;
                        Utilities.PrepararProgressBar(prgbIzquierdoGrande, new LabelControl(), Convert.ToInt32(pic.Tag));
                        picSeleccionIzquierda.Image = pic.Image;
                        lblDedosIzquierda.Text = pic.ToolTip.Replace("Izquierdo", "").Trim();

                    }
                }
                else
                {
                    if (pic.ToolTip.ToLower().Contains("derecho"))
                    {
                        pnlManoDerecha.Visible = false;
                        picSeleccionDerecha.Image = Resources.Cancel01;
                    }
                    else
                    {
                        pnlManoIzquierda.Visible = false;
                        picSeleccionIzquierda.Image = Resources.Cancel01;
                    }
                }
            }
        }

        void tmr1aN_Tick(object sender, EventArgs e)
        {
            btnSalir.Enabled = false;
            lblGifValidando.Visible = true;

            lblEstadoValidacion.Visible = true;
            SetLabelText(lblEstadoValidacion, "Empaquetando el archivo NIST ...", "I");
            if (!enviarNist())
            {
                tmr1aN.Stop();
                lblEstadoValidacion.Font = new System.Drawing.Font(lblEstadoValidacion.Font, FontStyle.Bold);
                lblEstadoValidacion.Font = new System.Drawing.Font(lblEstadoValidacion.Font, FontStyle.Bold);
                SetLabelText(lblEstadoValidacion, "Error al enviar el archivo NIST ", "A");
                btnSalir.Enabled = true;
                //btnGuardar.Enabled = true;
                lblGifValidando.Visible = false;
                return;
            }

            if (intentos == 12)
            {
                contador = 0;
                intentos = 0;
                tmr1aN.Stop();
                ValidacionHuellas = ValidacionAfis.TIMEOUT;
                btnSalir.Enabled = true;
                lblEstadoValidacion.Font = new System.Drawing.Font(lblEstadoValidacion.Font, FontStyle.Bold);
                SetLabelText(lblEstadoValidacion, "Tiempo de espera agotado, no se pueden validar las huellas dactilares", "A");
                //sbtnGuardar.Enabled = true;
                lblGifValidando.Visible = false;
                return;
            }

            if (contador == 10)
            {
                AfisTransaccion at = ValidarHuellaAfis(numeroConsultaAfis);

                if (at.Status == "STATE_FINAL")
                {
                    intentos = 0;
                    contador = 0;
                    tmr1aN.Stop();
                    ValidacionHuellas = ValidacionAfis.COMPLETADO;
                    //VERIFICAR SI HUBO HIT O NO
                    if (at.ConfirmStatus == "Y")
                    {
                        try
                        {
                            PERSONA_ID_HIT = at.CAN_TCN;
                            lblEstadoValidacion.ForeColor = Color.Red;
                            lblEstadoValidacion.Font = new System.Drawing.Font(lblEstadoValidacion.Font, FontStyle.Bold);
                            SetLabelText(lblEstadoValidacion, "HIT, Huellas ya se encuentran registradas", "A");
                            lblGifValidando.Visible = false;


                            //Mostrar lado a lado
                            InsertarHit();
                            frmEnrolamientoVisualizarHit frm = new frmEnrolamientoVisualizarHit();
                            frm.PersonaId = Convert.ToInt32(PERSONA_ID_HIT);
                            frm.NoIdentidad = txtIdentificacion.Text;
                            frm.Nombre = (txtPrimerNombre.Text + " " + txtSegundoNombre.Text).Trim().ToUpper();
                            frm.Apellido = (txtPrimerApellido.Text + " " + txtSegundoApellido.Text).Trim().ToUpper();
                            frm.FechaNacimiento = Convert.ToDateTime(dtmFechaNacimiento.EditValue);
                            frm.Sexo = lueSexo.Text.Trim().ToUpper();
                            frm.Foto = picFotoCapturada.Image;
                            frm.ShowDialog();
                            btnGuardar.Enabled = false;
                            sbtnSiguiente.Enabled = false;
                            sbtnAnterior.Enabled = false;
                            btnSalir.Enabled = true;



                        }
                        catch (Exception ex)
                        {
                            Utilidades.Log.InsertarLog(Utilidades.Log.ErrorType.Error, "frmInsPrimariaEntrada:tmr1aN_Tick", Utilidades.Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                        }

                    }
                    else
                    {
                        //sbtnGuardar.Enabled = true;
                        SetLabelText(lblEstadoValidacion, "Registro de nueva persona", "E");
                        lblGifValidando.Visible = false;
                        btnSalir.Enabled = true;

                       
                        return;
                    }

                }
                else
                {
                    contador = 0;
                    intentos++;
                    SetLabelText(lblEstadoValidacion, "Comparando Huellas..." + intentos, "I");
                }
            }
            else
            {
                SetLabelText(lblEstadoValidacion, "Comparando Huellas...", "I");
                contador++;
            }

        }

        void tmr1a1_Tick(object sender, EventArgs e)
        {
            btnSalir.Enabled = false;
            lblGifValidando.Visible = true;

            lblEstadoValidacion.Visible = true;
            SetLabelText(lblEstadoValidacion, "Empaquetando el archivo NIST ...", "I");
            Utilidades.Log.InsertarLog(Utilidades.Log.ErrorType.Informacion, "Enrolamiento::tmr1a1_Tick", "Empaquetando Nist", VariablesGlobales.PathDataLog);
            if (!enviarNist())
            {
                lblEstadoValidacion.Font = new System.Drawing.Font(lblEstadoValidacion.Font, FontStyle.Bold);
                tmr1a1.Stop();
                lblEstadoValidacion.Font = new System.Drawing.Font(lblEstadoValidacion.Font, FontStyle.Bold);
                SetLabelText(lblEstadoValidacion, "Error al enviar el archivo NIST ", "A");
                Utilidades.Log.InsertarLog(Utilidades.Log.ErrorType.Error, "Enrolamiento::tmr1a1_Tick", "Error al enviar Nist", VariablesGlobales.PathDataLog);
                btnGuardar.Enabled = true;
                btnSalir.Enabled = true;
                lblGifValidando.Visible = false;
            }

            if (intentos == 12)
            {
                contador = 0;
                intentos = 0;
                tmr1a1.Stop();
                ValidacionHuellas = ValidacionAfis.TIMEOUT;
                btnSalir.Enabled = true;
                lblEstadoValidacion.Font = new System.Drawing.Font(lblEstadoValidacion.Font, FontStyle.Bold);
                SetLabelText(lblEstadoValidacion, "Tiempo de espera agotado, no se pueden validar las huellas dactilares", "A");
                Utilidades.Log.InsertarLog(Utilidades.Log.ErrorType.Error, "Enrolamiento::tmr1a1_Tick", "Tiempo de espera agotado, no se pueden validar las huellas dactilares", VariablesGlobales.PathDataLog);
                btnGuardar.Enabled = true;
                btnSalir.Enabled = true;
                lblGifValidando.Visible = false;
                return;
            }

            if (contador == 10)
            {
                AfisTransaccion at = ValidarHuellaAfis(numeroConsultaAfis);

                if (at.Status == "STATE_FINAL")
                {
                    intentos = 0;
                    contador = 0;
                    tmr1a1.Stop();
                    ValidacionHuellas = ValidacionAfis.COMPLETADO;

                    //VERIFICAR SI HUBO HIT O NO
                    if (at.ConfirmStatus == "Y")
                    {
                        try
                        {
                            //if (at.CAN_TCN == ultSolicitud.FKPersonaId + "")
                            //{
                            //    //Validación exitosa
                            //    SetLabelText(lblEstadoValidacion, "Identidad confirmada", "E");
                            //    lblGifValidando.Visible = false;
                            //    btnCancelarTransacccion.Enabled = true;
                            //    //para activar automaticamente el botón de guardar si se encuentra en el ultimo tab
                            //    if (tabCaptura.SelectedPageIndex == 6)
                            //        btnGuardar.Enabled = true;

                            //    //Mostrar frm de identidad confirmada
                            //    frmEnrolamientoValidacionOK frm = new frmEnrolamientoValidacionOK();
                            //    frm.ShowDialog();

                            //}
                            //else
                            //{
                            //    //Validación fallida
                            //    SetLabelText(lblEstadoValidacion, "No HIT, Identidad no confirmada", "A");
                            //    lblGifValidando.Visible = false;

                            //    //Show form identidad fallida
                            //    frmEnrolamientoValidacionFallida frm = new frmEnrolamientoValidacionFallida();
                            //    frm.ShowDialog();
                            //    iniciarTramite();
                            //}

                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }

                    }
                    else
                    {
                        //Validación fallida
                        SetLabelText(lblEstadoValidacion, "No HIT, Identidad no confirmada", "A");
                        Utilidades.Log.InsertarLog(Utilidades.Log.ErrorType.Error, "Enrolamiento::tmr1a1_Tick", "No HIT, Identidad no confirmada", VariablesGlobales.PathDataLog);
                        lblGifValidando.Visible = false;

                        //Show form identidad fallida
                        //frmEnrolamientoValidacionFallida frm = new frmEnrolamientoValidacionFallida();
                        //frm.ShowDialog();
                     

                        return;
                    }

                }
                else
                {
                    contador = 0;
                    intentos++;
                    SetLabelText(lblEstadoValidacion, "Comparando Huellas..." + intentos, "I");
                }

            }
            else
            {
                SetLabelText(lblEstadoValidacion, "Comparando Huellas...", "I");
                contador++;
            }

        }

        bool enviarNist()
        {
            //Envio Nist
            try
            {
                SetLabelText(lblEstadoValidacion, "Sending NIST file to AFIS...", "I");
                Utilidades.Log.InsertarLog(Utilidades.Log.ErrorType.Error, "Enrolamiento::enviarNist", "Enviando Archivo Nist al Afis", VariablesGlobales.PathDataLog);

                string NombreArchivo = string.Format(@"{0}\{1}.tdf", Path.Combine(VariablesGlobales.PathWSQ, lblNumeroTransaccion.Text, "FID"), numeroConsultaAfis);
                FileInfo forigen = new FileInfo(NombreArchivo);
                byte[] barc;
                barc = File.ReadAllBytes(NombreArchivo);
                string base64 = Convert.ToBase64String(barc);
                int respuestaEnvioCafis = wsAfis.UploadNist(NombreArchivo, base64);

                if (respuestaEnvioCafis == 0)
                {
                    ValidacionHuellas = ValidacionAfis.NIST_ENVIADO;
                    nistEnviado = true;
                    return true;
                }
                else
                {
                    ValidacionHuellas = ValidacionAfis.ERROR_ENVIO_NIST;
                    nistEnviado = false;
                    return false;
                }
            }
            catch (Exception ex)
            {
                ValidacionHuellas = ValidacionAfis.ERROR_ENVIO_NIST;
                nistEnviado = false;
                Utilidades.Log.InsertarLog(Utilidades.Log.ErrorType.Error, "frmBusquedaPersonas::enviarNist", Utilidades.Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                return false;

            }
        }

        void cargarInfoLeida()
        {

            if (frmEnrolamientoLectora.objLectura != null)
            {

                //**LLENAR CONTROLES**
                //***Identidad***
                if (frmEnrolamientoLectora.objLectura.NoIdentidad != null)
                {
                    lueTipoDocumento.EditValue = Convert.ToInt64(1);
                    txtIdentificacion.Text = frmEnrolamientoLectora.objLectura.NoIdentidad;
                }
                //***Nombres***
                var _primerNombre = Regex.Replace(frmEnrolamientoLectora.objLectura.Nombre1, "Ñ", "N");
                txtPrimerNombre.Text = _primerNombre;
                var _primerSegundoNombre = Regex.Replace(frmEnrolamientoLectora.objLectura.Nombre2, "Ñ", "N");
                txtSegundoNombre.Text = _primerSegundoNombre;
                //***Apellidos***
                if (frmEnrolamientoLectora.objLectura.Apellido1.Contains("Ñ"))
                {
                    var _primerApellido = Regex.Replace(frmEnrolamientoLectora.objLectura.Apellido1, "Ñ", "N");
                    txtPrimerApellido.Text = _primerApellido;
                }
                else if (frmEnrolamientoLectora.objLectura.Apellido1.Contains("¥"))
                {
                    var _primerApellido = Regex.Replace(frmEnrolamientoLectora.objLectura.Apellido1, "¥", "N");
                    txtPrimerApellido.Text = _primerApellido;
                }
                else
                {
                    txtPrimerApellido.Text = frmEnrolamientoLectora.objLectura.Apellido1;
                }
                if (frmEnrolamientoLectora.objLectura.Apellido2.Contains("Ñ"))
                {
                    var _segundoApellido = Regex.Replace(frmEnrolamientoLectora.objLectura.Apellido2, "Ñ", "N");
                    txtSegundoApellido.Text = _segundoApellido;
                }
                else if (frmEnrolamientoLectora.objLectura.Apellido2.Contains("¥"))
                {
                    var _segundoApellido = Regex.Replace(frmEnrolamientoLectora.objLectura.Apellido2, "¥", "N");
                    txtSegundoApellido.Text = _segundoApellido;
                }
                else
                {
                    txtSegundoApellido.Text = frmEnrolamientoLectora.objLectura.Apellido2;
                }

                //***Nacimiento***
                luePaisNacimiento.EditValue = Convert.ToInt64(102); 
                lueDepartamentos.EditValue = Convert.ToInt64(frmEnrolamientoLectora.objLectura.Departamento);
                lueMunicipios.EditValue = Convert.ToInt64(frmEnrolamientoLectora.objLectura.Municipio);
                dtmFechaNacimiento.EditValue = frmEnrolamientoLectora.objLectura.FechaNacimiento;
                lueSexo.Select();
            }
        }

        private void Init()
        {
            //Llenar combos
            ExodusBcBase.Helper.Combobox.setCombo(lueSexo, Convert.ToInt32(LN.LISTCATALOGSTYPES.ListTypesCatalogs.Sexo));
            ExodusBcBase.Helper.Combobox.setComboTipoDocumento(lueTipoDocumento);
            ExodusBcBase.Helper.Combobox.setCombo(lueEstadoCivil, Convert.ToInt32(LN.LISTCATALOGSTYPES.ListTypesCatalogs.EstadoCivil));
            ExodusBcBase.Helper.Combobox.setComboProfesiones(lueProfesion);
            ExodusBcBase.Helper.Combobox.setCombo(lueNivelEducativo, Convert.ToInt32(LN.LISTCATALOGSTYPES.ListTypesCatalogs.NivelEducativo));
            ExodusBcBase.Helper.Combobox.setComboCountry(luePaisNacimiento);
            ExodusBcBase.Helper.Combobox.setComboCountry(luePaisResidencia);

            ExodusBcBase.Helper.Combobox.setComboNationality(lueNacionalidad);
            ExodusBcBase.Helper.Combobox.setComboStateCountry(lueDepartamentos);
        }

        private void frmInsPrimariaEntrada_Load(object sender, EventArgs e)
        {
            try
            {
                ssForm.ShowWaitForm();
                //VariablesGlobales.Inicializar();
                SetControles();
                cargarInfoLeida();
                comprobarCamara();
                comprobarPad();

                lueTipoDocumento.EditValue = Convert.ToInt64(TipoDocumento);
                txtIdentificacion.Text = NoIdentidad;
                luePaisNacimiento.EditValue = VariablesGlobales.COUNTRY_DEFAULT;
                lueNacionalidad.EditValue = VariablesGlobales.NATIONALITY_DEFAULT;
                txtPrimerNombre.Focus();
                luePaisResidencia.EditValue = VariablesGlobales.COUNTRYRESIDENCE_DEFAULT;
                ssForm.CloseWaitForm();
            }
            catch (Exception ex)
            {
                if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                Utilidades.Log.InsertarLog(Utilidades.Log.ErrorType.Error, "frmInsPrimaria:frmInsPrimariaEntrada_Load", ExodusBcBase.Helper.ExtraerExcepcion(ex) + "--> " + ex.StackTrace, VariablesGlobales.PathDataLog);
                XtraMessageBox.Show(frmMain2.lookAndFeelMsgBox, "Ha ocurrido un error al cargar el formulario", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void comprobarCamara()
        {
            //Iniciar Camara
            DispositivosDeVideo = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            CargarCamara(DispositivosDeVideo);
            ////Temporal	
            //ExisteCamara = true;
            if (ExisteCamara) picCamara.Image = Resources.camera1;
            else picCamara.Image = Resources.camera2;
        }

        private void CargarCamara(FilterInfoCollection Dispositivos)
        {
            lstCamaras = new List<CamarasDisponibles>();
            for (int item = 0; item <= Dispositivos.Count - 1; item++)
            {
                CamarasDisponibles camara = new CamarasDisponibles() { Index = item, Nombre = Dispositivos[item].Name };
                lstCamaras.Add(camara);
            }
            foreach (var camara in lstCamaras)
            {
                if (camara.Nombre.ToString() == VariablesGlobales.NOMBRECAMARA) ExisteCamara = true;
                //else ExisteCamara = false;
            }
        }

        public void comprobarEscaner()
        {
            try
            {
                if (VariablesGlobales.SeCapturaHuella)
                {
                    //Iniciar Scanner
                    cs500eInicializado = CLSFPCaptureDllWrapper.CLS_Initialize();
                    if (cs500eInicializado < 0)
                    {
                        cs500eInicializado = -1;
                        picCS500e.Image = Resources.dsscanner2;
                        
                        //***En el caso que se omita la captura de huella***
                        omitirHuellas = true;
                        lblVerificarHuella.Visible = false;
                        lblGifValidando.Visible = false;
                        lblEstadoValidacion.ForeColor = Color.Sienna;
                        picAtencion.Visible = true;
                        lblEstadoValidacion.Font = new Font("Segoe UI Semibold", 8, FontStyle.Bold);
                        lblEstadoValidacion.Text = "Se omite la captura de huellas, no se encontro ningún scanner conectado al equipo.";
                        XtraMessageBox.Show("El scanner no fue detectado, se omitirá la captura de huellas.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        //=========================================================
                    }
                    else
                    {
                        CLSFPCaptureDllWrapper.CLS_SetLanguage(CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_RESOURCE.SPANISH);
                        picCS500e.Image = Resources.dsscanner1;
                    }
                    VariablesGlobales.CS500eInicializado = cs500eInicializado;
                }
                else picCS500e.Visible = false;
            }
            catch (Exception ex)
            {
                picCS500e.Image = Resources.dsscanner2;
            }
        }

        public void comprobarPad()
        {
            if (VariablesGlobales.SeCapturaFirma)
            {
                //Iniciar PadFirmas
                try
                {
                    ExistePadFirmas = sigPlusNET1.TabletConnectQuery();
                    if (ExistePadFirmas) picPadFirma.Image = Resources.dspad1;
                    else picPadFirma.Image = Resources.dspad2;
                }
                catch (Exception)
                {
                    picPadFirma.Image = Resources.dspad2;
                }
            }
            else picPadFirma.Visible = false;
        }

        private void sbtnAnterior_Click(object sender, EventArgs e)
        {
            if (tabEnrolamiento.SelectedPageIndex > 0)
            {
                tabEnrolamiento.SelectPrevPage();
                SetTitulos();
            }
            else if (tabEnrolamiento.SelectedPageIndex == 0) sbtnAnterior.Enabled = false;

            //***En el caso que se omita la captura de huellas***
            if (omitirHuellas) tabEnrolamiento.SelectedPageIndex = 0;
            //=======================================================

            if (sbtnSiguiente.Enabled == false) sbtnSiguiente.Enabled = true;
        }
        
        void SetTitulos()
        {
            if (tabEnrolamiento.SelectedPageIndex == 0)
            {
                lblTituloAccion.Text = "Información Biográfica";
                lblDetalles.Text = "Datos Personales.";
                picNumeros.Image = Resources.Circulo;
            }
            else if (tabEnrolamiento.SelectedPageIndex == 1)
            {
                lblTituloAccion.Text = "Captura de Huellas";
                lblDetalles.Text = "Captura de huellas dactilares";
                picNumeros.Image = Resources.Circulo2;
            }
            else if (tabEnrolamiento.SelectedPageIndex == 2)
            {
                lblTituloAccion.Text = "Captura de Fotografía";
                lblDetalles.Text = "Captura aplicando foto vertical.";
                picNumeros.Image = Resources.Circulo3;
            }
            else if (tabEnrolamiento.SelectedPageIndex == 3)
            {
                lblTituloAccion.Text = "Captura de Firma";
                lblDetalles.Text = "Por favór ingresar su firma";
                picNumeros.Image = Resources.Circulo4;
            }
            else if (tabEnrolamiento.SelectedPageIndex == 4)
            {
                lblTituloAccion.Text = "Revisión de Información";
                lblDetalles.Text = "Por favór revise los datos recolectados.";
                picNumeros.Image = Resources.Circulo5;
            }
        }
        private void sbtnSiguiente_Click(object sender, EventArgs e)
        {
            switch (tabEnrolamiento.SelectedPageIndex)
            {
                case 0:
                    {
                        estadoBiograficos = false;
                        if (ValidarTabBiograficos())
                        {
                            //***En el caso que se omita la captura de huellas***
                            if(!omitirHuellas)
                            {
                                if(chkNoPresencial.Checked) omitirHuellas = true;
                                else omitirHuellas = false;
                            }
                            
                            if (omitirHuellas)
                            {
                                estadoHuellas = true;
                                tabEnrolamiento.SelectedPageIndex = 2;
                            }
                            else tabEnrolamiento.SelectNextPage();

                            sbtnAnterior.Enabled = true;
                            GenerarNumeroTransaccion();
                        }
                        break;
                    }
                case 1:
                    {
                        estadoHuellas = false;
                        if (ValidarTabHuella()) tabEnrolamiento.SelectNextPage();
                        break;
                    }
                case 2:
                    {
                        estadoFoto = false;
                        if (ValidarTabFoto())
                        {
                            tabEnrolamiento.SelectNextPage();
                            if ((int)luePersonaHuellaEstado.EditValue != 1 && (int)luePersonaHuellaEstado.EditValue != 2)
                                if (!nistGenerado)
                                {
                                    string dirWSQ = Path.Combine(VariablesGlobales.PathWSQ, lblNumeroTransaccion.Text);
                                    if (_RegistroPersonas <= 0)
                                    {
                                        string _Sexo = "";
                                        if (lueSexo.EditValue.ToString() == "1") _Sexo = "F";
                                        else _Sexo = "M";

                                        string _Nacionalidad = "";
                                        var objGetAlphaNationality = LN.NATIONALITYLN.GetAlpha3byNationalityId(Convert.ToInt32(lueNacionalidad.EditValue));
                                        if (objGetAlphaNationality != null) _Nacionalidad = objGetAlphaNationality.Alpha3;

                                        //Quitar consulta al Afis
                                        var fid = Nist.EmpaquetarNistFID(numeroConsultaAfis.ToString(), Path.Combine(dirWSQ, "FID"), txtPrimerNombre.Text.Trim().ToUpper(), txtPrimerApellido.Text.Trim().ToUpper(), "", _Sexo, _Nacionalidad, Convert.ToDateTime(dtmFechaNacimiento.EditValue), "GVISION");

                                        nistGenerado = true;
                                    }

                                    if (VariablesGlobales.SeCapturaHuella)
                                    {
                                        if (nistGenerado)
                                        {
                                            if (ValidacionHuellas == ValidacionAfis.NO_INICIADO || ValidacionHuellas == ValidacionAfis.TIMEOUT)
                                            {
                                                ValidacionHuellas = ValidacionAfis.ENVIANDO_NIST;
                                                if (_RegistroPersonas <= 0) IniciarProcesamientoNist();
                                                else IniciarProcesamientoNist1a1();
                                            }
                                        }
                                    }
                                }
                        }
                        sigPlusNET1.SetTabletState(1);
                        break;
                    }
                case 3://Firma
                    {
                        if (ValidarTabFirma())
                        {
                            CargarInformacionFinal();
                            btnGuardar.Enabled = true;
                            tabEnrolamiento.SelectNextPage();
                            sbtnSiguiente.Enabled = false;
                        }
                        break;
                    }
            }
            SetTitulos();
        }

        private bool ValidarTabFirma()
        {
            if (sigPlusNET1.NumberOfTabletPoints() == 0 && !chknoPuedeFirmar.Checked)
            {
                XtraMessageBox.Show("La firma esta vacia, esta campo es requerido.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                Utilidades.Log.InsertarLog(Utilidades.Log.ErrorType.Informacion, "Enrolamiento::ValidarTabFirma", "No se capturó la firma", VariablesGlobales.PathDataLog);
                return false;
            }
            if (firmaCapturada == null)
            {
                sigPlusNET1.SetImageXSize(500);
                sigPlusNET1.SetImageYSize(150);
                sigPlusNET1.SetJustifyMode(5);
                var firma = sigPlusNET1.GetSigImage();
                firmaCapturada = firma;
            }
            Utilidades.Log.InsertarLog(Utilidades.Log.ErrorType.Informacion, "Enrolamiento::ValidarTabFirma", "Firma Capturada", VariablesGlobales.PathDataLog);
            return true;
        }

        void IniciarProcesamientoNist()
        {
            try
            {
                tmr1aN.Start();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Utilidades.Log.InsertarLog(Utilidades.Log.ErrorType.Error, "Enrolamiento::IniciarProcesamientoNist", Utilidades.Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        private void IniciarProcesamientoNist1a1()
        {
            try
            {
                tmr1a1.Start();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Utilidades.Log.InsertarLog(Utilidades.Log.ErrorType.Error, "Enrolamiento::IniciarProcesamientoNist1a1", Utilidades.Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        void CargarInformacionFinal()
        {
            //BIOGRAFICOS
            lblTipoDocumento.Text = lueTipoDocumento.Text;
            lblIdentificacion.Text = txtIdentificacion.Text;

            lblNombres.Text = string.Format("{0} {1}", txtPrimerNombre.Text, txtSegundoNombre.Text);
            lblApellidos.Text = string.Format("{0} {1}", txtPrimerApellido.Text, txtSegundoApellido.Text);

            lblSexo.Text = lueSexo.Text;

            lblEstadoCivil.Text = lueEstadoCivil.Text;

            lblProfesionOficio.Text = lueProfesion.Text;

            if (txtCorreo.Text != string.Empty) lblCorreoElectronico.Text = txtCorreo.Text;
            else lblCorreoElectronico.Text = string.Empty;


            lblNacionalidad.Text = lueNacionalidad.Text;

            lblPaisNacimiento.Text = luePaisNacimiento.Text;
            lblNivelEducativo.Text = lueNivelEducativo.Text;

            lblNacimiento.Text = Convert.ToString(string.Format("{0:dd/MM/yyyy}", dtmFechaNacimiento.EditValue));

            lblDepartamento.Text = lueDepartamentos.Text;

            lblMunicipio.Text = lueMunicipios.Text;

            lblColoniaCasa.Text = lueColonia.Text;

            lblTelefono.Text = txtTelefono.Text;

            lblMovil.Text = txtMovil.Text;

            lblDireccionResidencia.Text = meDireccionCompleta.Text;
            picDedo1Final.Image = picPulgarDerecho.Image;
            picDedo2Final.Image = picIndiceDerecho.Image;
            picDedo3Final.Image = picMedioDerecho.Image;
            picDedo4Final.Image = picAnularDerecho.Image;
            picDedo5Final.Image = picMeniequeDerecho.Image;
            picDedo6Final.Image = picPulgarIzquierdo.Image;
            picDedo7Final.Image = picIndiceIzquierdo.Image;
            picDedo8Final.Image = picMedioIzquierdo.Image;
            picDedo9Final.Image = picAnularIzquierdo.Image;
            picDedo10Final.Image = picMeniequeIzquierdo.Image;

            picFotoFinal.Image = picFotoCapturada.Image;
            if (firmaCapturada != null) reviewSignature.Image = firmaCapturada;
        }

        public bool ValidarTabAdjuntos()
        {
            estadoAdjuntos = true;
            return true;
        }

#region PROCESO_HUELLAS
        private bool ValidarTabHuella()
        {
            totalDedos = 0;
            if (picPulgarDerecho.Image != null) totalDedos += 1;
            if (picIndiceDerecho.Image != null) totalDedos += 1;
            if (picMedioDerecho.Image != null) totalDedos += 1;
            if (picAnularDerecho.Image != null) totalDedos += 1;
            if (picMeniequeDerecho.Image != null) totalDedos += 1;
            if (picPulgarIzquierdo.Image != null) totalDedos += 1;
            if (picIndiceIzquierdo.Image != null) totalDedos += 1;
            if (picMedioIzquierdo.Image != null) totalDedos += 1;
            if (picAnularIzquierdo.Image != null) totalDedos += 1;
            if (picMeniequeIzquierdo.Image != null) totalDedos += 1;
            if (pnlObservacionesPersonaHuellaEstado.Visible)
            {
                if (meObservacionHuellas.Text.Trim().Length == 0)
                {
                    XtraMessageBox.Show("Escriba una justificación por la cual no capturará ninguna huella.", "Atención", 
                                               MessageBoxButtons.OK, MessageBoxIcon.Error);
                    meObservacionHuellas.Focus();
                    return false;
                }
            }

            if ((PersonsLN.PersonaHuellasEstado)luePersonaHuellaEstado.EditValue != PersonsLN.PersonaHuellasEstado.TemporalmenteSinHuellas
                && (PersonsLN.PersonaHuellasEstado)luePersonaHuellaEstado.EditValue != PersonsLN.PersonaHuellasEstado.SinHuellas)
            {
                if (totalDedos == 0)
                {
                    XtraMessageBox.Show("No ha capturado ninguna huella.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Utilidades.Log.InsertarLog(Utilidades.Log.ErrorType.Informacion, "Enrolamiento::ValidarHuellas", "No Se capturaron las huellas", VariablesGlobales.PathDataLog);
                    sbtnCapturaHuellas.Focus();
                    return false;
                }
            }

            estadoHuellas = true;
            Utilidades.Log.InsertarLog(Utilidades.Log.ErrorType.Informacion, "Enrolamiento::ValidarHuellas", "Se capturaron las huellas", VariablesGlobales.PathDataLog);
            return true;
        }
       
        AfisTransaccion ValidarHuellaAfis(string noTransaccion)
        {
            string strTransNo = noTransaccion;
            string strTOT = string.Empty;
            string strExternalID = string.Empty;
            string strStatus = string.Empty;
            string strConfirm_Status = string.Empty;
            string strCAN_TransNo = string.Empty;
            string strCAN_ExternalId = string.Empty;
            string strCAN_TCN = string.Empty;
            string strCAN_LastName = string.Empty;
            string strCAN_FirstName = string.Empty;
            string strCAND_ID1 = string.Empty;
            string strCAND_SID = string.Empty;
            string strCAN_Sex = string.Empty;
            string strCAN_DateOfBirth = string.Empty;
            string strCAN_TOT = string.Empty;
            string strCAND_POB = string.Empty;

            afisRes.TransNo = noTransaccion;

            int respuesta = wsAfis.GetTransStatus(strTransNo, out strTOT, out strExternalID, out strStatus, out strConfirm_Status, out strCAN_TransNo, out strCAN_ExternalId, out strCAN_TCN,
                out strCAN_LastName, out strCAN_FirstName, out strCAND_ID1, out strCAND_SID, out strCAN_Sex, out strCAN_DateOfBirth, out strCAN_TOT, out strCAND_POB);

            afisRes.TransNo = strTransNo;
            afisRes.TOT = strTOT;
            afisRes.ExternalID = strExternalID;
            afisRes.Status = strStatus;
            afisRes.ConfirmStatus = strConfirm_Status;
            afisRes.CAN_TransNo = strCAN_TransNo;
            afisRes.CAN_ExternalId = strCAN_ExternalId;
            afisRes.CAN_TCN = strCAN_TCN;
            afisRes.CAN_LastName = strCAN_LastName;
            afisRes.CAN_FirstName = strCAN_FirstName;
            afisRes.CAND_ID1 = strCAND_ID1;
            afisRes.CAN_SID = strCAND_SID;
            afisRes.CAN_Sex = strCAN_Sex;
            afisRes.CAN_DateOfBirth = strCAN_DateOfBirth;
            afisRes.CAN_TOT = strCAN_TOT;
            afisRes.CAND_POB = strCAND_POB;

            return afisRes;
        }

        void InsertarHit()
        {
            CrearXMLHits();
            try
            {
                InsertarEnrolmentExcepciones();
            }
            catch (Exception ex)
            {
                Utilidades.Log.InsertarLog(Utilidades.Log.ErrorType.Error, "frmInsPrimaria:InsertarHit", ExodusBcBase.Helper.ExtraerExcepcion(ex) + "--> " + ex.StackTrace, VariablesGlobales.PathDataLog);
                XtraMessageBox.Show(frmMain2.lookAndFeelMsgBox, "Ha ocurrido un error al Insertar Hit", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        void CrearXMLHits()
        {
            //NODO RAIZ
            XElement nodoRaiz = new XElement("Enrolment");
            ////Nodo Biograficos
            XElement EnrolmentPerson = new XElement("EnrolmentsPerson");
            EnrolmentPerson.Add(new XElement("FirstName"), txtPrimerNombre.Text);
            EnrolmentPerson.Add(new XElement("SecondName"), txtSegundoNombre.Text);
            EnrolmentPerson.Add(new XElement("FirstLastName"), txtPrimerApellido.Text);
            EnrolmentPerson.Add(new XElement("SecondLastName"), txtSegundoApellido.Text);
            EnrolmentPerson.Add(new XElement("DateOfBirth"), Convert.ToString(String.Format("{0:dd/MM/yyyy}", dtmFechaNacimiento.EditValue)));
            EnrolmentPerson.Add(new XElement("CreationDate"), ExodusBcBase.Utilities.getFechaActual());
            EnrolmentPerson.Add(new XElement("ModifyDate"), ExodusBcBase.Utilities.getFechaActual());
            EnrolmentPerson.Add(new XElement("PersonalMovilPhone"), txtTelefono.Text);
            EnrolmentPerson.Add(new XElement("Address"), meDireccionCompleta.Text);
            EnrolmentPerson.Add(new XElement("WorkPhone"), txtTelefonoTrabajo.Text);
           
            EnrolmentPerson.Add(new XElement("WorkAddress"), meDireccionTrabajo.Text);
            EnrolmentPerson.Add(new XElement("PersonalEmail"), txtCorreo.Text);
            EnrolmentPerson.Add(new XElement("WorkEmail"), txtCorreoTrabajo.Text);
            EnrolmentPerson.Add(new XElement("FKSexId"), lueSexo.EditValue);
            EnrolmentPerson.Add(new XElement("FKCivilStatusId"), lueEstadoCivil.EditValue);
            EnrolmentPerson.Add(new XElement("FKPersonProfessionsId"), lueProfesion.EditValue);
            EnrolmentPerson.Add(new XElement("FKEducationLevelId"), lueNivelEducativo.EditValue);
            EnrolmentPerson.Add(new XElement("FKStateCityId"), lueMunicipios.EditValue);
            EnrolmentPerson.Add(new XElement("FKStateCountryId"), lueDepartamentos.EditValue);
            EnrolmentPerson.Add(new XElement("FKNeighborhoodId"), lueColonia.EditValue);
            EnrolmentPerson.Add(new XElement("FKNationalityId"), lueNacionalidad.EditValue);
            EnrolmentPerson.Add(new XElement("FKCatalogStatusId"), 1);
            EnrolmentPerson.Add(new XElement("FKCountryId"), luePaisNacimiento.EditValue);
            EnrolmentPerson.Add(new XElement("FKCreateUserId"), VariablesGlobales.USER_ID);
            nodoRaiz.Add(EnrolmentPerson);
            ////Nodo Huellas	
            XElement EnrolmentHuellas= new XElement("Huellas");
            EnrolmentHuellas.Add(new XElement("EstadoHuellas", luePersonaHuellaEstado.EditValue));
            EnrolmentHuellas.Add(new XElement("ObservacionHuellas", meObservacionHuellas.Text));
            EnrolmentHuellas.Add(new XElement("EnviadoAfis", afisRes.Status));

            foreach (var item in personaEnrolarHuellasCapturadas.Dedos)
            {
                XElement EnrolmentDedos = new XElement("Dedos");

                if (picPulgarDerecho.Image != null && item.WSQ != null && item.Dedo == 1)
                {
                    XElement nodoDedo = new XElement("Dedo");
                    byte[] dedo1 = ExodusBcBase.Utilities.ImageToByteArrayBmp(picPulgarDerecho.Image);
                    nodoDedo.Add(new XElement("NumeroDedo", item.Dedo));
                    nodoDedo.Add(new XElement("Calidad", item.Calidad));
                    nodoDedo.Add(new XElement("WSQ", Convert.ToBase64String(item.WSQ)));
                    nodoDedo.Add(new XElement("Imagen", Convert.ToBase64String(dedo1)));
                    EnrolmentDedos.Add(nodoDedo);
                }
                if (picIndiceDerecho.Image != null && item.WSQ != null && item.Dedo == 2)
                {
                    XElement nodoDedo = new XElement("Dedo");
                    byte[] dedo2 = ExodusBcBase.Utilities.ImageToByteArrayBmp(picIndiceDerecho.Image);
                    nodoDedo.Add(new XElement("NumeroDedo", item.Dedo));
                    nodoDedo.Add(new XElement("Calidad", item.Calidad));
                    nodoDedo.Add(new XElement("WSQ", Convert.ToBase64String(item.WSQ)));
                    nodoDedo.Add(new XElement("Imagen", Convert.ToBase64String(dedo2)));
                    EnrolmentDedos.Add(nodoDedo);

                }
                if (picMedioDerecho.Image != null && item.WSQ != null && item.Dedo == 3)
                {
                    XElement nodoDedo = new XElement("Dedo");
                    byte[] dedo3 = ExodusBcBase.Utilities.ImageToByteArrayBmp(picMedioDerecho.Image);
                    nodoDedo.Add(new XElement("NumeroDedo", item.Dedo));
                    nodoDedo.Add(new XElement("Calidad", item.Calidad));
                    nodoDedo.Add(new XElement("WSQ", Convert.ToBase64String(item.WSQ)));
                    nodoDedo.Add(new XElement("Imagen", Convert.ToBase64String(dedo3)));
                    EnrolmentDedos.Add(nodoDedo);

                }
                if (picAnularDerecho.Image != null && item.WSQ != null && item.Dedo == 4)
                {
                    XElement nodoDedo = new XElement("Dedo");
                    byte[] dedo4 = ExodusBcBase.Utilities.ImageToByteArrayBmp(picAnularDerecho.Image);
                    nodoDedo.Add(new XElement("NumeroDedo", item.Dedo));
                    nodoDedo.Add(new XElement("Calidad", item.Calidad));
                    nodoDedo.Add(new XElement("WSQ", Convert.ToBase64String(item.WSQ)));
                    nodoDedo.Add(new XElement("Imagen", Convert.ToBase64String(dedo4)));
                    EnrolmentDedos.Add(nodoDedo);

                }
                if (picMeniequeDerecho.Image != null && item.WSQ != null && item.Dedo == 5)
                {
                    XElement nodoDedo = new XElement("Dedo");
                    byte[] dedo5 = ExodusBcBase.Utilities.ImageToByteArrayBmp(picMeniequeDerecho.Image);
                    nodoDedo.Add(new XElement("NumeroDedo", item.Dedo));
                    nodoDedo.Add(new XElement("Calidad", item.Calidad));
                    nodoDedo.Add(new XElement("WSQ", Convert.ToBase64String(item.WSQ)));
                    nodoDedo.Add(new XElement("Imagen", Convert.ToBase64String(dedo5)));
                    EnrolmentDedos.Add(nodoDedo);

                }
                if (picPulgarIzquierdo.Image != null && item.WSQ != null && item.Dedo == 6)
                {
                    XElement nodoDedo = new XElement("Dedo");
                    byte[] dedo6 = ExodusBcBase.Utilities.ImageToByteArrayBmp(picPulgarIzquierdo.Image);
                    nodoDedo.Add(new XElement("NumeroDedo", item.Dedo));
                    nodoDedo.Add(new XElement("Calidad", item.Calidad));
                    nodoDedo.Add(new XElement("WSQ", Convert.ToBase64String(item.WSQ)));
                    nodoDedo.Add(new XElement("Imagen", Convert.ToBase64String(dedo6)));
                    EnrolmentDedos.Add(nodoDedo);

                }
                if (picIndiceIzquierdo.Image != null && item.WSQ != null && item.Dedo == 7)
                {
                    XElement nodoDedo = new XElement("Dedo");
                    byte[] dedo7 = ExodusBcBase.Utilities.ImageToByteArrayBmp(picIndiceIzquierdo.Image);
                    nodoDedo.Add(new XElement("NumeroDedo", item.Dedo));
                    nodoDedo.Add(new XElement("Calidad", item.Calidad));
                    nodoDedo.Add(new XElement("WSQ", Convert.ToBase64String(item.WSQ)));
                    nodoDedo.Add(new XElement("Imagen", Convert.ToBase64String(dedo7)));
                    EnrolmentDedos.Add(nodoDedo);

                }
                if (picMedioIzquierdo.Image != null && item.WSQ != null && item.Dedo == 8)
                {
                    XElement nodoDedo = new XElement("Dedo");
                    byte[] dedo8 = ExodusBcBase.Utilities.ImageToByteArrayBmp(picMedioIzquierdo.Image);
                    nodoDedo.Add(new XElement("NumeroDedo", item.Dedo));
                    nodoDedo.Add(new XElement("Calidad", item.Calidad));
                    nodoDedo.Add(new XElement("WSQ", Convert.ToBase64String(item.WSQ)));
                    nodoDedo.Add(new XElement("Imagen", Convert.ToBase64String(dedo8)));
                    EnrolmentDedos.Add(nodoDedo);

                }
                if (picAnularIzquierdo.Image != null && item.WSQ != null && item.Dedo == 9)
                {
                    XElement nodoDedo = new XElement("Dedo");
                    byte[] dedo9 = ExodusBcBase.Utilities.ImageToByteArrayBmp(picAnularIzquierdo.Image);
                    nodoDedo.Add(new XElement("NumeroDedo", item.Dedo));
                    nodoDedo.Add(new XElement("Calidad", item.Calidad));
                    nodoDedo.Add(new XElement("WSQ", Convert.ToBase64String(item.WSQ)));
                    nodoDedo.Add(new XElement("Imagen", Convert.ToBase64String(dedo9)));
                    EnrolmentDedos.Add(nodoDedo);

                }
                if (picMeniequeIzquierdo.Image != null && item.WSQ != null && item.Dedo == 10)
                {
                    XElement nodoDedo = new XElement("Dedo");
                    byte[] dedo10 = ExodusBcBase.Utilities.ImageToByteArrayBmp(picMeniequeIzquierdo.Image);
                    nodoDedo.Add(new XElement("NumeroDedo", item.Dedo));
                    nodoDedo.Add(new XElement("Calidad", item.Calidad));
                    nodoDedo.Add(new XElement("WSQ", Convert.ToBase64String(item.WSQ)));
                    nodoDedo.Add(new XElement("Imagen", Convert.ToBase64String(dedo10)));
                    EnrolmentDedos.Add(nodoDedo);

                }
                EnrolmentHuellas.Add(EnrolmentDedos);


            }
            nodoRaiz.Add(EnrolmentHuellas);
            ////Nodo Foto	
            XElement EnrolmentPersonPictures = new XElement("PersonPictures");
            if (picFotoCapturada.EditValue != null)
            {
                byte[] foto = ExodusBcBase.Utilities.ImageToByteArray(picFotoCapturada.Image);
                EnrolmentPersonPictures.Add(new XElement("Image", Convert.ToBase64String(foto)));
                EnrolmentPersonPictures.Add(new XElement("CreationDate", ExodusBcBase.Utilities.getFechaActual()));
               
            }
            nodoRaiz.Add(EnrolmentPersonPictures);
           
         
            //Nodo Solicitud
            XElement Enrolments = new XElement("Enrolments");
            Enrolments.Add(new XElement("DocumentNumber", txtIdentificacion.Text));
            Enrolments.Add(new XElement("FPCaptured",personaEnrolarHuellasCapturadas.TotalDedos));
            Enrolments.Add(new XElement("Attachments", Documentos.Count));
            Enrolments.Add(new XElement("RegisteredAfis", nistEnviado));
            Enrolments.Add(new XElement("CreationDate", ExodusBcBase.Utilities.getFechaActual()));
            Enrolments.Add(new XElement("ModifyDate", ExodusBcBase.Utilities.getFechaActual()));
            Enrolments.Add(new XElement("FKDocumentType", lueTipoDocumento.EditValue));
            Enrolments.Add(new XElement("FKWorkStationId", VariablesGlobales.WS_ID));
            Enrolments.Add(new XElement("FKCreationUserId", VariablesGlobales.USER_ID));
            nodoRaiz.Add(Enrolments);
            string encodingXml = @"<?xml version=""1.0"" encoding=""utf-8"" standalone=""yes""?>";
            HitXML = string.Format("{0}{1}", encodingXml, nodoRaiz);

        }

        public bool InsertarEnrolmentExcepciones()
        {
            bool salida = false;
            try
            {
                if (VariablesGlobales.MODOTRABAJO.Equals("ON"))
                {
                    string sql = String.Format("INSERT INTO EnrolmentExceptions (DocumentNumber, Names, Surnames, FKSexId, DateOfBirth, Filehit, FKCatalogStatusId, CreationDate, ModifyDate, FKWorkStationId, FKExceptionType, FKEnrolmentId, FKOfficeId, FKNationalityId, FKUserId ) VALUES ('{0}','{1}','{2}',{3},'{4}','{5}',{6},GetDate(),GetDate(),{7},{8},{9},{10},{11},{12})", txtIdentificacion.Text, txtPrimerNombre.Text + " " + txtSegundoNombre.Text, txtPrimerApellido.Text + " " + txtSegundoApellido.Text, lueSexo.EditValue, Convert.ToDateTime(dtmFechaNacimiento.EditValue).ToString("dd/MM/yyyy"), HitXML, 1, VariablesGlobales.WS_ID, 17, Convert.ToInt64(PERSONA_ID_HIT), Convert.ToInt32(VariablesGlobales.OFFICE_CODE), lueNacionalidad.EditValue, VariablesGlobales.USER_ID);
                    string res = ws.Insertar(VariablesGlobales.TOKEN, sql, host);
                    if (res == "\"\"") salida = true;
                }
                else ExodusBcBase.Helper.Mensajes.getMensajeSinConexion();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Error al intentar procesar log: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Utilidades.Log.InsertarLog(Utilidades.Log.ErrorType.Error, "frmLogin::InsertarLoginLog", ExodusBcBase.Helper.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
            return salida;
        }

        void Limpiar()
        {
            ValidacionHuellas = ValidacionAfis.NO_INICIADO;
            _RegistroPersonas = 0;

            SetTitulos();
            lblNumeroTransaccion.Text = "";

            lblEstadoValidacion.ForeColor = Color.White;
            lblEstadoValidacion.Text = "No iniciada";

            //Limpiar page captura
            txtIdentificacion.Text = "";
            lueTipoDocumento.EditValue = null;
            txtPrimerNombre.Text = "";
            txtSegundoNombre.Text = "";
            txtPrimerApellido.Text = "";
            txtSegundoApellido.Text = "";
            lueSexo.EditValue = null;
            luePaisNacimiento.EditValue = null;
            lueNacionalidad.EditValue = null;
            dtmFechaNacimiento.Text = "";
            lueSexo.EditValue = null;
            lueDepartamentos.EditValueChanged -= lueDepartamentos_EditValueChanged;
            lueDepartamentos.EditValue = null;
            lueMunicipios.EditValue = null;
            lueMunicipios.Properties.DataSource = null;
            lueMunicipios.EditValueChanged -= lueMunicipios_EditValueChanged;
            lueColonia.EditValue = null;
            meDireccionCompleta.Text = "";
            txtTelefono.Text = "";
            txtTelefonoTrabajo.Text = "";
            txtCorreo.Text = "";
            txtCorreoTrabajo.Text = "";
            lueProfesion.EditValue = null;
            lueNivelEducativo.EditValue = null;
            meDireccionTrabajo.Text = "";
            txtCorreoTrabajo.Text = "";
          
            prepararCapturaHuellas();

            luePersonaHuellaEstado.EditValueChanged -= luePersonaHuellaEstado_EditValueChanged;
            luePersonaHuellaEstado.ItemIndex = 0;
            luePersonaHuellaEstado.EditValueChanged += luePersonaHuellaEstado_EditValueChanged;
            pnlObservacionesPersonaHuellaEstado.Visible = false;
            meObservacionHuellas.Text = "";
            nistGenerado = false;

            //Limpiar page Foto
            picFotoCapturada.Image = null;

            //Limpiar page Firma
            sigPlusNET1.ClearTablet();
            sigPlusNET1.BackgroundImage = null;
            sigPlusNET1.SetJustifyMode(0);
            firmaCapturada = null;
            meObservacionFirma.Text = "";
            chknoPuedeFirmar.Checked = false;

            //Limpiar page Adjuntos
            Documentos = new List<clsDocumento>();
            NumeroDocumentoTwain = 0;
          

            //Limpiar page Review
            lblTipoDocumento.Text = "";
            lblIdentificacion.Text = "";

            lblNombres.Text = "";
            lblApellidos.Text = "";
            lblSexo.Text = "";
            lblEstadoCivil.Text = "";
            lblProfesionOficio.Text = "";
            lblCorreoElectronico.Text = "";
            lblNacionalidad.Text = "";
            lblPaisNacimiento.Text = "";
            lblNivelEducativo.Text = "";
            lblNacimiento.Text = "";
            lblDepartamento.Text = "";
            lblMunicipio.Text = "";
            lblColoniaCasa.Text = "";
            lblTelefono.Text = "";
            lblMovil.Text = "";
            lblDireccionResidencia.Text = "";
            picDedo1Final.Image =null;
            picDedo2Final.Image = null;
            picDedo3Final.Image = null;
            picDedo4Final.Image = null;
            picDedo5Final.Image = null;
            picDedo6Final.Image = null;
            picDedo7Final.Image = null;
            picDedo8Final.Image = null;
            picDedo9Final.Image = null;
            picDedo10Final.Image = null;
            picFotoCapturada.Image = null;
            picFotoFinal.Image = null;

            frmEnrolamientoLectora.objLectura = null;
        }

        void prepararCapturaHuellas()
        {
            personaEnrolarHuellasCapturadas = new ClasesPersonalizadas.HuellasCapturar();
            personaEnrolarHuellasCapturadas.TotalDedos = 0;
            nistGenerado = false;
            CLSFPCaptureDllWrapper.CLS_Clear();
            CLSFPCaptureDllWrapper.CLS_Terminate();
            LimpiarControlesHuellas();
            LimpiarBarrasLabel();
            LimpiarCarpetas();
        }

        class AfisTransaccion
        {

            public string TransNo { get; set; }
            public string TOT { get; set; }
            public string ExternalID { get; set; }
            public string Status { get; set; }
            public string ConfirmStatus { get; set; }
            public string CAN_TransNo { get; set; }
            public string CAN_ExternalId { get; set; }
            public string CAN_TCN { get; set; }
            public string CAN_LastName { get; set; }
            public string CAN_FirstName { get; set; }
            public string CAND_ID1 { get; set; }
            public string CAN_SID { get; set; }
            public string CAN_Sex { get; set; }
            public string CAN_DateOfBirth { get; set; }
            public string CAN_TOT { get; set; }
            public string CAND_POB { get; set; }


        }
        void LimpiarControlesHuellas()
        {
            //MANO DERECHA
            if (picPulgarDerecho.Image != null)
            {
                picPulgarDerecho.Image.Dispose();
                picPulgarDerecho.Image = null;
            }
            if (picIndiceDerecho.Image != null)
            {
                picIndiceDerecho.Image.Dispose();
                picIndiceDerecho.Image = null;
            }

            if (picMedioDerecho.Image != null)
            {
                picMedioDerecho.Image.Dispose();
                picMedioDerecho.Image = null;
            }
            if (picAnularDerecho.Image != null)
            {
                picAnularDerecho.Image.Dispose();
                picAnularDerecho.Image = null;
            }
            if (picMeniequeDerecho.Image != null)
            {
                picMeniequeDerecho.Image.Dispose();
                picMeniequeDerecho.Image = null;
            }

            //MANO IZQUIERDA
            if (picPulgarIzquierdo.Image != null)
            {
                picPulgarIzquierdo.Image.Dispose();
                picPulgarIzquierdo.Image = null;
            }
            if (picIndiceIzquierdo.Image != null)
            {
                picIndiceIzquierdo.Image.Dispose();
                picIndiceIzquierdo.Image = null;
            }
            if (picMedioIzquierdo.Image != null)
            {
                picMedioIzquierdo.Image.Dispose();
                picMedioIzquierdo.Image = null;
            }
            if (picAnularIzquierdo.Image != null)
            {
                picAnularIzquierdo.Image.Dispose();
                picAnularIzquierdo.Image = null;
            }
            if (picMeniequeIzquierdo.Image != null)
            {
                picMeniequeIzquierdo.Image.Dispose();
                picMeniequeIzquierdo.Image = null;
            }
        }
        void ReiniciarCaptura()
        {
            TotalHuellasCapturadas = 0;
            procesoValidacionIniciado = false;

            CLSFPCaptureDllWrapper.CLS_Clear();
            LimpiarControlesHuellas();
            LimpiarBarrasLabel();
            totalDedos = 0;
            TransaccionAfis = String.Empty;

            if (valoresCalidad.Count > 0)
            {
                for (int i = 0; i < valoresCalidad.Count; i++)
                {
                    valoresCalidad.RemoveAt(i);
                    i--;
                }
                for (int i = 0; i < valoresCalidad.Count; i++)
                {
                    accesoTemporalWSQ.RemoveAt(i);
                    i--;
                }
            }

            if (accesoTemporal != null) Array.Clear(accesoTemporal, 0, accesoTemporal.Length);
            if (AccesoTemporal2 != null) Array.Clear(AccesoTemporal2, 0, AccesoTemporal2.Length);

            numeroTransaccion.Clear();
            directorio.Clear();
            accesoTemporalWSQ.Clear();
        }
        public void LimpiarBarrasLabel()
        {
            //Barras Progresoas
            prgbDedo1.Text = string.Empty;
            prgbDedo1.Visible = false;

            prgbDedo2.Text = string.Empty;
            prgbDedo2.Visible = false;

            prgbDedo3.Text = string.Empty;
            prgbDedo3.Visible = false;

            prgbDedo4.Text = string.Empty;
            prgbDedo4.Visible = false;

            prgbDedo5.Text = string.Empty;
            prgbDedo5.Visible = false;

            prgbDedo6.Text = string.Empty;
            prgbDedo6.Visible = false;

            prgbDedo7.Text = string.Empty;
            prgbDedo7.Visible = false;

            prgbDedo8.Text = string.Empty;
            prgbDedo8.Visible = false;

            prgbDedo9.Text = string.Empty;
            prgbDedo9.Visible = false;

            prgbDedo10.Text = string.Empty;
            prgbDedo10.Visible = false;

            //labels
            lblDedo1.Text = string.Empty;
            lblDedo1.Visible = false;

            lblDedo2.Text = string.Empty;
            lblDedo2.Visible = false;

            lblDedo3.Text = string.Empty;
            lblDedo3.Visible = false;

            lblDedo4.Text = string.Empty;
            lblDedo4.Visible = false;

            lblDedo5.Text = string.Empty;
            lblDedo5.Visible = false;

            lblDedo6.Text = string.Empty;
            lblDedo6.Visible = false;

            lblDedo7.Text = string.Empty;
            lblDedo7.Visible = false;

            lblDedo8.Text = string.Empty;
            lblDedo8.Visible = false;

            lblDedo9.Text = string.Empty;
            lblDedo9.Visible = false;

            lblDedo10.Text = string.Empty;
            lblDedo10.Visible = false;

            prgbIzquierdoGrande.Text = string.Empty;
            prgbDerechoGrande.Text = string.Empty;

            lblDedosDerecha.Text = string.Empty;
            lblDedosIzquierda.Text = string.Empty;
            picSeleccionDerecha.Image = Resources.Cancel01;
            picSeleccionIzquierda.Image = Resources.Cancel01;
            pnlManoDerecha.Visible = false;
            pnlManoIzquierda.Visible = false;
        }
      
        public List<EN.EnrolmentsHuellas> GetHuellasCapturas()
        {
            List<EN.EnrolmentsHuellas> listaHuellas = new List<EN.EnrolmentsHuellas>();
            foreach (ClasesPersonalizadas.Dedos dedo in personaEnrolarHuellasCapturadas.Dedos)
            {
                EN.EnrolmentsHuellas _enrolmentHuellas = new EN.EnrolmentsHuellas();
                _enrolmentHuellas.dedo = dedo.Dedo;
                _enrolmentHuellas.estado = dedo.Estado;
                _enrolmentHuellas.calidad = dedo.Calidad;
                if (dedo.Estado == 0) _enrolmentHuellas.wsq = Convert.ToBase64String(dedo.WSQ);
                else _enrolmentHuellas.wsq = "";
                listaHuellas.Add(_enrolmentHuellas);
            }
            return listaHuellas;
        }

        public List<EN.EnrolmentsAttachs> getDocumentosAdjuntos()
        {
            List<EN.EnrolmentsAttachs> listaDocumentos = new List<EN.EnrolmentsAttachs>();
            foreach (clsDocumento doc in Documentos)
            {
                 EN.EnrolmentsAttachs _enrolmentsAttachs = new EN.EnrolmentsAttachs();
                _enrolmentsAttachs.Numero = doc.Numero;
                _enrolmentsAttachs.Nombre = doc.Nombre;
                _enrolmentsAttachs.CreationDate = ExodusBcBase.Utilities.getFechaActual();
                _enrolmentsAttachs.Documento = doc.Documento;
                _enrolmentsAttachs.FKCreateUserId = Convert.ToInt32(VariablesGlobales.USER_ID);
                _enrolmentsAttachs.FKDocumentType = doc.FKDocumentType;
                _enrolmentsAttachs.Size = doc.Size;
                _enrolmentsAttachs.Type = doc.Type;
                listaDocumentos.Add(_enrolmentsAttachs);
            }
            return listaDocumentos;
        }
#endregion

#region PROCESO_FOTO
        private bool ValidarTabFoto()
        {
            if (picFotoCapturada.Image == null)
            {
                XtraMessageBox.Show("Foto esta vacía, esta imagen es requerida.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                sbtnIniciarCamara.Select();
                return false;
            }
            Utilidades.Log.InsertarLog(Utilidades.Log.ErrorType.Informacion, "Enrolamiento::ValidarHuellas", "Se capturó la fotografía", VariablesGlobales.PathDataLog);
            estadoFoto = true;
            return true;
        }

        public class CamarasDisponibles
        {
            public int Index { get; set; }
            public string Nombre { get; set; }
        }
        
        private void sbtnIniciarCamara_Click(object sender, EventArgs e)
        {
            PictureBox picTemporal = new PictureBox();
            WrapperCamara CamSDK = new WrapperCamara();

            CamSDK.SetNombreCamara(VariablesGlobales.NOMBRECAMARA);
            CamSDK.SetPictureBoxDestino(picTemporal);
            try
            {
                CamSDK.Inicializar();
                ValoresIcao = CamSDK.MensajeResultado;
                TotalErroresIcao = CamSDK.TotalErrores;
                if (picTemporal.Image != null)
                {
                    picFotoCapturada.Image = picTemporal.Image;

                    //***Guardamos foto para el SID***
                    //string dir = Path.Combine(VariablesGlobales.PathWSQ, lblNumeroTransaccion.Text);
                    //string pathFoto = Path.Combine(dir, lblNumeroTransaccion.Text + "_FF.jpg");
                    //if (!Directory.Exists(dir)) Directory.CreateDirectory(dir);

                    //FileStream fstream = new FileStream(pathFoto, FileMode.OpenOrCreate, FileAccess.Write);
                    //picFotoCapturada.Image.Save(fstream, System.Drawing.Imaging.ImageFormat.Jpeg);


                    GuardarFoto();
                    //picFotoCapturada.Image.Save(pathFoto, System.Drawing.Imaging.ImageFormat.Jpeg);
                    //if (File.Exists(pathFoto)) File.Delete(pathFoto);
                }
             
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnAdjuntarFoto_Click(object sender, EventArgs e)
        {
            long tamanioMB = (VariablesGlobales.AdjuntoTamanioMaximo / 1024) / 1024;
            System.Drawing.Image img = picFotoCapturada.Image;
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "JPEG Image (.jpg)|*.jpg|PNG Image|*.png";
            dialog.Multiselect = false;
            
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    picFotoCapturada.Image = ComprimirImagen(dialog.FileName);
                    GuardarFoto();

                    byte[] valorFoto = Utilities.ImageToByteArrayJpg(picFotoCapturada.Image);
                    if (valorFoto.Length > VariablesGlobales.AdjuntoTamanioMaximo)
                    {
                        XtraMessageBox.Show(string.Format("No puede adjuntar archivo mayor a {0} MB, por favor intente con otro de igual o menor tamaño.",
                            tamanioMB), "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        picFotoCapturada.Image = img;
                        return;
                    }
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Hubo un error al tratar de adjuntar la fotografía.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Log.InsertarLog(Log.ErrorType.Error, "frmInsPrimariaEntrada::ComprimirImagen", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                    return;
                }
            }
        }


        private void GuardarFoto()
        {
            //***Guardamos foto para el SID***
            string dir = Path.Combine(VariablesGlobales.PathWSQ, lblNumeroTransaccion.Text);
            string pathFoto = Path.Combine(dir, lblNumeroTransaccion.Text + "_FF.jpg");
            if (!Directory.Exists(dir)) Directory.CreateDirectory(dir);

            FileStream fstream = new FileStream(pathFoto, FileMode.OpenOrCreate, FileAccess.Write);
            picFotoCapturada.Image.Save(fstream, System.Drawing.Imaging.ImageFormat.Jpeg);
        }

        Image ComprimirImagen(string imgAConvertir)
        {
            try
            {
                Bitmap bmpOrigen = (Bitmap)Image.FromFile(imgAConvertir);
                //***Porcentaje de compresión***
                int valorCompresion = 60; 
                
                float porcentajeCompresion = ((float)valorCompresion / 100);
                int destinoAncho = (int)(bmpOrigen.Width * porcentajeCompresion);
                int destinoAlto = (int)(bmpOrigen.Height * porcentajeCompresion);

                Bitmap bmpAComprimir = new Bitmap(destinoAncho, destinoAlto);
                using (Graphics g = Graphics.FromImage(bmpAComprimir))
                    g.DrawImage(bmpOrigen, 0, 0, destinoAncho, destinoAlto);

                bmpOrigen.Dispose();
                //bmpAComprimir.Save(VariablesGlobales.PathDataSystem + archivo, ImageFormat.Jpeg);

                var bmpFinal = new Bitmap(bmpAComprimir);
                Image imgFinal = (Image)bmpFinal;

                return (imgFinal);
            }
            catch (Exception ex)
            {
                Utilidades.Log.InsertarLog(Utilidades.Log.ErrorType.Error, "frmInsPrimariaEntrada::ComprimirImagen", Utilidades.Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                return null;
            }
        }

        Image RedimensionarImagen(byte[] img, string archivo)
        {
            try
            {
                int tamanio = 560;
                byte[] imgData = null;
                imgData = img;
                Image imgtemporal = Image.FromStream(new MemoryStream(imgData));
                double imgEscala = tamanio / (double)imgtemporal.Width;
                Graphics grf = default(Graphics);
                Bitmap imgRedim = new Bitmap(Convert.ToInt32(imgEscala * imgtemporal.Width), Convert.ToInt32(imgEscala * imgtemporal.Height));
                grf = Graphics.FromImage(imgRedim);
                grf.InterpolationMode = InterpolationMode.HighQualityBilinear;
                grf.DrawImage(imgtemporal, 0, 0, imgRedim.Width + 1, imgRedim.Height + 1);
                System.Drawing.Image imgFinal = imgRedim;

                imgFinal.Save(VariablesGlobales.PathDataSystem + archivo, ImageFormat.Jpeg);
                return imgFinal;
            }
            catch (Exception ex)
            {
                Utilidades.Log.InsertarLog(Utilidades.Log.ErrorType.Error, "frmInsPrimariaEntrada::RedimensionarImagen", Utilidades.Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                return null;
            }
        }


        #endregion

        private void GenerarNumeroTransaccion()
        {
            string _fechaServer = ExodusBcBase.Utilities.getFechaActual().ToString("yyyyMMdd"); 
            string Codigo = string.Format("{0}{1}{2}", VariablesGlobales.OFFICE_CODE, _fechaServer, LN.SequenceLN.GetSequenceEnrolment());
            lblNumeroTransaccion.Text = Codigo;
            numeroConsultaAfis = "C" + LN.SequenceLN.GetSequenceConsultaAfis();// PRODUCCION
            //numeroConsultaAfis = "Z" + LN.SequenceLN.GetSequenceConsultaAfis(); //PRUEBAS
            Utilidades.Log.InsertarLog(Utilidades.Log.ErrorType.Informacion, "Enrolamiento::GenerarNumeroTransaccion", "N#Transaccion: "+numeroConsultaAfis, VariablesGlobales.PathDataLog);

        }

        private bool ValidarTabBiograficos()
        {
            if (lueTipoDocumento.EditValue == null)
            {
                XtraMessageBox.Show("Tipo de Documento esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                lueTipoDocumento.Select();
                return false;
            }
            if (txtIdentificacion.Text.Trim().Length == 0)
            {
                XtraMessageBox.Show("Número de Identificación esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtIdentificacion.Select();
                return false;
            }
            if (txtPrimerNombre.Text.Trim().Length == 0)
            {
                XtraMessageBox.Show("Primer Nombre esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtPrimerNombre.Select();
                return false;
            }
            if (txtPrimerApellido.Text.Trim().Length == 0)
            {
                XtraMessageBox.Show("Primer Apellido esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtPrimerApellido.Select();
                return false;
            }
            if (lueSexo.Text.Trim().Length == 0)
            {
                XtraMessageBox.Show("Sexo esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                lueSexo.Select();
                return false;
            }
            if (luePaisNacimiento.Text.Trim().Length == 0)
            {
                XtraMessageBox.Show("País de nacimiento esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                luePaisNacimiento.Select();
                return false;
            }
            if (lueNacionalidad.Text.Trim().Length == 0)
            {
                XtraMessageBox.Show("Nacionalidad esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                lueNacionalidad.Select();
                return false;
            }
            if (dtmFechaNacimiento.DateTime == DateTime.MinValue)
            {
                XtraMessageBox.Show("Fecha de nacimiento esta vacía, este campo es nesesario", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                dtmFechaNacimiento.Select();
                return false;
            }
            else
            {
                dtmFechaNacimiento.Properties.MaxValue = ExodusBcBase.Utilities.getFechaActual();
                if (VariablesGlobales.EDADMINIMA >= 0)
                {
                    string calculoEdad = ExodusBcBase.Utilities.getEdad(dtmFechaNacimiento.DateTime).ToString();
                    if (Convert.ToInt32(calculoEdad) < VariablesGlobales.EDADMINIMA)
                    {
                        XtraMessageBox.Show(string.Format("La edad permitida para enrolar es {0} años.", VariablesGlobales.EDADMINIMA), "Atención",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        dtmFechaNacimiento.Select();
                        return false;
                    }
                }

                if (lueEstadoCivil.Text.Trim().Length == 0)
                {
                    XtraMessageBox.Show("Estado Civil esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    lueEstadoCivil.Select();
                    return false;
                }
                if (luePaisResidencia.Text.Trim().Length == 0)
                {
                    XtraMessageBox.Show("País de residencia esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    luePaisResidencia.Select();
                    return false;
                }
                if (lueDepartamentos.Text.Trim().Length == 0)
                {
                    XtraMessageBox.Show("Departamento de Nacimiento esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    lueDepartamentos.Select();
                    return false;
                }
                if (lueMunicipios.Text.Trim().Length == 0)
                {
                    XtraMessageBox.Show("Municipio de Nacimiento esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    lueMunicipios.Select();
                    return false;
                }
                if (lueColonia.Text.Trim().Length == 0)
                {
                    XtraMessageBox.Show("Colonia esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    lueMunicipios.Select();
                    return false;
                }
                if (meDireccionCompleta.Text.Trim().Length == 0)
                {
                    XtraMessageBox.Show("Dirección particular esta vacía, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    meDireccionCompleta.Select();
                    return false;
                }
                if (txtTelefono.Text.Trim().Length == 0 && txtMovil.Text.Trim().Length == 0)
                {
                    XtraMessageBox.Show("Debe agregar al menos un número telefónico", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtTelefono.Select();
                    return false;
                }
                if (lueProfesion.Text.Trim().Length == 0)
                {
                    XtraMessageBox.Show("Profesión  esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    lueProfesion.Select();
                    return false;
                }
                if (lueNivelEducativo.Text.Trim().Length == 0)
                {
                    XtraMessageBox.Show("Nivel educativo esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    lueNivelEducativo.Select();
                    return false;
                }
                estadoBiograficos = true;
                return true;
            }
        }

        private void lueDepartamentos_EditValueChanged(object sender, EventArgs e)
        {
            if (lueDepartamentos.EditValue != null)
            {
                ExodusBcBase.Helper.Combobox.setComboStateCity(lueMunicipios, Convert.ToInt32(lueDepartamentos.EditValue));
                lueMunicipios.EditValueChanged += lueMunicipios_EditValueChanged;
            }
        }

        private void lueMunicipios_EditValueChanged(object sender, EventArgs e)
        {
            if (lueMunicipios.EditValue != null && lueDepartamentos.EditValue != null)
                ExodusBcBase.Helper.Combobox.setComboNeighborhood(lueColonia, Convert.ToInt32(lueMunicipios.EditValue));
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            frmMain2 changeValue = (frmMain2)System.Windows.Forms.Application.OpenForms["frmMain2"];
            changeValue.OpenForms("frmPrimariaEntrada");
            Limpiar();
            Close();
        }
      
        void SetLabelText(LabelControl lbl, string texto, string tipo)
        {
            try
            {
                if (lbl.InvokeRequired)
                {
                    SetLabelCallBack dele = new SetLabelCallBack(SetLabelText);
                    Invoke(dele, new object[] { lbl, texto, tipo });
                }
                else
                {
                    lbl.Text = texto;
                    switch (tipo)
                    {
                        case "E":
                            {
                                lbl.ForeColor = Color.SeaGreen;
                                break;
                            }
                        case "A":
                            {
                                lbl.ForeColor = ColorTranslator.FromHtml("#E54B4B");
                                break;
                            }
                        case "I":
                            {
                                lbl.ForeColor = Color.DarkOrange;
                                break;
                            }
                    }
                }
            }
            catch (Exception)
            {

            }
        }

        private void LimpiarCarpetasSalida()
        {
            try
            {
                foreach (var item in Directory.GetFiles(VariablesGlobales.PathHuellas)) File.Delete(item);
                foreach (var item in Directory.GetFiles(VariablesGlobales.PathWSQ)) File.Delete(item);
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        private void sbtnCapturaHuellas_Click(object sender, EventArgs e)
        {
            ReiniciarCaptura();
            lblNumeroTransaccion.Visible = true;
            try
            {
                transaccion = CLSFPCaptureDllWrapper.CLS_SetTransNo(lblNumeroTransaccion.Text.Trim());
                captura = CLSFPCaptureDllWrapper.CLS_Capture(CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_TYPE.IDFLATS_SEPARATE_THUMBS);
                if (captura == 0)
                {
                    CargarHuellasCapturadas();
                    luePersonaHuellaEstado.EditValueChanged -= luePersonaHuellaEstado_EditValueChanged;
                    if (personaEnrolarHuellasCapturadas.TotalDedos == 10)
                    {
                        luePersonaHuellaEstado.EditValue = Convert.ToInt32(PersonsLN.PersonaHuellasEstado.HuellasCompletas);
                        pnlObservacionesPersonaHuellaEstado.Visible = false;
                    }
                    else if (personaEnrolarHuellasCapturadas.TotalDedos > 0 && personaEnrolarHuellasCapturadas.TotalDedos < 10)
                    {
                        luePersonaHuellaEstado.EditValue = Convert.ToInt32(PersonsLN.PersonaHuellasEstado.HuellasTemporalmenteIncompletas);
                        pnlObservacionesPersonaHuellaEstado.Visible = true;
                        meObservacionHuellas.Focus();
                    }
                    luePersonaHuellaEstado.EditValueChanged += luePersonaHuellaEstado_EditValueChanged;
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmInsPrimariaEntrada::sbtnCapturaHuellas_Click", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        public void CargarHuellasCapturadas()
        {
            string dirWSQ = Path.Combine(VariablesGlobales.PathWSQ, lblNumeroTransaccion.Text);
            string dirBmp = Path.Combine(VariablesGlobales.PathHuellas, lblNumeroTransaccion.Text);
          
            if (!Directory.Exists(dirBmp)) Directory.CreateDirectory(dirBmp);
            if (!Directory.Exists(dirWSQ)) Directory.CreateDirectory(dirWSQ);


            int respSaveWSQ = CLSFPCaptureDllWrapper.CLS_SaveAllImages(CLSFPCaptureDllWrapper.IMG_TYPE.WSQ, dirWSQ, CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_SAVE_IMG.SEG_FING_ONLY);
            int respSaveImages = CLSFPCaptureDllWrapper.CLS_SaveAllImages(CLSFPCaptureDllWrapper.IMG_TYPE.BMP, dirBmp, CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_SAVE_IMG.SEG_FING_ONLY);

            CLSFPCaptureDllWrapper.CLS_SetTransNo(numeroConsultaAfis);
            CLSFPCaptureDllWrapper.CLS_SaveAllImages(CLSFPCaptureDllWrapper.IMG_TYPE.WSQ, Path.Combine(dirWSQ, "FID"), CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_SAVE_IMG.SEG_FING_ONLY);

            try
            {
                if (respSaveWSQ == 0 && respSaveImages == 0)
                {
                    string[] archivoImagenes = Directory.GetFiles(dirBmp);
                    personaEnrolarHuellasCapturadas.Dedos  = new List<ExodusBcBase.ClasesPersonalizadas.Dedos>();
                    personaEnrolarHuellasCapturadas.ID = lblNumeroTransaccion.Text;
                    personaEnrolarHuellasCapturadas.TotalDedos = archivoImagenes.Count();

                    for (int i = 1; i <= 10; i++)
                    {
                        //variables temporales para cada dedo
                        string fileWSQ = Path.Combine(dirWSQ, lblNumeroTransaccion.Text);
                        string fileBmp = Path.Combine(dirBmp, lblNumeroTransaccion.Text);
                        int calidad = 0;
                        CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_IMG_EXP estado = CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_IMG_EXP.NO_EXCEPTION; //el estado del dedo 0=present, 1=amputado, 2=vendado

                        byte[] wsqBytes;
                        byte[] imgBytes;

                        fileWSQ += string.Format("_{0}.wsq", i.ToString("00"));
                        fileBmp += string.Format("_{0}.bmp", i.ToString("00"));
                        var existe = archivoImagenes.Contains(fileBmp);

                        switch (i)
                        {
                            case 1:
                                {
                                    ////Dedo 1 
                                    CLSFPCaptureDllWrapper.CLS_GetImageException(CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.RIGHT_THUMB, ref estado);
                                    if (existe)
                                    {
                                        CLSFPCaptureDllWrapper.CLS_GetImageNFIQ(CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.RIGHT_THUMB, ref calidad, CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_IMPRESSION_TYPE.PLAIN);
                                        wsqBytes = File.ReadAllBytes(fileWSQ);
                                        imgBytes = File.ReadAllBytes(fileBmp);
                                        personaEnrolarHuellasCapturadas.Dedos.Add(new ClasesPersonalizadas.Dedos(1, calidad, (int)estado, wsqBytes, imgBytes));
                                        picPulgarDerecho.Image = ExodusBcBase.Helper.ImageFromFile(fileBmp);
                                        picPulgarDerecho.Tag = calidad.ToString(); //Aqui guardo la calidad de la huella
                                        Utilities.PrepararProgressBar(prgbDedo1, lblDedo1, calidad);
                                    }
                                    else personaEnrolarHuellasCapturadas.Dedos.Add(new ClasesPersonalizadas.Dedos(1, calidad, (int)estado, null, null));
                                    //Fin Dedo 1
                                    break;
                                }
                            case 2:
                                {
                                    CLSFPCaptureDllWrapper.CLS_GetImageException(CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.RIGHT_INDEX, ref estado);
                                    if (existe)
                                    {
                                        CLSFPCaptureDllWrapper.CLS_GetImageNFIQ(CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.RIGHT_INDEX, ref calidad, CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_IMPRESSION_TYPE.PLAIN);
                                        wsqBytes = File.ReadAllBytes(fileWSQ);
                                        imgBytes = File.ReadAllBytes(fileBmp);
                                        personaEnrolarHuellasCapturadas.Dedos.Add(new ExodusBcBase.ClasesPersonalizadas.Dedos(2, calidad, (int)estado, wsqBytes, imgBytes));
                                        picIndiceDerecho.Image = ExodusBcBase.Helper.ImageFromFile(fileBmp);
                                        picIndiceDerecho.Tag = calidad.ToString(); //Aqui guardo la calidad de la huella
                                        Utilities.PrepararProgressBar(prgbDedo2, lblDedo2, calidad);
                                    }
                                    else personaEnrolarHuellasCapturadas.Dedos.Add(new ClasesPersonalizadas.Dedos(2, calidad, (int)estado, null, null));
                                    break;
                                }
                            case 3:
                                {
                                    //Dedo 3 
                                    CLSFPCaptureDllWrapper.CLS_GetImageException(CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.RIGHT_MIDDLE, ref estado);
                                    if (existe)
                                    {
                                        CLSFPCaptureDllWrapper.CLS_GetImageNFIQ(CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.RIGHT_MIDDLE, ref calidad, CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_IMPRESSION_TYPE.PLAIN);
                                        wsqBytes = File.ReadAllBytes(fileWSQ);
                                        imgBytes = File.ReadAllBytes(fileBmp);
                                        personaEnrolarHuellasCapturadas.Dedos.Add(new ClasesPersonalizadas.Dedos(3, calidad, (int)estado, wsqBytes, imgBytes));
                                        picMedioDerecho.Image = ExodusBcBase.Helper.ImageFromFile(fileBmp);
                                        picMedioDerecho.Tag = calidad.ToString(); //Aqui guardo la calidad de la huella
                                        Utilities.PrepararProgressBar(prgbDedo3, lblDedo3, calidad);
                                    }
                                    else personaEnrolarHuellasCapturadas.Dedos.Add(new ClasesPersonalizadas.Dedos(3, calidad, (int)estado, null, null));
                                    //Fin Dedo 3
                                    break;
                                }
                            case 4:
                                {
                                    //Dedo 4 
                                    CLSFPCaptureDllWrapper.CLS_GetImageException(CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.RIGHT_RING, ref estado);
                                    if (existe)
                                    {
                                        CLSFPCaptureDllWrapper.CLS_GetImageNFIQ(CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.RIGHT_RING, ref calidad, CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_IMPRESSION_TYPE.PLAIN);
                                        wsqBytes = File.ReadAllBytes(fileWSQ);
                                        imgBytes = File.ReadAllBytes(fileBmp);
                                        personaEnrolarHuellasCapturadas.Dedos.Add(new ClasesPersonalizadas.Dedos(4, calidad, (int)estado, wsqBytes, imgBytes));
                                        picAnularDerecho.Image = ExodusBcBase.Helper.ImageFromFile(fileBmp);
                                        picAnularDerecho.Tag = calidad.ToString(); //Aqui guardo la calidad de la huella
                                        Utilities.PrepararProgressBar(prgbDedo4, lblDedo4, calidad);
                                    }
                                    else personaEnrolarHuellasCapturadas.Dedos.Add(new ClasesPersonalizadas.Dedos(4, calidad, (int)estado, null, null));
                                    //Fin Dedo 4
                                    break;
                                }
                            case 5:
                                {
                                    //Dedo 5 
                                    CLSFPCaptureDllWrapper.CLS_GetImageException(CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.RIGHT_LITTLE, ref estado);
                                    if (existe)
                                    {
                                        CLSFPCaptureDllWrapper.CLS_GetImageNFIQ(CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.RIGHT_LITTLE, ref calidad, CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_IMPRESSION_TYPE.PLAIN);
                                        wsqBytes = File.ReadAllBytes(fileWSQ);
                                        imgBytes = File.ReadAllBytes(fileBmp);
                                        personaEnrolarHuellasCapturadas.Dedos.Add(new ClasesPersonalizadas.Dedos(5, calidad, (int)estado, wsqBytes, imgBytes));
                                        picMeniequeDerecho.Image = ExodusBcBase.Helper.ImageFromFile(fileBmp);
                                        picMeniequeDerecho.Tag = calidad.ToString(); //Aqui guardo la calidad de la huella
                                        Utilities.PrepararProgressBar(prgbDedo5, lblDedo5, calidad);
                                    }
                                    else personaEnrolarHuellasCapturadas.Dedos.Add(new ClasesPersonalizadas.Dedos(5, calidad, (int)estado, null, null));
                                    //Fin Dedo 5
                                    break;
                                }
                            case 6:
                                {
                                    //Dedo 6 
                                    CLSFPCaptureDllWrapper.CLS_GetImageException(CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.LEFT_THUMB, ref estado);
                                    if (existe)
                                    {
                                        CLSFPCaptureDllWrapper.CLS_GetImageNFIQ(CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.LEFT_THUMB, ref calidad, CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_IMPRESSION_TYPE.PLAIN);
                                        wsqBytes = File.ReadAllBytes(fileWSQ);
                                        imgBytes = File.ReadAllBytes(fileBmp);
                                        personaEnrolarHuellasCapturadas.Dedos.Add(new ClasesPersonalizadas.Dedos(6, calidad, (int)estado, wsqBytes, imgBytes));
                                        picPulgarIzquierdo.Image = ExodusBcBase.Helper.ImageFromFile(fileBmp);
                                        picPulgarIzquierdo.Tag = calidad.ToString(); //Aqui guardo la calidad de la huella
                                        Utilities.PrepararProgressBar(prgbDedo6, lblDedo6, calidad);
                                    }
                                    else personaEnrolarHuellasCapturadas.Dedos.Add(new ClasesPersonalizadas.Dedos(6, calidad, (int)estado, null, null));
                                    //Fin Dedo 6 v
                                    break;
                                }
                            case 7:
                                {
                                    //Dedo 7 
                                    CLSFPCaptureDllWrapper.CLS_GetImageException(CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.LEFT_INDEX, ref estado);
                                    if (existe)
                                    {
                                        CLSFPCaptureDllWrapper.CLS_GetImageNFIQ(CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.LEFT_INDEX, ref calidad, CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_IMPRESSION_TYPE.PLAIN);
                                        wsqBytes = File.ReadAllBytes(fileWSQ);
                                        imgBytes = File.ReadAllBytes(fileBmp);
                                        personaEnrolarHuellasCapturadas.Dedos.Add(new ClasesPersonalizadas.Dedos(7, calidad, (int)estado, wsqBytes, imgBytes));
                                        picIndiceIzquierdo.Image = ExodusBcBase.Helper.ImageFromFile(fileBmp);
                                        picIndiceIzquierdo.Tag = calidad.ToString(); //Aqui guardo la calidad de la huella
                                        Utilities.PrepararProgressBar(prgbDedo7, lblDedo7, calidad);
                                    }
                                    else personaEnrolarHuellasCapturadas.Dedos.Add(new ClasesPersonalizadas.Dedos(7, calidad, (int)estado, null, null));
                                    //Fin Dedo 7
                                    break;
                                }
                            case 8:
                                {
                                    //Dedo 8 
                                    CLSFPCaptureDllWrapper.CLS_GetImageException(CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.LEFT_MIDDLE, ref estado);
                                    if (existe)
                                    {
                                        CLSFPCaptureDllWrapper.CLS_GetImageNFIQ(CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.LEFT_MIDDLE, ref calidad, CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_IMPRESSION_TYPE.PLAIN);
                                        wsqBytes = File.ReadAllBytes(fileWSQ);
                                        imgBytes = File.ReadAllBytes(fileBmp);
                                        personaEnrolarHuellasCapturadas.Dedos.Add(new ClasesPersonalizadas.Dedos(8, calidad, (int)estado, wsqBytes, imgBytes));
                                        picMedioIzquierdo.Image = ExodusBcBase.Helper.ImageFromFile(fileBmp);
                                        picMedioIzquierdo.Tag = calidad.ToString(); //Aqui guardo la calidad de la huella
                                        Utilities.PrepararProgressBar(prgbDedo8, lblDedo8, calidad);
                                    }
                                    else personaEnrolarHuellasCapturadas.Dedos.Add(new ClasesPersonalizadas.Dedos(8, calidad, (int)estado, null, null));
                                    //Fin Dedo 8
                                    break;
                                }
                            case 9:
                                {
                                    //Dedo 9 
                                    CLSFPCaptureDllWrapper.CLS_GetImageException(CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.LEFT_RING, ref estado);
                                    if (existe)
                                    {
                                        CLSFPCaptureDllWrapper.CLS_GetImageNFIQ(CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.LEFT_RING, ref calidad, CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_IMPRESSION_TYPE.PLAIN);
                                        wsqBytes = File.ReadAllBytes(fileWSQ);
                                        imgBytes = File.ReadAllBytes(fileBmp);
                                        personaEnrolarHuellasCapturadas.Dedos.Add(new ClasesPersonalizadas.Dedos(9, calidad, (int)estado, wsqBytes, imgBytes));
                                        picAnularIzquierdo.Image = ExodusBcBase.Helper.ImageFromFile(fileBmp);
                                        picAnularIzquierdo.Tag = calidad.ToString(); //Aqui guardo la calidad de la huella
                                        Utilities.PrepararProgressBar(prgbDedo9, lblDedo9, calidad);
                                    }
                                    else personaEnrolarHuellasCapturadas.Dedos.Add(new ClasesPersonalizadas.Dedos(9, calidad, (int)estado, null, null));
                                    //Fin Dedo 9
                                    break;
                                }
                            case 10:
                                {
                                    //Dedo 10 
                                    CLSFPCaptureDllWrapper.CLS_GetImageException(CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.LEFT_LITTLE, ref estado);
                                    if (existe)
                                    {
                                        CLSFPCaptureDllWrapper.CLS_GetImageNFIQ(CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_FINGER.LEFT_LITTLE, ref calidad, CLSFPCaptureDllWrapper.CLS_FP_CAPTURE_IMPRESSION_TYPE.PLAIN);
                                        wsqBytes = File.ReadAllBytes(fileWSQ);
                                        imgBytes = File.ReadAllBytes(fileBmp);
                                        personaEnrolarHuellasCapturadas.Dedos.Add(new ClasesPersonalizadas.Dedos(10, calidad, (int)estado, wsqBytes, imgBytes));
                                        picMeniequeIzquierdo.Image = ExodusBcBase.Helper.ImageFromFile(fileBmp);
                                        picMeniequeIzquierdo.Tag = calidad.ToString(); //Aqui guardo la calidad de la huella
                                        Utilities.PrepararProgressBar(prgbDedo10, lblDedo10, calidad);
                                    }
                                    else personaEnrolarHuellasCapturadas.Dedos.Add(new ClasesPersonalizadas.Dedos(10, calidad, (int)estado, null, null));
                                    //Fin Dedo 10
                                    break;
                                }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        private void luePersonaHuellaEstado_EditValueChanged(object sender, EventArgs e)
        {
            bool permitirCambiar;

            if ((PersonsLN.PersonaHuellasEstado)luePersonaHuellaEstado.OldEditValue == PersonsLN.PersonaHuellasEstado.HuellasTemporalmenteIncompletas && (PersonsLN.PersonaHuellasEstado)luePersonaHuellaEstado.EditValue == PersonsLN.PersonaHuellasEstado.HuellasIncompletas)
                permitirCambiar = true;
            else if ((PersonsLN.PersonaHuellasEstado)luePersonaHuellaEstado.OldEditValue == PersonsLN.PersonaHuellasEstado.HuellasIncompletas && (PersonsLN.PersonaHuellasEstado)luePersonaHuellaEstado.EditValue == PersonsLN.PersonaHuellasEstado.HuellasTemporalmenteIncompletas)
                permitirCambiar = true;
            else permitirCambiar = false;

            if (!permitirCambiar)
            {
                if (TotalHuellasCapturadas > 0)
                {
                    if (XtraMessageBox.Show("Si cambia a otra opción tendrá que capturar nuevamente las huellas, ¿Está seguro de proceder?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        if ((PersonsLN.PersonaHuellasEstado)luePersonaHuellaEstado.EditValue != PersonsLN.PersonaHuellasEstado.HuellasCompletas)
                        {
                            pnlObservacionesPersonaHuellaEstado.Visible = true;
                            meObservacionHuellas.Select();
                        }
                        else pnlObservacionesPersonaHuellaEstado.Visible = false;

                        if ((PersonsLN.PersonaHuellasEstado)luePersonaHuellaEstado.EditValue == PersonsLN.PersonaHuellasEstado.SinHuellas
                            || (PersonsLN.PersonaHuellasEstado)luePersonaHuellaEstado.EditValue == PersonsLN.PersonaHuellasEstado.TemporalmenteSinHuellas)
                        {
                            sbtnCapturaHuellas.Enabled = false;
                            btnReiniciarEscaner.Enabled = false;
                        }
                        ReiniciarCaptura();
                    }
                    else
                    {
                        luePersonaHuellaEstado.EditValueChanged -= luePersonaHuellaEstado_EditValueChanged;
                        luePersonaHuellaEstado.EditValue = luePersonaHuellaEstado.OldEditValue;
                        luePersonaHuellaEstado.EditValueChanged += luePersonaHuellaEstado_EditValueChanged;
                    }
                    return;
                }
            }

            sbtnCapturaHuellas.Enabled = true;
            btnReiniciarEscaner.Enabled = true;
            pnlObservacionesPersonaHuellaEstado.Visible = true;
            meObservacionHuellas.Select();

            if ((PersonsLN.PersonaHuellasEstado)luePersonaHuellaEstado.EditValue == PersonsLN.PersonaHuellasEstado.HuellasCompletas)
            {
                pnlObservacionesPersonaHuellaEstado.Visible = false;
                estadoHuellas = false;
            }
            else if ((PersonsLN.PersonaHuellasEstado)luePersonaHuellaEstado.EditValue == PersonsLN.PersonaHuellasEstado.SinHuellas
                || (PersonsLN.PersonaHuellasEstado)luePersonaHuellaEstado.EditValue == PersonsLN.PersonaHuellasEstado.TemporalmenteSinHuellas)
            {
                sbtnCapturaHuellas.Enabled = false;
                btnReiniciarEscaner.Enabled = false;
            }
        }
   
        public void SetControles()
        {

            lueDepartamentos.EditValueChanged -= lueDepartamentos_EditValueChanged;
            luePersonaHuellaEstado.EditValueChanged -= luePersonaHuellaEstado_EditValueChanged;
            //Llenar combos
            ExodusBcBase.Helper.Combobox.setCombo(lueSexo, Convert.ToInt32(LN.LISTCATALOGSTYPES.ListTypesCatalogs.Sexo));
            ExodusBcBase.Helper.Combobox.setComboTipoDocumento(lueTipoDocumento);
            ExodusBcBase.Helper.Combobox.setCombo(lueEstadoCivil, Convert.ToInt32(LN.LISTCATALOGSTYPES.ListTypesCatalogs.EstadoCivil));
            ExodusBcBase.Helper.Combobox.setComboProfesiones(lueProfesion);
            ExodusBcBase.Helper.Combobox.setCombo(lueNivelEducativo, Convert.ToInt32(LN.LISTCATALOGSTYPES.ListTypesCatalogs.NivelEducativo));
            ExodusBcBase.Helper.Combobox.setComboCountry(luePaisNacimiento);
            ExodusBcBase.Helper.Combobox.setComboNationality(lueNacionalidad);
            ExodusBcBase.Helper.Combobox.setComboStateCountry(lueDepartamentos);
            ExodusBcBase.Helper.Combobox.SetPersonaHuellaEstados(luePersonaHuellaEstado);
            luePersonaHuellaEstado.EditValue = Convert.ToInt32(LN.PersonsLN.PersonaHuellasEstado.HuellasCompletas);
            lueDepartamentos.EditValue = null;
            lueEstadoCivil.EditValue = null;
            lueSexo.EditValue = null;
            lueNacionalidad.EditValue = null;
            luePaisNacimiento.EditValue = null;
            lueNivelEducativo.EditValue = null;

            dtmFechaNacimiento.Properties.NullDate = DateTime.MinValue;
            dtmFechaNacimiento.Properties.NullText = string.Empty;

            LimpiarCarpetas();

            lblNumeroTransaccion.Visible = false;
            //Barras Progreso y label Huellas
            prgbDedo1.Visible = false;
            prgbDedo2.Visible = false;
            prgbDedo3.Visible = false;
            prgbDedo4.Visible = false;
            prgbDedo5.Visible = false;
            prgbDedo6.Visible = false;
            prgbDedo7.Visible = false;
            prgbDedo8.Visible = false;
            prgbDedo9.Visible = false;
            prgbDedo10.Visible = false;

            lblDedo1.Visible = false;
            lblDedo2.Visible = false;
            lblDedo3.Visible = false;
            lblDedo4.Visible = false;
            lblDedo5.Visible = false;
            lblDedo6.Visible = false;
            lblDedo7.Visible = false;
            lblDedo8.Visible = false;
            lblDedo9.Visible = false;
            lblDedo10.Visible = false;

            pnlManoIzquierda.Visible = false;
            pnlManoDerecha.Visible = false;
         
            picManoDerecha.Image = Resources.Mano_Derecha;
            picManoIzquierda.Image = Resources.Mano_Izquierda;
            picSeleccionDerecha.Image = Resources.Cancel01;
            picSeleccionIzquierda.Image = Resources.Cancel01;
            tabEnrolamiento.SelectedPageIndex = 0;
            sbtnAnterior.Enabled = false;
            SetTitulos();

            lueDepartamentos.EditValueChanged += lueDepartamentos_EditValueChanged;
            luePersonaHuellaEstado.EditValueChanged += luePersonaHuellaEstado_EditValueChanged;
            luePaisResidencia.EditValue = 102;
        }
      
        private void LimpiarCarpetas()
        {
            try
            {
                if (Directory.Exists(VariablesGlobales.PathWSQ))
                {
                    string[] subCarpetasWSQ = Directory.GetDirectories(VariablesGlobales.PathWSQ);
                    if (subCarpetasWSQ.Count() > 0)
                    {
                        foreach (var ruta in subCarpetasWSQ) Directory.Delete(ruta, true);
                    }
                }
                if (Directory.Exists(VariablesGlobales.PathHuellas))
                {
                    string[] subCarpetasHuellas = Directory.GetDirectories(VariablesGlobales.PathHuellas);
                    if (subCarpetasHuellas.Count() > 0)
                    {
                        foreach (var ruta in subCarpetasHuellas) Directory.Delete(ruta, true);
                    }
                }
            }
            catch (Exception ex)
            {
                Utilidades.Log.InsertarLog(Utilidades.Log.ErrorType.Error, "LimpiarCarpetas", ExodusBcBase.Helper.ExtraerExcepcion(ex) + "--> " + ex.StackTrace, VariablesGlobales.PathDataLog);
                XtraMessageBox.Show(frmMain2.lookAndFeelMsgBox, "Ha ocurrido un error al Limpiar carpetas", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        

#region Acciones Documentos Adjuntos
        //TWAIN
        public class clsDocumento
        {
            public int Numero { get; set; }
            public string Nombre { get; set; }
            public int FKDocumentType { get; set; }
            public string Observacion { get; set; }
            public string Type { get; set; }
           
            public string Size { get; set; }
            public byte[] Documento { get; set; }
        }
        private bool OpenSelectedSource()
        {
            bool respuesta = false;
            respuesta = gdTwain.TwainOpenSource(Handle, Dispositivo);
            if (!respuesta) MessageBox.Show("Unable to open selected device.", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return respuesta;
        }
    
#endregion

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (VariablesGlobales.ENVIOTRANSACCION == 1)
            {
                if (GuardarPersonaEnLinea()) RegistroOK();
                else XtraMessageBox.Show("Ha ocurrido un error, no se pudo guardar el registro.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            } 
            else
            {
                if (CrearJSON()) RegistroOK();
                else XtraMessageBox.Show("Ha ocurrido un error, no se pudo guardar el registro.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
       
        void RegistroOK()
        {
            XtraMessageBox.Show("Registro guardado satisfactoriamente", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
            Limpiar();
            frmMain2 changeValue = (frmMain2)System.Windows.Forms.Application.OpenForms["frmMain2"];
            changeValue.OpenForms("frmPrimariaEntrada");
            Close();
        }

        public bool CrearJSON()
        {
            try
            {
                JArray enrolment = new JArray();
                JObject items;
                JObject objeto = new JObject();
                
                //***Obtener secuencia***
                string fecha = DateTime.Parse(Utilities.getFechaActual().ToString()).ToString("yyyy-MM-dd").Replace("-", "");
                long _secuenciaenrolment = LN.SequenceLN.GetSequenceEnrolment();
                string secuencia = VariablesGlobales.OFFICE_CODE + fecha + _secuenciaenrolment + ".json";

                //***Agregar datos de la Persona***
                _enrolmentPersons.FirstName = txtPrimerNombre.Text.Trim();
                _enrolmentPersons.SecondName = txtSegundoNombre.Text.Trim();
                _enrolmentPersons.FirstLastName = txtPrimerApellido.Text.Trim();
                _enrolmentPersons.SecondLastName = txtSegundoApellido.Text.Trim();
                _enrolmentPersons.DateOfBirth = (dtmFechaNacimiento.EditValue != null) ? dtmFechaNacimiento.DateTime.Date : DateTime.MinValue;
                _enrolmentPersons.CreationDate = Utilities.getFechaActual();
                _enrolmentPersons.ModifyDate = Utilities.getFechaActual();
                _enrolmentPersons.PersonalMovilPhone = txtMovil.Text.Trim();
                _enrolmentPersons.HomePhone = txtTelefono.Text.Trim();
                _enrolmentPersons.Address = RequestCertificate.VerificarCaracteresEspeciales(meDireccionCompleta.Text.Trim());
                _enrolmentPersons.WorkPhone = txtTelefonoTrabajo.Text.Trim();
                _enrolmentPersons.WorkAddress = meDireccionTrabajo.Text.Trim();
                _enrolmentPersons.PersonalEmail = txtCorreo.Text.Trim();
                _enrolmentPersons.WorkEmail = txtCorreoTrabajo.Text.Trim();
                _enrolmentPersons.FKSexId = Convert.ToInt32(lueSexo.EditValue);
                _enrolmentPersons.FKCatalogStatusId = 1;
                _enrolmentPersons.FKCivilStatusId = Convert.ToInt32(lueEstadoCivil.EditValue);
                _enrolmentPersons.FKCountryId = Convert.ToInt32(luePaisNacimiento.EditValue);
                _enrolmentPersons.FKCountryResidenceId = Convert.ToInt32(luePaisResidencia.EditValue);
                _enrolmentPersons.FKCreateUserId = Convert.ToInt32(VariablesGlobales.USER_ID);
                _enrolmentPersons.FKEducationLevelId = Convert.ToInt32(lueNivelEducativo.EditValue);
                _enrolmentPersons.FKNationalityId = Convert.ToInt32(lueNacionalidad.EditValue);
                _enrolmentPersons.FKNeighborhoodId = Convert.ToInt32(lueColonia.EditValue);
                _enrolmentPersons.FKPersonProfessionsId = Convert.ToInt32(lueProfesion.EditValue);
                _enrolmentPersons.FKSexId = Convert.ToInt32(lueSexo.EditValue);
                _enrolmentPersons.FKStateCityId = Convert.ToInt32(lueMunicipios.EditValue);
                _enrolmentPersons.FKStateCountryId = Convert.ToInt32(lueDepartamentos.EditValue);
                _enrolmentPersons.AfisHuellasHit = HitenVivo; //Definir
                _enrolmentPersons.hitPersonId = "0"; //Definir
               
                items = new JObject(new JProperty("item", "_enrolmentPersons"), new JProperty("data", (JObject)JToken.FromObject(_enrolmentPersons)));
                enrolment.Add(items);

                //Enrolments
                _enrolments.sequenceEnrolment = Convert.ToString(_secuenciaenrolment);
                _enrolments.DocumentNumber = txtIdentificacion.Text.Trim();
                _enrolments.FPCaptured = totalDedos;
                _enrolments.Attachments = Documentos.Count();
                _enrolments.RegisteredAfis = nistEnviado;
                _enrolments.CreationDate = Utilities.getFechaActual();
                _enrolments.ModifyDate = Utilities.getFechaActual();
                _enrolments.FKDocumentType = Convert.ToInt32(lueTipoDocumento.EditValue);
                _enrolments.FKWorkStationId = Convert.ToInt32(VariablesGlobales.WS_ID);
                _enrolments.FKCreationUserId = Convert.ToInt32(VariablesGlobales.USER_ID);
                _enrolments.FKOficinaId = VariablesGlobales.OFFICE_ID;
                _enrolments.NoPresential = (!chkNoPresencial.Checked) ? false : true;

                items = new JObject(new JProperty("item", "_enrolments"), new JProperty("data", (JObject)JToken.FromObject(_enrolments)));
                enrolment.Add(items);

                //***Agregar datos de fotografía***
                if (picFotoCapturada.Image != null)
                {
                    _enrolmentPersonPictures.CreationDate = ExodusBcBase.Utilities.getFechaActual();
                    using (MemoryStream m = new MemoryStream())
                    {
                        Image imageIn = picFotoCapturada.Image;
                        MemoryStream ms = new MemoryStream();
                        imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                        byte[] imageBytes = ms.ToArray();
                        _enrolmentPersonPictures.Image = Convert.ToBase64String(imageBytes);
                    }

                    items = new JObject(new JProperty("item", "_enrolmentPersonPictures"), new JProperty("data", (JObject)JToken.FromObject(_enrolmentPersonPictures)));
                    enrolment.Add(items);
                }
                //***Agregar Firma***
                if (firmaCapturada != null)
                {
                    _enrolmentSignature.CreationDate = ExodusBcBase.Utilities.getFechaActual();
                    using (MemoryStream m = new MemoryStream())
                    {
                        Image imageIn = firmaCapturada;
                        MemoryStream ms = new MemoryStream();
                        imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                        byte[] imageBytes = ms.ToArray();
                        _enrolmentSignature.Image = Convert.ToBase64String(imageBytes);

                    }
                    _enrolmentSignature.FKCatalogStatusId = 1;
                    _enrolmentSignature.FKCreateUserId = Convert.ToInt32(VariablesGlobales.USER_ID);

                    items = new JObject(new JProperty("item", "_enrolmentSignature"), new JProperty("data", (JObject)JToken.FromObject(_enrolmentSignature)));
                    enrolment.Add(items);
                }

                //***Huellas***
                if (personaEnrolarHuellasCapturadas.TotalDedos > 0)
                {
                    List<EN.EnrolmentsHuellas> fp = GetHuellasCapturas();
                    if (fp != null)
                    {
                        objeto["lista"] = JToken.FromObject(fp);
                        items = new JObject(new JProperty("item", "_enrolmentsHuellas"), objeto.Last);
                        enrolment.Add(items);
                    }
                }
                
                //***Creacion de la ruta***
                string path = VariablesGlobales.PathDataLocal;
                if (!Directory.Exists(path)) Directory.CreateDirectory(path);

                
                //***Asignar la ruta y crear el archivo***
                string ruta = Path.Combine(path, secuencia);
                using (StreamWriter file = File.CreateText(@ruta))
                {
                    string json = JsonConvert.SerializeObject(enrolment, Formatting.None);

                    //***Cambiar a encriptación cuando este el servicio de transferencia***
                    string encrypt = Seguridad.GVC.Encriptar(json, "hYjQgsWz");
                    string gvcfile = Convert.ToBase64String(Encoding.ASCII.GetBytes(encrypt));
                    file.Write(gvcfile);
                    file.Flush();
                    file.Close();

                    Utilidades.Log.InsertarLog(Utilidades.Log.ErrorType.Informacion, "Enrolamiento:CrearJSON", "Archivo JSON Creado correctamente", VariablesGlobales.PathDataLog);
                }
                if (Directory.Exists(Path.Combine(path, lblNumeroTransaccion.Text.Trim()))) 
                    Directory.Delete(Path.Combine(path, lblNumeroTransaccion.Text.Trim()), true);
                
                return true;
            }
            catch (Exception ex)
            {
                Utilidades.Log.InsertarLog(Utilidades.Log.ErrorType.Error, "frmInsPrimariaEntrada::CrearJSON)", Utilidades.Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                return false;
            }

        }

        bool GuardarPersonaEnLinea()
        {
            try
            {
                //***Buscar persona para verificar si existe***
                ssForm.ShowWaitForm();
                ssForm.SetWaitFormCaption("Guardando");
                
                JArray person = new JArray();
                person = RequestCertificate.GetPersonByDocument(Convert.ToInt32(lueTipoDocumento.EditValue), txtIdentificacion.Text.Trim(),
                   txtPrimerNombre.Text.Trim(), txtPrimerApellido.Text.Trim(), dtmFechaNacimiento.DateTime.Date, Convert.ToInt32(lueSexo.EditValue));

                if (person.Count > 0)
                {
                    foreach (JObject element in person)
                    {
                        if (element["PersonId"].ToString() != null)
                        {
                            frmHitIdentidad.arPersona = person;
                            frmHitIdentidad frm = new frmHitIdentidad();
                            frm.ShowDialog();
                        }
                    }
                    return false;
                }


                //**Objeto JSON para anidar todas las tablas**
                JObject json = new JObject();

                //***Persons***
                NewPerson.PersonsRoot prsRoot = new NewPerson.PersonsRoot();
                NewPerson.persons prFields = new NewPerson.persons();
                JArray jaPrs = new JArray();

                prFields.FirstName = txtPrimerNombre.Text.Trim();
                prFields.SecondName = txtSegundoNombre.Text.Trim();
                prFields.FirstLastName = txtPrimerApellido.Text.Trim();
                prFields.SecondLastName = txtSegundoApellido.Text.Trim();
                prFields.DateOfBirth = (dtmFechaNacimiento.EditValue != null) ? dtmFechaNacimiento.DateTime.Date : DateTime.MinValue;
                prFields.CreationDate = Utilities.getFechaActual();
                prFields.ModifyDate = Utilities.getFechaActual();
                prFields.PersonalMovilPhone = txtMovil.Text.Trim();
                prFields.HomePhone = txtTelefono.Text.Trim();
                prFields.Address = RequestCertificate.VerificarCaracteresEspeciales(meDireccionCompleta.Text.Trim());
                prFields.WorkPhone = txtTelefonoTrabajo.Text.Trim();
                prFields.WorkAddress = meDireccionTrabajo.Text.Trim();
                prFields.PersonalEmail = txtCorreo.Text.Trim();
                prFields.WorkEmail = txtCorreoTrabajo.Text.Trim();
                prFields.FKSexId = Convert.ToInt32(lueSexo.EditValue);
                prFields.FKCivilStatusId = Convert.ToInt32(lueEstadoCivil.EditValue);
                prFields.FKPersonProfessionsId = Convert.ToInt32(lueProfesion.EditValue);
                prFields.FKEducationLevelId = Convert.ToInt32(lueNivelEducativo.EditValue);
                prFields.FKStateCityId = Convert.ToInt32(lueMunicipios.EditValue);
                prFields.FKStateCountryId = Convert.ToInt32(lueDepartamentos.EditValue);
                prFields.FKNeighborhoodId = Convert.ToInt32(lueColonia.EditValue);
                prFields.FKNationalityId = Convert.ToInt32(lueNacionalidad.EditValue);
                prFields.FKCatalogStatusId = 1;
                prFields.FKCountryId = Convert.ToInt32(luePaisNacimiento.EditValue);
                prFields.FKCountryResidenceId = Convert.ToInt32(luePaisResidencia.EditValue);
                prFields.FKCreateUserId = Convert.ToInt32(VariablesGlobales.USER_ID);

                jaPrs.Add((JObject)JToken.FromObject(prFields));

                prsRoot.PersonsName = jaPrs;
                JProperty Person = new JProperty("Person", prsRoot.PersonsName);

                json.Add(Person);
                //------------------------------------------------------------------------------------------


                //***Enrolments***
                NewPerson.EnrolmentsRoot erRoot = new NewPerson.EnrolmentsRoot();
                NewPerson.enrolments erFields = new NewPerson.enrolments();
                JArray jaEr = new JArray();

                erFields.DocumentNumber = txtIdentificacion.Text.Trim();
                erFields.FPCaptured = totalDedos;
                erFields.Attachments = Documentos.Count();
                erFields.RegisteredAfis = nistEnviado;
                erFields.CreationDate = Utilities.getFechaActual();
                erFields.ModifyDate = Utilities.getFechaActual();
                erFields.NoPresential = (!chkNoPresencial.Checked) ? false : true;
                erFields.FKDocumentType = Convert.ToInt32(lueTipoDocumento.EditValue);
                erFields.FKWorkStationId = Convert.ToInt32(VariablesGlobales.WS_ID);
                erFields.FKCreationUserId = Convert.ToInt32(VariablesGlobales.USER_ID);
                erFields.FKOfficeId = VariablesGlobales.OFFICE_ID;

                jaEr.Add((JObject)JToken.FromObject(erFields));

                erRoot.EnrolmentsName = jaEr;
                JProperty Enrolments = new JProperty("Enrolments", erRoot.EnrolmentsName);

                json.Add(Enrolments);
                //------------------------------------------------------------------------------------------


                //***PersonPictures***
                NewPerson.PersonPicturesRoot ppRoot = new NewPerson.PersonPicturesRoot();
                NewPerson.personPictures ppFields = new NewPerson.personPictures();
                JArray jaPp = new JArray();

                ppFields.Image = Convert.ToBase64String(ExodusBcBase.Utilities.ImageToByteArrayJpg(picFotoCapturada.Image));
                ppFields.CreationDate = ExodusBcBase.Utilities.getFechaActual();

                jaPp.Add((JObject)JToken.FromObject(ppFields));

                ppRoot.PersonPicturesName = jaPp;
                JProperty Pictures = new JProperty("Pictures", ppRoot.PersonPicturesName);

                json.Add(Pictures);
                //------------------------------------------------------------------------------------------


                //***PersonSignatures***
                JProperty Signatures = null;
                if (sigPlusNET1.NumberOfTabletPoints() != 0)
                {
                    NewPerson.PersonSignaturesRoot psRoot = new NewPerson.PersonSignaturesRoot();
                    NewPerson.personSignatures psFields = new NewPerson.personSignatures();
                    JArray jaPs = new JArray();

                    psFields.Image = Convert.ToBase64String(ExodusBcBase.Utilities.ImageToByteArrayJpg(firmaCapturada));
                    psFields.CreationDate = ExodusBcBase.Utilities.getFechaActual();
                    psFields.FKCatalogStatusId = 1;
                    psFields.FKCreateUserId = Convert.ToInt32(VariablesGlobales.USER_ID);

                    jaPs.Add((JObject)JToken.FromObject(psFields));

                    psRoot.PersonSignaturesName = jaPs;
                    Signatures = new JProperty("Signatures", psRoot.PersonSignaturesName);
                    json.Add(Signatures);
                    //------------------------------------------------------------------------------------------
                }


                //***Huellas***
                JProperty lstHuellas = null;
                if (personaEnrolarHuellasCapturadas.TotalDedos > 0)
                {
                    List<EN.EnrolmentsHuellas> lstFpr = GetHuellasCapturas();
                    NewPerson.HuellasEnrolmentRoot fpRoot = new NewPerson.HuellasEnrolmentRoot();
                    NewPerson.huellasEnrolment fpFields = new NewPerson.huellasEnrolment();
                    JArray jaFp = new JArray();
                    if (lstFpr != null)
                    {
                        foreach (ClasesPersonalizadas.Dedos dedo in personaEnrolarHuellasCapturadas.Dedos)
                        {
                            fpFields.Dedo = dedo.Dedo;
                            fpFields.Estado = dedo.Estado;
                            fpFields.Calidad = dedo.Calidad;
                            if (dedo.Estado == 0) fpFields.WSQ = Convert.ToBase64String(dedo.WSQ);
                            else fpFields.WSQ = "";
                            jaFp.Add((JObject)JToken.FromObject(fpFields));
                        }
                        fpRoot.HuellasEnrolmentName = jaFp;
                        lstHuellas = new JProperty("Huellas", fpRoot.HuellasEnrolmentName);
                        json.Add(lstHuellas);
                    }
                }
                //------------------------------------------------------------------------------------------

                string jsonPerson = JsonConvert.SerializeObject(json, Formatting.Indented);
                File.WriteAllText(VariablesGlobales.PathDataSystem + "jsonPerson.json", jsonPerson);

                //***Guardar Registro***
                bool InsertarPersona = web.AddNewPerson(VariablesGlobales.TOKEN, jsonPerson, ExodusBc.UI.ExodusBcBase.Utilities.getHostName());
                if (InsertarPersona) return true;
                else return false;

                ssForm.CloseWaitForm();
            }
            catch (Exception ex)
            {
                if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                Utilidades.Log.InsertarLog(Utilidades.Log.ErrorType.Error, "frmInsPrimariaEntrada::GuardarPersonaEnLinea)", Utilidades.Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                return false;
            }
        }

        private void btnReiniciarEscaner_Click(object sender, EventArgs e)
        {
            CLSFPCaptureDllWrapper.CLS_Terminate();
            CLSFPCaptureDllWrapper.CLS_Clear();
            CLSFPCaptureDllWrapper.CLS_Initialize();
        }

        public void CerrarBandejaEnrolamiento()
        {
            cs500e.cs500eInicializado = -1; 
            try
            {
                CLSFPCaptureDllWrapper.CLS_Terminate();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("El Scanner CS500E no fue entontrado, o no se ha instalado.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Utilidades.Log.InsertarLog(Utilidades.Log.ErrorType.Error, "frmClientes::CrearClienteGP", Utilidades.Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }

            Close();
        }

        private void chknoPuedeFirmar_CheckedChanged(object sender, EventArgs e)
        {
            if (chknoPuedeFirmar.Checked)
            {
                if (sigPlusNET1.NumberOfTabletPoints() > 0)
                {
                    if (XtraMessageBox.Show("¿La firma capturada se borrará, está seguro de continuar?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    {
                        chknoPuedeFirmar.CheckedChanged -= chknoPuedeFirmar_CheckedChanged;
                        chknoPuedeFirmar.Checked = false;
                        chknoPuedeFirmar.CheckedChanged += chknoPuedeFirmar_CheckedChanged;
                        return;
                    }
                    else
                    {
                        sigPlusNET1.ClearTablet();
                        sigPlusNET1.BackgroundImage = null;
                    }
                }
                sigPlusNET1.Enabled = false;
                meObservacionFirma.Select();
            }
            else
            {
                sigPlusNET1.Enabled = true;
                meObservacionFirma.Text = "";
            }
        }

        private void sbtnBorrarFirma_Click(object sender, EventArgs e)
        {
            sigPlusNET1.ClearTablet();
            sigPlusNET1.BackgroundImage = null;
            sigPlusNET1.SetJustifyMode(0);
        }

        private void txtSegundoNombre_Leave(object sender, EventArgs e)
        {
            var s = Regex.Replace(txtSegundoNombre.Text, "Ñ", "N");
            txtSegundoNombre.Text = s;
        }

        private void txtPrimerNombre_Leave(object sender, EventArgs e)
        {
            var s = Regex.Replace(txtPrimerNombre.Text, "Ñ", "N");
            txtPrimerNombre.Text = s;
        }

        private void txtPrimerApellido_Leave(object sender, EventArgs e)
        {
            var s = Regex.Replace(txtPrimerApellido.Text, "Ñ", "N");
            txtPrimerApellido.Text = s;
        }

        private void txtSegundoApellido_Leave(object sender, EventArgs e)
        {
            var s = Regex.Replace(txtSegundoApellido.Text, "Ñ", "N");
            txtSegundoApellido.Text = s;
        }

        private void meDireccionCompleta_Leave(object sender, EventArgs e)
        {
            var s = Regex.Replace(meDireccionCompleta.Text, "ñ", "n");
            s = Regex.Replace(s, "Ñ", "N");
            s = Regex.Replace(s, @"[^0-9A-Za-znN, ]", "").Trim();
            meDireccionCompleta.Text = s;
        }

        private void lueTipoDocumento_EditValueChanged(object sender, EventArgs e)
        {
            txtIdentificacion.Text = null;
            if (Convert.ToInt32(lueTipoDocumento.EditValue) == (int)DOCUMENTTYPELN.DocumentType.IDENTIDAD)
            {
                txtIdentificacion.Properties.Mask.EditMask = "0000-0000-00000";
                txtIdentificacion.Properties.Mask.MaskType = MaskType.Simple;
            }
            else if (Convert.ToInt32(lueTipoDocumento.EditValue) == (int)DOCUMENTTYPELN.DocumentType.RTN)
            {
                txtIdentificacion.Properties.Mask.EditMask = "0000-0000-000000";
                txtIdentificacion.Properties.Mask.MaskType = MaskType.Simple;
            }
            else
            {
                txtIdentificacion.Properties.Mask.MaskType = MaskType.None;
                txtIdentificacion.Properties.CharacterCasing = CharacterCasing.Upper;
            }
            txtIdentificacion.Focus();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            txtPrimerNombre.Text = "CARLOS";
            txtSegundoNombre.Text = "DANIEL";
            txtPrimerApellido.Text = "ORTEZ";
            txtSegundoApellido.Text = "CANALES";
            lueSexo.ItemIndex = 1;
            dtmFechaNacimiento.Text = "16/08/1990";
            lueEstadoCivil.ItemIndex = 1;
            lueDepartamentos.ItemIndex = 1;
            lueMunicipios.ItemIndex = 101;
            lueColonia.ItemIndex = 1;
            meDireccionCompleta.Text = "COLONIA DE PRUEBA";
            txtTelefono.Text = "22645147";
            txtMovil.Text = "89745511";
            txtCorreo.Text = "carlos@gmail.com";
            lueProfesion.ItemIndex = 1;
            lueNivelEducativo.ItemIndex = 2;
            txtTelefonoTrabajo.Text = "22658545";
            meDireccionTrabajo.Text = "DIRECIÓN PRUEBA";
            txtCorreoTrabajo.Text = "carlos@gmail.com";
        }

        private void tabEnrolamiento_Click(object sender, EventArgs e)
        {

        }

        private void chkNoPresencial_CheckedChanged(object sender, EventArgs e)
        {
            picFotoCapturada.Image = null;
            if (chkNoPresencial.Checked)
            {
                sbtnIniciarCamara.SendToBack();
                btnAdjuntarFoto.BringToFront();
                btnRotar.Visible = true;
            }
            else
            {
                sbtnIniciarCamara.BringToFront();
                btnAdjuntarFoto.SendToBack();
                btnRotar.Visible = false;
            }
        }

        private void navigationPage1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnRotar_Click(object sender, EventArgs e)
        {
            if (picFotoCapturada != null)
            {
                int rotacion = 0;
                rotacion += 1;

                if (rotacion == 1)
                {
                    Image image = picFotoCapturada.Image.Clone() as Image;
                    image.RotateFlip(RotateFlipType.Rotate90FlipNone);
                    picFotoCapturada.Image.Dispose();
                    picFotoCapturada.Image = image;
                }
                else if (rotacion == 2)
                {
                    Image image = picFotoCapturada.Image.Clone() as Image;
                    image.RotateFlip(RotateFlipType.Rotate270FlipNone);
                    picFotoCapturada.Image.Dispose();
                    picFotoCapturada.Image = image;
                }
                else if (rotacion == 3)
                {
                    Image image = picFotoCapturada.Image.Clone() as Image;
                    image.RotateFlip(RotateFlipType.Rotate180FlipNone);
                    picFotoCapturada.Image.Dispose();
                    picFotoCapturada.Image = image;
                }
                else if (rotacion == 4)
                {
                    Image image = picFotoCapturada.Image.Clone() as Image;
                    image.RotateFlip(RotateFlipType.Rotate90FlipNone);
                    picFotoCapturada.Image.Dispose();
                    picFotoCapturada.Image = image;
                    rotacion = 0;
                }

            }
        }

        private async void btnSyncCatalogos_Click(object sender, EventArgs e)
        {
            try
            {
                Task.Run(async () =>
                {
                    await VariablesGlobales.SincronizarCatalogos();
                    Init();
                });
                XtraMessageBox.Show("Sicronización finalizada.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Hubo un error al sicronizar los catálogos. Intente en otro momento.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmCertificados::btnSyncCatalogos_Click", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }
    }

}