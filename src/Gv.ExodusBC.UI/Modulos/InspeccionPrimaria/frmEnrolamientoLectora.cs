﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace Gv.ExodusBc.UI.Modulos.InspeccionPrimaria
{
	public partial class frmEnrolamientoLectora : DevExpress.XtraEditors.XtraForm
	{

		public static string Respuesta = string.Empty;
        public static bool LecturaOK;
        public static LecturaTarjetaIdentidad objLectura = null;
		int sinDatos = 0;


		public frmEnrolamientoLectora()
		{
			InitializeComponent();
            Respuesta = string.Empty;
            sinDatos = 0;
            tmrLeerDatos.Interval = 1;
            tmrLeerDatos.Enabled = false;
            LecturaOK = false;
		}

		private void txtCapturaDatos_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (txtCapturaDatos.Focused == false)
			{
				txtCapturaDatos.Focus();
				txtCapturaDatos.Text = e.KeyChar.ToString();
				txtCapturaDatos.SelectionStart = txtCapturaDatos.Text.Length;
				e.Handled = true;
			}

			Respuesta += e.KeyChar.ToString();
			sinDatos = 0;
			e.KeyChar = Convert.ToChar(0);
			if(!tmrLeerDatos.Enabled)
			{
				tmrLeerDatos.Enabled = true;
				lblTitulo.Text = "Por favor espere...";
			}
		}

		private void tmrLeerDatos_Tick(object sender, EventArgs e)
		{
            sinDatos += 1;
			if(sinDatos == 3)
			{
                tmrLeerDatos.Enabled = false;
                cargarInfoLeida();
				this.Close();
			}
		}
        void cargarInfoLeida()
        {
            try
            {
                string LeerArchivo = string.Empty;
                LeerArchivo = Respuesta;
                List<string> LecturaObtenida = new List<string>(LeerArchivo.Split(','));

                if (LecturaObtenida.Count == 1)
                    return;
                while (LecturaObtenida.Count > 22)
                    LecturaObtenida.RemoveAt(LecturaObtenida.Count - 1);

                foreach (string item in LecturaObtenida)
                    LeerArchivo = item;

                if (LecturaObtenida.Count > 23)
                {
                    LecturaOK = false;
                    return;
                }
                objLectura = new LecturaTarjetaIdentidad();
                //Identidad
                objLectura.NoIdentidad = string.Format("{0}{1}{2}", LecturaObtenida[2].Substring(0, 4), LecturaObtenida[2].Substring(4, 4), (LecturaObtenida[2].Substring(8, 5)).ToUpper());

                //Nombres
                objLectura.Nombre1 = LecturaObtenida[3].ToUpper();
                if (LecturaObtenida[4] != null) objLectura.Nombre2 = LecturaObtenida[4].ToUpper();


                //Apellidos
                objLectura.Apellido1 = LecturaObtenida[5].ToUpper();
                if (LecturaObtenida[6] != null) objLectura.Apellido2 = LecturaObtenida[6].ToUpper();

                //Nacimiento
                //luePaisNacimiento.EditValue = "HND";
                objLectura.Departamento = LecturaObtenida[8];
                objLectura.Municipio = LecturaObtenida[8] + LecturaObtenida[9];
                objLectura.FechaNacimiento = string.Format("{0}/{1}/{2}", LecturaObtenida[10].Substring(0, 2), LecturaObtenida[10].Substring(2, 2), LecturaObtenida[10].Substring(4, 4));

                //Nombres padres
                objLectura.PadreNombre = string.Format("{0} {1}", LecturaObtenida[13], LecturaObtenida[14]);
                objLectura.PadreApellido = string.Format("{0} {1}", LecturaObtenida[11], LecturaObtenida[12]);

                objLectura.MadreNombre = string.Format("{0} {1}", LecturaObtenida[18], LecturaObtenida[19]);
                objLectura.MadreApellido = string.Format("{0} {1}", LecturaObtenida[16], LecturaObtenida[17]);
                LecturaOK = true;
            }
            catch (Exception)
            {
                LecturaOK = false;
            }
           

        }

        public class LecturaTarjetaIdentidad
        {
            public string NoIdentidad { get; set; }
            public string Nombre1 { get; set; }
            public string Nombre2 { get; set; }
            public string Apellido1 { get; set; }
            public string Apellido2 { get; set; }
            public string FechaNacimiento { get; set; }
            public string PaisNacimiento { get; set; }
            public string Departamento { get; set; }
            public string Municipio { get; set; }
            public string PadreNombre { get; set; }
            public string PadreApellido { get; set; }
            public string MadreNombre { get; set; }
            public string MadreApellido { get; set; }

        }
	}
}