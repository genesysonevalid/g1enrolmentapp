﻿namespace Gv.ExodusBc.UI.Modulos.InspeccionPrimaria
{
    partial class frmBusquedaPersonas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBusquedaPersonas));
            this.lueDocumentoTipo = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtIdentificacion = new DevExpress.XtraEditors.TextEdit();
            this.btnBuscarPersona = new DevExpress.XtraEditors.SimpleButton();
            this.lblIdentificacion = new DevExpress.XtraEditors.LabelControl();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtPrimerNombre = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtPrimerApellido = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.dtmFechaNacimiento = new DevExpress.XtraEditors.DateEdit();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnLeerIdentidad = new DevExpress.XtraEditors.SimpleButton();
            this.btnLimpiar = new DevExpress.XtraEditors.SimpleButton();
            this.gcPersonas = new DevExpress.XtraGrid.GridControl();
            this.gvPersonas = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.PersonId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Foto = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TipoDocumento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NumeroDocumento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Nombres = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Apellidos = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FechaNacimiento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnActualzar = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ribeVer = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.lblAtencion = new DevExpress.XtraEditors.LabelControl();
            this.picAtencion = new DevExpress.XtraEditors.PictureEdit();
            this.ssForm = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::Gv.ExodusBc.UI.frmWaitForm), true, true);
            ((System.ComponentModel.ISupportInitialize)(this.lueDocumentoTipo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdentificacion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrimerNombre.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrimerApellido.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtmFechaNacimiento.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtmFechaNacimiento.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcPersonas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvPersonas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribeVer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAtencion.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // lueDocumentoTipo
            // 
            this.lueDocumentoTipo.Location = new System.Drawing.Point(18, 58);
            this.lueDocumentoTipo.Margin = new System.Windows.Forms.Padding(4);
            this.lueDocumentoTipo.Name = "lueDocumentoTipo";
            this.lueDocumentoTipo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.lueDocumentoTipo.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueDocumentoTipo.Properties.Appearance.Options.UseBackColor = true;
            this.lueDocumentoTipo.Properties.Appearance.Options.UseFont = true;
            this.lueDocumentoTipo.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lueDocumentoTipo.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lueDocumentoTipo.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.lueDocumentoTipo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueDocumentoTipo.Properties.NullText = "";
            this.lueDocumentoTipo.Size = new System.Drawing.Size(238, 26);
            this.lueDocumentoTipo.TabIndex = 1;
            this.lueDocumentoTipo.EditValueChanged += new System.EventHandler(this.lueDocumentoTipo_EditValueChanged);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Appearance.Options.UseForeColor = true;
            this.labelControl1.Appearance.Options.UseTextOptions = true;
            this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.Location = new System.Drawing.Point(18, 27);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(130, 26);
            this.labelControl1.TabIndex = 132;
            this.labelControl1.Text = "Tipo Documento:";
            // 
            // txtIdentificacion
            // 
            this.txtIdentificacion.Location = new System.Drawing.Point(18, 124);
            this.txtIdentificacion.Margin = new System.Windows.Forms.Padding(4);
            this.txtIdentificacion.Name = "txtIdentificacion";
            this.txtIdentificacion.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIdentificacion.Properties.Appearance.Options.UseFont = true;
            this.txtIdentificacion.Properties.Mask.EditMask = "0000-0000-00000";
            this.txtIdentificacion.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
            this.txtIdentificacion.Properties.Mask.SaveLiteral = false;
            this.txtIdentificacion.Properties.MaxLength = 20;
            this.txtIdentificacion.Size = new System.Drawing.Size(238, 26);
            this.txtIdentificacion.TabIndex = 2;
            // 
            // btnBuscarPersona
            // 
            this.btnBuscarPersona.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.btnBuscarPersona.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuscarPersona.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.btnBuscarPersona.Appearance.Options.UseBackColor = true;
            this.btnBuscarPersona.Appearance.Options.UseFont = true;
            this.btnBuscarPersona.Appearance.Options.UseForeColor = true;
            this.btnBuscarPersona.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnBuscarPersona.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_search_32x32;
            this.btnBuscarPersona.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnBuscarPersona.Location = new System.Drawing.Point(556, 115);
            this.btnBuscarPersona.Margin = new System.Windows.Forms.Padding(4);
            this.btnBuscarPersona.Name = "btnBuscarPersona";
            this.btnBuscarPersona.Size = new System.Drawing.Size(180, 35);
            this.btnBuscarPersona.TabIndex = 6;
            this.btnBuscarPersona.Text = "&Buscar ";
            this.btnBuscarPersona.Click += new System.EventHandler(this.btnBuscarPersona_Click);
            // 
            // lblIdentificacion
            // 
            this.lblIdentificacion.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIdentificacion.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lblIdentificacion.Appearance.Options.UseFont = true;
            this.lblIdentificacion.Appearance.Options.UseForeColor = true;
            this.lblIdentificacion.Appearance.Options.UseTextOptions = true;
            this.lblIdentificacion.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblIdentificacion.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblIdentificacion.Location = new System.Drawing.Point(18, 95);
            this.lblIdentificacion.Margin = new System.Windows.Forms.Padding(4);
            this.lblIdentificacion.Name = "lblIdentificacion";
            this.lblIdentificacion.Size = new System.Drawing.Size(130, 26);
            this.lblIdentificacion.TabIndex = 131;
            this.lblIdentificacion.Text = "N. Identificación:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(139, 34);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(16, 17);
            this.label6.TabIndex = 371;
            this.label6.Text = "*";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(143, 100);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(16, 17);
            this.label1.TabIndex = 372;
            this.label1.Text = "*";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Appearance.Options.UseForeColor = true;
            this.labelControl2.Appearance.Options.UseTextOptions = true;
            this.labelControl2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl2.Location = new System.Drawing.Point(284, 25);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(135, 26);
            this.labelControl2.TabIndex = 375;
            this.labelControl2.Text = "Primer Nombre:";
            // 
            // txtPrimerNombre
            // 
            this.txtPrimerNombre.Location = new System.Drawing.Point(284, 58);
            this.txtPrimerNombre.Margin = new System.Windows.Forms.Padding(4);
            this.txtPrimerNombre.Name = "txtPrimerNombre";
            this.txtPrimerNombre.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtPrimerNombre.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrimerNombre.Properties.Appearance.Options.UseBackColor = true;
            this.txtPrimerNombre.Properties.Appearance.Options.UseFont = true;
            this.txtPrimerNombre.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtPrimerNombre.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtPrimerNombre.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtPrimerNombre.Properties.Mask.BeepOnError = true;
            this.txtPrimerNombre.Properties.Mask.EditMask = "[A-ZÑ]+";
            this.txtPrimerNombre.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtPrimerNombre.Properties.MaxLength = 100;
            this.txtPrimerNombre.Size = new System.Drawing.Size(250, 26);
            this.txtPrimerNombre.TabIndex = 3;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Appearance.Options.UseForeColor = true;
            this.labelControl3.Appearance.Options.UseTextOptions = true;
            this.labelControl3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl3.Location = new System.Drawing.Point(284, 95);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(135, 26);
            this.labelControl3.TabIndex = 377;
            this.labelControl3.Text = "Primer Apellido:";
            // 
            // txtPrimerApellido
            // 
            this.txtPrimerApellido.Location = new System.Drawing.Point(284, 124);
            this.txtPrimerApellido.Margin = new System.Windows.Forms.Padding(4);
            this.txtPrimerApellido.Name = "txtPrimerApellido";
            this.txtPrimerApellido.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtPrimerApellido.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrimerApellido.Properties.Appearance.Options.UseBackColor = true;
            this.txtPrimerApellido.Properties.Appearance.Options.UseFont = true;
            this.txtPrimerApellido.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtPrimerApellido.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtPrimerApellido.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtPrimerApellido.Properties.Mask.EditMask = "[A-ZÑ]+";
            this.txtPrimerApellido.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtPrimerApellido.Properties.MaxLength = 100;
            this.txtPrimerApellido.Size = new System.Drawing.Size(250, 26);
            this.txtPrimerApellido.TabIndex = 4;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Appearance.Options.UseForeColor = true;
            this.labelControl4.Appearance.Options.UseTextOptions = true;
            this.labelControl4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl4.Location = new System.Drawing.Point(556, 34);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(180, 26);
            this.labelControl4.TabIndex = 379;
            this.labelControl4.Text = "Fecha de Nacimiento:";
            // 
            // dtmFechaNacimiento
            // 
            this.dtmFechaNacimiento.EditValue = null;
            this.dtmFechaNacimiento.Location = new System.Drawing.Point(556, 58);
            this.dtmFechaNacimiento.Margin = new System.Windows.Forms.Padding(4);
            this.dtmFechaNacimiento.Name = "dtmFechaNacimiento";
            this.dtmFechaNacimiento.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.dtmFechaNacimiento.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtmFechaNacimiento.Properties.Appearance.Options.UseBackColor = true;
            this.dtmFechaNacimiento.Properties.Appearance.Options.UseFont = true;
            this.dtmFechaNacimiento.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dtmFechaNacimiento.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dtmFechaNacimiento.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.dtmFechaNacimiento.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtmFechaNacimiento.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtmFechaNacimiento.Size = new System.Drawing.Size(180, 26);
            this.dtmFechaNacimiento.TabIndex = 5;
            // 
            // panelControl1
            // 
            this.panelControl1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(254)))), ((int)(((byte)(255)))));
            this.panelControl1.Appearance.Options.UseBackColor = true;
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.btnLeerIdentidad);
            this.panelControl1.Controls.Add(this.btnLimpiar);
            this.panelControl1.Controls.Add(this.label6);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.lueDocumentoTipo);
            this.panelControl1.Controls.Add(this.dtmFechaNacimiento);
            this.panelControl1.Controls.Add(this.lblIdentificacion);
            this.panelControl1.Controls.Add(this.btnBuscarPersona);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.label1);
            this.panelControl1.Controls.Add(this.txtPrimerApellido);
            this.panelControl1.Controls.Add(this.txtIdentificacion);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.txtPrimerNombre);
            this.panelControl1.Location = new System.Drawing.Point(15, 50);
            this.panelControl1.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1085, 186);
            this.panelControl1.TabIndex = 383;
            // 
            // btnLeerIdentidad
            // 
            this.btnLeerIdentidad.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.btnLeerIdentidad.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLeerIdentidad.Appearance.Options.UseBackColor = true;
            this.btnLeerIdentidad.Appearance.Options.UseFont = true;
            this.btnLeerIdentidad.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnLeerIdentidad.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.id_card;
            this.btnLeerIdentidad.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnLeerIdentidad.Location = new System.Drawing.Point(880, 51);
            this.btnLeerIdentidad.Margin = new System.Windows.Forms.Padding(4);
            this.btnLeerIdentidad.Name = "btnLeerIdentidad";
            this.btnLeerIdentidad.Size = new System.Drawing.Size(182, 35);
            this.btnLeerIdentidad.TabIndex = 380;
            this.btnLeerIdentidad.Text = "&Leer Identidad";
            this.btnLeerIdentidad.Click += new System.EventHandler(this.btnLeerIdentidad_Click);
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.Appearance.BackColor = System.Drawing.Color.White;
            this.btnLimpiar.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLimpiar.Appearance.Options.UseBackColor = true;
            this.btnLimpiar.Appearance.Options.UseFont = true;
            this.btnLimpiar.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnLimpiar.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_eraser_32x32;
            this.btnLimpiar.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnLimpiar.Location = new System.Drawing.Point(880, 117);
            this.btnLimpiar.Margin = new System.Windows.Forms.Padding(4);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(182, 35);
            this.btnLimpiar.TabIndex = 7;
            this.btnLimpiar.Text = "&Limpiar";
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // gcPersonas
            // 
            this.gcPersonas.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(4);
            this.gcPersonas.Location = new System.Drawing.Point(18, 253);
            this.gcPersonas.LookAndFeel.SkinName = "Office 2016 Colorful";
            this.gcPersonas.LookAndFeel.UseDefaultLookAndFeel = false;
            this.gcPersonas.MainView = this.gvPersonas;
            this.gcPersonas.Margin = new System.Windows.Forms.Padding(4);
            this.gcPersonas.Name = "gcPersonas";
            this.gcPersonas.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.ribeVer});
            this.gcPersonas.Size = new System.Drawing.Size(1073, 180);
            this.gcPersonas.TabIndex = 9;
            this.gcPersonas.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvPersonas,
            this.gridView1});
            // 
            // gvPersonas
            // 
            this.gvPersonas.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gvPersonas.Appearance.HeaderPanel.Options.UseFont = true;
            this.gvPersonas.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.gvPersonas.Appearance.OddRow.Options.UseBackColor = true;
            this.gvPersonas.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9F);
            this.gvPersonas.Appearance.Row.Options.UseFont = true;
            this.gvPersonas.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.PersonId,
            this.Foto,
            this.TipoDocumento,
            this.NumeroDocumento,
            this.Nombres,
            this.Apellidos,
            this.FechaNacimiento,
            this.btnActualzar});
            this.gvPersonas.GridControl = this.gcPersonas;
            this.gvPersonas.Name = "gvPersonas";
            this.gvPersonas.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gvPersonas.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gvPersonas.OptionsView.ShowGroupPanel = false;
            this.gvPersonas.RowHeight = 60;
            // 
            // PersonId
            // 
            this.PersonId.Caption = "PersonId";
            this.PersonId.FieldName = "PersonId";
            this.PersonId.Name = "PersonId";
            // 
            // Foto
            // 
            this.Foto.AppearanceCell.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Foto.AppearanceCell.Options.UseFont = true;
            this.Foto.AppearanceHeader.Font = new System.Drawing.Font("Segoe UI Semibold", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Foto.AppearanceHeader.Options.UseFont = true;
            this.Foto.Caption = "Foto";
            this.Foto.FieldName = "Foto";
            this.Foto.MaxWidth = 100;
            this.Foto.MinWidth = 100;
            this.Foto.Name = "Foto";
            this.Foto.OptionsColumn.ReadOnly = true;
            this.Foto.Visible = true;
            this.Foto.VisibleIndex = 0;
            this.Foto.Width = 100;
            // 
            // TipoDocumento
            // 
            this.TipoDocumento.AppearanceCell.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TipoDocumento.AppearanceCell.Options.UseFont = true;
            this.TipoDocumento.AppearanceHeader.Font = new System.Drawing.Font("Segoe UI Semibold", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TipoDocumento.AppearanceHeader.Options.UseFont = true;
            this.TipoDocumento.Caption = "Tipo Documento";
            this.TipoDocumento.FieldName = "TipoDocumento";
            this.TipoDocumento.Name = "TipoDocumento";
            this.TipoDocumento.OptionsColumn.ReadOnly = true;
            this.TipoDocumento.Visible = true;
            this.TipoDocumento.VisibleIndex = 1;
            // 
            // NumeroDocumento
            // 
            this.NumeroDocumento.AppearanceCell.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NumeroDocumento.AppearanceCell.Options.UseFont = true;
            this.NumeroDocumento.AppearanceHeader.Font = new System.Drawing.Font("Segoe UI Semibold", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NumeroDocumento.AppearanceHeader.Options.UseFont = true;
            this.NumeroDocumento.Caption = "NumeroDocumento";
            this.NumeroDocumento.FieldName = "NumeroDocumento";
            this.NumeroDocumento.Name = "NumeroDocumento";
            this.NumeroDocumento.OptionsColumn.ReadOnly = true;
            this.NumeroDocumento.Visible = true;
            this.NumeroDocumento.VisibleIndex = 2;
            // 
            // Nombres
            // 
            this.Nombres.AppearanceCell.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Nombres.AppearanceCell.Options.UseFont = true;
            this.Nombres.AppearanceHeader.Font = new System.Drawing.Font("Segoe UI Semibold", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Nombres.AppearanceHeader.Options.UseFont = true;
            this.Nombres.Caption = "Nombres";
            this.Nombres.FieldName = "Nombres";
            this.Nombres.Name = "Nombres";
            this.Nombres.OptionsColumn.ReadOnly = true;
            this.Nombres.Visible = true;
            this.Nombres.VisibleIndex = 3;
            // 
            // Apellidos
            // 
            this.Apellidos.AppearanceCell.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Apellidos.AppearanceCell.Options.UseFont = true;
            this.Apellidos.AppearanceHeader.Font = new System.Drawing.Font("Segoe UI Semibold", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Apellidos.AppearanceHeader.Options.UseFont = true;
            this.Apellidos.Caption = "Apellidos";
            this.Apellidos.FieldName = "Apellidos";
            this.Apellidos.Name = "Apellidos";
            this.Apellidos.OptionsColumn.ReadOnly = true;
            this.Apellidos.Visible = true;
            this.Apellidos.VisibleIndex = 4;
            // 
            // FechaNacimiento
            // 
            this.FechaNacimiento.AppearanceCell.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FechaNacimiento.AppearanceCell.Options.UseFont = true;
            this.FechaNacimiento.AppearanceHeader.Font = new System.Drawing.Font("Segoe UI Semibold", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FechaNacimiento.AppearanceHeader.Options.UseFont = true;
            this.FechaNacimiento.Caption = "Fecha Nacimiento";
            this.FechaNacimiento.FieldName = "FechaNacimiento";
            this.FechaNacimiento.Name = "FechaNacimiento";
            this.FechaNacimiento.OptionsColumn.ReadOnly = true;
            this.FechaNacimiento.Visible = true;
            this.FechaNacimiento.VisibleIndex = 5;
            // 
            // btnActualzar
            // 
            this.btnActualzar.AppearanceCell.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnActualzar.AppearanceCell.Options.UseFont = true;
            this.btnActualzar.AppearanceHeader.Font = new System.Drawing.Font("Segoe UI Semibold", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnActualzar.AppearanceHeader.Options.UseFont = true;
            this.btnActualzar.Caption = "Actualizar";
            this.btnActualzar.ColumnEdit = this.ribeVer;
            this.btnActualzar.Name = "btnActualzar";
            this.btnActualzar.Visible = true;
            this.btnActualzar.VisibleIndex = 6;
            // 
            // ribeVer
            // 
            this.ribeVer.AutoHeight = false;
            editorButtonImageOptions1.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_ver_detalle_28x28;
            this.ribeVer.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.ribeVer.Name = "ribeVer";
            this.ribeVer.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.ribeVer.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ribeVer_ButtonClick);
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gcPersonas;
            this.gridView1.Name = "gridView1";
            // 
            // panelControl2
            // 
            this.panelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl2.Location = new System.Drawing.Point(12, 249);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1085, 190);
            this.panelControl2.TabIndex = 384;
            // 
            // lblAtencion
            // 
            this.lblAtencion.Appearance.Font = new System.Drawing.Font("Segoe UI", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAtencion.Appearance.ForeColor = System.Drawing.Color.Maroon;
            this.lblAtencion.Appearance.Options.UseFont = true;
            this.lblAtencion.Appearance.Options.UseForeColor = true;
            this.lblAtencion.Appearance.Options.UseTextOptions = true;
            this.lblAtencion.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblAtencion.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblAtencion.Location = new System.Drawing.Point(49, 20);
            this.lblAtencion.Margin = new System.Windows.Forms.Padding(4);
            this.lblAtencion.Name = "lblAtencion";
            this.lblAtencion.Size = new System.Drawing.Size(771, 24);
            this.lblAtencion.TabIndex = 458;
            this.lblAtencion.Text = "Es obligatorio ingresar el tipo de documento y el número de identificación para r" +
    "ealizar la búsqueda\r\n";
            // 
            // picAtencion
            // 
            this.picAtencion.Cursor = System.Windows.Forms.Cursors.Default;
            this.picAtencion.EditValue = global::Gv.ExodusBc.UI.Properties.Resources.lightbulb;
            this.picAtencion.Location = new System.Drawing.Point(12, 14);
            this.picAtencion.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.picAtencion.Name = "picAtencion";
            this.picAtencion.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picAtencion.Properties.Appearance.Options.UseBackColor = true;
            this.picAtencion.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.picAtencion.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picAtencion.Size = new System.Drawing.Size(38, 30);
            this.picAtencion.TabIndex = 457;
            // 
            // ssForm
            // 
            this.ssForm.ClosingDelay = 500;
            // 
            // frmBusquedaPersonas
            // 
            this.Appearance.BackColor = System.Drawing.Color.White;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(1112, 465);
            this.Controls.Add(this.gcPersonas);
            this.Controls.Add(this.lblAtencion);
            this.Controls.Add(this.picAtencion);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmBusquedaPersonas";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Busqueda de Personas";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmBusquedaPersonas_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmBusquedaPersonas_FormClosed);
            this.Load += new System.EventHandler(this.frmBusquedaPersonas_Load);
            ((System.ComponentModel.ISupportInitialize)(this.lueDocumentoTipo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdentificacion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrimerNombre.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrimerApellido.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtmFechaNacimiento.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtmFechaNacimiento.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcPersonas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvPersonas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribeVer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAtencion.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.LookUpEdit lueDocumentoTipo;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtIdentificacion;
        private DevExpress.XtraEditors.SimpleButton btnBuscarPersona;
        private DevExpress.XtraEditors.LabelControl lblIdentificacion;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txtPrimerNombre;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txtPrimerApellido;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.DateEdit dtmFechaNacimiento;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton btnLimpiar;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton btnLeerIdentidad;
        private DevExpress.XtraGrid.Columns.GridColumn PersonId;
        private DevExpress.XtraGrid.Columns.GridColumn Foto;
        private DevExpress.XtraGrid.Columns.GridColumn TipoDocumento;
        private DevExpress.XtraGrid.Columns.GridColumn NumeroDocumento;
        private DevExpress.XtraGrid.Columns.GridColumn Nombres;
        private DevExpress.XtraGrid.Columns.GridColumn Apellidos;
        private DevExpress.XtraGrid.Columns.GridColumn FechaNacimiento;
        internal DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit ribeVer;
        internal DevExpress.XtraGrid.Columns.GridColumn btnActualzar;
        internal DevExpress.XtraGrid.Views.Grid.GridView gvPersonas;
        internal DevExpress.XtraGrid.GridControl gcPersonas;
        private DevExpress.XtraEditors.LabelControl lblAtencion;
        private DevExpress.XtraEditors.PictureEdit picAtencion;
        private DevExpress.XtraSplashScreen.SplashScreenManager ssForm;
    }
}