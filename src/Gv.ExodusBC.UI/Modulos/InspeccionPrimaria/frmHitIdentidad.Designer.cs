﻿
namespace Gv.ExodusBc.UI.Modulos.InspeccionPrimaria
{
    partial class frmHitIdentidad
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.picIdentidad = new DevExpress.XtraEditors.PictureEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.lblIdentificacionExistente = new DevExpress.XtraEditors.LabelControl();
            this.lblNombresExistente = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.lblSexoExistente = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.lblNacimientoExistente = new DevExpress.XtraEditors.LabelControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.lblApellidosExistente = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.picIdentidad.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // picIdentidad
            // 
            this.picIdentidad.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picIdentidad.Cursor = System.Windows.Forms.Cursors.Default;
            this.picIdentidad.Location = new System.Drawing.Point(140, 27);
            this.picIdentidad.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.picIdentidad.Name = "picIdentidad";
            this.picIdentidad.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picIdentidad.Properties.Appearance.BorderColor = System.Drawing.Color.SteelBlue;
            this.picIdentidad.Properties.Appearance.Options.UseBackColor = true;
            this.picIdentidad.Properties.Appearance.Options.UseBorderColor = true;
            this.picIdentidad.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.picIdentidad.Properties.NullText = " ";
            this.picIdentidad.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picIdentidad.Properties.ShowMenu = false;
            this.picIdentidad.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.picIdentidad.Size = new System.Drawing.Size(215, 252);
            this.picIdentidad.TabIndex = 315;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Beige;
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label5.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(12, 292);
            this.label5.Name = "label5";
            this.label5.Padding = new System.Windows.Forms.Padding(6);
            this.label5.Size = new System.Drawing.Size(462, 61);
            this.label5.TabIndex = 314;
            this.label5.Text = "Este número de identificación ya fue registrado en el sistema con los siguientes " +
    "datos:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblIdentificacionExistente
            // 
            this.lblIdentificacionExistente.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIdentificacionExistente.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(100)))), ((int)(((byte)(141)))));
            this.lblIdentificacionExistente.Appearance.Options.UseFont = true;
            this.lblIdentificacionExistente.Appearance.Options.UseForeColor = true;
            this.lblIdentificacionExistente.Appearance.Options.UseTextOptions = true;
            this.lblIdentificacionExistente.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblIdentificacionExistente.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblIdentificacionExistente.Location = new System.Drawing.Point(12, 398);
            this.lblIdentificacionExistente.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblIdentificacionExistente.Name = "lblIdentificacionExistente";
            this.lblIdentificacionExistente.Size = new System.Drawing.Size(462, 27);
            this.lblIdentificacionExistente.TabIndex = 325;
            this.lblIdentificacionExistente.Text = "[lblIdentificacionExistente]";
            // 
            // lblNombresExistente
            // 
            this.lblNombresExistente.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombresExistente.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(100)))), ((int)(((byte)(141)))));
            this.lblNombresExistente.Appearance.Options.UseFont = true;
            this.lblNombresExistente.Appearance.Options.UseForeColor = true;
            this.lblNombresExistente.Appearance.Options.UseTextOptions = true;
            this.lblNombresExistente.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblNombresExistente.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblNombresExistente.Location = new System.Drawing.Point(12, 446);
            this.lblNombresExistente.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblNombresExistente.Name = "lblNombresExistente";
            this.lblNombresExistente.Size = new System.Drawing.Size(462, 27);
            this.lblNombresExistente.TabIndex = 318;
            this.lblNombresExistente.Text = "lblNombreExistente]";
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Appearance.ForeColor = System.Drawing.Color.SteelBlue;
            this.labelControl8.Appearance.Options.UseFont = true;
            this.labelControl8.Appearance.Options.UseForeColor = true;
            this.labelControl8.Appearance.Options.UseTextOptions = true;
            this.labelControl8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl8.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl8.Location = new System.Drawing.Point(12, 470);
            this.labelControl8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(73, 27);
            this.labelControl8.TabIndex = 317;
            this.labelControl8.Text = "Apellidos:";
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl14.Appearance.ForeColor = System.Drawing.Color.SteelBlue;
            this.labelControl14.Appearance.Options.UseFont = true;
            this.labelControl14.Appearance.Options.UseForeColor = true;
            this.labelControl14.Appearance.Options.UseTextOptions = true;
            this.labelControl14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl14.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl14.Location = new System.Drawing.Point(12, 376);
            this.labelControl14.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(111, 27);
            this.labelControl14.TabIndex = 324;
            this.labelControl14.Text = "Identificación:";
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl9.Appearance.ForeColor = System.Drawing.Color.SteelBlue;
            this.labelControl9.Appearance.Options.UseFont = true;
            this.labelControl9.Appearance.Options.UseForeColor = true;
            this.labelControl9.Appearance.Options.UseTextOptions = true;
            this.labelControl9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl9.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl9.Location = new System.Drawing.Point(12, 423);
            this.labelControl9.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(73, 27);
            this.labelControl9.TabIndex = 316;
            this.labelControl9.Text = "Nombres:";
            // 
            // lblSexoExistente
            // 
            this.lblSexoExistente.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSexoExistente.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(100)))), ((int)(((byte)(141)))));
            this.lblSexoExistente.Appearance.Options.UseFont = true;
            this.lblSexoExistente.Appearance.Options.UseForeColor = true;
            this.lblSexoExistente.Appearance.Options.UseTextOptions = true;
            this.lblSexoExistente.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblSexoExistente.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblSexoExistente.Location = new System.Drawing.Point(12, 591);
            this.lblSexoExistente.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblSexoExistente.Name = "lblSexoExistente";
            this.lblSexoExistente.Size = new System.Drawing.Size(462, 27);
            this.lblSexoExistente.TabIndex = 323;
            this.lblSexoExistente.Text = "[lblSexoExistente]";
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl13.Appearance.ForeColor = System.Drawing.Color.SteelBlue;
            this.labelControl13.Appearance.Options.UseFont = true;
            this.labelControl13.Appearance.Options.UseForeColor = true;
            this.labelControl13.Appearance.Options.UseTextOptions = true;
            this.labelControl13.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl13.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl13.Location = new System.Drawing.Point(12, 569);
            this.labelControl13.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(111, 27);
            this.labelControl13.TabIndex = 322;
            this.labelControl13.Text = "Sexo:";
            // 
            // lblNacimientoExistente
            // 
            this.lblNacimientoExistente.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNacimientoExistente.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(100)))), ((int)(((byte)(141)))));
            this.lblNacimientoExistente.Appearance.Options.UseFont = true;
            this.lblNacimientoExistente.Appearance.Options.UseForeColor = true;
            this.lblNacimientoExistente.Appearance.Options.UseTextOptions = true;
            this.lblNacimientoExistente.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblNacimientoExistente.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblNacimientoExistente.Location = new System.Drawing.Point(12, 543);
            this.lblNacimientoExistente.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblNacimientoExistente.Name = "lblNacimientoExistente";
            this.lblNacimientoExistente.Size = new System.Drawing.Size(462, 27);
            this.lblNacimientoExistente.TabIndex = 321;
            this.lblNacimientoExistente.Text = "[lblNacimientoExistente]";
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl15.Appearance.ForeColor = System.Drawing.Color.SteelBlue;
            this.labelControl15.Appearance.Options.UseFont = true;
            this.labelControl15.Appearance.Options.UseForeColor = true;
            this.labelControl15.Appearance.Options.UseTextOptions = true;
            this.labelControl15.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl15.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl15.Location = new System.Drawing.Point(12, 521);
            this.labelControl15.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(135, 27);
            this.labelControl15.TabIndex = 320;
            this.labelControl15.Text = "Fecha Nacimiento:";
            // 
            // lblApellidosExistente
            // 
            this.lblApellidosExistente.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblApellidosExistente.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(100)))), ((int)(((byte)(141)))));
            this.lblApellidosExistente.Appearance.Options.UseFont = true;
            this.lblApellidosExistente.Appearance.Options.UseForeColor = true;
            this.lblApellidosExistente.Appearance.Options.UseTextOptions = true;
            this.lblApellidosExistente.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblApellidosExistente.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblApellidosExistente.Location = new System.Drawing.Point(12, 497);
            this.lblApellidosExistente.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblApellidosExistente.Name = "lblApellidosExistente";
            this.lblApellidosExistente.Size = new System.Drawing.Size(462, 27);
            this.lblApellidosExistente.TabIndex = 319;
            this.lblApellidosExistente.Text = "[lblApellidosExistente]";
            // 
            // frmHitIdentidad
            // 
            this.Appearance.BackColor = System.Drawing.Color.White;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(499, 656);
            this.Controls.Add(this.lblIdentificacionExistente);
            this.Controls.Add(this.lblNombresExistente);
            this.Controls.Add(this.labelControl8);
            this.Controls.Add(this.labelControl14);
            this.Controls.Add(this.labelControl9);
            this.Controls.Add(this.lblSexoExistente);
            this.Controls.Add(this.labelControl13);
            this.Controls.Add(this.lblNacimientoExistente);
            this.Controls.Add(this.labelControl15);
            this.Controls.Add(this.lblApellidosExistente);
            this.Controls.Add(this.picIdentidad);
            this.Controls.Add(this.label5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmHitIdentidad";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmHitIdentidad";
            this.Load += new System.EventHandler(this.frmHitIdentidad_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picIdentidad.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PictureEdit picIdentidad;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.LabelControl lblIdentificacionExistente;
        private DevExpress.XtraEditors.LabelControl lblNombresExistente;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl lblSexoExistente;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl lblNacimientoExistente;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.LabelControl lblApellidosExistente;
    }
}