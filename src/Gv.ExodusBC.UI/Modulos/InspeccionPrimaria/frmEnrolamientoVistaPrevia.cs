﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Reflection;
using DevExpress.XtraEditors.ViewInfo;

namespace Gv.ExodusBc.UI.Modulos.InspeccionPrimaria
{
    public partial class frmEnrolamientoVistaPrevia : DevExpress.XtraEditors.XtraForm
    {
        public Image imgDocPreview { get; set; }
        public frmEnrolamientoVistaPrevia()
        {
            InitializeComponent();
        }

        private void frmEnrolamientoVistaPrevia_Load(object sender, EventArgs e)
        {
            PropertyInfo pi = typeof(BaseEdit).GetProperty("ViewInfo", BindingFlags.NonPublic | BindingFlags.Instance);
            PictureEditViewInfo info = pi.GetValue(ptbPrevia, null) as PictureEditViewInfo;
            float horizRatio = Convert.ToSingle(info.ClientRect.Width) / info.Image.Width;
            float verticalRatio = Convert.ToSingle(info.ClientRect.Height) / info.Image.Height;
            float zoomRatio = Math.Min(horizRatio, verticalRatio);
            ptbPrevia.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            ptbPrevia.Properties.ZoomPercent = Convert.ToInt32(zoomRatio * 100);
            tbZoomBar.Value = Convert.ToInt32(zoomRatio * 100);
            lblZoom.Text = Convert.ToInt32(zoomRatio * 100) + " %";
            ptbPrevia.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
        }

        private void btnRotarIzquierda_Click(object sender, EventArgs e)
        {
            imgDocPreview.RotateFlip(RotateFlipType.Rotate270FlipNone);
            ptbPrevia.Image = imgDocPreview;
        }

        private void btnRotarDerecha_Click(object sender, EventArgs e)
        {
            imgDocPreview.RotateFlip(RotateFlipType.Rotate90FlipNone);
            ptbPrevia.Image = imgDocPreview;
        }

        private void btnAjustarTamanio_Click(object sender, EventArgs e)
        {
            PropertyInfo pi = typeof(BaseEdit).GetProperty("ViewInfo", BindingFlags.NonPublic | BindingFlags.Instance);
            PictureEditViewInfo info = pi.GetValue(ptbPrevia, null) as PictureEditViewInfo;
            float horizRatio = Convert.ToSingle(info.ClientRect.Width) / info.Image.Width;
            float verticalRatio = Convert.ToSingle(info.ClientRect.Height) / info.Image.Height;
            float zoomRatio = Math.Min(horizRatio, verticalRatio);
            ptbPrevia.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            ptbPrevia.Properties.ZoomPercent = Convert.ToInt32(zoomRatio * 100);
            tbZoomBar.Value = Convert.ToInt32(zoomRatio * 100);
            lblZoom.Text = Convert.ToInt32(zoomRatio * 100) + " %";
            ptbPrevia.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Clip;
        }

        private void btnTamanioReal_Click(object sender, EventArgs e)
        {

        }

        private void tbZoomBar_Scroll(object sender, EventArgs e)
        {
            ptbPrevia.Properties.ZoomPercent = tbZoomBar.Value;
            lblZoom.Text = tbZoomBar.Value.ToString() + " %";
        }
    }
}