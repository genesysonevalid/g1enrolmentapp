﻿using wyDay.Controls;

namespace Gv.ExodusBc.UI.Modulos.Registro
{
    partial class frmLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLogin));
            this.ptbLogoGV = new System.Windows.Forms.PictureBox();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.btnEntrar = new DevExpress.XtraEditors.SimpleButton();
            this.txtNombreUsuario = new DevExpress.XtraEditors.TextEdit();
            this.lblModoTrabajo = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit2 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit3 = new DevExpress.XtraEditors.PictureEdit();
            this.lblNombreSistema = new DevExpress.XtraEditors.LabelControl();
            this.lblEstacion = new DevExpress.XtraEditors.LabelControl();
            this.lblDelegacion = new DevExpress.XtraEditors.LabelControl();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblDerechos = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.pnlMoverForm = new System.Windows.Forms.Panel();
            this.lblSalir = new DevExpress.XtraEditors.LabelControl();
            this.lblTitulo = new DevExpress.XtraEditors.LabelControl();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureEdit4 = new DevExpress.XtraEditors.PictureEdit();
            this.txtContrasenia = new DevExpress.XtraEditors.TextEdit();
            this.wyUpd = new wyDay.Controls.AutomaticUpdater();
            this.ssForm = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::Gv.ExodusBc.UI.frmWaitForm), true, true);
            ((System.ComponentModel.ISupportInitialize)(this.ptbLogoGV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNombreUsuario.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.pnlMoverForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContrasenia.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wyUpd)).BeginInit();
            this.SuspendLayout();
            // 
            // ptbLogoGV
            // 
            this.ptbLogoGV.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ptbLogoGV.BackColor = System.Drawing.Color.Transparent;
            this.ptbLogoGV.Image = global::Gv.ExodusBc.UI.Properties.Resources.grupovision;
            this.ptbLogoGV.Location = new System.Drawing.Point(730, 630);
            this.ptbLogoGV.Margin = new System.Windows.Forms.Padding(4);
            this.ptbLogoGV.Name = "ptbLogoGV";
            this.ptbLogoGV.Size = new System.Drawing.Size(245, 69);
            this.ptbLogoGV.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbLogoGV.TabIndex = 12;
            this.ptbLogoGV.TabStop = false;
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit1.EditValue = global::Gv.ExodusBc.UI.Properties.Resources.logo_genesysone;
            this.pictureEdit1.Location = new System.Drawing.Point(32, 56);
            this.pictureEdit1.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.AllowFocused = false;
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit1.Properties.ShowMenu = false;
            this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit1.Size = new System.Drawing.Size(371, 71);
            this.pictureEdit1.TabIndex = 24;
            // 
            // btnEntrar
            // 
            this.btnEntrar.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.btnEntrar.Appearance.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEntrar.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.btnEntrar.Appearance.Options.UseBackColor = true;
            this.btnEntrar.Appearance.Options.UseFont = true;
            this.btnEntrar.Appearance.Options.UseForeColor = true;
            this.btnEntrar.AppearanceDisabled.ForeColor = System.Drawing.Color.Silver;
            this.btnEntrar.AppearanceDisabled.Options.UseForeColor = true;
            this.btnEntrar.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btnEntrar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEntrar.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_login_21x32;
            this.btnEntrar.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnEntrar.ImageOptions.SvgImageColorizationMode = DevExpress.Utils.SvgImageColorizationMode.Full;
            this.btnEntrar.Location = new System.Drawing.Point(63, 406);
            this.btnEntrar.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.btnEntrar.Name = "btnEntrar";
            this.btnEntrar.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btnEntrar.Size = new System.Drawing.Size(340, 38);
            this.btnEntrar.TabIndex = 2;
            this.btnEntrar.Text = "Entrar";
            this.btnEntrar.ToolTip = "Ingresar al sistema";
            this.btnEntrar.Click += new System.EventHandler(this.btnEntrar_Click);
            // 
            // txtNombreUsuario
            // 
            this.txtNombreUsuario.AllowHtmlTextInToolTip = DevExpress.Utils.DefaultBoolean.True;
            this.txtNombreUsuario.EditValue = "";
            this.txtNombreUsuario.Location = new System.Drawing.Point(63, 278);
            this.txtNombreUsuario.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.txtNombreUsuario.Name = "txtNombreUsuario";
            this.txtNombreUsuario.Properties.Appearance.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtNombreUsuario.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombreUsuario.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.txtNombreUsuario.Properties.Appearance.Options.UseBackColor = true;
            this.txtNombreUsuario.Properties.Appearance.Options.UseFont = true;
            this.txtNombreUsuario.Properties.Appearance.Options.UseForeColor = true;
            this.txtNombreUsuario.Properties.Appearance.Options.UseTextOptions = true;
            this.txtNombreUsuario.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.txtNombreUsuario.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtNombreUsuario.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtNombreUsuario.Properties.AutoHeight = false;
            this.txtNombreUsuario.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.txtNombreUsuario.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNombreUsuario.Properties.ContextImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.user;
            this.txtNombreUsuario.Properties.MaxLength = 20;
            this.txtNombreUsuario.Size = new System.Drawing.Size(340, 38);
            this.txtNombreUsuario.TabIndex = 0;
            // 
            // lblModoTrabajo
            // 
            this.lblModoTrabajo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblModoTrabajo.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblModoTrabajo.Appearance.Font = new System.Drawing.Font("Tahoma", 11F);
            this.lblModoTrabajo.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(204)))), ((int)(((byte)(113)))));
            this.lblModoTrabajo.Appearance.Image = global::Gv.ExodusBc.UI.Properties.Resources.gf_loading;
            this.lblModoTrabajo.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblModoTrabajo.Appearance.Options.UseBackColor = true;
            this.lblModoTrabajo.Appearance.Options.UseFont = true;
            this.lblModoTrabajo.Appearance.Options.UseForeColor = true;
            this.lblModoTrabajo.Appearance.Options.UseImage = true;
            this.lblModoTrabajo.Appearance.Options.UseImageAlign = true;
            this.lblModoTrabajo.Appearance.Options.UseTextOptions = true;
            this.lblModoTrabajo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lblModoTrabajo.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblModoTrabajo.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.lblModoTrabajo.Location = new System.Drawing.Point(761, 6);
            this.lblModoTrabajo.Margin = new System.Windows.Forms.Padding(4);
            this.lblModoTrabajo.Name = "lblModoTrabajo";
            this.lblModoTrabajo.Size = new System.Drawing.Size(249, 37);
            this.lblModoTrabajo.TabIndex = 115;
            this.lblModoTrabajo.Text = "Fuera de línea";
            // 
            // labelControl5
            // 
            this.labelControl5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.ForeColor = System.Drawing.Color.DimGray;
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Appearance.Options.UseForeColor = true;
            this.labelControl5.Appearance.Options.UseTextOptions = true;
            this.labelControl5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl5.Location = new System.Drawing.Point(678, 598);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(351, 26);
            this.labelControl5.TabIndex = 123;
            this.labelControl5.Text = "Powered by";
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.DimGray;
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Appearance.Options.UseForeColor = true;
            this.labelControl1.Appearance.Options.UseTextOptions = true;
            this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.Location = new System.Drawing.Point(678, 703);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(351, 31);
            this.labelControl1.TabIndex = 124;
            this.labelControl1.Text = "Todos los derechos reservados 2018.";
            // 
            // labelControl4
            // 
            this.labelControl4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Yu Gothic", 11F, System.Drawing.FontStyle.Bold);
            this.labelControl4.Appearance.ForeColor = System.Drawing.Color.DimGray;
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Appearance.Options.UseForeColor = true;
            this.labelControl4.Appearance.Options.UseTextOptions = true;
            this.labelControl4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl4.Location = new System.Drawing.Point(678, 244);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(351, 32);
            this.labelControl4.TabIndex = 127;
            this.labelControl4.Text = "Credenciales de Acceso";
            // 
            // pictureEdit2
            // 
            this.pictureEdit2.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit2.EditValue = global::Gv.ExodusBc.UI.Properties.Resources.ic_station_32x32;
            this.pictureEdit2.Location = new System.Drawing.Point(63, 483);
            this.pictureEdit2.Margin = new System.Windows.Forms.Padding(4);
            this.pictureEdit2.Name = "pictureEdit2";
            this.pictureEdit2.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit2.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit2.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit2.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit2.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.pictureEdit2.Size = new System.Drawing.Size(25, 30);
            this.pictureEdit2.TabIndex = 128;
            this.pictureEdit2.ToolTip = "Estación";
            // 
            // pictureEdit3
            // 
            this.pictureEdit3.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit3.EditValue = global::Gv.ExodusBc.UI.Properties.Resources.ic_office_32X32;
            this.pictureEdit3.Location = new System.Drawing.Point(63, 523);
            this.pictureEdit3.Margin = new System.Windows.Forms.Padding(4);
            this.pictureEdit3.Name = "pictureEdit3";
            this.pictureEdit3.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit3.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit3.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit3.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit3.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.pictureEdit3.Size = new System.Drawing.Size(25, 30);
            this.pictureEdit3.TabIndex = 129;
            this.pictureEdit3.ToolTip = "Oficina";
            // 
            // lblNombreSistema
            // 
            this.lblNombreSistema.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblNombreSistema.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lblNombreSistema.Appearance.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombreSistema.Appearance.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lblNombreSistema.Appearance.Options.UseBackColor = true;
            this.lblNombreSistema.Appearance.Options.UseFont = true;
            this.lblNombreSistema.Appearance.Options.UseForeColor = true;
            this.lblNombreSistema.Appearance.Options.UseTextOptions = true;
            this.lblNombreSistema.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblNombreSistema.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.lblNombreSistema.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblNombreSistema.Location = new System.Drawing.Point(97, 113);
            this.lblNombreSistema.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lblNombreSistema.Name = "lblNombreSistema";
            this.lblNombreSistema.Size = new System.Drawing.Size(145, 30);
            this.lblNombreSistema.TabIndex = 249;
            this.lblNombreSistema.Text = "Enrollment System";
            // 
            // lblEstacion
            // 
            this.lblEstacion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEstacion.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lblEstacion.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstacion.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lblEstacion.Appearance.Options.UseBackColor = true;
            this.lblEstacion.Appearance.Options.UseFont = true;
            this.lblEstacion.Appearance.Options.UseForeColor = true;
            this.lblEstacion.Appearance.Options.UseTextOptions = true;
            this.lblEstacion.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblEstacion.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblEstacion.LineColor = System.Drawing.Color.LightGray;
            this.lblEstacion.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblEstacion.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblEstacion.LineVisible = true;
            this.lblEstacion.Location = new System.Drawing.Point(90, 480);
            this.lblEstacion.LookAndFeel.SkinName = "Metropolis";
            this.lblEstacion.LookAndFeel.UseDefaultLookAndFeel = false;
            this.lblEstacion.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lblEstacion.Name = "lblEstacion";
            this.lblEstacion.Size = new System.Drawing.Size(306, 28);
            this.lblEstacion.TabIndex = 252;
            this.lblEstacion.ToolTip = "Estación de trabajo";
            // 
            // lblDelegacion
            // 
            this.lblDelegacion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDelegacion.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lblDelegacion.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDelegacion.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lblDelegacion.Appearance.Options.UseBackColor = true;
            this.lblDelegacion.Appearance.Options.UseFont = true;
            this.lblDelegacion.Appearance.Options.UseForeColor = true;
            this.lblDelegacion.Appearance.Options.UseTextOptions = true;
            this.lblDelegacion.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblDelegacion.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblDelegacion.LineColor = System.Drawing.Color.LightGray;
            this.lblDelegacion.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblDelegacion.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblDelegacion.LineVisible = true;
            this.lblDelegacion.Location = new System.Drawing.Point(90, 518);
            this.lblDelegacion.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lblDelegacion.Name = "lblDelegacion";
            this.lblDelegacion.Size = new System.Drawing.Size(306, 28);
            this.lblDelegacion.TabIndex = 253;
            this.lblDelegacion.ToolTip = "Oficina";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::Gv.ExodusBc.UI.Properties.Resources.lg_gv;
            this.pictureBox1.Location = new System.Drawing.Point(427, 0);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(21, 25);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 11;
            this.pictureBox1.TabStop = false;
            // 
            // lblDerechos
            // 
            this.lblDerechos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDerechos.ForeColor = System.Drawing.Color.White;
            this.lblDerechos.Location = new System.Drawing.Point(218, 2);
            this.lblDerechos.Name = "lblDerechos";
            this.lblDerechos.Size = new System.Drawing.Size(206, 22);
            this.lblDerechos.TabIndex = 254;
            this.lblDerechos.Text = "Powered by Grupo Visión © 2019 ";
            this.lblDerechos.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.panel1.Controls.Add(this.lblDerechos);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Location = new System.Drawing.Point(-1, 669);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(467, 24);
            this.panel1.TabIndex = 255;
            // 
            // labelControl3
            // 
            this.labelControl3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl3.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.labelControl3.Appearance.Options.UseBackColor = true;
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Appearance.Options.UseForeColor = true;
            this.labelControl3.Appearance.Options.UseTextOptions = true;
            this.labelControl3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl3.Location = new System.Drawing.Point(63, 204);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(333, 31);
            this.labelControl3.TabIndex = 258;
            this.labelControl3.Text = "Inicio de sesión";
            // 
            // pnlMoverForm
            // 
            this.pnlMoverForm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.pnlMoverForm.Controls.Add(this.lblSalir);
            this.pnlMoverForm.Controls.Add(this.lblTitulo);
            this.pnlMoverForm.Controls.Add(this.pictureBox2);
            this.pnlMoverForm.Controls.Add(this.pictureEdit4);
            this.pnlMoverForm.Location = new System.Drawing.Point(0, 0);
            this.pnlMoverForm.Name = "pnlMoverForm";
            this.pnlMoverForm.Size = new System.Drawing.Size(466, 35);
            this.pnlMoverForm.TabIndex = 260;
            this.pnlMoverForm.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlMoverForm_MouseDown);
            // 
            // lblSalir
            // 
            this.lblSalir.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSalir.Appearance.ForeColor = System.Drawing.Color.White;
            this.lblSalir.Appearance.Options.UseFont = true;
            this.lblSalir.Appearance.Options.UseForeColor = true;
            this.lblSalir.Appearance.Options.UseTextOptions = true;
            this.lblSalir.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblSalir.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lblSalir.AppearanceHovered.BackColor = System.Drawing.Color.Red;
            this.lblSalir.AppearanceHovered.Options.UseBackColor = true;
            this.lblSalir.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblSalir.Location = new System.Drawing.Point(413, -1);
            this.lblSalir.Name = "lblSalir";
            this.lblSalir.Size = new System.Drawing.Size(48, 36);
            this.lblSalir.TabIndex = 261;
            this.lblSalir.Text = "X";
            this.lblSalir.ToolTip = "Salir";
            this.lblSalir.Click += new System.EventHandler(this.lblSalir_Click);
            // 
            // lblTitulo
            // 
            this.lblTitulo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTitulo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lblTitulo.Appearance.Font = new System.Drawing.Font("Calibri Light", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.Appearance.ForeColor = System.Drawing.Color.White;
            this.lblTitulo.Appearance.Options.UseBackColor = true;
            this.lblTitulo.Appearance.Options.UseFont = true;
            this.lblTitulo.Appearance.Options.UseForeColor = true;
            this.lblTitulo.Appearance.Options.UseTextOptions = true;
            this.lblTitulo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblTitulo.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblTitulo.Location = new System.Drawing.Point(42, 4);
            this.lblTitulo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(320, 31);
            this.lblTitulo.TabIndex = 263;
            this.lblTitulo.Text = "Inicio de sesión";
            this.lblTitulo.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lblTitulo_MouseDown);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Image = global::Gv.ExodusBc.UI.Properties.Resources.huellaazul;
            this.pictureBox2.Location = new System.Drawing.Point(-46, 2);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(26, 30);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 262;
            this.pictureBox2.TabStop = false;
            // 
            // pictureEdit4
            // 
            this.pictureEdit4.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit4.EditValue = global::Gv.ExodusBc.UI.Properties.Resources.iconoapp;
            this.pictureEdit4.Location = new System.Drawing.Point(4, 2);
            this.pictureEdit4.Margin = new System.Windows.Forms.Padding(4);
            this.pictureEdit4.Name = "pictureEdit4";
            this.pictureEdit4.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit4.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit4.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit4.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit4.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit4.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit4.TabIndex = 261;
            this.pictureEdit4.ToolTip = "Estación";
            // 
            // txtContrasenia
            // 
            this.txtContrasenia.EditValue = "";
            this.txtContrasenia.Location = new System.Drawing.Point(63, 340);
            this.txtContrasenia.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.txtContrasenia.Name = "txtContrasenia";
            this.txtContrasenia.Properties.Appearance.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtContrasenia.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtContrasenia.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.txtContrasenia.Properties.Appearance.Options.UseBackColor = true;
            this.txtContrasenia.Properties.Appearance.Options.UseFont = true;
            this.txtContrasenia.Properties.Appearance.Options.UseForeColor = true;
            this.txtContrasenia.Properties.Appearance.Options.UseTextOptions = true;
            this.txtContrasenia.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.txtContrasenia.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtContrasenia.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtContrasenia.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.txtContrasenia.Properties.ContextImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.password;
            this.txtContrasenia.Properties.MaxLength = 20;
            this.txtContrasenia.Properties.PasswordChar = '*';
            this.txtContrasenia.Properties.UseSystemPasswordChar = true;
            this.txtContrasenia.Size = new System.Drawing.Size(340, 38);
            this.txtContrasenia.TabIndex = 1;
            // 
            // wyUpd
            // 
            this.wyUpd.Animate = false;
            this.wyUpd.BackColor = System.Drawing.Color.White;
            this.wyUpd.ContainerForm = this;
            this.wyUpd.DaysBetweenChecks = 0;
            this.wyUpd.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.wyUpd.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.wyUpd.GUID = "13fa122c-a717-401a-82e2-f8d25f692415";
            this.wyUpd.Location = new System.Drawing.Point(1, 626);
            this.wyUpd.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.wyUpd.Name = "wyUpd";
            this.wyUpd.Size = new System.Drawing.Size(462, 19);
            this.wyUpd.TabIndex = 6;
            this.wyUpd.wyUpdateCommandline = null;
            this.wyUpd.Cancelled += new System.EventHandler(this.wyUpd_Cancelled);
            this.wyUpd.CheckingFailed += new wyDay.Controls.FailHandler(this.wyUpd_CheckingFailed);
            this.wyUpd.ReadyToBeInstalled += new System.EventHandler(this.wyUpd_ReadyToBeInstalled);
            this.wyUpd.UpdateFailed += new wyDay.Controls.FailHandler(this.wyUpd_UpdateFailed);
            this.wyUpd.UpdateSuccessful += new wyDay.Controls.SuccessHandler(this.wyUpd_UpdateSuccessful);
            this.wyUpd.UpToDate += new wyDay.Controls.SuccessHandler(this.wyUpd_UpToDate);
            // 
            // ssForm
            // 
            this.ssForm.ClosingDelay = 500;
            // 
            // frmLogin
            // 
            this.Appearance.BackColor = System.Drawing.Color.White;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(460, 693);
            this.Controls.Add(this.lblNombreSistema);
            this.Controls.Add(this.pictureEdit1);
            this.Controls.Add(this.wyUpd);
            this.Controls.Add(this.pnlMoverForm);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblDelegacion);
            this.Controls.Add(this.lblEstacion);
            this.Controls.Add(this.pictureEdit3);
            this.Controls.Add(this.pictureEdit2);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.ptbLogoGV);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.lblModoTrabajo);
            this.Controls.Add(this.btnEntrar);
            this.Controls.Add(this.txtContrasenia);
            this.Controls.Add(this.txtNombreUsuario);
            this.FormBorderEffect = DevExpress.XtraEditors.FormBorderEffect.Shadow;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmLogin";
            this.Padding = new System.Windows.Forms.Padding(1);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "GenesysOne - Login";
            this.Load += new System.EventHandler(this.frmLogin_Load);
            this.Shown += new System.EventHandler(this.frmLogin_Shown);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frmLogin_MouseDown);
            ((System.ComponentModel.ISupportInitialize)(this.ptbLogoGV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNombreUsuario.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.pnlMoverForm.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContrasenia.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wyUpd)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.PictureBox ptbLogoGV;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.SimpleButton btnEntrar;
        private DevExpress.XtraEditors.TextEdit txtNombreUsuario;
        private DevExpress.XtraEditors.LabelControl lblModoTrabajo;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.PictureEdit pictureEdit3;
        private DevExpress.XtraEditors.PictureEdit pictureEdit2;
        private DevExpress.XtraEditors.LabelControl lblNombreSistema;
        private DevExpress.XtraEditors.LabelControl lblEstacion;
        private DevExpress.XtraEditors.LabelControl lblDelegacion;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblDerechos;
        private System.Windows.Forms.Panel panel1;

        private DevExpress.XtraEditors.LabelControl labelControl3;
        private System.Windows.Forms.Panel pnlMoverForm;
        private DevExpress.XtraEditors.PictureEdit pictureEdit4;
        private System.Windows.Forms.PictureBox pictureBox2;
        private DevExpress.XtraEditors.LabelControl lblTitulo;
        private DevExpress.XtraEditors.TextEdit txtContrasenia;
        private AutomaticUpdater wyUpd;
        private DevExpress.XtraEditors.LabelControl lblSalir;
        private DevExpress.XtraSplashScreen.SplashScreenManager ssForm;
    }
}