﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Gv.ExodusBc.LN;
using Newtonsoft.Json.Linq;
using Gv.ExodusBc.EN;
using System.IO;
using System.Drawing;
using DevExpress.LookAndFeel;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using Gv.ExodusBc.UI.Modulos.InspeccionPrimaria;
using Gv.ExodusBc.UI.Modulos.Mantenimientos;
using System.Management;
using Gv.Utilidades;
using System.Net.NetworkInformation;

namespace Gv.ExodusBc.UI.Modulos.Registro
{
    public partial class frmLogin : DevExpress.XtraEditors.XtraForm
    {
        MenuItem mn = new MenuItem();
        private WebReference.ExodusBcService ws = null;
        public static long WS_ID = 0;
        public static int OFFICE_CODE = 0;
    
        private string OFFICE_TYPE_CODE = "";
        public static string _fechaAcceso = "";
        private string host = "";
        UserLogin ObjUser = null;
        Users objUsuario = null;

        //USERS objUsuario = null;
        UserLookAndFeel lookAndFeelMsgBox;
        int DiasCambioContrasenia = 0;
        bool comprobacionFinalizada;
        bool wyFinalizado;
        int _intentos = 0;

        delegate void SetModoTrabajoCallBack(string text, string estado);
        delegate void SetLabelTextCallBack(string text, LabelControl lbl);

        //***Bandera para timer de sincronizacion***
        private bool _sincronizando;
        private readonly Timer tmrConexion;
        private const int INTERVALO_TIMER_CONEXION = 1; //minutos


        //Mover form con modo sin bordes
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(IntPtr hWnd, int wMsg, int wParam, int lParam);

        public frmLogin()
        {
            InitializeComponent();
            try
            {
                ws = new WebReference.ExodusBcService();

                lookAndFeelMsgBox = new UserLookAndFeel(this);
                lookAndFeelMsgBox.SkinName = VariablesGlobales.MessageBoxDevExpressStyle;

                lookAndFeelMsgBox.Style = LookAndFeelStyle.Skin;
                lookAndFeelMsgBox.UseDefaultLookAndFeel = false;

                ConfigurarWyUpdate();

                Text = "GenesysOne - Enrollment System v " + ExodusBcBase.Helper.GetVersion();
                lblTitulo.Text = "GenesysOne - Enrollment System v " + ExodusBcBase.Helper.GetVersion();
                lblDerechos.Text = "Powered by Grupo Visión © " + ExodusBcBase.Utilities.getFechaActual().Year;

  
                tmrConexion = new Timer { Enabled = true, Interval = INTERVALO_TIMER_CONEXION * 60_000 };
                tmrConexion.Tick += Conexion_Tick;
                tmrConexion.Start();
            }
            catch (Exception ex)
            {
                Log.InsertarLog(Log.ErrorType.Error, "frmLogin::frmLogin", ExodusBcBase.Helper.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }


        void ConfigurarWyUpdate()
        {
            wyUpd.Translation.AlreadyUpToDate = "Sistema actualizado a la última versión.";
            wyUpd.Translation.CancelCheckingMenu = "Cancelar la búsqueda de actualizaciones.";
            wyUpd.Translation.CancelUpdatingMenu = "Cancelar actualizaciones.";
            wyUpd.Translation.ChangesInVersion = "Cambios en la versión.";
            wyUpd.Translation.CheckForUpdatesMenu = "Buscar actualizaciones.";
            wyUpd.Translation.Checking = "Buscando actualizaciones...";
            wyUpd.Translation.CloseButton = "Cerrar";
            wyUpd.Translation.Downloading = "Descargando actualización.";
            wyUpd.Translation.DownloadUpdateMenu = "Descargar actualización.";
            wyUpd.Translation.ErrorTitle = "Se presento el siguiente error";
            wyUpd.Translation.Extracting = "Extrayendo.";
            wyUpd.Translation.FailedToCheck = "No se pudo conectar al servidor de actualizaciones.";
            wyUpd.Translation.FailedToDownload = "Fallo durante la descarga.";
            wyUpd.Translation.FailedToExtract = "Fallo durante la extracción";
            wyUpd.Translation.HideMenu = "Ocultar";
            wyUpd.Translation.InstallOnNextStart = "Instalar en el siguiente inicio.";
            wyUpd.Translation.InstallUpdateMenu = "Instalar actualización.";
            wyUpd.Translation.StopChecking = "Detener la búsqueda.";
            wyUpd.Translation.StopDownloading = "Detener la descarga.";
            wyUpd.Translation.StopExtracting = "Detener la extracción.";
            wyUpd.Translation.SuccessfullyUpdated = "Actualización exitosa.";
            wyUpd.Translation.TryAgainLater = "Intentar de nuevo más tarde.";
            wyUpd.Translation.TryAgainNow = "Intentar de nuevo ahora.";
            wyUpd.Translation.UpdateAvailable = "Actualización disponible.";
            wyUpd.Translation.UpdateFailed = "Actualización fallida.";
            wyUpd.Translation.UpdateNowButton = "Actualizar ahora.";
            wyUpd.Translation.ViewChangesMenu = "Ver cambios.";
            wyUpd.Translation.ViewError = "Ver detalles del error.";
            wyUpd.Translation.PrematureExitMessage = "Proceso cancelado.";
            wyUpd.Translation.PrematureExitTitle = "Proceso cancelado.";
        }

        private void Conexion_Tick(object sender, EventArgs e)
        {
            try
            {
                if (_sincronizando) return;

                _sincronizando = true;

                Task.Run(async () =>
                {
                    bool enLinea = VariablesGlobales.VerificarConexionAlservidor();
                    if(enLinea)
                    {
                        if (VariablesGlobales.NecesitaSincronizar) VariablesGlobales.SincronizarCatalogos();
                    }
                });

                _sincronizando = false;

            }
            catch (Exception ex)
            {
                if (DateTime.Now.Minute > 0 && DateTime.Now.Minute < INTERVALO_TIMER_CONEXION * 3)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "frmLogin::Conexion_Tick:: Error al ejectuar timer de conexión.", ExodusBcBase.Helper.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                }
                _sincronizando = false;
            }
        }

        private void frmLogin_Load1(object sender, EventArgs e)
        {
            try
            {
                wyUpd.MenuItem = mn;
                wyUpd.ForceCheckForUpdate(true);

                //ExodusBcBase.Helper.EncriptarCadenaConexion();

                //Creación de Directorios
                if (!Directory.Exists(VariablesGlobales.PathDataDB))
                    Directory.CreateDirectory(VariablesGlobales.PathDataDB);
                if (!Directory.Exists(VariablesGlobales.PathDataLocal))
                    Directory.CreateDirectory(VariablesGlobales.PathDataLocal);
                if (!Directory.Exists(VariablesGlobales.PathDataLog))
                    Directory.CreateDirectory(VariablesGlobales.PathDataLog);
                if (!Directory.Exists(VariablesGlobales.PathDataSystem))
                    Directory.CreateDirectory(VariablesGlobales.PathDataSystem);
                if (!Directory.Exists(VariablesGlobales.PathPdf))
                    Directory.CreateDirectory(VariablesGlobales.PathPdf);

                SetModoTrabajo("Comprobando conexión...", "C");
                Iniciar();
                txtNombreUsuario.Focus();

            }
            catch (Exception ex)
            {
                Log.InsertarLog(Log.ErrorType.Error, "frmLogin::frmLogin_Load", ExodusBcBase.Helper.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        private async void Iniciar()
        {
            try
            {
                btnEntrar.Enabled = false;
                await Task.Run(() =>
                {
                    VariablesGlobales.Inicializar();

                    if (ExodusBcBase.Utilities.existsFileSQLite())
                    {
                        _fechaAcceso = ExodusBcBase.LoginWebService.getStatusConecction();
                        VariablesGlobales.MODOTRABAJO = (_fechaAcceso == string.Empty) ? "OFF" : "ON";
                    }
                    else
                    if (ExodusBcBase.Utilities.copyFileSQLite())
                    {
                        _fechaAcceso = ExodusBcBase.LoginWebService.getStatusConecction();
                        VariablesGlobales.MODOTRABAJO = (_fechaAcceso == string.Empty) ? "OFF" : "ON";
                    }
                    else
                    {
                        _fechaAcceso = ExodusBcBase.LoginWebService.getStatusConecction();
                        VariablesGlobales.MODOTRABAJO = (_fechaAcceso == string.Empty) ? "OFF" : "ON";
                    }

                    VariablesGlobales.FechaHora = ExodusBcBase.Utilities.getFechaActual();
                    CheckHostFile(VariablesGlobales.WY_UPDATE_IP, VariablesGlobales.WY_UPDATE_URL);
                    if (VariablesGlobales.MODOTRABAJO.Equals("ON")) GetHostNameOnline();
                    else GetHostNameOffline();

                    if (VariablesGlobales.MODOTRABAJO.Equals("ON")) SetModoTrabajo("En línea", "ON");
                    else SetModoTrabajo("Fuera de línea", "OF");

                    comprobacionFinalizada = true;
                    btnEntrar.Enabled = true;
                });
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Hubo un error al iniciar la aplicación.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmLogin::Iniciar", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
            
        }

        private async void CheckHostFile(string ip, string url)
        {
            await Task.Run(() => 
            {
                var OSInfo = Environment.OSVersion;
                string pathpart = "hosts";
                if (OSInfo.Platform == PlatformID.Win32NT)
                {
                    //is windows NT
                    pathpart = "system32\\drivers\\etc\\hosts";
                }
                string hostfile = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Windows), pathpart);
                string ruta = ip + "   " + url;
                string[] lines = File.ReadAllLines(hostfile);

                if (lines.Any(s => s.Contains(url) && s.Contains(ip))) return;
                else
                {
                    if (lines.Any(s => s.Contains(url) && !s.Contains(ip)))
                    {
                        for (int i = 0; i < lines.Length; i++)
                            if (lines[i].Contains(url)) lines[i] = ruta;
                        File.WriteAllLines(hostfile, lines);
                    }
                    else File.AppendAllLines(hostfile, new string[] { ruta });
                }
            });
        }

        private void GetHostNameOnline()
        {
            try
            {
                if (VariablesGlobales.MODOTRABAJO.Equals("ON"))
                {
                    //1.VALIDAR SI LA MÁQUINA ESTÁ LICENCIADA
                    string procesador = ObtenerMaquinaId();
                    string mensaje = "";

                    var objEstacion = WORKSTATIONSLN.VerificarEstacionLicenciada(procesador, ref mensaje);
                    if (objEstacion != null)
                    {
                        VariablesGlobales.HOST_NAME = ExodusBc.UI.ExodusBcBase.Utilities.getHostName();

                        JArray workstation = ExodusBc.UI.ExodusBcBase.LoginWebService.getWorkStation(VariablesGlobales.HOST_NAME);
                        if (workstation != null)
                        {
                            var objWorkStation = LN.WORKSTATIONSLN.GeWorkstationbyHostName(VariablesGlobales.HOST_NAME);
                            if (objWorkStation != null)
                            {
                                object obj = ExodusBc.UI.ExodusBcBase.LoginWebService.getItemFromJArray(workstation, "FKCatalogStatusId");
                                if (obj != null)
                                {
                                    if (obj.ToString().ToLower().Equals("true") || obj.ToString().ToLower().Equals("1"))
                                    {
                                        WS_ID = long.Parse(ExodusBcBase.LoginWebService.getItemFromJArray(workstation, "WorkStationsId").ToString());
                                        OFFICE_CODE = Convert.ToInt32(ExodusBcBase.LoginWebService.getItemFromJArray(workstation, "FKOfficeId"));
                                        host = ExodusBcBase.LoginWebService.getItemFromJArray(workstation, "HostName").ToString();

                                        JArray office = ExodusBcBase.LoginWebService.getOffice(OFFICE_CODE, host);
                                        if (office != null)
                                        {
                                            OFFICE_TYPE_CODE = ExodusBcBase.LoginWebService.getItemFromJArray(office, "OfficeId").ToString();
                                            SetLabelText(ExodusBcBase.LoginWebService.getItemFromJArray(office, "Name").ToString(), lblDelegacion);
                                            obj = ExodusBcBase.LoginWebService.getItemFromJArray(office, "FKCatalogStatusId");
                                            if (obj != null)
                                            {
                                                if (obj.ToString().ToLower().Equals("false") || obj.ToString().ToLower().Equals("0"))
                                                {
                                                    XtraMessageBox.Show(lookAndFeelMsgBox, "La Oficina a la que pertence la estación de trabajo esta inactiva, comuníquese con el Administrador del Sistema.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                                    Application.Exit();
                                                }
                                                else SetLabelText(host, lblEstacion);
                                            }
                                        }
                                        else
                                        {
                                            XtraMessageBox.Show(lookAndFeelMsgBox, "La Oficina a la que pertenece la estación de trabajo esta inactiva, comuníquese con el administrador del sistema.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                            Application.Exit();
                                        }
                                    }
                                    else
                                    {
                                        XtraMessageBox.Show(lookAndFeelMsgBox, "La estación de trabajo esta inactiva, comuníquese con el Administrador del Sistema.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                        Application.Exit();
                                    }
                                }
                            }
                            else
                            {
                                XtraMessageBox.Show(lookAndFeelMsgBox, "La estación de trabajo no esta registrada, comuníquese con el Administrador del Sistema.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                Application.Exit();
                            }
                        }
                        else
                        {
                            XtraMessageBox.Show(this.lookAndFeelMsgBox, "La estación de trabajo no esta registrada, comuníquese con el Administrador del Sistema.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            Application.Exit();
                        }
                    }
                    else
                    {
                        XtraMessageBox.Show("La  estación de trabajo no esta registrada, Comuníquelo al administrador del sistema.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                else ExodusBcBase.Helper.Mensajes.getMensajeSinConexion();
            }
            catch (Exception ex)
            {
                Log.InsertarLog(Log.ErrorType.Error, "frmLogin::getHostNameOnline", ExodusBcBase.Helper.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        public static string ObtenerMaquinaId()

        {
            string id = string.Empty;
            try
            {
                ManagementObjectSearcher mbs = new ManagementObjectSearcher("Select * from Win32_BIOS");
                ManagementObjectCollection mbsList = mbs.Get();

                foreach (ManagementObject mo in mbsList)
                    id = mo["SerialNumber"].ToString();
               
                string prueba = Seguridad.GVC.EncriptarEnUnaVia(id);
                return Seguridad.GVC.EncriptarEnUnaVia(id);
            }
            catch (Exception) { return id; }
        }

        private void GetHostNameOffline()
        {
            try
            {
                if (VariablesGlobales.MODOTRABAJO.Equals("OFF"))
                {
                    /*
                    *  1. Obtener el código de la máquina
                    *  1.1 Valida si existe
                    *  1.2 Válida si la máquina está activa
                    */
                    VariablesGlobales.HOST_NAME = ExodusBc.UI.ExodusBcBase.Utilities.getHostName();
                    SetLabelText(VariablesGlobales.HOST_NAME, lblEstacion);
                    var objWorkStation = LN.WORKSTATIONSLN.GeWorkstationbyHostName(VariablesGlobales.HOST_NAME);
                    if (objWorkStation != null)
                    {
                        OFFICE_CODE = Convert.ToInt32(objWorkStation.FKOfficeId);
                        WS_ID = objWorkStation.WorkStationsId;
                        var objOfficeCode = LN.OFFICELN.GetOfficebyOfficeCode(OFFICE_CODE);
                        if (objOfficeCode != null)
                        {
                            SetLabelText(objOfficeCode.Name, lblDelegacion);
                            if (objOfficeCode.FKCatalogStatusId == 0)
                            {
                                XtraMessageBox.Show(this.lookAndFeelMsgBox, "La Oficina no esta activa, Comuníquelo al administrador del sistema.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                Application.Exit();
                            }
                        }
                    }
                    else
                    {
                        XtraMessageBox.Show(this.lookAndFeelMsgBox, "La estación no esta activa, Comuníquelo al administrador del sistema.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        Application.Exit();
                    }
                }
            }
            catch (Exception ex)
            {
                Gv.Utilidades.Log.InsertarLog(Utilidades.Log.ErrorType.Error, "frmLogin::getHostNameOffline", ExodusBcBase.Helper.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        private void btnEntrar_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidarControles())
                    txtNombreUsuario.Select();
                else
                {
                    if (VariablesGlobales.MODOTRABAJO.Equals("ON"))
                    {
                        Cursor = Cursors.WaitCursor;
                        GetSesionOnline();
                        Cursor = Cursors.Default;
                    }
                    else GetSesionOffline();
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(this.lookAndFeelMsgBox, "Error al iniciar sesión: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmLogin::iniciarSesion", ExodusBcBase.Helper.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        private void GetSesionOffline()
        {
            /*
            1. Buscar al usuario por el Nombre de Usuario
               1.0 Validar si existe
               1.1 Validar que pertenezca a la oficina
               1.3 Validar si la cuenta de usuario está activa
               1.4 Validar la contraseña
               1.5 validar intentos fallidos para bloquear la cuenta
 
               1.7 Validar el horario (fecha, hora y días)
           */
            try
            {
                switch (ValidarPermiso())
                {
                    case -1:
                        {
                            Cursor = Cursors.Default;
                            Application.Exit();
                            break;
                        }
                    case 0:
                        {
                            Cursor = Cursors.Default;
                            break;
                        }
                    case 1:
                        {
                            Hide();
                            frmMain2 frm = new frmMain2();
                            frm.ShowDialog();
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                Log.InsertarLog(Log.ErrorType.Error, "frmLogin::getSesionOffline", ExodusBcBase.Helper.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        private void GetSesionOnline()
        {

            try
            {
                JArray user = ExodusBcBase.LoginWebService.getUserInfo(txtNombreUsuario.Text.Trim(), host);
                if (user == null)
                {
                    XtraMessageBox.Show(this.lookAndFeelMsgBox, "No se logró establecer conexión con la base de datos", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    VariablesGlobales.MODOTRABAJO = "OFF";
                    SetModoTrabajo("Fuera de línea", "OFF");
                    return;
                }
                if (user.Count == 0)
                {
                    XtraMessageBox.Show(this.lookAndFeelMsgBox, "Acceso denegado, las credenciales ingresadas son inválidas", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                ObjUser = new UserLogin();
                ObjUser.UserId = Convert.ToInt32(ExodusBcBase.LoginWebService.getItemFromJArray(user, "UserId"));
                ObjUser.Name = ExodusBcBase.LoginWebService.getItemFromJArray(user, "Name").ToString();
                ObjUser.Password = ExodusBcBase.LoginWebService.getItemFromJArray(user, "Password").ToString();
                ObjUser.DaysOfAccess = ExodusBcBase.LoginWebService.getItemFromJArray(user, "DaysOfAccess").ToString();
                ObjUser.StartTime = TimeSpan.Parse(ExodusBcBase.LoginWebService.getItemFromJArray(user, "StartTime").ToString());
                ObjUser.EndTime = TimeSpan.Parse(ExodusBcBase.LoginWebService.getItemFromJArray(user, "EndTime").ToString());
                ObjUser.CreationDate = Convert.ToDateTime(ExodusBcBase.LoginWebService.getItemFromJArray(user, "CreationDate").ToString());
                ObjUser.ModifyDate = Convert.ToDateTime(ExodusBcBase.LoginWebService.getItemFromJArray(user, "ModifyDate").ToString());
                ObjUser.CreateUserId = Convert.ToInt32(ExodusBcBase.LoginWebService.getItemFromJArray(user, "CreateUserId"));
                ObjUser.FKPersonId = Convert.ToInt32(ExodusBcBase.LoginWebService.getItemFromJArray(user, "FKPersonId"));
                ObjUser.FKUserRolesId = Convert.ToInt32(ExodusBcBase.LoginWebService.getItemFromJArray(user, "FKUserRolesId"));
                ObjUser.FKCatalogStatusId = Convert.ToInt32(ExodusBcBase.LoginWebService.getItemFromJArray(user, "FKCatalogStatusId"));
                ObjUser.FKOfficeId = Convert.ToInt32(ExodusBcBase.LoginWebService.getItemFromJArray(user, "FKOfficeId"));
                ObjUser.ResetPassword = Convert.ToBoolean(ExodusBcBase.LoginWebService.getItemFromJArray(user, "ResetPassword"));
                ObjUser.PasswordChangeDate = Convert.ToDateTime(ExodusBcBase.LoginWebService.getItemFromJArray(user, "PasswordChangeDate").ToString());
                JArray loginFailLog = ExodusBcBase.LoginWebService.getLoginFailLog(ObjUser.UserId, host);
                //if (loginFailLog != null)
                //{
                //    _intentos = Convert.ToInt32(ExodusBcBase.LoginWebService.getItemFromJArray(loginFailLog, "FailCount"));
                //}

                //Evaluamos que el usuario pertenezca a la oficina
                if (!ObjUser.FKOfficeId.Equals(OFFICE_CODE))
                {
                    XtraMessageBox.Show(this.lookAndFeelMsgBox, "El usuario ingresado no pertenece a esta oficina.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                //Evaluamos que el usuario esté activo
                if (ObjUser.FKCatalogStatusId == 2)
                {

                    if (_intentos >= VariablesGlobales.CANTIDADMAXIMAINTENTOS)
                        XtraMessageBox.Show(this.lookAndFeelMsgBox, "Cuenta de usuario inactiva por mas de " + VariablesGlobales.CANTIDADMAXIMAINTENTOS + " intentos fallidos, comuníquese con el Administrador del Sistema.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    else
                        XtraMessageBox.Show(this.lookAndFeelMsgBox, "Cuenta de usuario inactiva, comuníquese con el Administrador del Sistema", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                //Verificamos la contraseña

                if (ObjUser.Password != Gv.Utilidades.Seguridad.EncriptarSha1(txtContrasenia.Text.Trim()))
                {
                    //Pendiente
                    //InsertarLogin_Log(2);
                    //ExodusBcBase.LoginWebService.EjecutarLoginFileLog(ObjUser.UserId, host);
                    //JArray loginFailLog1 = ExodusBcBase.LoginWebService.getLoginFailLog(ObjUser.UserId, host);
                    //if (loginFailLog1 != null)
                    //{
                    //    //_intentos = Convert.ToInt32(ExodusBcBase.LoginWebService.getItemFromJArray(loginFailLog1, "FailCount"));
                    //    if (_intentos >= VariablesGlobales.CANTIDADMAXIMAINTENTOS)
                    //    {
                    //        ExodusBcBase.LoginWebService.UpdateUserStatus(ObjUser.UserId, host);
                    //    }
                    //}

                    XtraMessageBox.Show(this.lookAndFeelMsgBox, "Acceso denagado, credenciales de acceso invalidas.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtContrasenia.Focus();
                    return;
                }

                //Verificamos si requiere reseteo de password
                if (ObjUser.ResetPassword)
                {
                    XtraMessageBox.Show(this.lookAndFeelMsgBox, "Se requiere que actualice su contraseña.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    frmCambioContraseña frm = new frmCambioContraseña(ObjUser.UserId, ObjUser.Name, true);
                    frm.ShowDialog();
                    return;
                }

                //Verificamos si requiere cambiar contraseña porque pasaron los 90 días
                DateTime _fechaActual = ExodusBcBase.Utilities.getFechaActual();
                TimeSpan diasUltimoPassword = _fechaActual - ObjUser.PasswordChangeDate;
                int diasUltimoCambio = diasUltimoPassword.Days;
                //if (diasUltimoCambio >= VariablesGlobales.DIASEXPIRACION)
                //{
                //    XtraMessageBox.Show(this.lookAndFeelMsgBox, "Se requiere que actualice su contraseña.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //    frmCambioContraseña frm = new frmCambioContraseña(ObjUser.UserId, ObjUser.Name, true);
                //    frm.ShowDialog();
                //    return;
                //}


                DateTime fechaIngreso = ExodusBcBase.Utilities.getFechaActual();
                //Verificarmos días de ingreso al sistema
                bool diapermitido = false;
                int diaActual = (int)fechaIngreso.DayOfWeek;
                string[] lista = ObjUser.DaysOfAccess.Split(',');
                for (int i = 0; i < lista.Length; i++)
                {
                    if (Convert.ToInt32(lista[i]) == diaActual)
                    {
                        diapermitido = true;
                        break;
                    }
                }
                if (!diapermitido)
                {
                    XtraMessageBox.Show(this.lookAndFeelMsgBox, "Sin autorización para acceder al sistema según sus días de ingreso asignados, comuníquese con el Administrador del Sistema.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }


                //Verificamos hora de ingreso al sistema
                TimeSpan horaIngreso = Convert.ToDateTime(DateTime.Parse(_fechaAcceso).ToString("HH:mm")).TimeOfDay;
                if (horaIngreso <= ObjUser.StartTime && horaIngreso >= ObjUser.EndTime)
                {
                    XtraMessageBox.Show(this.lookAndFeelMsgBox, "Sin autorización para acceder al sistema en este horario, comuníquese con el Administrador del Sistema", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }


                //Todas las validaciones superadas, se da ingreso al sistema
                bool permitirIngreso = true;
                JArray session = ExodusBcBase.LoginWebService.getSessionActiva(ObjUser.UserId, host);
                if (session == null)
                    permitirIngreso = true;
                else
                {
                    if (session.Count > 0)
                    {
                        var liveflag = ExodusBcBase.LoginWebService.getItemFromJArray(session, "FKCatalogStatusId");
                        var sessionid = ExodusBcBase.LoginWebService.getItemFromJArray(session, "LoginSessionId");
                        var checktime = ExodusBcBase.LoginWebService.getItemFromJArray(session, "LogoutTime");

                        if (liveflag.ToString() != "") ObjUser.StatusLog = Convert.ToInt32(liveflag);
                        if (sessionid.ToString() != "") ObjUser.LoginSessionId = Convert.ToInt32(sessionid);
                        if (checktime.ToString() != "") ObjUser.LogoutTime = Convert.ToDateTime(checktime);
                        //significa que no hay registro de session activa
                        if (ObjUser.LoginSessionId == 0) permitirIngreso = true;
                        else if (ObjUser.StatusLog == 0) permitirIngreso = true;
                        else
                        {
                            TimeSpan dif = fechaIngreso - ObjUser.LogoutTime;
                            if (dif.TotalMinutes > VariablesGlobales.updateLiveTime) permitirIngreso = true;
                            else permitirIngreso = false;
                        }
                    }
                }

                if (permitirIngreso)
                {
                    ExodusBcBase.LoginWebService.setTimeLogin(ObjUser.LoginSessionId, VariablesGlobales.HOST_NAME);
                    InsertarLogin_Log(1);
                    InsertarLogin_Session(ObjUser.UserId);

                    VariablesGlobales.USER_ID = ObjUser.UserId;
                    VariablesGlobales.USER_NAME = ObjUser.Name;
                    VariablesGlobales.USER_PASSWORD = ObjUser.Password;
                    VariablesGlobales.USER_ROL_ID = ObjUser.FKUserRolesId;


                    Hide();
                    frmMain2 frm = new frmMain2();
                    frm.ShowDialog();
                    //Close();
                }
                else
                {
                    XtraMessageBox.Show(this.lookAndFeelMsgBox, "Existe una sesión activa con este usuario.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            catch (Exception exception)
            {
                XtraMessageBox.Show(this.lookAndFeelMsgBox, "Error al intentar iniciar sesión: " + exception.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Utilidades.Log.InsertarLog(Utilidades.Log.ErrorType.Error, "frmOffice::btnAceptar", ExodusBcBase.Helper.ExtraerExcepcion(exception), VariablesGlobales.PathDataLog);
            }
        }

        int ValidarPermiso()
        {
            try
            {
                //**BUSQUEDA DEL USUARIO**
                objUsuario = USERSLN.GetUserByUserName(txtNombreUsuario.Text.Trim());
                if (objUsuario == null)
                {
                    XtraMessageBox.Show(lookAndFeelMsgBox, "Usuario no encontrado.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return 0;
                }
                else
                {
                    //**VALIDA SI EL USUARIO PERTENECE LA OFICINA**
                    if (objUsuario.FKOfficeId != OFFICE_CODE)
                    {
                        XtraMessageBox.Show(lookAndFeelMsgBox, "Acceso denegado. Este usuario no esta autorizado para esta Oficina.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return 0;
                    }
                    //**VALIDA SI EL USUARIO ESTA ACTIVO**
                    else if (objUsuario.FKCatalogStatusId == 0)
                    {
                        XtraMessageBox.Show(lookAndFeelMsgBox, "Acceso denegado. La cuenta de usuario está inactiva.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return 0;
                    }

                    //**Verificar que la contraseña sea correcta**	
                    else if (objUsuario.Password != Utilidades.Seguridad.EncriptarSha1(txtContrasenia.Text.Trim()))
                    {
                        var objLoginLog = LN.LOGIN_FAIL_LOGLN.GetFailLogbyUserId(objUsuario.UserId);
                        if (objLoginLog != null)
                        {
                            if (objLoginLog.FailCount >= VariablesGlobales.CANTIDADMAXIMAINTENTOS)
                            {
                                objUsuario.FKCatalogStatusId = 0;
                                objUsuario.ResetPassword = true;
                                USERSLN.EditUser(objUsuario);
                                txtContrasenia.Select();
                            }

                            objLoginLog.FailCount = objLoginLog.FailCount + 1;
                            LOGIN_FAIL_LOGLN.EditLoginFailLog(objLoginLog);
                            XtraMessageBox.Show(lookAndFeelMsgBox, "Acceso denegado. La contraseña es incorrecta.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            txtContrasenia.Select();
                            return 0;
                        }
                    }
                    //**VALIDAR HORARIO USUARIO**
                    string horax = DateTime.Now.ToString("HH:mm");
                    var ObjHorarioUsuario = LN.USERSLN.GetAccessDatebyUserId(objUsuario.UserId, DateTime.Now.ToString("yyyy-MM-dd"), horax);
                    var ObjUsuarioDia = LN.USERSLN.GetAccessDatebyUserId(objUsuario.UserId);

                    //!Revisar esta parte
                    if (ObjUsuarioDia == null) return 0;

                    if (ObjUsuarioDia.DaysOfAccess != "-1")
                    {
                        string dias = ObjUsuarioDia.DaysOfAccess;
                        int diaActual = (int)DateTime.Now.DayOfWeek;
                        int _contadorDia = 0;

                        string[] substrings = dias.Split(',');

                        //if (Convert.ToInt32(dia) == diaActual && ObjHorarioUsuario) _contadorDia += 1;
                        foreach (var dia in substrings)
                            if (Convert.ToInt32(dia) == diaActual) _contadorDia += 1;
                        
                        
                        if (_contadorDia == 0)
                        {
                            XtraMessageBox.Show(this.lookAndFeelMsgBox, "Acceso denegado. Este usuario no esta autorizado para usar el sistema en este horario.", "Atención",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return 0;
                        }

                        //***Pasó todas las validaciones***
                        VariablesGlobales.USER_ID = Convert.ToInt32(objUsuario.UserId);
                        VariablesGlobales.USER_NAME = objUsuario.Name;
                        VariablesGlobales.USER_PASSWORD = objUsuario.Password;

                        return 1;
                    }
                    else
                    {
                        XtraMessageBox.Show(lookAndFeelMsgBox, "Acceso denegado. Este usuario no esta autorizado para usar el sistema en este horario.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return 0;
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(lookAndFeelMsgBox, "Hubo un error al tratar de validar los permisos. " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmLogin::ValidarPermiso", ExodusBcBase.Helper.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                return 0;
            }
        }

        public bool InsertarLogin_Log(int success)
        {
            bool salida = false;
            try
            {
                if (VariablesGlobales.MODOTRABAJO.Equals("ON"))
                {
                  
                    string sql = String.Format("INSERT INTO LoginLog (FKUserId, FKWorkStationId, LoginTime, FKCatalogStatusId) VALUES ({0},{1},(Select GetDate()),{2})", ObjUser.UserId, WS_ID, success);
                    string res = ws.Insertar(VariablesGlobales.TOKEN, sql, host);
                    if (res == "\"\"")
                        salida = true;
                }
                else
                    ExodusBcBase.Helper.Mensajes.getMensajeSinConexion();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(this.lookAndFeelMsgBox, "Error al intentar procesar log: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Utilidades.Log.InsertarLog(Utilidades.Log.ErrorType.Error, "frmLogin::InsertarLoginLog", ExodusBcBase.Helper.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
            return salida;
        }

        public bool InsertarLoginFailLog(int intentos)
        {
            bool salida = false;
            try
            {
                if (VariablesGlobales.MODOTRABAJO.Equals("ON"))
                {

                    string sql = String.Format("INSERT INTO LoginFailLog (FailCount, LastFailTime, FKuserId) VALUES ({0},(Select GetDate()),{1})",intentos, ObjUser.UserId);
                    string res = ws.Insertar(VariablesGlobales.TOKEN, sql, host);
                    if (res == "\"\"")
                        salida = true;
                }
                else
                    ExodusBcBase.Helper.Mensajes.getMensajeSinConexion();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(this.lookAndFeelMsgBox, "Error al intentar procesar log: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Utilidades.Log.InsertarLog(Utilidades.Log.ErrorType.Error, "frmLogin::InsertarLoginLog", ExodusBcBase.Helper.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
            return salida;
        }

        public bool InsertarLogin_Session(int _USER_ID)
        {
            bool salida = false;
            try
            {
                if (VariablesGlobales.MODOTRABAJO.Equals("ON"))
                {
                    string sql = String.Format("INSERT INTO LoginSession (FKUserId, FKWorkStationId, LoginTime, FKCatalogStatusId) VALUES ({0},{1},(SELECT GETDATE()),'{2}')", _USER_ID, WS_ID, 1);
                    string res = ws.Insertar(VariablesGlobales.TOKEN, sql, host);
                    if (res == "\"\"")
                        salida = true;
                }
                else
                    ExodusBcBase.Helper.Mensajes.getMensajeSinConexion();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(this.lookAndFeelMsgBox, "Error al intentar procesar sesión: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Utilidades.Log.InsertarLog(Utilidades.Log.ErrorType.Error, "frmLogin::InsertarLoginSession", ExodusBcBase.Helper.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
            return salida;
        }

        private class UserLogin
        {
            public int UserId { get; set; }
            public string Name { get; set; }
            public string Password { get; set; }
            public string DaysOfAccess { get; set; }
            public TimeSpan StartTime { get; set; }
            public TimeSpan EndTime { get; set; }
            public DateTime CreationDate { get; set; }
            public DateTime ModifyDate { get; set; }
            public int CreateUserId { get; set; }
            public int FKPersonId { get; set; }
            public int FKUserRolesId { get; set; }
            public int FKCatalogStatusId { get; set; }
            public int FKOfficeId { get; set; }
            public int FKRegistrationAgentId { get; set; }
            public Boolean ResetPassword { get; set; }
            public DateTime PasswordChangeDate { get; set; }

            public int LoginSessionId { get; set; }
            public int StatusLog { get; set; }
            public DateTime LogoutTime { get; set; }

        }

        bool ValidarControles()
        {
            if (lblEstacion.Text.Trim().Length == 0)
            {
                XtraMessageBox.Show(lookAndFeelMsgBox, "Esta estación no está registrada.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            if (txtNombreUsuario.Text.Trim().Length == 0)
            {
                XtraMessageBox.Show(lookAndFeelMsgBox, "Ingrese el nombre de usuario.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtNombreUsuario.Focus();
                return false;
            }

            if (txtContrasenia.Text.Trim().Length == 0)
            {
                XtraMessageBox.Show(lookAndFeelMsgBox, "Ingrese la contraseña.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtContrasenia.Focus();
                return false;
            }

            return true;
        }

        public void SetLabelText(string text, LabelControl lbl)
        {
            if (InvokeRequired)
            {
                SetLabelTextCallBack d = new SetLabelTextCallBack(SetLabelText);
                Invoke(d, new object[] { text, lbl });
            }
            else lbl.Text = text;
        }
    
        public void SetModoTrabajo(string text, string estado)
        {
            if (InvokeRequired)
            {
                SetModoTrabajoCallBack d = new SetModoTrabajoCallBack(SetModoTrabajo);
                Invoke(d, new object[] { text, estado });
            }
            else
            {
                if (estado == "C") lblModoTrabajo.ForeColor = Color.White;
                else if (estado == "ON")
                {
                    lblModoTrabajo.ForeColor = Color.FromArgb(46, 204, 113);
                    lblModoTrabajo.Appearance.Image = Properties.Resources.ic_online26x26;
                    //btnEntrar.Enabled = wyFinalizado;
                    btnEntrar.Enabled = true;
                }
                else
                {
                    lblModoTrabajo.ForeColor = Color.Tomato;
                    lblModoTrabajo.Appearance.Image = Properties.Resources.ic_menu_fueralinea_24x24;
                    btnEntrar.Enabled = true;
                }
                lblModoTrabajo.Text = text;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void frmLogin_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void pnlMoverForm_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void lblTitulo_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void wyUpd_UpToDate(object sender, wyDay.Controls.SuccessArgs e)
        {
            btnEntrar.Enabled = true;
        }

        private void wyUpd_CheckingFailed(object sender, wyDay.Controls.FailArgs e)
        {
            Log.InsertarLog(Log.ErrorType.Advertencia, "frmLogin::wyUpd_CheckingFailed", e.ErrorMessage, VariablesGlobales.PathDataLog);
            btnEntrar.Enabled = true;
        }

        private void wyUpd_UpdateSuccessful(object sender, wyDay.Controls.SuccessArgs e)
        {
            Log.InsertarLog(Log.ErrorType.Informacion, "frmLogin::wyUpd_UpdateSuccessful", "Aplicación actualizada a la versión: " + e.Version, VariablesGlobales.PathDataLog);
            XtraMessageBox.Show("El sistema fue actualizado exitosamente a la versión " + e.Version + ". Inicie nuevamente la aplicación", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
            Application.Exit();
        }

        private void wyUpd_UpdateFailed(object sender, wyDay.Controls.FailArgs e)
        {
            Log.InsertarLog(Log.ErrorType.Advertencia, "frmLogin::wyUpd_UpdateFailed", e.ErrorMessage, VariablesGlobales.PathDataLog);
            btnEntrar.Enabled = true;
        }

        private void wyUpd_Cancelled(object sender, EventArgs e)
        {
            btnEntrar.Enabled = true;
        }

        private void wyUpd_ReadyToBeInstalled(object sender, EventArgs e)
        {
            wyUpd.InstallNow();
        }

        private void lblSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void frmLogin_Shown(object sender, EventArgs e)
        {
            //ssForm.ShowWaitForm();
            //ssForm.SetWaitFormCaption("Conectando con el servidor");

            //try
            //{
            //    bool conectadoServer = VariablesGlobales.VerificarConexionAlservidor();

            //    if (!conectadoServer)
            //    {
            //        ssForm.CloseWaitForm();
            //        XtraMessageBox.Show("Esta estación no se pudo conectar al servidor." + Environment.NewLine +
            //            "Por favor verifique la conexión de la red" + Environment.NewLine +
            //            "o la configuración de la ruta del servidor, " + Environment.NewLine +
            //            "o contacte a soporte técnico.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    }

            //    if(conectadoServer)
            //    {
            //        if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();

            //        if (VariablesGlobales.NecesitaSincronizar)
            //        {
            //            Task.Run(async () =>
            //            {
            //                await VariablesGlobales.SincronizarCatalogos();
            //            });
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
            //    Log.InsertarLog(Log.ErrorType.Error, "frmLogin::frmLogin_Load", ExodusBcBase.Helper.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            //}
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {
            wyUpd.MenuItem = mn;
            wyUpd.ForceCheckForUpdate(true);

            //ExodusBcBase.Helper.EncriptarCadenaConexion();

            //Creación de Directorios
            if (!Directory.Exists(VariablesGlobales.PathDataDB)) Directory.CreateDirectory(VariablesGlobales.PathDataDB);
            if (!Directory.Exists(VariablesGlobales.PathDataLocal)) Directory.CreateDirectory(VariablesGlobales.PathDataLocal);
            if (!Directory.Exists(VariablesGlobales.PathDataLog)) Directory.CreateDirectory(VariablesGlobales.PathDataLog);
            if (!Directory.Exists(VariablesGlobales.PathDataSystem)) Directory.CreateDirectory(VariablesGlobales.PathDataSystem);
            if (!Directory.Exists(VariablesGlobales.PathPdf)) Directory.CreateDirectory(VariablesGlobales.PathPdf);


            SetModoTrabajo("Comprobando conexión...", "C");
            Iniciar();


            ssForm.ShowWaitForm();
            ssForm.SetWaitFormCaption("Conectando con el servidor");

            try
            {
                bool conectadoServer = VariablesGlobales.VerificarConexionAlservidor();

                if (!conectadoServer)
                {
                    ssForm.CloseWaitForm();
                    XtraMessageBox.Show("Esta estación no se pudo conectar al servidor." + Environment.NewLine +
                        "Por favor verifique la conexión de la red" + Environment.NewLine +
                        "o la configuración de la ruta del servidor, " + Environment.NewLine +
                        "o contacte a soporte técnico.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                if (conectadoServer)
                {
                    if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();

                    if (VariablesGlobales.NecesitaSincronizar)
                    {
                        Task.Run(async () =>
                        {
                            await VariablesGlobales.SincronizarCatalogos();
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                Log.InsertarLog(Log.ErrorType.Error, "frmLogin::frmLogin_Load", ExodusBcBase.Helper.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }

            txtNombreUsuario.Focus();
        }
    }
}