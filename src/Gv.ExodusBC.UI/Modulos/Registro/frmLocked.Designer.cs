﻿namespace Gv.ExodusBc.UI.Modulos.Registro
{
    partial class frmLocked
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtContrasenia = new DevExpress.XtraEditors.TextEdit();
            this.txtNombreUsuario = new DevExpress.XtraEditors.TextEdit();
            this.pnlLogin = new DevExpress.XtraEditors.PanelControl();
            this.btnSalir = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.btnEntrar = new DevExpress.XtraEditors.SimpleButton();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContrasenia.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNombreUsuario.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlLogin)).BeginInit();
            this.pnlLogin.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.White;
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Appearance.Options.UseForeColor = true;
            this.labelControl1.Location = new System.Drawing.Point(104, 173);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(65, 16);
            this.labelControl1.TabIndex = 20;
            this.labelControl1.Text = "Contraseña";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.labelControl3.Appearance.ForeColor = System.Drawing.Color.White;
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Appearance.Options.UseForeColor = true;
            this.labelControl3.Location = new System.Drawing.Point(104, 119);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(109, 16);
            this.labelControl3.TabIndex = 19;
            this.labelControl3.Text = "Nombre de usuario";
            // 
            // txtContrasenia
            // 
            this.txtContrasenia.EditValue = "";
            this.txtContrasenia.Location = new System.Drawing.Point(104, 194);
            this.txtContrasenia.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtContrasenia.Name = "txtContrasenia";
            this.txtContrasenia.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtContrasenia.Properties.Appearance.Options.UseFont = true;
            this.txtContrasenia.Properties.Appearance.Options.UseTextOptions = true;
            this.txtContrasenia.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtContrasenia.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtContrasenia.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtContrasenia.Properties.PasswordChar = '*';
            this.txtContrasenia.Size = new System.Drawing.Size(270, 28);
            this.txtContrasenia.TabIndex = 1;
            this.txtContrasenia.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtContrasenia_KeyDown);
            // 
            // txtNombreUsuario
            // 
            this.txtNombreUsuario.EditValue = "";
            this.txtNombreUsuario.Location = new System.Drawing.Point(104, 140);
            this.txtNombreUsuario.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtNombreUsuario.Name = "txtNombreUsuario";
            this.txtNombreUsuario.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtNombreUsuario.Properties.Appearance.Options.UseFont = true;
            this.txtNombreUsuario.Properties.Appearance.Options.UseTextOptions = true;
            this.txtNombreUsuario.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtNombreUsuario.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtNombreUsuario.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtNombreUsuario.Properties.ReadOnly = true;
            this.txtNombreUsuario.Size = new System.Drawing.Size(270, 28);
            this.txtNombreUsuario.TabIndex = 10;
            // 
            // pnlLogin
            // 
            this.pnlLogin.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.pnlLogin.Appearance.Options.UseBackColor = true;
            this.pnlLogin.Controls.Add(this.pictureEdit1);
            this.pnlLogin.Controls.Add(this.btnSalir);
            this.pnlLogin.Controls.Add(this.labelControl2);
            this.pnlLogin.Controls.Add(this.btnEntrar);
            this.pnlLogin.Controls.Add(this.txtNombreUsuario);
            this.pnlLogin.Controls.Add(this.labelControl1);
            this.pnlLogin.Controls.Add(this.txtContrasenia);
            this.pnlLogin.Controls.Add(this.labelControl3);
            this.pnlLogin.Location = new System.Drawing.Point(168, 114);
            this.pnlLogin.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.pnlLogin.LookAndFeel.UseDefaultLookAndFeel = false;
            this.pnlLogin.Name = "pnlLogin";
            this.pnlLogin.Size = new System.Drawing.Size(478, 313);
            this.pnlLogin.TabIndex = 21;
            // 
            // btnSalir
            // 
            this.btnSalir.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnSalir.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btnSalir.Appearance.Options.UseFont = true;
            this.btnSalir.Appearance.Options.UseForeColor = true;
            this.btnSalir.AppearanceHovered.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnSalir.AppearanceHovered.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnSalir.AppearanceHovered.Options.UseBackColor = true;
            this.btnSalir.AppearanceHovered.Options.UseFont = true;
            this.btnSalir.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btnSalir.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.cancel_32x321;
            this.btnSalir.Location = new System.Drawing.Point(274, 257);
            this.btnSalir.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnSalir.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(100, 37);
            this.btnSalir.TabIndex = 22;
            this.btnSalir.Text = "Salir";
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.White;
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Appearance.Options.UseForeColor = true;
            this.labelControl2.Location = new System.Drawing.Point(43, 80);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(392, 16);
            this.labelControl2.TabIndex = 21;
            this.labelControl2.Text = "Sesión bloqueada, por favor ingrese la contraseña para desbloquear";
            // 
            // btnEntrar
            // 
            this.btnEntrar.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnEntrar.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btnEntrar.Appearance.Options.UseFont = true;
            this.btnEntrar.Appearance.Options.UseForeColor = true;
            this.btnEntrar.AppearanceHovered.BackColor = System.Drawing.Color.Silver;
            this.btnEntrar.AppearanceHovered.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnEntrar.AppearanceHovered.Options.UseBackColor = true;
            this.btnEntrar.AppearanceHovered.Options.UseFont = true;
            this.btnEntrar.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btnEntrar.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_enter;
            this.btnEntrar.Location = new System.Drawing.Point(104, 257);
            this.btnEntrar.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnEntrar.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnEntrar.Name = "btnEntrar";
            this.btnEntrar.Size = new System.Drawing.Size(100, 37);
            this.btnEntrar.TabIndex = 2;
            this.btnEntrar.Text = "Entrar";
            this.btnEntrar.Click += new System.EventHandler(this.btnEntrar_Click);
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit1.EditValue = global::Gv.ExodusBc.UI.Properties.Resources.logoExodusBC3;
            this.pictureEdit1.Location = new System.Drawing.Point(147, 27);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.AllowFocused = false;
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit1.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit1.Size = new System.Drawing.Size(181, 43);
            this.pictureEdit1.TabIndex = 23;
            // 
            // frmLocked
            // 
            this.Appearance.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(820, 504);
            this.Controls.Add(this.pnlLogin);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmLocked";
            this.Opacity = 0.9D;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Bloqueo de sesión";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmLocked_FormClosing);
            this.Load += new System.EventHandler(this.frmLocked_Load);
            this.SizeChanged += new System.EventHandler(this.frmLocked_SizeChanged);
            ((System.ComponentModel.ISupportInitialize)(this.txtContrasenia.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNombreUsuario.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlLogin)).EndInit();
            this.pnlLogin.ResumeLayout(false);
            this.pnlLogin.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnEntrar;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txtContrasenia;
        private DevExpress.XtraEditors.TextEdit txtNombreUsuario;
        private DevExpress.XtraEditors.PanelControl pnlLogin;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.SimpleButton btnSalir;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
    }
}