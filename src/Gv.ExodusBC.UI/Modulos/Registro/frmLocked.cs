﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace Gv.ExodusBc.UI.Modulos.Registro
{
    public partial class frmLocked : DevExpress.XtraEditors.XtraForm
    {
        public long UserID { get; set; }
        public string UserName { get; set; }

        public string Password { get; set; }

        private bool validado;

        public frmLocked()
        {
            InitializeComponent();
        }

        private void frmLocked_Load(object sender, EventArgs e)
        {
            txtNombreUsuario.Text = this.UserName;
            this.WindowState = FormWindowState.Maximized;
        }

        private void btnEntrar_Click(object sender, EventArgs e)
        {
            if (txtContrasenia.Text.Trim().Length == 0)
            {
                XtraMessageBox.Show(frmMain2.lookAndFeelMsgBox, "Ingrese la contraseña", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtContrasenia.Focus();
                return;
            }

            else if (this.Password != Gv.Utilidades.Seguridad.EncriptarSha1(txtContrasenia.Text.Trim()))
            {
                XtraMessageBox.Show(frmMain2.lookAndFeelMsgBox, "La contraseña ingresada es inválida", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtContrasenia.SelectAll();
                txtContrasenia.Focus();
                return;
            }
            else
            {
                VariablesGlobales.IsLocked = false;
                validado = true;
                this.DialogResult = DialogResult.OK;
                this.Close();
            }

           
            
        }

        private void frmLocked_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.DialogResult != DialogResult.Abort)
            {
                if (!validado)
                {
                    e.Cancel = true;
                    btnEntrar_Click(sender, e);
                }
            }
        }

        private void frmLocked_SizeChanged(object sender, EventArgs e)
        {
            pnlLogin.Location = new Point(this.ClientSize.Width / 2 - pnlLogin.Size.Width / 2, this.ClientSize.Height / 2 - pnlLogin.Size.Height / 2);
          
            pnlLogin.Anchor = AnchorStyles.None;
        }

        private void txtContrasenia_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
                btnEntrar_Click(sender, e);
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            if (XtraMessageBox.Show(frmMain2.lookAndFeelMsgBox, "¿Está seguro que desea salir del sistema?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                frmMain2.confirmToClose = false;
                this.DialogResult = DialogResult.Abort;
                this.Close();
            }
            else
                txtContrasenia.Focus();
           
         
        }

      

    }
}