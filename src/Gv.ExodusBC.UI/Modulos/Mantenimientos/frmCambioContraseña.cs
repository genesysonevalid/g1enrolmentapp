﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Newtonsoft.Json.Linq;
using Gv.ExodusBc.EN;
using Gv.Utilidades;
using static Gv.ExodusBc.UI.ExodusBcBase;

namespace Gv.ExodusBc.UI.Modulos.Mantenimientos
{
    public partial class frmCambioContraseña : DevExpress.XtraEditors.XtraForm
    {
        private long USERID = 0;
        private string USERNAME = string.Empty;
        private bool VerificaClaveActual = false;
        WebReference.ExodusBcService service = new WebReference.ExodusBcService();

        public frmCambioContraseña(long UserId, string UserName, bool OldPassword)
        {
            InitializeComponent();
            USERID = UserId;
            USERNAME = UserName;
            VerificaClaveActual = OldPassword;
            if (OldPassword)
            {
                txtCurrentPassword.Enabled = true;
                txtCurrentPassword.Focus();
            }
            else
            {
                txtCurrentPassword.Enabled = false;
                txtNewPassword.Focus();
            }
            lblAviso.Text = "La contraseña debe tener como mínimo " + VariablesGlobales.CONTRASENIAMIN + " caracteres.";
        }

        private void init()
        {
            //try
            //{
            //    if (VariablesGlobales.MODOTRABAJO.Equals("ON"))
            //    {
            //        JArray post = new JArray();
            //        JObject ob1 = (JObject)JToken.FromObject(new JsonItem("USERS_SYNC", "SELECT USER_ID FROM USERS_SYNC WHERE USER_ID=" + USERID + ""));
            //        post.Add(ob1);
            //        string pre = post.ToString().Substring(0, post.ToString().Length);
            //        string res = service.GetJson(VariablesGlobales.Token, pre, host);

            //        JArray items;
            //        dynamic dyarr = JArray.Parse(res)[0];
            //        if (dyarr.Id == "USERS_SYNC")
            //        {
            //            items = dyarr.Obj;
            //            if (items.Count > 0)
            //            {
            //                foreach (JObject job in items)
            //                {
            //                    long id = (long)job.GetValue("USER_ID");
            //                    VerificaClaveActual = true;
            //                }
            //            }
            //        }
            //    }
            //    else
            //    {
            //        Verificar si existe en USERS_SYNC
            //        EN.Users user = LN.USERSLN.GetUserByUserName(USERNAME);
            //        USERS_SYNC user_sync = LN.USERSLN.GetUserSyncById(user.USER_ID);
            //        if (user_sync != null)
            //            VerificaClaveActual = true;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Log.InsertarLog(Log.ErrorType.Error, "frmCambioContraseña::init", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            //}
        }

        private void ValidaUsuarioRegistrado()
        {
            try
            {
                if (VariablesGlobales.MODOTRABAJO.Equals("ON"))
                {
                    EN.Users users = LN.USERSLN.GetUserByUserName(USERNAME);
                    if (users != null)
                    {
                        string encryp = Utilidades.Seguridad.EncriptarSha1(txtCurrentPassword.Text.Trim());
                        if (users.Password.Equals(encryp))
                        {
                            if ((txtNewPassword.Text.Length >= VariablesGlobales.CONTRASENIAMIN && txtConfirmPassword.Text.Length >= VariablesGlobales.CONTRASENIAMIN) && (txtCurrentPassword.Text.Length >= VariablesGlobales.CONTRASENIAMIN)) 
                            {
                                if (txtNewPassword.Text.Trim().Equals(txtConfirmPassword.Text.Trim()))
                                {
                                    if (VariablesGlobales.MODOTRABAJO.Equals("ON"))
                                    {
                                        if (!RequestCertificate.UpdateUserPasswordUser((int)users.UserId, Utilidades.Seguridad.EncriptarSha1(txtNewPassword.Text.Trim())))
                                        {
                                            XtraMessageBox.Show(frmMain2.lookAndFeelMsgBox, "Error al actualizar la contraseña.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                            Log.InsertarLog(Log.ErrorType.Error, "frmCambioContraseña::ValidaUsuarioRegistrado", "Error al actualizar la contraseña.", VariablesGlobales.PathDataLog);
                                        }
                                        else
                                        {
                                            //Actualiza tablas en SQLite
                                            users.Password = Utilidades.Seguridad.EncriptarSha1(txtNewPassword.Text.Trim());
                                            var userEditar = LN.USERSLN.EditUser(users);
                                            if(userEditar != null)
                                            {
                                                XtraMessageBox.Show(frmMain2.lookAndFeelMsgBox, "Credenciales sincronizadas correctamente.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                                VariablesGlobales.USER_PASSWORD = users.Password;
                                                Dispose();
                                            }
                                            else XtraMessageBox.Show(frmMain2.lookAndFeelMsgBox, "Error al actualiar la contraseña.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        }
                                    }
                                    else
                                    {
                                        //Actualiza tablas en SQLite
                                        users.Password = Gv.Utilidades.Seguridad.EncriptarSha1(txtNewPassword.Text.Trim());
                                        LN.USERSLN.EditUser(users);

                                        XtraMessageBox.Show(frmMain2.lookAndFeelMsgBox, "Credenciales sincronizadas correctamente.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        Dispose();
                                    }
                                }
                                else
                                {
                                    XtraMessageBox.Show(frmMain2.lookAndFeelMsgBox, "Las contraseñas ingresadas no coinciden.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    txtConfirmPassword.Focus();
                                }
                            }
                            else XtraMessageBox.Show(frmMain2.lookAndFeelMsgBox, "La nueva contraseña no cumplen con los requerimientos de longitud.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        else
                        {
                            XtraMessageBox.Show(frmMain2.lookAndFeelMsgBox, "La contraseña actual no coincide con la ingresada.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            txtCurrentPassword.ResetText();
                            txtCurrentPassword.Focus();
                        }
                    }
                    else
                    {
                        XtraMessageBox.Show(frmMain2.lookAndFeelMsgBox, "Usuario no tiene privilegios en esta oficina.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                else ExodusBcBase.Helper.Mensajes.getMensajeSinConexion();
            }
            catch (Exception ex)
            {
                Log.InsertarLog(Log.ErrorType.Error, "frmCambioContraseña::validaUsuarioRegistrado", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            try
            {
                if (VerificaClaveActual) ValidaUsuarioRegistrado();
            }
            catch (Exception ex)
            {
                Log.InsertarLog(Log.ErrorType.Error, "frmCambioContraseña::btnActualizar", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        private void frmCambioContraseña_Load(object sender, EventArgs e)
        {
            txtCurrentPassword.Focus();
        }
    }
}