﻿using System;
using System.Drawing;
using Newtonsoft.Json.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Gv.ExodusBc.UI.Modulos.Registro;
using Gv.Utilidades;
using static Gv.ExodusBc.UI.ExodusBcBase;
using Newtonsoft.Json;
using System.IO;
using System.Net;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using static Gv.ExodusBc.EN.Request;
using Gv.ExodusBc.LN;
using Gv.ExodusBc.UI.Modulos.Certificados.Reportes;
using System.Linq;
using DevExpress.XtraEditors.Repository;
using System.Data;
using Gv.ExodusBc.UI.Modulos.InspeccionPrimaria;
using Cogent.Biometrics;
using DevExpress.XtraEditors.Mask;
using System.Globalization;
using System.Threading.Tasks;
using System.Threading;
using DevExpress.XtraEditors.Controls;

namespace Gv.ExodusBc.UI.Modulos.Certificados
{
    public partial class frmCertificados : XtraForm
    {

        #region VARIABLES GLOBALES

        //***Variables Generales***
        public static JArray personData = null;
        public static JArray personDataRepresentantes = null;
        JArray customer = null;
        string NUMEROTICKET = string.Empty;
        int TipoPersona = 0;
        string pathAttached = string.Empty;
        string pathRequest = string.Empty;
        public static string PASSWORDCLIENT = string.Empty;
        int TipoBusqueda = 1;
        int PersonaId = 0;

        //***Representantes legales***
        public static List<EN.RepresentativeRequest> ListaRepresentantes = null;
        public static int TotalRepresentantes = 0;
        public frmAsociarRepresentantes frmRpst;
        public static string IdentidadRepresentante = string.Empty;
        public static string CARGOEMPRESA = string.Empty;
        public static string ESTADOCIVILREPRESENTANTE = string.Empty;
        public static string PROFESIONREPRESENTANTE = string.Empty;
        public static string NACIONALIDADREPRESENTANTE = string.Empty;
        public static string CORREOREPRESENTANTE = string.Empty;

        //***Documentos adjuntos***
        public static List<EN.RequestAttach> ListaDocumentosAdjuntos = null;
        public static int TotalDocPersonaNatural = 0;
        public static int TotalDocPersonaJuridica = 0;
        public frmAdjuntos frmAdj;
        public static List<EN.Attached> lstDocumentosAdjuntados = new List<EN.Attached>();

        //***Clientes***
        string DocumentNumberCustomer = string.Empty;

        //***Profesional titulado***
        public static string PROFESION = string.Empty;
        public static string NUMEROCOLEGIO = string.Empty;
        public static int INSTITUCIONEMITE_ID = 0;
        public static string INSTITUCIONEMITE = string.Empty;

        //***Validaciones Afis***
        System.Windows.Forms.Timer tmr1a1Certificados = new System.Windows.Forms.Timer();
        public delegate void SetLabelCallBack(LabelControl lbx, string texto, string tipo);
        AfisTransaccion afisRespuesta = new AfisTransaccion();
        WsAFIS.NistService wsAfis = new WsAFIS.NistService();
        GP.DatosGP wsFunction = new GP.DatosGP();
        int contador = 10;
        int intentos = 0;
        bool nistEnviado;
        string numeroConsultaAfis = string.Empty;
        public static AxCls cs500e = null;
        int TipoPersonaAfis = 0;
        public static JArray personArrayAfis = null;
        private enum ValidacionAfis
        {
            NO_INICIADO = 0,
            ENVIANDO_NIST = 1,
            NIST_ENVIADO = 2,
            ERROR_ENVIO_NIST = 3,
            ESPERANDO_RESPUESTA = 4,
            COMPLETADO = 5,
            TIMEOUT = 6
        }

        ValidacionAfis ValidacionHuellas = ValidacionAfis.NO_INICIADO;

        #endregion

        #region VALORES INICIALES DEL FORMULARIO
        public frmCertificados(frmAdjuntos frmAdj, frmAsociarRepresentantes frmRpst)
        {
            InitializeComponent();
            this.frmAdj = frmAdj;
            this.frmRpst = frmRpst;
            Init();
            
            try
            {
                ssForm.ShowWaitForm();
                CargaInfoData();
                ssForm.CloseWaitForm();

            }
            catch (Exception ex)
            {
                if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                XtraMessageBox.Show("Hubo un error al cargar alguna informacón. Intente en otro momento.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmCertificados::Load", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }

        }

        void Init()
        {
            //cs500e = new AxCls();
            lblTIpoDocumento.Text = string.Empty;
            lblIdentificacion.Text = string.Empty;
            lblNombres.Text = string.Empty;
            lblApellidos.Text = string.Empty;
            txtCorreoPn.Text= string.Empty;
            lblcantidadproductosPN.Text = string.Empty;
            txtTelefonoPn.Text = string.Empty; 
            pnlPersonaJuridica.Visible = false;
            picTicketGenerado.Visible = false;
            lblRespuestaGenerado.Visible = false;
            lueClientes.Visible = false;
            pnlClientes.Visible = false;
            btnCrearCliente.Visible = false;
            lblTituloPj.Visible = false;
            UpdateAdjuntos();
            //cs500e.ComprobarEscaner();
            //cs500e.cs500eInicializado = 1;
            btnAdjuntarPn.Enabled = true;
            List<string> lstProductos = new List<string>();

            if(ListaDocumentosAdjuntos != null) if (ListaDocumentosAdjuntos.Count() != 0) ListaDocumentosAdjuntos.Clear();
            if (ListaRepresentantes != null) if (ListaRepresentantes.Count() != 0) ListaRepresentantes.Clear();

            if (frmAdjuntos.Documentos != null) if (frmAdjuntos.Documentos.Count > 0) frmAdjuntos.Documentos.Clear();

            TotalRepresentantes = 0;
            TotalDocPersonaJuridica = 0;
            TotalDocPersonaNatural = 0;

            UpdateAdjuntos();
            UpdateRepresentantes();
        }


        #endregion

        #region FUNCIONES DE LOS CONTROLES

        private void frmCertificados_Load(object sender, EventArgs e)
        {
            //try
            //{
            //    ssForm.ShowWaitForm();
            //    CargaInfoData();
            //    ssForm.CloseWaitForm();

            //}
            //catch (Exception ex)
            //{
            //    if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
            //    XtraMessageBox.Show("Hubo un error al cargar alguna informacón. Intente en otro momento.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    Log.InsertarLog(Log.ErrorType.Error, "frmCertificados::Load", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            //}
        }

        void CargaInfoData()
        {
            ExodusBcBase.Helper.Combobox.SetComboByTypeDocumentId(lueTipoDocumentoPn);
            ExodusBcBase.Helper.Combobox.SetComboByDocumentType(lueTipoDocumentoPj);
            ExodusBcBase.Helper.Combobox.setCombo(lueSexo, Convert.ToInt32(LISTCATALOGSTYPES.ListTypesCatalogs.Sexo));
            ExodusBcBase.Helper.Combobox.GetComboProductsByProductId(lueProductosPn, (int)TypePerson.PRSNATURAL);
            ExodusBcBase.Helper.Combobox.GetComboProductsByProductId(lueProductosPj, (int)TypePerson.PRSJURIDICA);
            ExodusBcBase.Helper.Combobox.GetColegioProfesional(lueColegioProfesional);

            ExodusBcBase.Helper.Combobox.GetCostumers(lueClientes);

            customer = RequestCertificate.GetCustomer();

            lueTipoDocumentoPn.EditValue = null;
            lueTipoDocumentoPj.EditValue = null;
            lueSexo.EditValue = null;
            tmr1a1Certificados.Tick += tmr1a1Certificados_Tick;
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            LimpiarControles();
            frmRpst.LimpiarControles();
            frmMain2 changeValue = (frmMain2)Application.OpenForms["frmMain2"];
            changeValue.OpenForms("frmCertificados");
            CerrarFormCertificados();
            frmAsociarRepresentantes.Representantes.Clear();
            frmAdjuntos.Documentos.Clear();
        }
  
        private void btnBuscarRegistro_Click(object sender, EventArgs e)
        {
            lblRespuestaGenerado.Visible = false;
            picTicketGenerado.Visible = false;
            if (!ValidarEntradas()) return;

            try
            {
                ssForm.ShowWaitForm();
                personData = new JArray();
                
                if(TipoBusqueda == 1)
                    personData = RequestCertificate.GetPersonByDocument(Convert.ToInt32(lueTipoDocumentoPn.EditValue), txtNumeroDocumentoPn.Text,
                    txtPrimerNombre.Text.Trim(), txtPrimerApellido.Text.Trim(), dteFechaNacimiento.DateTime.Date, Convert.ToInt32(lueSexo.EditValue));
                else personData = RequestCertificate.GetPersonByNombres(txtPrimerNombre.Text.Trim(), txtPrimerApellido.Text.Trim(), dteFechaNacimiento.DateTime.Date, Convert.ToInt32(lueSexo.EditValue));

                if (personData.Count > 0)
                {
                    lblTIpoDocumento.Text = RequestCertificate.GetItemFromJArray(personData, "DocumentType").ToString();
                    lblIdentificacion.Text = RequestCertificate.GetItemFromJArray(personData, "DocumentNumber").ToString();
                    lblNombres.Text = RequestCertificate.GetItemFromJArray(personData, "Names").ToString();
                    lblApellidos.Text = RequestCertificate.GetItemFromJArray(personData, "LastNames").ToString();
                    txtCorreoPn.Text = RequestCertificate.GetItemFromJArray(personData, "PersonalEmail").ToString().ToLower();
                    txtTelefonoPn.Text = RequestCertificate.GetItemFromJArray(personData, "PersonalMovilPhone").ToString().ToLower();
                    PersonaId = Convert.ToInt32(RequestCertificate.GetItemFromJArray(personData, "PersonId").ToString());
                    //Foto
                    JArray arrayFoto = new JArray();
                    arrayFoto = RequestCertificate.GetLastPhoto(Convert.ToInt32(RequestCertificate.GetItemFromJArray(personData, "PersonId").ToString()));
                    foreach (JObject element in arrayFoto)
                        if (element["Image"].ToString() != null) picSolicitante.EditValue = ExodusBcBase.Helper.byteToImage((byte[])element["Image"]);

                    grpDetalleSolicitante.Visible = true;
                    btnSolicitarTicket.Visible = true;
                    TipoBusqueda = 1;
                    lueTipoDocumentoPn.Focus();
                }
                else if(personData.Count == 0 && pnlBusquedaAvanzada.Visible == false)
                {
                    if (XtraMessageBox.Show("La persona no se encontro con estos criterios. ¿Desea hacer otro tipo de búsqueda?", "Atención",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        txtNumeroDocumentoPn.Text = string.Empty;
                        pnlBusquedaBasica.Enabled = false;
                        pnlBusquedaBasica.Appearance.BackColor = Color.White;
                        pnlBusquedaAvanzada.Visible = true;
                        txtPrimerNombre.Focus();
                        TipoBusqueda = 2;
                    }
                }
                else if (personData.Count == 0 && pnlBusquedaAvanzada.Visible == true)
                {
                    pnlBusquedaBasica.Enabled = true;
                    pnlBusquedaAvanzada.Visible = false;
                    lueTipoDocumentoPn.EditValue = null;
                    LimpiarControles();
                    lueTipoDocumentoPn.Focus();
                    TipoBusqueda = 1;
                    XtraMessageBox.Show("La persona no se encuentra enrolada en el sistema.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                ssForm.CloseWaitForm();
            }
            catch (Exception ex)
            {
                if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                XtraMessageBox.Show("Hubo un error al hacer la búsqueda de la persona. Intente en otro momento.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmCertificados::btnBuscarRegistro", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        private void btnLeerIdentidad_Click(object sender, EventArgs e)
        {
            frmEnrolamientoLectora frmLector = new frmEnrolamientoLectora();
            frmLector.ShowDialog();

            if (frmEnrolamientoLectora.LecturaOK)
            {
                lueTipoDocumentoPn.ItemIndex = 0;
                txtNumeroDocumentoPn.Text = frmEnrolamientoLectora.objLectura.NoIdentidad;
                btnBuscarRegistro_Click(sender, e);
                lueTipoDocumentoPn.Focus();
            }
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            LimpiarControles();
            LimpiarVariables();
            frmAdj.limpiarVariables();
        }
     
        private void btnSolicitarTicket_Click(object sender, EventArgs e)
        {
            try
            {
                if (ListaDocumentosAdjuntos != null) ListaDocumentosAdjuntos.Clear();
                if (chkPersonaJ.Checked)
                {
                    if (lueProductosPj.EditValue == null)
                    {
                        XtraMessageBox.Show("Tipo de producto esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        lueProductosPj.Focus();
                        return;
                    }
                    if (txtNombresPj.Text == string.Empty)
                    {
                        XtraMessageBox.Show("El nombre o la razon social de la Persona Juridica esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        txtNombresPj.Focus();
                        return;
                    }
                    if (lueTipoDocumentoPj.EditValue == null)
                    {
                        XtraMessageBox.Show("El tipo de documento esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        lueTipoDocumentoPj.Focus();
                        return;
                    }
                    if (txtNumeroDocumentoPj.Text == string.Empty)
                    {
                        XtraMessageBox.Show("El número de documento esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        txtNumeroDocumentoPj.Focus();
                        return;
                    }
                    if (txtNumeroTelefonoPj.Text == string.Empty)
                    {
                        XtraMessageBox.Show("El número de teléfono esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        txtNumeroTelefonoPj.Focus();
                        return;
                    }
                    if (txtCorreoPj.Text == string.Empty)
                    {
                        XtraMessageBox.Show("El correo esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        txtCorreoPj.Focus();
                        return;
                    }
                    if (meDireccionPj.Text == string.Empty)
                    {
                        XtraMessageBox.Show("La direccion de la empresa esta vacía, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        meDireccionPj.Focus();
                        return;
                    }
                    if (TotalRepresentantes == 0)
                    {
                        XtraMessageBox.Show("Debe asociar como mínimo un representante.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        btnAsociarRepresentante.Focus();
                        return;
                    }
                    if (TotalDocPersonaJuridica == 0)
                    {
                        XtraMessageBox.Show("Debe adjuntar como mínimo el contrato del cliente.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        btnAdjuntarPj.Focus();
                        return;
                    }
                    if (PASSWORDCLIENT == string.Empty)
                    {
                        XtraMessageBox.Show("Debe crear la contraseña del cliente para la solictud.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        btnPasswordClientPj.Focus();
                        return;
                    }

                    //**VALIDAR DOCUMENTOS ADJUNTOS PERSONA JURIDICA**
                    ssForm.ShowWaitForm();
                    frmAdj.GetDocumentosAdjuntos();
                    int documentosAdjuntados = 0;
                    int cantidadAdjuntosj = 0;
                    string nombredocumentos = string.Empty;
                    var nombreadjuntos = DocumentTypePersonLN.GetDocumentByPersonType((int)TypePerson.PRSJURIDICA);
                    cantidadAdjuntosj = nombreadjuntos.Count();
                    List<EN.DocumentTypePerson> listaAuxiliar = new List<EN.DocumentTypePerson>();
                    var listadoadjuntos = nombreadjuntos;
                    
                    if (nombreadjuntos != null && ListaDocumentosAdjuntos != null)
                    {
                        foreach (var item in nombreadjuntos)
                        {
                            foreach (var nombre in ListaDocumentosAdjuntos)
                            {
                                if (item.Name == nombre.NameDocument)
                                {
                                    listaAuxiliar.Add(item);
                                    documentosAdjuntados = documentosAdjuntados + 1;
                                }
                            }
                        }
                        foreach (var item in listaAuxiliar) listadoadjuntos.Remove(item);

                        foreach (var item in listadoadjuntos)
                        {
                            if (nombredocumentos != string.Empty) nombredocumentos = nombredocumentos + ", " + item.Name;
                            else nombredocumentos = item.Name;
                        }

                        if (cantidadAdjuntosj != documentosAdjuntados)
                        {
                            if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                            XtraMessageBox.Show("Los siguientes documentos no han sido adjuntados: " + nombredocumentos, "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            lueProductosPj.Focus();
                            return;
                        }
                    }

                    if(TotalRepresentantes == 0)
                    {
                        if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                        XtraMessageBox.Show("Debe tener como mínino un Representant Legal.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }

                    if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                    if (XtraMessageBox.Show("Confirma que enviará la solicitud como Persona Juridica.", "Atención", MessageBoxButtons.YesNo,
                        MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        ssForm.ShowWaitForm();
                        TipoPersona = (int)TypePerson.PRSJURIDICA;
                        if (SaveRequest())
                        {
                            LimpiarControles();
                            LimpiarVariables();
                            pnlBusquedaBasica.Visible = true;
                            btnBuscarRegistro.Visible = true;
                            btnLimpiar.Visible = true;
                            frmAsociarRepresentantes.Representantes.Clear();
                            if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                            XtraMessageBox.Show("Solicitud creada satisfactoriamente.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                        else
                        {
                            if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                            XtraMessageBox.Show("Hubo un error al guardar la solicitud.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            Log.InsertarLog(Log.ErrorType.Error, "frmCertificados::SaveRequest", "Error al guardar la solicitid", VariablesGlobales.PathDataLog);
                            return;
                        }
                    }
                    else
                    {
                        if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                        return;
                    }
                }
                else
                {
                    if (lueProductosPn.EditValue == null)
                    {
                        XtraMessageBox.Show("Tipo de producto esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        lueProductosPn.Focus();
                        return;
                    }
                    if (txtCorreoPn.Text == string.Empty)
                    {
                        XtraMessageBox.Show("Correo de la persona esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        txtCorreoPn.Focus();
                        return;
                    }
                    if (txtTelefonoPn.Text == string.Empty)
                    {
                        XtraMessageBox.Show("Teléfono de la persona esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        txtTelefonoPn.Focus();
                        return;
                    }
                    if (TotalDocPersonaNatural == 0)
                    {
                        XtraMessageBox.Show("Debe adjuntar como mínimo el contrato del cliente.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        btnAdjuntarPn.Focus();
                        return;
                    }
                    if (chkProfesionalTitulo.Checked && lueColegioProfesional.EditValue != null)
                    {
                        if (NUMEROCOLEGIO == string.Empty)
                        {
                            XtraMessageBox.Show("No se ha digitado el número de colegiación.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            btnAcreditarTitulo.Focus();
                            return;
                        }
                    }
                    if (PASSWORDCLIENT == string.Empty)
                    {
                        XtraMessageBox.Show("Debe crear la contraseña del cliente para la solicitud.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        btnPasswordClient.Focus();
                        return;
                    }

                    //**VALIDAR DOCUMENTOS ADJUNTOS PERSONA NATURAL**
                    ssForm.ShowWaitForm();
                    frmAdj.GetDocumentosAdjuntos();
                    int cantidadAdjuntos = 0;
                    int documentosAdjuntados = 0;
                    var lstNombreAdjuntos = new List<EN.DocumentTypePerson>();
                    if (chkProfesionalTitulo.Checked && lueColegioProfesional.EditValue != null)
                        lstNombreAdjuntos = LN.DocumentTypePersonLN.GetDocumentByPersonType((int)TypePerson.PROFTITULADO);
                    else lstNombreAdjuntos = LN.DocumentTypePersonLN.GetDocumentByPersonType((int)TypePerson.PRSNATURAL);

                    cantidadAdjuntos = lstNombreAdjuntos.Count();

                    string nombredocumentos = string.Empty;
                    List<EN.DocumentTypePerson> listaAuxiliar = new List<EN.DocumentTypePerson>();
                    var listadoadjuntos = lstNombreAdjuntos;

                    if (lstNombreAdjuntos != null && ListaDocumentosAdjuntos != null)
                    {
                        foreach (var item in lstNombreAdjuntos)
                        {
                            foreach (var nombre in ListaDocumentosAdjuntos)
                            {
                                if (item.Name == nombre.NameDocument)
                                {
                                    listaAuxiliar.Add(item);
                                    documentosAdjuntados = documentosAdjuntados + 1;
                                }
                            }
                        }
                        foreach (var item in listaAuxiliar) listadoadjuntos.Remove(item);

                        foreach (var item in listadoadjuntos)
                        {
                            if (nombredocumentos != string.Empty) nombredocumentos = nombredocumentos + ", " + item.Name;
                            else nombredocumentos = item.Name;
                        }

                        if (cantidadAdjuntos != documentosAdjuntados)
                        {
                            if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                            XtraMessageBox.Show("Los siguientes documentos no han sido adjuntados: " + nombredocumentos, "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            lueProductosPj.Focus();
                            return;
                        }
                    }
                    if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                }

                ssForm.ShowWaitForm();
                TipoPersona = (int)TypePerson.PRSNATURAL;

                if (SaveRequest())
                {
                    LimpiarControles();
                    LimpiarVariables();
                    frmAdjuntos.Documentos.Clear();
                    if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                    XtraMessageBox.Show("Solicitud creada satisfactoriamente.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm(); 
                    XtraMessageBox.Show("Hubo un error al guardar la solicitud.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Log.InsertarLog(Log.ErrorType.Error, "frmCertificados::SaveRequest", "Error al guardar la solicitid", VariablesGlobales.PathDataLog);
                    return;
                }
            }
            catch (Exception ex)
            {
                if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                XtraMessageBox.Show("Hubo un error al guardar la solicitud.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmCertificados::SaveRequest", "Error al guardar la solicitid", VariablesGlobales.PathDataLog);
            }
        }

        private void chkPersonaJ_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPersonaJ.Checked)
            {
                lblTituloPj.Visible = true;
                lueTipoDocumentoPj.EditValue = 2;
                pnlPersonaJuridica.Visible = true;
                pnlPersonaNatural.Visible = false;
                grpDetalleSolicitante.Visible = true;
                lblTituloPn.Visible = false;
                picPaso3.Visible = false;
                lblTituloAccion.Visible = false;
                btnSolicitarTicket.Visible = true;
                pnlSolicitarTicket.Visible = false;
                pnlBusquedaBasica.Visible = false;
                btnBuscarRegistro.Visible = false;
                btnLeerIdentidad.Visible = false;
                btnLimpiar.Visible = false;
                lueTipoDocumentoPn.EditValue = null;
                lblcantidadproductosPN.Text = string.Empty;
                lblEstadoValidacion.Text = string.Empty;
                txtNumeroDocumentoPn.Text = string.Empty;
                btnAdjuntarPj.Enabled = false;
                //!btnValidarPersona.Visible = false;
                //!pnlVerificacionHuellas.Visible = false;
                TotalDocPersonaJuridica = 0;
                TotalDocPersonaNatural = 0;
                TotalRepresentantes = 0;
                UpdateRepresentantes();
                UpdateAdjuntos();
                btnAdjuntarPn.Enabled = true;
                lueProductosPn.EditValue = null;
                meDescripcionProductoPn.Text = string.Empty;
                PASSWORDCLIENT = string.Empty;
                chkProfesionalTitulo.Checked = false;
                chkProfesionalTitulo.Visible = false;

                if (frmAsociarRepresentantes.Representantes != null) frmAsociarRepresentantes.Representantes.Clear();
                if (lstDocumentosAdjuntados.Count > 0) lstDocumentosAdjuntados.Clear();
            }
            else
            {
                lblTituloPj.Visible = false;
                pnlPersonaJuridica.Visible = false;
                pnlPersonaNatural.Visible = true;
                grpDetalleSolicitante.Visible = false;
                lblTituloPn.Visible = true;
                picPaso3.Visible = true;
                lblTituloAccion.Visible = true;
                btnSolicitarTicket.Visible = false;
                pnlSolicitarTicket.Visible = true;
                pnlBusquedaBasica.Visible = true;
                btnBuscarRegistro.Visible = true;
                btnLimpiar.Visible = true;
                btnLeerIdentidad.Visible = true;
                btnAdjuntarPj.Enabled = true;
                //!btnValidarPersona.Visible = true;
                //!pnlVerificacionHuellas.Visible = true;
                frmAsociarRepresentantes.Representantes.Clear();
                TotalDocPersonaNatural = 0;
                TotalDocPersonaJuridica = 0;
                TotalRepresentantes = 0;
                UpdateRepresentantes();
                UpdateAdjuntos();

                txtNombresPj.Text = string.Empty;
                lueTipoDocumentoPj.EditValue = null;
                lueProductosPj.EditValue = null;
                txtNumeroDocumentoPj.Text = string.Empty;
                txtNumeroTelefonoPj.Text = string.Empty;
                txtCorreoPj.Text = string.Empty;
                meDireccionPj.Text = string.Empty;
                pnlPersonaJuridica.Visible = false;
                chkProfesionalTitulo.Visible = true;

            }
        }

        private void btnAdjuntarPn_Click(object sender, EventArgs e)
        {
            try
            {
                if (ValidarDocumentos((int)TypePerson.PRSNATURAL))
                {
                    txtTelefonoPn.Text = RequestCertificate.GetItemFromJArray(personData, "PersonalMovilPhone").ToString().ToLower();
                    rptPersonaNatural.PERSONAID = PersonaId;
                    rptPersonaNatural.CLIENTE = lblNombres.Text + " " + lblApellidos.Text;
                    rptPersonaNatural.RESIDENCIA = RequestCertificate.GetItemFromJArray(personData, "Colonia").ToString() + ", " + RequestCertificate.GetItemFromJArray(personData, "Address").ToString();
                    rptPersonaNatural.TELEFONOOFICINA = RequestCertificate.GetItemFromJArray(personData, "WorkPhone").ToString().ToLower();
                    rptPersonaNatural.TELEFONOCASA = RequestCertificate.GetItemFromJArray(personData, "HomePhone").ToString().ToLower();
                    rptPersonaNatural.TELEFONOMOVIL = txtTelefonoPn.Text.Trim();
                    rptPersonaNatural.CORREO = txtCorreoPn.Text.Trim();
                    rptPersonaNatural.IDENTIDAD = lblIdentificacion.Text;

                    string lstProductos = string.Empty;
                    if(chkProfesionalTitulo.Checked) lstProductos = PRODUCTSLN.GetProductByDescription((int)TypePerson.PROFTITULADO, Convert.ToInt32(lueProductosPn.EditValue));
                    else lstProductos = PRODUCTSLN.GetProductByDescription((int)TypePerson.PRSNATURAL, Convert.ToInt32(lueProductosPn.EditValue));

                    string listVigencia = VariablesGlobales.VIGENCIACERTIFICADOS;
                    if (lstProductos != null)
                    {
                        if (lstProductos.Contains("1 Año") && listVigencia.Contains("1")) rptPersonaNatural.VIGENCIA = (int)PRODUCTSLN.VigenciaCertificados.UNO;
                        else if (lstProductos.Contains("2 Años") && listVigencia.Contains("2")) rptPersonaNatural.VIGENCIA = (int)PRODUCTSLN.VigenciaCertificados.DOS;
                        else if (lstProductos.Contains("3 Años") && listVigencia.Contains("3")) rptPersonaNatural.VIGENCIA = (int)PRODUCTSLN.VigenciaCertificados.TRES;
                        else if (lstProductos.Contains("4 Años") && listVigencia.Contains("4")) rptPersonaNatural.VIGENCIA = (int)PRODUCTSLN.VigenciaCertificados.CUATRO;
                        else rptPersonaNatural.VIGENCIA = (int)PRODUCTSLN.VigenciaCertificados.CINCO;
                    }
                    frmVisorReporte.NOMBRECLIENTE = lblNombres.Text + " " + lblApellidos.Text;
                    frmAdjuntos.Documentos = lstDocumentosAdjuntados;
                    if (chkProfesionalTitulo.Checked) frmAdjuntos.TipoPrsTransaccion = (int)TypePerson.PROFTITULADO;
                    else frmAdjuntos.TipoPrsTransaccion = (int)TypePerson.PRSNATURAL;
                    frmAdjuntos frm = new frmAdjuntos();
                    frm.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Hubo un error al tratar de adjuntar el documento.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmCertificados::btnAdjuntarPn_Click", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        private void btnAdjuntarPj_Click(object sender, EventArgs e)
        {
            if (txtNombresPj.Text == string.Empty)
            {
                XtraMessageBox.Show("El nombre o la razón social esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtNombresPj.Focus();
                return;
            }
            if (txtNumeroTelefonoPj.Text == string.Empty)
            {
                XtraMessageBox.Show("El número de teléfono esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtNumeroTelefonoPj.Focus();
                return;
            }
            if (txtCorreoPj.Text == string.Empty)
            {
                XtraMessageBox.Show("El correo esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtCorreoPj.Focus();
                return;
            }
            if (meDireccionPj.Text == string.Empty)
            {
                XtraMessageBox.Show("La dirección de la empresa esta vacía, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                meDireccionPj.Focus();
                return;
            }

            try
            {
                frmRpst.GetRepresentantes();
                string NombreRepresentante = string.Empty;
                if (ListaRepresentantes.Count > 0)
                {
                    int Contador = 0;
                    foreach (var rpt in ListaRepresentantes)
                    {
                        Contador++;
                        EN.RepresentativeRequest rpRq = new EN.RepresentativeRequest();
                        rpRq.Names = rpt.Names;
                        rpRq.LastNames = rpt.LastNames;
                        if (Contador == 1) NombreRepresentante = rpt.Names + " " + rpt.LastNames;
                    }
                }

                if (personDataRepresentantes != null)
                {
                    rptPersonaJuridicaPublica.PERSONAID = Convert.ToInt32(RequestCertificate.GetItemFromJArray(personDataRepresentantes, "PersonId").ToString());
                    rptPersonaJuridicaPrivada.PERSONAID = Convert.ToInt32(RequestCertificate.GetItemFromJArray(personDataRepresentantes, "PersonId").ToString());
                }
                else
                {
                    rptPersonaJuridicaPublica.PERSONAID = PersonaId;
                    rptPersonaJuridicaPrivada.PERSONAID = PersonaId;
                }

                //***Contrato Empresa Privada***    
                rptPersonaJuridicaPrivada.NOMBREREPRESENTANTE = NombreRepresentante;
                rptPersonaJuridicaPrivada.NOMBRESOCIEDAD = txtNombresPj.Text.Trim();
                rptPersonaJuridicaPrivada.ESTADOCIVIL = ESTADOCIVILREPRESENTANTE;
                rptPersonaJuridicaPrivada.PROFESION = PROFESIONREPRESENTANTE;
                rptPersonaJuridicaPrivada.NACIONALIDAD = NACIONALIDADREPRESENTANTE;
                rptPersonaJuridicaPrivada.CORREOOREPRESENTANTE = CORREOREPRESENTANTE;
                rptPersonaJuridicaPrivada.DIRECCIONOFICINA = meDireccionPj.Text.Trim();
                rptPersonaJuridicaPrivada.TELEFONOOFICINA = txtNumeroTelefonoPj.Text.Trim();
                rptPersonaJuridicaPrivada.CORREOOFICINA = txtCorreoPj.Text.Trim();
                rptPersonaJuridicaPrivada.RTN = txtNumeroDocumentoPj.Text.Trim();
                rptPersonaJuridicaPrivada.IDENTIDADREPRESENTANTE = RequestCertificate.GetItemFromJArray(personDataRepresentantes, "DocumentNumber").ToString();
                rptPersonaJuridicaPrivada.CARGOEMPRESA = CARGOEMPRESA;


                //***Contrato Empresa Publica***    
                rptPersonaJuridicaPublica.NOMBREREPRESENTANTE = NombreRepresentante;
                rptPersonaJuridicaPublica.NOMBRESOCIEDAD = txtNombresPj.Text.Trim();
                rptPersonaJuridicaPublica.ESTADOCIVIL = ESTADOCIVILREPRESENTANTE;
                rptPersonaJuridicaPublica.PROFESION = PROFESIONREPRESENTANTE;
                rptPersonaJuridicaPublica.NACIONALIDAD = NACIONALIDADREPRESENTANTE;
                rptPersonaJuridicaPublica.CORREOOREPRESENTANTE = CORREOREPRESENTANTE;
                rptPersonaJuridicaPublica.DIRECCIONOFICINA = meDireccionPj.Text.Trim();
                rptPersonaJuridicaPublica.TELEFONOOFICINA = txtNumeroTelefonoPj.Text.Trim();
                rptPersonaJuridicaPublica.CORREOOFICINA = txtCorreoPj.Text.Trim();
                rptPersonaJuridicaPublica.RTN = txtNumeroDocumentoPj.Text.Trim();
                rptPersonaJuridicaPublica.IDENTIDADREPRESENTANTE = RequestCertificate.GetItemFromJArray(personDataRepresentantes, "DocumentNumber").ToString();
                rptPersonaJuridicaPublica.CARGOEMPRESA = CARGOEMPRESA;

                string producto = PRODUCTSLN.GetProductByDescription((int)TypePerson.PRSJURIDICA, Convert.ToInt32(lueProductosPj.EditValue));
                string listVigencia = VariablesGlobales.VIGENCIACERTIFICADOS;
                if (producto != null) 
                {
                    if (producto.Contains("1 Año") && listVigencia.Contains("1"))
                    {
                        rptPersonaJuridicaPrivada.VIGENCIA = (int)PRODUCTSLN.VigenciaCertificados.UNO;
                        rptPersonaJuridicaPublica.VIGENCIA = (int)PRODUCTSLN.VigenciaCertificados.UNO;
                    }
                    else if (producto.Contains("2 Años") && listVigencia.Contains("2"))
                    {
                        rptPersonaJuridicaPrivada.VIGENCIA = (int)PRODUCTSLN.VigenciaCertificados.DOS;
                        rptPersonaJuridicaPublica.VIGENCIA = (int)PRODUCTSLN.VigenciaCertificados.DOS;
                    }
                    else if (producto.Contains("3 Años") && listVigencia.Contains("3"))
                    {
                        rptPersonaJuridicaPrivada.VIGENCIA = (int)PRODUCTSLN.VigenciaCertificados.DOS;
                        rptPersonaJuridicaPublica.VIGENCIA = (int)PRODUCTSLN.VigenciaCertificados.TRES;
                    }
                    else if (producto.Contains("4 Años") && listVigencia.Contains("4"))
                    {
                        rptPersonaJuridicaPrivada.VIGENCIA = (int)PRODUCTSLN.VigenciaCertificados.DOS;
                        rptPersonaJuridicaPublica.VIGENCIA = (int)PRODUCTSLN.VigenciaCertificados.CUATRO;
                    }
                    else
                    {
                        rptPersonaJuridicaPrivada.VIGENCIA = (int)PRODUCTSLN.VigenciaCertificados.DOS;
                        rptPersonaJuridicaPublica.VIGENCIA = (int)PRODUCTSLN.VigenciaCertificados.CINCO;
                    }
                }

                frmAdjuntos.Documentos = lstDocumentosAdjuntados;
                frmAdjuntos.TipoPrsTransaccion = (int)TypePerson.PRSJURIDICA;
                frmAdjuntos frm = new frmAdjuntos();
                frm.ShowDialog();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Hubo un error al tratar de adjuntar el documento.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmCertificados::btnAdjuntarPj_Click", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        private void btnAsociarRepresentante_Click(object sender, EventArgs e)
        {
            frmAsociarRepresentantes.NUMERODOCUMENTOPJ = txtNumeroDocumentoPj.Text.Trim().Replace("-", "");
            frmAsociarRepresentantes.NOMBREPJ = txtNombresPj.Text.Trim();
            frmAsociarRepresentantes frm = new frmAsociarRepresentantes();
            frm.ShowDialog();
        }

        private void btnPasswordClient_Click(object sender, EventArgs e)
        {
            frmPasswordCliente frm = new frmPasswordCliente();
            frm.ShowDialog();
        }

        private void btnPasswordClientPj_Click(object sender, EventArgs e)
        {
            frmPasswordCliente frm = new frmPasswordCliente();
            frm.ShowDialog();
        }

        private void chkOtrosClientes_CheckedChanged(object sender, EventArgs e)
        {
            lueClientes.EditValue = null;
            if (chkOtrosClientes.Checked)
            {
                lueClientes.Visible = true;
                btnCrearCliente.Visible = true;
            }
            else
            {
                lueClientes.Visible = false;
                btnCrearCliente.Visible = false;
                pnlClientes.Visible = false;
            }
        }

        private void lueClientes_EditValueChanged(object sender, EventArgs e)
        {
            pnlClientes.Visible = false;
            JArray objCliente = null;
            try
            {
                objCliente = RequestCertificate.GetExistCustomer(lueClientes.Text);
                if (objCliente.Count > 0)
                {
                    foreach (JObject element in objCliente)
                    {
                        lblNombreCliente.Text = element["Names"].ToString();
                        lblApellidosCliente.Text = element["LastNames"].ToString();
                        lblTituloApellidoCliente.Visible = true;
                        lblApellidosCliente.Visible = true;
                        pnlClientes.Visible = true;
                        DocumentNumberCustomer = lueClientes.Text;
                        if (lblApellidosCliente.Text == string.Empty)
                        {
                            lblTituloApellidoCliente.Visible = false;
                            lblApellidosCliente.Visible = false;
                        }
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Hubo un error al tratar de visualizar el registro.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmCertificados::lueClientes_EditValueChanged", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        private void btnCrearCliente_Click(object sender, EventArgs e)
        {
            frmClientes frm = new frmClientes();
            frm.ShowDialog();
        }

        private void lueProductosPn_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (lueProductosPn.EditValue != null)
                {
                    btnSolicitarTicket.Enabled = false;
                    LookUpEdit editor = sender as LookUpEdit;
                    object value = editor.GetColumnValue("Description");
                    meDescripcionProductoPn.Text = value.ToString();
                    wsFunction = new GP.DatosGP();
                    DataSet ds = new DataSet();
                    double existencia = 0;
                    ssForm.ShowWaitForm();
                    //ds = wsFunction.DisponibilidadITEM_por_Bodega(lueProductosPn.Text, VariablesGlobales.OFFICEIDGP);
                    ds = wsFunction.DisponibilidadITEM_por_Bodega(lueProductosPn.Text, "1001");
                    if (ds.Tables[0].Rows.Count != 0 )
                    {
                        frmExitenciaProducto.dsProductos = ds;
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            string cantidad = Convert.ToString(string.Format("{0:N}", dr["Cantidad"]));
                            existencia = Convert.ToDouble(cantidad);
                            if (cantidad != string.Empty)
                            {
                                if (existencia <= 0)
                                {
                                    lblcantidadproductosPN.ForeColor = Color.Red;
                                    lblcantidadproductosPN.Text = "NO PUEDE GENERAR UN CERTIFICADO, NO HAY STOCK DEL PRODUCTO, CANTIDAD EN EXISTENCIA: " + cantidad;
                                }
                                else
                                {
                                    lblcantidadproductosPN.ForeColor = Color.Black;
                                    lblcantidadproductosPN.Text = "PRODUCTOS EN EXISTENCIA: " + cantidad;
                                }
                            }
                        }
                        ssForm.CloseWaitForm();
                        btnSolicitarTicket.Enabled = (existencia <= 0) ? false : true;

                        frmExitenciaProducto frm = new frmExitenciaProducto();
                        frm.ShowDialog();
                    }
                    else
                    {
                        ssForm.CloseWaitForm();
                        XtraMessageBox.Show("No hay existencia del producto en GP.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                XtraMessageBox.Show("Hubo un error al mostrar la descripción y las existencia del producto.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmCertificados::lueProductosPn_EditValueChanged", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        private void lueProductosPj_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (lueProductosPj.EditValue != null)
                {
                    btnSolicitarTicket.Enabled = true;
                    wsFunction = new GP.DatosGP();
                    DataSet ds = new DataSet();
                    double existencia = 0;
                    ds = wsFunction.DisponibilidadITEM_por_Bodega(lueProductosPj.Text, VariablesGlobales.OFFICEIDGP);

                    if (ds.Tables[0].Rows.Count != 0)
                    {
                        frmExitenciaProducto.dsProductos = ds;
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            string cantidad = Convert.ToString(string.Format("{0:N}", dr["Cantidad"]));
                            existencia = Convert.ToDouble(cantidad);
                            if (cantidad != string.Empty)
                            {
                                if (existencia <= 0)
                                {
                                    lblcantidadproductosPN.ForeColor = System.Drawing.Color.Red;
                                    lblcantidadproductosPN.Text = "NO PUEDE GENERAR UN CERTIFICADO, NO HAY STOCK DEL PRODUCTO, CANTIDAD EN EXISTENCIA: " + cantidad;
                                }
                                else
                                {
                                    lblcantidadproductosPN.ForeColor = System.Drawing.Color.Black;
                                    lblcantidadproductosPN.Text = "PRODUCTOS EN EXISTENCIA: " + cantidad;
                                }
                            }
                        }
                        btnSolicitarTicket.Enabled = (existencia <= 0) ? false : true;
                        frmExitenciaProducto frm = new frmExitenciaProducto();
                        frm.ShowDialog();
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Hubo un error al mostrar la descripción del producto.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmCertificados::lueProductosPj_EditValueChanged", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }

        }

        #endregion

        #region METODOS Y PROCEDIMIENTOS

        private bool ValidarEntradas()
        {
            if (pnlBusquedaBasica.Enabled == true)
            {
                if (lueTipoDocumentoPn.EditValue == null)
                {
                    XtraMessageBox.Show("Tipo de Documento esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    lueTipoDocumentoPn.Focus();
                    return false;
                }

                if (txtNumeroDocumentoPn.Text == string.Empty)
                {
                    XtraMessageBox.Show("Número de identificación esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtNumeroDocumentoPn.Focus();
                    return false;
                }
            }
            return true;
        }

        public void GetCustomers()
        {
            customer = RequestCertificate.GetCustomer();
            ExodusBcBase.Helper.Combobox.GetCostumers(lueClientes);
        }

        void LimpiarControles()
        {
            txtNumeroDocumentoPn.Text = string.Empty;
            grpDetalleSolicitante.Visible = false;
            pnlBusquedaAvanzada.Visible = false;
            pnlBusquedaBasica.Enabled = true;
            txtPrimerNombre.Text = string.Empty;
            txtPrimerApellido.Text = string.Empty;
            dteFechaNacimiento.EditValue = null;
            lueSexo.EditValue = null;
            picTicketGenerado.Visible = false;
            lblRespuestaGenerado.Visible = false;
            lueTipoDocumentoPn.EditValue = null;
            lueProductosPn.EditValue = null;
            txtCorreoPn.Text = string.Empty;
            txtTelefonoPn.Text = string.Empty;
            TotalDocPersonaNatural = 0;
            lblEstadoValidacion.Text = "No Iniciada";
            lblEstadoValidacion.ForeColor = Color.FromArgb(55, 71, 79);
            meDescripcionProductoPn.Text = string.Empty;
            btnSolicitarTicket.Enabled = true;
            btnAdjuntarPn.Enabled = true;
            lblcantidadproductosPN.Text = string.Empty;
            lueTipoDocumentoPn.Focus();
            chkFacturaManual.Checked = false;

            //otros cliente
            chkOtrosClientes.Checked = false;

            //Persona juridica
            txtNombresPj.Text = string.Empty;
            lueTipoDocumentoPj.EditValue = null;
            lueProductosPj.EditValue = null;
            txtNumeroDocumentoPj.Text = string.Empty;
            txtNumeroTelefonoPj.Text = string.Empty;
            txtCorreoPj.Text = string.Empty;
            meDireccionPj.Text = string.Empty;
            pnlPersonaJuridica.Visible = false;
            TotalDocPersonaJuridica = 0;
            chkPersonaJ.Checked = false;

            UpdateAdjuntos();

        }

        void LimpiarVariables()
        {
            personData = null;
            personDataRepresentantes = null;
            customer = null;
            pathAttached = string.Empty;
            pathRequest = string.Empty;

            if (ListaDocumentosAdjuntos != null) if (ListaDocumentosAdjuntos.Count() != 0) ListaDocumentosAdjuntos.Clear();
            if (ListaRepresentantes != null) if (ListaRepresentantes.Count() != 0) ListaRepresentantes.Clear();
            if (lstDocumentosAdjuntados != null) if (lstDocumentosAdjuntados.Count() != 0) lstDocumentosAdjuntados.Clear();

            TotalRepresentantes = 0;
            TotalDocPersonaNatural = 0;
            TotalDocPersonaJuridica = 0;
            PASSWORDCLIENT = string.Empty;
            DocumentNumberCustomer = string.Empty;
            PersonaId = 0;
            TipoPersona = 0;
            PROFESION = string.Empty;
            NUMEROCOLEGIO = string.Empty;
            INSTITUCIONEMITE = string.Empty;
        }

        public void CerrarFormCertificados()
        {
            LimpiarControles();
            frmRpst.LimpiarControles();
            frmMain2 changeValue = (frmMain2)Application.OpenForms["frmMain2"];
            changeValue.OpenForms("frmCertificados");
            frmAsociarRepresentantes.Representantes.Clear();
            frmAdjuntos.Documentos.Clear();
            //cs500e.DesconectarEscaner();
            Close();
        }

        bool ValidarDocumentos(int tPersona)
        {
            if (lueProductosPn.EditValue == null && tPersona == (int)TypePerson.PRSNATURAL)
            {
                XtraMessageBox.Show("Tipo de producto esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                lueProductosPn.Focus();
                return false;
            }
            else if (lueProductosPj.EditValue == null && tPersona == (int)TypePerson.PRSJURIDICA)
            {
                XtraMessageBox.Show("Tipo de producto esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                lueProductosPj.Focus();
                return false;
            }
            else if(chkProfesionalTitulo.Checked && lueColegioProfesional.EditValue == null)
            {
                XtraMessageBox.Show("Colegio profesional esta vacío, esta campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                lueColegioProfesional.Focus();
                return false;
            }
            else if (chkProfesionalTitulo.Checked && lueProductosPn.EditValue == null)
            {
                XtraMessageBox.Show("Tipo de producto esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                lueProductosPn.Focus();
                return false;
            }
            return true;
        }

        public void UpdateAdjuntos()
        {
            lblFilesPn.Text = "(" + TotalDocPersonaNatural.ToString() + ") archivos";
            lblFilesPj.Text = "(" + TotalDocPersonaJuridica.ToString() + ") archivos";
        }

        public void UpdateRepresentantes()
        {
            lblRepresentantes.Text = "(" + TotalRepresentantes.ToString() + ") Representantes";
            if (TotalRepresentantes > 0) btnAdjuntarPj.Enabled = true;
        }

        public void LimpiarListadosPorEnvioRevision()
        {
            TotalRepresentantes = 0;
            TotalDocPersonaJuridica = 0;
            TotalDocPersonaNatural = 0;
        }

        bool SaveRequest()
        {
            try
            {
                //**GUARDAR SOLICITUD**
                EN.Request rq = new EN.Request();
                rq.FKCatalogStatusId = 1;
                rq.FKCreateUserId = Convert.ToInt32(VariablesGlobales.USER_ID);
                rq.FKTransactionStatusId = Convert.ToInt32(TransactionStatus.SOLICITUDCREADA);
                rq.FKCertificateOfficeId = Convert.ToInt32(VariablesGlobales.OFFICE_CERTIFICATE_ID);
                rq.FKRegistrationAuthorityId = Convert.ToInt32(VariablesGlobales.OFFICE_REGISTRATIONAUTHORITY_ID);
                rq.FKRegistrationAgentId = Convert.ToInt32(VariablesGlobales.REGISTRATION_AGENT_ID);
                rq.FKOfficeId = Convert.ToInt32(VariablesGlobales.OFFICE_ID);
                rq.ZipCode = Convert.ToInt32(VariablesGlobales.OFFICEZIPCODE);
                rq.CreationDate = Convert.ToDateTime(LoginWebService.getStatusConecction());
                rq.ModifyDate = Convert.ToDateTime(LoginWebService.getStatusConecction());
                rq.PasswordClient = PASSWORDCLIENT;
                rq.DocumentNumberCustomer = DocumentNumberCustomer;
                rq.FKWorkStationId = Convert.ToInt32(VariablesGlobales.WS_ID);
                rq.FacturaManual = lueFacturasCreadas.Text;

                var insertRequest = (dynamic)null;

                if (TipoPersona == (int)TypePerson.PRSJURIDICA)
                {
                    rq.DocumentNumberNaturalPerson = RequestCertificate.GetItemFromJArray(personDataRepresentantes, "DocumentNumber").ToString();
                    rq.Names = RequestCertificate.VerificarCaracteresEspeciales(RequestCertificate.GetItemFromJArray(personDataRepresentantes, "Names").ToString());
                    rq.LastName = RequestCertificate.VerificarCaracteresEspeciales(RequestCertificate.GetItemFromJArray(personDataRepresentantes, "LastNames").ToString());
                    rq.Email = RequestCertificate.GetItemFromJArray(personDataRepresentantes, "PersonalEmail").ToString();
                    rq.CellPhonePersonal = RequestCertificate.GetItemFromJArray(personDataRepresentantes, "PersonalMovilPhone").ToString().Replace("-", "");
                    rq.PhoneWork = RequestCertificate.GetItemFromJArray(personDataRepresentantes, "WorkPhone").ToString();
                    rq.FKDocumentTypeNaturalPerson = Convert.ToInt32(RequestCertificate.GetItemFromJArray(personDataRepresentantes, "DocumentTypeId").ToString());
                    rq.FKCountryId = Convert.ToInt32(RequestCertificate.GetItemFromJArray(personDataRepresentantes, "FKCountryId").ToString());
                    rq.FKStateCountryId = Convert.ToInt32(RequestCertificate.GetItemFromJArray(personDataRepresentantes, "FKStateCountryId").ToString());
                    rq.FKStateCityId = Convert.ToInt32(RequestCertificate.GetItemFromJArray(personDataRepresentantes, "FKStateCityId").ToString());
                    rq.FKNeighborhoodId = Convert.ToInt32(RequestCertificate.GetItemFromJArray(personDataRepresentantes, "FKNeighborhoodId").ToString());
                    rq.FKEnrolmentId = Convert.ToInt64(RequestCertificate.GetItemFromJArray(personDataRepresentantes, "EnrolmentId").ToString());
                    rq.FKPersonId = Convert.ToInt32(RequestCertificate.GetItemFromJArray(personDataRepresentantes, "PersonId").ToString());
                    rq.FKProductId = Convert.ToInt32(lueProductosPj.EditValue);
                    rq.NameLegalPerson = RequestCertificate.VerificarCaracteresEspeciales(txtNombresPj.Text.Trim());
                    rq.DocumentNumberLegalPerson = txtNumeroDocumentoPj.Text.Replace("-", "");
                    rq.FKDocumentTypeLegalPerson = Convert.ToInt32(lueTipoDocumentoPj.EditValue);
                    rq.LegalPersonEmail = txtCorreoPj.Text.Trim();
                    rq.LegalPersonPhone = txtNumeroTelefonoPj.Text.Trim();
                    rq.FKPersonTypeId = TipoPersona;

                    insertRequest = RequestCertificate.InsertNewRequestPJ(rq);
                }
                else
                {
                    rq.DocumentNumberNaturalPerson = RequestCertificate.GetItemFromJArray(personData, "DocumentNumber").ToString();
                    rq.Names = RequestCertificate.VerificarCaracteresEspeciales(RequestCertificate.GetItemFromJArray(personData, "Names").ToString());
                    rq.LastName = RequestCertificate.VerificarCaracteresEspeciales(RequestCertificate.GetItemFromJArray(personData, "LastNames").ToString());
                    rq.Email = RequestCertificate.GetItemFromJArray(personData, "PersonalEmail").ToString();
                    rq.CellPhonePersonal = RequestCertificate.GetItemFromJArray(personData, "PersonalMovilPhone").ToString().Replace("-", "");
                    rq.PhoneWork = RequestCertificate.GetItemFromJArray(personData, "WorkPhone").ToString();
                    rq.FKDocumentTypeNaturalPerson = Convert.ToInt32(RequestCertificate.GetItemFromJArray(personData, "DocumentTypeId").ToString());
                    rq.FKCountryId = Convert.ToInt32(RequestCertificate.GetItemFromJArray(personData, "FKCountryResidenceId").ToString());
                    rq.FKCountryResidenceId = Convert.ToInt32(RequestCertificate.GetItemFromJArray(personData, "FKCountryResidenceId").ToString());
                    rq.FKStateCountryId = Convert.ToInt32(RequestCertificate.GetItemFromJArray(personData, "FKStateCountryId").ToString());
                    rq.FKStateCityId = Convert.ToInt32(RequestCertificate.GetItemFromJArray(personData, "FKStateCityId").ToString());
                    rq.FKNeighborhoodId = Convert.ToInt32(RequestCertificate.GetItemFromJArray(personData, "FKNeighborhoodId").ToString());
                    rq.FKEnrolmentId = Convert.ToInt64(RequestCertificate.GetItemFromJArray(personData, "EnrolmentId").ToString());
                    rq.FKPersonId = Convert.ToInt32(RequestCertificate.GetItemFromJArray(personData, "PersonId").ToString());
                    rq.FKProductId = Convert.ToInt32(lueProductosPn.EditValue);
                    rq.FKPersonTypeId = TipoPersona;
                    rq.Profession = PROFESION;
                    rq.RegistrationNumber = NUMEROCOLEGIO;
                    rq.InstitutionIssues = INSTITUCIONEMITE;

                    insertRequest = RequestCertificate.InsertNewRequestPN(rq);
                }
               
                if (!insertRequest)
                {
                    Log.InsertarLog(Log.ErrorType.Error, "frmCertificados::SaveRequest", "Error al guardar el registro.", VariablesGlobales.PathDataLog);
                    return false;
                }
                else
                {
                    //**GUARDAR DOCUMENTOS ADJUNTOS**
                    int requestId = 0;
                    if (ListaDocumentosAdjuntos.Count > 0)
                    {
                        JArray requestByDocument = new JArray();
                        if (TipoPersona == (int)TypePerson.PRSJURIDICA)
                            requestByDocument = RequestCertificate.GetRequestByIdDocument(RequestCertificate.GetItemFromJArray(personDataRepresentantes, "DocumentNumber").ToString());
                        else
                            requestByDocument = RequestCertificate.GetRequestByIdDocument(RequestCertificate.GetItemFromJArray(personData, "DocumentNumber").ToString());

                        foreach (JObject element in requestByDocument)
                        {
                            if (element["RequestId"].ToString() != null) requestId = Convert.ToInt32(element["RequestId"].ToString());
                        }

                        pathRequest = Path.Combine(VariablesGlobales.PathDataBackup, "RQS", DateTime.Now.ToString("yyyy"), DateTime.Now.ToString("MM"), DateTime.Now.ToString("dd"), requestId.ToString());
                        pathAttached = Path.Combine(pathRequest, "ATT");
                        int contador = 0;

                        foreach (var doc in ListaDocumentosAdjuntos)
                        {
                            contador++;
                            EN.RequestAttach rqa = new EN.RequestAttach();
                            string nameFile = string.Format("{0}_{1}_{2}.{3}", requestId, DateTime.Now.ToString("ddhhmmss"), contador, doc.Type);
                            string unidad = pathAttached.Substring(0, 1) + @":\";

                            rqa.NameDocument = doc.NameDocument;
                            rqa.Folder = (string.Format(@"{0}\{1}", pathAttached, nameFile)).Replace(unidad, "");
                            rqa.Type = doc.Type;
                            rqa.Size = doc.Size;
                            rqa.CreationDate = doc.CreationDate;
                            rqa.TipoPersona = doc.TipoPersona;
                            rqa.FKRequestId = requestId;
                            rqa.FKDocumentType = doc.FKDocumentType;
                            rqa.FKCreateUserId = doc.FKCreateUserId;

                            string fileStr64 = Convert.ToBase64String(doc.Imagen);
                            if (!RequestCertificate.InsertNewRequestAttachImg(fileStr64, pathAttached, nameFile))
                            {
                                Log.InsertarLog(Log.ErrorType.Error, "frmCertificados::SaveRequest", "Error al guardar documentos adjuntos en los directorios.", VariablesGlobales.PathDataLog);
                                return false;
                            }
                            else
                            {
                                var insertRequestAttach = RequestCertificate.InsertNewRequestAttach(rqa);
                                if (!insertRequestAttach)
                                {
                                    Log.InsertarLog(Log.ErrorType.Error, "frmCertificados::SaveRequest", "Error al guardar documentos adjuntos en la tabla", VariablesGlobales.PathDataLog);
                                    return false;
                                }
                            }
                        }
                    }

                    //**GUARDAR REPRESENTANTES**
                    if (TipoPersona == (int)TypePerson.PRSJURIDICA)
                    {
                        if (ListaRepresentantes.Count == 0 | ListaRepresentantes == null) frmRpst.GetRepresentantes();
                        else
                        {
                            if (ListaRepresentantes.Count > 0)
                            {
                                foreach (var rpt in ListaRepresentantes)
                                {
                                    EN.RepresentativeRequest rpRq = new EN.RepresentativeRequest();
                                    rpRq.DocumentNumber = rpt.DocumentNumber;
                                    rpRq.Names = rpt.Names;
                                    rpRq.LastNames = rpt.LastNames;
                                    rpRq.Nationality = rpt.Nationality;
                                    rpRq.CreationDate = rpt.CreationDate;
                                    rpRq.CreationDate = rpt.CreationDate;
                                    rpRq.FKRequestId = requestId;
                                    rpRq.FKCreateUserId = rpt.FKCreateUserId;

                                    var insertRepresentative = RequestCertificate.InsertNewRepresentativeRequest(rpRq);
                                    if (!insertRepresentative)
                                    {
                                        Log.InsertarLog(Log.ErrorType.Error, "frmCertificados::SaveRequest", "Error al guardar el(los) representante(s)", VariablesGlobales.PathDataLog);
                                        return false;
                                    }
                                }
                            }
                        }
                    }

                    //**GUARDAR SEGUIMIENTO**
                    EN.RequestFollow rqf = new EN.RequestFollow();
                    rqf.Observation = "SOLICITUDCREADA";
                    rqf.FKRequestId = requestId;
                    rqf.FKTransactionEstatusId = (int)TransactionStatus.SOLICITUDCREADA;
                    rqf.FKCreateUserId = Convert.ToInt32(VariablesGlobales.USER_ID);
                    rqf.FKOfficeId = Convert.ToInt32(VariablesGlobales.OFFICE_ID);
                    rqf.FKTransactionStatusPrevious = (int)TransactionStatus.SOLICITUDCREADA;
                    rqf.FKWorkstationId = Convert.ToInt32(VariablesGlobales.WS_ID);
                    var insertRqFallow = RequestCertificate.InsertNewRequestFollow(rqf);
                    if (!insertRqFallow)
                    {
                        Log.InsertarLog(Log.ErrorType.Error, "frmCertificados::SaveRequest", "Error al guardar el seguimiento.", VariablesGlobales.PathDataLog);
                        return false;
                    }

                    //**INSERTAR BITÁCORA**
                    var InsertarBitacora = RequestCertificate.InsertarRegistro((int)RequestCertificate.TipoEvento.Insertar,
                            VariablesGlobales.USER_ID, (int)RequestCertificate.Modulos.Certificados, "Se registro nueva Solicitud con el ID: " + requestId);

                    if (!InsertarBitacora)
                    {
                        Log.InsertarLog(Log.ErrorType.Error, "frmCertificados::SaveRequest", "Error al guardar la bitácora.", VariablesGlobales.PathDataLog);
                        return false;
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Hubo un error al guardar la solicitud.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmCertificados::SaveRequest", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                return false;
            }
        }

        string VerificarCaracteresEspeciales(string textoOrigen)
        {
            if(textoOrigen.Length > 0)
            {
                textoOrigen.ToUpper();
                char[] car = textoOrigen.ToCharArray();
                foreach(char c in car)
                {
                    if (c.ToString() == "Â" | c.ToString() == "À" | c.ToString() == "Á" | c.ToString() == "Ä" | c.ToString() == "Ã")
                        textoOrigen = textoOrigen.Replace(c.ToString(), "A");
                    else if (c.ToString() == "Ê" | c.ToString() == "È" | c.ToString() == "É" | c.ToString() == "Ë")
                        textoOrigen = textoOrigen.Replace(c.ToString(), "E");
                    else if (c.ToString() == "Î" | c.ToString() == "Í" | c.ToString() == "Ì" | c.ToString() == "Ï")
                        textoOrigen = textoOrigen.Replace(c.ToString(), "I");
                    else if (c.ToString() == "Ô" | c.ToString() == "Õ" | c.ToString() == "Ò" | c.ToString() == "Ó" | c.ToString() == "Ö")
                        textoOrigen = textoOrigen.Replace(c.ToString(), "O");
                    else if (c.ToString() == "Û" | c.ToString() == "Ù" | c.ToString() == "Ú" | c.ToString() == "Ü")
                        textoOrigen = textoOrigen.Replace(c.ToString(), "U");
                    else if (c.ToString() == "<>" | c.ToString() == "\\" | c.ToString() == "|" | c.ToString() == "/")
                        textoOrigen = textoOrigen.Replace(c.ToString(), "");
                    else if (c.ToString() == "Ý" | c.ToString() == "Ÿ") textoOrigen = textoOrigen.Replace(c.ToString(), "Y");
                    else if (c.ToString() == "Ç") textoOrigen = textoOrigen.Replace(c.ToString(), "C");
                    else if (c.ToString() == "Ñ") textoOrigen = textoOrigen.Replace(c.ToString(), "N");
                }
            }
            return textoOrigen;
        }


        #endregion

        #region PROCEDIMIENTO CONSULTA AFIS
        private void btnValidarPersona_Click(object sender, EventArgs e)
        {
            if (cs500e.cs500eInicializado != 1)
            {
                XtraMessageBox.Show("El scanner cs500e no se detecto. revise que este conectado al equipo.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cs500e.ComprobarEscaner();
                return;
            }
            
            IniciarCaptura(personData);
        }

        public void IniciarCaptura(JArray personArray)
        {
            ValidacionHuellas = ValidacionAfis.NO_INICIADO;
            personArrayAfis = personArray;

            try
            {
                numeroConsultaAfis = "C" + SequenceLN.GetSequenceConsultaAfis();
                string sexo = string.Empty;
                string nombre = string.Empty;
                string apellido = string.Empty;
                string nacionalidad = string.Empty;
                string fecNacimiento = string.Empty;

                if (Convert.ToInt32(RequestCertificate.GetItemFromJArray(personArray, "FKSexId").ToString()) == (int)EN.Sex.SexoIndex.FEMENINO)
                    sexo = EN.Sex.SexoCode.F.ToString();
                else sexo = EN.Sex.SexoCode.M.ToString();
                nombre = RequestCertificate.GetItemFromJArray(personArray, "FirstName").ToString();
                apellido = RequestCertificate.GetItemFromJArray(personArray, "FirstLastName").ToString();
                nacionalidad = RequestCertificate.GetItemFromJArray(personArray, "Nacionality").ToString();
                fecNacimiento = string.Format("{0:yyyy-MM-dd}", RequestCertificate.GetItemFromJArray(personArray, "FechaNacimiento").ToString());

                string patWSQFiles = Path.Combine(VariablesGlobales.PathWSQFID + numeroConsultaAfis);
                cs500e.PrepararCapturaHuellas();
                if (cs500e.CapturarHuellasDerecha(numeroConsultaAfis) == 0)
                {
                    if(cs500e.SalvarHuellasCapturadas(numeroConsultaAfis, patWSQFiles, "") > 0)
                    {
                        var empNist = Nist.EmpaquetarNistFID(numeroConsultaAfis, patWSQFiles, nombre, apellido, "", sexo, nacionalidad, Convert.ToDateTime(fecNacimiento), "GVISION");
                        if (empNist)
                        {
                            if (ValidacionHuellas == ValidacionAfis.NO_INICIADO || ValidacionHuellas == ValidacionAfis.TIMEOUT)
                            {
                                ValidacionHuellas = ValidacionAfis.ENVIANDO_NIST;
                                IniciarProcesamientoNist1a1();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmCertificados::IniciarCaptura", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        private void IniciarProcesamientoNist1a1()
        {
            try
            {
                tmr1a1Certificados.Start();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        void tmr1a1Certificados_Tick(object sender, EventArgs e)
        {
            //***Verificar si el form de representantes esta abierto***
            Form frmRepresentates = Application.OpenForms.OfType<Form>().Where(fr => fr.Name == "frmComprobarIdentidadRepresentante").SingleOrDefault();

            lblGifValidando.Visible = true;
            lblEstadoValidacion.Visible = true;
            //!pnlVerificacionHuellas.Visible = true;
            SetLabelText(lblEstadoValidacion, "Packaging the NIST file...", "I");

            if (!EnviarNist())
            {
                lblEstadoValidacion.Font = new Font(lblEstadoValidacion.Font, FontStyle.Bold);
                tmr1a1Certificados.Stop();
                lblEstadoValidacion.Font = new Font(lblEstadoValidacion.Font, FontStyle.Bold);
                SetLabelText(lblEstadoValidacion, "Error sending NIST file", "A");
                lblGifValidando.Visible = false;
            }

            if (intentos == 12)
            {
                contador = 0;
                intentos = 0;
                tmr1a1Certificados.Stop();
                ValidacionHuellas = ValidacionAfis.TIMEOUT;
                if (frmRepresentates != null)
                {
                    frmComprobarIdentidadRepresentante.RESULTADOVALIDACION = (int)frmComprobarIdentidadRepresentante.RESULTADOVALIDACIONAFIS.VALIDACIONTIMEOUT;
                    frmComprobarIdentidadRepresentante.VALIDACIONAFISDESCRIPCION = "Time out, Unable to validate fingerprints";
                }
                btnSalir.Enabled = true;
                lblEstadoValidacion.Font = new Font(lblEstadoValidacion.Font, FontStyle.Bold);
                SetLabelText(lblEstadoValidacion, "Time out, Unable to validate fingerprints", "A");
                if(XtraMessageBox.Show("Se presento algun error con la verificación de la persona en el servidor. ¿Desea continuar con el proceso?", "Atención", 
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    btnAdjuntarPn.Enabled = true;
                }
                else btnAdjuntarPn.Enabled = false;
                lblGifValidando.Visible = false;
                return;
            }

            if (contador == 10)
            {
                AfisTransaccion at = ValidarHuellaAfis(numeroConsultaAfis);
                if (at.Status == "STATE_FINAL")
                {
                    intentos = 0;
                    contador = 0;
                    tmr1a1Certificados.Stop();
                    ValidacionHuellas = ValidacionAfis.COMPLETADO;
                    if (frmRepresentates != null) frmComprobarIdentidadRepresentante.ESTADOVALIDACION = true;

                    //**VERIFICAR SI HUBO HIT O NO**
                    if (at.ConfirmStatus == "Y")
                    {
                        try
                        {
                            if (at.CAN_TCN == RequestCertificate.GetItemFromJArray(personArrayAfis, "EnrolmentId").ToString() + "")
                            {
                                //***Validación exitosa***
                                SetLabelText(lblEstadoValidacion, "Identidad confirmada", "E");
                                lblGifValidando.Visible = false;
                                btnAdjuntarPn.Enabled = true;
                                if(frmRepresentates != null)
                                {
                                    frmComprobarIdentidadRepresentante.RESULTADOVALIDACION = (int)frmComprobarIdentidadRepresentante.RESULTADOVALIDACIONAFIS.VALIDACIONEXITOSA;
                                    frmComprobarIdentidadRepresentante.VALIDACIONAFISDESCRIPCION = "Identidad confirmada";
                                }
                            }
                            else if (at.CAN_TCN != RequestCertificate.GetItemFromJArray(personArrayAfis, "EnrolmentId").ToString() + "")
                            {
                                string fechaNacimientoAfis = at.CAN_DateOfBirth;
                                string fechaNacimientoSQL = DateTime.ParseExact(RequestCertificate.GetItemFromJArray(personArrayAfis, "FechaNacimiento").ToString(), "dd/MM/yyyy", CultureInfo.CurrentCulture).ToString("yyyyMMdd");
                                string sexo = string.Empty;
                                if (Convert.ToInt32(RequestCertificate.GetItemFromJArray(personArrayAfis, "FKSexId").ToString()) == 2) sexo = "M";
                                else sexo = "F";

                                if (at.CAN_FirstName == RequestCertificate.GetItemFromJArray(personArrayAfis, "FirstName").ToString()
                                    && at.CAN_LastName == RequestCertificate.GetItemFromJArray(personArrayAfis, "FirstLastName").ToString()
                                    && at.CAN_Sex == sexo
                                    && fechaNacimientoAfis == fechaNacimientoSQL)
                                {
                                    //***Validación exitosa***
                                    SetLabelText(lblEstadoValidacion, "Identidad confirmada", "E");
                                    lblGifValidando.Visible = false;
                                    btnAdjuntarPn.Enabled = true;
                                    if (frmRepresentates != null)
                                    {
                                        frmComprobarIdentidadRepresentante.RESULTADOVALIDACION = (int)frmComprobarIdentidadRepresentante.RESULTADOVALIDACIONAFIS.VALIDACIONEXITOSA;
                                        frmComprobarIdentidadRepresentante.VALIDACIONAFISDESCRIPCION = "Identidad confirmada";
                                    }
                                }
                                else
                                {
                                    //***Validación fallida***
                                    XtraMessageBox.Show("Las huellas enviadas NO coinciden con las huellas del solicitante.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    SetLabelText(lblEstadoValidacion, "No HIT, Identidad NO confirmada.", "A");
                                    lblGifValidando.Visible = false;
                                    btnAdjuntarPn.Enabled = false;
                                    if (frmRepresentates != null)
                                    {
                                        frmComprobarIdentidadRepresentante.RESULTADOVALIDACION = (int)frmComprobarIdentidadRepresentante.RESULTADOVALIDACIONAFIS.VALIDACIONHUELLASDIFERENTES;
                                        frmComprobarIdentidadRepresentante.VALIDACIONAFISDESCRIPCION = "No HIT, Identidad NO confirmada";
                                    }
                                }
                            }
                            else
                            {
                                //***Validación fallida***
                                SetLabelText(lblEstadoValidacion, "No HIT, Identidad NO confirmada", "A");
                                lblGifValidando.Visible = false;
                                btnAdjuntarPn.Enabled = false;
                                if (frmRepresentates != null)
                                {
                                    frmComprobarIdentidadRepresentante.RESULTADOVALIDACION = (int)frmComprobarIdentidadRepresentante.RESULTADOVALIDACIONAFIS.VALIDACIONFALLIDA;
                                    frmComprobarIdentidadRepresentante.VALIDACIONAFISDESCRIPCION = "No HIT, Identidad NO confirmada";
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                    else
                    {
                        //***Validación fallida***
                        SetLabelText(lblEstadoValidacion, "No HIT, Identidad NO confirmada", "A");
                        lblGifValidando.Visible = false;
                        if (frmRepresentates != null)
                        {
                            frmComprobarIdentidadRepresentante.RESULTADOVALIDACION = (int)frmComprobarIdentidadRepresentante.RESULTADOVALIDACIONAFIS.VALIDACIONFALLIDA;
                            frmComprobarIdentidadRepresentante.VALIDACIONAFISDESCRIPCION = "No HIT, Identidad NO confirmada";
                        }
                        return;
                    }
                }
                else
                {
                    contador = 0;
                    intentos++;
                    SetLabelText(lblEstadoValidacion, "Comparando huellas..." + intentos, "I");
                }
            }
            else
            {
                SetLabelText(lblEstadoValidacion, "Comparando huellas...", "I");
                contador++;
            }
        }

        bool EnviarNist()
        {
            //Envio Nist
            try
            {
                SetLabelText(lblEstadoValidacion, "Sending NIST file to AFIS...", "I");
                string pathFileNist = Path.Combine(VariablesGlobales.PathWSQFID, numeroConsultaAfis + "\\" + numeroConsultaAfis + ".tdf");

                if (!File.Exists(pathFileNist)) return false;

                byte[] barc = File.ReadAllBytes(pathFileNist);
                string base64 = Convert.ToBase64String(barc);
                int respuestaEnvioCafis = wsAfis.UploadNist(pathFileNist, base64);

                if (respuestaEnvioCafis == 0)
                {
                    ValidacionHuellas = ValidacionAfis.NIST_ENVIADO;
                    nistEnviado = true;
                    return true;
                }
                else
                {
                    ValidacionHuellas = ValidacionAfis.ERROR_ENVIO_NIST;
                    nistEnviado = false;
                    return false;
                }
            }
            catch (Exception ex)
            {
                ValidacionHuellas = ValidacionAfis.ERROR_ENVIO_NIST;
                nistEnviado = false;
                return false;
            }
        }

        void SetLabelText(LabelControl lbl, string texto, string tipo)
        {
            try
            {
                if (lbl.InvokeRequired)
                {
                    SetLabelCallBack continuar = new SetLabelCallBack(SetLabelText);
                    Invoke(continuar, new object[] { lbl, texto, tipo });
                }
                else
                {
                    lbl.Text = texto;
                    switch (tipo)
                    {
                        case "E":
                            {
                                lbl.ForeColor = Color.SeaGreen;
                                break;
                            }
                        case "A":
                            {
                                lbl.ForeColor = ColorTranslator.FromHtml("#E54B4B");
                                break;
                            }
                        case "I":
                            {
                                lbl.ForeColor = Color.DarkOrange;
                                break;
                            }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InsertarLog(Log.ErrorType.Error, "frmCertificados::SetLabelText", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        AfisTransaccion ValidarHuellaAfis(string noTransaccion)
        {
            string strTransNo = noTransaccion;
            string strTOT = string.Empty;
            string strExternalID = string.Empty;
            string strStatus = string.Empty;
            string strConfirm_Status = string.Empty;
            string strCAN_TransNo = string.Empty;
            string strCAN_ExternalId = string.Empty;
            string strCAN_TCN = string.Empty;
            string strCAN_LastName = string.Empty;
            string strCAN_FirstName = string.Empty;
            string strCAND_ID1 = string.Empty;
            string strCAND_SID = string.Empty;
            string strCAN_Sex = string.Empty;
            string strCAN_DateOfBirth = String.Empty;
            string strCAN_TOT = string.Empty;
            string strCAND_POB = string.Empty;


            afisRespuesta.TransNo = noTransaccion;

            int respuesta = wsAfis.GetTransStatus(strTransNo, out strTOT, out strExternalID, out strStatus, out strConfirm_Status, out strCAN_TransNo, out strCAN_ExternalId, out strCAN_TCN,
                out strCAN_LastName, out strCAN_FirstName, out strCAND_ID1, out strCAND_SID, out strCAN_Sex, out strCAN_DateOfBirth, out strCAN_TOT, out strCAND_POB);

            afisRespuesta.TransNo = strTransNo;
            afisRespuesta.TOT = strTOT;
            afisRespuesta.ExternalID = strExternalID;
            afisRespuesta.Status = strStatus;
            afisRespuesta.ConfirmStatus = strConfirm_Status;
            afisRespuesta.CAN_TransNo = strCAN_TransNo;
            afisRespuesta.CAN_ExternalId = strCAN_ExternalId;
            afisRespuesta.CAN_TCN = strCAN_TCN;
            afisRespuesta.CAN_LastName = strCAN_LastName;
            afisRespuesta.CAN_FirstName = strCAN_FirstName;
            afisRespuesta.CAND_ID1 = strCAND_ID1;
            afisRespuesta.CAN_SID = strCAND_SID;
            afisRespuesta.CAN_Sex = strCAN_Sex;
            afisRespuesta.CAN_DateOfBirth = strCAN_DateOfBirth;
            afisRespuesta.CAN_TOT = strCAN_TOT;
            afisRespuesta.CAND_POB = strCAND_POB;

            return afisRespuesta;
        }

        class AfisTransaccion
        {
            public string TransNo { get; set; }
            public string TOT { get; set; }
            public string ExternalID { get; set; }
            public string Status { get; set; }
            public string ConfirmStatus { get; set; }
            public string CAN_TransNo { get; set; }
            public string CAN_ExternalId { get; set; }
            public string CAN_TCN { get; set; }
            public string CAN_LastName { get; set; }
            public string CAN_FirstName { get; set; }
            public string CAND_ID1 { get; set; }
            public string CAN_SID { get; set; }
            public string CAN_Sex { get; set; }
            public string CAN_DateOfBirth { get; set; }
            public string CAN_TOT { get; set; }
            public string CAND_POB { get; set; }


        }


        #endregion

        private void lueTipoDocumentoPn_EditValueChanged(object sender, EventArgs e)
        {
            txtNumeroDocumentoPn.Text = null;
            if (Convert.ToInt32(lueTipoDocumentoPn.EditValue) == (int)DOCUMENTTYPELN.DocumentType.IDENTIDAD)
            {
                txtNumeroDocumentoPn.Properties.Mask.EditMask = "0000-0000-00000";
                txtNumeroDocumentoPn.Properties.Mask.MaskType = MaskType.Simple;
            }
            else if (Convert.ToInt32(lueTipoDocumentoPn.EditValue) == (int)DOCUMENTTYPELN.DocumentType.RTN)
            {
                txtNumeroDocumentoPn.Properties.Mask.EditMask = "0000-0000-000000";
                txtNumeroDocumentoPn.Properties.Mask.MaskType = MaskType.Simple;
            }
            else
            {
                txtNumeroDocumentoPn.Properties.Mask.MaskType = MaskType.None;
                txtNumeroDocumentoPn.Properties.CharacterCasing = CharacterCasing.Upper;
            }
            txtNumeroDocumentoPn.Focus();
        }

        private void lueTipoDocumentoPj_EditValueChanged(object sender, EventArgs e)
        {
            txtNumeroDocumentoPj.Text = null;
            if (Convert.ToInt32(lueTipoDocumentoPj.EditValue) == (int)DOCUMENTTYPELN.DocumentType.RTN)
            {
                txtNumeroDocumentoPj.Properties.Mask.EditMask = "0000-0000-000000";
                txtNumeroDocumentoPj.Properties.Mask.MaskType = MaskType.Simple;
            }
            txtNumeroDocumentoPj.Focus();
        }

        private void chkFacturaManual_CheckedChanged(object sender, EventArgs e)
        {
            if (chkFacturaManual.Checked)
            {
                pnlProfesionalTitulado.DateTime = DateTime.Now;
                dteFin.DateTime = DateTime.Now;
                dteFin.Properties.MaxValue = DateTime.Now;
                pnlProfesionalTitulado.Visible = true;
                dteFin.Visible = true;
                lblFechaInicio.Visible = true;
                lblFechaFin.Visible = true;
                btnConsultaFacturaManual.Visible = true;
            }
            else
            {
                lueFacturasCreadas.Properties.DataSource = null;
                lueFacturasCreadas.EditValue = null;
                lueFacturasCreadas.Visible = false;
                pnlProfesionalTitulado.Visible = false;
                dteFin.Visible = false;
                lblFechaInicio.Visible = false;
                lblFechaFin.Visible = false;
                btnConsultaFacturaManual.Visible = false;
            }
        }

        class FacturaManual
        {
            public int Id { get; set; }
            public string Valor { get; set; }
            public string Descripcion { get; set; }

            public FacturaManual(int fId, string fValor, string fDescripcion)
            {
                Id = fId;
                Valor = fValor;
                Descripcion = fDescripcion;
            }
        }

        private void btnConsultaFacturaManual_Click(object sender, EventArgs e)
        {
            //***Cargar datos***
            try
            {
                GP.DatosGP wsFunction = new GP.DatosGP();
                DataSet ds = new DataSet();

                ssForm.ShowWaitForm();
                ds = wsFunction.Consulta_Facturas(pnlProfesionalTitulado.DateTime, dteFin.DateTime);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    pnlProfesionalTitulado.Visible = true;
                    dteFin.Visible = true;
                    lueFacturasCreadas.Visible = true;
                    List<FacturaManual> lst = new List<FacturaManual>();
                    int Id = 0;
                    string oficina = string.Empty;
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        if (dr["NumDoc"].ToString() != string.Empty)
                        {
                            if (dr["DocID"].ToString().Trim() == "TS-FACT-01") oficina = "TEGUCIGALPA";
                            if (dr["DocID"].ToString().Trim() == "TS-FACT-02") oficina = "SAN PEDRO SULA";
                            lst.Add(new FacturaManual(Id++, dr["NumDoc"].ToString().Trim(), oficina));
                        }
                    }

                    lueFacturasCreadas.Properties.DataSource = null;
                    lueFacturasCreadas.Properties.DataSource = lst;
                    lueFacturasCreadas.Properties.ValueMember = "Id";
                    lueFacturasCreadas.Properties.DisplayMember = "Valor";
                    lueFacturasCreadas.Properties.Columns.Clear();
                    lueFacturasCreadas.Properties.BestFitMode = BestFitMode.BestFit;
                    lueFacturasCreadas.Properties.Columns.Add(new LookUpColumnInfo("Valor", 0, "FACTURA"));
                    lueFacturasCreadas.Properties.Columns.Add(new LookUpColumnInfo("Descripcion", 0, "OFICINA"));
                    
                    lueFacturasCreadas.ItemIndex = 0;
                    lueFacturasCreadas.Focus();
                }
                else
                {
                    lueFacturasCreadas.Properties.DataSource = null;
                    XtraMessageBox.Show("No existen facturas creadas en este rango de fechas.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                } 
                ssForm.CloseWaitForm();
            }
            catch (Exception ex)
            {
                if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                XtraMessageBox.Show("Hubo un error al tratar de visualizar las facturas.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmCertificados::btnConsultaFacturaManual_Click()", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        private void lueFacturasCreadas_CustomDrawCell(object sender, DevExpress.XtraEditors.Popup.LookUpCustomDrawCellArgs e)
        {
            if ("TEGUCIGALPA".Equals(e.DisplayText)) e.Appearance.ForeColor = Color.SteelBlue;
            if ("SAN PEDRO SULA".Equals(e.DisplayText)) e.Appearance.ForeColor = Color.Teal;
        }

        private void chkProfesionalTitulo_CheckedChanged(object sender, EventArgs e)
        {
            lueProductosPn.Properties.DataSource = null;
            lueProductosPn.EditValue = null;
            lueColegioProfesional.EditValue = null;
            if (chkProfesionalTitulo.Checked)
            {
                ExodusBcBase.Helper.Combobox.GetComboProductsByProductId(lueProductosPn, (int)TypePerson.PROFTITULADO);
                lueColegioProfesional.Visible = true;
            }
            else
            {
                ExodusBcBase.Helper.Combobox.GetComboProductsByProductId(lueProductosPn, (int)TypePerson.PRSNATURAL);
                lueColegioProfesional.Visible = false;
                btnAcreditarTitulo.Visible = false;
                PROFESION = string.Empty;
                NUMEROCOLEGIO = string.Empty;
            }
        }

        private void btnAcreditarTitulo_Click(object sender, EventArgs e)
        {
            frmProfesionalTitulado.PROFESION = RequestCertificate.GetItemFromJArray(personData, "Profesion").ToString();
            frmProfesionalTitulado frm = new frmProfesionalTitulado();
            frm.ShowDialog();
        }

        private void lueColegioProfesional_EditValueChanged(object sender, EventArgs e)
        {
            LookUpEdit editor = sender as LookUpEdit;
            object convenio = editor.GetColumnValue("Agreement");

            if(convenio != null)
            {
                if (!(bool)convenio)
                {
                    btnAcreditarTitulo.Visible = false;
                    chkProfesionalTitulo.Checked = false;
                    editor.EditValue = null;
                    XtraMessageBox.Show("Actualmente esta institución, no tiene niungún convenio activo.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else btnAcreditarTitulo.Visible = true;
            }
        }

        private void lueColegioProfesional_CustomDrawCell(object sender, DevExpress.XtraEditors.Popup.LookUpCustomDrawCellArgs e)
        {
            if ("True".Equals(e.DisplayText))
            {
                e.DisplayText = "CONVENIO ACTIVO";
                e.Appearance.ForeColor = Color.Teal;
            }
            if("False".Equals(e.DisplayText)) e.DisplayText = "SIN CONVENIO";
        }

        private void lueColegioProfesional_CustomDisplayText(object sender, CustomDisplayTextEventArgs e)
        {
            if (e.Value == null) e.DisplayText = (sender as LookUpEdit).Properties.NullValuePrompt;
        }

        private async void btnSyncCatalogos_Click(object sender, EventArgs e)
        {
            try
            {
                Task.Run(async () =>
                {
                    await VariablesGlobales.SincronizarCatalogos();
                    VariablesGlobales.Inicializar();
                });
                XtraMessageBox.Show("Sicronización finalizada.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Hubo un error al sicronizar los catálogos. Intente en otro momento.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmCertificados::btnSyncCatalogos_Click", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }
    }
}