﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Gv.Utilidades;
using static Gv.ExodusBc.UI.ExodusBcBase;
using static Gv.ExodusBc.EN.Request;
using Newtonsoft.Json.Linq;

namespace Gv.ExodusBc.UI.Modulos.Certificados
{
    public partial class frmClientes : XtraForm
    {


        #region VALORES INICIALES DEL FORMULARIO
   
        public frmClientes()
        {
            InitializeComponent();
        }

        private void frmClientes_Load(object sender, EventArgs e)
        {
            rdgTipoPerpona.Focus();
            rdgTipoPerpona.Select();
        }

        #endregion

        #region FUNCIONES DE LOS CONTROLES

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            if(ValidarEntradas())
            {
                try
                {
                    ssForm.ShowWaitForm();
                    JArray objCliente = new JArray();
                    objCliente = RequestCertificate.GetExistCustomer(txtNumeroDocumento.Text.Trim().Replace("-", ""));
                    if (objCliente.Count > 0)
                    {
                        if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                        XtraMessageBox.Show("Este cliente ya esta registrado con este número de documento.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }

                    EN.Customers clientes = new EN.Customers();
                    clientes.DocumentNumber = txtNumeroDocumento.Text.Trim().Replace("-", "");
                    clientes.Names = txtNombres.Text.Trim();
                    clientes.City = txtCiudad.Text.Trim();
                    clientes.Address = meDireccion.Text.Trim();
                    clientes.PersonalMovilPhone = txtTelefonoPersonal.Text.Trim();
                    clientes.WorkPhone = txtTelefonoEmpresa.Text.Trim();

                    if (rdgTipoPerpona.SelectedIndex == 0) clientes.LastNames = txtApellidos.Text.Trim();

                    clientes.CreationDate = Convert.ToDateTime(LoginWebService.getStatusConecction());
                    clientes.FKPersonType = (rdgTipoPerpona.SelectedIndex == 0) ? (int)TypePerson.PRSNATURAL : (int)TypePerson.PRSJURIDICA;
                    clientes.FKCreateUserId = VariablesGlobales.USER_ID;
                    if (RequestCertificate.InsertNewRCustomer(clientes))
                    {
                        CrearClienteGP();
                        if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                        XtraMessageBox.Show("Registro guardado satisfactoriamente.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        frmCertificados ActualizarClientes = (frmCertificados)Application.OpenForms["frmCertificados"];
                        ActualizarClientes.GetCustomers();
                        Close();
                    }
                }
                catch (Exception ex)
                {
                    if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                    XtraMessageBox.Show("Hubo un error al guardar el registro.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Log.InsertarLog(Log.ErrorType.Error, "frmClientes::btnAceptar_Click", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                }
            }
        }
  
        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void rgTipoPerpona_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtNumeroDocumento.Text = string.Empty;
            txtNumeroDocumento.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
            txtNumeroDocumento.Properties.MaxLength = 20;
            txtNumeroDocumento.Focus();
            if (rdgTipoPerpona.SelectedIndex == 0)
            {
                txtApellidos.Enabled = true;
                txtNumeroDocumento.Properties.Mask.EditMask = "0000-0000-00000";
            } 
            else
            {
                txtNumeroDocumento.Properties.Mask.EditMask = "0000-0000-000000";
                txtApellidos.Enabled = false;
                txtTelefonoPersonal.Enabled = false;
            }
        }

        #endregion

        #region MOTODOS Y PROCEDIMIENTOS
        bool ValidarEntradas()
        {
            if (rdgTipoPerpona.EditValue == null)
            {
                XtraMessageBox.Show("Seleccione el tipo de persona.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                rdgTipoPerpona.Focus();
                return false;
            }
            if (txtCiudad.Text == string.Empty)
            {
                XtraMessageBox.Show("Nombre de la ciudad esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtCiudad.Focus();
                return false;
            }
            if (txtNombres.Text == string.Empty)
            {
                XtraMessageBox.Show("Los nombres o la razón social estan vacios, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtNombres.Focus();
                return false;
            }
            if (rdgTipoPerpona.SelectedIndex == 0)
            {
                txtApellidos.Enabled = true;
                if (txtApellidos.Text == string.Empty)
                {
                    XtraMessageBox.Show("Seleccione el tipo de persona, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtApellidos.Focus();
                    return false;
                }
            }
            else txtApellidos.Enabled = false;

            if (txtNumeroDocumento.Text == string.Empty)
            {
                XtraMessageBox.Show("El número de Identificación esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtNumeroDocumento.Focus();
                return false;
            }
            if (meDireccion.Text == string.Empty)
            {
                XtraMessageBox.Show("La dirección esta vacia, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                meDireccion.Focus();
                return false;
            }
            if (txtTelefonoPersonal.Text == string.Empty & rdgTipoPerpona.SelectedIndex == 0)
            {
                XtraMessageBox.Show("El número de telefono personal esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtTelefonoPersonal.Focus();
                return false;
            }
            if (txtTelefonoEmpresa.Text == string.Empty & rdgTipoPerpona.SelectedIndex == 1) 
            {
                XtraMessageBox.Show("El número de telefono de la empresa esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtTelefonoPersonal.Focus();
                return false;
            }

            return true;
        }

        void CrearClienteGP()
        {
            try
            {
                GP.DatosGP wsFunction = new GP.DatosGP();
                GP.clsClientes wsClientes = new GP.clsClientes();

                wsClientes.Codigo = txtNumeroDocumento.Text.Trim().Replace("-", "");
                wsClientes.Nombres = txtNombres.Text.Trim();
                if (rdgTipoPerpona.SelectedIndex == 0)
                {
                    wsClientes.Apellidos = txtApellidos.Text.Trim();
                    wsClientes.Ciudad = txtCiudad.Text.Trim();
                    wsClientes.Contacto = "";
                    wsClientes.Direccion1 = meDireccion.Text.Trim();
                    wsClientes.Direccion2 = "";
                    wsClientes.Telefono1 = txtTelefonoPersonal.Text.Trim();
                    wsClientes.Telefono2 = txtTelefonoEmpresa.Text.Trim();
                    wsClientes.RTN = string.Empty;
                }
                else
                {
                    wsClientes.Apellidos = "";
                    wsClientes.Ciudad = "";
                    wsClientes.Contacto = "";
                    wsClientes.Direccion1 = "";
                    wsClientes.Direccion2 = "";
                    wsClientes.Telefono1 = "";
                    wsClientes.Telefono2 = txtTelefonoEmpresa.Text.Trim();
                    wsClientes.RTN = txtNumeroDocumento.Text.Trim();
                }
                var resultClientGp = wsFunction.CreacionCliente(wsClientes);
                if (resultClientGp.ToString() == "False")
                {
                    XtraMessageBox.Show("Hubo un error al tratar de crear el Cliente en GP.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Log.InsertarLog(Log.ErrorType.Error, "frmEnvioSolicitud::CrearClienteGP", resultClientGp.ToString(), VariablesGlobales.PathDataLog);
                    return;
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Hubo un error al tratar de crear el cliente en GP. Intente nuevamente en otro momento.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmClientes::CrearClienteGP", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        #endregion
    }
}