﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Newtonsoft.Json.Linq;
using static Gv.ExodusBc.UI.ExodusBcBase;
using System.IO;
using Gv.Utilidades;
using Gv.ExodusBc.UI.Modulos.InspeccionPrimaria;
using System.Globalization;
using Newtonsoft.Json;
using static Gv.ExodusBc.EN.Request;
using DevExpress.XtraLayout;

namespace Gv.ExodusBc.UI.Modulos.Certificados
{
    public partial class frmDetalleSolicitud : DevExpress.XtraEditors.XtraForm
    {
        public static int REQUESTID = 0;
        byte[] imgPn = null;
        byte[] imgSolicitante = null;
        byte[] imgPj = null;
        Image imgDocSolicitante = default(Image);
        Image imgDocPn = default(Image);
        Image imgScanPreview = default(Image);
        string IdentificacionRepresentante = string.Empty;
        string NombreRepresentante = string.Empty;
        int TransactionStatusPreviousId = 0;
        public static string observacionTransaccion = string.Empty;
        List<EN.RequestAttach> lstAdjuntos = new List<EN.RequestAttach>();
        string OficinaId = string.Empty;
        public static MemoEdit meObservacionAccion = new MemoEdit();
        public int TIPOPERSONA = 0;


        #region VALORES INICIALES DEL FORMULARIO

        public frmDetalleSolicitud()
        {
            InitializeComponent();
            Init();
        }

        void Init()
        {
            try
            {
                SetControles();
                ssForm.ShowWaitForm();
                JArray requestById = new JArray();
                requestById = RequestCertificate.GetRequestById(REQUESTID);
                if (requestById.Count > 0 | requestById != null)
                {
                    foreach (JObject element in requestById)
                    {
                        if (Convert.ToInt32(element["FKPersonTypeId"].ToString()) == (int)TypePerson.PRSJURIDICA)
                        {
                            TIPOPERSONA = (int)TypePerson.PRSJURIDICA;
                            lblProductoPj.Text = element["CODIGOPRODUCTO"].ToString();
                            meDescripcionProductoPj.Text = element["DESCRIPCIONPRODUCTO"].ToString();
                            lblNombrePj.Text = element["NOMBREPJ"].ToString();
                            lblNumeroDocumentoPj.Text = element["IDENTIFICACIONPJ"].ToString();
                            lblCorreoPj.Text = element["LegalPersonEmail"].ToString();
                            lblTelefonoPj.Text = element["LegalPersonPhone"].ToString();
                            lblFechaCreacionPj.Text = element["Fecha"].ToString();
                            lblUsuarioCreoPj.Text = element["Usuario"].ToString();
                            lblOficinacCreoPj.Text = element["Oficina"].ToString();
                            OficinaId = element["OfficeId"].ToString();
                            pnlPersonaJuridica.Visible = true;
                            pnlPersonaNatural.Visible = false;
                            GetAttached((int)TypePerson.PRSJURIDICA);
                            GetRepresentative(Convert.ToInt32(element["PersonId"].ToString()));
                            pnlPersonaJuridica.Location = new Point(20, 59);
                        }
                        else
                        {
                            TIPOPERSONA = (int)TypePerson.PRSNATURAL;
                            lblProductoPn.Text = element["CODIGOPRODUCTO"].ToString();
                            meDescripcionProductoPn.Text = element["DESCRIPCIONPRODUCTO"].ToString();
                            lblNombresPn.Text = element["NOMBRES"].ToString();
                            lblNumeroDocumentoPn.Text = element["IDENTIFICACIONPN"].ToString();
                            lblApellidos.Text = element["APELLIDOS"].ToString();
                            lblFechaNacimiento.Text = element["DateOfBirth"].ToString();
                            lblSexo.Text = element["Sexo"].ToString();
                            lblEstadoCivil.Text = element["EstadoCivil"].ToString();
                            lblNacionalidad.Text = element["Nacionalidad"].ToString();
                            lblDepartamento.Text = element["Departamento"].ToString();
                            lblMunicipio.Text = element["Municipio"].ToString();
                            meDireccion.Text = element["Direccion"].ToString();
                            lblCorreoPn.Text = element["Email"].ToString();
                            lblTelefonoPn.Text = element["CellPhonePersonal"].ToString();
                            lblFechaCreacionPn.Text = element["Fecha"].ToString();
                            lblUsuarioCreoPn.Text = element["Usuario"].ToString();
                            lblOficinacCreoPn.Text = element["Oficina"].ToString().ToUpper();
                            OficinaId = element["OfficeId"].ToString();
                            if (element["NUMEROCOLEGIO"].ToString() != "") lblNumeroColegio.Text = element["NUMEROCOLEGIO"].ToString();
                            else
                            {
                                lblTituloColegio.Visible = false;
                                lblNumeroColegio.Visible = false;
                            }
                            JArray arrayFoto = new JArray();
                            arrayFoto = RequestCertificate.GetLastPhoto(Convert.ToInt32(RequestCertificate.GetItemFromJArray(requestById, "PersonId").ToString()));
                            foreach (JObject elementPhoto in arrayFoto)
                                if (elementPhoto["Image"].ToString() != null)
                                {
                                    picSolicitante.EditValue = ExodusBcBase.Helper.byteToImage((byte[])elementPhoto["Image"]);
                                    imgSolicitante = (byte[])elementPhoto["Image"];
                                }

                            //Adjuntos
                            GetAttached((int)TypePerson.PRSNATURAL);

                            pnlPersonaJuridica.Visible = false;
                            pnlPersonaNatural.Visible = true;
                        }
                    }

                    TransactionStatusPreviousId = Convert.ToInt32(RequestCertificate.GetItemFromJArray(requestById, "FKTransactionStatusId").ToString());
                    ssForm.CloseWaitForm();
                }
            }
            catch (Exception ex)
            {
                if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                XtraMessageBox.Show("Hubo un error al tratar de cargar algunos valores iniciales.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmDetalleSolicitud::Init", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }

        }

        #endregion


        #region FUNCIONES DE LOS CONTROLES
        private void btnAprobarSolicitud_Click(object sender, EventArgs e)
        {
            try
            {
                if (XtraMessageBox.Show("Confirma que aprobará esta solicitud.", "Atención", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                {
                    if (RequestCertificate.UpdateRequestStatus(REQUESTID, (int)TransactionStatus.SOLICITUDAPROBADA,Convert.ToInt32(OficinaId)))
                    {
                        frmBandejaAprobacion ActualizarSolicitudes = (frmBandejaAprobacion)Application.OpenForms["frmBandejaAprobacion"];
                        ActualizarSolicitudes.CargarSolicitudesCreadas(Convert.ToInt32(OficinaId));
                        SaveRequestFollow((int)TransactionStatus.SOLICITUDAPROBADA);

                        //Consultar si la factura es manual
                        var consultaFacturaManual = RequestCertificate.GetNumeroFacturaManualByTicket(REQUESTID);
                        if (consultaFacturaManual.Count > 0 | consultaFacturaManual != null)
                        {
                            foreach (JObject element in consultaFacturaManual)
                            {
                                string numeroFacturaManual = RequestCertificate.GetItemFromJArray(consultaFacturaManual, "NumeroFacturaManual").ToString();
                                int requestId = Convert.ToInt32(RequestCertificate.GetItemFromJArray(consultaFacturaManual, "RequestId"));

                                if(numeroFacturaManual != string.Empty)
                                {
                                    if (RequestCertificate.UpdateRequestStatusFacturaPagada(requestId, (int)TransactionStatus.FACTURAPAGADA, numeroFacturaManual))
                                    {
                                        //**GUARDAR SEGUIMIENTO PARA FACTURA MANUAL**
                                        EN.RequestFollow rqf = new EN.RequestFollow();
                                        rqf.Observation = "FACTURA PAGADA";
                                        rqf.FKRequestId = requestId;
                                        rqf.FKTransactionEstatusId = (int)TransactionStatus.FACTURAPAGADA;
                                        rqf.FKCreateUserId = Convert.ToInt32(VariablesGlobales.USER_ID);
                                        rqf.FKOfficeId = Convert.ToInt32(VariablesGlobales.OFFICE_ID);
                                        rqf.FKTransactionStatusPrevious = (int)TransactionStatus.FACTURAPAGADA;
                                        rqf.FKWorkstationId = (int)VariablesGlobales.WS_ID;
                                        var insertRqFallow = RequestCertificate.InsertNewRequestFollow(rqf);
                                        if (!insertRqFallow)
                                        {
                                            Log.InsertarLog(Log.ErrorType.Error, "frmDetalleSolicitud::btnAprobarSolicitud_Click", "Error al guardar el seguimiento.", VariablesGlobales.PathDataLog);
                                            return;
                                        }
                                    }
                                    else
                                    {
                                        Log.InsertarLog(Log.ErrorType.Error, "frmDetalleSolicitud::btnAprobarSolicitud_Click", "Error al editar el estado de la factura.", VariablesGlobales.PathDataLog);
                                        return;
                                    }
                                }
                            }
                        }


                        //**Insertar Bitácora**
                        var InsertarBitacora = RequestCertificate.InsertarRegistro((int)RequestCertificate.TipoEvento.Insertar,
                                VariablesGlobales.USER_ID, (int)RequestCertificate.Modulos.Certificados, "Aprobo la solicitud con el ID: " + REQUESTID);

                        if (!InsertarBitacora)
                            Log.InsertarLog(Log.ErrorType.Error, "frmDetalleSolicitud::btnAprobarSolicitud_Click", "Error al guardar la bitácora.", VariablesGlobales.PathDataLog);

                        XtraMessageBox.Show("Solicitud aprobada satisfactoriamente.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Close();
                    }
                    else XtraMessageBox.Show("Hubo un error al aprobar la solicitud. Intente en otro momento.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else return;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Hubo un error al aprobar la solicitud. Intente en otro momento.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmDetalleSolicitud::btnAprobarSolicitud_Click", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        private void btnRechazar_Click(object sender, EventArgs e)
        {
            try
            {
                if (XtraMessageBox.Show("Confirma que rechazará la solicitud.", "Atención", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                {
                    frmAccionesRequest.ACCION = "RECHAZO";
                    frmAccionesRequest frm = new frmAccionesRequest("SOLICITUDES RECHAZADAS");
                    frm.ShowDialog();

                    if (observacionTransaccion == string.Empty)
                    {
                        XtraMessageBox.Show("Debe escribir una justificación para rechazar la solicitud.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    else
                    {
                        if (RequestCertificate.UpdateRequestStatus(REQUESTID, (int)TransactionStatus.SOLICITUDRECHAZADA, Convert.ToInt32(OficinaId)))
                        {
                            frmBandejaAprobacion ActualizarSolicitudes = (frmBandejaAprobacion)Application.OpenForms["frmBandejaAprobacion"];
                            ActualizarSolicitudes.CargarSolicitudesCreadas(Convert.ToInt32(OficinaId));
                            SaveRequestFollow((int)TransactionStatus.SOLICITUDRECHAZADA);

                            //**Insertar Bitácora**
                            var InsertarBitacora = RequestCertificate.InsertarRegistro((int)RequestCertificate.TipoEvento.Insertar,
                                    VariablesGlobales.USER_ID, (int)RequestCertificate.Modulos.Certificados, "Rechazó la solicitud con el ID: " + REQUESTID);

                            if (!InsertarBitacora)
                                Log.InsertarLog(Log.ErrorType.Error, "frmDetalleSolicitud::btnRechazar_Click", "Error al guardar la bitácora", VariablesGlobales.PathDataLog);

                            XtraMessageBox.Show("Solicitud rechazada satisfactoriamente.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            Close();
                        }
                        else XtraMessageBox.Show("Hubo un error al rechazar la solicitud. Intente en otro momento.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else return;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Hubo un error al rechazar la solicitud. Intente en otro momento.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmDetalleSolicitud::btnRechazar_Click", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        private void btnEnviarRevision_Click(object sender, EventArgs e)
        {
            try
            {
                if (XtraMessageBox.Show("Confirma que enviará a revisión esta solicitud.", "Atención", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                {
                    frmAccionesRequest.ACCION = "REVISION";
                    frmAccionesRequest frm = new frmAccionesRequest("SOLICITUDES ENVIADAS A REVISIÓN");
                    frm.ShowDialog();

                    if (observacionTransaccion == string.Empty)
                    {
                        XtraMessageBox.Show("Debe escribir una justificación para enviar a revisión la solicitud.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    else
                    {
                        if (RequestCertificate.UpdateRequestStatus(REQUESTID, (int)TransactionStatus.SOLICITUDENREVISION, Convert.ToInt32(OficinaId)))
                        {
                            frmBandejaAprobacion ActualizarSolicitudes = (frmBandejaAprobacion)Application.OpenForms["frmBandejaAprobacion"];
                            ActualizarSolicitudes.CargarSolicitudesCreadas(Convert.ToInt32(OficinaId));
                            SaveRequestFollow((int)TransactionStatus.SOLICITUDENREVISION);

                            //**Insertar Bitácora**
                            var InsertarBitacora = RequestCertificate.InsertarRegistro((int)RequestCertificate.TipoEvento.Insertar,
                                    VariablesGlobales.USER_ID, (int)RequestCertificate.Modulos.Certificados, "Envio a revisión la solicitud con el ID: " + REQUESTID);

                            if (!InsertarBitacora)
                                Log.InsertarLog(Log.ErrorType.Error, "frmDetalleSolicitud::btnEnviarRevision_Click", "Error al guardar la bitácora", VariablesGlobales.PathDataLog);
                            XtraMessageBox.Show("Solicitud enviada a revisión satisfactoriamente.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            Close();
                        }
                        else XtraMessageBox.Show("Hubo un error al enviar a revisión la solicitud. Intente en otro momento.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else return;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Hubo un error al enviar a revisión la solicitud. Intente en otro momento.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmDetalleSolicitud::btnEnviarRevision_Click", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        private void Inbox_Showing(object sender, XtraMessageShowingArgs e)
        {
            e.Form.Icon = Icon;
            e.Buttons[DialogResult.Cancel].Visible = false;
        }
 
        private void picSolicitante_Click(object sender, EventArgs e)
        {
            byte[] Archivo = new byte[] { };
            Archivo = imgSolicitante;

            MemoryStream TempImg = new MemoryStream();
            TempImg.Write(Archivo, 0, Archivo.Length);

            imgDocSolicitante = Image.FromStream(TempImg);
            picSolicitante.Image = Image.FromStream(TempImg);

            frmEnrolamientoVistaPrevia frm = new frmEnrolamientoVistaPrevia();
            frm.ptbPrevia.Image = picSolicitante.Image;
            frm.imgDocPreview = imgDocSolicitante;
            frm.ShowDialog();
        }

        private void btnVisualizar1_Click(object sender, EventArgs e)
        {
            try
            {
                if (gvAdjuntosPn.RowCount != -1)
                {
                    byte[] Archivo = new byte[] { };
                    Archivo = lstAdjuntos.Find(x => x.Folder == Convert.ToString(gvAdjuntosPn.GetFocusedRowCellValue("Folder"))).Imagen;

                    frmVisorDocumentosAdjuntos.Archivo = Archivo;
                    frmVisorDocumentosAdjuntos frm = new frmVisorDocumentosAdjuntos();
                    frm.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Hubo un error al tratar de visualizar el documento.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmDetalleSolicitud::btnVisualizar1_Click", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }

        }

        private void btnVisualizarDocPj_Click(object sender, EventArgs e)
        {
            try
            {
                if (gvAdjuntosPj.RowCount != -1)
                {
                    byte[] Archivo = new byte[] { };
                    Archivo = lstAdjuntos.Find(x => x.Folder == Convert.ToString(gvAdjuntosPj.GetFocusedRowCellValue("Folder"))).Imagen;

                    frmVisorDocumentosAdjuntos.Archivo = Archivo;
                    frmVisorDocumentosAdjuntos frm = new frmVisorDocumentosAdjuntos();
                    frm.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Hubo un error al tratar de visualizar el documento.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmDetalleSolicitud::btnVisualizarDocPj_Click", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }

        }

        private void btnVisualizarRepresentante_Click(object sender, EventArgs e)
        {
            frmInformacionRepresentantes.FOTO = (byte[])gvRepresentantes.GetRowCellValue(gvRepresentantes.FocusedRowHandle, "Foto");
            frmInformacionRepresentantes.IDENTIFICACION = gvRepresentantes.GetRowCellValue(gvRepresentantes.FocusedRowHandle, "DocumentNumber").ToString();
            frmInformacionRepresentantes.NOMBRES = gvRepresentantes.GetRowCellValue(gvRepresentantes.FocusedRowHandle, "Names").ToString();
            frmInformacionRepresentantes.NACIONALIDAD = gvRepresentantes.GetRowCellValue(gvRepresentantes.FocusedRowHandle, "Nationality").ToString();
            frmInformacionRepresentantes frm = new frmInformacionRepresentantes();
            frm.ShowDialog();
        }

        #endregion


        #region METODOS Y PROCEDIMIENTOS

        bool ValidarRepresentantes()
        {
            if (gvRepresentantes.RowCount == 0) return false;
            return true;
        }

        void SetControles()
        {
            lblProductoPn.Text = string.Empty;
            meDescripcionProductoPn.Text = string.Empty;
            lblNombresPn.Text = string.Empty;
            lblCorreoPn.Text = string.Empty;
            lblTelefonoPn.Text = string.Empty;

            lblProductoPj.Text = string.Empty;
            meDescripcionProductoPj.Text = string.Empty;
            lblNombrePj.Text = string.Empty;
            lblNumeroDocumentoPj.Text = string.Empty;
        }

        void GetAttached(int tPerson)
        {
            var list = new List<EN.RequestAttach>();
            JArray arrayImgAdjunto = new JArray();
            arrayImgAdjunto = RequestCertificate.GetAdjuntoByRequestId(REQUESTID);
            if (arrayImgAdjunto.Count > 0)
            {
                foreach (JObject elementImg in arrayImgAdjunto)
                {
                    imgPn = null;
                    string result = null;
                    string strRelativePath = null;
                    string file = null;
                    if (elementImg["Folder"].ToString() != null)
                    {
                        string path = @"C:\";
                        strRelativePath = path + elementImg["Folder"].ToString();
                        file = Path.GetDirectoryName(strRelativePath);
                        result = RequestCertificate.GetRequestAttachImg(strRelativePath, file);
                        if (result != null) imgPn = Convert.FromBase64String(result);
                    }

                    EN.RequestAttach rqAttach = new EN.RequestAttach
                    {
                        NameDocument = elementImg["NombreDocumento"].ToString(),
                        Folder = elementImg["Folder"].ToString(),
                        Imagen = imgPn
                    };
                    if (imgPn != null) rqAttach.Imagen = imgPn;
                    lstAdjuntos.Add(rqAttach);
                }
                if (tPerson == (int)TypePerson.PRSNATURAL)
                {
                    gvAdjuntosPn.Columns[0].Visible = false;
                    gcAdjuntosPn.DataSource = lstAdjuntos;
                }
                else
                {
                    gvAdjuntosPj.Columns[0].Visible = false;
                    gcAdjuntosPj.DataSource = lstAdjuntos;
                }
            }
        }

        void GetRepresentative(int prsId)
        {
            JArray personRepresentante = new JArray();
            personRepresentante = RequestCertificate.GetARepresentativeRequestByRequestId(REQUESTID);
            if (personRepresentante.Count > 0)
            {
                var list = new List<EN.AddRepresentatives>();
                foreach (JObject element in personRepresentante)
                {
                    EN.AddRepresentatives rps = new EN.AddRepresentatives
                    {
                        DocumentNumber = element["DocumentNumber"].ToString(),
                        Names = element["Nombres"].ToString(),
                        Nationality = element["Nacionality"].ToString(),
                    };

                    //Foto
                    JArray arrayFoto = new JArray();
                    arrayFoto = RequestCertificate.GetLastPhoto(Convert.ToInt32(element["FKPersonId"].ToString()));
                    foreach (JObject elementPhoto in arrayFoto)
                        if (elementPhoto["Image"].ToString() != null) rps.Foto = (byte[])elementPhoto["Image"];

                    list.Add(rps);
                }
                gcRepresentantes.DataSource = list;
                gcRepresentantes.RefreshDataSource();
            }
        }

        bool InputBoxEstatus(string accion)
        {
            meObservacionAccion.Text = string.Empty;
            XtraInputBoxArgs inBox = new XtraInputBoxArgs();
            inBox.Caption = "Digite una justificación del: " + accion + " de la solicitud.";
            inBox.Prompt = "Justificación";
            inBox.DefaultButtonIndex = 0;
            inBox.Showing += Inbox_Showing;
            inBox.Editor = meObservacionAccion;
            meObservacionAccion.Focus();
            meObservacionAccion.Font = new Font("Segoe UI", 10, FontStyle.Bold);
            meObservacionAccion.Properties.CharacterCasing = CharacterCasing.Upper;
            meObservacionAccion.ForeColor = Color.FromArgb(55, 71, 79);
            var result = XtraInputBox.Show(inBox);
            if (result == null)
            {
                XtraMessageBox.Show("Debe digitar la justificación para continuar.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            else observacionTransaccion = meObservacionAccion.Text;

            return true;
        }

        bool InputBox(string titulo, string accion)
        {
            meObservacionAccion.Text = string.Empty;
            Form frm = new Form();
            frm.Width = 420;
            frm.Height = 226;
            frm.FormBorderStyle = FormBorderStyle.FixedDialog;
            frm.StartPosition = FormStartPosition.CenterScreen;
            frm.MinimizeBox = false;
            frm.MaximizeBox = false;
            frm.Text = "SOLICITUDES RECHAZADAS";


            LookUpEdit lueMotivos = new LookUpEdit();
            lueMotivos.SetBounds(11, 8, 372, 10);
            frm.Controls.Add(lueMotivos);

            Label lblObserva = new Label();
            lblObserva.Text = "Observacion";
            lblObserva.SetBounds(9, 40, 372, 13);
            frm.Controls.Add(lblObserva);
            meObservacionAccion.SetBounds(12, 56, 372, 20);
            meObservacionAccion.Height = 80;
            frm.Controls.Add(meObservacionAccion);

            SeparatorControl separatorControl = new SeparatorControl();
            SimpleButton btnOk = new SimpleButton(); 
            btnOk.Text = "OK";
            btnOk.Name = "btnOk";
            btnOk.DialogResult = DialogResult.OK;
            btnOk.SetBounds(130, 150, 150, 30);
            btnOk.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            btnOk.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            btnOk.Font = new Font("Segoe UI", 10);
            btnOk.ForeColor = Color.FromArgb(55, 71, 79);
            btnOk.BackColor = Color.AliceBlue;

            frm.Controls.Add(btnOk);
            frm.ShowDialog();

    
            if (meObservacionAccion.Text == string.Empty)
            {
                XtraMessageBox.Show("Debe digitar la justificación para continuar.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            else observacionTransaccion = meObservacionAccion.Text.Trim();

            return true;
        }

        public void AccionControl()
        {

            LayoutControl lc = new LayoutControl();
            meObservacionAccion.Text = string.Empty;
            lc.Dock = DockStyle.Fill;
            TextEdit teLogin = new TextEdit();
            Label label = new Label();
            TextEdit tePassword = new TextEdit();
            SeparatorControl separatorControl = new SeparatorControl();
            lc.AddItem(string.Empty, teLogin).TextVisible = false;
            lc.AddItem(string.Empty, tePassword).TextVisible = false;
            Controls.Add(lc);
            Height = 100;
            Dock = DockStyle.Top;


        }

        void SaveRequestFollow(int trnStatusCurrentId)
        {
            EN.RequestFollow rqf = new EN.RequestFollow();
            rqf.Observation = observacionTransaccion;
            rqf.FKRequestId = REQUESTID;
            rqf.FKTransactionEstatusId = trnStatusCurrentId;
            rqf.FKCreateUserId = Convert.ToInt32(VariablesGlobales.USER_ID);
            rqf.FKOfficeId = Convert.ToInt32(VariablesGlobales.OFFICE_ID);
            rqf.FKTransactionStatusPrevious = TransactionStatusPreviousId;
            rqf.FKWorkstationId = (int)VariablesGlobales.WS_ID;
            var insertRqFallow = RequestCertificate.InsertNewRequestFollow(rqf);
            if (!insertRqFallow)
            {
                Log.InsertarLog(Log.ErrorType.Error, "frmDetalleSolicitud::SaveRequestFollow", "Error al guardar el seguimiento.", VariablesGlobales.PathDataLog);
                return;
            }
        }



        #endregion
    }

}
