﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace Gv.ExodusBc.UI.Modulos.Certificados
{
    public partial class frmInformacionRepresentantes : DevExpress.XtraEditors.XtraForm
    {
        public static byte[] FOTO = null;
        public static string IDENTIFICACION = string.Empty;
        public static string NOMBRES = string.Empty;
        public static string NACIONALIDAD = string.Empty;


        public frmInformacionRepresentantes()
        {
            InitializeComponent();
            Init();
        }

        void Init()
        {
            picRepresentante.EditValue = FOTO;
            lblIdentificacion.Text = IDENTIFICACION;
            lblNombres.Text = NOMBRES;
            lblNacionalidad.Text = NACIONALIDAD;
        }
    }
}