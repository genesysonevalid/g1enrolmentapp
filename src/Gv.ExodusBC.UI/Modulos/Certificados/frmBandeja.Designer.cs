﻿namespace Gv.ExodusBc.UI.Modulos.Certificados
{
    partial class frmBandeja
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnSalir = new DevExpress.XtraEditors.SimpleButton();
            this.lblNombreFormulario = new DevExpress.XtraEditors.LabelControl();
            this.lblTituloAccion = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.gcRequest = new DevExpress.XtraGrid.GridControl();
            this.gvRequest = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVerRechazo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnVerRechazo = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colVerRevision = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnVerRevision = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colVerTicket = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnVerTicket = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colVerSolicitud = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnVerSolicitud = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.btnRefrescar = new DevExpress.XtraEditors.SimpleButton();
            this.ssForm = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::Gv.ExodusBc.UI.frmWaitForm), true, true);
            this.btnActualizarEstadosValid = new DevExpress.XtraEditors.SimpleButton();
            this.dteFechaFinal = new DevExpress.XtraEditors.DateEdit();
            this.dteFechaInicial = new DevExpress.XtraEditors.DateEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.lblAtencion = new DevExpress.XtraEditors.LabelControl();
            this.picAtencion = new DevExpress.XtraEditors.PictureEdit();
            this.btnBuscarSolicitudes = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcRequest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvRequest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnVerRechazo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnVerRevision)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnVerTicket)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnVerSolicitud)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteFechaFinal.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteFechaFinal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteFechaInicial.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteFechaInicial.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAtencion.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.panelControl1.Appearance.Options.UseBackColor = true;
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.btnSalir);
            this.panelControl1.Controls.Add(this.lblNombreFormulario);
            this.panelControl1.Location = new System.Drawing.Point(-6, 0);
            this.panelControl1.LookAndFeel.SkinName = "Blue";
            this.panelControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1361, 71);
            this.panelControl1.TabIndex = 118;
            // 
            // btnSalir
            // 
            this.btnSalir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSalir.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.btnSalir.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalir.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.btnSalir.Appearance.Options.UseBackColor = true;
            this.btnSalir.Appearance.Options.UseFont = true;
            this.btnSalir.Appearance.Options.UseForeColor = true;
            this.btnSalir.Appearance.Options.UseTextOptions = true;
            this.btnSalir.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnSalir.AppearanceHovered.BackColor = System.Drawing.Color.Crimson;
            this.btnSalir.AppearanceHovered.Options.UseBackColor = true;
            this.btnSalir.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btnSalir.Location = new System.Drawing.Point(1306, 2);
            this.btnSalir.Margin = new System.Windows.Forms.Padding(2);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(50, 27);
            this.btnSalir.TabIndex = 72;
            this.btnSalir.Text = "X";
            this.btnSalir.ToolTip = "Salir";
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // lblNombreFormulario
            // 
            this.lblNombreFormulario.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblNombreFormulario.Appearance.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombreFormulario.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lblNombreFormulario.Appearance.Options.UseFont = true;
            this.lblNombreFormulario.Appearance.Options.UseForeColor = true;
            this.lblNombreFormulario.Appearance.Options.UseTextOptions = true;
            this.lblNombreFormulario.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblNombreFormulario.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblNombreFormulario.Location = new System.Drawing.Point(9, 12);
            this.lblNombreFormulario.Margin = new System.Windows.Forms.Padding(2);
            this.lblNombreFormulario.Name = "lblNombreFormulario";
            this.lblNombreFormulario.Size = new System.Drawing.Size(1350, 44);
            this.lblNombreFormulario.TabIndex = 72;
            this.lblNombreFormulario.Text = "BANDEJA DE CERTIFICADOS";
            // 
            // lblTituloAccion
            // 
            this.lblTituloAccion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTituloAccion.Appearance.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblTituloAccion.Appearance.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTituloAccion.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lblTituloAccion.Appearance.Options.UseBackColor = true;
            this.lblTituloAccion.Appearance.Options.UseFont = true;
            this.lblTituloAccion.Appearance.Options.UseForeColor = true;
            this.lblTituloAccion.Appearance.Options.UseTextOptions = true;
            this.lblTituloAccion.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblTituloAccion.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblTituloAccion.LineColor = System.Drawing.Color.Gainsboro;
            this.lblTituloAccion.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblTituloAccion.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblTituloAccion.LineVisible = true;
            this.lblTituloAccion.Location = new System.Drawing.Point(3, 74);
            this.lblTituloAccion.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblTituloAccion.Name = "lblTituloAccion";
            this.lblTituloAccion.Size = new System.Drawing.Size(1349, 34);
            this.lblTituloAccion.TabIndex = 418;
            this.lblTituloAccion.Text = "Detalle y seguimiento de solicitudes";
            // 
            // labelControl9
            // 
            this.labelControl9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl9.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl9.Appearance.Options.UseFont = true;
            this.labelControl9.Appearance.Options.UseForeColor = true;
            this.labelControl9.Appearance.Options.UseTextOptions = true;
            this.labelControl9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl9.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl9.LineColor = System.Drawing.Color.Gainsboro;
            this.labelControl9.LineLocation = DevExpress.XtraEditors.LineLocation.Top;
            this.labelControl9.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.labelControl9.LineVisible = true;
            this.labelControl9.Location = new System.Drawing.Point(-1, 66);
            this.labelControl9.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(1359, 42);
            this.labelControl9.TabIndex = 419;
            // 
            // gcRequest
            // 
            this.gcRequest.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcRequest.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gcRequest.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gcRequest.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gcRequest.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gcRequest.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gcRequest.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(2);
            this.gcRequest.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gcRequest.Location = new System.Drawing.Point(5, 209);
            this.gcRequest.LookAndFeel.SkinName = "Office 2016 Colorful";
            this.gcRequest.LookAndFeel.UseDefaultLookAndFeel = false;
            this.gcRequest.MainView = this.gvRequest;
            this.gcRequest.Margin = new System.Windows.Forms.Padding(2);
            this.gcRequest.Name = "gcRequest";
            this.gcRequest.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.btnVerTicket,
            this.btnVerSolicitud,
            this.btnVerRechazo,
            this.btnVerRevision});
            this.gcRequest.Size = new System.Drawing.Size(1338, 545);
            this.gcRequest.TabIndex = 420;
            this.gcRequest.UseEmbeddedNavigator = true;
            this.gcRequest.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvRequest});
            // 
            // gvRequest
            // 
            this.gvRequest.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvRequest.Appearance.HeaderPanel.Options.UseFont = true;
            this.gvRequest.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.gvRequest.Appearance.OddRow.Options.UseBackColor = true;
            this.gvRequest.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gvRequest.Appearance.Row.Font = new System.Drawing.Font("Segoe UI Semibold", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvRequest.Appearance.Row.Options.UseBackColor = true;
            this.gvRequest.Appearance.Row.Options.UseFont = true;
            this.gvRequest.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.colVerRechazo,
            this.colVerRevision,
            this.colVerTicket,
            this.colVerSolicitud});
            this.gvRequest.GridControl = this.gcRequest;
            this.gvRequest.Name = "gvRequest";
            this.gvRequest.OptionsClipboard.AllowCopy = DevExpress.Utils.DefaultBoolean.True;
            this.gvRequest.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gvRequest.OptionsView.EnableAppearanceOddRow = true;
            this.gvRequest.OptionsView.ShowAutoFilterRow = true;
            this.gvRequest.OptionsView.ShowGroupPanel = false;
            this.gvRequest.RowHeight = 32;
            this.gvRequest.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gvRequest_RowCellStyle);
            this.gvRequest.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gvRequest_PopupMenuShowing);
            this.gvRequest.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gvRequest_CustomDrawEmptyForeground);
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceCell.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn1.AppearanceCell.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.gridColumn1.AppearanceCell.Options.UseFont = true;
            this.gridColumn1.AppearanceCell.Options.UseForeColor = true;
            this.gridColumn1.AppearanceHeader.Font = new System.Drawing.Font("Segoe UI Semibold", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn1.AppearanceHeader.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.gridColumn1.AppearanceHeader.Options.UseFont = true;
            this.gridColumn1.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.Caption = "N° TICKET";
            this.gridColumn1.FieldName = "TICKET";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 108;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceCell.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn2.AppearanceCell.Options.UseFont = true;
            this.gridColumn2.AppearanceHeader.Font = new System.Drawing.Font("Segoe UI Semibold", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn2.AppearanceHeader.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.gridColumn2.AppearanceHeader.Options.UseFont = true;
            this.gridColumn2.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.Caption = "PRODUCTO";
            this.gridColumn2.FieldName = "PRODUCTO";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 167;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceCell.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn3.AppearanceCell.Options.UseFont = true;
            this.gridColumn3.AppearanceHeader.Font = new System.Drawing.Font("Segoe UI Semibold", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn3.AppearanceHeader.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.gridColumn3.AppearanceHeader.Options.UseFont = true;
            this.gridColumn3.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.Caption = "NOMBRES";
            this.gridColumn3.FieldName = "NOMBRES";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            this.gridColumn3.Width = 167;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceCell.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn4.AppearanceCell.Options.UseFont = true;
            this.gridColumn4.AppearanceHeader.Font = new System.Drawing.Font("Segoe UI Semibold", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn4.AppearanceHeader.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.gridColumn4.AppearanceHeader.Options.UseFont = true;
            this.gridColumn4.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn4.Caption = "APELLIDOS";
            this.gridColumn4.FieldName = "APELLIDOS";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsFilter.AllowFilter = false;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            this.gridColumn4.Width = 234;
            // 
            // gridColumn5
            // 
            this.gridColumn5.AppearanceCell.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn5.AppearanceCell.Options.UseFont = true;
            this.gridColumn5.AppearanceHeader.Font = new System.Drawing.Font("Segoe UI Semibold", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn5.AppearanceHeader.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.gridColumn5.AppearanceHeader.Options.UseFont = true;
            this.gridColumn5.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn5.Caption = "SEGUIMIENTO";
            this.gridColumn5.FieldName = "SEGUIMIENTO";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 4;
            this.gridColumn5.Width = 199;
            // 
            // gridColumn6
            // 
            this.gridColumn6.AppearanceCell.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn6.AppearanceCell.Options.UseFont = true;
            this.gridColumn6.AppearanceHeader.Font = new System.Drawing.Font("Segoe UI Semibold", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn6.AppearanceHeader.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.gridColumn6.AppearanceHeader.Options.UseFont = true;
            this.gridColumn6.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn6.Caption = "FACTURA";
            this.gridColumn6.FieldName = "FACTURA";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsEditForm.Visible = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 5;
            this.gridColumn6.Width = 125;
            // 
            // gridColumn7
            // 
            this.gridColumn7.AppearanceCell.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn7.AppearanceCell.Options.UseFont = true;
            this.gridColumn7.AppearanceHeader.Font = new System.Drawing.Font("Segoe UI Semibold", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn7.AppearanceHeader.Options.UseFont = true;
            this.gridColumn7.Caption = "IDSTATUS";
            this.gridColumn7.FieldName = "RequestStatusId";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 6;
            this.gridColumn7.Width = 59;
            // 
            // gridColumn8
            // 
            this.gridColumn8.AppearanceCell.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn8.AppearanceCell.Options.UseFont = true;
            this.gridColumn8.AppearanceHeader.Font = new System.Drawing.Font("Segoe UI Semibold", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn8.AppearanceHeader.Options.UseFont = true;
            this.gridColumn8.Caption = "REQUESTID";
            this.gridColumn8.FieldName = "RequestId";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 7;
            this.gridColumn8.Width = 62;
            // 
            // gridColumn9
            // 
            this.gridColumn9.AppearanceCell.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn9.AppearanceCell.Options.UseFont = true;
            this.gridColumn9.AppearanceHeader.Font = new System.Drawing.Font("Segoe UI Semibold", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn9.AppearanceHeader.Options.UseFont = true;
            this.gridColumn9.Caption = "TRANSACCIONID";
            this.gridColumn9.FieldName = "FKTransactionStatusId";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 8;
            this.gridColumn9.Width = 62;
            // 
            // colVerRechazo
            // 
            this.colVerRechazo.ColumnEdit = this.btnVerRechazo;
            this.colVerRechazo.FieldName = "colVerRechazo";
            this.colVerRechazo.Name = "colVerRechazo";
            this.colVerRechazo.OptionsColumn.ShowCaption = false;
            this.colVerRechazo.OptionsColumn.ShowInExpressionEditor = false;
            this.colVerRechazo.OptionsFilter.AllowAutoFilter = false;
            this.colVerRechazo.OptionsFilter.AllowFilter = false;
            this.colVerRechazo.ToolTip = "Informacion del rechazo";
            this.colVerRechazo.Visible = true;
            this.colVerRechazo.VisibleIndex = 9;
            this.colVerRechazo.Width = 28;
            // 
            // btnVerRechazo
            // 
            this.btnVerRechazo.AutoHeight = false;
            editorButtonImageOptions1.Image = global::Gv.ExodusBc.UI.Properties.Resources.cancelrq;
            this.btnVerRechazo.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Ver información del rechazo", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.btnVerRechazo.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnVerRechazo.Name = "btnVerRechazo";
            this.btnVerRechazo.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btnVerRechazo.Click += new System.EventHandler(this.btnVerRechazo_Click);
            // 
            // colVerRevision
            // 
            this.colVerRevision.ColumnEdit = this.btnVerRevision;
            this.colVerRevision.FieldName = "colVerRevision";
            this.colVerRevision.Name = "colVerRevision";
            this.colVerRevision.OptionsColumn.ShowCaption = false;
            this.colVerRevision.OptionsColumn.ShowInExpressionEditor = false;
            this.colVerRevision.OptionsFilter.AllowAutoFilter = false;
            this.colVerRevision.OptionsFilter.AllowFilter = false;
            this.colVerRevision.ToolTip = "Informacion de la revisión";
            this.colVerRevision.Visible = true;
            this.colVerRevision.VisibleIndex = 10;
            this.colVerRevision.Width = 28;
            // 
            // btnVerRevision
            // 
            this.btnVerRevision.AutoHeight = false;
            editorButtonImageOptions2.Image = global::Gv.ExodusBc.UI.Properties.Resources.review;
            this.btnVerRevision.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Ver información de la revisión", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.btnVerRevision.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnVerRevision.Name = "btnVerRevision";
            this.btnVerRevision.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btnVerRevision.Click += new System.EventHandler(this.btnVerRevision_Click);
            // 
            // colVerTicket
            // 
            this.colVerTicket.ColumnEdit = this.btnVerTicket;
            this.colVerTicket.FieldName = "colVerTicket";
            this.colVerTicket.Name = "colVerTicket";
            this.colVerTicket.OptionsColumn.ShowCaption = false;
            this.colVerTicket.OptionsColumn.ShowInExpressionEditor = false;
            this.colVerTicket.OptionsFilter.AllowAutoFilter = false;
            this.colVerTicket.OptionsFilter.AllowFilter = false;
            this.colVerTicket.ToolTip = "Generar solicitud";
            this.colVerTicket.Visible = true;
            this.colVerTicket.VisibleIndex = 11;
            this.colVerTicket.Width = 28;
            // 
            // btnVerTicket
            // 
            this.btnVerTicket.AllowMouseWheel = false;
            editorButtonImageOptions3.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_ticketok_32x32;
            this.btnVerTicket.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "Ver información del ticket", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.btnVerTicket.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnVerTicket.Name = "btnVerTicket";
            this.btnVerTicket.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btnVerTicket.Click += new System.EventHandler(this.btnVerTicket_Click);
            // 
            // colVerSolicitud
            // 
            this.colVerSolicitud.ColumnEdit = this.btnVerSolicitud;
            this.colVerSolicitud.FieldName = "colVerSolicitud";
            this.colVerSolicitud.Name = "colVerSolicitud";
            this.colVerSolicitud.OptionsColumn.ShowCaption = false;
            this.colVerSolicitud.OptionsColumn.ShowInExpressionEditor = false;
            this.colVerSolicitud.OptionsFilter.AllowAutoFilter = false;
            this.colVerSolicitud.OptionsFilter.AllowFilter = false;
            this.colVerSolicitud.ToolTip = "Informacion de la solicitud";
            this.colVerSolicitud.Visible = true;
            this.colVerSolicitud.VisibleIndex = 12;
            this.colVerSolicitud.Width = 28;
            // 
            // btnVerSolicitud
            // 
            this.btnVerSolicitud.AutoHeight = false;
            editorButtonImageOptions4.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_requesok_32x32;
            this.btnVerSolicitud.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "Ver información de la solicitud", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.btnVerSolicitud.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnVerSolicitud.Name = "btnVerSolicitud";
            this.btnVerSolicitud.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btnVerSolicitud.Click += new System.EventHandler(this.btnVerSolicitud_Click);
            // 
            // btnRefrescar
            // 
            this.btnRefrescar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefrescar.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.btnRefrescar.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRefrescar.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.btnRefrescar.Appearance.Options.UseBackColor = true;
            this.btnRefrescar.Appearance.Options.UseFont = true;
            this.btnRefrescar.Appearance.Options.UseForeColor = true;
            this.btnRefrescar.Appearance.Options.UseTextOptions = true;
            this.btnRefrescar.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnRefrescar.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnRefrescar.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_refresh_24x24;
            this.btnRefrescar.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnRefrescar.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnRefrescar.Location = new System.Drawing.Point(1206, 160);
            this.btnRefrescar.LookAndFeel.SkinName = "Whiteprint";
            this.btnRefrescar.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnRefrescar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnRefrescar.Name = "btnRefrescar";
            this.btnRefrescar.Size = new System.Drawing.Size(137, 35);
            this.btnRefrescar.TabIndex = 421;
            this.btnRefrescar.Text = "&Refrescar";
            this.btnRefrescar.Click += new System.EventHandler(this.btnRefrescar_Click);
            // 
            // ssForm
            // 
            this.ssForm.ClosingDelay = 500;
            // 
            // btnActualizarEstadosValid
            // 
            this.btnActualizarEstadosValid.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnActualizarEstadosValid.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.btnActualizarEstadosValid.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnActualizarEstadosValid.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.btnActualizarEstadosValid.Appearance.Options.UseBackColor = true;
            this.btnActualizarEstadosValid.Appearance.Options.UseFont = true;
            this.btnActualizarEstadosValid.Appearance.Options.UseForeColor = true;
            this.btnActualizarEstadosValid.Appearance.Options.UseTextOptions = true;
            this.btnActualizarEstadosValid.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnActualizarEstadosValid.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnActualizarEstadosValid.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_certificate_32x32;
            this.btnActualizarEstadosValid.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnActualizarEstadosValid.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnActualizarEstadosValid.Location = new System.Drawing.Point(984, 161);
            this.btnActualizarEstadosValid.LookAndFeel.SkinName = "Whiteprint";
            this.btnActualizarEstadosValid.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnActualizarEstadosValid.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnActualizarEstadosValid.Name = "btnActualizarEstadosValid";
            this.btnActualizarEstadosValid.Size = new System.Drawing.Size(200, 35);
            this.btnActualizarEstadosValid.TabIndex = 439;
            this.btnActualizarEstadosValid.Text = "Certificados Emitidos";
            this.btnActualizarEstadosValid.Click += new System.EventHandler(this.btnActualizarEstadosValid_Click);
            // 
            // dteFechaFinal
            // 
            this.dteFechaFinal.EditValue = null;
            this.dteFechaFinal.Location = new System.Drawing.Point(237, 171);
            this.dteFechaFinal.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dteFechaFinal.Name = "dteFechaFinal";
            this.dteFechaFinal.Properties.Appearance.BorderColor = System.Drawing.SystemColors.ActiveBorder;
            this.dteFechaFinal.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dteFechaFinal.Properties.Appearance.Options.UseBorderColor = true;
            this.dteFechaFinal.Properties.Appearance.Options.UseFont = true;
            this.dteFechaFinal.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dteFechaFinal.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.dteFechaFinal.Properties.AutoHeight = false;
            this.dteFechaFinal.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.dteFechaFinal.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dteFechaFinal.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dteFechaFinal.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dteFechaFinal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dteFechaFinal.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dteFechaFinal.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dteFechaFinal.Properties.LookAndFeel.SkinName = "Metropolis";
            this.dteFechaFinal.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.dteFechaFinal.Properties.Mask.IgnoreMaskBlank = false;
            this.dteFechaFinal.Properties.Mask.ShowPlaceHolders = false;
            this.dteFechaFinal.Size = new System.Drawing.Size(200, 25);
            this.dteFechaFinal.TabIndex = 473;
            // 
            // dteFechaInicial
            // 
            this.dteFechaInicial.EditValue = null;
            this.dteFechaInicial.Location = new System.Drawing.Point(31, 171);
            this.dteFechaInicial.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dteFechaInicial.Name = "dteFechaInicial";
            this.dteFechaInicial.Properties.Appearance.BorderColor = System.Drawing.SystemColors.ActiveBorder;
            this.dteFechaInicial.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dteFechaInicial.Properties.Appearance.Options.UseBorderColor = true;
            this.dteFechaInicial.Properties.Appearance.Options.UseFont = true;
            this.dteFechaInicial.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dteFechaInicial.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.dteFechaInicial.Properties.AutoHeight = false;
            this.dteFechaInicial.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.dteFechaInicial.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dteFechaInicial.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dteFechaInicial.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dteFechaInicial.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dteFechaInicial.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dteFechaInicial.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dteFechaInicial.Properties.LookAndFeel.SkinName = "Metropolis";
            this.dteFechaInicial.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.dteFechaInicial.Properties.Mask.BeepOnError = true;
            this.dteFechaInicial.Properties.Mask.IgnoreMaskBlank = false;
            this.dteFechaInicial.Properties.Mask.ShowPlaceHolders = false;
            this.dteFechaInicial.Size = new System.Drawing.Size(200, 25);
            this.dteFechaInicial.TabIndex = 472;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Appearance.Options.UseForeColor = true;
            this.labelControl2.Location = new System.Drawing.Point(239, 150);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(78, 20);
            this.labelControl2.TabIndex = 470;
            this.labelControl2.Text = "Fecha final:";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Appearance.Options.UseForeColor = true;
            this.labelControl4.Location = new System.Drawing.Point(34, 149);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(106, 20);
            this.labelControl4.TabIndex = 468;
            this.labelControl4.Text = "Fecha de inicio:";
            // 
            // lblAtencion
            // 
            this.lblAtencion.Appearance.Font = new System.Drawing.Font("Segoe UI", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAtencion.Appearance.ForeColor = System.Drawing.Color.Maroon;
            this.lblAtencion.Appearance.Options.UseFont = true;
            this.lblAtencion.Appearance.Options.UseForeColor = true;
            this.lblAtencion.Appearance.Options.UseTextOptions = true;
            this.lblAtencion.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblAtencion.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblAtencion.Location = new System.Drawing.Point(28, 110);
            this.lblAtencion.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblAtencion.Name = "lblAtencion";
            this.lblAtencion.Size = new System.Drawing.Size(1315, 23);
            this.lblAtencion.TabIndex = 477;
            this.lblAtencion.Text = "Para realizar una busqueda de solicitudes seleccione un rango de fechas.";
            // 
            // picAtencion
            // 
            this.picAtencion.Cursor = System.Windows.Forms.Cursors.Default;
            this.picAtencion.EditValue = global::Gv.ExodusBc.UI.Properties.Resources.lightbulb;
            this.picAtencion.Location = new System.Drawing.Point(0, 112);
            this.picAtencion.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.picAtencion.Name = "picAtencion";
            this.picAtencion.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picAtencion.Properties.Appearance.Options.UseBackColor = true;
            this.picAtencion.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.picAtencion.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picAtencion.Size = new System.Drawing.Size(26, 30);
            this.picAtencion.TabIndex = 476;
            // 
            // btnBuscarSolicitudes
            // 
            this.btnBuscarSolicitudes.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.btnBuscarSolicitudes.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuscarSolicitudes.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.btnBuscarSolicitudes.Appearance.Options.UseBackColor = true;
            this.btnBuscarSolicitudes.Appearance.Options.UseFont = true;
            this.btnBuscarSolicitudes.Appearance.Options.UseForeColor = true;
            this.btnBuscarSolicitudes.Appearance.Options.UseTextOptions = true;
            this.btnBuscarSolicitudes.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnBuscarSolicitudes.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnBuscarSolicitudes.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_search_32x32;
            this.btnBuscarSolicitudes.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnBuscarSolicitudes.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnBuscarSolicitudes.Location = new System.Drawing.Point(446, 162);
            this.btnBuscarSolicitudes.LookAndFeel.SkinName = "Whiteprint";
            this.btnBuscarSolicitudes.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnBuscarSolicitudes.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnBuscarSolicitudes.Name = "btnBuscarSolicitudes";
            this.btnBuscarSolicitudes.Size = new System.Drawing.Size(134, 35);
            this.btnBuscarSolicitudes.TabIndex = 478;
            this.btnBuscarSolicitudes.Text = "&Buscar";
            this.btnBuscarSolicitudes.Click += new System.EventHandler(this.btnBuscarSolicitudes_Click);
            // 
            // frmBandeja
            // 
            this.Appearance.BackColor = System.Drawing.Color.White;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1350, 760);
            this.Controls.Add(this.btnBuscarSolicitudes);
            this.Controls.Add(this.lblAtencion);
            this.Controls.Add(this.picAtencion);
            this.Controls.Add(this.dteFechaFinal);
            this.Controls.Add(this.dteFechaInicial);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.btnActualizarEstadosValid);
            this.Controls.Add(this.btnRefrescar);
            this.Controls.Add(this.gcRequest);
            this.Controls.Add(this.lblTituloAccion);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.labelControl9);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "frmBandeja";
            this.Text = "frmBandeja";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmBandeja_FormClosing);
            this.Load += new System.EventHandler(this.frmBandeja_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcRequest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvRequest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnVerRechazo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnVerRevision)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnVerTicket)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnVerSolicitud)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteFechaFinal.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteFechaFinal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteFechaInicial.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteFechaInicial.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAtencion.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton btnSalir;
        private DevExpress.XtraEditors.LabelControl lblNombreFormulario;
        private DevExpress.XtraEditors.LabelControl lblTituloAccion;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraGrid.GridControl gcRequest;
        private DevExpress.XtraGrid.Views.Grid.GridView gvRequest;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraEditors.SimpleButton btnRefrescar;
        private DevExpress.XtraGrid.Columns.GridColumn colVerTicket;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnVerTicket;
        private DevExpress.XtraGrid.Columns.GridColumn colVerSolicitud;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnVerSolicitud;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraSplashScreen.SplashScreenManager ssForm;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraEditors.SimpleButton btnActualizarEstadosValid;
        private DevExpress.XtraGrid.Columns.GridColumn colVerRechazo;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnVerRechazo;
        private DevExpress.XtraEditors.DateEdit dteFechaFinal;
        private DevExpress.XtraEditors.DateEdit dteFechaInicial;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl lblAtencion;
        private DevExpress.XtraEditors.PictureEdit picAtencion;
        private DevExpress.XtraEditors.SimpleButton btnBuscarSolicitudes;
        private DevExpress.XtraGrid.Columns.GridColumn colVerRevision;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnVerRevision;
    }
}