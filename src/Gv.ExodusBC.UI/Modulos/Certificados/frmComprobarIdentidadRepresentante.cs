﻿using System;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Gv.ExodusBc.LN;
using Gv.Utilidades;
using Newtonsoft.Json.Linq;
using static Gv.ExodusBc.UI.ExodusBcBase;

namespace Gv.ExodusBc.UI.Modulos.Certificados
{
    public partial class frmComprobarIdentidadRepresentante : XtraForm
    {
        public static string FORMULARIOREVISION = string.Empty;
        public static byte[] FOTO = null;
        public static string IDENTIFICACION = string.Empty;
        public static string NOMBRES = string.Empty;
        public static string NACIONALIDAD = string.Empty;
        public static string ESTADOCIVIL = string.Empty;
        public static string PROFESION = string.Empty;
        public static JArray ARRAYPERSON = null;
        public static int RESULTADOVALIDACION = 0;
        public static string VALIDACIONAFISDESCRIPCION = string.Empty;
        public static bool ESTADOVALIDACION = false;
        public static string CARGOEMPRESA = string.Empty;
        Timer tmr1a1Representante = new Timer();
        AxCls cs500e = null;

        //***Variables Afis***
        public delegate void SetLabelCallBack(LabelControl lbx, string texto, string tipo);
        string numeroConsultaAfis = string.Empty;
        System.Windows.Forms.Timer tmr1a1Certificados = new System.Windows.Forms.Timer();
        AfisTransaccion afisRespuesta = new AfisTransaccion();
        WsAFIS.NistService wsAfis = new WsAFIS.NistService();
        int contador = 10;
        int intentos = 0;
        bool nistEnviado;


        #region VALORES INICIALES DEL FORMUALRIO

        public frmComprobarIdentidadRepresentante()
        {
            InitializeComponent();
        }

        private enum ValidacionAfis
        {
            NO_INICIADO = 0,
            ENVIANDO_NIST = 1,
            NIST_ENVIADO = 2,
            ERROR_ENVIO_NIST = 3,
            ESPERANDO_RESPUESTA = 4,
            COMPLETADO = 5,
            TIMEOUT = 6
        }
        ValidacionAfis ValidacionHuellas = ValidacionAfis.NO_INICIADO;

        public enum RESULTADOVALIDACIONAFIS
        {
            VALIDACIONEXITOSA = 1,
            VALIDACIONFALLIDA = 2,
            VALIDACIONTIMEOUT = 3,
            VALIDACIONHUELLASDIFERENTES = 4
        }

        private void frmComprobarIdentidadRepresentante_Load(object sender, EventArgs e)
        {
            //cs500e = new AxCls();
            //tmr1a1Certificados.Tick += tmr1a1Certificados_Tick;
            //tmr1a1Representante.Tick += tmr1a1Representante_Tick;
            //lblGifValidando.Visible = false;
            lblIdentificacion.Text = IDENTIFICACION;
            lblNombres.Text = NOMBRES;
            lblNacionalidad.Text = NACIONALIDAD;
            lblEstadoCivil.Text = ESTADOCIVIL;
            lblProfesion.Text = PROFESION;
            picRepresentante.EditValue = FOTO;
            btnAceptar.Enabled = true;
            txtCargo.Select();
           
        }

        #endregion


        #region   DE LOS CONTROLES

        private void btnValidarPersona_Click(object sender, EventArgs e)
        {
            cs500e.ComprobarEscaner();
            tmr1a1Representante.Start();
            IniciarCaptura();
        }

        void tmr1a1Representante_Tick(object sender, EventArgs e)
        {
            lblGifValidando.Visible = true;
            if(ESTADOVALIDACION)
            {
                lblGifValidando.Visible = false;
                tmr1a1Representante.Stop();
                btnAceptar.Enabled = true;
                switch (RESULTADOVALIDACION)
                {
                    case (int)RESULTADOVALIDACIONAFIS.VALIDACIONEXITOSA:
                        {
                            lblEstadoValidacion.ForeColor = Color.SeaGreen;
                            lblEstadoValidacion.Text = VALIDACIONAFISDESCRIPCION;
                            break;
                        }
                    case (int)RESULTADOVALIDACIONAFIS.VALIDACIONFALLIDA:
                        {
                            lblEstadoValidacion.ForeColor = ColorTranslator.FromHtml("#E54B4B");
                            lblEstadoValidacion.Text = VALIDACIONAFISDESCRIPCION;
                            break;
                        }
                    case (int)RESULTADOVALIDACIONAFIS.VALIDACIONTIMEOUT:
                        {
                            lblEstadoValidacion.ForeColor = Color.DarkOrange;
                            lblEstadoValidacion.Text = VALIDACIONAFISDESCRIPCION;
                            break;
                        }
                    case (int)RESULTADOVALIDACIONAFIS.VALIDACIONHUELLASDIFERENTES:
                        {
                            lblEstadoValidacion.ForeColor = ColorTranslator.FromHtml("#E54B4B");
                            lblEstadoValidacion.Text = VALIDACIONAFISDESCRIPCION;
                            break;
                        }
                }
            }
            else
            {
                lblGifValidando.Visible = false;
                tmr1a1Representante.Stop();
            }
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            //if (RESULTADOVALIDACION == (int)RESULTADOVALIDACIONAFIS.VALIDACIONEXITOSA) frmAsociarRepresentantes.IDENTIDADCOMPROBADA = true;
            //else if(RESULTADOVALIDACION == (int)RESULTADOVALIDACIONAFIS.VALIDACIONHUELLASDIFERENTES) frmAsociarRepresentantes.IDENTIDADCOMPROBADA = false;
            //else if (RESULTADOVALIDACION == (int)RESULTADOVALIDACIONAFIS.VALIDACIONTIMEOUT)
            //{
            //    if (XtraMessageBox.Show("Se presento algun error con la verificación de la persona en el servidor. ¿Desea continuar con el proceso?", "Atención",
            //        MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            //    {
            //        frmAsociarRepresentantes.IDENTIDADCOMPROBADA = true;
            //    }
            //    else frmAsociarRepresentantes.IDENTIDADCOMPROBADA = false;
            //}
            //else lblGifValidando.Visible = false;

            if (txtCargo.Text == string.Empty)
            {
                XtraMessageBox.Show("Este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtCargo.Focus();
                return;
            }
            else CARGOEMPRESA = txtCargo.Text.Trim().ToUpper();

            frmAsociarRepresentantes.IDENTIDADCOMPROBADA = true;
            Close();
        }

        private void frmComprobarIdentidadRepresentante_FormClosing(object sender, FormClosingEventArgs e)
        {
            //if (RESULTADOVALIDACION == (int)RESULTADOVALIDACIONAFIS.VALIDACIONEXITOSA) frmAsociarRepresentantes.IDENTIDADCOMPROBADA = true;
            //if (RESULTADOVALIDACION == (int)RESULTADOVALIDACIONAFIS.VALIDACIONTIMEOUT) frmAsociarRepresentantes.IDENTIDADCOMPROBADA = false;
            //if (RESULTADOVALIDACION == (int)RESULTADOVALIDACIONAFIS.VALIDACIONHUELLASDIFERENTES) frmAsociarRepresentantes.IDENTIDADCOMPROBADA = false;

            frmAsociarRepresentantes.IDENTIDADCOMPROBADA = true;

            if(txtCargo.Text == string.Empty)
            {
                XtraMessageBox.Show("El cargo del representante esta vacío, este dato no se reflejará en el contrato.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        #endregion


        #region FUNCIONES AFIS

        public void IniciarCaptura()
        {
            ValidacionHuellas = ValidacionAfis.NO_INICIADO;
            try
            {
                numeroConsultaAfis = "C" + SequenceLN.GetSequenceConsultaAfis();
                string sexo = string.Empty;
                string nombre = string.Empty;
                string apellido = string.Empty;
                string nacionalidad = string.Empty;
                string fecNacimiento = string.Empty;

                if (Convert.ToInt32(RequestCertificate.GetItemFromJArray(ARRAYPERSON, "FKSexId").ToString()) == (int)EN.Sex.SexoIndex.FEMENINO)
                    sexo = EN.Sex.SexoCode.F.ToString();
                else sexo = EN.Sex.SexoCode.M.ToString();
                nombre = RequestCertificate.GetItemFromJArray(ARRAYPERSON, "FirstName").ToString();
                apellido = RequestCertificate.GetItemFromJArray(ARRAYPERSON, "FirstLastName").ToString();
                nacionalidad = RequestCertificate.GetItemFromJArray(ARRAYPERSON, "Nacionality").ToString();
                fecNacimiento = string.Format("{0:yyyy-MM-dd}", RequestCertificate.GetItemFromJArray(ARRAYPERSON, "FechaNacimiento").ToString());

                string patWSQFiles = Path.Combine(VariablesGlobales.PathWSQFID + numeroConsultaAfis);
                cs500e.PrepararCapturaHuellas();
                if (cs500e.CapturarHuellasDerecha(numeroConsultaAfis) == 0)
                {
                    if (cs500e.SalvarHuellasCapturadas(numeroConsultaAfis, patWSQFiles, "") > 0)
                    {
                        var empNist = Nist.EmpaquetarNistFID(numeroConsultaAfis, patWSQFiles, nombre, apellido, "", sexo, nacionalidad, Convert.ToDateTime(fecNacimiento), "GVISION");
                        if (empNist)
                        {
                            if (ValidacionHuellas == ValidacionAfis.NO_INICIADO || ValidacionHuellas == ValidacionAfis.TIMEOUT)
                            {
                                ValidacionHuellas = ValidacionAfis.ENVIANDO_NIST;
                                IniciarProcesamientoNist1a1();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmCertificados::IniciarCaptura", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }

        }

        private void IniciarProcesamientoNist1a1()
        {
            try
            {
                tmr1a1Certificados.Start();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        void tmr1a1Certificados_Tick(object sender, EventArgs e)
        {
            lblGifValidando.Visible = true;
            lblEstadoValidacion.Visible = true;
            pnlVerificacionHuellas.Visible = true;
            SetLabelText(lblEstadoValidacion, "Packaging the NIST file...", "I");

            if (!EnviarNist())
            {
                lblEstadoValidacion.Font = new Font(lblEstadoValidacion.Font, FontStyle.Bold);
                tmr1a1Certificados.Stop();
                lblEstadoValidacion.Font = new Font(lblEstadoValidacion.Font, FontStyle.Bold);
                SetLabelText(lblEstadoValidacion, "Error sending NIST file", "A");
                lblGifValidando.Visible = false;
            }

            if (intentos == 12)
            {
                contador = 0;
                intentos = 0;
                tmr1a1Certificados.Stop();
                ValidacionHuellas = ValidacionAfis.TIMEOUT;
                RESULTADOVALIDACION = (int)RESULTADOVALIDACIONAFIS.VALIDACIONTIMEOUT;

                lblEstadoValidacion.Font = new Font(lblEstadoValidacion.Font, FontStyle.Bold);
                SetLabelText(lblEstadoValidacion, "Time out, Unable to validate fingerprints", "A");
                if (XtraMessageBox.Show("Se presento algun error con la verificación de la persona en el servidor. ¿Desea continuar con el proceso?", "Atención",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    btnAceptar.Enabled = true;
                }
                else btnAceptar.Enabled = false;
                lblGifValidando.Visible = false;
                return;
            }

            if (contador == 10)
            {
                AfisTransaccion at = ValidarHuellaAfis(numeroConsultaAfis);
                if (at.Status == "STATE_FINAL")
                {
                    intentos = 0;
                    contador = 0;
                    tmr1a1Certificados.Stop();
                    ValidacionHuellas = ValidacionAfis.COMPLETADO;

                    //**VERIFICAR SI HUBO HIT O NO**
                    if (at.ConfirmStatus == "Y")
                    {
                        try
                        {
                            if (at.CAN_TCN == RequestCertificate.GetItemFromJArray(ARRAYPERSON, "EnrolmentId").ToString() + "")
                            {
                                //***Validación exitosa***
                                RESULTADOVALIDACION = (int)RESULTADOVALIDACIONAFIS.VALIDACIONEXITOSA;
                                SetLabelText(lblEstadoValidacion, "Identidad confirmada", "E");
                                lblGifValidando.Visible = false;
                                btnAceptar.Enabled = true;
                            }
                            else if (at.CAN_TCN != RequestCertificate.GetItemFromJArray(ARRAYPERSON, "EnrolmentId").ToString() + "")
                            {
                                string fechaNacimientoAfis = at.CAN_DateOfBirth;
                                string fechaNacimientoSQL = DateTime.ParseExact(RequestCertificate.GetItemFromJArray(ARRAYPERSON, "FechaNacimiento").ToString(), "dd/MM/yyyy", CultureInfo.CurrentCulture).ToString("yyyyMMdd");
                                string sexo = string.Empty;
                                if (Convert.ToInt32(RequestCertificate.GetItemFromJArray(ARRAYPERSON, "FKSexId").ToString()) == 2) sexo = "M";
                                else sexo = "F";

                                if (at.CAN_FirstName == RequestCertificate.GetItemFromJArray(ARRAYPERSON, "FirstName").ToString()
                                    && at.CAN_LastName == RequestCertificate.GetItemFromJArray(ARRAYPERSON, "FirstLastName").ToString()
                                    && at.CAN_Sex == sexo
                                    && fechaNacimientoAfis == fechaNacimientoSQL)
                                {
                                    //***Validación exitosa***
                                    RESULTADOVALIDACION = (int)RESULTADOVALIDACIONAFIS.VALIDACIONEXITOSA;
                                    SetLabelText(lblEstadoValidacion, "Identidad confirmada", "E");
                                    lblGifValidando.Visible = false;
                                    btnAceptar.Enabled = true;
                                }
                                else
                                {
                                    //***Validación fallida***
                                    RESULTADOVALIDACION = (int)RESULTADOVALIDACIONAFIS.VALIDACIONFALLIDA;
                                    SetLabelText(lblEstadoValidacion, "No HIT, Identidad NO confirmada.", "A");
                                    lblGifValidando.Visible = false;
                                    btnAceptar.Enabled = false;
                                    XtraMessageBox.Show("Las huellas enviadas NO coinciden con las huellas del solicitante.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                }
                            }
                            else
                            {
                                //***Validación fallida***
                                RESULTADOVALIDACION = (int)RESULTADOVALIDACIONAFIS.VALIDACIONFALLIDA;
                                SetLabelText(lblEstadoValidacion, "No HIT, Identidad NO confirmada", "A");
                                lblGifValidando.Visible = false;
                                btnAceptar.Enabled = false;
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                    else
                    {
                        //***Validación fallida***
                        RESULTADOVALIDACION = (int)RESULTADOVALIDACIONAFIS.VALIDACIONFALLIDA;
                        SetLabelText(lblEstadoValidacion, "No HIT, Identidad NO confirmada", "A");
                        lblGifValidando.Visible = false;
                        return;
                    }
                }
                else
                {
                    contador = 0;
                    intentos++;
                    SetLabelText(lblEstadoValidacion, "Comparando huellas..." + intentos, "I");
                }
            }
            else
            {
                SetLabelText(lblEstadoValidacion, "Comparando huellas...", "I");
                contador++;
            }

        }

        bool EnviarNist()
        {
            //Envio Nist
            try
            {
                SetLabelText(lblEstadoValidacion, "Sending NIST file to AFIS...", "I");
                string pathFileNist = Path.Combine(VariablesGlobales.PathWSQFID, numeroConsultaAfis + "\\" + numeroConsultaAfis + ".tdf");

                if (!File.Exists(pathFileNist))
                    return false;

                byte[] barc = File.ReadAllBytes(pathFileNist);
                string base64 = Convert.ToBase64String(barc);
                int respuestaEnvioCafis = wsAfis.UploadNist(pathFileNist, base64);

                if (respuestaEnvioCafis == 0)
                {
                    ValidacionHuellas = ValidacionAfis.NIST_ENVIADO;
                    nistEnviado = true;
                    return true;
                }
                else
                {
                    ValidacionHuellas = ValidacionAfis.ERROR_ENVIO_NIST;
                    nistEnviado = false;
                    return false;
                }
            }
            catch (Exception ex)
            {
                ValidacionHuellas = ValidacionAfis.ERROR_ENVIO_NIST;
                nistEnviado = false;
                return false;
            }
        }

        void SetLabelText(LabelControl lbl, string texto, string tipo)
        {
            try
            {
                if (lbl.InvokeRequired)
                {
                    SetLabelCallBack continuar = new SetLabelCallBack(SetLabelText);
                    Invoke(continuar, new object[] { lbl, texto, tipo });
                }
                else
                {
                    lbl.Text = texto;
                    switch (tipo)
                    {
                        case "E":
                            {
                                lbl.ForeColor = Color.SeaGreen;
                                break;
                            }
                        case "A":
                            {
                                lbl.ForeColor = ColorTranslator.FromHtml("#E54B4B");
                                break;
                            }
                        case "I":
                            {
                                lbl.ForeColor = Color.DarkOrange;
                                break;
                            }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InsertarLog(Log.ErrorType.Error, "frmCertificados::SetLabelText", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        AfisTransaccion ValidarHuellaAfis(string noTransaccion)
        {
            string strTransNo = noTransaccion;
            string strTOT = string.Empty;
            string strExternalID = string.Empty;
            string strStatus = string.Empty;
            string strConfirm_Status = string.Empty;
            string strCAN_TransNo = string.Empty;
            string strCAN_ExternalId = string.Empty;
            string strCAN_TCN = string.Empty;
            string strCAN_LastName = string.Empty;
            string strCAN_FirstName = string.Empty;
            string strCAND_ID1 = string.Empty;
            string strCAND_SID = string.Empty;
            string strCAN_Sex = string.Empty;
            string strCAN_DateOfBirth = String.Empty;
            string strCAN_TOT = string.Empty;
            string strCAND_POB = string.Empty;


            afisRespuesta.TransNo = noTransaccion;

            int respuesta = wsAfis.GetTransStatus(strTransNo, out strTOT, out strExternalID, out strStatus, out strConfirm_Status, out strCAN_TransNo, out strCAN_ExternalId, out strCAN_TCN,
                out strCAN_LastName, out strCAN_FirstName, out strCAND_ID1, out strCAND_SID, out strCAN_Sex, out strCAN_DateOfBirth, out strCAN_TOT, out strCAND_POB);

            afisRespuesta.TransNo = strTransNo;
            afisRespuesta.TOT = strTOT;
            afisRespuesta.ExternalID = strExternalID;
            afisRespuesta.Status = strStatus;
            afisRespuesta.ConfirmStatus = strConfirm_Status;
            afisRespuesta.CAN_TransNo = strCAN_TransNo;
            afisRespuesta.CAN_ExternalId = strCAN_ExternalId;
            afisRespuesta.CAN_TCN = strCAN_TCN;
            afisRespuesta.CAN_LastName = strCAN_LastName;
            afisRespuesta.CAN_FirstName = strCAN_FirstName;
            afisRespuesta.CAND_ID1 = strCAND_ID1;
            afisRespuesta.CAN_SID = strCAND_SID;
            afisRespuesta.CAN_Sex = strCAN_Sex;
            afisRespuesta.CAN_DateOfBirth = strCAN_DateOfBirth;
            afisRespuesta.CAN_TOT = strCAN_TOT;
            afisRespuesta.CAND_POB = strCAND_POB;

            return afisRespuesta;
        }

        class AfisTransaccion
        {
            public string TransNo { get; set; }
            public string TOT { get; set; }
            public string ExternalID { get; set; }
            public string Status { get; set; }
            public string ConfirmStatus { get; set; }
            public string CAN_TransNo { get; set; }
            public string CAN_ExternalId { get; set; }
            public string CAN_TCN { get; set; }
            public string CAN_LastName { get; set; }
            public string CAN_FirstName { get; set; }
            public string CAND_ID1 { get; set; }
            public string CAN_SID { get; set; }
            public string CAN_Sex { get; set; }
            public string CAN_DateOfBirth { get; set; }
            public string CAN_TOT { get; set; }
            public string CAND_POB { get; set; }
        }

        #endregion


    }
}