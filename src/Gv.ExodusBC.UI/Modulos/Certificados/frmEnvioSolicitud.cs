﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Newtonsoft.Json.Linq;
using static Gv.ExodusBc.UI.ExodusBcBase;
using static Gv.ExodusBc.EN.Request;
using Newtonsoft.Json;
using System.IO;
using Gv.Utilidades;
using System.Net;
using Gv.ExodusBc.LN;

namespace Gv.ExodusBc.UI.Modulos.Certificados
{
    public partial class frmEnvioSolicitud : XtraForm
    {
        public static string CODIGOPRODUCTO = string.Empty;
        public static string DESCRIPCIONPRODUCTO = string.Empty;
        public static string NUMERODOCUMENTO = string.Empty;
        public static string NOMBRES = string.Empty;
        public static string CORREO = string.Empty;
        public static string TELEFONO = string.Empty;
        public static string NUMERODETICKET = string.Empty;
        public static string NUMERODETICKETFINAL = string.Empty;
        public static string NUMERODEFACTURAMANUAL = string.Empty;
        public static int TIPOPERSONA = 0;
        public static int TIPOTRANSACCION = 0;
        public static int ESTADOSOLICITUD = 0;
        public static int ESTADOTRANSACION = 0;
        public static int REQUESTID = 0;
        public static JArray ARRAYREQUEST = null;
        public static string NUMEROFACTURA = string.Empty;
        GP.DatosGP wsFunction = new GP.DatosGP();

        //Ajuntos
        public string ADJUNTOPRSNATURAL = string.Empty;
        public string ADJUNTOPRSJURIDICA = string.Empty;
        public static List<EN.Dossie> ListadoAdjuntos = new List<EN.Dossie>();
        public static List<EN.Dossie> ListadoFinalAdjuntos = new List<EN.Dossie>();

        //CLiente RTN
        public static string clienteRTN = string.Empty;



        #region VALORES INICIALES DEL FORMUALRIO

        public frmEnvioSolicitud()
        {
            InitializeComponent();
            try
            {
                ssForm.ShowWaitForm();
                Init();
                ssForm.CloseWaitForm();
            }
            catch (Exception ex)
            {
                if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                XtraMessageBox.Show("Hubo un error al tratar de mostrar la información del ticket.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmEnvioSolicitud::InitializeComponent", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }

        }

        void Init()
        {
            ShowIcon = true;
            lblProducto.Text = string.Empty;
            lblDescripcion.Text = string.Empty;
            lblNumeroDocumento.Text = string.Empty;
            lblNombres.Text = string.Empty;
            lblCorreo.Text = string.Empty;
            lblTelefono.Text = string.Empty;
            txtRtnCiente.Text = string.Empty;

            lblProducto.Text = CODIGOPRODUCTO;
            lblDescripcion.Text = DESCRIPCIONPRODUCTO;
            lblNumeroDocumento.Text = NUMERODOCUMENTO;
            lblNombres.Text = NOMBRES;
            lblCorreo.Text = CORREO;
            lblTelefono.Text = TELEFONO;

            if (NUMERODETICKETFINAL == string.Empty)
            {
                lblNumeroTicket.Visible = false;
                lblNumeroTicketTitulo.Visible = false;
            }
            else
            {
                lblNumeroTicket.Visible = true;
                lblNumeroTicketTitulo.Visible = true;
                lblNumeroTicket.Text = NUMERODETICKETFINAL;
            }

            ValidarPermisos();
            if (ESTADOTRANSACION == (int)TransactionStatus.SOLICITUDAPROBADA && ESTADOSOLICITUD == (int)RequestStatusList.TRANSACCIONOINICIADA && NUMERODETICKETFINAL == string.Empty && NUMEROFACTURA == string.Empty)
            {
                btnEnviarSolicitud.Text = "Solicitar ticket";
                btnEnviarSolicitud.Enabled = true;
                btnEnviarFactura.Enabled = false;
            }
            else if (ESTADOTRANSACION == (int)TransactionStatus.SOLICITUDAPROBADA && ESTADOSOLICITUD == (int)RequestStatusList.PENDIENTESOLICITUD && NUMERODETICKETFINAL == string.Empty && NUMEROFACTURA == string.Empty) 
            {
                btnEnviarSolicitud.Text = "Solicitar ticket";
                btnEnviarSolicitud.Enabled = true;
                btnEnviarFactura.Enabled = false;
            }
            else if (ESTADOTRANSACION == (int)TransactionStatus.FACTURAPAGADA && NUMERODEFACTURAMANUAL != string.Empty && NUMERODETICKETFINAL == string.Empty)
            {
                btnEnviarSolicitud.Text = "Solicitar ticket";
                btnEnviarSolicitud.Enabled = true;
                btnEnviarFactura.Enabled = false;
            }
            else if (ESTADOTRANSACION == (int)TransactionStatus.SOLICITUDAPROBADA && ESTADOSOLICITUD == (int)RequestStatusList.PENDIENTESOLICITUD && NUMERODETICKETFINAL != string.Empty)
            {
                btnEnviarSolicitud.Text = "Enviar solicitud";
                btnEnviarSolicitud.Enabled = false;
                btnEnviarFactura.Enabled = true;
            }
            else if (ESTADOTRANSACION == (int)TransactionStatus.FACTURAPAGADA && NUMERODEFACTURAMANUAL != string.Empty && NUMERODETICKETFINAL != string.Empty)
            {
                btnEnviarSolicitud.Text = "Enviar solicitud";
                btnEnviarSolicitud.Enabled = true;
                btnEnviarFactura.Enabled = false;
            }
            else if (ESTADOTRANSACION == (int)TransactionStatus.FACTURAPAGADA && NUMEROFACTURA != string.Empty)
            {
                btnEnviarSolicitud.Text = "Enviar solicitud";
                btnEnviarSolicitud.Enabled = true;
                btnEnviarFactura.Enabled = false;
            }

        }

        #endregion

        #region FUNCIONES DE LOS CONTROLES
       
        private void btnEnviarSolicitud_Click(object sender, EventArgs e)
        {
            if(TIPOTRANSACCION == (int)TransactionType.GENERATETICKET && NUMERODETICKETFINAL != string.Empty)
            {
                XtraMessageBox.Show("Esta solicitud ya tiene asignado número de ticket.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            string mensaje = string.Empty;
            if (TIPOTRANSACCION == (int)TransactionType.GENERATETICKET) mensaje = "Confirma que procederá a solicitar el ticket.";
            else mensaje = "Confirma que procederá a enviar la solicitud.";

            if (XtraMessageBox.Show(mensaje, "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                try
                {
                    ssForm.ShowWaitForm();
                    if (TIPOTRANSACCION == (int)TransactionType.GENERATETICKET) GenerateTicket();
                    else GenerateRequest();

                    if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                }
                catch (Exception ex)
                {
                    if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                    XtraMessageBox.Show("Hubo un error al tratar de enviar la solicitud. Intente nuevamente en otro momento.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Log.InsertarLog(Log.ErrorType.Error, "frmEnvioSolicitud::btnEnviarSolicitud_Click", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                }
            }
            else return;
        }

        private void btnEnviarFactura_Click(object sender, EventArgs e)
        {
            CrearFactura(ARRAYREQUEST);
        }

        #endregion

        #region METODOS Y PROCEDIMIENTOS

        void ValidarPermisos()
        {
            var agAutorizado = REGISTRATIONAGENTLN.GetCodeAgent(Convert.ToInt32(VariablesGlobales.USER_ID));
            if (agAutorizado != null)
            {
                VariablesGlobales.AGAUTORITY = agAutorizado.Code;
                VariablesGlobales.REGISTRATION_AGENT_ID = Convert.ToInt32(agAutorizado.RegistrationAgentId);
            }
            else
            {
                XtraMessageBox.Show("Este usuario no esta autorizado para realizar emisión de certificados.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
        }

        void GenerateTicket()
        {
            try
            {
                //**CREAR JSON PARA GENERACION DE TICKET A VALID**
                JObject json = new JObject();

                //**ENCABEZADO**
                RequestCertificate.ApplicantDataGlobal appDtFirstCall = new RequestCertificate.ApplicantDataGlobal();
                JProperty sku = new JProperty("sku", "");

                foreach (JObject element in ARRAYREQUEST)
                {
                    //**TIPO DE PERSONA**
                    if (TIPOPERSONA == (int)TypePerson.PRSNATURAL)
                    {
                        sku = new JProperty("sku", element["CodigoProducto"].ToString().Trim());
                        appDtFirstCall.name = element["Names"].ToString() +" "+ element["LastNames"].ToString();
                        appDtFirstCall.documentType = element["CodigoDocumento"].ToString();
                        appDtFirstCall.documentNumber = element["DocumentNumberNaturalPerson"].ToString();
                        appDtFirstCall.email = element["Email"].ToString();
                        appDtFirstCall.phoneDDD = "00";
                        appDtFirstCall.phoneNumber = element["CellPhonePersonal"].ToString();
                    }
                    else
                    {
                        sku = new JProperty("sku", element["CodigoProducto"].ToString());
                        appDtFirstCall.name = element["NameLegalPerson"].ToString();
                        appDtFirstCall.documentType = element["CodigoDocumentoLegal"].ToString();
                        appDtFirstCall.documentNumber = element["DocumentNumberLegalPerson"].ToString();
                        appDtFirstCall.email = element["LegalPersonEmail"].ToString();
                        appDtFirstCall.phoneDDD = "00";
                        appDtFirstCall.phoneNumber = element["LegalPersonPhone"].ToString();
                    }
                }

                //**PROPIEDADES DEL JSON**
                JProperty language = new JProperty("language", VariablesGlobales.IDIOMA.ToLower());
                JProperty pedido = new JProperty("idPedido", VariablesGlobales.PEDIDO);
                JProperty raOperator = new JProperty("raOperatorCode", VariablesGlobales.AGAUTORITY);
                JProperty certOffice = new JProperty("certificateOfficeCode", VariablesGlobales.CERTOFFICE);
                JProperty raCode = new JProperty("raCode", VariablesGlobales.RGAURORITY);
                JProperty skinCode = new JProperty("skinCode", VariablesGlobales.SKINCODE);
                JProperty person = new JProperty("applicantDataGlobal", (JObject)JToken.FromObject(appDtFirstCall));

                json.Add(language);
                json.Add(pedido);
                json.Add(sku);
                json.Add(raOperator);
                json.Add(certOffice);
                json.Add(raCode);
                json.Add(skinCode);
                json.Add(person);

                //**ARCHIVO DE SALIDA**
                string jsonOut = JsonConvert.SerializeObject(json, Formatting.None);
                File.WriteAllText(VariablesGlobales.PathDataSystem + "jsonTicket.json", jsonOut);

                //**RESPUESTA DEL TICKET POR VALID**
                NUMERODETICKET = string.Empty;
                string result = string.Empty;
                dynamic rspValid = null;
                string mensajeError = string.Empty;
                result = ResponseRequestValid(jsonOut, (int)TransactionType.GENERATETICKET);
                if (result != string.Empty)
                {
                    rspValid = JObject.Parse(result);
                    if (rspValid.success == "true")
                    {
                        NUMERODETICKET = rspValid.ticket;

                        //**Verificar si existe en la tabla de cliente**
                        //**Si no existe entonces crearlo y enviarlo a GP**
                        JArray objPersona = new JArray();
                        JArray objCliente = new JArray();
                        if (TIPOPERSONA == (int)TypePerson.PRSNATURAL)
                            objCliente = RequestCertificate.GetExistCustomer(RequestCertificate.GetItemFromJArray(ARRAYREQUEST, "DocumentNumberNaturalPerson").ToString());
                        else objCliente = RequestCertificate.GetExistCustomer(RequestCertificate.GetItemFromJArray(ARRAYREQUEST, "DocumentNumberLegalPerson").ToString());
                        if (objCliente.Count > 0)
                        {
                            objPersona = RequestCertificate.GetPersonById(Convert.ToInt32(RequestCertificate.GetItemFromJArray(ARRAYREQUEST, "PersonId").ToString()));
                            if (objPersona != null)
                            {
                                if (RequestCertificate.UpdateRequestStatusTicket(REQUESTID, NUMERODETICKET, (int)RequestStatusList.PENDIENTESOLICITUD))
                                {
                                    GuardarSeguimientoCertificado(REQUESTID, (int)RequestStatusList.PENDIENTESOLICITUD, (int)RequestStatusList.PENDIENTESOLICITUD);
                                    ssForm.CloseWaitForm();
                                    XtraMessageBox.Show("Ticket generado satisfactoriamente con el numero:." + NUMERODETICKET + ".", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information);

                                    btnEnviarFactura.Enabled = true;
                                    btnEnviarSolicitud.Enabled = false;
                                }
                                else
                                {
                                    XtraMessageBox.Show("Hubo un error al solicitar el ticket. Intente en otro momento.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    Log.InsertarLog(Log.ErrorType.Error, "frmEnvioSolicitud::GenerateTicket", "Error", VariablesGlobales.PathDataLog);
                                    Close();
                                }
                            }
                        }
                        else
                        {
                            //***Crear cliente en GENESYSONE que no existen***
                            objPersona = RequestCertificate.GetPersonById(Convert.ToInt32(RequestCertificate.GetItemFromJArray(ARRAYREQUEST, "PersonId").ToString()));
                            EN.Customers clientes = new EN.Customers();
                            if (TIPOPERSONA == (int)TypePerson.PRSNATURAL)
                            {
                                clientes.DocumentNumber = RequestCertificate.GetItemFromJArray(objPersona, "DocumentNumber").ToString();
                                clientes.Names = RequestCertificate.GetItemFromJArray(objPersona, "Names").ToString();
                                clientes.LastNames = RequestCertificate.GetItemFromJArray(objPersona, "LastNames").ToString();
                                clientes.City = RequestCertificate.GetItemFromJArray(objPersona, "City").ToString();
                                clientes.Address = RequestCertificate.GetItemFromJArray(objPersona, "Address").ToString();
                                clientes.PersonalMovilPhone = RequestCertificate.GetItemFromJArray(objPersona, "PersonalMovilPhone").ToString();
                                clientes.WorkPhone = RequestCertificate.GetItemFromJArray(objPersona, "WorkPhone").ToString();
                            }
                            else
                            {
                                clientes.DocumentNumber = RequestCertificate.GetItemFromJArray(ARRAYREQUEST, "DocumentNumberLegalPerson").ToString();
                                clientes.Names = RequestCertificate.GetItemFromJArray(ARRAYREQUEST, "NameLegalPerson").ToString();
                                clientes.WorkPhone = RequestCertificate.GetItemFromJArray(ARRAYREQUEST, "LegalPersonPhone").ToString();
                            }

                            clientes.CreationDate = Convert.ToDateTime(LoginWebService.getStatusConecction());
                            clientes.FKPersonType = TIPOPERSONA;
                            clientes.FKCreateUserId = (int)VariablesGlobales.USER_ID;
                            if (RequestCertificate.InsertNewRCustomer(clientes))
                            {
                                //***Información para GP creación del cliente***
                                wsFunction = new GP.DatosGP();
                                GP.clsClientes wsClientes = new GP.clsClientes();
                                if (TIPOPERSONA == (int)TypePerson.PRSNATURAL)
                                {
                                    wsClientes.Codigo = RequestCertificate.GetItemFromJArray(objPersona, "DocumentNumber").ToString();
                                    wsClientes.Nombres = RequestCertificate.GetItemFromJArray(objPersona, "Names").ToString();
                                    wsClientes.Apellidos = RequestCertificate.GetItemFromJArray(objPersona, "LastNames").ToString();
                                    wsClientes.Ciudad = RequestCertificate.GetItemFromJArray(objPersona, "City").ToString();
                                    wsClientes.Contacto = "";
                                    wsClientes.RTN = txtRtnCiente.Text.Trim();
                                    wsClientes.Direccion1 = RequestCertificate.GetItemFromJArray(objPersona, "Address").ToString();
                                    wsClientes.Direccion2 = "";
                                    wsClientes.Telefono1 = RequestCertificate.GetItemFromJArray(objPersona, "PersonalMovilPhone").ToString();
                                    wsClientes.Telefono2 = RequestCertificate.GetItemFromJArray(objPersona, "WorkPhone").ToString();
                                }
                                else
                                {
                                    wsClientes.Codigo = RequestCertificate.GetItemFromJArray(ARRAYREQUEST, "DocumentNumberLegalPerson").ToString();
                                    wsClientes.Nombres = RequestCertificate.GetItemFromJArray(ARRAYREQUEST, "NameLegalPerson").ToString();
                                    wsClientes.Apellidos = "";
                                    wsClientes.Ciudad = "";
                                    wsClientes.Contacto = "";
                                    wsClientes.Direccion1 = "";
                                    wsClientes.Direccion2 = "";
                                    wsClientes.Telefono1 = "";
                                    wsClientes.RTN = RequestCertificate.GetItemFromJArray(ARRAYREQUEST, "DocumentNumberLegalPerson").ToString();
                                    wsClientes.Telefono2 = RequestCertificate.GetItemFromJArray(ARRAYREQUEST, "LegalPersonPhone").ToString();
                                }

                                var resultGp = wsFunction.CreacionCliente(wsClientes);
                                if (resultGp.ToString() == "True")
                                {
                                    lblSolicitudEnviada.Text = "Ticket generado satisfactoriamente con el numero: " + NUMERODETICKET + ".";
                                    lblSolicitudEnviada.Visible = true;
                                    picSolicitudEnviada.Visible = true;

                                    if (RequestCertificate.UpdateRequestStatusTicket(REQUESTID, NUMERODETICKET, (int)RequestStatusList.PENDIENTESOLICITUD))
                                    {
                                        ssForm.CloseWaitForm();
                                        GuardarSeguimientoCertificado(REQUESTID, (int)RequestStatusList.PENDIENTESOLICITUD, (int)RequestStatusList.PENDIENTESOLICITUD);
                                        XtraMessageBox.Show("Ticket generado satisfactoriamente con el numero:." + NUMERODETICKET + ".", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        btnEnviarSolicitud.Enabled = false;
                                        btnEnviarFactura.Enabled = true;
                                    }
                                    else
                                    {
                                        XtraMessageBox.Show("Hubo un error al solicitar el ticket. Intente en otro momento.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        Log.InsertarLog(Log.ErrorType.Error, "frmEnvioSolicitud::GenerateTicket", "Error", VariablesGlobales.PathDataLog);
                                    }
                                }
                                else
                                {
                                    XtraMessageBox.Show("Hubo un error al tratar de crear el cliente en GP. Intente nuevamente en otro momento.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    Log.InsertarLog(Log.ErrorType.Error, "frmEnvioSolicitud::GenerateTicket", resultGp.ToString(), VariablesGlobales.PathDataLog);
                                    return;
                                }
                            }
                            else
                            {
                                XtraMessageBox.Show("Hubo un error al crear el cliente en G1. Intente en otro momento.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                Log.InsertarLog(Log.ErrorType.Error, "frmEnvioSolicitud::GenerateTicket", "Error", VariablesGlobales.PathDataLog);
                                Close();
                            }
                        }

                        frmBandeja ActualizarSolicitudes = (frmBandeja)Application.OpenForms["frmBandeja"];
                        ActualizarSolicitudes.CargarSolicitudesAprobadas();
                        Close();
                    }
                    else
                    {
                        if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();

                        JObject jObj = (JObject)JsonConvert.DeserializeObject(result);
                        foreach (var it in jObj)
                        {
                            if(it.Key == "messages" | it.Key == "message") mensajeError = it.Value.ToString().Replace("[", "").Replace("]", "");
                        }

                        XtraMessageBox.Show("Hubo un error al tratar de generar el ticket en VALID." + Environment.NewLine + "El mansaje es:" + mensajeError + "Intente nuevamente en otro momento. ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Log.InsertarLog(Log.ErrorType.Error, "frmEnvioSolicitud::GenerateTicket", result.ToString(), VariablesGlobales.PathDataLog);
                        return;
                    }
                }
                else
                {
                    //if (rspValid.errorList != null)
                    //    foreach (JToken fundingSource in rspValid.SelectToken("errorList")) mensajeError = (string)fundingSource.SelectToken("message");

                    //if (mensajeError != string.Empty) mensajeError = "--> " + mensajeError + " <--";

                    JObject jObj = (JObject)JsonConvert.DeserializeObject(result);
                    foreach (var it in jObj)
                    {
                        if (it.Key == "errorList") mensajeError = it.Value.ToString();
                    }

                    XtraMessageBox.Show("Hubo un error al tratar de generar el ticket. Intente nuevamente en otro momento. " + Environment.NewLine + mensajeError, "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Log.InsertarLog(Log.ErrorType.Error, "frmEnvioSolicitud::GenerateTicket", result, VariablesGlobales.PathDataLog);
                    return;
                }
            }
            catch (Exception ex)
            {
                if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                XtraMessageBox.Show("Hubo un error al tratar de generar el ticket. Intente nuevamente en otro momento. ", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmEnvioSolicitud::GenerateTicket", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        void CrearFactura(JArray Persona)
        {
            try
            {
                //**INFORMACIÓN PARA GP - CREACIÓN DE LA FACTURA**
                wsFunction = new GP.DatosGP();
                string OtrosCLientes = string.Empty;
                string ClientePrincipal = string.Empty;
                JArray requestById = new JArray();
                requestById = RequestCertificate.GetRequestById(REQUESTID);

                if (requestById.Count > 0)
                    OtrosCLientes = RequestCertificate.GetItemFromJArray(requestById, "DocumentNumberCustomer").ToString();

                if (TIPOPERSONA == (int)TypePerson.PRSNATURAL)
                    ClientePrincipal = RequestCertificate.GetItemFromJArray(Persona, "DocumentNumberNaturalPerson").ToString();
                else ClientePrincipal = RequestCertificate.GetItemFromJArray(Persona, "DocumentNumberLegalPerson").ToString();


                //***Crear Encabezado***
                GP.clsCotizacion EncabezadoCotizacion = new GP.clsCotizacion();
                EncabezadoCotizacion.LOCNCODE = VariablesGlobales.OFFICEIDGP.ToString();
                EncabezadoCotizacion.DOCDATE = Convert.ToDateTime(LoginWebService.getStatusConecction());
                EncabezadoCotizacion.CUSTNMBR = (OtrosCLientes != string.Empty) ? OtrosCLientes : ClientePrincipal;
                EncabezadoCotizacion.SLPRSNID = VariablesGlobales.AGAUTORITY;
                EncabezadoCotizacion.OfinicaRegistroPrincipal_Enca = VariablesGlobales.OFFICEDESCRIPTIONGP;

                var resultEncabezadoGp = wsFunction.EncabezadoCotizacion(EncabezadoCotizacion);
                if (resultEncabezadoGp == null | resultEncabezadoGp == "False")
                {
                    XtraMessageBox.Show("Hubo un error al tratar de crear el Encabezado de la factura en GP.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Log.InsertarLog(Log.ErrorType.Error, "frmEnvioSolicitud::CrearFactura", resultEncabezadoGp.ToString(), VariablesGlobales.PathDataLog);
                    return;
                }

                //***Crear Detalle***
                GP.clsCotizacionDetalle dtCotizacion = new GP.clsCotizacionDetalle();
                dtCotizacion.SOPNUMBE = resultEncabezadoGp.ToString().Trim();
                dtCotizacion.CUSTNMBR = (OtrosCLientes != string.Empty) ? OtrosCLientes : ClientePrincipal;
                dtCotizacion.DOCDATE = Convert.ToDateTime(LoginWebService.getStatusConecction());
                dtCotizacion.LOCNCODE = VariablesGlobales.OFFICEIDGP.ToString();
                dtCotizacion.SLPRSNID = VariablesGlobales.AGAUTORITY;
                dtCotizacion.ITEMNMBR = RequestCertificate.GetItemFromJArray(ARRAYREQUEST, "CodigoProducto").ToString();
                dtCotizacion.QUANTITY = 1;

                var resultDetalleGp = wsFunction.DetalleCotizacion(dtCotizacion);
                if (resultDetalleGp == null | resultDetalleGp == "False")
                {
                    XtraMessageBox.Show("Hubo un error al tratar de crear el Detalle de la factura en GP.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Log.InsertarLog(Log.ErrorType.Error, "frmEnvioSolicitud::CrearFactura", resultDetalleGp.ToString(), VariablesGlobales.PathDataLog);
                    return;
                }

                //***Crear Impuesto***
                GP.clsCotizacion imptCotizacion = new GP.clsCotizacion();
                imptCotizacion.SOPNUMBE = resultEncabezadoGp.ToString().Trim();
                imptCotizacion.LOCNCODE = VariablesGlobales.OFFICEIDGP.ToString();
                imptCotizacion.DOCDATE = Convert.ToDateTime(LoginWebService.getStatusConecction());
                imptCotizacion.CUSTNMBR = (OtrosCLientes != string.Empty) ? OtrosCLientes : ClientePrincipal;
                imptCotizacion.SLPRSNID = VariablesGlobales.AGAUTORITY;
                imptCotizacion.Item_Imput = RequestCertificate.GetItemFromJArray(ARRAYREQUEST, "CodigoProducto").ToString();
                imptCotizacion.OfinicaRegistroPrincipal_Enca = VariablesGlobales.OFFICEDESCRIPTIONGP;

                var resultImpuestotGp = wsFunction.ImpuestoCotizacion(imptCotizacion);
                if (resultImpuestotGp == null | resultImpuestotGp == "False")
                {
                    XtraMessageBox.Show("Hubo un error al tratar de crear el Impuesto de la factura en GP.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Log.InsertarLog(Log.ErrorType.Error, "frmEnvioSolicitud::CrearFactura", resultImpuestotGp.ToString(), VariablesGlobales.PathDataLog);
                    btnEnviarSolicitud.Enabled = false;
                    return;
                }
                else
                {
                    if (!RequestCertificate.UpdateRequestOrderNumber(REQUESTID, resultEncabezadoGp.ToString().Trim(), (int)TransactionStatus.FACTURAENVIADA))
                    {
                        Log.InsertarLog(Log.ErrorType.Error, "frmEnvioSolicitud::CrearFactura", "Error al actualizar la solicitud con la factura", VariablesGlobales.PathDataLog);
                        btnEnviarSolicitud.Enabled = false;
                    }
                    else
                    {
                        XtraMessageBox.Show("Factura enviada exitosamente.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        GuardarSeguimientoSolicitud(REQUESTID, (int)TransactionStatus.FACTURAENVIADA, (int)TransactionStatus.SOLICITUDAPROBADA);
                        frmBandeja ActualizarSolicitudes = (frmBandeja)Application.OpenForms["frmBandeja"];
                        ActualizarSolicitudes.CargarSolicitudesAprobadas();
                        btnEnviarFactura.Enabled = false;
                        btnEnviarSolicitud.Enabled = false;
                    }
                }
            }
            catch (Exception ex)
            {
                if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                XtraMessageBox.Show("Hubo un error al tratar de crear la factura. Intente nuevamente en otro momento.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmEnvioSolicitud::CrearFactura", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        void GenerateRequest()
        {
            try
            {
                //**CREAR JSON PARA ENVÍO DE LA SOLICITUD A VALID**
                JObject json = new JObject();
                RequestCertificate.Client requestClient = new RequestCertificate.Client();
                RequestCertificate.ClientInfo requestClientPn = new RequestCertificate.ClientInfo();
                RequestCertificate.ClientPj requestClientPj = new RequestCertificate.ClientPj();
                RequestCertificate.certOf crtOffie = new RequestCertificate.certOf();
                RequestCertificate.raOperator raOperator = new RequestCertificate.raOperator();
                RequestCertificate.AgentAut ar = new RequestCertificate.AgentAut();
                RequestCertificate.Product pr = new RequestCertificate.Product();
                
                //***Adjuntos***
                RequestCertificate.DossiePrincipal dsRoot = new RequestCertificate.DossiePrincipal();
                RequestCertificate.Dossie dsChildren = new RequestCertificate.Dossie();

                crtOffie.code = VariablesGlobales.CERTOFFICE;
                raOperator.code = VariablesGlobales.AGAUTORITY;
                ar.code = VariablesGlobales.RGAURORITY;

                //***Propiedades del JSON***
                JProperty language = new JProperty("language", VariablesGlobales.IDIOMA.ToLower());
                JProperty office = new JProperty("certificateOffice", (JObject)JToken.FromObject(crtOffie));
                JProperty Operator = new JProperty("raOperator", (JObject)JToken.FromObject(raOperator));
                JProperty Ar = new JProperty("ar", (JObject)JToken.FromObject(ar));
                JProperty ticket = new JProperty("ticket", NUMERODETICKETFINAL);

                requestClient.zipCode = VariablesGlobales.OFFICEZIPCODE;
                requestClient.country = RequestCertificate.GetItemFromJArray(ARRAYREQUEST, "Alpha2").ToString();
                requestClient.state = RequestCertificate.GetItemFromJArray(ARRAYREQUEST, "FKStateCountryId").ToString();
                requestClient.city = RequestCertificate.GetItemFromJArray(ARRAYREQUEST, "FKStateCityId").ToString();
                requestClient.address = RequestCertificate.VerificarCaracteresEspeciales(RequestCertificate.GetItemFromJArray(ARRAYREQUEST, "Address").ToString());
                requestClient.addressNumber = "d";
                requestClient.addressAdditionalInfo = "d";
                requestClient.neighborhood = "d";
                requestClient.cellPhoneDDD = "00";
                requestClient.cellPhoneNumber = RequestCertificate.GetItemFromJArray(ARRAYREQUEST, "CellPhonePersonal").ToString().Replace("-", "");
                requestClient.phoneDDD = "00";
                requestClient.phoneNumber = RequestCertificate.GetItemFromJArray(ARRAYREQUEST, "CellPhonePersonal").ToString();

                //**Tipo de persona información adicional**
                JArray jArrayPn = new JArray();
                if (TIPOPERSONA == (int)TypePerson.PRSNATURAL)
                {
                    pr.sku = RequestCertificate.GetItemFromJArray(ARRAYREQUEST, "CodigoProducto").ToString().Trim();
                    requestClient.email = RequestCertificate.GetItemFromJArray(ARRAYREQUEST, "Email").ToString();
                    requestClient.emailConfirmation = RequestCertificate.GetItemFromJArray(ARRAYREQUEST, "Email").ToString();

                    requestClientPn.name = RequestCertificate.GetItemFromJArray(ARRAYREQUEST, "Names").ToString().Trim() + " " + RequestCertificate.GetItemFromJArray(ARRAYREQUEST, "LastNames").ToString().Trim();
                    requestClientPn.document = RequestCertificate.GetItemFromJArray(ARRAYREQUEST, "DocumentNumberNaturalPerson").ToString();
                    requestClientPn.documentType = RequestCertificate.GetItemFromJArray(ARRAYREQUEST, "CodigoDocumento").ToString();
                    requestClientPn.email = RequestCertificate.GetItemFromJArray(ARRAYREQUEST, "Email").ToString();
                    requestClientPn.profession = RequestCertificate.GetItemFromJArray(ARRAYREQUEST, "Profession").ToString();
                    requestClientPn.professionalCardNumber = RequestCertificate.GetItemFromJArray(ARRAYREQUEST, "RegistrationNumber").ToString();
                    requestClientPn.institutionAwardedDegress = RequestCertificate.GetItemFromJArray(ARRAYREQUEST, "InstitutionIssues").ToString();

                    jArrayPn.Add((JObject)JToken.FromObject(requestClientPn));
                    requestClient.requestClientInfo = jArrayPn;
                }
                else
                {
                    pr.sku = RequestCertificate.GetItemFromJArray(ARRAYREQUEST, "CodigoProducto").ToString();
                    requestClient.email = RequestCertificate.GetItemFromJArray(ARRAYREQUEST, "Email").ToString();
                    requestClient.emailConfirmation = RequestCertificate.GetItemFromJArray(ARRAYREQUEST, "Email").ToString();

                    requestClientPn.name = RequestCertificate.GetItemFromJArray(ARRAYREQUEST, "Names").ToString() + " " + RequestCertificate.GetItemFromJArray(ARRAYREQUEST, "LastNames").ToString();
                    requestClientPn.document = RequestCertificate.GetItemFromJArray(ARRAYREQUEST, "DocumentNumberNaturalPerson").ToString();
                    requestClientPn.documentType = RequestCertificate.GetItemFromJArray(ARRAYREQUEST, "CodigoDocumento").ToString();
                    requestClientPn.email = RequestCertificate.GetItemFromJArray(ARRAYREQUEST, "Email").ToString();
                    jArrayPn.Add((JObject)JToken.FromObject(requestClientPn));

                    requestClient.requestClientInfo = jArrayPn;

                    requestClientPj.name = RequestCertificate.GetItemFromJArray(ARRAYREQUEST, "NameLegalPerson").ToString();
                    requestClientPj.document = RequestCertificate.GetItemFromJArray(ARRAYREQUEST, "DocumentNumberLegalPerson").ToString();
                    requestClientPj.documentType = RequestCertificate.GetItemFromJArray(ARRAYREQUEST, "CodigoDocumentoLegal").ToString();

                    requestClient.requestClientPj = requestClientPj;
                }

                //***Agregar adjuntos***
                JArray jArrayDoss = new JArray();
                ListadoFinalAdjuntos.Clear();
                if (GetAttached())
                {
                    if (ListadoFinalAdjuntos.Count > 0)
                    {
                        foreach (var adj in ListadoFinalAdjuntos)
                        {
                            dsChildren.dateCreate = adj.Fecha;
                            dsChildren.description = adj.Descripcion;
                            dsChildren.docto = adj.Archivo;
                            dsChildren.hash = adj.Hash;
                            jArrayDoss.Add((JObject)JToken.FromObject(dsChildren));
                        }
                    }
                }
                dsRoot.dossie = jArrayDoss;

                JProperty doss = new JProperty("dossie", dsRoot.dossie);
                JProperty product = new JProperty("product", (JObject)JToken.FromObject(pr));
                JProperty propRequestPn = new JProperty("requestClient", (JObject)JToken.FromObject(requestClient));

                //**Encriptar con Sha1**
                string password = RequestCertificate.GetItemFromJArray(ARRAYREQUEST, "PasswordClient").ToString();
                JProperty encript = new JProperty("passwordRequest", password);
                JProperty encriptConfirmation = new JProperty("passwordRequestConfirmation", password.ToString());
                JProperty previous = new JProperty("ticketAnterior", "");

                //**Agregar los objecto al JSON**
                json.Add(language);
                json.Add(office);
                json.Add(Operator);
                json.Add(Ar);
                json.Add(ticket);
                json.Add(product);
                json.Add(propRequestPn);
                json.Add(doss);
                json.Add(encript);
                json.Add(encriptConfirmation);
                json.Add(previous);

                //**Salidas**
                string jsonOut = JsonConvert.SerializeObject(json, Formatting.None);
                File.WriteAllText(VariablesGlobales.PathDataSystem + "jsonRequest.json", jsonOut);

                string result = string.Empty;
                dynamic rspValid = null;
                result = ResponseRequestValid(jsonOut, (int)TransactionType.GENERATEREQUEST);
                if (result != string.Empty)
                {
                    rspValid = JObject.Parse(result);
                    if (rspValid.success == "true")
                    {
                        lblSolicitudEnviada.Text = "Solicitud enviada satisfactoriamente.";
                        picSolicitudEnviada.Visible = true;
                        lblSolicitudEnviada.Visible = true;

                        if (RequestCertificate.UpdateRequestSend((int)TransactionStatus.FACTURAPAGADA, (int)RequestStatusList.SOLICITUDCOMPROBADA, REQUESTID))
                        {
                            GuardarSeguimientoCertificado(REQUESTID, (int)RequestStatusList.SOLICITUDCOMPROBADA, (int)RequestStatusList.PENDIENTESOLICITUD);
                            if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                            frmBandeja ActualizarSolicitudes = (frmBandeja)Application.OpenForms["frmBandeja"];
                            ActualizarSolicitudes.CargarSolicitudesAprobadas();
                            XtraMessageBox.Show("Solicitud enviada satisfactoriamente.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            btnEnviarSolicitud.Enabled = false;
                            Close();
                        }
                        else
                        {
                            XtraMessageBox.Show("Hubo un error al actualizar el estado de la solicitud.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            Log.InsertarLog(Log.ErrorType.Error, "frmEnvioSolicitud::GenerateRequest()", "Error de red", VariablesGlobales.PathDataLog);
                        }
                    }
                    else
                    {
                        if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                        string mensajeError = string.Empty;
                        if (rspValid.errorList != null)
                              foreach (JToken fundingSource in rspValid.SelectToken("errorList")) mensajeError = (string)fundingSource.SelectToken("message");

                        XtraMessageBox.Show("Hubo un error al tratar de enviar la solicitud. " + Environment.NewLine + mensajeError, "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Log.InsertarLog(Log.ErrorType.Error, mensajeError, "Error", VariablesGlobales.PathDataLog);
                    }
                }
                else Init();
            }
            catch (Exception ex)
            {
                if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                XtraMessageBox.Show("Hubo un error al tratar de enviar la solicitud. ", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmEnvioSolicitud::GenerateRequest()", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }

        }

        void GuardarSeguimientoSolicitud(int rqId, int status, int statusPrevious)
        {
            //**GUARDAR SEGUIMIENTO**
            EN.RequestFollow rqf = new EN.RequestFollow();
            rqf.Observation = "";
            rqf.FKRequestId = rqId;
            rqf.FKTransactionEstatusId = status;
            rqf.FKCreateUserId = Convert.ToInt32(VariablesGlobales.USER_ID);
            rqf.FKOfficeId = Convert.ToInt32(VariablesGlobales.OFFICE_ID);
            rqf.FKTransactionStatusPrevious = statusPrevious;
            rqf.FKWorkstationId = Convert.ToInt32(VariablesGlobales.WS_ID);
            var insertRqFallow = RequestCertificate.InsertNewRequestFollow(rqf);
            if (!insertRqFallow)
            {
                Log.InsertarLog(Log.ErrorType.Error, "frmEnvioSolicitud::GuardarSeguimientoSolicitud", "Error al guardar el seguimiento.", VariablesGlobales.PathDataLog);
            }
        }

        void GuardarSeguimientoCertificado(int rqId, int status, int statusPrevious)
        {
            //**GUARDAR SEGUIMIENTO DEL CERTIFICADO**
            EN.RequestFollowValid rqVd = new EN.RequestFollowValid();
            rqVd.Observation = "";
            rqVd.FKRequestId = rqId;
            rqVd.FKRequestStatusId = status;
            rqVd.FKCreateUserId = Convert.ToInt32(VariablesGlobales.USER_ID);
            rqVd.FKOfficeId = Convert.ToInt32(VariablesGlobales.OFFICE_ID);
            rqVd.FKRequestStatusPreviousId = statusPrevious;
            rqVd.FKWorkstationId = Convert.ToInt32(VariablesGlobales.WS_ID);
            var insertRqFallow = RequestCertificate.InsertNewRequestFollowValid(rqVd);
            if (!insertRqFallow)
            {
                Log.InsertarLog(Log.ErrorType.Error, "frmEnvioSolicitud::GuardarSeguimientoCertificado", "Error al guardar el seguimiento.", VariablesGlobales.PathDataLog);
            }
        }

        public string ResponseRequestValid(string json, int transaction)
        {
            string result = string.Empty;
            string url = string.Empty;
            if(transaction == (int)TransactionType.GENERATETICKET)
                url = "https://agenda.tecnisign.net/scheduler-rest/loadOrderGlobal";
            else url = "http://ar.tecnisign.net/ra-ws-rest/applicantGlobalRequest";

            //    url = "http://hml-agenda-global.validcertificadora.com.br/scheduler-rest/loadOrderGlobal";
            //else url = "http://hml-ar-global.validcertificadora.com.br/ra-ws-rest/applicantGlobalRequest";

            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.ProtocolVersion = HttpVersion.Version10;
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";
            httpWebRequest.Timeout = 180000;

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                streamWriter.Write(json);

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();

            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                result = streamReader.ReadToEnd();

            return result;
        }

        bool GetAttached()
        {
            try
            {
                JArray arAttached = new JArray();
                arAttached = RequestCertificate.GetAdjuntoByRequestId(REQUESTID);
                if (arAttached.Count > 0)
                {
                    foreach (JObject elementImg in arAttached)
                    {
                        if (elementImg["Folder"].ToString() != null)
                        {
                            string path = @"C:\";
                            string strRelativePath = path + elementImg["Folder"].ToString();
                            string file = Path.GetDirectoryName(strRelativePath);
                            string result = RequestCertificate.GetRequestAttachImg(strRelativePath, file);
                            if (result != null)
                            {
                                EN.Dossie ds = new EN.Dossie();
                                ds.Archivo = result;
                                byte[] byteFile = Convert.FromBase64String(ds.Archivo);
                                ds.Fecha = elementImg["CreationDate"].ToString();
                                ds.Descripcion = elementImg["NombreDocumento"].ToString() + "." + elementImg["Type"].ToString();
                                ds.Hash = RequestCertificate.EncodeJsonBytes(byteFile);
                                ListadoAdjuntos.Add(ds);
                            }
                        }
                    }
                    ListadoFinalAdjuntos = ListadoAdjuntos;
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                Log.InsertarLog(Log.ErrorType.Error, "frmEnvioSolicitud::GetAttached", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                return false;
            }
        }

        #endregion

       
    }

}