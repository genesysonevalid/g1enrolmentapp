﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Columns;
using DevExpress.Utils.FormatStrings;
using DevExpress.Office.Crypto;
using Gv.Utilidades;

namespace Gv.ExodusBc.UI.Modulos.Certificados
{
    public partial class frmExitenciaProducto : XtraForm
    {
        public static DataSet dsProductos = new DataSet();

        public frmExitenciaProducto()
        {
            InitializeComponent();
        }

        private void frmExitenciaProducto_Load(object sender, EventArgs e)
        {
            DataTable dtAll = dsProductos.Tables[0].Copy();
            gvStockProductosPn.OptionsBehavior.AutoPopulateColumns = true;
            gvStockProductosPn.OptionsView.BestFitMode = DevExpress.XtraGrid.Views.Grid.GridBestFitMode.Fast;
             gcStockProductosPn.DataSource = dtAll;
            gvStockProductosPn.Columns[3].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            gvStockProductosPn.Columns[3].DisplayFormat.FormatString = "n0";
            gvStockProductosPn.Appearance.OddRow.BackColor = Color.FromArgb(241, 249, 247);
            gvStockProductosPn.OptionsView.EnableAppearanceOddRow = true;
        }

    }
}