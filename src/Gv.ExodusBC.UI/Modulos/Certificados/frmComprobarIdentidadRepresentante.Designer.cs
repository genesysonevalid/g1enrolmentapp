﻿namespace Gv.ExodusBc.UI.Modulos.Certificados
{
    partial class frmComprobarIdentidadRepresentante
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmComprobarIdentidadRepresentante));
            this.picRepresentante = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.lblNacionalidad = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.lblNombres = new DevExpress.XtraEditors.LabelControl();
            this.lblIdentificacion = new DevExpress.XtraEditors.LabelControl();
            this.btnValidarPersona = new DevExpress.XtraEditors.SimpleButton();
            this.pnlVerificacionHuellas = new DevExpress.XtraEditors.PanelControl();
            this.lblGifValidando = new DevExpress.XtraEditors.LabelControl();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.lblEstadoValidacion = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.btnAceptar = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.lblEstadoCivil = new DevExpress.XtraEditors.LabelControl();
            this.Profesion = new DevExpress.XtraEditors.LabelControl();
            this.lblProfesion = new DevExpress.XtraEditors.LabelControl();
            this.txtCargo = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.picRepresentante.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlVerificacionHuellas)).BeginInit();
            this.pnlVerificacionHuellas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCargo.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // picRepresentante
            // 
            this.picRepresentante.Cursor = System.Windows.Forms.Cursors.Default;
            this.picRepresentante.EditValue = global::Gv.ExodusBc.UI.Properties.Resources.SinFoto;
            this.picRepresentante.Location = new System.Drawing.Point(552, 58);
            this.picRepresentante.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.picRepresentante.Name = "picRepresentante";
            this.picRepresentante.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picRepresentante.Properties.Appearance.Options.UseBackColor = true;
            this.picRepresentante.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.picRepresentante.Size = new System.Drawing.Size(187, 233);
            this.picRepresentante.TabIndex = 450;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Appearance.Options.UseForeColor = true;
            this.labelControl3.Appearance.Options.UseTextOptions = true;
            this.labelControl3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl3.Location = new System.Drawing.Point(14, 247);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(143, 23);
            this.labelControl3.TabIndex = 458;
            this.labelControl3.Text = "Nacionalidad:";
            // 
            // lblNacionalidad
            // 
            this.lblNacionalidad.Appearance.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNacionalidad.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblNacionalidad.Appearance.Options.UseFont = true;
            this.lblNacionalidad.Appearance.Options.UseForeColor = true;
            this.lblNacionalidad.Appearance.Options.UseTextOptions = true;
            this.lblNacionalidad.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblNacionalidad.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblNacionalidad.LineColor = System.Drawing.Color.DimGray;
            this.lblNacionalidad.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblNacionalidad.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblNacionalidad.LineVisible = true;
            this.lblNacionalidad.Location = new System.Drawing.Point(14, 270);
            this.lblNacionalidad.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblNacionalidad.Name = "lblNacionalidad";
            this.lblNacionalidad.Size = new System.Drawing.Size(520, 33);
            this.lblNacionalidad.TabIndex = 459;
            this.lblNacionalidad.Text = "[lblNacionalidad]";
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl7.Appearance.Options.UseFont = true;
            this.labelControl7.Appearance.Options.UseForeColor = true;
            this.labelControl7.Appearance.Options.UseTextOptions = true;
            this.labelControl7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl7.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl7.Location = new System.Drawing.Point(14, 15);
            this.labelControl7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(143, 23);
            this.labelControl7.TabIndex = 454;
            this.labelControl7.Text = "Identificación:";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Appearance.Options.UseForeColor = true;
            this.labelControl1.Appearance.Options.UseTextOptions = true;
            this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.Location = new System.Drawing.Point(14, 68);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(143, 23);
            this.labelControl1.TabIndex = 456;
            this.labelControl1.Text = "Nombres:";
            // 
            // lblNombres
            // 
            this.lblNombres.Appearance.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombres.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblNombres.Appearance.Options.UseFont = true;
            this.lblNombres.Appearance.Options.UseForeColor = true;
            this.lblNombres.Appearance.Options.UseTextOptions = true;
            this.lblNombres.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblNombres.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblNombres.LineColor = System.Drawing.Color.DimGray;
            this.lblNombres.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblNombres.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblNombres.LineVisible = true;
            this.lblNombres.Location = new System.Drawing.Point(14, 91);
            this.lblNombres.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblNombres.Name = "lblNombres";
            this.lblNombres.Size = new System.Drawing.Size(520, 30);
            this.lblNombres.TabIndex = 457;
            this.lblNombres.Text = "[lblNombres]";
            // 
            // lblIdentificacion
            // 
            this.lblIdentificacion.Appearance.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIdentificacion.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblIdentificacion.Appearance.Options.UseFont = true;
            this.lblIdentificacion.Appearance.Options.UseForeColor = true;
            this.lblIdentificacion.Appearance.Options.UseTextOptions = true;
            this.lblIdentificacion.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblIdentificacion.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblIdentificacion.LineColor = System.Drawing.Color.DimGray;
            this.lblIdentificacion.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblIdentificacion.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblIdentificacion.LineVisible = true;
            this.lblIdentificacion.Location = new System.Drawing.Point(14, 38);
            this.lblIdentificacion.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblIdentificacion.Name = "lblIdentificacion";
            this.lblIdentificacion.Size = new System.Drawing.Size(520, 30);
            this.lblIdentificacion.TabIndex = 455;
            this.lblIdentificacion.Text = "[lblIdentificacion]";
            // 
            // btnValidarPersona
            // 
            this.btnValidarPersona.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.btnValidarPersona.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnValidarPersona.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.btnValidarPersona.Appearance.Options.UseBackColor = true;
            this.btnValidarPersona.Appearance.Options.UseFont = true;
            this.btnValidarPersona.Appearance.Options.UseForeColor = true;
            this.btnValidarPersona.Appearance.Options.UseTextOptions = true;
            this.btnValidarPersona.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnValidarPersona.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnValidarPersona.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_fingerscan_24x24;
            this.btnValidarPersona.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnValidarPersona.Location = new System.Drawing.Point(15, 443);
            this.btnValidarPersona.LookAndFeel.SkinName = "Whiteprint";
            this.btnValidarPersona.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnValidarPersona.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnValidarPersona.Name = "btnValidarPersona";
            this.btnValidarPersona.Size = new System.Drawing.Size(162, 34);
            this.btnValidarPersona.TabIndex = 462;
            this.btnValidarPersona.Text = "&Validar persona";
            this.btnValidarPersona.Visible = false;
            this.btnValidarPersona.Click += new System.EventHandler(this.btnValidarPersona_Click);
            // 
            // pnlVerificacionHuellas
            // 
            this.pnlVerificacionHuellas.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.pnlVerificacionHuellas.Appearance.Options.UseBackColor = true;
            this.pnlVerificacionHuellas.Appearance.Options.UseBorderColor = true;
            this.pnlVerificacionHuellas.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlVerificacionHuellas.Controls.Add(this.lblGifValidando);
            this.pnlVerificacionHuellas.Controls.Add(this.labelControl22);
            this.pnlVerificacionHuellas.Controls.Add(this.lblEstadoValidacion);
            this.pnlVerificacionHuellas.Location = new System.Drawing.Point(183, 443);
            this.pnlVerificacionHuellas.LookAndFeel.SkinName = "Foggy";
            this.pnlVerificacionHuellas.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.pnlVerificacionHuellas.LookAndFeel.UseDefaultLookAndFeel = false;
            this.pnlVerificacionHuellas.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pnlVerificacionHuellas.Name = "pnlVerificacionHuellas";
            this.pnlVerificacionHuellas.Size = new System.Drawing.Size(555, 34);
            this.pnlVerificacionHuellas.TabIndex = 463;
            this.pnlVerificacionHuellas.Visible = false;
            // 
            // lblGifValidando
            // 
            this.lblGifValidando.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lblGifValidando.Appearance.BackColor2 = System.Drawing.Color.Transparent;
            this.lblGifValidando.Appearance.Image = global::Gv.ExodusBc.UI.Properties.Resources.loading;
            this.lblGifValidando.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblGifValidando.Appearance.Options.UseBackColor = true;
            this.lblGifValidando.Appearance.Options.UseImage = true;
            this.lblGifValidando.Appearance.Options.UseImageAlign = true;
            this.lblGifValidando.Location = new System.Drawing.Point(197, 10);
            this.lblGifValidando.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblGifValidando.Name = "lblGifValidando";
            this.lblGifValidando.Size = new System.Drawing.Size(50, 16);
            this.lblGifValidando.TabIndex = 2;
            this.lblGifValidando.Visible = false;
            // 
            // labelControl22
            // 
            this.labelControl22.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl22.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl22.Appearance.Options.UseFont = true;
            this.labelControl22.Appearance.Options.UseForeColor = true;
            this.labelControl22.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl22.Location = new System.Drawing.Point(7, 6);
            this.labelControl22.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(184, 22);
            this.labelControl22.TabIndex = 1;
            this.labelControl22.Text = "Verificación de huellas:";
            // 
            // lblEstadoValidacion
            // 
            this.lblEstadoValidacion.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstadoValidacion.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblEstadoValidacion.Appearance.Options.UseFont = true;
            this.lblEstadoValidacion.Appearance.Options.UseForeColor = true;
            this.lblEstadoValidacion.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblEstadoValidacion.Location = new System.Drawing.Point(279, 6);
            this.lblEstadoValidacion.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblEstadoValidacion.Name = "lblEstadoValidacion";
            this.lblEstadoValidacion.Size = new System.Drawing.Size(270, 22);
            this.lblEstadoValidacion.TabIndex = 0;
            this.lblEstadoValidacion.Text = "No Iniciada";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Appearance.Options.UseForeColor = true;
            this.labelControl5.Appearance.Options.UseTextOptions = true;
            this.labelControl5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl5.LineColor = System.Drawing.Color.SteelBlue;
            this.labelControl5.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.labelControl5.LineVisible = true;
            this.labelControl5.Location = new System.Drawing.Point(14, 400);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(724, 33);
            this.labelControl5.TabIndex = 464;
            this.labelControl5.Text = "Validacion biométrica de la persona";
            this.labelControl5.Visible = false;
            // 
            // btnAceptar
            // 
            this.btnAceptar.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.btnAceptar.Appearance.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAceptar.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.btnAceptar.Appearance.Options.UseBackColor = true;
            this.btnAceptar.Appearance.Options.UseFont = true;
            this.btnAceptar.Appearance.Options.UseForeColor = true;
            this.btnAceptar.Appearance.Options.UseTextOptions = true;
            this.btnAceptar.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnAceptar.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnAceptar.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.check_box;
            this.btnAceptar.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnAceptar.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnAceptar.Location = new System.Drawing.Point(592, 542);
            this.btnAceptar.LookAndFeel.SkinName = "Whiteprint";
            this.btnAceptar.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnAceptar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(149, 35);
            this.btnAceptar.TabIndex = 468;
            this.btnAceptar.Text = "&Aceptar";
            this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl6.Appearance.Options.UseFont = true;
            this.labelControl6.Appearance.Options.UseForeColor = true;
            this.labelControl6.Appearance.Options.UseTextOptions = true;
            this.labelControl6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl6.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl6.LineColor = System.Drawing.Color.Gainsboro;
            this.labelControl6.LineLocation = DevExpress.XtraEditors.LineLocation.Top;
            this.labelControl6.LineVisible = true;
            this.labelControl6.Location = new System.Drawing.Point(14, 530);
            this.labelControl6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(724, 25);
            this.labelControl6.TabIndex = 469;
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl8.Appearance.Options.UseFont = true;
            this.labelControl8.Appearance.Options.UseForeColor = true;
            this.labelControl8.Appearance.Options.UseTextOptions = true;
            this.labelControl8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl8.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl8.Location = new System.Drawing.Point(14, 129);
            this.labelControl8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(143, 23);
            this.labelControl8.TabIndex = 470;
            this.labelControl8.Text = "Estado Civil:";
            // 
            // lblEstadoCivil
            // 
            this.lblEstadoCivil.Appearance.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstadoCivil.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblEstadoCivil.Appearance.Options.UseFont = true;
            this.lblEstadoCivil.Appearance.Options.UseForeColor = true;
            this.lblEstadoCivil.Appearance.Options.UseTextOptions = true;
            this.lblEstadoCivil.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblEstadoCivil.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblEstadoCivil.LineColor = System.Drawing.Color.DimGray;
            this.lblEstadoCivil.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblEstadoCivil.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblEstadoCivil.LineVisible = true;
            this.lblEstadoCivil.Location = new System.Drawing.Point(14, 152);
            this.lblEstadoCivil.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblEstadoCivil.Name = "lblEstadoCivil";
            this.lblEstadoCivil.Size = new System.Drawing.Size(520, 30);
            this.lblEstadoCivil.TabIndex = 471;
            this.lblEstadoCivil.Text = "[lblEstadoCivil]";
            // 
            // Profesion
            // 
            this.Profesion.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Profesion.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.Profesion.Appearance.Options.UseFont = true;
            this.Profesion.Appearance.Options.UseForeColor = true;
            this.Profesion.Appearance.Options.UseTextOptions = true;
            this.Profesion.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.Profesion.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.Profesion.Location = new System.Drawing.Point(14, 186);
            this.Profesion.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Profesion.Name = "Profesion";
            this.Profesion.Size = new System.Drawing.Size(143, 23);
            this.Profesion.TabIndex = 472;
            this.Profesion.Text = "Profesión:";
            // 
            // lblProfesion
            // 
            this.lblProfesion.Appearance.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProfesion.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblProfesion.Appearance.Options.UseFont = true;
            this.lblProfesion.Appearance.Options.UseForeColor = true;
            this.lblProfesion.Appearance.Options.UseTextOptions = true;
            this.lblProfesion.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblProfesion.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblProfesion.LineColor = System.Drawing.Color.DimGray;
            this.lblProfesion.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblProfesion.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblProfesion.LineVisible = true;
            this.lblProfesion.Location = new System.Drawing.Point(14, 209);
            this.lblProfesion.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblProfesion.Name = "lblProfesion";
            this.lblProfesion.Size = new System.Drawing.Size(520, 30);
            this.lblProfesion.TabIndex = 473;
            this.lblProfesion.Text = "[lblProfesion]";
            // 
            // txtCargo
            // 
            this.txtCargo.Location = new System.Drawing.Point(15, 334);
            this.txtCargo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtCargo.Name = "txtCargo";
            this.txtCargo.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCargo.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.txtCargo.Properties.Appearance.Options.UseFont = true;
            this.txtCargo.Properties.Appearance.Options.UseForeColor = true;
            this.txtCargo.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtCargo.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtCargo.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtCargo.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCargo.Size = new System.Drawing.Size(723, 32);
            this.txtCargo.TabIndex = 474;
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl10.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl10.Appearance.Options.UseFont = true;
            this.labelControl10.Appearance.Options.UseForeColor = true;
            this.labelControl10.Location = new System.Drawing.Point(16, 309);
            this.labelControl10.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(222, 20);
            this.labelControl10.TabIndex = 475;
            this.labelControl10.Text = "Cargo en la Empresa o Institución";
            // 
            // frmComprobarIdentidadRepresentante
            // 
            this.Appearance.BackColor = System.Drawing.Color.White;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(752, 601);
            this.Controls.Add(this.txtCargo);
            this.Controls.Add(this.labelControl10);
            this.Controls.Add(this.Profesion);
            this.Controls.Add(this.lblProfesion);
            this.Controls.Add(this.labelControl8);
            this.Controls.Add(this.lblEstadoCivil);
            this.Controls.Add(this.btnAceptar);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.pnlVerificacionHuellas);
            this.Controls.Add(this.btnValidarPersona);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.lblNacionalidad);
            this.Controls.Add(this.labelControl7);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.lblNombres);
            this.Controls.Add(this.lblIdentificacion);
            this.Controls.Add(this.picRepresentante);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmComprobarIdentidadRepresentante";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Comprobar identidad del representante";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmComprobarIdentidadRepresentante_FormClosing);
            this.Load += new System.EventHandler(this.frmComprobarIdentidadRepresentante_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picRepresentante.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlVerificacionHuellas)).EndInit();
            this.pnlVerificacionHuellas.ResumeLayout(false);
            this.pnlVerificacionHuellas.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCargo.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PictureEdit picRepresentante;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl lblNacionalidad;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl lblNombres;
        private DevExpress.XtraEditors.LabelControl lblIdentificacion;
        private DevExpress.XtraEditors.SimpleButton btnValidarPersona;
        private DevExpress.XtraEditors.PanelControl pnlVerificacionHuellas;
        private DevExpress.XtraEditors.LabelControl lblGifValidando;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.LabelControl lblEstadoValidacion;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.SimpleButton btnAceptar;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl lblEstadoCivil;
        private DevExpress.XtraEditors.LabelControl Profesion;
        private DevExpress.XtraEditors.LabelControl lblProfesion;
        private DevExpress.XtraEditors.TextEdit txtCargo;
        private DevExpress.XtraEditors.LabelControl labelControl10;
    }
}