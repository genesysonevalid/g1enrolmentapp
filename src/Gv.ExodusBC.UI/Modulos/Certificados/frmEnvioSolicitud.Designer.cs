﻿namespace Gv.ExodusBc.UI.Modulos.Certificados
{
    partial class frmEnvioSolicitud
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEnvioSolicitud));
            this.lblTituloAccion = new DevExpress.XtraEditors.LabelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.btnEnviarFactura = new DevExpress.XtraEditors.SimpleButton();
            this.btnEnviarSolicitud = new DevExpress.XtraEditors.SimpleButton();
            this.txtRtnCiente = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.lblNumeroTicketTitulo = new DevExpress.XtraEditors.LabelControl();
            this.lblNumeroTicket = new DevExpress.XtraEditors.LabelControl();
            this.picSolicitudEnviada = new DevExpress.XtraEditors.PictureEdit();
            this.lblSolicitudEnviada = new DevExpress.XtraEditors.LabelControl();
            this.lblTelefono = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.lblNombres = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.lblCorreo = new DevExpress.XtraEditors.LabelControl();
            this.lblNumeroDocumento = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.lblDescripcion = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.lblProducto = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.picPaso1 = new DevExpress.XtraEditors.PictureEdit();
            this.ssForm = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::Gv.ExodusBc.UI.frmWaitForm), true, true);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtRtnCiente.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSolicitudEnviada.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPaso1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTituloAccion
            // 
            this.lblTituloAccion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTituloAccion.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lblTituloAccion.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTituloAccion.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblTituloAccion.Appearance.Options.UseBackColor = true;
            this.lblTituloAccion.Appearance.Options.UseFont = true;
            this.lblTituloAccion.Appearance.Options.UseForeColor = true;
            this.lblTituloAccion.Appearance.Options.UseTextOptions = true;
            this.lblTituloAccion.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblTituloAccion.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblTituloAccion.LineColor = System.Drawing.Color.WhiteSmoke;
            this.lblTituloAccion.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblTituloAccion.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblTituloAccion.LineVisible = true;
            this.lblTituloAccion.Location = new System.Drawing.Point(28, 13);
            this.lblTituloAccion.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblTituloAccion.Name = "lblTituloAccion";
            this.lblTituloAccion.Size = new System.Drawing.Size(857, 46);
            this.lblTituloAccion.TabIndex = 120;
            this.lblTituloAccion.Text = "Trámite de la solicitud";
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.AppearanceCaption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.AppearanceCaption.Options.UseForeColor = true;
            this.groupControl1.AppearanceCaption.Options.UseTextOptions = true;
            this.groupControl1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.groupControl1.Controls.Add(this.btnEnviarFactura);
            this.groupControl1.Controls.Add(this.btnEnviarSolicitud);
            this.groupControl1.Controls.Add(this.txtRtnCiente);
            this.groupControl1.Controls.Add(this.labelControl8);
            this.groupControl1.Controls.Add(this.labelControl6);
            this.groupControl1.Controls.Add(this.lblNumeroTicketTitulo);
            this.groupControl1.Controls.Add(this.lblNumeroTicket);
            this.groupControl1.Controls.Add(this.picSolicitudEnviada);
            this.groupControl1.Controls.Add(this.lblSolicitudEnviada);
            this.groupControl1.Controls.Add(this.lblTelefono);
            this.groupControl1.Controls.Add(this.labelControl7);
            this.groupControl1.Controls.Add(this.lblNombres);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.lblCorreo);
            this.groupControl1.Controls.Add(this.lblNumeroDocumento);
            this.groupControl1.Controls.Add(this.labelControl5);
            this.groupControl1.Controls.Add(this.labelControl4);
            this.groupControl1.Controls.Add(this.lblDescripcion);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Controls.Add(this.lblProducto);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.picPaso1);
            this.groupControl1.Location = new System.Drawing.Point(16, 70);
            this.groupControl1.LookAndFeel.SkinMaskColor = System.Drawing.Color.AliceBlue;
            this.groupControl1.LookAndFeel.SkinName = "Whiteprint";
            this.groupControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.groupControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(892, 444);
            this.groupControl1.TabIndex = 312;
            this.groupControl1.Text = "      Información del ticket";
            // 
            // btnEnviarFactura
            // 
            this.btnEnviarFactura.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.btnEnviarFactura.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEnviarFactura.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.btnEnviarFactura.Appearance.Options.UseBackColor = true;
            this.btnEnviarFactura.Appearance.Options.UseFont = true;
            this.btnEnviarFactura.Appearance.Options.UseForeColor = true;
            this.btnEnviarFactura.Appearance.Options.UseTextOptions = true;
            this.btnEnviarFactura.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnEnviarFactura.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnEnviarFactura.Enabled = false;
            this.btnEnviarFactura.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_sendfile_32x32;
            this.btnEnviarFactura.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnEnviarFactura.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnEnviarFactura.Location = new System.Drawing.Point(492, 332);
            this.btnEnviarFactura.LookAndFeel.SkinName = "Whiteprint";
            this.btnEnviarFactura.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnEnviarFactura.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnEnviarFactura.Name = "btnEnviarFactura";
            this.btnEnviarFactura.Size = new System.Drawing.Size(165, 37);
            this.btnEnviarFactura.TabIndex = 460;
            this.btnEnviarFactura.Text = "&Enviar Factura";
            this.btnEnviarFactura.Click += new System.EventHandler(this.btnEnviarFactura_Click);
            // 
            // btnEnviarSolicitud
            // 
            this.btnEnviarSolicitud.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.btnEnviarSolicitud.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEnviarSolicitud.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.btnEnviarSolicitud.Appearance.Options.UseBackColor = true;
            this.btnEnviarSolicitud.Appearance.Options.UseFont = true;
            this.btnEnviarSolicitud.Appearance.Options.UseForeColor = true;
            this.btnEnviarSolicitud.Appearance.Options.UseTextOptions = true;
            this.btnEnviarSolicitud.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnEnviarSolicitud.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnEnviarSolicitud.Enabled = false;
            this.btnEnviarSolicitud.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_sendfile_32x32;
            this.btnEnviarSolicitud.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnEnviarSolicitud.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnEnviarSolicitud.Location = new System.Drawing.Point(215, 332);
            this.btnEnviarSolicitud.LookAndFeel.SkinName = "Whiteprint";
            this.btnEnviarSolicitud.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnEnviarSolicitud.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnEnviarSolicitud.Name = "btnEnviarSolicitud";
            this.btnEnviarSolicitud.Size = new System.Drawing.Size(161, 37);
            this.btnEnviarSolicitud.TabIndex = 2;
            this.btnEnviarSolicitud.Text = "&Solicitar ticket";
            this.btnEnviarSolicitud.Click += new System.EventHandler(this.btnEnviarSolicitud_Click);
            // 
            // txtRtnCiente
            // 
            this.txtRtnCiente.Location = new System.Drawing.Point(463, 253);
            this.txtRtnCiente.Margin = new System.Windows.Forms.Padding(2);
            this.txtRtnCiente.Name = "txtRtnCiente";
            this.txtRtnCiente.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRtnCiente.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.txtRtnCiente.Properties.Appearance.Options.UseFont = true;
            this.txtRtnCiente.Properties.Appearance.Options.UseForeColor = true;
            this.txtRtnCiente.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtRtnCiente.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtRtnCiente.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtRtnCiente.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtRtnCiente.Properties.Mask.EditMask = "0000-0000-000000";
            this.txtRtnCiente.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
            this.txtRtnCiente.Properties.MaxLength = 20;
            this.txtRtnCiente.Size = new System.Drawing.Size(360, 30);
            this.txtRtnCiente.TabIndex = 462;
            // 
            // labelControl8
            // 
            this.labelControl8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl8.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl8.Appearance.Options.UseBackColor = true;
            this.labelControl8.Appearance.Options.UseFont = true;
            this.labelControl8.Appearance.Options.UseForeColor = true;
            this.labelControl8.Appearance.Options.UseTextOptions = true;
            this.labelControl8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl8.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl8.LineColor = System.Drawing.Color.Gainsboro;
            this.labelControl8.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.labelControl8.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.labelControl8.LineVisible = true;
            this.labelControl8.Location = new System.Drawing.Point(29, 280);
            this.labelControl8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(805, 45);
            this.labelControl8.TabIndex = 463;
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl6.Appearance.Options.UseFont = true;
            this.labelControl6.Appearance.Options.UseForeColor = true;
            this.labelControl6.Appearance.Options.UseTextOptions = true;
            this.labelControl6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl6.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl6.Location = new System.Drawing.Point(463, 224);
            this.labelControl6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(360, 23);
            this.labelControl6.TabIndex = 461;
            this.labelControl6.Text = "RTN Cliente persona natural:";
            // 
            // lblNumeroTicketTitulo
            // 
            this.lblNumeroTicketTitulo.Appearance.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumeroTicketTitulo.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblNumeroTicketTitulo.Appearance.Options.UseFont = true;
            this.lblNumeroTicketTitulo.Appearance.Options.UseForeColor = true;
            this.lblNumeroTicketTitulo.Appearance.Options.UseTextOptions = true;
            this.lblNumeroTicketTitulo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblNumeroTicketTitulo.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblNumeroTicketTitulo.Location = new System.Drawing.Point(353, 6);
            this.lblNumeroTicketTitulo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblNumeroTicketTitulo.Name = "lblNumeroTicketTitulo";
            this.lblNumeroTicketTitulo.Size = new System.Drawing.Size(149, 23);
            this.lblNumeroTicketTitulo.TabIndex = 459;
            this.lblNumeroTicketTitulo.Text = "Numero de ticket:";
            // 
            // lblNumeroTicket
            // 
            this.lblNumeroTicket.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumeroTicket.Appearance.ForeColor = System.Drawing.Color.Teal;
            this.lblNumeroTicket.Appearance.Options.UseFont = true;
            this.lblNumeroTicket.Appearance.Options.UseForeColor = true;
            this.lblNumeroTicket.Appearance.Options.UseTextOptions = true;
            this.lblNumeroTicket.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblNumeroTicket.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblNumeroTicket.LineColor = System.Drawing.Color.DimGray;
            this.lblNumeroTicket.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblNumeroTicket.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblNumeroTicket.Location = new System.Drawing.Point(509, 0);
            this.lblNumeroTicket.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblNumeroTicket.Name = "lblNumeroTicket";
            this.lblNumeroTicket.Size = new System.Drawing.Size(325, 34);
            this.lblNumeroTicket.TabIndex = 458;
            this.lblNumeroTicket.Text = "[lblNumeroTicket]";
            // 
            // picSolicitudEnviada
            // 
            this.picSolicitudEnviada.Cursor = System.Windows.Forms.Cursors.Default;
            this.picSolicitudEnviada.EditValue = global::Gv.ExodusBc.UI.Properties.Resources.about_32x32;
            this.picSolicitudEnviada.Location = new System.Drawing.Point(2, 396);
            this.picSolicitudEnviada.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.picSolicitudEnviada.Name = "picSolicitudEnviada";
            this.picSolicitudEnviada.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picSolicitudEnviada.Properties.Appearance.Options.UseBackColor = true;
            this.picSolicitudEnviada.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.picSolicitudEnviada.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picSolicitudEnviada.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.picSolicitudEnviada.Size = new System.Drawing.Size(41, 41);
            this.picSolicitudEnviada.TabIndex = 457;
            this.picSolicitudEnviada.Visible = false;
            // 
            // lblSolicitudEnviada
            // 
            this.lblSolicitudEnviada.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSolicitudEnviada.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.lblSolicitudEnviada.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblSolicitudEnviada.Appearance.Options.UseFont = true;
            this.lblSolicitudEnviada.Appearance.Options.UseForeColor = true;
            this.lblSolicitudEnviada.Appearance.Options.UseImageAlign = true;
            this.lblSolicitudEnviada.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblSolicitudEnviada.Location = new System.Drawing.Point(49, 396);
            this.lblSolicitudEnviada.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblSolicitudEnviada.Name = "lblSolicitudEnviada";
            this.lblSolicitudEnviada.Size = new System.Drawing.Size(820, 41);
            this.lblSolicitudEnviada.TabIndex = 456;
            this.lblSolicitudEnviada.Text = "[lblSolicitudEnviada]";
            this.lblSolicitudEnviada.Visible = false;
            // 
            // lblTelefono
            // 
            this.lblTelefono.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTelefono.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblTelefono.Appearance.Options.UseFont = true;
            this.lblTelefono.Appearance.Options.UseForeColor = true;
            this.lblTelefono.Appearance.Options.UseTextOptions = true;
            this.lblTelefono.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblTelefono.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblTelefono.LineColor = System.Drawing.Color.DimGray;
            this.lblTelefono.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblTelefono.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblTelefono.LineVisible = true;
            this.lblTelefono.Location = new System.Drawing.Point(463, 190);
            this.lblTelefono.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblTelefono.Name = "lblTelefono";
            this.lblTelefono.Size = new System.Drawing.Size(360, 26);
            this.lblTelefono.TabIndex = 455;
            this.lblTelefono.Text = "[lblTelefono]";
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl7.Appearance.Options.UseFont = true;
            this.labelControl7.Appearance.Options.UseForeColor = true;
            this.labelControl7.Appearance.Options.UseTextOptions = true;
            this.labelControl7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl7.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl7.Location = new System.Drawing.Point(463, 164);
            this.labelControl7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(360, 23);
            this.labelControl7.TabIndex = 454;
            this.labelControl7.Text = "Teléfono:";
            // 
            // lblNombres
            // 
            this.lblNombres.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombres.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblNombres.Appearance.Options.UseFont = true;
            this.lblNombres.Appearance.Options.UseForeColor = true;
            this.lblNombres.Appearance.Options.UseTextOptions = true;
            this.lblNombres.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblNombres.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblNombres.LineColor = System.Drawing.Color.DimGray;
            this.lblNombres.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblNombres.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblNombres.LineVisible = true;
            this.lblNombres.Location = new System.Drawing.Point(463, 74);
            this.lblNombres.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblNombres.Name = "lblNombres";
            this.lblNombres.Size = new System.Drawing.Size(360, 26);
            this.lblNombres.TabIndex = 453;
            this.lblNombres.Text = "[lblNombres]";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Appearance.Options.UseForeColor = true;
            this.labelControl3.Appearance.Options.UseTextOptions = true;
            this.labelControl3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl3.Location = new System.Drawing.Point(463, 52);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(360, 23);
            this.labelControl3.TabIndex = 452;
            this.labelControl3.Text = "Nombre:";
            // 
            // lblCorreo
            // 
            this.lblCorreo.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCorreo.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblCorreo.Appearance.Options.UseFont = true;
            this.lblCorreo.Appearance.Options.UseForeColor = true;
            this.lblCorreo.Appearance.Options.UseTextOptions = true;
            this.lblCorreo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblCorreo.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblCorreo.LineColor = System.Drawing.Color.DimGray;
            this.lblCorreo.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblCorreo.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblCorreo.LineVisible = true;
            this.lblCorreo.Location = new System.Drawing.Point(463, 130);
            this.lblCorreo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblCorreo.Name = "lblCorreo";
            this.lblCorreo.Size = new System.Drawing.Size(360, 26);
            this.lblCorreo.TabIndex = 451;
            this.lblCorreo.Text = "[lblCorreo]";
            // 
            // lblNumeroDocumento
            // 
            this.lblNumeroDocumento.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumeroDocumento.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblNumeroDocumento.Appearance.Options.UseFont = true;
            this.lblNumeroDocumento.Appearance.Options.UseForeColor = true;
            this.lblNumeroDocumento.Appearance.Options.UseTextOptions = true;
            this.lblNumeroDocumento.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblNumeroDocumento.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblNumeroDocumento.LineColor = System.Drawing.Color.DimGray;
            this.lblNumeroDocumento.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblNumeroDocumento.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblNumeroDocumento.LineVisible = true;
            this.lblNumeroDocumento.Location = new System.Drawing.Point(29, 190);
            this.lblNumeroDocumento.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblNumeroDocumento.Name = "lblNumeroDocumento";
            this.lblNumeroDocumento.Size = new System.Drawing.Size(360, 26);
            this.lblNumeroDocumento.TabIndex = 450;
            this.lblNumeroDocumento.Text = "[lblNumeroDocumento]";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Appearance.Options.UseForeColor = true;
            this.labelControl5.Appearance.Options.UseTextOptions = true;
            this.labelControl5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl5.Location = new System.Drawing.Point(463, 110);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(360, 23);
            this.labelControl5.TabIndex = 449;
            this.labelControl5.Text = "Correo electrónico:";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Appearance.Options.UseForeColor = true;
            this.labelControl4.Appearance.Options.UseTextOptions = true;
            this.labelControl4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl4.Location = new System.Drawing.Point(29, 164);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(360, 23);
            this.labelControl4.TabIndex = 448;
            this.labelControl4.Text = "Número del documento:";
            // 
            // lblDescripcion
            // 
            this.lblDescripcion.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescripcion.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblDescripcion.Appearance.Options.UseFont = true;
            this.lblDescripcion.Appearance.Options.UseForeColor = true;
            this.lblDescripcion.Appearance.Options.UseTextOptions = true;
            this.lblDescripcion.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblDescripcion.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblDescripcion.LineColor = System.Drawing.Color.DimGray;
            this.lblDescripcion.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblDescripcion.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblDescripcion.LineVisible = true;
            this.lblDescripcion.Location = new System.Drawing.Point(29, 130);
            this.lblDescripcion.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblDescripcion.Name = "lblDescripcion";
            this.lblDescripcion.Size = new System.Drawing.Size(360, 26);
            this.lblDescripcion.TabIndex = 447;
            this.lblDescripcion.Text = "[lblDescripcion]";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Appearance.Options.UseForeColor = true;
            this.labelControl1.Appearance.Options.UseTextOptions = true;
            this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.Location = new System.Drawing.Point(29, 110);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(360, 23);
            this.labelControl1.TabIndex = 446;
            this.labelControl1.Text = "Descripción del producto:";
            // 
            // lblProducto
            // 
            this.lblProducto.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProducto.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblProducto.Appearance.Options.UseFont = true;
            this.lblProducto.Appearance.Options.UseForeColor = true;
            this.lblProducto.Appearance.Options.UseTextOptions = true;
            this.lblProducto.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblProducto.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblProducto.LineColor = System.Drawing.Color.DimGray;
            this.lblProducto.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblProducto.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblProducto.LineVisible = true;
            this.lblProducto.Location = new System.Drawing.Point(29, 74);
            this.lblProducto.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblProducto.Name = "lblProducto";
            this.lblProducto.Size = new System.Drawing.Size(360, 26);
            this.lblProducto.TabIndex = 445;
            this.lblProducto.Text = "[lblProducto]";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Appearance.Options.UseForeColor = true;
            this.labelControl2.Appearance.Options.UseTextOptions = true;
            this.labelControl2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl2.Location = new System.Drawing.Point(29, 52);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(360, 23);
            this.labelControl2.TabIndex = 444;
            this.labelControl2.Text = "Codigo producto:";
            // 
            // picPaso1
            // 
            this.picPaso1.Cursor = System.Windows.Forms.Cursors.Default;
            this.picPaso1.EditValue = global::Gv.ExodusBc.UI.Properties.Resources.Circulo5;
            this.picPaso1.Location = new System.Drawing.Point(5, 4);
            this.picPaso1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.picPaso1.Name = "picPaso1";
            this.picPaso1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picPaso1.Properties.Appearance.Options.UseBackColor = true;
            this.picPaso1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.picPaso1.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picPaso1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.picPaso1.Size = new System.Drawing.Size(35, 30);
            this.picPaso1.TabIndex = 430;
            // 
            // ssForm
            // 
            this.ssForm.ClosingDelay = 500;
            // 
            // frmEnvioSolicitud
            // 
            this.Appearance.BackColor = System.Drawing.Color.White;
            this.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.Appearance.Options.UseBackColor = true;
            this.Appearance.Options.UseBorderColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(920, 525);
            this.Controls.Add(this.lblTituloAccion);
            this.Controls.Add(this.groupControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEnvioSolicitud";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Envío de la solicitud";
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtRtnCiente.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSolicitudEnviada.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPaso1.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraEditors.LabelControl lblTituloAccion;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.SimpleButton btnEnviarSolicitud;
        private DevExpress.XtraEditors.PictureEdit picPaso1;
        private DevExpress.XtraEditors.LabelControl lblProducto;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl lblDescripcion;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl lblCorreo;
        private DevExpress.XtraEditors.LabelControl lblNumeroDocumento;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl lblTelefono;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl lblNombres;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.PictureEdit picSolicitudEnviada;
        private DevExpress.XtraEditors.LabelControl lblSolicitudEnviada;
        private DevExpress.XtraSplashScreen.SplashScreenManager ssForm;
        private DevExpress.XtraEditors.LabelControl lblNumeroTicketTitulo;
        private DevExpress.XtraEditors.LabelControl lblNumeroTicket;
        private DevExpress.XtraEditors.SimpleButton btnEnviarFactura;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit txtRtnCiente;
        private DevExpress.XtraEditors.LabelControl labelControl8;
    }
}