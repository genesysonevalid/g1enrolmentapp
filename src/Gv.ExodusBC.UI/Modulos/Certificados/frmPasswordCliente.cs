﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using static Gv.ExodusBc.UI.ExodusBcBase;

namespace Gv.ExodusBc.UI.Modulos.Certificados
{
    public partial class frmPasswordCliente : DevExpress.XtraEditors.XtraForm
    {
        public frmPasswordCliente()
        {
            InitializeComponent();
            lblAviso.Text = "La contraseña debe tener como mínimo " + VariablesGlobales.LONGITUDMINPASSRSQT + " caracteres.";
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            if(ValidarPassword())
            {
                string encriptSha1 = RequestCertificate.EncodeJsonString(txtPassword.Text.Trim());
                frmCertificados.PASSWORDCLIENT = encriptSha1;
                XtraMessageBox.Show("Contraseña creada satisfactoriamente.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
        }

        bool ValidarPassword()
        {
            if(txtPassword.Text == string.Empty)
            {
                XtraMessageBox.Show("La contraseña esta vacía. este campo es requerido", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtPassword.Focus();
                return false;
            }
            if(txtConfirmPassword.Text == string.Empty)
            {
                XtraMessageBox.Show("La confirmación de la contraseña esta vacía. este campo es requerido", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtConfirmPassword.Focus();
                return false;
            }
            if (txtPassword.Text.Trim() != txtConfirmPassword.Text.Trim())
            {
                XtraMessageBox.Show("Las contraseñas no coinciden.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            if(txtPassword.Text.Length < VariablesGlobales.LONGITUDMINPASSRSQT)
            {
                XtraMessageBox.Show("La contraseña debe contener como mínimo " + VariablesGlobales.LONGITUDMINPASSRSQT + " caracteres.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            return true;
        }
    }
}