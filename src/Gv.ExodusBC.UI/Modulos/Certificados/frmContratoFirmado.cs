﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Newtonsoft.Json.Linq;
using static Gv.ExodusBc.UI.ExodusBcBase;
using Gv.Utilidades;
using System.IO;
using DevExpress.XtraPdfViewer.Commands;
using DevExpress.XtraPdfViewer;

namespace Gv.ExodusBc.UI.Modulos.Certificados
{
    public partial class frmContratoFirmado : DevExpress.XtraEditors.XtraForm
    {
        int RequestId = 0;
        PdfViewerCommand zoomIn;
        PdfViewerCommand zoomOut;
        public byte[] Archivo;
        string Folder = string.Empty;
        string NombreDocumeto = string.Empty;
        string Archivo64str = string.Empty;
        string Path = @"C:\";


        [DefaultValue(PdfZoomMode.ActualSize)]
        public PdfZoomMode ZoomMode
        {
            get; set;
        }

        public frmContratoFirmado()
        {
            InitializeComponent();
            Init();
        }


        void Init()
        {
            txtNumeroTicket.Text = string.Empty;
            lblIdentificacion.Text = string.Empty;
            lblNombres.Text = string.Empty;
            lblCorreo.Text = string.Empty;
            lblTelefono.Text = string.Empty;
            lblTipoPersona.Text = string.Empty;
            pnlDatosCliente.Visible = false;
            btnImportarContrato.Enabled = false;
            btnGuardarContrato.Enabled = false;
            RequestId = 0;
            Folder = string.Empty;
            NombreDocumeto = string.Empty;
            Archivo64str = string.Empty;
            Archivo = null;
            pdfViewerDocs.CloseDocument();
        }

        public void CerrarContratoFirmado()
        {
            Close();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            frmMain2 openForm = (frmMain2)Application.OpenForms["frmMain2"];
            openForm.OpenForms("frmContratoFirmado");
            CerrarContratoFirmado();
        }

        private void btnBuscarRegistro_Click(object sender, EventArgs e)
        {
            if(Validaciones())
            {
                try
                {
                    JArray requesTicket = new JArray();
                    ssForm.ShowWaitForm();
                    requesTicket = RequestCertificate.GetRequestByNumberTicket(txtNumeroTicket.Text.Trim());
                    if (requesTicket.Count > 0)
                    {
                        foreach (JObject element in requesTicket)
                        {
                            lblIdentificacion.Text = element["IDENTIFICACION"].ToString();
                            lblNombres.Text = element["NOMBRES"].ToString();
                            lblCorreo.Text = element["CORREO"].ToString();
                            lblTelefono.Text = element["TELEFONO"].ToString();
                            lblTipoPersona.Text = element["TIPOPERSONA"].ToString();
                            RequestId = Convert.ToInt32(element["RequestId"].ToString());
                        }
                        pnlDatosCliente.Visible = true;
                        btnImportarContrato.Enabled = true;
                    }
                    ssForm.CloseWaitForm();
                }
                catch (Exception ex)
                {
                    if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                    XtraMessageBox.Show("Hubo un error al obtener la búsqueda. Comuníquelo al administrador del sistema.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Log.InsertarLog(Log.ErrorType.Error, "frmContratoFirmado::btnBuscarRegistro_Click", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                }
            }
        }

        bool Validaciones()
        {
            if(txtNumeroTicket.Text == string.Empty)
            {
                XtraMessageBox.Show("El campo número de ticket esta vacio.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            return true;
        }

        private void btnImportarContrato_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog() { Filter = "Documentos PDF|*.pdf" };
            try
            {
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    long tamanioMB = (VariablesGlobales.AdjuntoTamanioMaximo / 1024) / 1024;
                    string tipo = System.IO.Path.GetExtension(ofd.FileName);
                    Archivo = File.ReadAllBytes(ofd.FileName);
                    int imagenTamanio = File.ReadAllBytes(ofd.FileName).Length;
                    decimal imagenConvertida = 0;

                    if (imagenTamanio > 0) imagenConvertida = Utilities.FormatByteSize(imagenTamanio);
                    else
                    {
                        XtraMessageBox.Show("Este documento probablemente esta dañado, pruebe con otro documento.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }

                    if (imagenTamanio > VariablesGlobales.AdjuntoTamanioMaximo)
                    {
                        XtraMessageBox.Show(string.Format("No puede adjuntar archivo mayor a {0} MB, por favor intente con otro de igual o menor tamaño.",
                            tamanioMB), "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }

                    MemoryStream pdfStream = new MemoryStream(Archivo);
                    pdfViewerDocs.LoadDocument(pdfStream);
                    pdfViewerDocs.ZoomFactor = 45f;
                    pdfViewerDocs.NavigationPaneInitialVisibility = PdfNavigationPaneVisibility.Hidden;
                    pdfViewerDocs.ZoomMode = PdfZoomMode.PageLevel;
                    pdfViewerDocs.Refresh();

                    btnGuardarContrato.Enabled = true;
                    zoomIn = new PdfZoomInCommand(pdfViewerDocs);
                    zoomOut = new PdfZoomOutCommand(pdfViewerDocs);
                }
            }
            catch (Exception ex)
            {
                Log.InsertarLog(Log.ErrorType.Error, "frmAdjuntos::btnImportar_Click", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        private void btnGuardarContrato_Click(object sender, EventArgs e)
        {
            try
            {
                if(XtraMessageBox.Show("Confirma que actualizará este contrato, y que la información del documento concuerda con la solicitud.", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    ssForm.ShowWaitForm();
                    JArray arrayImgAdjunto = new JArray();
                    arrayImgAdjunto = RequestCertificate.GetAdjuntoByRequestId(RequestId);
                    if (arrayImgAdjunto.Count > 0)
                    {
                        foreach (JObject elementImg in arrayImgAdjunto)
                        {
                            if (elementImg["Folder"].ToString() != null)
                            {
                                if (lblTipoPersona.Text == "PERSONA NATURAL")
                                {
                                    if (elementImg["NombreDocumento"].ToString() == "CONTRATO PERSONA NATURAL")
                                    {
                                        NombreDocumeto = System.IO.Path.GetFileName(Path + elementImg["Folder"].ToString());
                                        Folder = System.IO.Path.GetDirectoryName(Path + elementImg["Folder"].ToString());
                                        Archivo64str = Convert.ToBase64String(Archivo);
                                    }
                                    else if (elementImg["NombreDocumento"].ToString() == "CONTRATO PERSONA NATURAL DEL PROFESIONAL")
                                    {
                                        NombreDocumeto = System.IO.Path.GetFileName(Path + elementImg["Folder"].ToString());
                                        Folder = System.IO.Path.GetDirectoryName(Path + elementImg["Folder"].ToString());
                                        Archivo64str = Convert.ToBase64String(Archivo);
                                    }
                                }
                                else
                                {
                                    if (elementImg["NombreDocumento"].ToString() == "CONTRATO PERSONA JURIDICA")
                                    {
                                        NombreDocumeto = System.IO.Path.GetFileName(Path + elementImg["Folder"].ToString());
                                        Folder = System.IO.Path.GetDirectoryName(Path + elementImg["Folder"].ToString());
                                        Archivo64str = Convert.ToBase64String(Archivo);
                                    }
                                }
                            }
                        }
                    }


                    if (NombreDocumeto != string.Empty & Folder != string.Empty & Archivo64str != string.Empty)
                    {
                        if (!RequestCertificate.UpdateAttachRequestDirectory(Archivo64str, Folder, NombreDocumeto))
                        {
                            if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                            XtraMessageBox.Show("Hubo un error al actualizar el registro. Comuníquelo al administrador del sistema.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            Log.InsertarLog(Log.ErrorType.Error, "frmContratoFirmado::SaveRequest", "Error al guardar documentos adjuntos en los directorios.", VariablesGlobales.PathDataLog);
                        }
                        else
                        {
                            if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                            XtraMessageBox.Show("Registro actualizado exitosamente.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            Init();
                        }
                    }
                }
                else
                {
                    if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                    return;
                }
            }
            catch (Exception ex)
            {
                if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                XtraMessageBox.Show("Hubo un error al guardar el registro. Comuníquelo al administrador del sistema.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmContratoFirmado::btnGuardarContrato_Click", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        private void pdfViewerDocs_DockChanged(object sender, EventArgs e)
        {
            pdfViewerDocs.ZoomMode = DevExpress.XtraPdfViewer.PdfZoomMode.PageLevel;
        }

        private void pdfViewerDocs_PopupMenuShowing(object sender, PdfPopupMenuShowingEventArgs e)
        {
            e.ItemLinks.Clear();
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            Init();
        }

    }
}