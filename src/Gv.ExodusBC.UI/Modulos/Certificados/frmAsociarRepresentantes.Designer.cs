﻿namespace Gv.ExodusBc.UI.Modulos.Certificados
{
    partial class frmAsociarRepresentantes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAsociarRepresentantes));
            this.lblTituloAccion = new DevExpress.XtraEditors.LabelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.btnLeerIdentidad = new DevExpress.XtraEditors.SimpleButton();
            this.lblAtencion = new DevExpress.XtraEditors.LabelControl();
            this.picAtencion = new DevExpress.XtraEditors.PictureEdit();
            this.gcPersonas = new DevExpress.XtraGrid.GridControl();
            this.cmsEditar = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.editToolStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.gvPersonas = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ttcVisualizarTooltip = new DevExpress.Utils.ToolTipController(this.components);
            this.pnlBusquedaBasica = new DevExpress.XtraEditors.PanelControl();
            this.txtNumeroDocumentoPn = new DevExpress.XtraEditors.TextEdit();
            this.lueTipoDocumentoPj = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.pnlBusquedaAvanzada = new DevExpress.XtraEditors.PanelControl();
            this.dteFechaNacimiento = new DevExpress.XtraEditors.DateEdit();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.lueSexo = new DevExpress.XtraEditors.LookUpEdit();
            this.txtPrimerApellido = new DevExpress.XtraEditors.TextEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.txtPrimerNombre = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.btnLimpiar = new DevExpress.XtraEditors.SimpleButton();
            this.btnBuscar = new DevExpress.XtraEditors.SimpleButton();
            this.ssForm = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::Gv.ExodusBc.UI.frmWaitForm), true, true);
            this.lblCantidadRepresentantes = new DevExpress.XtraEditors.LabelControl();
            this.picAddPn = new DevExpress.XtraEditors.PictureEdit();
            this.gcRepresentantes = new DevExpress.XtraGrid.GridControl();
            this.cmsEliminar = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.deleteToolStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.gvRepresentantes = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.Numero = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.pnlRepresentantes = new DevExpress.XtraEditors.PanelControl();
            this.lblRemoverRepresentante = new DevExpress.XtraEditors.LabelControl();
            this.picRemoverRepresentante = new DevExpress.XtraEditors.PictureEdit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picAtencion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcPersonas)).BeginInit();
            this.cmsEditar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvPersonas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlBusquedaBasica)).BeginInit();
            this.pnlBusquedaBasica.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumeroDocumentoPn.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTipoDocumentoPj.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlBusquedaAvanzada)).BeginInit();
            this.pnlBusquedaAvanzada.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dteFechaNacimiento.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteFechaNacimiento.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueSexo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrimerApellido.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrimerNombre.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAddPn.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcRepresentantes)).BeginInit();
            this.cmsEliminar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvRepresentantes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlRepresentantes)).BeginInit();
            this.pnlRepresentantes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picRemoverRepresentante.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTituloAccion
            // 
            this.lblTituloAccion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTituloAccion.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lblTituloAccion.Appearance.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTituloAccion.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lblTituloAccion.Appearance.Options.UseBackColor = true;
            this.lblTituloAccion.Appearance.Options.UseFont = true;
            this.lblTituloAccion.Appearance.Options.UseForeColor = true;
            this.lblTituloAccion.Appearance.Options.UseTextOptions = true;
            this.lblTituloAccion.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblTituloAccion.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblTituloAccion.LineColor = System.Drawing.Color.WhiteSmoke;
            this.lblTituloAccion.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblTituloAccion.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblTituloAccion.LineVisible = true;
            this.lblTituloAccion.Location = new System.Drawing.Point(3, 14);
            this.lblTituloAccion.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblTituloAccion.Name = "lblTituloAccion";
            this.lblTituloAccion.Size = new System.Drawing.Size(1108, 46);
            this.lblTituloAccion.TabIndex = 121;
            this.lblTituloAccion.Text = "Asociar representantes para las empresas";
            // 
            // groupControl1
            // 
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.AppearanceCaption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.AppearanceCaption.Options.UseForeColor = true;
            this.groupControl1.AppearanceCaption.Options.UseTextOptions = true;
            this.groupControl1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.groupControl1.Controls.Add(this.btnLeerIdentidad);
            this.groupControl1.Controls.Add(this.lblAtencion);
            this.groupControl1.Controls.Add(this.picAtencion);
            this.groupControl1.Controls.Add(this.gcPersonas);
            this.groupControl1.Controls.Add(this.pnlBusquedaBasica);
            this.groupControl1.Controls.Add(this.pnlBusquedaAvanzada);
            this.groupControl1.Controls.Add(this.btnLimpiar);
            this.groupControl1.Controls.Add(this.btnBuscar);
            this.groupControl1.Location = new System.Drawing.Point(12, 78);
            this.groupControl1.LookAndFeel.SkinMaskColor = System.Drawing.Color.AliceBlue;
            this.groupControl1.LookAndFeel.SkinName = "Whiteprint";
            this.groupControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.groupControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1127, 390);
            this.groupControl1.TabIndex = 312;
            this.groupControl1.Text = "Búsqueda de personas";
            // 
            // btnLeerIdentidad
            // 
            this.btnLeerIdentidad.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.btnLeerIdentidad.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLeerIdentidad.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.btnLeerIdentidad.Appearance.Options.UseBackColor = true;
            this.btnLeerIdentidad.Appearance.Options.UseFont = true;
            this.btnLeerIdentidad.Appearance.Options.UseForeColor = true;
            this.btnLeerIdentidad.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnLeerIdentidad.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.id_card;
            this.btnLeerIdentidad.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnLeerIdentidad.Location = new System.Drawing.Point(774, 68);
            this.btnLeerIdentidad.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnLeerIdentidad.Name = "btnLeerIdentidad";
            this.btnLeerIdentidad.Size = new System.Drawing.Size(155, 35);
            this.btnLeerIdentidad.TabIndex = 457;
            this.btnLeerIdentidad.Text = "&Leer Identidad";
            this.btnLeerIdentidad.Click += new System.EventHandler(this.btnLeerIdentidad_Click);
            // 
            // lblAtencion
            // 
            this.lblAtencion.Appearance.Font = new System.Drawing.Font("Segoe UI", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAtencion.Appearance.ForeColor = System.Drawing.Color.Maroon;
            this.lblAtencion.Appearance.Options.UseFont = true;
            this.lblAtencion.Appearance.Options.UseForeColor = true;
            this.lblAtencion.Appearance.Options.UseTextOptions = true;
            this.lblAtencion.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblAtencion.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblAtencion.Location = new System.Drawing.Point(48, 357);
            this.lblAtencion.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblAtencion.Name = "lblAtencion";
            this.lblAtencion.Size = new System.Drawing.Size(1067, 23);
            this.lblAtencion.TabIndex = 456;
            this.lblAtencion.Text = "Para agregar un representante haga click derecho sobre la persona seleccionada.";
            this.lblAtencion.Visible = false;
            // 
            // picAtencion
            // 
            this.picAtencion.Cursor = System.Windows.Forms.Cursors.Default;
            this.picAtencion.EditValue = global::Gv.ExodusBc.UI.Properties.Resources.lightbulb;
            this.picAtencion.Location = new System.Drawing.Point(15, 352);
            this.picAtencion.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.picAtencion.Name = "picAtencion";
            this.picAtencion.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picAtencion.Properties.Appearance.Options.UseBackColor = true;
            this.picAtencion.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.picAtencion.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picAtencion.Size = new System.Drawing.Size(26, 30);
            this.picAtencion.TabIndex = 455;
            this.picAtencion.Visible = false;
            // 
            // gcPersonas
            // 
            this.gcPersonas.ContextMenuStrip = this.cmsEditar;
            this.gcPersonas.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gcPersonas.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gcPersonas.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gcPersonas.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gcPersonas.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gcPersonas.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gcPersonas.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gcPersonas.Location = new System.Drawing.Point(13, 190);
            this.gcPersonas.LookAndFeel.SkinName = "Office 2016 Colorful";
            this.gcPersonas.LookAndFeel.UseDefaultLookAndFeel = false;
            this.gcPersonas.MainView = this.gvPersonas;
            this.gcPersonas.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gcPersonas.Name = "gcPersonas";
            this.gcPersonas.Size = new System.Drawing.Size(1102, 162);
            this.gcPersonas.TabIndex = 446;
            this.gcPersonas.ToolTipController = this.ttcVisualizarTooltip;
            this.gcPersonas.UseEmbeddedNavigator = true;
            this.gcPersonas.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvPersonas});
            // 
            // cmsEditar
            // 
            this.cmsEditar.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.cmsEditar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editToolStrip});
            this.cmsEditar.Name = "cmsEditar";
            this.cmsEditar.Size = new System.Drawing.Size(274, 30);
            // 
            // editToolStrip
            // 
            this.editToolStrip.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_legal_32x32;
            this.editToolStrip.Name = "editToolStrip";
            this.editToolStrip.Size = new System.Drawing.Size(273, 26);
            this.editToolStrip.Text = "Agregar como representante";
            this.editToolStrip.Click += new System.EventHandler(this.editToolStrip_Click);
            // 
            // gvPersonas
            // 
            this.gvPersonas.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvPersonas.Appearance.HeaderPanel.Options.UseFont = true;
            this.gvPersonas.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.gvPersonas.Appearance.OddRow.Options.UseBackColor = true;
            this.gvPersonas.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gvPersonas.Appearance.Row.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvPersonas.Appearance.Row.Options.UseBackColor = true;
            this.gvPersonas.Appearance.Row.Options.UseFont = true;
            this.gvPersonas.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6});
            this.gvPersonas.GridControl = this.gcPersonas;
            this.gvPersonas.Name = "gvPersonas";
            this.gvPersonas.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gvPersonas.OptionsView.EnableAppearanceOddRow = true;
            this.gvPersonas.OptionsView.ShowGroupPanel = false;
            this.gvPersonas.RowHeight = 30;
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceCell.Font = new System.Drawing.Font("Segoe UI", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn1.AppearanceCell.Options.UseFont = true;
            this.gridColumn1.AppearanceHeader.Font = new System.Drawing.Font("Segoe UI Semibold", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn1.AppearanceHeader.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.gridColumn1.AppearanceHeader.Options.UseFont = true;
            this.gridColumn1.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.Caption = "FOTOGRAFÍA";
            this.gridColumn1.FieldName = "Foto";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 130;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceCell.Font = new System.Drawing.Font("Segoe UI", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn2.AppearanceCell.Options.UseFont = true;
            this.gridColumn2.AppearanceHeader.Font = new System.Drawing.Font("Segoe UI Semibold", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn2.AppearanceHeader.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.gridColumn2.AppearanceHeader.Options.UseFont = true;
            this.gridColumn2.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.Caption = "IDENTIFICACIÓN";
            this.gridColumn2.FieldName = "Identificacion";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 180;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceCell.Font = new System.Drawing.Font("Segoe UI", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn3.AppearanceCell.Options.UseFont = true;
            this.gridColumn3.AppearanceHeader.Font = new System.Drawing.Font("Segoe UI Semibold", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn3.AppearanceHeader.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.gridColumn3.AppearanceHeader.Options.UseFont = true;
            this.gridColumn3.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.Caption = "NOMBRES";
            this.gridColumn3.FieldName = "Nombres";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsFilter.AllowFilter = false;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            this.gridColumn3.Width = 234;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceCell.Font = new System.Drawing.Font("Segoe UI", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn4.AppearanceCell.Options.UseFont = true;
            this.gridColumn4.AppearanceHeader.Font = new System.Drawing.Font("Segoe UI Semibold", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn4.AppearanceHeader.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.gridColumn4.AppearanceHeader.Options.UseFont = true;
            this.gridColumn4.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn4.Caption = "APELLIDOS";
            this.gridColumn4.FieldName = "Apellidos";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            this.gridColumn4.Width = 234;
            // 
            // gridColumn5
            // 
            this.gridColumn5.AppearanceCell.Font = new System.Drawing.Font("Segoe UI", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn5.AppearanceCell.Options.UseFont = true;
            this.gridColumn5.AppearanceHeader.Font = new System.Drawing.Font("Segoe UI Semibold", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn5.AppearanceHeader.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.gridColumn5.AppearanceHeader.Options.UseFont = true;
            this.gridColumn5.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn5.Caption = "NACIONALIDAD";
            this.gridColumn5.FieldName = "Nacionalidad";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 4;
            this.gridColumn5.Width = 234;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "PersonId";
            this.gridColumn6.FieldName = "PersonaId";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 5;
            this.gridColumn6.Width = 50;
            // 
            // ttcVisualizarTooltip
            // 
            this.ttcVisualizarTooltip.GetActiveObjectInfo += new DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventHandler(this.ttcVisualizarTooltip_GetActiveObjectInfo);
            // 
            // pnlBusquedaBasica
            // 
            this.pnlBusquedaBasica.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlBusquedaBasica.Controls.Add(this.txtNumeroDocumentoPn);
            this.pnlBusquedaBasica.Controls.Add(this.lueTipoDocumentoPj);
            this.pnlBusquedaBasica.Controls.Add(this.labelControl1);
            this.pnlBusquedaBasica.Controls.Add(this.labelControl4);
            this.pnlBusquedaBasica.Location = new System.Drawing.Point(15, 42);
            this.pnlBusquedaBasica.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pnlBusquedaBasica.Name = "pnlBusquedaBasica";
            this.pnlBusquedaBasica.Size = new System.Drawing.Size(566, 66);
            this.pnlBusquedaBasica.TabIndex = 445;
            // 
            // txtNumeroDocumentoPn
            // 
            this.txtNumeroDocumentoPn.Location = new System.Drawing.Point(285, 28);
            this.txtNumeroDocumentoPn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtNumeroDocumentoPn.Name = "txtNumeroDocumentoPn";
            this.txtNumeroDocumentoPn.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNumeroDocumentoPn.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.txtNumeroDocumentoPn.Properties.Appearance.Options.UseFont = true;
            this.txtNumeroDocumentoPn.Properties.Appearance.Options.UseForeColor = true;
            this.txtNumeroDocumentoPn.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtNumeroDocumentoPn.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtNumeroDocumentoPn.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtNumeroDocumentoPn.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNumeroDocumentoPn.Properties.Mask.EditMask = "0000-0000-00000";
            this.txtNumeroDocumentoPn.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
            this.txtNumeroDocumentoPn.Properties.Mask.SaveLiteral = false;
            this.txtNumeroDocumentoPn.Properties.MaxLength = 20;
            this.txtNumeroDocumentoPn.Size = new System.Drawing.Size(265, 30);
            this.txtNumeroDocumentoPn.TabIndex = 1;
            this.txtNumeroDocumentoPn.ToolTip = "Número de identificación";
            // 
            // lueTipoDocumentoPj
            // 
            this.lueTipoDocumentoPj.Location = new System.Drawing.Point(3, 30);
            this.lueTipoDocumentoPj.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lueTipoDocumentoPj.Name = "lueTipoDocumentoPj";
            this.lueTipoDocumentoPj.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.lueTipoDocumentoPj.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueTipoDocumentoPj.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lueTipoDocumentoPj.Properties.Appearance.Options.UseBackColor = true;
            this.lueTipoDocumentoPj.Properties.Appearance.Options.UseFont = true;
            this.lueTipoDocumentoPj.Properties.Appearance.Options.UseForeColor = true;
            this.lueTipoDocumentoPj.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lueTipoDocumentoPj.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lueTipoDocumentoPj.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.lueTipoDocumentoPj.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueTipoDocumentoPj.Properties.NullText = "";
            this.lueTipoDocumentoPj.Size = new System.Drawing.Size(261, 30);
            this.lueTipoDocumentoPj.TabIndex = 0;
            this.lueTipoDocumentoPj.ToolTip = "Tipo de documento";
            this.lueTipoDocumentoPj.EditValueChanged += new System.EventHandler(this.lueTipoDocumentoPj_EditValueChanged);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Appearance.Options.UseForeColor = true;
            this.labelControl1.Location = new System.Drawing.Point(285, 2);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(172, 20);
            this.labelControl1.TabIndex = 440;
            this.labelControl1.Text = "Número de identificación:";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Appearance.Options.UseForeColor = true;
            this.labelControl4.Location = new System.Drawing.Point(7, 2);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(134, 20);
            this.labelControl4.TabIndex = 439;
            this.labelControl4.Text = "Tipo de documento:";
            // 
            // pnlBusquedaAvanzada
            // 
            this.pnlBusquedaAvanzada.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlBusquedaAvanzada.Controls.Add(this.dteFechaNacimiento);
            this.pnlBusquedaAvanzada.Controls.Add(this.labelControl16);
            this.pnlBusquedaAvanzada.Controls.Add(this.labelControl12);
            this.pnlBusquedaAvanzada.Controls.Add(this.lueSexo);
            this.pnlBusquedaAvanzada.Controls.Add(this.txtPrimerApellido);
            this.pnlBusquedaAvanzada.Controls.Add(this.labelControl11);
            this.pnlBusquedaAvanzada.Controls.Add(this.txtPrimerNombre);
            this.pnlBusquedaAvanzada.Controls.Add(this.labelControl10);
            this.pnlBusquedaAvanzada.Location = new System.Drawing.Point(13, 114);
            this.pnlBusquedaAvanzada.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pnlBusquedaAvanzada.Name = "pnlBusquedaAvanzada";
            this.pnlBusquedaAvanzada.Size = new System.Drawing.Size(1102, 65);
            this.pnlBusquedaAvanzada.TabIndex = 444;
            this.pnlBusquedaAvanzada.Visible = false;
            // 
            // dteFechaNacimiento
            // 
            this.dteFechaNacimiento.EditValue = null;
            this.dteFechaNacimiento.Location = new System.Drawing.Point(570, 30);
            this.dteFechaNacimiento.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dteFechaNacimiento.Name = "dteFechaNacimiento";
            this.dteFechaNacimiento.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dteFechaNacimiento.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.dteFechaNacimiento.Properties.Appearance.Options.UseFont = true;
            this.dteFechaNacimiento.Properties.Appearance.Options.UseForeColor = true;
            this.dteFechaNacimiento.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dteFechaNacimiento.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dteFechaNacimiento.Properties.AppearanceFocused.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dteFechaNacimiento.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.dteFechaNacimiento.Properties.AppearanceFocused.Options.UseFont = true;
            this.dteFechaNacimiento.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dteFechaNacimiento.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dteFechaNacimiento.Size = new System.Drawing.Size(247, 30);
            this.dteFechaNacimiento.TabIndex = 6;
            // 
            // labelControl16
            // 
            this.labelControl16.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl16.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl16.Appearance.Options.UseFont = true;
            this.labelControl16.Appearance.Options.UseForeColor = true;
            this.labelControl16.Location = new System.Drawing.Point(828, 5);
            this.labelControl16.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(35, 20);
            this.labelControl16.TabIndex = 448;
            this.labelControl16.Text = "Sexo:";
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl12.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl12.Appearance.Options.UseFont = true;
            this.labelControl12.Appearance.Options.UseForeColor = true;
            this.labelControl12.Location = new System.Drawing.Point(570, 5);
            this.labelControl12.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(140, 20);
            this.labelControl12.TabIndex = 449;
            this.labelControl12.Text = "Fecha de nacimiento:";
            // 
            // lueSexo
            // 
            this.lueSexo.Location = new System.Drawing.Point(828, 30);
            this.lueSexo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lueSexo.Name = "lueSexo";
            this.lueSexo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.lueSexo.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueSexo.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lueSexo.Properties.Appearance.Options.UseBackColor = true;
            this.lueSexo.Properties.Appearance.Options.UseFont = true;
            this.lueSexo.Properties.Appearance.Options.UseForeColor = true;
            this.lueSexo.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lueSexo.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lueSexo.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.lueSexo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueSexo.Properties.NullText = "";
            this.lueSexo.Size = new System.Drawing.Size(258, 30);
            this.lueSexo.TabIndex = 7;
            // 
            // txtPrimerApellido
            // 
            this.txtPrimerApellido.Location = new System.Drawing.Point(287, 30);
            this.txtPrimerApellido.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPrimerApellido.Name = "txtPrimerApellido";
            this.txtPrimerApellido.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrimerApellido.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.txtPrimerApellido.Properties.Appearance.Options.UseFont = true;
            this.txtPrimerApellido.Properties.Appearance.Options.UseForeColor = true;
            this.txtPrimerApellido.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtPrimerApellido.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtPrimerApellido.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtPrimerApellido.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPrimerApellido.Size = new System.Drawing.Size(265, 30);
            this.txtPrimerApellido.TabIndex = 5;
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl11.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl11.Appearance.Options.UseFont = true;
            this.labelControl11.Appearance.Options.UseForeColor = true;
            this.labelControl11.Location = new System.Drawing.Point(287, 5);
            this.labelControl11.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(105, 20);
            this.labelControl11.TabIndex = 447;
            this.labelControl11.Text = "Primer apellido:";
            // 
            // txtPrimerNombre
            // 
            this.txtPrimerNombre.Location = new System.Drawing.Point(5, 30);
            this.txtPrimerNombre.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPrimerNombre.Name = "txtPrimerNombre";
            this.txtPrimerNombre.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrimerNombre.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.txtPrimerNombre.Properties.Appearance.Options.UseFont = true;
            this.txtPrimerNombre.Properties.Appearance.Options.UseForeColor = true;
            this.txtPrimerNombre.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtPrimerNombre.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtPrimerNombre.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtPrimerNombre.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPrimerNombre.Size = new System.Drawing.Size(262, 30);
            this.txtPrimerNombre.TabIndex = 4;
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl10.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl10.Appearance.Options.UseFont = true;
            this.labelControl10.Appearance.Options.UseForeColor = true;
            this.labelControl10.Location = new System.Drawing.Point(6, 5);
            this.labelControl10.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(102, 20);
            this.labelControl10.TabIndex = 446;
            this.labelControl10.Text = "Primer nombre:";
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLimpiar.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.btnLimpiar.Appearance.Options.UseFont = true;
            this.btnLimpiar.Appearance.Options.UseForeColor = true;
            this.btnLimpiar.Appearance.Options.UseTextOptions = true;
            this.btnLimpiar.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnLimpiar.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnLimpiar.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_eraser_32x32;
            this.btnLimpiar.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnLimpiar.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLimpiar.Location = new System.Drawing.Point(962, 67);
            this.btnLimpiar.LookAndFeel.SkinName = "Whiteprint";
            this.btnLimpiar.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnLimpiar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(155, 35);
            this.btnLimpiar.TabIndex = 8;
            this.btnLimpiar.Text = "&Limpiar";
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // btnBuscar
            // 
            this.btnBuscar.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.btnBuscar.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuscar.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.btnBuscar.Appearance.Options.UseBackColor = true;
            this.btnBuscar.Appearance.Options.UseFont = true;
            this.btnBuscar.Appearance.Options.UseForeColor = true;
            this.btnBuscar.Appearance.Options.UseTextOptions = true;
            this.btnBuscar.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnBuscar.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnBuscar.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_search_32x32;
            this.btnBuscar.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnBuscar.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnBuscar.Location = new System.Drawing.Point(589, 68);
            this.btnBuscar.LookAndFeel.SkinName = "Whiteprint";
            this.btnBuscar.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnBuscar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(155, 35);
            this.btnBuscar.TabIndex = 2;
            this.btnBuscar.Text = "&Buscar ";
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // ssForm
            // 
            this.ssForm.ClosingDelay = 500;
            // 
            // lblCantidadRepresentantes
            // 
            this.lblCantidadRepresentantes.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCantidadRepresentantes.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lblCantidadRepresentantes.Appearance.Options.UseFont = true;
            this.lblCantidadRepresentantes.Appearance.Options.UseForeColor = true;
            this.lblCantidadRepresentantes.Appearance.Options.UseTextOptions = true;
            this.lblCantidadRepresentantes.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblCantidadRepresentantes.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblCantidadRepresentantes.Location = new System.Drawing.Point(54, 8);
            this.lblCantidadRepresentantes.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblCantidadRepresentantes.Name = "lblCantidadRepresentantes";
            this.lblCantidadRepresentantes.Size = new System.Drawing.Size(386, 30);
            this.lblCantidadRepresentantes.TabIndex = 521;
            this.lblCantidadRepresentantes.Text = "(0) Representantes asociados";
            // 
            // picAddPn
            // 
            this.picAddPn.Cursor = System.Windows.Forms.Cursors.Default;
            this.picAddPn.EditValue = global::Gv.ExodusBc.UI.Properties.Resources.ic_groupperson_32x32;
            this.picAddPn.Location = new System.Drawing.Point(22, 11);
            this.picAddPn.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.picAddPn.Name = "picAddPn";
            this.picAddPn.Properties.AllowFocused = false;
            this.picAddPn.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.picAddPn.Properties.Appearance.Font = new System.Drawing.Font("Arial Black", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.picAddPn.Properties.Appearance.ForeColor = System.Drawing.Color.DarkGray;
            this.picAddPn.Properties.Appearance.Options.UseBackColor = true;
            this.picAddPn.Properties.Appearance.Options.UseFont = true;
            this.picAddPn.Properties.Appearance.Options.UseForeColor = true;
            this.picAddPn.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.picAddPn.Properties.NullText = "5";
            this.picAddPn.Properties.ShowMenu = false;
            this.picAddPn.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.picAddPn.Size = new System.Drawing.Size(23, 23);
            this.picAddPn.TabIndex = 520;
            // 
            // gcRepresentantes
            // 
            this.gcRepresentantes.ContextMenuStrip = this.cmsEliminar;
            this.gcRepresentantes.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gcRepresentantes.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gcRepresentantes.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gcRepresentantes.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gcRepresentantes.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gcRepresentantes.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gcRepresentantes.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gcRepresentantes.Location = new System.Drawing.Point(15, 44);
            this.gcRepresentantes.LookAndFeel.SkinName = "Office 2016 Colorful";
            this.gcRepresentantes.LookAndFeel.UseDefaultLookAndFeel = false;
            this.gcRepresentantes.MainView = this.gvRepresentantes;
            this.gcRepresentantes.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gcRepresentantes.Name = "gcRepresentantes";
            this.gcRepresentantes.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemButtonEdit1});
            this.gcRepresentantes.Size = new System.Drawing.Size(1102, 178);
            this.gcRepresentantes.TabIndex = 521;
            this.gcRepresentantes.ToolTipController = this.ttcVisualizarTooltip;
            this.gcRepresentantes.UseEmbeddedNavigator = true;
            this.gcRepresentantes.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvRepresentantes});
            // 
            // cmsEliminar
            // 
            this.cmsEliminar.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.cmsEliminar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteToolStrip});
            this.cmsEliminar.Name = "cmsEditar";
            this.cmsEliminar.Size = new System.Drawing.Size(232, 30);
            // 
            // deleteToolStrip
            // 
            this.deleteToolStrip.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_legal_32x32;
            this.deleteToolStrip.Name = "deleteToolStrip";
            this.deleteToolStrip.Size = new System.Drawing.Size(231, 26);
            this.deleteToolStrip.Text = "Eliminar representante";
            this.deleteToolStrip.Click += new System.EventHandler(this.deleteToolStrip_Click);
            // 
            // gvRepresentantes
            // 
            this.gvRepresentantes.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvRepresentantes.Appearance.HeaderPanel.Options.UseFont = true;
            this.gvRepresentantes.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.gvRepresentantes.Appearance.OddRow.Options.UseBackColor = true;
            this.gvRepresentantes.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gvRepresentantes.Appearance.Row.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvRepresentantes.Appearance.Row.Options.UseBackColor = true;
            this.gvRepresentantes.Appearance.Row.Options.UseFont = true;
            this.gvRepresentantes.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.Numero,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11});
            this.gvRepresentantes.GridControl = this.gcRepresentantes;
            this.gvRepresentantes.Name = "gvRepresentantes";
            this.gvRepresentantes.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gvRepresentantes.OptionsView.EnableAppearanceOddRow = true;
            this.gvRepresentantes.OptionsView.ShowGroupPanel = false;
            this.gvRepresentantes.RowHeight = 30;
            // 
            // Numero
            // 
            this.Numero.Caption = "Numero";
            this.Numero.Name = "Numero";
            // 
            // gridColumn7
            // 
            this.gridColumn7.AppearanceCell.Font = new System.Drawing.Font("Segoe UI", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn7.AppearanceCell.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.gridColumn7.AppearanceCell.Options.UseFont = true;
            this.gridColumn7.AppearanceCell.Options.UseForeColor = true;
            this.gridColumn7.AppearanceHeader.Font = new System.Drawing.Font("Segoe UI Semibold", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn7.AppearanceHeader.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.gridColumn7.AppearanceHeader.Options.UseFont = true;
            this.gridColumn7.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn7.Caption = "FOTOGRAFÍA";
            this.gridColumn7.FieldName = "Foto";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 0;
            this.gridColumn7.Width = 130;
            // 
            // gridColumn8
            // 
            this.gridColumn8.AppearanceCell.Font = new System.Drawing.Font("Segoe UI", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn8.AppearanceCell.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.gridColumn8.AppearanceCell.Options.UseFont = true;
            this.gridColumn8.AppearanceCell.Options.UseForeColor = true;
            this.gridColumn8.AppearanceHeader.Font = new System.Drawing.Font("Segoe UI Semibold", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn8.AppearanceHeader.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.gridColumn8.AppearanceHeader.Options.UseFont = true;
            this.gridColumn8.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn8.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn8.Caption = "IDENTIFICACIÓN";
            this.gridColumn8.FieldName = "DocumentNumber";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 1;
            this.gridColumn8.Width = 180;
            // 
            // gridColumn9
            // 
            this.gridColumn9.AppearanceCell.Font = new System.Drawing.Font("Segoe UI", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn9.AppearanceCell.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.gridColumn9.AppearanceCell.Options.UseFont = true;
            this.gridColumn9.AppearanceCell.Options.UseForeColor = true;
            this.gridColumn9.AppearanceHeader.Font = new System.Drawing.Font("Segoe UI Semibold", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn9.AppearanceHeader.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.gridColumn9.AppearanceHeader.Options.UseFont = true;
            this.gridColumn9.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn9.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn9.Caption = "NOMBRES";
            this.gridColumn9.FieldName = "Names";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsFilter.AllowFilter = false;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 2;
            this.gridColumn9.Width = 234;
            // 
            // gridColumn10
            // 
            this.gridColumn10.AppearanceCell.Font = new System.Drawing.Font("Segoe UI", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn10.AppearanceCell.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.gridColumn10.AppearanceCell.Options.UseFont = true;
            this.gridColumn10.AppearanceCell.Options.UseForeColor = true;
            this.gridColumn10.AppearanceHeader.Font = new System.Drawing.Font("Segoe UI Semibold", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn10.AppearanceHeader.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.gridColumn10.AppearanceHeader.Options.UseFont = true;
            this.gridColumn10.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn10.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn10.Caption = "APELLIDOS";
            this.gridColumn10.FieldName = "LastNames";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 3;
            this.gridColumn10.Width = 234;
            // 
            // gridColumn11
            // 
            this.gridColumn11.AppearanceCell.Font = new System.Drawing.Font("Segoe UI", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn11.AppearanceCell.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.gridColumn11.AppearanceCell.Options.UseFont = true;
            this.gridColumn11.AppearanceCell.Options.UseForeColor = true;
            this.gridColumn11.AppearanceHeader.Font = new System.Drawing.Font("Segoe UI Semibold", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn11.AppearanceHeader.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.gridColumn11.AppearanceHeader.Options.UseFont = true;
            this.gridColumn11.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn11.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn11.Caption = "NACIONALIDAD";
            this.gridColumn11.FieldName = "Nationality";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 4;
            this.gridColumn11.Width = 234;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AllowMouseWheel = false;
            editorButtonImageOptions1.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_ticketok_32x32;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Ver detalle de la solicitud para aprobación", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEdit1.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // pnlRepresentantes
            // 
            this.pnlRepresentantes.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlRepresentantes.Controls.Add(this.lblRemoverRepresentante);
            this.pnlRepresentantes.Controls.Add(this.picRemoverRepresentante);
            this.pnlRepresentantes.Controls.Add(this.gcRepresentantes);
            this.pnlRepresentantes.Controls.Add(this.picAddPn);
            this.pnlRepresentantes.Controls.Add(this.lblCantidadRepresentantes);
            this.pnlRepresentantes.Location = new System.Drawing.Point(12, 473);
            this.pnlRepresentantes.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pnlRepresentantes.Name = "pnlRepresentantes";
            this.pnlRepresentantes.Size = new System.Drawing.Size(1127, 256);
            this.pnlRepresentantes.TabIndex = 522;
            // 
            // lblRemoverRepresentante
            // 
            this.lblRemoverRepresentante.Appearance.Font = new System.Drawing.Font("Segoe UI", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRemoverRepresentante.Appearance.ForeColor = System.Drawing.Color.Maroon;
            this.lblRemoverRepresentante.Appearance.Options.UseFont = true;
            this.lblRemoverRepresentante.Appearance.Options.UseForeColor = true;
            this.lblRemoverRepresentante.Appearance.Options.UseTextOptions = true;
            this.lblRemoverRepresentante.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblRemoverRepresentante.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblRemoverRepresentante.Location = new System.Drawing.Point(48, 228);
            this.lblRemoverRepresentante.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblRemoverRepresentante.Name = "lblRemoverRepresentante";
            this.lblRemoverRepresentante.Size = new System.Drawing.Size(1067, 23);
            this.lblRemoverRepresentante.TabIndex = 523;
            this.lblRemoverRepresentante.Text = "Para eliminar un representante, haga click derecho sobre la persona seleccionada." +
    "";
            this.lblRemoverRepresentante.Visible = false;
            // 
            // picRemoverRepresentante
            // 
            this.picRemoverRepresentante.Cursor = System.Windows.Forms.Cursors.Default;
            this.picRemoverRepresentante.EditValue = global::Gv.ExodusBc.UI.Properties.Resources.lightbulb;
            this.picRemoverRepresentante.Location = new System.Drawing.Point(15, 223);
            this.picRemoverRepresentante.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.picRemoverRepresentante.Name = "picRemoverRepresentante";
            this.picRemoverRepresentante.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picRemoverRepresentante.Properties.Appearance.Options.UseBackColor = true;
            this.picRemoverRepresentante.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.picRemoverRepresentante.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picRemoverRepresentante.Size = new System.Drawing.Size(26, 30);
            this.picRemoverRepresentante.TabIndex = 522;
            this.picRemoverRepresentante.Visible = false;
            // 
            // frmAsociarRepresentantes
            // 
            this.Appearance.BackColor = System.Drawing.Color.White;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1151, 741);
            this.Controls.Add(this.pnlRepresentantes);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.lblTituloAccion);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmAsociarRepresentantes";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Representantes legales";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmAsociarRepresentantes_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picAtencion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcPersonas)).EndInit();
            this.cmsEditar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gvPersonas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlBusquedaBasica)).EndInit();
            this.pnlBusquedaBasica.ResumeLayout(false);
            this.pnlBusquedaBasica.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumeroDocumentoPn.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTipoDocumentoPj.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlBusquedaAvanzada)).EndInit();
            this.pnlBusquedaAvanzada.ResumeLayout(false);
            this.pnlBusquedaAvanzada.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dteFechaNacimiento.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteFechaNacimiento.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueSexo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrimerApellido.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrimerNombre.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAddPn.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcRepresentantes)).EndInit();
            this.cmsEliminar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gvRepresentantes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlRepresentantes)).EndInit();
            this.pnlRepresentantes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picRemoverRepresentante.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl lblTituloAccion;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.PanelControl pnlBusquedaBasica;
        private DevExpress.XtraEditors.TextEdit txtNumeroDocumentoPn;
        private DevExpress.XtraEditors.LookUpEdit lueTipoDocumentoPj;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.PanelControl pnlBusquedaAvanzada;
        private DevExpress.XtraEditors.DateEdit dteFechaNacimiento;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LookUpEdit lueSexo;
        private DevExpress.XtraEditors.TextEdit txtPrimerApellido;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.TextEdit txtPrimerNombre;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.SimpleButton btnLimpiar;
        private DevExpress.XtraEditors.SimpleButton btnBuscar;
        private DevExpress.XtraSplashScreen.SplashScreenManager ssForm;
        private DevExpress.XtraGrid.GridControl gcPersonas;
        private DevExpress.XtraGrid.Views.Grid.GridView gvPersonas;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private System.Windows.Forms.ContextMenuStrip cmsEditar;
        private System.Windows.Forms.ToolStripMenuItem editToolStrip;
        private DevExpress.Utils.ToolTipController ttcVisualizarTooltip;
        private DevExpress.XtraEditors.LabelControl lblCantidadRepresentantes;
        private DevExpress.XtraEditors.PictureEdit picAddPn;
        private DevExpress.XtraGrid.GridControl gcRepresentantes;
        private DevExpress.XtraGrid.Views.Grid.GridView gvRepresentantes;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraEditors.PanelControl pnlRepresentantes;
        private DevExpress.XtraGrid.Columns.GridColumn Numero;
        private DevExpress.XtraEditors.LabelControl lblAtencion;
        private DevExpress.XtraEditors.PictureEdit picAtencion;
        private DevExpress.XtraEditors.SimpleButton btnLeerIdentidad;
        private System.Windows.Forms.ContextMenuStrip cmsEliminar;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStrip;
        private DevExpress.XtraEditors.LabelControl lblRemoverRepresentante;
        private DevExpress.XtraEditors.PictureEdit picRemoverRepresentante;
    }
}