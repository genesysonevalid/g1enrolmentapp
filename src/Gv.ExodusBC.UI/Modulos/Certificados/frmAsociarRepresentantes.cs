﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Newtonsoft.Json.Linq;
using static Gv.ExodusBc.UI.ExodusBcBase;
using Gv.Utilidades;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.Utils;
using Gv.ExodusBc.UI.Modulos.InspeccionPrimaria;
using Gv.ExodusBc.LN;
using DevExpress.XtraEditors.Mask;

namespace Gv.ExodusBc.UI.Modulos.Certificados
{
    public partial class frmAsociarRepresentantes : DevExpress.XtraEditors.XtraForm
    {
        public static JArray personRepresentante = null;
        public static string FORMULARIOREVISION = string.Empty;
        public static JArray personDataRepresentante = null;
        public static string NUMERODOCUMENTOPJ = string.Empty;
        public static string NOMBREPJ = string.Empty;
        public static int TotalRepresentantes = 0;
        int idRepresentante = 0;
        int TipoBusqueda = 1;
        public static List<EN.AddRepresentatives> Representantes = new List<EN.AddRepresentatives>();
        public static List<EN.RepresentativeRequest> ListaRepresentantes = new List<EN.RepresentativeRequest>();
        public static List<EN.RepresentativeRequest> ListaRepresentantesRevision = new List<EN.RepresentativeRequest>();
        public static bool IDENTIDADCOMPROBADA = false;
        public frmCertificados frmCert;


        #region VALORES INICIALES DEL FORMULARIO

        public frmAsociarRepresentantes()
        {
            InitializeComponent();
            Init();
        }

        void Init()
        {
            try
            {
                ExodusBcBase.Helper.Combobox.SetComboByTypeDocumentId(lueTipoDocumentoPj);
                ExodusBcBase.Helper.Combobox.setCombo(lueSexo, Convert.ToInt32(LISTCATALOGSTYPES.ListTypesCatalogs.Sexo));
                gcPersonas.Visible = false;

                if(FORMULARIOREVISION == "REVISION")
                {
                    Representantes.Clear();
                    ListaRepresentantes.Clear();
                    ListaRepresentantesRevision.Clear();
                    pnlRepresentantes.Visible = false;
                }
                else 
                {
                    if (Representantes.Count > 0)
                    {
                        CargarRepresentantesGrid();
                        pnlRepresentantes.Visible = true;
                    }
                    else pnlRepresentantes.Visible = false;
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Hubo un error al cargar los tipos de documentos.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmAsociarRepresentantes::btnBuscar_Click", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        #endregion

        #region FUNCIONES DE LOS CONTROLES

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidarControles()) return;

                ssForm.ShowWaitForm();
                personDataRepresentante = new JArray();
                if(TipoBusqueda == 1)
                {
                   personDataRepresentante = RequestCertificate.GetPersonByDocument(Convert.ToInt32(lueTipoDocumentoPj.EditValue), txtNumeroDocumentoPn.Text,
                   txtPrimerNombre.Text.Trim(), txtPrimerApellido.Text.Trim(), dteFechaNacimiento.DateTime.Date, Convert.ToInt32(lueSexo.EditValue));
                }
                else personDataRepresentante = RequestCertificate.GetPersonByNombres(txtPrimerNombre.Text.Trim(), txtPrimerApellido.Text.Trim(), dteFechaNacimiento.DateTime.Date, Convert.ToInt32(lueSexo.EditValue));

                gcPersonas.DataSource = null;
                ListaRepresentantesRevision.Clear();
                if (personDataRepresentante.Count > 0)
                {
                    var list = new List<EN.GridPersonas>();
                    foreach (JObject element in personDataRepresentante)
                    {
                        EN.GridPersonas prs = new EN.GridPersonas
                        {
                            Identificacion = element["DocumentNumber"].ToString(),
                            Nombres = element["Names"].ToString(),
                            Apellidos = element["LastNames"].ToString(),
                            Nacionalidad = element["Nacionality"].ToString(),
                            personaId = Convert.ToInt32(element["PersonId"].ToString())
                        };

                        //Foto
                        JArray arrayFoto = new JArray();
                        arrayFoto = RequestCertificate.GetLastPhoto(Convert.ToInt32(RequestCertificate.GetItemFromJArray(personDataRepresentante, "PersonId").ToString()));
                        foreach (JObject elementPhoto in arrayFoto)
                            if (elementPhoto["Image"].ToString() != null) prs.Foto = (byte[])elementPhoto["Image"];

                        list.Add(prs);
                    }
                    gcPersonas.DataSource = list;
                    gvPersonas.Columns[5].Visible = false;
                    gcPersonas.Visible = true;
                    picAtencion.Visible = true;
                    lblAtencion.Visible = true;
                    TipoBusqueda = 1;
                }
                else if(personDataRepresentante.Count == 0 && pnlBusquedaAvanzada.Visible == false)
                {
                    if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                    if (XtraMessageBox.Show("La persona no se encontro con estos criterios. ¿Desea hacer una búsqueda avanzada?", "Atención",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        txtNumeroDocumentoPn.Text = string.Empty;
                        pnlBusquedaBasica.Visible = false;
                        pnlBusquedaBasica.Appearance.BackColor = Color.White;
                        pnlBusquedaAvanzada.Visible = true;
                        txtPrimerNombre.Focus();
                        TipoBusqueda = 2;
                    }
                }
                else if (personDataRepresentante.Count == 0 && pnlBusquedaAvanzada.Visible == true)
                {
                    if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                    pnlBusquedaBasica.Enabled = true;
                    pnlBusquedaAvanzada.Visible = false;
                    LimpiarControles();
                    TipoBusqueda = 1;
                    XtraMessageBox.Show("La persona no se encuentra enrolada en el sistema.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
            }
            catch (Exception ex)
            {
                if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                XtraMessageBox.Show("Hubo un error al hacer la búsqueda de la persona. Intente en otro momento.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmAsociarRepresentantes::btnBuscar_Click", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        bool ValidarControles()
        {
            if(TipoBusqueda == 1)
            {
                if (lueTipoDocumentoPj.EditValue == null) 
                {
                    XtraMessageBox.Show("Tipo de documento esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    lueTipoDocumentoPj.Focus();
                    return false;
                }
                if (txtNumeroDocumentoPn.Text == string.Empty)
                {
                    XtraMessageBox.Show("Número de documento esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtNumeroDocumentoPn.Focus();
                    return false;
                }
            }
            else
            {
                if (txtPrimerNombre.Text == string.Empty) 
                {
                    XtraMessageBox.Show("El nombre esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtPrimerNombre.Focus();
                    return false;
                }
                if (txtPrimerApellido.Text == string.Empty) 
                {
                    XtraMessageBox.Show("El apellido esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtPrimerNombre.Focus();
                    return false;
                }
                if (lueSexo.EditValue == null)
                {
                    XtraMessageBox.Show("Sexo esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    lueSexo.Focus();
                    return false;
                }
                if (dteFechaNacimiento.EditValue == null)
                {
                    XtraMessageBox.Show("Fecha de nacimiento esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    dteFechaNacimiento.Focus();
                    return false;
                }
                return true;
            }
            return true;
        }

        private void btnLeerIdentidad_Click(object sender, EventArgs e)
        {
            frmEnrolamientoLectora frmLector = new frmEnrolamientoLectora();
            frmLector.ShowDialog();

            if (frmEnrolamientoLectora.LecturaOK)
            {
                lueTipoDocumentoPj.ItemIndex = 0;
                txtNumeroDocumentoPn.Text = frmEnrolamientoLectora.objLectura.NoIdentidad;
                btnBuscar_Click(sender, e);
            }
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            LimpiarControles();
        }

        private void editToolStrip_Click(object sender, EventArgs e)
        {
            if(XtraMessageBox.Show("Confirma que agregará este representante.", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                try
                {
                    if (!VerificarSiExisteRepresentante(RequestCertificate.GetItemFromJArray(personDataRepresentante, "DocumentNumber").ToString()))
                    {
                        XtraMessageBox.Show("Este representante ya esta asociado a esta solicitud.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }

                    frmComprobarIdentidadRepresentante.IDENTIFICACION = RequestCertificate.GetItemFromJArray(personDataRepresentante, "DocumentNumber").ToString();
                    frmComprobarIdentidadRepresentante.NOMBRES = RequestCertificate.GetItemFromJArray(personDataRepresentante, "Names").ToString()
                        + " " + RequestCertificate.GetItemFromJArray(personDataRepresentante, "LastNames").ToString();
                    frmComprobarIdentidadRepresentante.NACIONALIDAD = RequestCertificate.GetItemFromJArray(personDataRepresentante, "Nacionality").ToString();
                    frmComprobarIdentidadRepresentante.PROFESION = RequestCertificate.GetItemFromJArray(personDataRepresentante, "Profesion").ToString();
                    frmComprobarIdentidadRepresentante.ESTADOCIVIL = RequestCertificate.GetItemFromJArray(personDataRepresentante, "ESTADOCIVIL").ToString();
                    frmComprobarIdentidadRepresentante.ARRAYPERSON = personDataRepresentante;
                    frmComprobarIdentidadRepresentante.FOTO = (byte[])gvPersonas.GetRowCellValue(gvPersonas.FocusedRowHandle, "Foto");

                    if(FORMULARIOREVISION == "REVISION") frmComprobarIdentidadRepresentante.FORMULARIOREVISION = "REVISION";
                    frmComprobarIdentidadRepresentante frm = new frmComprobarIdentidadRepresentante();
                    frm.ShowDialog();

                    if (!IDENTIDADCOMPROBADA)
                    {
                        XtraMessageBox.Show("No puede asociar el representante, debido a que su identidad no se confirmo.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }

                    EN.AddRepresentatives rpRq = new EN.AddRepresentatives();
                    rpRq.Number = idRepresentante += 1;
                    rpRq.DocumentNumber = gvPersonas.GetRowCellValue(gvPersonas.FocusedRowHandle, "Identificacion").ToString();
                    rpRq.Names = gvPersonas.GetRowCellValue(gvPersonas.FocusedRowHandle, "Nombres").ToString();
                    rpRq.LastNames = gvPersonas.GetRowCellValue(gvPersonas.FocusedRowHandle, "Apellidos").ToString();
                    rpRq.Nationality = gvPersonas.GetRowCellValue(gvPersonas.FocusedRowHandle, "Nacionalidad").ToString();
                    rpRq.Foto = (byte[])gvPersonas.GetRowCellValue(gvPersonas.FocusedRowHandle, "Foto");
                    Representantes.Add(rpRq);

                    if(FORMULARIOREVISION == "REVISION")
                    {
                        EN.RepresentativeRequest rps = new EN.RepresentativeRequest
                        {
                            DocumentNumber = gvPersonas.GetRowCellValue(gvPersonas.FocusedRowHandle, "Identificacion").ToString(),
                            Names = gvPersonas.GetRowCellValue(gvPersonas.FocusedRowHandle, "Nombres").ToString(),
                            LastNames = gvPersonas.GetRowCellValue(gvPersonas.FocusedRowHandle, "Apellidos").ToString(),
                            Nationality = gvPersonas.GetRowCellValue(gvPersonas.FocusedRowHandle, "Nacionalidad").ToString(),
                            FKRequestId = 0,
                            Foto = (byte[])gvPersonas.GetRowCellValue(gvPersonas.FocusedRowHandle, "Foto")
                        };

                        frmDetalleSolicitudRevision.IDENTIDAD_REPRESENTANTE = gvPersonas.GetRowCellValue(gvPersonas.FocusedRowHandle, "Identificacion").ToString();
                        frmDetalleSolicitudRevision.lstRepresentantes.Add(rps);
                        frmDetalleSolicitudRevision ActualizarGrid = (frmDetalleSolicitudRevision)Application.OpenForms["frmDetalleSolicitudRevision"];
                        ActualizarGrid.UpdateGridRepresentative();

                    }

                    TotalRepresentantes = rpRq.Number; 
                    lblCantidadRepresentantes.Text = "(" + TotalRepresentantes.ToString() + ") Representantes asociados";
                    lblCantidadRepresentantes.Visible = true;

                    CargarRepresentantesGrid();
                    gcRepresentantes.Visible = true;
                    pnlRepresentantes.Visible = true;
                    lblRemoverRepresentante.Visible = true;
                    picRemoverRepresentante.Visible = true;
                  
                    gcPersonas.DataSource = null;
                    gcPersonas.Visible = false;
                    picAtencion.Visible = false;
                    lblAtencion.Visible = false;

                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Hubo un error asociar el representante.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Log.InsertarLog(Log.ErrorType.Error, "frmAsociarRepresentantes::btnBuscar_Click", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                }
            }
            else return;
        }

        private void deleteToolStrip_Click(object sender, EventArgs e)
        {
            try
            {
                var identificacion = gvRepresentantes.GetFocusedRowCellValue("DocumentNumber");
                if (identificacion != null)
                {
                    if (XtraMessageBox.Show("¿Está seguro de eliminar este representante?", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        gvRepresentantes.DeleteRow(gvRepresentantes.FocusedRowHandle);
                        if (gvRepresentantes.RowCount == 0)
                        {
                            TotalRepresentantes = 0;
                            lblCantidadRepresentantes.Text = "(" + TotalRepresentantes.ToString() + ") Representantes asociados";
                            lblCantidadRepresentantes.Visible = false;
                            Representantes.Clear();
                            idRepresentante = 0;
                            gcRepresentantes.Visible = false;
                            gcRepresentantes.DataSource = null;
                            pnlRepresentantes.Visible = false;
                            lblRemoverRepresentante.Visible = false;
                            picRemoverRepresentante.Visible = false;
                            frmCertificados.TotalRepresentantes = 0;
                            frmCertificados ActualizarRepresentantes = (frmCertificados)Application.OpenForms["frmCertificados"];
                            ActualizarRepresentantes.UpdateRepresentantes();
                        }
                        else
                        {
                            Representantes.RemoveAll(r => r.DocumentNumber == (string)identificacion);
                            TotalRepresentantes = Representantes.Count;
                            lblCantidadRepresentantes.Text = "(" + TotalRepresentantes.ToString() + ") Representantes asociados";
                            lblCantidadRepresentantes.Visible = true;
                            picAddPn.Visible = true;
                            idRepresentante -= 1;
                            pnlRepresentantes.Visible = true;
                            CargarRepresentantesGrid();
                        }

                        lueTipoDocumentoPj.EditValue = null;
                        txtNumeroDocumentoPn.Text = string.Empty;
                        IDENTIDADCOMPROBADA = false;
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Hubo en error al remover el representante ", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                Log.InsertarLog(Log.ErrorType.Error, "frmAdjuntos:btnEliminar_Click", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        private void ttcVisualizarTooltip_GetActiveObjectInfo(object sender, ToolTipControllerGetActiveObjectInfoEventArgs e)
        {
            if (e.Info == null && e.SelectedControl == gcPersonas)
            {
                GridView view = gcPersonas.FocusedView as GridView;
                DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo info = view.CalcHitInfo(e.ControlMousePosition);
                if (info.InRowCell)
                {
                    string text = "Asociar representantes para esta persona jurídica";
                    string cellKey = info.RowHandle.ToString() + " - " + info.Column.ToString();
                    e.Info = new ToolTipControlInfo(cellKey, text);
                }
            }
        }

        #endregion

        #region METODOS Y PROCEDIMIENTOS
 
        bool VerificarSiExisteRepresentante(string identidad)
        {
            //***Recorrer el listadd de representantes***
            foreach (var rp in Representantes)
                if (rp.DocumentNumber == identidad) return false;

            //***Buscar en la BBDD*** 
            int rqId = 0;
            if (FORMULARIOREVISION == "REVISION")
            {
                ListaRepresentantesRevision.Clear();
                frmDetalleSolicitudRevision lstRepresentanteRevision = (frmDetalleSolicitudRevision)Application.OpenForms["frmDetalleSolicitudRevision"];
                lstRepresentanteRevision.GetRepresentantes();

                foreach (var rq in ListaRepresentantesRevision)
                {
                    if (rq.FKRequestId != 0) rqId = rq.FKRequestId;
                    if (rq.DocumentNumber == identidad) return false;
                }
            }

            JArray arrayRps = new JArray();
            if (rqId != 0)
            {
                arrayRps = RequestCertificate.GetRepresentativeRequestByDocument(identidad, rqId);
                if (arrayRps != null)
                {
                    if (arrayRps.Count > 0)
                    {
                        foreach (JObject element in arrayRps)
                            if (element["IDENTIFICACION"].ToString() == identidad & Convert.ToInt32(element["FKRequestId"].ToString()) == rqId) return false;
                    }
                }
            }
            else 
            {
                arrayRps = RequestCertificate.GetRepresentativeRequestByDocument(txtNumeroDocumentoPn.Text.Trim());
                if (arrayRps != null)
                {
                    if (arrayRps.Count > 0)
                        foreach (JObject element in arrayRps) if (element["IDENTIFICACION"].ToString() == txtNumeroDocumentoPn.Text.Trim()) return false;
                    else return true;
                }
            }

            return true;
        }

        public void LimpiarControles()
        {
            pnlBusquedaBasica.Visible = true;
            pnlBusquedaAvanzada.Visible = false;
            gcPersonas.Visible = false;
            gcPersonas.DataSource = null;
            lueTipoDocumentoPj.EditValue = null;
            txtNumeroDocumentoPn.Text = string.Empty;
            lblAtencion.Visible = false;
            picAtencion.Visible = false;
            txtPrimerNombre.Text = string.Empty;
            txtPrimerApellido.Text = string.Empty;
            lueSexo.EditValue = null;
            dteFechaNacimiento.EditValue = null;
            TipoBusqueda = 1;
        }

        public void CargarRepresentantesGrid()
        {
            gcRepresentantes.DataSource = Representantes;
            gcRepresentantes.RefreshDataSource();
        }

        public List<EN.RepresentativeRequest> GetRepresentantes()
        {
            if (Representantes.Count > 0)
            {
                foreach (EN.AddRepresentatives rp in Representantes)
                {
                    EN.RepresentativeRequest rpRq = new EN.RepresentativeRequest();
                    rpRq.DocumentNumber = rp.DocumentNumber;
                    rpRq.Names = rp.Names;
                    rpRq.LastNames = rp.LastNames;
                    rpRq.Nationality = rp.Nationality;
                    rpRq.CreationDate = Utilities.getFechaActual();
                    rpRq.FKCreateUserId = Convert.ToInt32(VariablesGlobales.USER_ID);
                    ListaRepresentantes.Add(rpRq);
                }
                if (FORMULARIOREVISION == "REVISION") frmDetalleSolicitudRevision.lstRepresentantes = ListaRepresentantes;
                else frmCertificados.ListaRepresentantes = ListaRepresentantes;
            }
            if (FORMULARIOREVISION == "REVISION") frmDetalleSolicitudRevision.lstRepresentantes = ListaRepresentantes;
            else frmCertificados.ListaRepresentantes = ListaRepresentantes;
               
            return ListaRepresentantes;
        }

        #endregion

        private void frmAsociarRepresentantes_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (gvPersonas.RowCount == 0 & gvRepresentantes.RowCount == 0)
            {
                XtraMessageBox.Show("No asocio ningún representante.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (FORMULARIOREVISION == "REVISION")
            {
                frmDetalleSolicitudRevision.personDataRepresentantes = personDataRepresentante;

                if (!IDENTIDADCOMPROBADA)
                {
                    personRepresentante = null;
                    XtraMessageBox.Show("No puede asociar el representante, debido a que su identidad no se confirmo.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                if (gvRepresentantes.RowCount == -1 | gvRepresentantes.RowCount == 0) frmCertificados.TotalRepresentantes = 0;
                if (!IDENTIDADCOMPROBADA)
                {
                    personRepresentante = null;
                    XtraMessageBox.Show("No puede asociar el representante, debido a que su identidad no se confirmo.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

                frmCertificados.TotalRepresentantes = TotalRepresentantes;
                frmCertificados.personDataRepresentantes = personDataRepresentante;
                frmCertificados ActualizarRepresentantes = (frmCertificados)Application.OpenForms["frmCertificados"];
                frmCertificados.CARGOEMPRESA = frmComprobarIdentidadRepresentante.CARGOEMPRESA;
                frmCertificados.PROFESIONREPRESENTANTE = frmComprobarIdentidadRepresentante.PROFESION;
                frmCertificados.ESTADOCIVILREPRESENTANTE = frmComprobarIdentidadRepresentante.ESTADOCIVIL;
                frmCertificados.NACIONALIDADREPRESENTANTE = frmComprobarIdentidadRepresentante.NACIONALIDAD;
                frmCertificados.CORREOREPRESENTANTE = RequestCertificate.GetItemFromJArray(personDataRepresentante, "PersonalEmail").ToString();
                ActualizarRepresentantes.UpdateRepresentantes();
            }
              
        }

        private void lueTipoDocumentoPj_EditValueChanged(object sender, EventArgs e)
        {
            txtNumeroDocumentoPn.Text = null;
            if (Convert.ToInt32(lueTipoDocumentoPj.EditValue) == (int)DOCUMENTTYPELN.DocumentType.IDENTIDAD)
            {
                txtNumeroDocumentoPn.Properties.Mask.EditMask = "0000-0000-00000";
                txtNumeroDocumentoPn.Properties.Mask.MaskType = MaskType.Simple;
            }
            else if (Convert.ToInt32(lueTipoDocumentoPj.EditValue) == (int)DOCUMENTTYPELN.DocumentType.RTN)
            {
                txtNumeroDocumentoPn.Properties.Mask.EditMask = "0000-0000-000000";
                txtNumeroDocumentoPn.Properties.Mask.MaskType = MaskType.Simple;
            }
            else
            {
                txtNumeroDocumentoPn.Properties.Mask.MaskType = MaskType.None;
                txtNumeroDocumentoPn.Properties.CharacterCasing = CharacterCasing.Upper;
            }
            txtNumeroDocumentoPn.Focus();
        }
    }
}