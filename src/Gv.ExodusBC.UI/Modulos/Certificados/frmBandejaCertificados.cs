﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Newtonsoft.Json.Linq;
using static Gv.ExodusBc.UI.ExodusBcBase;
using Gv.ExodusBc.EN;
using static Gv.ExodusBc.EN.Request;
using Newtonsoft.Json;
using System.Net;
using System.IO;
using Gv.Utilidades;
using DevExpress.Utils.Menu;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;

namespace Gv.ExodusBc.UI.Modulos.Certificados
{
    public partial class frmBandejaCertificados : DevExpress.XtraEditors.XtraForm
    {
        public List<EN.GridRequestBandeja> listaSolicitudes = new List<EN.GridRequestBandeja>();
        DateTime _fechaminima;
        public frmBandejaCertificados()
        {
            InitializeComponent();
        }

        private void frmBandejaCertificados_Load(object sender, EventArgs e)
        {
            ssForm.ShowWaitForm();
            dteFechaInicial.EditValue = DateTime.Now;
            dteFechaFinal.EditValue = DateTime.Now;
            CargarSolicitudesAprobadas();
            ssForm.CloseWaitForm();
        }

        public void CargarSolicitudesAprobadas()
        {
            gcRequest.DataSource = null;
            JArray requestTray = new JArray();

            if(VariablesGlobales.USER_ID == 1)
                requestTray = RequestCertificate.GetRequestBandejaCertificadosTodas(Convert.ToDateTime(dteFechaInicial.EditValue), Convert.ToDateTime(dteFechaFinal.EditValue));
            else requestTray = RequestCertificate.GetRequestBandejaCertificados(VariablesGlobales.USER_ID, VariablesGlobales.OFFICE_ID, Convert.ToDateTime(dteFechaInicial.EditValue), Convert.ToDateTime(dteFechaFinal.EditValue));
            
            if(requestTray != null)
            {
                if (requestTray.Count > 0)
                {
                    var list = new List<EN.GridRequestBandeja>();
                    foreach (JObject element in requestTray)
                    {
                        EN.GridRequestBandeja request = new EN.GridRequestBandeja
                        {
                            TICKET = element["TICKET"].ToString(),
                            PRODUCTO = element["PRODUCTO"].ToString(),
                            NOMBRES = element["NOMBRES"].ToString(),
                            APELLIDOS = element["APELLIDOS"].ToString(),
                            SEGUIMIENTO = element["SOLICITUD"].ToString(),
                            FACTURA = element["FACTURA"].ToString(),
                            IDSTATUS = Convert.ToInt32(element["FKRequestStatusId"].ToString()),
                            TRANSACCIONSTATUS = Convert.ToInt32(element["FKTransactionStatusId"].ToString()),
                            RequestId = Convert.ToInt32(element["RequestId"].ToString()),
                            FECHACREACION = Convert.ToDateTime(element["CreationDate"].ToString())

                        };
                        if (request.FACTURA == "") request.FACTURA = "PENDIENTE";
                        if (request.TRANSACCIONSTATUS == (int)Request.TransactionStatus.FACTURAENVIADA)
                            request.FACTURA = "FACTURA ENVIADA";
                        if (request.TICKET == "") request.TICKET = "CERTIFICADO PENDIENTE DE LA SOLICITUD";
                        if (request.IDSTATUS > 0 && request.IDSTATUS == (int)RequestStatusList.CERTIFICADOEMITIDO)
                            request.SEGUIMIENTO = "CERTIFICADO EMITIDO, PENDIENTE DE INSTALACIÓN EN LA AR";
                        if (request.IDSTATUS > 0 && request.IDSTATUS == (int)RequestStatusList.CERTIFICADOINSTALADO)
                            request.SEGUIMIENTO = "CERTIFICADO INSTALADO CON ÉXITO";
                        if (request.IDSTATUS > 0 && request.IDSTATUS == (int)RequestStatusList.CERTIFICADOREVOCADOATITULAR)
                            request.SEGUIMIENTO = "CERTIFICADO REVOCADO POR EL TITULAR";
                        if (request.IDSTATUS > 0 && request.IDSTATUS == (int)RequestStatusList.CERTIFICADOREVOCADOAGENTE)
                            request.SEGUIMIENTO = "CERTIFICADO REVOCADO POR UN AGENTE DE REGISTRO";
                        if (request.IDSTATUS > 0 && request.IDSTATUS == (int)RequestStatusList.SOLICITUDRECHAZADA)
                            request.SEGUIMIENTO = "SOLICITUD RECHAZADA EN LA EMISIÓN POR VALID";
                        if (request.TRANSACCIONSTATUS == (int)Request.TransactionStatus.FACTURAPAGADA)
                            request.FACTURA = element["TRANSACCION"].ToString();

                        if (element["DocumentNumberLegalPerson"].ToString() as string != string.Empty && element["NameLegalPerson"].ToString() as string != string.Empty)
                        {
                            request.NOMBRES = element["NameLegalPerson"].ToString();
                            request.APELLIDOS = string.Empty;
                        }

                        list.Add(request);
                        listaSolicitudes.Add(request);
                    }
                    gcRequest.DataSource = list;
                }
            }
           
        }

        public void ConsultarEstadosValid()
        {
            try
            {
                ssForm.ShowWaitForm();

                if(listaSolicitudes.Count > 0)
                {
                    JObject json = new JObject();
                    DateTime fechainicio = dteFechaInicial.DateTime.Date;
                    DateTime fechafinal = dteFechaFinal.DateTime.Date.AddDays(-1);
                    string _fi = fechainicio.ToString("dd/MM/yyyy") + " 00:00:00";
                    string _ff = fechafinal.ToString("dd/MM/yyyy") + " 22:00:00";
                    DateTime fecha1 = Convert.ToDateTime(_fi);
                    DateTime fecha2 = Convert.ToDateTime(_ff);
                    string fi = fecha1.ToString("yyyy-MM-dd'T'HH:mm:sss'Z'");
                    string ff = fecha2.ToString("yyyy-MM-dd'T'HH:mm:sss'Z'");
                    JProperty lenguage = new JProperty("language", LN.SETTINGSLN.GetSettingsValor("IDIOMA"));
                    JProperty dateBegin = new JProperty("dateBegin", fi);
                    JProperty dateEnd = new JProperty("dateEnd", ff);
                    json.Add(lenguage);
                    json.Add(dateBegin);
                    json.Add(dateEnd);

                    //**ARCHIVO DE SALIDA**
                    string jsonOut = JsonConvert.SerializeObject(json, Formatting.None);

                    string result = string.Empty;
                    result = ResponseRequestValid(jsonOut);
                    if (result != string.Empty)
                    {
                        foreach (var solicitud in listaSolicitudes)
                        {
                            JObject jObj = (JObject)JsonConvert.DeserializeObject(result);
                            foreach (var rq in jObj)
                            {
                                if (rq.Key == "listUpdatedGlobalRequestStatus")
                                {
                                    if (rq.Value.Count() > 0)
                                    {
                                        foreach (JObject element in rq.Value)
                                        {
                                            foreach (var item in element)
                                            {
                                                if (item.Key == "ticket")
                                                {
                                                    if (item.Value.ToString() == solicitud.TICKET)
                                                    {
                                                        if (element["requestStatus"] != null)
                                                        {
                                                            var request = JObject.FromObject(element["requestStatus"]);
                                                            int id = int.Parse(request.GetValue("idRequestStatus").ToString());
                                                            string name = request.GetValue("name").ToString();
                                                            RequestCertificate.UpdateStatusRequestValid(id, solicitud.RequestId);

                                                            if (solicitud.IDSTATUS != id) GuardarSeguimientoCertificado(id, solicitud.RequestId, solicitud.IDSTATUS, name);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        CargarSolicitudesAprobadas();
                        ssForm.CloseWaitForm();
                    }
                }
                else
                {
                    ssForm.CloseWaitForm();
                    XtraMessageBox.Show("No existen certificados emitidos.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }
            catch (Exception ex)
            {
                if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                XtraMessageBox.Show("Hubo un error al tratar de mostrar los estados del certificado.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmBandejaCertificados::ConsultarEstadosValid", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        public string ResponseRequestValid(string json)
        {
            string result = string.Empty;
            string url = string.Empty;
            url = "http://ar.tecnisign.net/ra-ws-rest/updatedGlobalRequestStatus";

            //url = "http://hml-ar-global.validcertificadora.com.br/ra-ws-rest/updatedGlobalRequestStatus";

            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.ProtocolVersion = HttpVersion.Version10;
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                streamWriter.Write(json);

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();

            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                result = streamReader.ReadToEnd();

            return result;
        }

        void GuardarSeguimientoCertificado(int rqId, int status, int statusPrevious, string observacion)
        {
            //**GUARDAR SEGUIMIENTO DEL CERTIFICADO**
            EN.RequestFollowValid rqVd = new EN.RequestFollowValid();
            rqVd.Observation = observacion;
            rqVd.FKRequestId = rqId;
            rqVd.FKRequestStatusId = status;
            rqVd.FKCreateUserId = Convert.ToInt32(VariablesGlobales.USER_ID);
            rqVd.FKOfficeId = Convert.ToInt32(VariablesGlobales.OFFICE_ID);
            rqVd.FKRequestStatusPreviousId = statusPrevious;
            rqVd.FKWorkstationId = Convert.ToInt32(VariablesGlobales.WS_ID);
            var insertRqFallow = RequestCertificate.InsertNewRequestFollowValid(rqVd);
            if (!insertRqFallow)
            {
                Log.InsertarLog(Log.ErrorType.Error, "frmBandejaCertificados::GuardarSeguimientoCertificado", "Error al guardar el seguimiento.", VariablesGlobales.PathDataLog);
            }
        }

        private void btnRefrescar_Click(object sender, EventArgs e)
        {
            if (dteFechaInicial.EditValue.ToString() == string.Empty)
            {
                XtraMessageBox.Show("La fecha inicial es obligatoria.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (dteFechaFinal.EditValue.ToString() == string.Empty)
            {
                XtraMessageBox.Show("La fecha final es obligatoria", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (Convert.ToDateTime(dteFechaInicial.EditValue).Date > Convert.ToDateTime(dteFechaFinal.EditValue).Date)
            {
                XtraMessageBox.Show("La fecha de inicio no puede ser mayor a la fecha final.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            DateTime fechainicio = dteFechaInicial.DateTime.Date;
            DateTime fechafinal = dteFechaFinal.DateTime.Date;
            TimeSpan t = fechafinal - fechainicio;
            double tDias = t.TotalDays;
            if(tDias > 30)
            {
                XtraMessageBox.Show("La consulta para los estado en Valid, debe ser igual o menor a 30 dias.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }


            CargarSolicitudesAprobadas();
            ConsultarEstadosValid();
        }

        private void gvRequest_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            if (!e.HitInfo.InDataRow) return;
            DevExpress.Utils.ImageCollection imageList = DevExpress.Utils.Controls.ImageHelper.CreateImageCollectionFromResources("DevExpress.XtraEditors.Images.TextEditMenu.png", typeof(TextEdit).Assembly, new Size(16, 16), Color.Empty);
            DXMenuItem item = new DXMenuItem("Copiar", item_Click, imageList.Images[2]);
                item.Tag = e.HitInfo;
                e.Menu.Items.Add(item);
        }

        void item_Click(object sender, EventArgs e)
        {
            DXMenuItem item = sender as DXMenuItem;
            GridHitInfo info = item.Tag as GridHitInfo;
            if (info.Column !=null) Clipboard.SetText(gvRequest.GetRowCellDisplayText(info.RowHandle, info.Column));
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            if (dteFechaInicial.EditValue.ToString() == String.Empty)
                XtraMessageBox.Show("La fecha inicial es obligatoria.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            else if (dteFechaFinal.EditValue.ToString() == String.Empty)
                XtraMessageBox.Show("La fecha final es obligatoria.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            else if (Convert.ToDateTime(dteFechaInicial.EditValue).Date > Convert.ToDateTime(dteFechaFinal.EditValue).Date)
                XtraMessageBox.Show("La fecha de inicio no puede ser mayor a la fecha final.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            else
            {
                try
                {
                    ssForm.ShowWaitForm();
                    CargarSolicitudesAprobadas();
                    ssForm.CloseWaitForm();
                }
                catch (Exception ex)
                {
                    if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                    XtraMessageBox.Show("Hubo un error al buscar las solicitudes aprobadas.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Log.InsertarLog(Log.ErrorType.Error, "frmBandejaSeguimientoCertificados::btnBuscar_Click", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                }
            }
        }

        private void gvRequest_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            try
            {
                if (e.RowHandle >= 0)
                {
                    e.Appearance.Font = new Font("Segoe UI Semibold", 8, FontStyle.Bold);
                    e.Appearance.ForeColor = Color.FromArgb(55, 71, 79);
                    e.Appearance.BackColor = Color.FromArgb(241, 249, 247);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Hubo un error al tratar de visualizar las solicitudes.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmBandeja::gvRequest_RowCellStyle", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }
    }
}