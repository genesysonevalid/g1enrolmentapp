﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.IO;
using Gv.Utilidades;

namespace Gv.ExodusBc.UI.Modulos.Certificados
{
    public partial class frmVisorDocumentosAdjuntos : DevExpress.XtraEditors.XtraForm
    {
        public static byte[] Archivo;

        public frmVisorDocumentosAdjuntos()
        {
            InitializeComponent();
        }

        private void frmVisorDocumentosAdjuntos_Load(object sender, EventArgs e)
        {
            try
            {
                ssForm.ShowWaitForm();
                MemoryStream streamPDF = new MemoryStream(Archivo);
                pdfViewerDocs.LoadDocument(streamPDF);
                pdfViewerDocs.ZoomFactor = 100f;
                pdfViewerDocs.NavigationPaneInitialVisibility = DevExpress.XtraPdfViewer.PdfNavigationPaneVisibility.Hidden;
                pdfViewerDocs.ZoomMode = DevExpress.XtraPdfViewer.PdfZoomMode.PageLevel;
                ssForm.CloseWaitForm();
            }
            catch (Exception ex)
            {
                if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                XtraMessageBox.Show("Error al visualizar los documentos.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmVisorDocumentos::frmVisorDocumentosAdjuntos_Load", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        private void pdfViewerDocs_PopupMenuShowing(object sender, DevExpress.XtraPdfViewer.PdfPopupMenuShowingEventArgs e)
        {
            e.ItemLinks.Clear();
        }
    }
}