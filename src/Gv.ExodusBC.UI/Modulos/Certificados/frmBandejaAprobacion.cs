﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Newtonsoft.Json.Linq;
using static Gv.ExodusBc.UI.ExodusBcBase;
using Gv.Utilidades;
using static Gv.ExodusBc.EN.Request;

namespace Gv.ExodusBc.UI.Modulos.Certificados
{
    public partial class frmBandejaAprobacion : XtraForm
    {
        JArray requestTray = null;


        #region VALORES INICIALES DEL FORMULARIO
        public frmBandejaAprobacion()
        {
            InitializeComponent();
        }

        private void frmBandejaAprobacion_Load(object sender, EventArgs e)
        {
            try
            {
                ssForm.ShowWaitForm();
                if (VariablesGlobales.PERMISO_VerOficinasAprobacion)
                {
                    lueOficinas.Enabled = true;
                    ExodusBcBase.Helper.Combobox.setComboOffice(lueOficinas);
                    lueOficinas.EditValue = Convert.ToInt64(VariablesGlobales.OFFICE_ID);
                }
                else
                {
                    ExodusBcBase.Helper.Combobox.setComboOffice(lueOficinas);
                    lueOficinas.EditValue = Convert.ToInt64(VariablesGlobales.OFFICE_ID);
                    lueOficinas.Enabled = false;
                }
              
                CargarSolicitudesCreadas(Convert.ToInt32(lueOficinas.EditValue));
                ssForm.CloseWaitForm();
            }
            catch (Exception ex)
            {
                if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                XtraMessageBox.Show("Hubo un error al cargar las solicitudes. Intente en otro momento.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmBandeja::frmBandeja_Load", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        #endregion

        #region FUNCIONES DE LOS CONTROLES

        private void btnSalir_Click(object sender, EventArgs e)
        {
            frmMain2 openForm = (frmMain2)Application.OpenForms["frmMain2"];
            openForm.OpenForms("frmBandejaAprobacion");
            CerrarBandejaAprobacion();
        }

        private void btnAprobarSolicitud_Click(object sender, EventArgs e)
        {
           

        }

        private void btnRefrescar_Click(object sender, EventArgs e)
        {
            try
            {
                ssForm.ShowWaitForm();
                CargarSolicitudesCreadas(Convert.ToInt32(lueOficinas.EditValue));
                ssForm.CloseWaitForm();
            }
            catch (Exception ex)
            {
                if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                XtraMessageBox.Show("Hubo un error al cargar las solicitudes. Intente en otro momento.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmBandeja::btnRefrescar_Click", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        private void btnAprobarSolicitud_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            int rqId = Convert.ToInt32(gvRequestAprobacion.GetRowCellValue(gvRequestAprobacion.FocusedRowHandle, "RequestId"));
            if (rqId != -1)
            {
                frmDetalleSolicitud.REQUESTID = rqId;
                frmDetalleSolicitud frm = new frmDetalleSolicitud();
                frm.ShowDialog();
            }
        }

        #endregion

        #region METODOS Y PROCEDIMIENTOS
   
        public void CargarSolicitudesCreadas(int OficinaId)
        {
            requestTray = new JArray();
            requestTray = RequestCertificate.GetRequestByStatusId(OficinaId);
            gcRequestAprobacion.DataSource = null;

            if(requestTray != null)
            {
                if (requestTray.Count > 0)
                {
                    var list = new List<EN.GridRequest>();
                    foreach (JObject element in requestTray)
                    {
                        EN.GridRequest request = new EN.GridRequest
                        {
                            IDENTIFICACION = element["IDENTIDAD"].ToString(),
                            NOMBRES = element["NOMBRES"].ToString(),
                            APELLIDOS = element["APELLIDOS"].ToString(),
                            SEGUIMIENTO = element["SEGUIMIENTO"].ToString(),
                            RequestId = Convert.ToInt32(element["RequestId"].ToString())
                        };

                        if (element["DocumentNumberLegalPerson"].ToString() as string != string.Empty && element["NameLegalPerson"].ToString() as string != string.Empty)
                        {
                            request.IDENTIFICACION = element["DocumentNumberLegalPerson"].ToString();
                            request.NOMBRES = element["NameLegalPerson"].ToString();
                            request.APELLIDOS = string.Empty;
                        }

                        list.Add(request);
                    }
                    gcRequestAprobacion.DataSource = list;
                    gvRequestAprobacion.Columns[4].Visible = false;
                }
            }
        }

        public void CerrarBandejaAprobacion()
        {
            Close();
        }

        #endregion

        private void lueOficinas_EditValueChanged(object sender, EventArgs e)
        {
            if (lueOficinas.EditValue != null)
            {
                CargarSolicitudesCreadas(Convert.ToInt32(lueOficinas.EditValue));
            }
        }

        private void gvRequestAprobacion_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            try
            {
                if (e.RowHandle >= 0)
                {
                    e.Appearance.Font = new Font("Segoe UI Semibold", 10, FontStyle.Bold);
                    e.Appearance.ForeColor = Color.FromArgb(55, 71, 79);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Hubo un error al tratar de visualizar las solicitudes.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmBandeja::gvRequestAprobacion_RowCellStyle", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }
    }
}