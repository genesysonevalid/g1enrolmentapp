﻿namespace Gv.ExodusBc.UI.Modulos.Certificados
{
    partial class frmDetalleRechazo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDetalleRechazo));
            this.lblFechaRechazo = new DevExpress.XtraEditors.LabelControl();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.lblUsuarioRechazo = new DevExpress.XtraEditors.LabelControl();
            this.lbl5 = new DevExpress.XtraEditors.LabelControl();
            this.lblFechaCreacion = new DevExpress.XtraEditors.LabelControl();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.lblOficinaCapturo = new DevExpress.XtraEditors.LabelControl();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.lblUsuarioCapturo = new DevExpress.XtraEditors.LabelControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.lblTipoPersona = new DevExpress.XtraEditors.LabelControl();
            this.lblTipoDocumento = new DevExpress.XtraEditors.LabelControl();
            this.lblApellidos = new DevExpress.XtraEditors.LabelControl();
            this.lblNombres = new DevExpress.XtraEditors.LabelControl();
            this.lblNumeroDocumento = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.meMotivoRechazo = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.meTipoProducto = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.lblEstadoSolicitud = new DevExpress.XtraEditors.LabelControl();
            this.ssForm = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::Gv.ExodusBc.UI.frmWaitForm), true, true);
            ((System.ComponentModel.ISupportInitialize)(this.meMotivoRechazo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meTipoProducto.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // lblFechaRechazo
            // 
            this.lblFechaRechazo.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFechaRechazo.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblFechaRechazo.Appearance.Options.UseFont = true;
            this.lblFechaRechazo.Appearance.Options.UseForeColor = true;
            this.lblFechaRechazo.Appearance.Options.UseTextOptions = true;
            this.lblFechaRechazo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblFechaRechazo.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblFechaRechazo.LineColor = System.Drawing.Color.Gainsboro;
            this.lblFechaRechazo.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblFechaRechazo.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblFechaRechazo.LineVisible = true;
            this.lblFechaRechazo.Location = new System.Drawing.Point(517, 324);
            this.lblFechaRechazo.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblFechaRechazo.Name = "lblFechaRechazo";
            this.lblFechaRechazo.Size = new System.Drawing.Size(461, 26);
            this.lblFechaRechazo.TabIndex = 501;
            this.lblFechaRechazo.Text = "[lblFechaRechazo]";
            // 
            // labelControl18
            // 
            this.labelControl18.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl18.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl18.Appearance.Options.UseFont = true;
            this.labelControl18.Appearance.Options.UseForeColor = true;
            this.labelControl18.Appearance.Options.UseTextOptions = true;
            this.labelControl18.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl18.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl18.Location = new System.Drawing.Point(517, 300);
            this.labelControl18.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(461, 26);
            this.labelControl18.TabIndex = 500;
            this.labelControl18.Text = "Fecha de rechazo:";
            // 
            // lblUsuarioRechazo
            // 
            this.lblUsuarioRechazo.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsuarioRechazo.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblUsuarioRechazo.Appearance.Options.UseFont = true;
            this.lblUsuarioRechazo.Appearance.Options.UseForeColor = true;
            this.lblUsuarioRechazo.Appearance.Options.UseTextOptions = true;
            this.lblUsuarioRechazo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblUsuarioRechazo.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblUsuarioRechazo.LineColor = System.Drawing.Color.Gainsboro;
            this.lblUsuarioRechazo.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblUsuarioRechazo.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblUsuarioRechazo.LineVisible = true;
            this.lblUsuarioRechazo.Location = new System.Drawing.Point(517, 266);
            this.lblUsuarioRechazo.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblUsuarioRechazo.Name = "lblUsuarioRechazo";
            this.lblUsuarioRechazo.Size = new System.Drawing.Size(461, 26);
            this.lblUsuarioRechazo.TabIndex = 499;
            this.lblUsuarioRechazo.Text = "[lblUsuarioRechazo]";
            // 
            // lbl5
            // 
            this.lbl5.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl5.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lbl5.Appearance.Options.UseFont = true;
            this.lbl5.Appearance.Options.UseForeColor = true;
            this.lbl5.Appearance.Options.UseTextOptions = true;
            this.lbl5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lbl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl5.Location = new System.Drawing.Point(517, 242);
            this.lbl5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lbl5.Name = "lbl5";
            this.lbl5.Size = new System.Drawing.Size(461, 26);
            this.lbl5.TabIndex = 498;
            this.lbl5.Text = "Usuario que rechazó:";
            // 
            // lblFechaCreacion
            // 
            this.lblFechaCreacion.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFechaCreacion.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblFechaCreacion.Appearance.Options.UseFont = true;
            this.lblFechaCreacion.Appearance.Options.UseForeColor = true;
            this.lblFechaCreacion.Appearance.Options.UseTextOptions = true;
            this.lblFechaCreacion.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblFechaCreacion.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblFechaCreacion.LineColor = System.Drawing.Color.Gainsboro;
            this.lblFechaCreacion.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblFechaCreacion.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblFechaCreacion.LineVisible = true;
            this.lblFechaCreacion.Location = new System.Drawing.Point(517, 208);
            this.lblFechaCreacion.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblFechaCreacion.Name = "lblFechaCreacion";
            this.lblFechaCreacion.Size = new System.Drawing.Size(461, 26);
            this.lblFechaCreacion.TabIndex = 497;
            this.lblFechaCreacion.Text = "[lblFechaCreacion]";
            // 
            // labelControl17
            // 
            this.labelControl17.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl17.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl17.Appearance.Options.UseFont = true;
            this.labelControl17.Appearance.Options.UseForeColor = true;
            this.labelControl17.Appearance.Options.UseTextOptions = true;
            this.labelControl17.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl17.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl17.Location = new System.Drawing.Point(517, 188);
            this.labelControl17.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(461, 26);
            this.labelControl17.TabIndex = 496;
            this.labelControl17.Text = "Fecha de creación:";
            // 
            // lblOficinaCapturo
            // 
            this.lblOficinaCapturo.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOficinaCapturo.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblOficinaCapturo.Appearance.Options.UseFont = true;
            this.lblOficinaCapturo.Appearance.Options.UseForeColor = true;
            this.lblOficinaCapturo.Appearance.Options.UseTextOptions = true;
            this.lblOficinaCapturo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblOficinaCapturo.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblOficinaCapturo.LineColor = System.Drawing.Color.Gainsboro;
            this.lblOficinaCapturo.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblOficinaCapturo.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblOficinaCapturo.LineVisible = true;
            this.lblOficinaCapturo.Location = new System.Drawing.Point(517, 86);
            this.lblOficinaCapturo.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblOficinaCapturo.Name = "lblOficinaCapturo";
            this.lblOficinaCapturo.Size = new System.Drawing.Size(461, 26);
            this.lblOficinaCapturo.TabIndex = 495;
            this.lblOficinaCapturo.Text = "[lblOficinaCapturo]";
            // 
            // labelControl16
            // 
            this.labelControl16.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl16.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl16.Appearance.Options.UseFont = true;
            this.labelControl16.Appearance.Options.UseForeColor = true;
            this.labelControl16.Appearance.Options.UseTextOptions = true;
            this.labelControl16.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl16.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl16.Location = new System.Drawing.Point(517, 60);
            this.labelControl16.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(461, 26);
            this.labelControl16.TabIndex = 494;
            this.labelControl16.Text = "Oficina que capturo:";
            // 
            // lblUsuarioCapturo
            // 
            this.lblUsuarioCapturo.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsuarioCapturo.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblUsuarioCapturo.Appearance.Options.UseFont = true;
            this.lblUsuarioCapturo.Appearance.Options.UseForeColor = true;
            this.lblUsuarioCapturo.Appearance.Options.UseTextOptions = true;
            this.lblUsuarioCapturo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblUsuarioCapturo.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblUsuarioCapturo.LineColor = System.Drawing.Color.Gainsboro;
            this.lblUsuarioCapturo.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblUsuarioCapturo.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblUsuarioCapturo.LineVisible = true;
            this.lblUsuarioCapturo.Location = new System.Drawing.Point(517, 150);
            this.lblUsuarioCapturo.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblUsuarioCapturo.Name = "lblUsuarioCapturo";
            this.lblUsuarioCapturo.Size = new System.Drawing.Size(461, 26);
            this.lblUsuarioCapturo.TabIndex = 493;
            this.lblUsuarioCapturo.Text = "[lblUsuarioCapturo]";
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl15.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl15.Appearance.Options.UseFont = true;
            this.labelControl15.Appearance.Options.UseForeColor = true;
            this.labelControl15.Appearance.Options.UseTextOptions = true;
            this.labelControl15.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl15.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl15.Location = new System.Drawing.Point(517, 126);
            this.labelControl15.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(461, 26);
            this.labelControl15.TabIndex = 492;
            this.labelControl15.Text = "Usuario que capturo:";
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.Font = new System.Drawing.Font("Segoe UI Light", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl14.Appearance.ForeColor = System.Drawing.Color.DimGray;
            this.labelControl14.Appearance.Options.UseFont = true;
            this.labelControl14.Appearance.Options.UseForeColor = true;
            this.labelControl14.Appearance.Options.UseTextOptions = true;
            this.labelControl14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl14.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl14.LineColor = System.Drawing.Color.DarkGray;
            this.labelControl14.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.labelControl14.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.labelControl14.LineVisible = true;
            this.labelControl14.Location = new System.Drawing.Point(517, 11);
            this.labelControl14.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(461, 41);
            this.labelControl14.TabIndex = 491;
            this.labelControl14.Text = "Información de la transacción";
            // 
            // lblTipoPersona
            // 
            this.lblTipoPersona.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipoPersona.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblTipoPersona.Appearance.Options.UseFont = true;
            this.lblTipoPersona.Appearance.Options.UseForeColor = true;
            this.lblTipoPersona.Appearance.Options.UseTextOptions = true;
            this.lblTipoPersona.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblTipoPersona.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblTipoPersona.LineColor = System.Drawing.Color.Gainsboro;
            this.lblTipoPersona.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblTipoPersona.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblTipoPersona.LineVisible = true;
            this.lblTipoPersona.Location = new System.Drawing.Point(17, 150);
            this.lblTipoPersona.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblTipoPersona.Name = "lblTipoPersona";
            this.lblTipoPersona.Size = new System.Drawing.Size(455, 26);
            this.lblTipoPersona.TabIndex = 490;
            this.lblTipoPersona.Text = "[lblTipoPersona]";
            // 
            // lblTipoDocumento
            // 
            this.lblTipoDocumento.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipoDocumento.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblTipoDocumento.Appearance.Options.UseFont = true;
            this.lblTipoDocumento.Appearance.Options.UseForeColor = true;
            this.lblTipoDocumento.Appearance.Options.UseTextOptions = true;
            this.lblTipoDocumento.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblTipoDocumento.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblTipoDocumento.LineColor = System.Drawing.Color.Gainsboro;
            this.lblTipoDocumento.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblTipoDocumento.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblTipoDocumento.LineVisible = true;
            this.lblTipoDocumento.Location = new System.Drawing.Point(17, 208);
            this.lblTipoDocumento.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblTipoDocumento.Name = "lblTipoDocumento";
            this.lblTipoDocumento.Size = new System.Drawing.Size(454, 26);
            this.lblTipoDocumento.TabIndex = 488;
            this.lblTipoDocumento.Text = "[lblTipoDocumento]";
            // 
            // lblApellidos
            // 
            this.lblApellidos.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblApellidos.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblApellidos.Appearance.Options.UseFont = true;
            this.lblApellidos.Appearance.Options.UseForeColor = true;
            this.lblApellidos.Appearance.Options.UseTextOptions = true;
            this.lblApellidos.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblApellidos.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblApellidos.LineColor = System.Drawing.Color.Gainsboro;
            this.lblApellidos.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblApellidos.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblApellidos.LineVisible = true;
            this.lblApellidos.Location = new System.Drawing.Point(17, 382);
            this.lblApellidos.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblApellidos.Name = "lblApellidos";
            this.lblApellidos.Size = new System.Drawing.Size(455, 26);
            this.lblApellidos.TabIndex = 486;
            this.lblApellidos.Text = "[lblApellidos]";
            // 
            // lblNombres
            // 
            this.lblNombres.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombres.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblNombres.Appearance.Options.UseFont = true;
            this.lblNombres.Appearance.Options.UseForeColor = true;
            this.lblNombres.Appearance.Options.UseTextOptions = true;
            this.lblNombres.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblNombres.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblNombres.LineColor = System.Drawing.Color.Gainsboro;
            this.lblNombres.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblNombres.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblNombres.LineVisible = true;
            this.lblNombres.Location = new System.Drawing.Point(17, 324);
            this.lblNombres.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblNombres.Name = "lblNombres";
            this.lblNombres.Size = new System.Drawing.Size(455, 26);
            this.lblNombres.TabIndex = 485;
            this.lblNombres.Text = "[lblNombres]";
            // 
            // lblNumeroDocumento
            // 
            this.lblNumeroDocumento.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumeroDocumento.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblNumeroDocumento.Appearance.Options.UseFont = true;
            this.lblNumeroDocumento.Appearance.Options.UseForeColor = true;
            this.lblNumeroDocumento.Appearance.Options.UseTextOptions = true;
            this.lblNumeroDocumento.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblNumeroDocumento.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblNumeroDocumento.LineColor = System.Drawing.Color.Gainsboro;
            this.lblNumeroDocumento.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblNumeroDocumento.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblNumeroDocumento.LineVisible = true;
            this.lblNumeroDocumento.Location = new System.Drawing.Point(17, 266);
            this.lblNumeroDocumento.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblNumeroDocumento.Name = "lblNumeroDocumento";
            this.lblNumeroDocumento.Size = new System.Drawing.Size(455, 26);
            this.lblNumeroDocumento.TabIndex = 482;
            this.lblNumeroDocumento.Text = "[lblNumeroDocumento]";
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Segoe UI Light", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl9.Appearance.ForeColor = System.Drawing.Color.DimGray;
            this.labelControl9.Appearance.Options.UseFont = true;
            this.labelControl9.Appearance.Options.UseForeColor = true;
            this.labelControl9.Appearance.Options.UseTextOptions = true;
            this.labelControl9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl9.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl9.LineColor = System.Drawing.Color.DarkGray;
            this.labelControl9.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.labelControl9.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.labelControl9.LineVisible = true;
            this.labelControl9.Location = new System.Drawing.Point(11, 11);
            this.labelControl9.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(461, 41);
            this.labelControl9.TabIndex = 480;
            this.labelControl9.Text = "Información del solicitante";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Appearance.Options.UseForeColor = true;
            this.labelControl1.Appearance.Options.UseTextOptions = true;
            this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.Location = new System.Drawing.Point(17, 126);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(455, 26);
            this.labelControl1.TabIndex = 506;
            this.labelControl1.Text = "Tipo de persona:";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Appearance.Options.UseForeColor = true;
            this.labelControl3.Appearance.Options.UseTextOptions = true;
            this.labelControl3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl3.Location = new System.Drawing.Point(17, 184);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(455, 26);
            this.labelControl3.TabIndex = 505;
            this.labelControl3.Text = "Tipo documento:";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Appearance.Options.UseForeColor = true;
            this.labelControl4.Appearance.Options.UseTextOptions = true;
            this.labelControl4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl4.Location = new System.Drawing.Point(17, 358);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(455, 26);
            this.labelControl4.TabIndex = 504;
            this.labelControl4.Text = "Apellidos:";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Appearance.Options.UseForeColor = true;
            this.labelControl5.Appearance.Options.UseTextOptions = true;
            this.labelControl5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl5.Location = new System.Drawing.Point(17, 300);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(455, 26);
            this.labelControl5.TabIndex = 503;
            this.labelControl5.Text = "Nombres:";
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl6.Appearance.Options.UseFont = true;
            this.labelControl6.Appearance.Options.UseForeColor = true;
            this.labelControl6.Appearance.Options.UseTextOptions = true;
            this.labelControl6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl6.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl6.Location = new System.Drawing.Point(17, 242);
            this.labelControl6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(455, 26);
            this.labelControl6.TabIndex = 502;
            this.labelControl6.Text = "Num. Documento:";
            // 
            // meMotivoRechazo
            // 
            this.meMotivoRechazo.Location = new System.Drawing.Point(17, 445);
            this.meMotivoRechazo.Margin = new System.Windows.Forms.Padding(2);
            this.meMotivoRechazo.Name = "meMotivoRechazo";
            this.meMotivoRechazo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.meMotivoRechazo.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.meMotivoRechazo.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.meMotivoRechazo.Properties.Appearance.Options.UseBackColor = true;
            this.meMotivoRechazo.Properties.Appearance.Options.UseFont = true;
            this.meMotivoRechazo.Properties.Appearance.Options.UseForeColor = true;
            this.meMotivoRechazo.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.meMotivoRechazo.Properties.ReadOnly = true;
            this.meMotivoRechazo.Size = new System.Drawing.Size(961, 53);
            this.meMotivoRechazo.TabIndex = 507;
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl7.Appearance.Options.UseFont = true;
            this.labelControl7.Appearance.Options.UseForeColor = true;
            this.labelControl7.Appearance.Options.UseTextOptions = true;
            this.labelControl7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl7.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl7.Location = new System.Drawing.Point(17, 421);
            this.labelControl7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(961, 26);
            this.labelControl7.TabIndex = 508;
            this.labelControl7.Text = "Motivo del rechazo:";
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl8.Appearance.Options.UseFont = true;
            this.labelControl8.Appearance.Options.UseForeColor = true;
            this.labelControl8.Appearance.Options.UseTextOptions = true;
            this.labelControl8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl8.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl8.Location = new System.Drawing.Point(17, 60);
            this.labelControl8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(445, 26);
            this.labelControl8.TabIndex = 509;
            this.labelControl8.Text = "Tipo de producto:";
            // 
            // meTipoProducto
            // 
            this.meTipoProducto.Location = new System.Drawing.Point(17, 87);
            this.meTipoProducto.Margin = new System.Windows.Forms.Padding(2);
            this.meTipoProducto.Name = "meTipoProducto";
            this.meTipoProducto.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.meTipoProducto.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.meTipoProducto.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.meTipoProducto.Properties.Appearance.Options.UseBackColor = true;
            this.meTipoProducto.Properties.Appearance.Options.UseFont = true;
            this.meTipoProducto.Properties.Appearance.Options.UseForeColor = true;
            this.meTipoProducto.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.meTipoProducto.Properties.ReadOnly = true;
            this.meTipoProducto.Size = new System.Drawing.Size(455, 40);
            this.meTipoProducto.TabIndex = 511;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Appearance.Options.UseForeColor = true;
            this.labelControl2.Appearance.Options.UseTextOptions = true;
            this.labelControl2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl2.Location = new System.Drawing.Point(517, 358);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(461, 26);
            this.labelControl2.TabIndex = 512;
            this.labelControl2.Text = "Estado actual de la solicitud:";
            // 
            // lblEstadoSolicitud
            // 
            this.lblEstadoSolicitud.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstadoSolicitud.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblEstadoSolicitud.Appearance.Options.UseFont = true;
            this.lblEstadoSolicitud.Appearance.Options.UseForeColor = true;
            this.lblEstadoSolicitud.Appearance.Options.UseTextOptions = true;
            this.lblEstadoSolicitud.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblEstadoSolicitud.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblEstadoSolicitud.LineColor = System.Drawing.Color.Gainsboro;
            this.lblEstadoSolicitud.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblEstadoSolicitud.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblEstadoSolicitud.LineVisible = true;
            this.lblEstadoSolicitud.Location = new System.Drawing.Point(517, 382);
            this.lblEstadoSolicitud.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblEstadoSolicitud.Name = "lblEstadoSolicitud";
            this.lblEstadoSolicitud.Size = new System.Drawing.Size(461, 26);
            this.lblEstadoSolicitud.TabIndex = 513;
            this.lblEstadoSolicitud.Text = "[lblEstadoSolicitud]";
            // 
            // ssForm
            // 
            this.ssForm.ClosingDelay = 500;
            // 
            // frmDetalleRechazo
            // 
            this.Appearance.BackColor = System.Drawing.Color.White;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(995, 508);
            this.Controls.Add(this.lblEstadoSolicitud);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.meTipoProducto);
            this.Controls.Add(this.labelControl8);
            this.Controls.Add(this.meMotivoRechazo);
            this.Controls.Add(this.labelControl7);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.lblFechaRechazo);
            this.Controls.Add(this.labelControl18);
            this.Controls.Add(this.lblUsuarioRechazo);
            this.Controls.Add(this.lbl5);
            this.Controls.Add(this.lblFechaCreacion);
            this.Controls.Add(this.labelControl17);
            this.Controls.Add(this.lblOficinaCapturo);
            this.Controls.Add(this.labelControl16);
            this.Controls.Add(this.lblUsuarioCapturo);
            this.Controls.Add(this.labelControl15);
            this.Controls.Add(this.labelControl14);
            this.Controls.Add(this.lblTipoPersona);
            this.Controls.Add(this.lblTipoDocumento);
            this.Controls.Add(this.lblApellidos);
            this.Controls.Add(this.lblNombres);
            this.Controls.Add(this.lblNumeroDocumento);
            this.Controls.Add(this.labelControl9);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDetalleRechazo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Detalle de la solicitud rechazada";
            ((System.ComponentModel.ISupportInitialize)(this.meMotivoRechazo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meTipoProducto.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl lblFechaRechazo;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.LabelControl lblUsuarioRechazo;
        private DevExpress.XtraEditors.LabelControl lbl5;
        private DevExpress.XtraEditors.LabelControl lblFechaCreacion;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.LabelControl lblOficinaCapturo;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.LabelControl lblUsuarioCapturo;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.LabelControl lblTipoPersona;
        private DevExpress.XtraEditors.LabelControl lblTipoDocumento;
        private DevExpress.XtraEditors.LabelControl lblApellidos;
        private DevExpress.XtraEditors.LabelControl lblNombres;
        private DevExpress.XtraEditors.LabelControl lblNumeroDocumento;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.MemoEdit meMotivoRechazo;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.MemoEdit meTipoProducto;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl lblEstadoSolicitud;
        private DevExpress.XtraSplashScreen.SplashScreenManager ssForm;
    }
}