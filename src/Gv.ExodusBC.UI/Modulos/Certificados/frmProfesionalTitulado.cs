﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using static Gv.ExodusBc.UI.ExodusBcBase;

namespace Gv.ExodusBc.UI.Modulos.Certificados
{
    public partial class frmProfesionalTitulado : DevExpress.XtraEditors.XtraForm
    {
        public static string PROFESION = string.Empty;

        public frmProfesionalTitulado()
        {
            InitializeComponent();
            Init();
        }

        void Init()
        {
            lblProfesion.Text = PROFESION;
            txtNumeroColegio.Text = frmCertificados.NUMEROCOLEGIO;
            lueColegioProfesional.EditValue = frmCertificados.INSTITUCIONEMITE;
            ExodusBcBase.Helper.Combobox.GetColegioProfesional(lueColegioProfesional);
            if (frmCertificados.INSTITUCIONEMITE_ID != 0) lueColegioProfesional.EditValue = frmCertificados.INSTITUCIONEMITE_ID;
            else lueColegioProfesional.EditValue = null;
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            if (lueColegioProfesional.Text == string.Empty)
            {
                XtraMessageBox.Show("El nombre de la institución emisora esta vacio, este campo es requerido", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                lueColegioProfesional.Focus();
                return;
            }

            if (txtNumeroColegio.Text == string.Empty)
            {
                XtraMessageBox.Show("El número de colegio esta vacio, este campo es requerido", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtNumeroColegio.Focus();
                return;
            }

            frmCertificados.NUMEROCOLEGIO = RequestCertificate.VerificarCaracteresEspeciales(txtNumeroColegio.Text.Trim());
            if(lueColegioProfesional.EditValue != null) frmCertificados.INSTITUCIONEMITE_ID = Convert.ToInt32(lueColegioProfesional.EditValue);
            frmCertificados.INSTITUCIONEMITE = RequestCertificate.VerificarCaracteresEspeciales(lueColegioProfesional.Text);
            frmCertificados.PROFESION = PROFESION;

            Close();
        }

        private void frmProfesionalTitulado_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (txtNumeroColegio.Text == string.Empty | lueColegioProfesional.EditValue == null)
            {
                frmCertificados.NUMEROCOLEGIO = RequestCertificate.VerificarCaracteresEspeciales(txtNumeroColegio.Text.Trim());
                XtraMessageBox.Show("No se realizará este tipo de certificado, no se digito el número de colegiación o la institución.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void lueColegioProfesional_CustomDrawCell(object sender, DevExpress.XtraEditors.Popup.LookUpCustomDrawCellArgs e)
        {
            if ("True".Equals(e.DisplayText))
            {
                e.DisplayText = "CONVENIO ACTIVO";
                e.Appearance.ForeColor = Color.Teal;
            }
            if ("False".Equals(e.DisplayText)) e.DisplayText = "SIN CONVENIO";
        }

        private void lueColegioProfesional_EditValueChanged(object sender, EventArgs e)
        {
            LookUpEdit editor = sender as LookUpEdit;
            object convenio = editor.GetColumnValue("Agreement");

            if (convenio != null)
            {
                if (!(bool)convenio)
                {
                    XtraMessageBox.Show("Actualmente esta institución, no tiene niungún convenio activo.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void frmProfesionalTitulado_Load(object sender, EventArgs e)
        {
            lueColegioProfesional.Select();
        }
    }
}