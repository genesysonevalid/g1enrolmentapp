﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;


namespace Gv.ExodusBc.UI.Modulos.Certificados
{
    public partial class frmAccionesRequest : XtraForm
    {
        public static string ACCION = string.Empty;
        

        public frmAccionesRequest(string titulo)
        {
            InitializeComponent();
            Text = titulo;
        }

        private void frmAccionesRequest_Load(object sender, EventArgs e)
        {
            frmDetalleSolicitud.observacionTransaccion = string.Empty;
            if (ACCION == "RECHAZO") ExodusBcBase.Helper.Combobox.SetComboReasonRejection(lueMotivos);
            else ExodusBcBase.Helper.Combobox.SetComboReasonReview(lueMotivos);

            lueMotivos.EditValue = null;
            lueMotivos.Select();
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            if(lueMotivos.EditValue == null)
            {
                XtraMessageBox.Show("Debe seleccionar un motivo de rechazo.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                lueMotivos.Focus();
                return;
            }
            if (meObservacion.Text.Trim() == string.Empty)
            {
                XtraMessageBox.Show("Debe digitar la observación.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                meObservacion.Focus();
                return;
            }
            else
            {
                frmDetalleSolicitud.observacionTransaccion = lueMotivos.Text + ": " + meObservacion.Text.Trim();
                Close();
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            ACCION = string.Empty;
            frmDetalleSolicitud.observacionTransaccion = string.Empty;
            Close();
        }

        private void frmAccionesRequest_FormClosing(object sender, FormClosingEventArgs e)
        {
           
        }
    }
}