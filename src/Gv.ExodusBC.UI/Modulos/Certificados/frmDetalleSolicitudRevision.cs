﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Newtonsoft.Json.Linq;
using static Gv.ExodusBc.UI.ExodusBcBase;
using System.IO;
using Gv.Utilidades;
using Gv.ExodusBc.UI.Modulos.Certificados.Reportes;
using static Gv.ExodusBc.EN.Request;
using Gv.ExodusBc.UI.Modulos.InspeccionPrimaria;
using Gv.ExodusBc.UI.WebReference;
using Gv.ExodusBc.LN;
using System.Collections;

namespace Gv.ExodusBc.UI.Modulos.Certificados
{
    public partial class frmDetalleSolicitudRevision : XtraForm
    {
        public static int REQUESTID = 0;
        public static int PERSONAID = 0;
        string DIRECCION = string.Empty;
        string TELEFONOOFICINA = string.Empty;
        public int TIPOPERSONA = 0;
        public static JArray requestById = null;
        byte[] imgPn = null;
        byte[] imgSolicitante = null;
        byte[] imgPj = null;
        Image imgDocSolicitante = default(Image);
        Image imgDocPn = default(Image);
        Image imgScanPreview = default(Image);
        public static string IDENTIDAD_REPRESENTANTE = string.Empty;
        string NombreRepresentante = string.Empty;
        public frmAsociarRepresentantes frmRpst;
        public static List<EN.RepresentativeRequest> lstRepresentantes = new List<EN.RepresentativeRequest>();
        private static ExodusBcService web = new ExodusBcService();
        //***Documentos adjuntos***
        string pathAttached = string.Empty;
        string pathRequest = string.Empty;
        public static List<EN.RequestAttach> ListaDocumentosAdjuntosRevision = null;
        public static List<EN.RequestAttach> lstAdjuntos = new List<EN.RequestAttach>();
        public static int TotalDocPersonaNatural = 0;
        public static int TotalDocPersonaJuridica = 0;
        public frmAdjuntos frmAdj;
        public frmCertificados frmCrt;
        public static JArray personDataRepresentantes = null;
        public frmDetalleSolicitudRevision(frmAsociarRepresentantes frmRpst, frmCertificados frmCrt, frmAdjuntos frmAdj)
        {
            InitializeComponent();
            this.frmRpst = frmRpst;
            this.frmCrt = frmCrt;
            this.frmAdj = frmAdj;
            Init();
        }

        void Init()
        {
            try
            {
                SetControles();
                ssForm.ShowWaitForm();
                requestById = RequestCertificate.GetRequestByIdRevision(REQUESTID);
                if (requestById.Count > 0 | requestById != null)
                {
                    foreach (JObject element in requestById)
                    {
                        if (Convert.ToInt32(element["TipoPersona"].ToString()) == (int)TypePerson.PRSJURIDICA)
                        {
                            TIPOPERSONA = (int)TypePerson.PRSJURIDICA;
                            PERSONAID = Convert.ToInt32(element["PersonId"].ToString());
                            DIRECCION = element["Colonia"].ToString() + ": " + element["Direccion"].ToString();
                            lueProductosPj.EditValue = element["PRODUCTOID"].ToString();
                            meDescripcionProductoPj.Text = element["DESCRIPCIONPRODUCTO"].ToString(); 
                            txtNombresPj.Text = element["NOMBREPJ"].ToString();
                            lblNumeroDocumentoPj.Text = element["IDENTIFICACIONPJ"].ToString();
                            txtCorreoPj.Text = element["LegalPersonEmail"].ToString();
                            txtTelefonoPj.Text = element["LegalPersonPhone"].ToString();
                            pnlPersonaJuridica.Visible = true;
                            pnlPersonaNatural.Visible = false;
                            GetAttached((int)TypePerson.PRSJURIDICA);
                            gvAdjuntosPj.Columns[2].Visible = false;
                            gvAdjuntosPj.Columns[3].Visible = false;
                            GetRepresentative(Convert.ToInt32(element["PersonId"].ToString()));
                            pnlPersonaJuridica.Location = new Point(20, 59);
                            btnEditarSolicitud.Location = new Point(30, 380);
                            Size = new Size(1360, 560);
                        }
                        else
                        {
                            TIPOPERSONA = (int)TypePerson.PRSNATURAL;
                            PERSONAID = Convert.ToInt32(element["PersonId"].ToString());
                            DIRECCION = element["Colonia"].ToString() + ": " + element["Direccion"].ToString();
                            TELEFONOOFICINA = element["TelefonoOficina"].ToString();
                            lueProductosPn.EditValue = element["PRODUCTOID"].ToString();
                            meDescripcionProductoPn.Text = element["DESCRIPCIONPRODUCTO"].ToString();
                            lblNumeroDocumentoPn.Text = element["IDENTIFICACIONPN"].ToString();
                            lblNombresPn.Text = element["NOMBRES"].ToString();
                            lblApellidosPn.Text = element["APELLIDOS"].ToString();
                            txtCorreoPn.Text = element["PersonalEmail"].ToString();
                            txtTelefonoPn.Text = element["PersonalMovilPhone"].ToString();
                            if(element["NUMEROCOLEGIO"].ToString() != "") txtNumeroColegio.Text = element["NUMEROCOLEGIO"].ToString();
                            else
                            {
                                lblNumeroColegioTitulo.Visible = false;
                                txtNumeroColegio.Visible = false;
                            }

                            //***Foto***
                            JArray arrayFoto = new JArray();
                            arrayFoto = RequestCertificate.GetLastPhoto(Convert.ToInt32(element["PersonId"].ToString()));
                            foreach (JObject elementPhoto in arrayFoto)
                                if (elementPhoto["Image"].ToString() != null)
                                {
                                    picSolicitante.EditValue = ExodusBcBase.Helper.byteToImage((byte[])elementPhoto["Image"]);
                                    imgSolicitante = (byte[])elementPhoto["Image"];
                                }

                            //***Adjuntos***
                            GetAttached((int)TypePerson.PRSNATURAL);

                            pnlPersonaJuridica.Visible = false;
                            pnlPersonaNatural.Visible = true;
                            gvAdjuntosPn.Columns[2].Visible = false; 
                        }
                    }
                }
                ssForm.CloseWaitForm();
            }
            catch (Exception ex)
            {
                if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                XtraMessageBox.Show("Hubo un error al tratar de cargar algunos valores iniciales.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmDetalleSolicitudRevision::Init", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        void SetControles()
        {
            lblNumeroDocumentoPn.Text = string.Empty;
            lblNombresPn.Text = string.Empty;
            lblNumeroDocumentoPj.Text = string.Empty;
            if(lstAdjuntos != null) if (lstAdjuntos.Count > 0) lstAdjuntos.Clear();
            if(lstRepresentantes != null) if (lstRepresentantes.Count > 0) lstRepresentantes.Clear();
            if(ListaDocumentosAdjuntosRevision != null) if (ListaDocumentosAdjuntosRevision.Count > 0) ListaDocumentosAdjuntosRevision.Clear();
        }

        private void meDireccionPj_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void frmDetalleSolicitudRevision_Load(object sender, EventArgs e)
        {
            ExodusBcBase.Helper.Combobox.setComboProducts(lueProductosPn);
            ExodusBcBase.Helper.Combobox.setComboProducts(lueProductosPj);
        }

        private void btnAdjuntarPn_Click(object sender, EventArgs e)
        {
            try
            {
                if (ValidarDocumentos((int)TypePerson.PRSNATURAL))
                {
                    rptPersonaNatural.PERSONAID = PERSONAID;
                    rptPersonaNatural.CLIENTE = lblNombresPn.Text + " " + lblApellidosPn.Text;
                    rptPersonaNatural.RESIDENCIA = DIRECCION;
                    rptPersonaNatural.TELEFONOOFICINA = TELEFONOOFICINA;
                    rptPersonaNatural.TELEFONOMOVIL = txtTelefonoPn.Text.Trim();
                    rptPersonaNatural.CORREO = txtCorreoPn.Text.Trim();
                    rptPersonaNatural.IDENTIDAD = lblNumeroDocumentoPn.Text; 

                    string producto = PRODUCTSLN.GetProductByDescription((int)TypePerson.PRSNATURAL, Convert.ToInt32(lueProductosPn.EditValue));
                    string listVigencia = VariablesGlobales.VIGENCIACERTIFICADOS;
                    if (producto != null)
                    {
                        if (producto.Contains("1 Año") && listVigencia.Contains("1")) rptPersonaNatural.VIGENCIA = (int)PRODUCTSLN.VigenciaCertificados.UNO;
                        else if (producto.Contains("2 Años") && listVigencia.Contains("2")) rptPersonaNatural.VIGENCIA = (int)PRODUCTSLN.VigenciaCertificados.DOS;
                        else if (producto.Contains("3 Años") && listVigencia.Contains("3")) rptPersonaNatural.VIGENCIA = (int)PRODUCTSLN.VigenciaCertificados.TRES;
                        else if (producto.Contains("4 Años") && listVigencia.Contains("4")) rptPersonaNatural.VIGENCIA = (int)PRODUCTSLN.VigenciaCertificados.CUATRO;
                        else rptPersonaNatural.VIGENCIA = (int)PRODUCTSLN.VigenciaCertificados.CINCO;
                    }
                    frmVisorReporte.NOMBRECLIENTE = lblNombresPn.Text + " " + lblApellidosPn.Text;
                    frmAdjuntos.TipoPrsTransaccion = (int)TypePerson.PRSNATURAL;
                    frmAdjuntos.Formulario = "REVISION";
                    frmAdjuntos frm = new frmAdjuntos();
                    frm.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Hubo un error al tratar de adjuntar el documento.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmDetalleSolicitudRevision::btnAdjuntarPn_Click", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }

        }

        private void btnAdjuntarPj_Click(object sender, EventArgs e)
        {
            try
            {
                if(!ValidarRepresentantes())
                {
                    XtraMessageBox.Show("Debe tener como mínino un Representant Legal.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    btnAsociarRepresentante.Focus();
                    return;
                }

                if (ValidarDocumentos((int)TypePerson.PRSJURIDICA))
                {
                    string NombreRepresentante = string.Empty;
                    string IdentidadRepresentante = string.Empty;
                    int Contador = 0;
                    foreach (var rpt in lstRepresentantes)
                    {
                        Contador++;
                        EN.RepresentativeRequest rpRq = new EN.RepresentativeRequest();
                        rpRq.Names = rpt.Names;
                        rpRq.LastNames = rpt.LastNames;
                        if (Contador == 1)
                        {
                            NombreRepresentante = rpt.Names + " " + rpt.LastNames;
                            IdentidadRepresentante = rpt.DocumentNumber;
                        }
                    }

                    rptPersonaJuridicaPublica.PERSONAID = PERSONAID;
                    rptPersonaJuridicaPublica.NOMBREREPRESENTANTE = NombreRepresentante;
                    rptPersonaJuridicaPublica.NOMBRESOCIEDAD = txtNombresPj.Text.Trim();
                    rptPersonaJuridicaPublica.DIRECCIONOFICINA = meDireccionPj.Text.Trim();
                    rptPersonaJuridicaPublica.TELEFONOOFICINA = txtTelefonoPj.Text.Trim();
                    rptPersonaJuridicaPublica.CORREOOFICINA = txtCorreoPj.Text.Trim();
                    rptPersonaJuridicaPublica.RTN = lblNumeroDocumentoPj.Text;
                    rptPersonaJuridicaPublica.IDENTIDADREPRESENTANTE = IdentidadRepresentante;

                    string producto = PRODUCTSLN.GetProductByDescription((int)TypePerson.PRSJURIDICA, Convert.ToInt32(lueProductosPj.EditValue));
                    string listVigencia = VariablesGlobales.VIGENCIACERTIFICADOS;
                    if (producto != null)
                    {
                        if (producto.Contains("1 Año") && listVigencia.Contains("1")) rptPersonaJuridicaPublica.VIGENCIA = (int)PRODUCTSLN.VigenciaCertificados.UNO;
                        else if (producto.Contains("2 Años") && listVigencia.Contains("2")) rptPersonaJuridicaPublica.VIGENCIA = (int)PRODUCTSLN.VigenciaCertificados.DOS;
                        else if (producto.Contains("3 Años") && listVigencia.Contains("3")) rptPersonaJuridicaPublica.VIGENCIA = (int)PRODUCTSLN.VigenciaCertificados.TRES;
                        else if (producto.Contains("4 Años") && listVigencia.Contains("4")) rptPersonaJuridicaPublica.VIGENCIA = (int)PRODUCTSLN.VigenciaCertificados.CUATRO;
                        else rptPersonaJuridicaPublica.VIGENCIA = (int)PRODUCTSLN.VigenciaCertificados.CINCO;
                    }

                    frmAdjuntos.TipoPrsTransaccion = (int)TypePerson.PRSJURIDICA;
                    frmAdjuntos.Formulario = "REVISION";
                    frmAdjuntos frm = new frmAdjuntos();
                    frm.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Hubo un error al tratar de adjuntar el documento.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmDetalleSolicitudRevision::btnAdjuntarPj_Click", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        private void btnAsociarRepresentante_Click(object sender, EventArgs e)
        {
            frmAsociarRepresentantes.NUMERODOCUMENTOPJ = lblNumeroDocumentoPj.Text.Trim().Replace("-", "");
            frmAsociarRepresentantes.NOMBREPJ = txtNombresPj.Text.Trim();
            frmAsociarRepresentantes.FORMULARIOREVISION = "REVISION";
            frmAsociarRepresentantes frm = new frmAsociarRepresentantes();
            frm.ShowDialog();
        }

        private void btnEditarSolicitud_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidarDocumentos(TIPOPERSONA)) return;

                if (ValidarAdjuntos(TIPOPERSONA))
                {
                    if(TIPOPERSONA == (int)TypePerson.PRSJURIDICA)
                    if (!ValidarRepresentantes())
                    {
                        XtraMessageBox.Show("Debe tener asociado como mínino un Representant Legal.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        btnAsociarRepresentante.Focus();
                        return;
                    }
                    if (requestById != null)
                    {
                        if (requestById.Count > 0)
                        {
                            //**GUARDAR ADJUNTOS**
                            ssForm.ShowWaitForm();
                            EN.Request rq = new EN.Request();
                            var updateRequest = (dynamic)null;
                            foreach (JObject element in requestById)
                            {
                                if (TIPOPERSONA == (int)TypePerson.PRSJURIDICA)
                                {
                                    rq.DocumentNumberNaturalPerson =  element["IDENTIFICACIONPN"].ToString();
                                    rq.Names = RequestCertificate.VerificarCaracteresEspeciales(element["NOMBRES"].ToString());
                                    rq.LastName = RequestCertificate.VerificarCaracteresEspeciales(element["APELLIDOS"].ToString());
                                    rq.Email = element["PersonalEmail"].ToString();
                                    if (element["TelefonoOficina"].ToString() != "") rq.CellPhonePersonal = element["TelefonoOficina"].ToString();
                                    else rq.CellPhonePersonal = element["HomePhone"].ToString();
                                    rq.PhoneWork = element["TelefonoOficina"].ToString();
                                    rq.FKStateCountryId = Convert.ToInt32(element["StateCountryId"].ToString());
                                    rq.FKStateCityId = Convert.ToInt32(element["StateCityId"].ToString());
                                    rq.FKNeighborhoodId = Convert.ToInt32(element["NeighborhoodId"].ToString());
                                    rq.FKProductId = Convert.ToInt32(lueProductosPj.EditValue);
                                    rq.NameLegalPerson = RequestCertificate.VerificarCaracteresEspeciales(txtNombresPj.Text.Trim());
                                    rq.ModifyDate = DateTime.Now;
                                    if (lblNumeroDocumentoPj.Text == string.Empty) rq.DocumentNumberLegalPerson  = IDENTIDAD_REPRESENTANTE;
                                    else rq.DocumentNumberLegalPerson = lblNumeroDocumentoPj.Text;
                                    rq.LegalPersonEmail = txtCorreoPj.Text.Trim();
                                    rq.LegalPersonPhone = txtTelefonoPj.Text.Trim();
                                    updateRequest = RequestCertificate.UpdateRequestReview((int)TransactionStatus.SOLICITUDREENVIADA, REQUESTID, rq);
                                }
                                else
                                {
                                    rq.DocumentNumberNaturalPerson = element["IDENTIFICACIONPN"].ToString();
                                    rq.Names = RequestCertificate.VerificarCaracteresEspeciales(element["NOMBRES"].ToString());
                                    rq.LastName = RequestCertificate.VerificarCaracteresEspeciales(element["APELLIDOS"].ToString());
                                    rq.Email = element["PersonalEmail"].ToString();
                                    rq.CellPhonePersonal = element["PersonalMovilPhone"].ToString();
                                    rq.PhoneWork = element["TelefonoOficina"].ToString();
                                    rq.FKStateCountryId = Convert.ToInt32(element["StateCountryId"].ToString());
                                    rq.FKStateCityId = Convert.ToInt32(element["StateCityId"].ToString());
                                    rq.FKNeighborhoodId = Convert.ToInt32(element["NeighborhoodId"].ToString());
                                    rq.FKProductId = Convert.ToInt32(lueProductosPn.EditValue);
                                    rq.RegistrationNumber = txtNumeroColegio.Text.Trim();
                                    rq.ModifyDate = DateTime.Now;
                                    updateRequest = RequestCertificate.UpdateRequestReview((int)TransactionStatus.SOLICITUDREENVIADA, REQUESTID, rq);
                                }
                            }

                            if (!updateRequest)
                            {
                                if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                                XtraMessageBox.Show("Hubo un error al tratar de actualizar la solicitud.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                Log.InsertarLog(Log.ErrorType.Error, "frmDetalleSolicitudRevision::btnEditarSolicitud_Click", "Error al guardar el registro en la tabla.", VariablesGlobales.PathDataLog);
                                return;
                            }
                            else
                            {
                                pathRequest = Path.Combine(VariablesGlobales.PathDataBackup, "RQS", DateTime.Now.ToString("yyyy"), DateTime.Now.ToString("MM"), DateTime.Now.ToString("dd"), REQUESTID.ToString());
                                pathAttached = Path.Combine(pathRequest, "ATT");

                                int contador = 0;
                                foreach (var doc in lstAdjuntos)
                                {
                                    contador++;
                                    if (doc.FKRequestId == 0)
                                    {
                                        EN.RequestAttach rqa = new EN.RequestAttach();
                                        string nameFile = string.Format("{0}_{1}_{2}.{3}", REQUESTID, DateTime.Now.ToString("ddhhmmss"), contador, doc.Type);
                                        string unidad = pathAttached.Substring(0, 1) + @":\";

                                        rqa.NameDocument = doc.NameDocument;
                                        rqa.Folder = string.Format(@"{0}\{1}", pathAttached, nameFile).Replace(unidad, "");
                                        rqa.Type = doc.Type;
                                        rqa.Size = doc.Size;
                                        rqa.CreationDate = doc.CreationDate;
                                        rqa.TipoPersona = doc.TipoPersona;
                                        rqa.FKRequestId = REQUESTID;
                                        rqa.FKDocumentType = doc.FKDocumentType;
                                        rqa.FKCreateUserId = Convert.ToInt32(VariablesGlobales.USER_ID);

                                        string fileStr64 = Convert.ToBase64String(doc.Imagen);
                                        if (!RequestCertificate.InsertNewRequestAttachImg(fileStr64, pathAttached, nameFile))
                                        {
                                            if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                                            XtraMessageBox.Show("Hubo un error al tratar de actualizar la solicitud.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                            Log.InsertarLog(Log.ErrorType.Error, "frmDetalleSolicitudRevision::btnEditarSolicitud_Click", "Error al guardar documentos adjuntos en los directorios.", VariablesGlobales.PathDataLog);
                                            return;
                                        }
                                        else
                                        {
                                            var insertRequestAttach = RequestCertificate.InsertNewRequestAttach(rqa);
                                            if (!insertRequestAttach)
                                            {
                                                if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                                                XtraMessageBox.Show("Hubo un error al tratar de actualizar la solicitud.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                                Log.InsertarLog(Log.ErrorType.Error, "frmDetalleSolicitudRevision::btnEditarSolicitud_Click", "Error al guardar documentos adjuntos en la tabla.", VariablesGlobales.PathDataLog);
                                                return;
                                            }
                                        }
                                    }
                                }

                                //**GUARDAR REPRESENTANTES**
                                if (TIPOPERSONA == (int)TypePerson.PRSJURIDICA)
                                {
                                    if (lstRepresentantes.Count == 0 | lstRepresentantes == null) frmRpst.GetRepresentantes();
                                    else
                                    {
                                        if (lstRepresentantes.Count > 0)
                                        {
                                            foreach (var rpt in lstRepresentantes)
                                            {
                                                if (rpt.FKRequestId == 0)
                                                {
                                                    EN.RepresentativeRequest rpRq = new EN.RepresentativeRequest();
                                                    rpRq.DocumentNumber = rpt.DocumentNumber;
                                                    rpRq.Names = rpt.Names;
                                                    rpRq.LastNames = rpt.LastNames;
                                                    rpRq.Nationality = rpt.Nationality;
                                                    rpRq.CreationDate = DateTime.Now;
                                                    rpRq.FKRequestId = REQUESTID;
                                                    rpRq.FKCreateUserId = Convert.ToInt32(VariablesGlobales.USER_ID);

                                                    var insertRepresentative = RequestCertificate.InsertNewRepresentativeRequest(rpRq);
                                                    if (!insertRepresentative)
                                                    {
                                                        if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                                                        Log.InsertarLog(Log.ErrorType.Error, "frmDetalleSolicitudRevision::btnEditarSolicitud_Click", "Error al guardar el(los) representante(s).", VariablesGlobales.PathDataLog);
                                                        return;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                //**GUARDAR SEGUIMIENTO**
                                EN.RequestFollow rqf = new EN.RequestFollow();
                                rqf.Observation = "SOLICITUDREENVIADA";
                                rqf.FKRequestId = REQUESTID;
                                rqf.FKTransactionEstatusId = (int)TransactionStatus.SOLICITUDREENVIADA;
                                rqf.FKCreateUserId = Convert.ToInt32(VariablesGlobales.USER_ID);
                                rqf.FKOfficeId = Convert.ToInt32(VariablesGlobales.OFFICE_ID);
                                rqf.FKTransactionStatusPrevious = (int)TransactionStatus.SOLICITUDENREVISION;
                                rqf.FKWorkstationId = Convert.ToInt32(VariablesGlobales.WS_ID);
                                var insertRqFollow = RequestCertificate.InsertNewRequestFollow(rqf);
                                if (!insertRqFollow)
                                {
                                    if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                                    XtraMessageBox.Show("Hubo un error al tratar de actualizar la solicitud.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    Log.InsertarLog(Log.ErrorType.Error, "frmDetalleSolicitudRevision::btnEditarSolicitud_Click", "Error al guardar el seguimiento.", VariablesGlobales.PathDataLog);
                                    return;
                                }

                                //**INSERTAR BITÁCORA**
                                var InsertarBitacora = RequestCertificate.InsertarRegistro((int)RequestCertificate.TipoEvento.Insertar,
                                        VariablesGlobales.USER_ID, (int)RequestCertificate.Modulos.Certificados, "Se registro nueva Solicitud con el ID: " + REQUESTID);

                                if (!InsertarBitacora)
                                {
                                    if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                                    XtraMessageBox.Show("Hubo un error al tratar de actualizar la solicitud.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    Log.InsertarLog(Log.ErrorType.Error, "frmDetalleSolicitudRevision::btnEditarSolicitud_Click", "Error al guardar la bitácora.", VariablesGlobales.PathDataLog);
                                    return;
                                }

                                if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();

                                frmAsociarRepresentantes.Representantes.Clear();
                                frmAsociarRepresentantes.FORMULARIOREVISION = string.Empty;
                                frmAdjuntos.Formulario = string.Empty;

                                XtraMessageBox.Show("Solicitud actualizada exitosamente y enviada de nuevo para aprobación.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information);

                                frmBandeja ActualizarSolicitudes = (frmBandeja)Application.OpenForms["frmBandeja"];
                                ActualizarSolicitudes.CargarSolicitudesAprobadas();
                                Close();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                XtraMessageBox.Show("Hubo un error al tratar de adjuntar el documento.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmDetalleSolicitudRevision::btnEditarSolicitud_Click", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        //void LimpiarVariables()
        //{
        //    personDataRepresentantes = null;
        //    pathAttached = string.Empty;
        //    pathRequest = string.Empty;

        //    if (ListaDocumentosAdjuntos != null) if (ListaDocumentosAdjuntos.Count() != 0) ListaDocumentosAdjuntos.Clear();
        //    if (ListaRepresentantes != null) if (ListaRepresentantes.Count() != 0) ListaRepresentantes.Clear();

        //    TotalDocPersonaNatural = 0;
        //    TotalDocPersonaJuridica = 0;
        //    PASSWORDCLIENT = string.Empty;
        //}


        private void btnVisualizar1_Click_1(object sender, EventArgs e)
        {
            try
            {
                if (gvAdjuntosPn.RowCount != -1)
                {
                    if(gvAdjuntosPn.GetFocusedRowCellValue("Numero") != null)
                    {
                        byte[] Archivo = new byte[] { };
                        Archivo = lstAdjuntos.Find(x => x.Numero == Convert.ToInt32(gvAdjuntosPn.GetFocusedRowCellValue("Numero"))).Imagen;

                        frmVisorDocumentosAdjuntos.Archivo = Archivo;
                        frmVisorDocumentosAdjuntos frm = new frmVisorDocumentosAdjuntos();
                        frm.ShowDialog();
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Hubo un error al tratar de visualizar el documento.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmDetalleSolicitudRevision::btnVisualizar1_Click", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        private void btnEliminarAdjuntoPN_Click(object sender, EventArgs e)
        {
            try
            {
                string path = string.Empty;
                var numero = gvAdjuntosPn.GetFocusedRowCellValue("Numero");
                path = gvAdjuntosPn.GetFocusedRowCellValue("Folder").ToString();

                if (numero != null)
                {
                    if (XtraMessageBox.Show("¿Está seguro de eliminar el documento?", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        lstAdjuntos.Remove(lstAdjuntos.Find(x => x.Numero == Convert.ToInt32(numero)));
                        gcAdjuntosPn.RefreshDataSource();

                        int totDocumentos = Convert.ToInt32(lstAdjuntos.Count());
                        int totprsNatural = 0;
                        int totprsJuridica = 0;
                        if (totDocumentos == 0)
                        {
                            TotalDocPersonaNatural = 0;
                            TotalDocPersonaJuridica = 0;
                        }
                        else
                        {
                            int tpersona = Convert.ToInt32(gvAdjuntosPn.GetFocusedRowCellValue("TipoPersona"));
                            if (tpersona != 0)
                            {
                                if (tpersona == (int)TypePerson.PRSNATURAL) totprsNatural = totDocumentos;
                                else totprsJuridica = totDocumentos;
                            }
                        }

                        //***Eliminar archivo de la BBDD***
                        if (path != string.Empty)
                        {
                            if (RequestCertificate.DeleteAttachRequestFolder(REQUESTID, path))
                            {
                                string file = @"C:\" + path;
                                RequestCertificate.DeleteAttachRequestDirectory(file);
                            }
                            else
                            {
                                Log.InsertarLog(Log.ErrorType.Error, "frmDetalleSolicitudRevision::btnEliminarAdjuntoPN_Click", "Error al eliminar el registro.", VariablesGlobales.PathDataLog);
                                return;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Hubo un error al eliminar el documento.", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                Log.InsertarLog(Log.ErrorType.Error, "frmDetalleSolicitudRevision:btnEliminarAdjuntoPN_Click", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        void GetRepresentative(int prsId)
        {
            string documento = string.Empty;
            JArray personRepresentante = new JArray();
            personRepresentante = RequestCertificate.GetRepresentativeRequestById(REQUESTID);
            if (personRepresentante.Count > 0)
            {
                var list = new List<EN.RepresentativeRequest>();
                foreach (JObject element in personRepresentante)
                {
                    EN.RepresentativeRequest rps = new EN.RepresentativeRequest
                    {
                        DocumentNumber = element["IDENTIFICACION"].ToString(),
                        Names = element["NOMBRES"].ToString(),
                        LastNames = element["APELLIDOS"].ToString(),
                        Nationality = element["NACIONALIDAD"].ToString(),
                        CreationDate = Convert.ToDateTime(element["CreationDate"].ToString()),
                        FKRequestId = Convert.ToInt32(element["FKRequestId"].ToString()),
                        FKCreateUserId = Convert.ToInt32(element["FKCreateUserId"].ToString()),
                    };

                    documento = element["IDENTIFICACION"].ToString();
                    //Foto
                    JArray arrayFoto = new JArray();
                    arrayFoto = RequestCertificate.GetLastPhotoRepresentative(documento);
                    foreach (JObject elementPhoto in arrayFoto)
                        if (elementPhoto["Image"].ToString() != null) rps.Foto = (byte[])elementPhoto["Image"];

                    lstRepresentantes.Add(rps); 
                }
                UpdateGridRepresentative();
            }
        }

        public void UpdateGridRepresentative()
        {
            gcRepresentantes.DataSource = lstRepresentantes;
            gcRepresentantes.RefreshDataSource();
        }

        void GetAttached(int tPerson)
        {
            int contador = 0;
            JArray arrayImgAdjunto = new JArray();
            arrayImgAdjunto = RequestCertificate.GetAdjuntoByRequestId(REQUESTID);
            if (arrayImgAdjunto.Count > 0)
            {
                foreach (JObject elementImg in arrayImgAdjunto)
                {
                    imgPn = null;
                    string result = null;
                    string strRelativePath = null;
                    string file = null;
                    if (elementImg["Folder"].ToString() != null)
                    {
                        string path = @"C:\";
                        strRelativePath = path + elementImg["Folder"].ToString();
                        file = Path.GetDirectoryName(strRelativePath);
                        result = RequestCertificate.GetRequestAttachImg(strRelativePath, file);
                        if (result != null) imgPn = Convert.FromBase64String(result);
                    }

                    EN.RequestAttach rq = new EN.RequestAttach
                    {
                        Numero = contador += 1,
                        NameDocument = elementImg["NombreDocumento"].ToString(),
                        Folder = elementImg["Folder"].ToString(),
                        Type = elementImg["Type"].ToString(),
                        Size = elementImg["Size"].ToString(),
                        TipoPersona = Convert.ToInt32(elementImg["TipoPersona"].ToString()),
                        FKDocumentType = Convert.ToInt32(elementImg["FKDocumentType"].ToString()),
                        FKRequestId = Convert.ToInt32(elementImg["FKRequestId"].ToString()),
                        Imagen = imgPn
                    };
                    lstAdjuntos.Add(rq);

                    if (tPerson == (int)TypePerson.PRSNATURAL) gcAdjuntosPn.DataSource = lstAdjuntos;
                    else gcAdjuntosPj.DataSource = lstAdjuntos;
                }
            }
        }

        public void UpdateDocumentosGrid()
        {
            if(TIPOPERSONA == (int)TypePerson.PRSJURIDICA)
            {
                gcAdjuntosPj.DataSource = lstAdjuntos;
                gcAdjuntosPj.RefreshDataSource();
            }
            else
            {
                gcAdjuntosPn.DataSource = lstAdjuntos;
                gcAdjuntosPn.RefreshDataSource();
            }
        }

        public List<EN.RepresentativeRequest> GetRepresentantes()
        {
            if (lstRepresentantes.Count > 0) 
            {
                foreach (var rp in lstRepresentantes)
                {
                    EN.RepresentativeRequest rpRq = new EN.RepresentativeRequest();
                    rpRq.DocumentNumber = rp.DocumentNumber;
                    rpRq.Names = rp.Names;
                    rpRq.LastNames = rp.LastNames;
                    rpRq.Nationality = rp.Nationality;
                    rpRq.CreationDate = rp.CreationDate;
                    rpRq.FKCreateUserId = rp.FKCreateUserId;
                    rpRq.FKRequestId = rp.FKRequestId;
                    frmAsociarRepresentantes.ListaRepresentantesRevision.Add(rpRq);
                }
            }

            return frmAsociarRepresentantes.ListaRepresentantesRevision;
        }

        bool ValidarDocumentos(int tPersona)
        {
            if(tPersona == (int)TypePerson.PRSNATURAL)
            {
                if (lueProductosPn.EditValue == null && tPersona == (int)TypePerson.PRSNATURAL)
                {
                    XtraMessageBox.Show("Tipo de producto esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    lueProductosPn.Focus();
                    return false;
                }
            }
            if (tPersona == (int)TypePerson.PRSJURIDICA)
            {
                if (lueProductosPj.EditValue == null && tPersona == (int)TypePerson.PRSJURIDICA)
                {
                    XtraMessageBox.Show("Tipo de producto esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    lueProductosPj.Focus();
                    return false;
                }
                if (txtNombresPj.Text == string.Empty)
                {
                    XtraMessageBox.Show("El nombre o la razon social esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtNombresPj.Focus();
                    return false;
                }
                if (txtTelefonoPj.Text == string.Empty)
                {
                    XtraMessageBox.Show("El número de teléfono esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtTelefonoPj.Focus();
                    return false;
                }
                if (txtCorreoPj.Text == string.Empty)
                {
                    XtraMessageBox.Show("El correo esta vacío, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtCorreoPj.Focus();
                    return false;
                }
                if (meDireccionPj.Text == string.Empty)
                {
                    XtraMessageBox.Show("La dirección de la empresa esta vacía, este campo es requerido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    meDireccionPj.Focus();
                    return false;
                }
            }

            return true;
        }

        bool ValidarAdjuntos(int tPersona)
        {
            int documentosAdjuntados = 0;
            string nombreDocumento = string.Empty;
            var lstAdjuntosPersona = new List<EN.DocumentTypePerson>();
            var listaAuxiliar = new List<EN.DocumentTypePerson>();
            var listadoAdjuntos = (dynamic)null;
            int contador = 0;

            //**VALIDAR DOCUMENTOS ADJUNTOS PERSONA JURIDICA**
            if (tPersona == 2)
            {
                int cantidadAdjuntosPJ = 0;
                lstAdjuntosPersona = DocumentTypePersonLN.GetDocumentByPersonType((int)TypePerson.PRSJURIDICA);
                cantidadAdjuntosPJ = DocumentTypePersonLN.GetDocumentByTypePerson((int)TypePerson.PRSJURIDICA);
                listadoAdjuntos = lstAdjuntosPersona;
                
                if (lstAdjuntos.Count != 0 | lstAdjuntos.Count == 0)
                {
                    foreach (var item in lstAdjuntosPersona)
                    {
                        foreach (var nombre in lstAdjuntos)
                        {
                            if (item.Name == nombre.NameDocument)
                            {
                                listaAuxiliar.Add(item);
                                documentosAdjuntados = documentosAdjuntados + 1;
                            }
                        }
                    }
                    foreach (var item in listaAuxiliar) listadoAdjuntos.Remove(item);

                    foreach (var item in listadoAdjuntos)
                    {
                        contador++;
                        if (nombreDocumento != string.Empty) nombreDocumento = string.Format(nombreDocumento + Environment.NewLine + contador + ": " + item.Name);
                        else nombreDocumento = string.Format(Environment.NewLine + contador + ": " + item.Name);
                    }

                    if (cantidadAdjuntosPJ != documentosAdjuntados)
                    {
                        XtraMessageBox.Show("Los siguientes documentos no han sido adjuntados: " + nombreDocumento , "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        lueProductosPn.Focus();
                        return false;
                    }
                }
                else
                {
                    foreach (var item in lstAdjuntosPersona)
                    {
                        foreach (var nombre in lstAdjuntos)
                        {
                            if (item.Name == nombre.NameDocument)
                            {
                                listaAuxiliar.Add(item);
                                documentosAdjuntados = documentosAdjuntados + 1;
                            }
                        }
                    }
                    foreach (var item in listaAuxiliar) listadoAdjuntos.Remove(item);

                    foreach (var item in listadoAdjuntos)
                    {
                        contador++;
                        if (nombreDocumento != string.Empty) nombreDocumento = string.Format(nombreDocumento + Environment.NewLine + contador + ": " + item.Name);
                        else nombreDocumento = string.Format(Environment.NewLine + contador + ": " + item.Name);
                    }

                    if (cantidadAdjuntosPJ != documentosAdjuntados)
                    {
                        XtraMessageBox.Show("Los siguientes documentos no han sido adjuntados: " + nombreDocumento, "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        lueProductosPn.Focus();
                        return false;
                    }
                }
            }
            //**VALIDAR DOCUMENTOS ADJUNTOS PERSONA NATURAL**
            else
            {
                int cantidadAdjuntosPN = 0;
                lstAdjuntosPersona = DocumentTypePersonLN.GetDocumentByPersonType((int)TypePerson.PRSNATURAL);
                cantidadAdjuntosPN = DocumentTypePersonLN.GetDocumentByTypePerson((int)TypePerson.PRSNATURAL);
                listadoAdjuntos = lstAdjuntosPersona;
                if (lstAdjuntos.Count != 0 | lstAdjuntos.Count == 0)
                {
                    foreach (var item in lstAdjuntosPersona)
                    {
                        foreach (var nombre in lstAdjuntos)
                        {
                            if (item.Name == nombre.NameDocument)
                            {
                                listaAuxiliar.Add(item);
                                documentosAdjuntados = documentosAdjuntados + 1;
                            }
                        }
                    }
                    foreach (var item in listaAuxiliar) listadoAdjuntos.Remove(item);

                    foreach (var item in listadoAdjuntos)
                    {
                        contador++;
                        if (nombreDocumento != string.Empty) nombreDocumento = string.Format(nombreDocumento + Environment.NewLine + contador + ": " + item.Name);
                        else nombreDocumento = string.Format(Environment.NewLine + contador + ": " + item.Name);
                    }

                    if (cantidadAdjuntosPN != documentosAdjuntados)
                    {
                        XtraMessageBox.Show("Los siguientes documentos no han sido adjuntados: " + nombreDocumento, "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        btnAdjuntarPn.Focus();
                        return false;
                    }
                }
            }
            return true;
        }

        bool ValidarRepresentantes()
        {
            if(gvRepresentantes.RowCount == 0) return false;
            return true;
        }

        void LimpiarControles()
        {
            
            lueProductosPn.EditValue = null;
            txtCorreoPn.Text = string.Empty;
            txtTelefonoPn.Text = string.Empty;
            TotalDocPersonaNatural = 0;

            //Persona juridica
            txtNombresPj.Text = string.Empty;
            lueProductosPj.EditValue = null;
            txtCorreoPj.Text = string.Empty;
            txtTelefonoPj.Text = string.Empty;
            pnlPersonaJuridica.Visible = false;
            TotalDocPersonaJuridica = 0;
        }

        private void btnVisualizarDocsPj_Click(object sender, EventArgs e)
        {
            try
            {
                if (gvAdjuntosPj.RowCount != -1) 
                {
                    byte[] Archivo = new byte[] { };
                    Archivo = lstAdjuntos.Find(x => x.Folder == Convert.ToString(gvAdjuntosPj.GetFocusedRowCellValue("Folder"))).Imagen;
                    frmVisorDocumentosAdjuntos.Archivo = Archivo;
                    frmVisorDocumentosAdjuntos frm = new frmVisorDocumentosAdjuntos();
                    frm.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Hubo un error al tratar de visualizar el documento.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmDetalleSolicitudRevision::btnVisualizarDocsPj_Click", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        private void btnEliminarDocsPj_Click(object sender, EventArgs e)
        {
            try
            {
                string path = string.Empty;
                var numero = gvAdjuntosPj.GetFocusedRowCellValue("Numero");
                path = gvAdjuntosPj.GetFocusedRowCellValue("Folder").ToString();

                if (numero != null)
                {
                    if (XtraMessageBox.Show("¿Está seguro de eliminar el documento?", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        lstAdjuntos.Remove(lstAdjuntos.Find(x => x.Numero == Convert.ToInt32(numero))); 
                        gcAdjuntosPj.RefreshDataSource(); 

                        int totDocumentos = Convert.ToInt32(lstRepresentantes.Count());
                        int totprsNatural = 0;
                        int totprsJuridica = 0;
                        if (totDocumentos == 0)
                        {
                            TotalDocPersonaNatural = 0;
                            TotalDocPersonaJuridica = 0;
                        }
                        else
                        {
                            int tpersona = Convert.ToInt32(gvAdjuntosPj.GetFocusedRowCellValue("TipoPersona"));
                            if (tpersona != 0)
                            {
                                if (tpersona == (int)TypePerson.PRSJURIDICA) totprsJuridica  = totDocumentos;
                                else totprsNatural = totDocumentos;
                            }
                        }

                        //***Eliminar archivo de la BBDD***
                        if (path != string.Empty)
                        {
                            if (RequestCertificate.DeleteAttachRequestFolder(REQUESTID, path))
                            {
                                string file = @"C:\" + path;
                                RequestCertificate.DeleteAttachRequestDirectory(file);
                            }
                            else
                            {
                                Log.InsertarLog(Log.ErrorType.Error, "frmCertificados::btnEliminarAdjuntoPN_Click", "Error al guardar el registro.", VariablesGlobales.PathDataLog);
                                return;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Hubo un error al guardar el registro.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmDetalleSolicitudRevision:btnEliminar_Click", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        private void btnVisualizarRepresentante_Click(object sender, EventArgs e)
        {
            try
            {
                frmInformacionRepresentantes.FOTO = (byte[])gvRepresentantes.GetRowCellValue(gvRepresentantes.FocusedRowHandle, "Foto");
                frmInformacionRepresentantes.IDENTIFICACION = gvRepresentantes.GetRowCellValue(gvRepresentantes.FocusedRowHandle, "DocumentNumber").ToString();
                frmInformacionRepresentantes.NOMBRES = gvRepresentantes.GetRowCellValue(gvRepresentantes.FocusedRowHandle, "Names").ToString();
                frmInformacionRepresentantes.NACIONALIDAD = gvRepresentantes.GetRowCellValue(gvRepresentantes.FocusedRowHandle, "Nationality").ToString();
                frmInformacionRepresentantes frm = new frmInformacionRepresentantes();
                frm.ShowDialog();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Hubo un error al tratar de visualizar el representante.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmDetalleSolicitudRevision::btnVisualizarRepresentante_Click", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        private void btnEliminarRepresentante_Click(object sender, EventArgs e)
        {
            try
            {
                int rqId = 0;
                string identificacion = string.Empty;
                identificacion = gvRepresentantes.GetFocusedRowCellValue("DocumentNumber").ToString(); 
                rqId = Convert.ToInt32(gvRepresentantes.GetFocusedRowCellValue("FKRequestId"));

                if (identificacion != string.Empty)
                {
                    if (XtraMessageBox.Show("¿Está seguro de eliminar el representante?", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        lstRepresentantes.Remove(lstRepresentantes.Find(x => x.DocumentNumber == identificacion));
                        gcRepresentantes.RefreshDataSource();

                        //***Eliminar archivo de la BBDD***
                        if (identificacion != string.Empty & rqId != 0)
                        {
                            if (RequestCertificate.DeleteRepresentativeRequestById(REQUESTID, identificacion))
                            {
                                XtraMessageBox.Show("Registro eliminado stisfactoriamente.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            }
                            else
                            {
                                XtraMessageBox.Show("Hubo un error al eliminar el documento.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                Log.InsertarLog(Log.ErrorType.Error, "frmDetalleSolicitudRevision::btnEliminarDocsPj_Click", "Error al eliminar el registro", VariablesGlobales.PathDataLog);
                                return;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Hubo un error al eliminar el documento.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmDetalleSolicitudRevision:btnEliminarDocsPj_Click", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        private void frmDetalleSolicitudRevision_FormClosing(object sender, FormClosingEventArgs e)
        {
            //frmAsociarRepresentantes.Representantes.Clear();
            frmAsociarRepresentantes.FORMULARIOREVISION = string.Empty;
            //frmAdjuntos.Formulario = string.Empty;
        }
    }
}