﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Newtonsoft.Json.Linq;
using static Gv.ExodusBc.UI.ExodusBcBase;
using Gv.Utilidades;
using static Gv.ExodusBc.EN.Request;
using DevExpress.XtraGrid.Views.Grid;

namespace Gv.ExodusBc.UI.Modulos.Certificados
{
    public partial class frmBandejaSeguimientoCertificados : DevExpress.XtraEditors.XtraForm
    {
        public frmBandejaSeguimientoCertificados()
        {
            InitializeComponent();
        }

        private void frmBandejaSeguimientoCertificados_Load(object sender, EventArgs e)
        {
            try
            {
                ssForm.ShowWaitForm();
                if (VariablesGlobales.PERMISO_VerOficinasSeguimiento)
                {
                    lueOficinas.Enabled = true;
                    ExodusBcBase.Helper.Combobox.setComboOffice(lueOficinas);
                    lueOficinas.EditValue = Convert.ToInt64(VariablesGlobales.OFFICE_ID);
                }
                else
                {
                    ExodusBcBase.Helper.Combobox.setComboOffice(lueOficinas);
                    lueOficinas.EditValue = Convert.ToInt64(VariablesGlobales.OFFICE_ID);
                    lueOficinas.Enabled = false;
                }
              
                dteFechaInicial.EditValue = DateTime.Now;
                dteFechaFinal.EditValue = DateTime.Now;
                dteFechaInicial.Properties.MaxValue = DateTime.Now;
                dteFechaFinal.Properties.MaxValue = DateTime.Now;
                CargarSeguimientoCertificados(Convert.ToInt32(lueOficinas.EditValue));
                ssForm.CloseWaitForm();
            }
            catch (Exception ex)
            {
                if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                XtraMessageBox.Show("Hubo un error al tratar de visualizar los certificados.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmBandejaSeguimientoCertificados::frmBandejaSeguimientoCertificados_Load", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        public void CargarSeguimientoCertificados(int OficinaId)
        {
            gcRequestCertificados.DataSource = null;
            JArray requestStatusTray = new JArray();
            requestStatusTray = RequestCertificate.GetRequestBandejaStatusCertificados(OficinaId, Convert.ToDateTime(dteFechaInicial.EditValue), Convert.ToDateTime(dteFechaFinal.EditValue));
            if (requestStatusTray.Count > 0)
            {
                var list = new List<EN.GridRequestStatusCertificados>();
                foreach (JObject element in requestStatusTray)
                {
                    EN.GridRequestStatusCertificados request = new EN.GridRequestStatusCertificados
                    {
                        NUMEROTICKET = element["NUMEROTICKET"].ToString(),
                        PRODUCTO = element["PRODUCTO"].ToString(),
                        NOMBRES = element["NOMBRES"].ToString(),
                        APELLIDOS = element["APELLIDOS"].ToString(),
                        SEGUIMIENTO = element["STATUSTRANSACCION"].ToString(),
                        OFICINA = element["OFICINA"].ToString(),
                        FECHACREACION = Convert.ToDateTime(element["FECHACREACION"].ToString()),
                        RequestId = Convert.ToInt32(element["RequestId"].ToString()),
                        TRANSACCIONSTATUSTID = Convert.ToInt32(element["FKTransactionStatusId"].ToString()),
                    };
                    list.Add(request);
                }
                gvRequestCertificados.Columns[7].Visible = false;
                gvRequestCertificados.Columns[8].Visible = false;
                gcRequestCertificados.DataSource = list;
            }
        }

        private void btnRefrescar_Click(object sender, EventArgs e)
        {
            if (dteFechaInicial.EditValue.ToString()==String.Empty)
                XtraMessageBox.Show("La fecha inicial es obligatoria.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            else if (dteFechaFinal.EditValue.ToString() == String.Empty)
                XtraMessageBox.Show("La fecha final es obligatoria.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            else if (Convert.ToDateTime(dteFechaInicial.EditValue).Date > Convert.ToDateTime(dteFechaFinal.EditValue).Date)
                XtraMessageBox.Show("La fecha de inicio no puede ser mayor a la fecha final.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            else
            {
                try
                {
                    ssForm.ShowWaitForm();
                    CargarSeguimientoCertificados(Convert.ToInt32(lueOficinas.EditValue));
                    ssForm.CloseWaitForm();
                }
                catch (Exception ex)
                {
                    if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                    XtraMessageBox.Show("Hubo un error al tratar de visualizar los certificados.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Log.InsertarLog(Log.ErrorType.Error, "frmBandejaSeguimientoCertificados::btnRefrescar_Click", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                }
            }
            
        }

        private void btnVerDetalle_Click(object sender, EventArgs e)
        {
            int rqId = Convert.ToInt32(gvRequestCertificados.GetRowCellValue(gvRequestCertificados.FocusedRowHandle, "RequestId")); 
            if(rqId != 0)
            {
                frmDetalleCertificados.REQUEST_ID = rqId;
                frmDetalleCertificados._OficinaId = Convert.ToInt32(lueOficinas.EditValue);
                frmDetalleCertificados frm = new frmDetalleCertificados();
                frm.ShowDialog();
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            frmMain2 openForm = (frmMain2)Application.OpenForms["frmMain2"];
            openForm.OpenForms("frmBandejaSeguimientoCertificados");
            CerrarBandejaCertificados();
        }

        public void CerrarBandejaCertificados()
        {
            Close();
        }

        private void lueOficinas_EditValueChanged(object sender, EventArgs e)
        {
            if (lueOficinas.EditValue != null) CargarSeguimientoCertificados(Convert.ToInt32(lueOficinas.EditValue));
        }

        private void gvRequestCertificados_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            try
            {
                if (e.RowHandle >= 0)
                {
                    e.Appearance.Font = new Font("Segoe UI Semibold", 8, FontStyle.Bold);
                    e.Appearance.ForeColor = Color.FromArgb(55, 71, 79);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Hubo un error al tratar de visualizar las solicitudes.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmBandeja::gvRequestCertificados_RowCellStyle", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }


    }
}