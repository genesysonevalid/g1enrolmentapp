﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Gv.Utilidades;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraReports.UI;

namespace Gv.ExodusBc.UI.Modulos.Certificados.Reportes
{
    public partial class frmVisorReporte : DevExpress.XtraEditors.XtraForm
    {
        public static int TIPOPERSONARPT = 0;
        public static string NOMBRECLIENTE = string.Empty;
        public static int TIPOCONTRATOJURIDICO = 0;

        public enum TipoPersona
        {
            PersonaNatural = 1,
            PersonanJuridica = 2,
            ProfesionalTitulado = 3
        }

        public enum TipoContrato
        {
            EmpresaPrivada = 1,
            EmpresaPublica = 2
        }

        public frmVisorReporte()
        {
            InitializeComponent();
        }

        private void frmVisorReporte_Load(object sender, EventArgs e)
        {
            try
            {
                if (TIPOPERSONARPT == (int)TipoPersona.PersonaNatural | TIPOPERSONARPT == (int)TipoPersona.ProfesionalTitulado)
                {
                    ssForm.ShowWaitForm();
                    rptPersonaNatural reporte = new rptPersonaNatural();
                    dcvContrato.DocumentSource = reporte;
                    reporte.Name = NOMBRECLIENTE;
                    reporte.CreateDocument();
                    ssForm.CloseWaitForm();
                }
                else
                {
                    ssForm.ShowWaitForm();
                    XtraReport reporte = new XtraReport();
                    if (TIPOCONTRATOJURIDICO == (int)TipoContrato.EmpresaPrivada) reporte = new rptPersonaJuridicaPrivada();
                    else reporte = new rptPersonaJuridicaPublica();
                    dcvContrato.DocumentSource = reporte;
                    reporte.Name = NOMBRECLIENTE;
                    reporte.CreateDocument();
                    ssForm.CloseWaitForm();
                }
            }
            catch (Exception ex)
            {
                if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                XtraMessageBox.Show("Huvo un error al generar el reporte.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmVisorReporte::frmVisorReporte_Load", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
            
        }

    }
}