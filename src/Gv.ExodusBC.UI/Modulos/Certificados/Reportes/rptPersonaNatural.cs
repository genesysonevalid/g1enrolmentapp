﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Newtonsoft.Json.Linq;
using static Gv.ExodusBc.UI.ExodusBcBase;

namespace Gv.ExodusBc.UI.Modulos.Certificados.Reportes
{
    public partial class rptPersonaNatural : DevExpress.XtraReports.UI.XtraReport
    {
        public static int PERSONAID = 0;
        public static string CLIENTE = string.Empty;
        public static string RESIDENCIA = string.Empty;
        public static string TELEFONOCASA = string.Empty;
        public static string TELEFONOOFICINA = string.Empty;
        public static string TELEFONOMOVIL = string.Empty;
        public static string CORREO = string.Empty;
        public static int VIGENCIA = 0;
        public static string IDENTIDAD = string.Empty;

        public rptPersonaNatural()
        {
            InitializeComponent();
        }

        private void rptPersonaNatural_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            picLogo.Image = Properties.Resources.Logo_Tecnisign_RGB;
            prmCliente.Value = CLIENTE;
            prmResidencia.Value = RESIDENCIA;
            if (TELEFONOCASA == string.Empty) prmTelefonoCasa.Value = "        ";
            else prmTelefonoCasa.Value = TELEFONOCASA;
            prmTelefonoOficina.Value = TELEFONOOFICINA;
            prmTelefonoMovil.Value = TELEFONOMOVIL;
            prmCorreo.Value = CORREO;
            prmVigencia.Value = VIGENCIA.ToString();

            lblIdentidad.Text = IDENTIDAD;

            lblLugarFecha.Text = VariablesGlobales.OFFICENAME + " " + ExodusBcBase.Utilities.getFechaActual();

        }

    }
}
