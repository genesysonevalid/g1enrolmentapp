﻿namespace Gv.ExodusBc.UI.Modulos.Certificados.Reportes
{
    partial class rptPersonaNatural
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptPersonaNatural));
			this.Detail = new DevExpress.XtraReports.UI.DetailBand();
			this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
			this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
			this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
			this.picLogo = new DevExpress.XtraReports.UI.XRPictureBox();
			this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
			this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
			this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
			this.lblIdentidad = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrRichText1 = new DevExpress.XtraReports.UI.XRRichText();
			this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
			this.lblLugarFecha = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
			this.prmCliente = new DevExpress.XtraReports.Parameters.Parameter();
			this.prmResidencia = new DevExpress.XtraReports.Parameters.Parameter();
			this.prmTelefonoCasa = new DevExpress.XtraReports.Parameters.Parameter();
			this.prmTelefonoOficina = new DevExpress.XtraReports.Parameters.Parameter();
			this.prmTelefonoMovil = new DevExpress.XtraReports.Parameters.Parameter();
			this.prmCorreo = new DevExpress.XtraReports.Parameters.Parameter();
			this.prmVigencia = new DevExpress.XtraReports.Parameters.Parameter();
			((System.ComponentModel.ISupportInitialize)(this.xrRichText1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Dpi = 254F;
			this.Detail.HeightF = 11.18813F;
			this.Detail.Name = "Detail";
			this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
			this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
			// 
			// TopMargin
			// 
			this.TopMargin.Dpi = 254F;
			this.TopMargin.HeightF = 94.93591F;
			this.TopMargin.Name = "TopMargin";
			this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
			this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
			// 
			// BottomMargin
			// 
			this.BottomMargin.Dpi = 254F;
			this.BottomMargin.HeightF = 144.7162F;
			this.BottomMargin.Name = "BottomMargin";
			this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
			this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
			// 
			// ReportHeader
			// 
			this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.picLogo,
            this.xrLabel1});
			this.ReportHeader.Dpi = 254F;
			this.ReportHeader.HeightF = 268.8166F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// picLogo
			// 
			this.picLogo.Borders = DevExpress.XtraPrinting.BorderSide.None;
			this.picLogo.Dpi = 254F;
			this.picLogo.LocationFloat = new DevExpress.Utils.PointFloat(343.6375F, 25.00001F);
			this.picLogo.Name = "picLogo";
			this.picLogo.SizeF = new System.Drawing.SizeF(1037.848F, 118.7688F);
			this.picLogo.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
			this.picLogo.StylePriority.UseBorders = false;
			// 
			// xrLabel1
			// 
			this.xrLabel1.Dpi = 254F;
			this.xrLabel1.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(99.08327F, 197.5897F);
			this.xrLabel1.Name = "xrLabel1";
			this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
			this.xrLabel1.SizeF = new System.Drawing.SizeF(1607.25F, 58.42F);
			this.xrLabel1.StylePriority.UseFont = false;
			this.xrLabel1.StylePriority.UseTextAlignment = false;
			this.xrLabel1.Text = "CONTRATO DE ADHESION PARA USO DE CERTIFICADO DIGITAL POR PERSONA NATURAL";
			this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel9,
            this.lblIdentidad,
            this.xrLabel5,
            this.xrRichText1,
            this.xrLabel6,
            this.xrLabel3,
            this.lblLugarFecha,
            this.xrLabel2});
			this.GroupHeader1.Dpi = 254F;
			this.GroupHeader1.HeightF = 3409.376F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// xrLabel9
			// 
			this.xrLabel9.Dpi = 254F;
			this.xrLabel9.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Bold);
			this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(641.4821F, 3262.032F);
			this.xrLabel9.Name = "xrLabel9";
			this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
			this.xrLabel9.SizeF = new System.Drawing.SizeF(898.1805F, 44.81177F);
			this.xrLabel9.StylePriority.UseFont = false;
			this.xrLabel9.StylePriority.UseTextAlignment = false;
			this.xrLabel9.Text = "GRUPO VISIÓN, S. DE R.L. DE C.V.";
			this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
			// 
			// lblIdentidad
			// 
			this.lblIdentidad.Borders = DevExpress.XtraPrinting.BorderSide.Top;
			this.lblIdentidad.BorderWidth = 1F;
			this.lblIdentidad.Dpi = 254F;
			this.lblIdentidad.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblIdentidad.LocationFloat = new DevExpress.Utils.PointFloat(386.6376F, 3063.454F);
			this.lblIdentidad.Name = "lblIdentidad";
			this.lblIdentidad.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
			this.lblIdentidad.SizeF = new System.Drawing.SizeF(1163.608F, 52.37231F);
			this.lblIdentidad.StylePriority.UseBorders = false;
			this.lblIdentidad.StylePriority.UseBorderWidth = false;
			this.lblIdentidad.StylePriority.UseFont = false;
			this.lblIdentidad.StylePriority.UseTextAlignment = false;
			this.lblIdentidad.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
			// 
			// xrLabel5
			// 
			this.xrLabel5.Dpi = 254F;
			this.xrLabel5.Font = new System.Drawing.Font("Calibri", 10F);
			this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(99.6965F, 3204.473F);
			this.xrLabel5.Name = "xrLabel5";
			this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
			this.xrLabel5.SizeF = new System.Drawing.SizeF(523.875F, 44.81226F);
			this.xrLabel5.StylePriority.UseFont = false;
			this.xrLabel5.StylePriority.UseTextAlignment = false;
			this.xrLabel5.Text = "Titular Signatario / Jefe de Agencia";
			this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			// 
			// xrRichText1
			// 
			this.xrRichText1.Dpi = 254F;
			this.xrRichText1.Font = new System.Drawing.Font("Times New Roman", 9.75F);
			this.xrRichText1.LocationFloat = new DevExpress.Utils.PointFloat(99.08326F, 51.60822F);
			this.xrRichText1.Name = "xrRichText1";
			this.xrRichText1.SerializableRtfString = resources.GetString("xrRichText1.SerializableRtfString");
			this.xrRichText1.SizeF = new System.Drawing.SizeF(1603.017F, 2703.913F);
			this.xrRichText1.StylePriority.UseFont = false;
			// 
			// xrLabel6
			// 
			this.xrLabel6.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
			this.xrLabel6.Dpi = 254F;
			this.xrLabel6.Font = new System.Drawing.Font("Segoe UI", 9.75F);
			this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(641.4821F, 3204.473F);
			this.xrLabel6.Name = "xrLabel6";
			this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
			this.xrLabel6.SizeF = new System.Drawing.SizeF(898.1805F, 44.81226F);
			this.xrLabel6.StylePriority.UseBorders = false;
			this.xrLabel6.StylePriority.UseFont = false;
			this.xrLabel6.StylePriority.UseTextAlignment = false;
			this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			// 
			// xrLabel3
			// 
			this.xrLabel3.Dpi = 254F;
			this.xrLabel3.Font = new System.Drawing.Font("Calibri", 10F);
			this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(104.294F, 3031.62F);
			this.xrLabel3.Name = "xrLabel3";
			this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
			this.xrLabel3.SizeF = new System.Drawing.SizeF(263.1936F, 46.07275F);
			this.xrLabel3.StylePriority.UseFont = false;
			this.xrLabel3.StylePriority.UseTextAlignment = false;
			this.xrLabel3.Text = "Firma de Titular:";
			this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			// 
			// lblLugarFecha
			// 
			this.lblLugarFecha.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
			this.lblLugarFecha.Dpi = 254F;
			this.lblLugarFecha.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblLugarFecha.LocationFloat = new DevExpress.Utils.PointFloat(331.3116F, 2850.704F);
			this.lblLugarFecha.Name = "lblLugarFecha";
			this.lblLugarFecha.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
			this.lblLugarFecha.SizeF = new System.Drawing.SizeF(1218.934F, 47.83667F);
			this.lblLugarFecha.StylePriority.UseBorders = false;
			this.lblLugarFecha.StylePriority.UseFont = false;
			this.lblLugarFecha.StylePriority.UseTextAlignment = false;
			this.lblLugarFecha.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			// 
			// xrLabel2
			// 
			this.xrLabel2.Dpi = 254F;
			this.xrLabel2.Font = new System.Drawing.Font("Calibri", 10F);
			this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(104.2937F, 2850.704F);
			this.xrLabel2.Name = "xrLabel2";
			this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
			this.xrLabel2.SizeF = new System.Drawing.SizeF(227.0178F, 47.83643F);
			this.xrLabel2.StylePriority.UseFont = false;
			this.xrLabel2.StylePriority.UseTextAlignment = false;
			this.xrLabel2.Text = "Lugar y fecha:";
			this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			// 
			// prmCliente
			// 
			this.prmCliente.Description = "Parameter1";
			this.prmCliente.MultiValue = true;
			this.prmCliente.Name = "prmCliente";
			this.prmCliente.Visible = false;
			// 
			// prmResidencia
			// 
			this.prmResidencia.Description = "Parameter1";
			this.prmResidencia.Name = "prmResidencia";
			this.prmResidencia.Visible = false;
			// 
			// prmTelefonoCasa
			// 
			this.prmTelefonoCasa.Description = "Parameter1";
			this.prmTelefonoCasa.Name = "prmTelefonoCasa";
			this.prmTelefonoCasa.Visible = false;
			// 
			// prmTelefonoOficina
			// 
			this.prmTelefonoOficina.Description = "Parameter1";
			this.prmTelefonoOficina.Name = "prmTelefonoOficina";
			this.prmTelefonoOficina.Visible = false;
			// 
			// prmTelefonoMovil
			// 
			this.prmTelefonoMovil.Description = "Parameter1";
			this.prmTelefonoMovil.Name = "prmTelefonoMovil";
			this.prmTelefonoMovil.Visible = false;
			// 
			// prmCorreo
			// 
			this.prmCorreo.Description = "Parameter1";
			this.prmCorreo.Name = "prmCorreo";
			this.prmCorreo.Visible = false;
			// 
			// prmVigencia
			// 
			this.prmVigencia.Description = "Parameter1";
			this.prmVigencia.Name = "prmVigencia";
			this.prmVigencia.Visible = false;
			// 
			// rptPersonaNatural
			// 
			this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.GroupHeader1});
			this.Dpi = 254F;
			this.Margins = new System.Drawing.Printing.Margins(172, 172, 95, 145);
			this.PageHeight = 2794;
			this.PageWidth = 2159;
			this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.prmCliente,
            this.prmResidencia,
            this.prmTelefonoCasa,
            this.prmTelefonoOficina,
            this.prmTelefonoMovil,
            this.prmCorreo,
            this.prmVigencia});
			this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
			this.SnapGridSize = 25F;
			this.Version = "22.1";
			this.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.rptPersonaNatural_BeforePrint);
			((System.ComponentModel.ISupportInitialize)(this.xrRichText1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel lblLugarFecha;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.Parameters.Parameter prmCliente;
        private DevExpress.XtraReports.UI.XRRichText xrRichText1;
        private DevExpress.XtraReports.Parameters.Parameter prmResidencia;
        private DevExpress.XtraReports.Parameters.Parameter prmTelefonoCasa;
        private DevExpress.XtraReports.Parameters.Parameter prmTelefonoOficina;
        private DevExpress.XtraReports.Parameters.Parameter prmTelefonoMovil;
        private DevExpress.XtraReports.Parameters.Parameter prmCorreo;
        private DevExpress.XtraReports.Parameters.Parameter prmVigencia;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRPictureBox picLogo;
        private DevExpress.XtraReports.UI.XRLabel lblIdentidad;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
    }
}
