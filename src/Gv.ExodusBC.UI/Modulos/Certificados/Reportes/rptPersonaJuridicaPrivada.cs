﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Gv.ExodusBc.UI;
using Newtonsoft.Json.Linq;
using static Gv.ExodusBc.UI.ExodusBcBase;
using System.Text.RegularExpressions;
using Gv.ExodusBc.LN;

/// <summary>
/// Summary description for rptPersonaJuridica
/// </summary>
public class rptPersonaJuridicaPrivada : DevExpress.XtraReports.UI.XtraReport
{
    private DetailBand Detail;
    private TopMarginBand TopMargin;
    private BottomMarginBand BottomMargin;
    private ReportHeaderBand ReportHeader;
    private XRLabel xrLabel1;
    private GroupHeaderBand GroupHeader1;
    private XRLabel xrLabel5;
    private XRRichText xrRichText1;
    private XRLabel xrLabel6;
    private XRLabel xrLabel3;
    private XRLabel lblLugarFecha;
    private XRLabel xrLabel2;
    private DevExpress.XtraReports.Parameters.Parameter prmRepresentante;
    private DevExpress.XtraReports.Parameters.Parameter prmDireccion;
    private DevExpress.XtraReports.Parameters.Parameter prmTelefonoOficina;
    private DevExpress.XtraReports.Parameters.Parameter prmCorreoOficina;
    private DevExpress.XtraReports.Parameters.Parameter prmVigencia;

    public static int PERSONAID = 0;
    public static string NOMBREREPRESENTANTE = string.Empty;
    public static string NOMBRESOCIEDAD = string.Empty;
    public static string ESTADOCIVIL = string.Empty;
    public static string PROFESION = string.Empty;
    public static string NACIONALIDAD = string.Empty;
    public static string CARGOEMPRESA = string.Empty;
    public static string DIRECCIONOFICINA = string.Empty;
    public static string TELEFONOOFICINA = string.Empty;
    public static string CORREOOFICINA = string.Empty;
    public static string RTN = string.Empty;
    public static string CORREOOREPRESENTANTE= string.Empty;
    public static string IDENTIDADREPRESENTANTE = string.Empty;
    public static string IDENTIDADSINFORMATO = string.Empty;
    public static string IDENTIDADFORMATEADA = string.Empty;
    public static int VIGENCIA = 0;
    private DevExpress.XtraReports.Parameters.Parameter prmSociedad;
    private XRPictureBox picLogo;
    private XRLabel lblRTN;
    private XRLabel lblIdentidadRepresentante;
    private XRLabel xrLabel8;
    private XRLabel xrLabel7;
    private XRLabel xrLabel4;
    private DevExpress.XtraReports.Parameters.Parameter prmEstadoCivil;
    private DevExpress.XtraReports.Parameters.Parameter prmProfesion;
    private DevExpress.XtraReports.Parameters.Parameter prmCargo;
    private DevExpress.XtraReports.Parameters.Parameter prmNacionalidad;
    private DevExpress.XtraReports.Parameters.Parameter prmIdentidadRepresentante;
    private DevExpress.XtraReports.Parameters.Parameter prmIdentificacionFormato;
    private DevExpress.XtraReports.Parameters.Parameter prmRTNEmpresa;
    private XRLabel xrLabel9;
    private DevExpress.XtraReports.Parameters.Parameter prmCorreoRepresentante;



    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    public rptPersonaJuridicaPrivada()
    {
        InitializeComponent();
        //
        // TODO: Add constructor logic here
        //
    }

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptPersonaJuridicaPrivada));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.picLogo = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblIdentidadRepresentante = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblRTN = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrRichText1 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblLugarFecha = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.prmRepresentante = new DevExpress.XtraReports.Parameters.Parameter();
            this.prmDireccion = new DevExpress.XtraReports.Parameters.Parameter();
            this.prmTelefonoOficina = new DevExpress.XtraReports.Parameters.Parameter();
            this.prmCorreoOficina = new DevExpress.XtraReports.Parameters.Parameter();
            this.prmVigencia = new DevExpress.XtraReports.Parameters.Parameter();
            this.prmSociedad = new DevExpress.XtraReports.Parameters.Parameter();
            this.prmEstadoCivil = new DevExpress.XtraReports.Parameters.Parameter();
            this.prmProfesion = new DevExpress.XtraReports.Parameters.Parameter();
            this.prmCargo = new DevExpress.XtraReports.Parameters.Parameter();
            this.prmNacionalidad = new DevExpress.XtraReports.Parameters.Parameter();
            this.prmIdentidadRepresentante = new DevExpress.XtraReports.Parameters.Parameter();
            this.prmIdentificacionFormato = new DevExpress.XtraReports.Parameters.Parameter();
            this.prmRTNEmpresa = new DevExpress.XtraReports.Parameters.Parameter();
            this.prmCorreoRepresentante = new DevExpress.XtraReports.Parameters.Parameter();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 11.18813F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.Detail.Visible = false;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 254F;
            this.TopMargin.HeightF = 95F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 254F;
            this.BottomMargin.HeightF = 26F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.picLogo,
            this.xrLabel1});
            this.ReportHeader.Dpi = 254F;
            this.ReportHeader.HeightF = 268.8166F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // picLogo
            // 
            this.picLogo.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.picLogo.Dpi = 254F;
            this.picLogo.LocationFloat = new DevExpress.Utils.PointFloat(140.5163F, 24.99996F);
            this.picLogo.Name = "picLogo";
            this.picLogo.SizeF = new System.Drawing.SizeF(1517.134F, 118.7688F);
            this.picLogo.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
            this.picLogo.StylePriority.UseBorders = false;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Dpi = 254F;
            this.xrLabel1.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(94.85022F, 185.3966F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(1607.25F, 58.42F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "CONTRATO DE ADHESION PARA USO DE CERTIFICADO DIGITAL POR PERSONA JURIDICA";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel9,
            this.lblIdentidadRepresentante,
            this.xrLabel8,
            this.xrLabel7,
            this.xrLabel4,
            this.lblRTN,
            this.xrLabel5,
            this.xrRichText1,
            this.xrLabel6,
            this.xrLabel3,
            this.lblLugarFecha,
            this.xrLabel2});
            this.GroupHeader1.Dpi = 254F;
            this.GroupHeader1.HeightF = 3624.519F;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // xrLabel9
            // 
            this.xrLabel9.Dpi = 254F;
            this.xrLabel9.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(497.2128F, 3460.555F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(1021.599F, 44.81201F);
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.StylePriority.UseTextAlignment = false;
            this.xrLabel9.Text = "GRUPO VISIÓN, S. DE R.L. DE C.V.";
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblIdentidadRepresentante
            // 
            this.lblIdentidadRepresentante.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblIdentidadRepresentante.BorderWidth = 1F;
            this.lblIdentidadRepresentante.Dpi = 254F;
            this.lblIdentidadRepresentante.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIdentidadRepresentante.LocationFloat = new DevExpress.Utils.PointFloat(497.2128F, 3221.303F);
            this.lblIdentidadRepresentante.Name = "lblIdentidadRepresentante";
            this.lblIdentidadRepresentante.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lblIdentidadRepresentante.SizeF = new System.Drawing.SizeF(1021.599F, 52.37183F);
            this.lblIdentidadRepresentante.StylePriority.UseBorders = false;
            this.lblIdentidadRepresentante.StylePriority.UseBorderWidth = false;
            this.lblIdentidadRepresentante.StylePriority.UseFont = false;
            this.lblIdentidadRepresentante.StylePriority.UseTextAlignment = false;
            this.lblIdentidadRepresentante.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // xrLabel8
            // 
            this.xrLabel8.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel8.BorderWidth = 1F;
            this.xrLabel8.Dpi = 254F;
            this.xrLabel8.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(497.2128F, 3116.56F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(1021.701F, 52.37183F);
            this.xrLabel8.StylePriority.UseBorders = false;
            this.xrLabel8.StylePriority.UseBorderWidth = false;
            this.xrLabel8.StylePriority.UseFont = false;
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // xrLabel7
            // 
            this.xrLabel7.Dpi = 254F;
            this.xrLabel7.Font = new System.Drawing.Font("Calibri", 10F);
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(5.578955F, 3227.602F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(469.1443F, 46.07275F);
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.Text = "Identidad del Representante:";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel4
            // 
            this.xrLabel4.Dpi = 254F;
            this.xrLabel4.Font = new System.Drawing.Font("Calibri", 10F);
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(140.5163F, 3175.23F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(334.2069F, 46.07275F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.Text = "RTN de la Empresa:";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblRTN
            // 
            this.lblRTN.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblRTN.BorderWidth = 1F;
            this.lblRTN.Dpi = 254F;
            this.lblRTN.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRTN.LocationFloat = new DevExpress.Utils.PointFloat(497.2128F, 3168.931F);
            this.lblRTN.Name = "lblRTN";
            this.lblRTN.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lblRTN.SizeF = new System.Drawing.SizeF(1021.599F, 52.37183F);
            this.lblRTN.StylePriority.UseBorders = false;
            this.lblRTN.StylePriority.UseBorderWidth = false;
            this.lblRTN.StylePriority.UseFont = false;
            this.lblRTN.StylePriority.UseTextAlignment = false;
            this.lblRTN.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // xrLabel5
            // 
            this.xrLabel5.Dpi = 254F;
            this.xrLabel5.Font = new System.Drawing.Font("Calibri", 10F);
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(5.579439F, 3388.853F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(469.1438F, 44.81201F);
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.Text = "Firma del Representante Legal:";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrRichText1
            // 
            this.xrRichText1.Dpi = 254F;
            this.xrRichText1.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrRichText1.LocationFloat = new DevExpress.Utils.PointFloat(99.08326F, 51.60818F);
            this.xrRichText1.Name = "xrRichText1";
            this.xrRichText1.SerializableRtfString = resources.GetString("xrRichText1.SerializableRtfString");
            this.xrRichText1.SizeF = new System.Drawing.SizeF(1603.017F, 2813.694F);
            this.xrRichText1.StylePriority.UseFont = false;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel6.Dpi = 254F;
            this.xrLabel6.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(497.2128F, 3388.853F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(1021.599F, 44.81226F);
            this.xrLabel6.StylePriority.UseBorders = false;
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Dpi = 254F;
            this.xrLabel3.Font = new System.Drawing.Font("Calibri", 10F);
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(176.7941F, 3116.56F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(297.9291F, 52.37231F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "Firma de Titular:";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblLugarFecha
            // 
            this.lblLugarFecha.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblLugarFecha.Dpi = 254F;
            this.lblLugarFecha.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLugarFecha.LocationFloat = new DevExpress.Utils.PointFloat(444.7878F, 2915.265F);
            this.lblLugarFecha.Name = "lblLugarFecha";
            this.lblLugarFecha.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lblLugarFecha.SizeF = new System.Drawing.SizeF(1074.024F, 47.83667F);
            this.lblLugarFecha.StylePriority.UseBorders = false;
            this.lblLugarFecha.StylePriority.UseFont = false;
            this.lblLugarFecha.StylePriority.UseTextAlignment = false;
            this.lblLugarFecha.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Dpi = 254F;
            this.xrLabel2.Font = new System.Drawing.Font("Calibri", 10F);
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(212.9704F, 2915.265F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(227.0178F, 47.83643F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "Lugar y fecha:";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // prmRepresentante
            // 
            this.prmRepresentante.Description = "Parameter1";
            this.prmRepresentante.MultiValue = true;
            this.prmRepresentante.Name = "prmRepresentante";
            this.prmRepresentante.Visible = false;
            // 
            // prmDireccion
            // 
            this.prmDireccion.Description = "Parameter1";
            this.prmDireccion.Name = "prmDireccion";
            this.prmDireccion.Visible = false;
            // 
            // prmTelefonoOficina
            // 
            this.prmTelefonoOficina.Description = "Parameter1";
            this.prmTelefonoOficina.Name = "prmTelefonoOficina";
            this.prmTelefonoOficina.Visible = false;
            // 
            // prmCorreoOficina
            // 
            this.prmCorreoOficina.Description = "Parameter1";
            this.prmCorreoOficina.Name = "prmCorreoOficina";
            this.prmCorreoOficina.Visible = false;
            // 
            // prmVigencia
            // 
            this.prmVigencia.Description = "Parameter1";
            this.prmVigencia.Name = "prmVigencia";
            this.prmVigencia.Visible = false;
            // 
            // prmSociedad
            // 
            this.prmSociedad.Description = "Parameter1";
            this.prmSociedad.Name = "prmSociedad";
            this.prmSociedad.Visible = false;
            // 
            // prmEstadoCivil
            // 
            this.prmEstadoCivil.Description = "Parameter1";
            this.prmEstadoCivil.Name = "prmEstadoCivil";
            this.prmEstadoCivil.Visible = false;
            // 
            // prmProfesion
            // 
            this.prmProfesion.Description = "Parameter1";
            this.prmProfesion.Name = "prmProfesion";
            this.prmProfesion.Visible = false;
            // 
            // prmCargo
            // 
            this.prmCargo.Description = "Parameter1";
            this.prmCargo.Name = "prmCargo";
            this.prmCargo.Visible = false;
            // 
            // prmNacionalidad
            // 
            this.prmNacionalidad.Description = "Parameter1";
            this.prmNacionalidad.Name = "prmNacionalidad";
            this.prmNacionalidad.Visible = false;
            // 
            // prmIdentidadRepresentante
            // 
            this.prmIdentidadRepresentante.Description = "Parameter1";
            this.prmIdentidadRepresentante.Name = "prmIdentidadRepresentante";
            this.prmIdentidadRepresentante.Visible = false;
            // 
            // prmIdentificacionFormato
            // 
            this.prmIdentificacionFormato.Description = "Parameter1";
            this.prmIdentificacionFormato.Name = "prmIdentificacionFormato";
            this.prmIdentificacionFormato.Visible = false;
            // 
            // prmRTNEmpresa
            // 
            this.prmRTNEmpresa.Description = "Parameter1";
            this.prmRTNEmpresa.Name = "prmRTNEmpresa";
            this.prmRTNEmpresa.Visible = false;
            // 
            // prmCorreoRepresentante
            // 
            this.prmCorreoRepresentante.Description = "Parameter1";
            this.prmCorreoRepresentante.Name = "prmCorreoRepresentante";
            this.prmCorreoRepresentante.Visible = false;
            // 
            // rptPersonaJuridicaPrivada
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.GroupHeader1});
            this.Dpi = 254F;
            this.Margins = new System.Drawing.Printing.Margins(172, 169, 95, 26);
            this.PageHeight = 2794;
            this.PageWidth = 2159;
            this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.prmRepresentante,
            this.prmDireccion,
            this.prmTelefonoOficina,
            this.prmCorreoOficina,
            this.prmVigencia,
            this.prmSociedad,
            this.prmEstadoCivil,
            this.prmProfesion,
            this.prmCargo,
            this.prmNacionalidad,
            this.prmIdentidadRepresentante,
            this.prmIdentificacionFormato,
            this.prmRTNEmpresa,
            this.prmCorreoRepresentante});
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.SnapGridSize = 25F;
            this.Version = "20.2";
            this.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.rptPersonaJuridicaPrivada_BeforePrint);
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

    }

    #endregion

    private void rptPersonaJuridicaPrivada_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
    {
        GetNumerosLetras();
        picLogo.Image = Gv.ExodusBc.UI.Properties.Resources.Logo_Tecnisign_RGB;
        prmRepresentante.Value = NOMBREREPRESENTANTE;
        prmSociedad.Value = NOMBRESOCIEDAD;
        prmCargo.Value = CARGOEMPRESA;
        prmCorreoRepresentante.Value = CORREOOREPRESENTANTE;
        prmProfesion.Value = PROFESION;
        prmEstadoCivil.Value = ESTADOCIVIL;
        prmNacionalidad.Value = NACIONALIDAD;
        prmDireccion.Value = DIRECCIONOFICINA;
        prmTelefonoOficina.Value = TELEFONOOFICINA;
        prmCorreoOficina.Value = CORREOOFICINA;
        prmVigencia.Value = VIGENCIA;
        prmRTNEmpresa.Value = RTN;
        prmIdentificacionFormato.Value = IDENTIDADFORMATEADA;
        prmIdentidadRepresentante.Value = IDENTIDADSINFORMATO;

        lblLugarFecha.Text = VariablesGlobales.OFFICENAME + " " + Utilities.getFechaActual();
        lblRTN.Text = RTN;
        lblIdentidadRepresentante.Text = IDENTIDADSINFORMATO;
    }

    string GetNumerosLetras()
    {
        string valorFormato = string.Empty;
        string textoCambiar = string.Empty;
        string Rango1 = string.Empty;
        string Rango2 = string.Empty;
        string Rango3 = string.Empty;
        string Rango4 = string.Empty;

        if (IDENTIDADREPRESENTANTE.Length <= (int)DOCUMENTTYPELN.DocumentTypeCaracters.IDENTIDAD) 
        {
            Rango1 = IDENTIDADREPRESENTANTE.Substring(0, 4);
            Rango2 = IDENTIDADREPRESENTANTE.Substring(4, 4);
            Rango3 = IDENTIDADREPRESENTANTE.Substring(8, 5);
            valorFormato = string.Format("{0}-{1}-{2}", Rango1, Rango2, Rango3);
        }
        else if (IDENTIDADREPRESENTANTE.Length == (int)DOCUMENTTYPELN.DocumentTypeCaracters.RTN)
        {
            Rango1 = IDENTIDADREPRESENTANTE.Substring(0, 2);
            Rango2 = IDENTIDADREPRESENTANTE.Substring(2, 4);
            Rango3 = IDENTIDADREPRESENTANTE.Substring(6, 4);
            Rango4 = IDENTIDADREPRESENTANTE.Substring(10, 4);
            valorFormato = string.Format("{0}-{1}-{2}-{3}", Rango1, Rango2, Rango3, Rango4);
        }
        else if (IDENTIDADREPRESENTANTE.Length == (int)DOCUMENTTYPELN.DocumentTypeCaracters.RESIDENTE)
        {
            Rango1 = IDENTIDADREPRESENTANTE.Substring(0, 2);
            Rango2 = IDENTIDADREPRESENTANTE.Substring(2, 4);
            Rango3 = IDENTIDADREPRESENTANTE.Substring(6, 4);
            Rango4 = IDENTIDADREPRESENTANTE.Substring(10, 5);
            valorFormato = string.Format("{0}-{1}-{2}-{3}", Rango1, Rango2, Rango3, Rango4);
        }

        if (valorFormato.Length > 0)
        {
            char[] car = valorFormato.ToCharArray();
            foreach (char c in car)
            {
                if (c.ToString() == "0") textoCambiar += "cero ";
                if (c.ToString() == "1") textoCambiar += "uno ";
                if (c.ToString() == "2") textoCambiar += "dos ";
                if (c.ToString() == "3") textoCambiar += "tres ";
                if (c.ToString() == "-") textoCambiar += "guion ";
                if (c.ToString() == "4") textoCambiar += "cuatro ";
                if (c.ToString() == "5") textoCambiar += "cinco ";
                if (c.ToString() == "6") textoCambiar += "seis ";
                if (c.ToString() == "7") textoCambiar += "siete ";
                if (c.ToString() == "8") textoCambiar += "ocho ";
                if (c.ToString() == "9") textoCambiar += "nueve ";
            }
        }

        IDENTIDADSINFORMATO = valorFormato;
        IDENTIDADFORMATEADA= textoCambiar;
        return textoCambiar;
    }



}
