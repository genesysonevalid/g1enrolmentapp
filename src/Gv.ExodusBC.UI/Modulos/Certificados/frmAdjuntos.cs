﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.IO;
using Gv.Utilidades;
using Gv.ExodusBc.UI.Modulos.Certificados.Reportes;
using DevExpress.XtraPdfViewer.Commands;
using iTextSharp.text.pdf;

namespace Gv.ExodusBc.UI.Modulos.Certificados
{
    public partial class frmAdjuntos : DevExpress.XtraEditors.XtraForm
    {
        public static string Formulario = string.Empty;
        public string AdjuntoTipo = string.Empty;
        public byte[] Archivo;
        public static List<EN.Attached> Documentos = new List<EN.Attached>();
        public static List<EN.RequestAttach> ListaDocumentos = new List<EN.RequestAttach>();
        GdPicture.GdPictureImaging gdTwain = new GdPicture.GdPictureImaging();
        int NumeroDocumentoTwain = 0;
        Image imgDoc = default(Image);
        int idDocumento = 0;
        string Dispositivo = null;
        int DPI = 0;
        public static int TipoPrsTransaccion = 0;
        public static int TotalDocPrsNatural = 0;
        public static int TotalDocPrsJuridica = 0;
        public static int TipoProducto = 0;
        int CantidadMaxAdjuntos = 0;
        PdfViewerCommand zoomIn;
        PdfViewerCommand zoomOut;


        #region VALORES INICIALES DEL FORMULARIO
    
        public frmAdjuntos()
        {
            InitializeComponent();
            Init();
        }

        private void frmAdjuntos_Load(object sender, EventArgs e)
        {

        }

        void Init()
        {
            int docByPerson = LN.DocumentTypePersonLN.GetDocumentByPerson(TipoPrsTransaccion);
            if (docByPerson > 0) CantidadMaxAdjuntos = docByPerson;

            ExodusBcBase.Helper.Combobox.GetComboDocumentByPersonType(lueTipoDocumentoAdjunto, TipoPrsTransaccion);
            if (Documentos.Count <= CantidadMaxAdjuntos) CargarDocumentosGrid();


            btnReporteContrato.Enabled = false;
            rdgTipoContratoJuridico.Enabled = false;

            if(Formulario == "REVISION")
            {
                Documentos.Clear();
                foreach (var ad in frmDetalleSolicitudRevision.lstAdjuntos)
                {
                    EN.Attached doc = new EN.Attached();
                    doc.Numero = ad.Numero;
                    doc.Name = ad.NameDocument;
                    doc.FKDocumentType = ad.FKDocumentType;
                    doc.Type = ad.Type;
                    doc.Size = ad.Size;
                    doc.TipoPersona = ad.TipoPersona;
                    doc.Documento = ad.Imagen;
                    Documentos.Add(doc);
                }
                CargarDocumentosGrid();
            }
            
        }

        #endregion

        #region FUNCIONES DE LOS CONTROLES

        private void btnImportar_Click(object sender, EventArgs e)
        {
            if (Documentos.Count >= CantidadMaxAdjuntos)
            {
                XtraMessageBox.Show("La cantidad de archivos permitidos a adjuntar ya ha llegado a su límite, no puede agregar más", "Atención",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (lueTipoDocumentoAdjunto.Text.Trim().Length == 0)
            {
                XtraMessageBox.Show("Seleccione un tipo de documento a escanear", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                lueTipoDocumentoAdjunto.Focus();
                return;
            }

            //CheckForIllegalCrossThreadCalls = true;
            OpenFileDialog ofd = new OpenFileDialog() {Filter = "Documentos PDF|*.pdf"};
            try
            {
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    long tamanioMB = (VariablesGlobales.AdjuntoTamanioMaximo / 1024) / 1024;
                    string tipo = Path.GetExtension(ofd.FileName);
                    Archivo = File.ReadAllBytes(ofd.FileName);
                    int imagenTamanio = File.ReadAllBytes(ofd.FileName).Length;
                    decimal imagenConvertida = 0;

                    if (imagenTamanio > 0) imagenConvertida = ExodusBcBase.Utilities.FormatByteSize(imagenTamanio);
                    else
                    {
                        XtraMessageBox.Show("Este documento probablemente esta dañado, pruebe con otro documento.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }

                    if (imagenTamanio > VariablesGlobales.AdjuntoTamanioMaximo)
                    {
                        XtraMessageBox.Show(string.Format("No puede adjuntar archivo mayor a {0} MB, por favor intente con otro de igual o menor tamaño.",
                            tamanioMB), "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        lueTipoDocumentoAdjunto.Select();
                        lueTipoDocumentoAdjunto.EditValue = null;
                        return;
                    }

                    MemoryStream pdfStream = new MemoryStream(Archivo);
                    pdfViewerDocs.LoadDocument(pdfStream);
                    pdfViewerDocs.ZoomFactor = 45f;
                    pdfViewerDocs.NavigationPaneInitialVisibility = DevExpress.XtraPdfViewer.PdfNavigationPaneVisibility.Hidden;
                    pdfViewerDocs.ZoomMode = DevExpress.XtraPdfViewer.PdfZoomMode.PageLevel;
                    pdfViewerDocs.Refresh();
                   
                    zoomIn = new PdfZoomInCommand(pdfViewerDocs);
                    zoomOut = new PdfZoomOutCommand(pdfViewerDocs);

                    idDocumento++;
                    EN.Attached doc = new EN.Attached();
                    doc.Numero = idDocumento;
                    doc.Name = lueTipoDocumentoAdjunto.Text;
                    doc.TipoPersona = TipoPrsTransaccion;
                    doc.Documento = Archivo;
                    doc.Type = tipo.Replace(".", "");
                    doc.Size = Math.Round(Convert.ToDecimal(imagenConvertida / 1024), 2) + " Kb";
                    doc.FKDocumentType = Convert.ToInt32(lueTipoDocumentoAdjunto.EditValue);
                    Documentos.Add(doc);
                    

                    //Cargar Clase de Documentos en el Grid
                    CargarDocumentosGrid();

                    //Solicitid Revision
                    if(Formulario == "REVISION")
                    {
                        EN.RequestAttach rq = new EN.RequestAttach
                        {
                            Numero = idDocumento += 1,
                            NameDocument = lueTipoDocumentoAdjunto.Text,
                            Type = tipo.Replace(".", ""),
                            Folder = string.Empty,
                            Size = Math.Round(Convert.ToDecimal(imagenConvertida / 1024), 2) + " Kb",
                            TipoPersona = TipoPrsTransaccion,
                            FKDocumentType = Convert.ToInt32(lueTipoDocumentoAdjunto.EditValue),
                            FKRequestId = 0,
                            Imagen = Archivo
                        };
                        frmDetalleSolicitudRevision.lstAdjuntos.Add(rq);
                        frmDetalleSolicitudRevision ActualizarArchivos = (frmDetalleSolicitudRevision)Application.OpenForms["frmDetalleSolicitudRevision"];
                        ActualizarArchivos.UpdateDocumentosGrid();
                    }


                    //Aumentar Id en 1
                    NumeroDocumentoTwain = idDocumento;

                    imgScanPreview.Image = imgDoc;
                    imgScanPreview.Tag = NumeroDocumentoTwain;

                    //Limpiar controles
                    lueTipoDocumentoAdjunto.EditValue = null;

                    if (doc.TipoPersona == (int)EN.Request.TypePerson.PRSNATURAL | doc.TipoPersona == (int)EN.Request.TypePerson.PROFTITULADO) frmCertificados.TotalDocPersonaNatural += 1;
                    else frmCertificados.TotalDocPersonaJuridica += 1;
                }
            }
            catch (Exception ex)
            {
                Log.InsertarLog(Log.ErrorType.Error, "frmAdjuntos::btnImportar_Click", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }

        }
     
        private void btnEliminar2_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            try
            {
                var numero = gvDocumentosAdjuntos.GetFocusedRowCellValue("Numero");
                if (numero != null)
                {
                    if (XtraMessageBox.Show("¿Está seguro de eliminar el documento?", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        Documentos.Remove(Documentos.Find(x => x.Numero == Convert.ToInt32(numero)));
                        CargarDocumentosGrid();

                        int totDocumentos = Convert.ToInt32(Documentos.Count());
                        int totprsNatural = 0;
                        int totprsJuridica = 0;
                        if (totDocumentos == 0)
                        {
                            frmCertificados.TotalDocPersonaNatural = 0;
                            frmCertificados.TotalDocPersonaJuridica = 0;
                        }
                        else
                        {
                            int tpersona = Convert.ToInt32(gvDocumentosAdjuntos.GetFocusedRowCellValue("TipoPersona"));
                            if (tpersona != 0)
                            {
                                if (tpersona == (int)EN.Request.TypePerson.PRSNATURAL) totprsNatural = totDocumentos;
                                else totprsJuridica = totDocumentos;
                            }

                            if (totprsNatural != 0)
                                frmCertificados.TotalDocPersonaNatural = totprsNatural;
                            if (totprsJuridica != 0)
                                frmCertificados.TotalDocPersonaJuridica = totprsJuridica;
                        }

                        idDocumento--;
                        NumeroDocumentoTwain -= 1;

                        if (Convert.ToInt32(imgScanPreview.Tag) == Convert.ToInt32(numero))
                            imgScanPreview.Image = null;

                        pdfViewerDocs.CloseDocument();
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Seleccione un tipo de documento a escanear", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                Log.InsertarLog(Utilidades.Log.ErrorType.Error, "frmAdjuntos:btnEliminar_Click", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        private void btnVisualizar2_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            
        }

        private void btnVisualizar2_Click(object sender, EventArgs e)
        {
            if (gvDocumentosAdjuntos.GetFocusedRowCellValue("Numero") != null)
            {
                byte[] Archivo = new byte[] { };
                Archivo = Documentos.Find(x => x.Numero == Convert.ToInt32(gvDocumentosAdjuntos.GetFocusedRowCellValue("Numero"))).Documento;
                idDocumento = Convert.ToInt32(gvDocumentosAdjuntos.GetFocusedRowCellValue("Numero"));

                MemoryStream pdfStream = new MemoryStream(Archivo);
                pdfViewerDocs.LoadDocument(pdfStream);
                pdfViewerDocs.ZoomFactor = 45f;
                pdfViewerDocs.NavigationPaneInitialVisibility = DevExpress.XtraPdfViewer.PdfNavigationPaneVisibility.Hidden;
                pdfViewerDocs.ZoomMode = DevExpress.XtraPdfViewer.PdfZoomMode.PageLevel;
            }
        }

        private void btnScanear_Click(object sender, EventArgs e)
        {
            List<byte[]> lstPdf = new List<byte[]>();
            try
            {
                if (Documentos.Count >= CantidadMaxAdjuntos)
                {
                    XtraMessageBox.Show("La cantidad de archivos permitidos a adjuntar ya ha llegado a su límite, no puede agregar más.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                if (lueTipoDocumentoAdjunto.Text.Trim().Length == 0)
                {
                    XtraMessageBox.Show("Seleccione un tipo de documento a escanear", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    lueTipoDocumentoAdjunto.Focus();
                    return;
                }

                int ImageID = 0;
               
                gdTwain.SetLicenseNumber("5329686119069406459411542");

                Dispositivo = VariablesGlobales.EscanerCPNombre;
                DPI = VariablesGlobales.EscanerCPDPI;
                //=================================================================
                NumeroDocumentoTwain = 1;

                if (OpenSelectedSource())
                {
                    //Esconde interfaz de usuario del software del escaner
                    gdTwain.TwainSetHideUI(true);
                    //Muestra el progreso del escaneo
                    gdTwain.TwainSetIndicators(true);
                    //Define la resolucion del escaneo (default 300dpi)
                    gdTwain.TwainSetResolution(DPI);
                    //Define si el escaneo sera en blaco negro/colores etc
                    gdTwain.TwainSetPixelType(GdPicture.TwainPixelType.TWPT_BGR);
                    //Define el nivel de profundida de los bit
                    gdTwain.TwainSetBitDepth(8);
                
                    //Indica si el escaneo se realizara desde el alimentador automatico
                    if (ckeAlimentadorAutomatico.Checked)
                    {
                        string docPdf = string.Empty;
                        bool bContinue = false;
                        docPdf = VariablesGlobales.PathPdf + lueTipoDocumentoAdjunto.Text.Replace("/", "") + ".pdf";
                        //int TemplateSeparatorID = 0;
                        //gdTwain.TwainSelectFeeder(ckeAlimentadorAutomatico.Checked);
                        gdTwain.TwainSetAutoFeed(ckeAlimentadorAutomatico.Checked);

                        gdTwain.TwainOpenDefaultSource(this.Handle);
                        gdTwain.TwainSetAutoFeed(true);
                        gdTwain.TwainSetAutoScan(true); //Achieving the maximum scanning rate.
                        gdTwain.TwainSetResolution(DPI);
                        gdTwain.TwainSetPaperSize(GdPicture.TwainPaperSize.USLETTER);
                        gdTwain.TwainSetBitDepth(1); //1 bpp
                        do
                        {
                            ImageID = gdTwain.TwainAcquireToGdPictureImage(Handle);
                            if (gdTwain.GetStat()==GdPicture.GdPictureStatus.OK)
                            {
                                if (ImageID != 0)
                                {
                                    byte[] Archivo = new byte[] { };
                                    int Tamaño = 0;

                                    gdTwain.CropWhiteBorders(ImageID);
                                    gdTwain.ResizeWidthRatio(ImageID, 1080, System.Drawing.Drawing2D.InterpolationMode.Default);
                                    gdTwain.TwainSetAutoBrightness(true);
                                    gdTwain.TwainSetAutomaticBorderDetection(true);
                                    gdTwain.TwainSetAutomaticDeskew(true);
                                
                                    docPdf = VariablesGlobales.PathPdf + lueTipoDocumentoAdjunto.Text.Replace("/", "") + ".pdf";
                                    gdTwain.SaveAsByteArray(Convert.ToInt32(ImageID), ref Archivo, ref Tamaño, GdPicture.DocumentFormat.DocumentFormatPDF, 75);
                                    gdTwain.SaveAsPDF(Convert.ToInt32(ImageID), docPdf, false, "", "", "", "", "");
                                    Archivo = File.ReadAllBytes(docPdf);

                                    //Agregar el arreglo de byte a una lista
                                    lstPdf.Add(Archivo);
                                    //***Aumentar Id en 1***
                                    idDocumento += 1;

                                    //TempImg = null;
                                    gdTwain.ReleaseGdPictureImage(ImageID);
                                }
                            }
                            if (gdTwain.TwainGetState() <= GdPicture.TwainStatus.TWAIN_SOURCE_ENABLED)
                            {
                                if (MessageBox.Show("¿Desea agregar más páginas?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                                {
                                    bContinue = true;
                                }
                                else
                                {
                                    bContinue = false;
                                    int Tamaño = 0;
                                    byte[] UnionPDF = concatAndAddContent(lstPdf);
                                    MemoryStream pdfStream = new MemoryStream(UnionPDF);
                                    pdfViewerDocs.LoadDocument(pdfStream);
                                    pdfViewerDocs.ZoomFactor = 45f;
                                    pdfViewerDocs.NavigationPaneInitialVisibility = DevExpress.XtraPdfViewer.PdfNavigationPaneVisibility.Hidden;
                                    pdfViewerDocs.ZoomMode = DevExpress.XtraPdfViewer.PdfZoomMode.PageLevel;

                                    //***Agregar Documento a la clase de Documentos***
                                    EN.Attached doc = new EN.Attached();
                                    doc.Numero = idDocumento;
                                    doc.Name = lueTipoDocumentoAdjunto.Text.ToUpper();
                                    doc.FKDocumentType = Convert.ToInt32(lueTipoDocumentoAdjunto.EditValue);
                                    doc.Type = "pdf";
                                    doc.Size = Math.Round(Convert.ToDecimal(pdfStream.Length / 1024), 2) + " Kb";
                                    doc.TipoPersona = TipoPrsTransaccion;
                                    doc.Documento = UnionPDF;
                                    Documentos.Add(doc);

                                    //***Cargar Clase de Documentos en el Grid***
                                    CargarDocumentosGrid();

                                    //***Solicitud Revision***
                                    if (Formulario == "REVISION")
                                    {
                                        EN.RequestAttach rq = new EN.RequestAttach
                                        {
                                            Numero = idDocumento,
                                            NameDocument = lueTipoDocumentoAdjunto.Text,
                                            Type = "pdf",
                                            Folder = string.Empty,
                                            Size = Math.Round(Convert.ToDecimal(pdfStream.Length / 1024), 2) + " Kb",
                                            TipoPersona = TipoPrsTransaccion,
                                            FKDocumentType = Convert.ToInt32(lueTipoDocumentoAdjunto.EditValue),
                                            FKRequestId = 0,
                                            Imagen = UnionPDF
                                        };
                                        frmDetalleSolicitudRevision.lstAdjuntos.Add(rq);
                                        frmDetalleSolicitudRevision ActualizarArchivosRevision = (frmDetalleSolicitudRevision)Application.OpenForms["frmDetalleSolicitudRevision"];
                                        ActualizarArchivosRevision.UpdateDocumentosGrid();
                                    }

                                    if (doc.TipoPersona == (int)EN.Request.TypePerson.PRSNATURAL) frmCertificados.TotalDocPersonaNatural += 1;
                                    else frmCertificados.TotalDocPersonaJuridica += 1;
                                }
                            }
                            else bContinue = true;
                        }
                        while (bContinue);
                    }
                    else
                    {
                        ImageID = gdTwain.TwainAcquireToGdPictureImage(Handle);

                        if (ImageID != 0)
                        {
                            byte[] Archivo = new byte[] { };
                            int Tamaño = 0;
                            string docPdf = string.Empty;

                            gdTwain.CropWhiteBorders(ImageID);
                            gdTwain.ResizeWidthRatio(ImageID, 1080, System.Drawing.Drawing2D.InterpolationMode.Default);
                            gdTwain.TwainSetAutoBrightness(true);
                            gdTwain.TwainSetAutomaticBorderDetection(true);
                            gdTwain.TwainSetAutomaticDeskew(true);
                            //gdTwain.SetContrast(ImageID, 50);
                            //gdTwain.SetBrightness(ImageID, 5);

                            docPdf = VariablesGlobales.PathPdf + lueTipoDocumentoAdjunto.Text.Replace("/", "") + ".pdf";
                            gdTwain.SaveAsByteArray(Convert.ToInt32(ImageID), ref Archivo, ref Tamaño, GdPicture.DocumentFormat.DocumentFormatPDF, 75);
                            gdTwain.SaveAsPDF(Convert.ToInt32(ImageID), docPdf, false, "", "", "", "", "");
                            Archivo = File.ReadAllBytes(docPdf);

                            MemoryStream pdfStream = new MemoryStream(Archivo);
                            pdfViewerDocs.LoadDocument(pdfStream);
                            pdfViewerDocs.ZoomFactor = 45f;
                            pdfViewerDocs.NavigationPaneInitialVisibility = DevExpress.XtraPdfViewer.PdfNavigationPaneVisibility.Hidden;
                            pdfViewerDocs.ZoomMode = DevExpress.XtraPdfViewer.PdfZoomMode.PageLevel;

                            //***Aumentar Id en 1***
                            idDocumento += 1;

                            //***Agregar Documento a la clase de Documentos***
                            EN.Attached doc = new EN.Attached();
                            doc.Numero = idDocumento;
                            doc.Name = lueTipoDocumentoAdjunto.Text.ToUpper();
                            doc.FKDocumentType = Convert.ToInt32(lueTipoDocumentoAdjunto.EditValue);
                            doc.Type = "pdf";
                            doc.Size = Math.Round(Convert.ToDecimal(Tamaño / 1024), 2) + " Kb";
                            doc.TipoPersona = TipoPrsTransaccion;
                            doc.Documento = Archivo;
                            Documentos.Add(doc);


                            //***Cargar Clase de Documentos en el Grid***
                            CargarDocumentosGrid();

                            //***Solicitud Revision***
                            if (Formulario == "REVISION")
                            {
                                EN.RequestAttach rq = new EN.RequestAttach
                                {
                                    Numero = idDocumento,
                                    NameDocument = lueTipoDocumentoAdjunto.Text.ToUpper(),
                                    Type = "pdf",
                                    Folder = string.Empty,
                                    Size = Math.Round(Convert.ToDecimal(pdfStream.Length / 1024), 2) + " Kb",
                                    TipoPersona = TipoPrsTransaccion,
                                    FKDocumentType = Convert.ToInt32(lueTipoDocumentoAdjunto.EditValue),
                                    FKRequestId = 0,
                                    Imagen = Archivo
                                };
                                frmDetalleSolicitudRevision.lstAdjuntos.Add(rq);
                                frmDetalleSolicitudRevision ActualizarArchivosRevision = (frmDetalleSolicitudRevision)Application.OpenForms["frmDetalleSolicitudRevision"];
                                ActualizarArchivosRevision.UpdateDocumentosGrid();
                            }

                            //TempImg = null;
                            gdTwain.ReleaseGdPictureImage(ImageID);

                            if (doc.TipoPersona == (int)EN.Request.TypePerson.PRSNATURAL) frmCertificados.TotalDocPersonaNatural += 1;
                            else frmCertificados.TotalDocPersonaJuridica += 1;
                        }

                        gdTwain.TwainCloseSource();
                        lueTipoDocumentoAdjunto.EditValue = null;
                    }
                }
            }
            catch (Exception ex)
            {
                if (gdTwain.TwainIsDeviceOnline() == true) gdTwain.TwainCloseSource();
            }
        }

        private void btnReporteContrato_Click(object sender, EventArgs e)
        {
            //ssForm.ShowWaitForm();
            if(TipoPrsTransaccion == 2)
            {
                if(rdgTipoContratoJuridico.EditValue == null)
                {
                    if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                    XtraMessageBox.Show("Debe seleccionar el tipo de contrato", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    rdgTipoContratoJuridico.Focus();
                    return;
                }
            }

            frmVisorReporte.TIPOPERSONARPT = TipoPrsTransaccion;
            if (rdgTipoContratoJuridico.SelectedIndex == 0) frmVisorReporte.TIPOCONTRATOJURIDICO = (int)frmVisorReporte.TipoContrato.EmpresaPrivada;
            else frmVisorReporte.TIPOCONTRATOJURIDICO = (int)frmVisorReporte.TipoContrato.EmpresaPublica;
            frmVisorReporte frm = new frmVisorReporte();
            //ssForm.CloseWaitForm();
            frm.ShowDialog();
        }

        public static byte[] concatAndAddContent(List<byte[]> pdfByteContent)
        {
            using (var ms = new MemoryStream())
            {
                using (var doc = new iTextSharp.text.Document())
                {
                    using (var copy = new PdfSmartCopy(doc, ms))
                    {
                        doc.Open();
                        foreach (var p in pdfByteContent)
                        {
                            using (var reader = new PdfReader(p))
                                copy.AddDocument(reader);
                        }
                        doc.Close();
                    }
                }
                return ms.ToArray();
            }
        }

        private void frmAdjuntos_FormClosing(object sender, FormClosingEventArgs e)
        {
            ssForm.ShowWaitForm();
            if (Formulario=="REVISION")
            {
                LimpiarCarpetas();
                ssForm.CloseWaitForm();
            }
            else
            {
                if (gvDocumentosAdjuntos.RowCount == -1)
                {
                    frmCertificados.TotalDocPersonaNatural = 0;
                    frmCertificados.TotalDocPersonaJuridica = 0;
                }
                frmCertificados.lstDocumentosAdjuntados = Documentos;
                frmCertificados ActualizarArchivos = (frmCertificados)Application.OpenForms["frmCertificados"];
                ActualizarArchivos.UpdateAdjuntos();
                LimpiarCarpetas();
                ssForm.CloseWaitForm();
            }
        }

        private void pdfViewerDocs_DockChanged(object sender, EventArgs e)
        {
            pdfViewerDocs.ZoomMode = DevExpress.XtraPdfViewer.PdfZoomMode.PageLevel; 
        }

        private void pdfViewerDocs_PopupMenuShowing(object sender, DevExpress.XtraPdfViewer.PdfPopupMenuShowingEventArgs e)
        {
            e.ItemLinks.Clear();
        }

        private void lueTipoDocumentoAdjunto_EditValueChanged(object sender, EventArgs e)
        {
            if (lueTipoDocumentoAdjunto.Text == "CONTRATO PERSONA NATURAL" 
                | lueTipoDocumentoAdjunto.Text == "CONTRATO PERSONA JURIDICA"
                | lueTipoDocumentoAdjunto.Text == "CONTRATO PERSONA NATURAL DEL PROFESIONAL")
            {
                btnReporteContrato.Enabled = true;
                if(TipoPrsTransaccion == 2) rdgTipoContratoJuridico.Enabled = true;
            }
            else btnReporteContrato.Enabled = false;
        }

        private void btnRotarDerecha_Click(object sender, EventArgs e)
        {
            if (imgDoc == null) return;
            imgDoc.RotateFlip(RotateFlipType.Rotate90FlipNone);
            imgScanPreview.Image = imgDoc;
        }

        private void btnRotarIzquierda_Click(object sender, EventArgs e)
        {
            if (imgDoc == null) return;
            imgDoc.RotateFlip(RotateFlipType.Rotate270FlipNone);
            imgScanPreview.Image = imgDoc;
        }

        private void btnGuardarRotacion_Click(object sender, EventArgs e)
        {
            if (imgDoc == null) return;
            dynamic ms = new MemoryStream();
            imgDoc.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
            dynamic bytes = ms.ToArray();

            var photoInGrid = Documentos.Find(x => x.Numero == idDocumento);
            if (photoInGrid != null) photoInGrid.Documento = bytes;

            //Cargar Clase de Documentos en el Grid
            CargarDocumentosGrid();
        }

        #endregion

        #region METODOS Y PROCEDIMIENTOS
       
        void LimpiarCarpetas()
        {
            try
            {
                DirectoryInfo pdfs = new DirectoryInfo(VariablesGlobales.PathPdf);
                DirectoryInfo wsq = new DirectoryInfo(VariablesGlobales.PathWSQFID);

                //***Eliminar Pdfs***
                foreach (FileInfo file in pdfs.GetFiles())
                    file.Delete();
                foreach (DirectoryInfo dir in pdfs.GetDirectories())
                    dir.Delete(true);

                //***Eliminar Wsqs***
                foreach (FileInfo file in wsq.GetFiles())
                    file.Delete();
                foreach (DirectoryInfo dir in wsq.GetDirectories())
                    dir.Delete(true);
            }
            catch (Exception ex)
            {
                Log.InsertarLog(Log.ErrorType.Error, "frmAdjuntos::LimpiarCarpetas", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        private bool OpenSelectedSource()
        {
            bool respuesta = false;
            respuesta = gdTwain.TwainOpenSource(Handle, Dispositivo);
            if (!respuesta)
                MessageBox.Show("Unable to open selected device.", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return respuesta;
        }

        public void CargarDocumentosGrid()
        {
            List<EN.Attached> DocAdjuntos = new List<EN.Attached>();
            foreach (EN.Attached doc in Documentos)
                if (doc.TipoPersona == TipoPrsTransaccion) DocAdjuntos.Add(doc);

            gcDocs.DataSource = DocAdjuntos;
            gcDocs.RefreshDataSource();
        }

        public List<EN.RequestAttach> GetDocumentosAdjuntos()
        {
            int docByPerson = LN.DocumentTypePersonLN.GetDocumentByPerson(TipoPrsTransaccion);
            if (docByPerson > 0) CantidadMaxAdjuntos = docByPerson;

            if (Documentos.Count <= CantidadMaxAdjuntos)
            {
                foreach (EN.Attached doc in Documentos)
                {
                    EN.RequestAttach rqAttach = new EN.RequestAttach();
                    rqAttach.NameDocument = doc.Name;
                    rqAttach.Imagen = doc.Documento;
                    rqAttach.Type = doc.Type;
                    rqAttach.Size = doc.Size;
                    rqAttach.TipoPersona = doc.TipoPersona;
                    rqAttach.CreationDate = ExodusBcBase.Utilities.getFechaActual();
                    rqAttach.FKDocumentType = doc.FKDocumentType;
                    rqAttach.FKCreateUserId = Convert.ToInt32(VariablesGlobales.USER_ID);
                    ListaDocumentos.Add(rqAttach);
                }
                if (Formulario == "REVISION") frmCertificados.ListaDocumentosAdjuntos = ListaDocumentos;
                else frmCertificados.ListaDocumentosAdjuntos = ListaDocumentos;
            }
            return ListaDocumentos;
        }

        public void limpiarVariables()
        {
            if (Documentos != null) if (Documentos.Count != 0) Documentos.Clear();
            if (ListaDocumentos != null) if (ListaDocumentos.Count != 0) ListaDocumentos.Clear();
        }


        #endregion

        
    }
}