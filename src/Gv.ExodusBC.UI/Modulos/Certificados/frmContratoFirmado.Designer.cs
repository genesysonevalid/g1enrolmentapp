﻿namespace Gv.ExodusBc.UI.Modulos.Certificados
{
    partial class frmContratoFirmado
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmContratoFirmado));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnSalir = new DevExpress.XtraEditors.SimpleButton();
            this.lblNombreFormulario = new DevExpress.XtraEditors.LabelControl();
            this.lblTituloAccion = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.btnLimpiar = new DevExpress.XtraEditors.SimpleButton();
            this.btnBuscarRegistro = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtNumeroTicket = new DevExpress.XtraEditors.TextEdit();
            this.pdfCommandBar1 = new DevExpress.XtraPdfViewer.Bars.PdfCommandBar();
            this.pdfViewerDocs = new DevExpress.XtraPdfViewer.PdfViewer();
            this.btnImportarContrato = new DevExpress.XtraEditors.SimpleButton();
            this.btnGuardarContrato = new DevExpress.XtraEditors.SimpleButton();
            this.pdfCommandBar2 = new DevExpress.XtraPdfViewer.Bars.PdfCommandBar();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.ssForm = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::Gv.ExodusBc.UI.frmWaitForm), true, true);
            this.pnlDatosCliente = new DevExpress.XtraEditors.GroupControl();
            this.lblTipoPersona = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.lblTelefono = new DevExpress.XtraEditors.LabelControl();
            this.lblCorreo = new DevExpress.XtraEditors.LabelControl();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.lblIdentificacion = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.lblNombres = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.pdfZoomInBarItem4 = new DevExpress.XtraPdfViewer.Bars.PdfZoomInBarItem();
            this.barDockControl5 = new DevExpress.XtraBars.BarDockControl();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.pdfCommandBar3 = new DevExpress.XtraPdfViewer.Bars.PdfCommandBar();
            this.pdfPreviousPageBarItem1 = new DevExpress.XtraPdfViewer.Bars.PdfPreviousPageBarItem();
            this.pdfNextPageBarItem1 = new DevExpress.XtraPdfViewer.Bars.PdfNextPageBarItem();
            this.pdfSetPageNumberBarItem1 = new DevExpress.XtraPdfViewer.Bars.PdfSetPageNumberBarItem();
            this.repositoryItemPageNumberEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPageNumberEdit();
            this.pdfZoomOutBarItem1 = new DevExpress.XtraPdfViewer.Bars.PdfZoomOutBarItem();
            this.pdfZoomInBarItem1 = new DevExpress.XtraPdfViewer.Bars.PdfZoomInBarItem();
            this.pdfExactZoomListBarSubItem1 = new DevExpress.XtraPdfViewer.Bars.PdfExactZoomListBarSubItem();
            this.pdfZoom10CheckItem1 = new DevExpress.XtraPdfViewer.Bars.PdfZoom10CheckItem();
            this.pdfZoom25CheckItem1 = new DevExpress.XtraPdfViewer.Bars.PdfZoom25CheckItem();
            this.pdfZoom50CheckItem1 = new DevExpress.XtraPdfViewer.Bars.PdfZoom50CheckItem();
            this.pdfZoom75CheckItem1 = new DevExpress.XtraPdfViewer.Bars.PdfZoom75CheckItem();
            this.pdfZoom100CheckItem1 = new DevExpress.XtraPdfViewer.Bars.PdfZoom100CheckItem();
            this.pdfZoom125CheckItem1 = new DevExpress.XtraPdfViewer.Bars.PdfZoom125CheckItem();
            this.pdfZoom150CheckItem1 = new DevExpress.XtraPdfViewer.Bars.PdfZoom150CheckItem();
            this.pdfZoom200CheckItem1 = new DevExpress.XtraPdfViewer.Bars.PdfZoom200CheckItem();
            this.pdfZoom400CheckItem1 = new DevExpress.XtraPdfViewer.Bars.PdfZoom400CheckItem();
            this.pdfZoom500CheckItem1 = new DevExpress.XtraPdfViewer.Bars.PdfZoom500CheckItem();
            this.pdfSetActualSizeZoomModeCheckItem1 = new DevExpress.XtraPdfViewer.Bars.PdfSetActualSizeZoomModeCheckItem();
            this.pdfSetPageLevelZoomModeCheckItem1 = new DevExpress.XtraPdfViewer.Bars.PdfSetPageLevelZoomModeCheckItem();
            this.pdfSetFitWidthZoomModeCheckItem1 = new DevExpress.XtraPdfViewer.Bars.PdfSetFitWidthZoomModeCheckItem();
            this.pdfSetFitVisibleZoomModeCheckItem1 = new DevExpress.XtraPdfViewer.Bars.PdfSetFitVisibleZoomModeCheckItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.pdfFindTextBarItem1 = new DevExpress.XtraPdfViewer.Bars.PdfFindTextBarItem();
            this.pdfBarController2 = new DevExpress.XtraPdfViewer.Bars.PdfBarController(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumeroTicket.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlDatosCliente)).BeginInit();
            this.pnlDatosCliente.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPageNumberEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pdfBarController2)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.panelControl1.Appearance.Options.UseBackColor = true;
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.btnSalir);
            this.panelControl1.Controls.Add(this.lblNombreFormulario);
            this.panelControl1.Location = new System.Drawing.Point(-9, -2);
            this.panelControl1.LookAndFeel.SkinName = "Blue";
            this.panelControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1248, 71);
            this.panelControl1.TabIndex = 422;
            // 
            // btnSalir
            // 
            this.btnSalir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSalir.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.btnSalir.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalir.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.btnSalir.Appearance.Options.UseBackColor = true;
            this.btnSalir.Appearance.Options.UseFont = true;
            this.btnSalir.Appearance.Options.UseForeColor = true;
            this.btnSalir.Appearance.Options.UseTextOptions = true;
            this.btnSalir.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnSalir.AppearanceHovered.BackColor = System.Drawing.Color.Crimson;
            this.btnSalir.AppearanceHovered.Options.UseBackColor = true;
            this.btnSalir.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btnSalir.Location = new System.Drawing.Point(1188, 4);
            this.btnSalir.Margin = new System.Windows.Forms.Padding(2);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(50, 27);
            this.btnSalir.TabIndex = 72;
            this.btnSalir.Text = "X";
            this.btnSalir.ToolTip = "Salir";
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // lblNombreFormulario
            // 
            this.lblNombreFormulario.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblNombreFormulario.Appearance.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombreFormulario.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblNombreFormulario.Appearance.Options.UseFont = true;
            this.lblNombreFormulario.Appearance.Options.UseForeColor = true;
            this.lblNombreFormulario.Appearance.Options.UseTextOptions = true;
            this.lblNombreFormulario.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblNombreFormulario.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblNombreFormulario.Location = new System.Drawing.Point(9, 12);
            this.lblNombreFormulario.Margin = new System.Windows.Forms.Padding(2);
            this.lblNombreFormulario.Name = "lblNombreFormulario";
            this.lblNombreFormulario.Size = new System.Drawing.Size(1236, 44);
            this.lblNombreFormulario.TabIndex = 72;
            this.lblNombreFormulario.Text = "CONTRATOS FIRMADOS";
            // 
            // lblTituloAccion
            // 
            this.lblTituloAccion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTituloAccion.Appearance.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblTituloAccion.Appearance.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTituloAccion.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblTituloAccion.Appearance.Options.UseBackColor = true;
            this.lblTituloAccion.Appearance.Options.UseFont = true;
            this.lblTituloAccion.Appearance.Options.UseForeColor = true;
            this.lblTituloAccion.Appearance.Options.UseTextOptions = true;
            this.lblTituloAccion.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblTituloAccion.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblTituloAccion.LineColor = System.Drawing.Color.Gainsboro;
            this.lblTituloAccion.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblTituloAccion.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblTituloAccion.LineVisible = true;
            this.lblTituloAccion.Location = new System.Drawing.Point(0, 72);
            this.lblTituloAccion.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblTituloAccion.Name = "lblTituloAccion";
            this.lblTituloAccion.Size = new System.Drawing.Size(1230, 34);
            this.lblTituloAccion.TabIndex = 423;
            this.lblTituloAccion.Text = "Adjuntar los contratos firmados ";
            // 
            // labelControl9
            // 
            this.labelControl9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl9.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl9.Appearance.Options.UseFont = true;
            this.labelControl9.Appearance.Options.UseForeColor = true;
            this.labelControl9.Appearance.Options.UseTextOptions = true;
            this.labelControl9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl9.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl9.LineColor = System.Drawing.Color.Gainsboro;
            this.labelControl9.LineLocation = DevExpress.XtraEditors.LineLocation.Top;
            this.labelControl9.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.labelControl9.LineVisible = true;
            this.labelControl9.Location = new System.Drawing.Point(-9, 65);
            this.labelControl9.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(1250, 42);
            this.labelControl9.TabIndex = 424;
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLimpiar.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.btnLimpiar.Appearance.Options.UseFont = true;
            this.btnLimpiar.Appearance.Options.UseForeColor = true;
            this.btnLimpiar.Appearance.Options.UseTextOptions = true;
            this.btnLimpiar.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnLimpiar.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnLimpiar.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_eraser_32x32;
            this.btnLimpiar.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnLimpiar.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLimpiar.Location = new System.Drawing.Point(1045, 191);
            this.btnLimpiar.LookAndFeel.SkinName = "Whiteprint";
            this.btnLimpiar.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnLimpiar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(174, 37);
            this.btnLimpiar.TabIndex = 428;
            this.btnLimpiar.Text = "&Limpiar";
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // btnBuscarRegistro
            // 
            this.btnBuscarRegistro.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.btnBuscarRegistro.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuscarRegistro.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.btnBuscarRegistro.Appearance.Options.UseBackColor = true;
            this.btnBuscarRegistro.Appearance.Options.UseFont = true;
            this.btnBuscarRegistro.Appearance.Options.UseForeColor = true;
            this.btnBuscarRegistro.Appearance.Options.UseTextOptions = true;
            this.btnBuscarRegistro.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnBuscarRegistro.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnBuscarRegistro.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_search_32x32;
            this.btnBuscarRegistro.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnBuscarRegistro.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnBuscarRegistro.Location = new System.Drawing.Point(525, 191);
            this.btnBuscarRegistro.LookAndFeel.SkinName = "Whiteprint";
            this.btnBuscarRegistro.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnBuscarRegistro.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnBuscarRegistro.Name = "btnBuscarRegistro";
            this.btnBuscarRegistro.Size = new System.Drawing.Size(177, 37);
            this.btnBuscarRegistro.TabIndex = 427;
            this.btnBuscarRegistro.Text = "&Buscar solicitud";
            this.btnBuscarRegistro.Click += new System.EventHandler(this.btnBuscarRegistro_Click);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Appearance.Options.UseForeColor = true;
            this.labelControl1.Location = new System.Drawing.Point(525, 129);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(118, 20);
            this.labelControl1.TabIndex = 442;
            this.labelControl1.Text = "Número de ticket:";
            // 
            // txtNumeroTicket
            // 
            this.txtNumeroTicket.Location = new System.Drawing.Point(525, 155);
            this.txtNumeroTicket.Margin = new System.Windows.Forms.Padding(2);
            this.txtNumeroTicket.Name = "txtNumeroTicket";
            this.txtNumeroTicket.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNumeroTicket.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.txtNumeroTicket.Properties.Appearance.Options.UseFont = true;
            this.txtNumeroTicket.Properties.Appearance.Options.UseForeColor = true;
            this.txtNumeroTicket.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtNumeroTicket.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtNumeroTicket.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtNumeroTicket.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNumeroTicket.Properties.Mask.EditMask = "d";
            this.txtNumeroTicket.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtNumeroTicket.Properties.MaxLength = 11;
            this.txtNumeroTicket.Size = new System.Drawing.Size(694, 30);
            this.txtNumeroTicket.TabIndex = 447;
            // 
            // pdfCommandBar1
            // 
            this.pdfCommandBar1.BarName = "";
            this.pdfCommandBar1.Control = this.pdfViewerDocs;
            this.pdfCommandBar1.DockCol = 0;
            this.pdfCommandBar1.DockRow = 0;
            this.pdfCommandBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.pdfCommandBar1.Offset = 1;
            this.pdfCommandBar1.OptionsBar.AllowQuickCustomization = false;
            this.pdfCommandBar1.OptionsBar.DisableCustomization = true;
            this.pdfCommandBar1.Text = "";
            // 
            // pdfViewerDocs
            // 
            this.pdfViewerDocs.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pdfViewerDocs.Location = new System.Drawing.Point(47, 167);
            this.pdfViewerDocs.LookAndFeel.SkinName = "Metropolis";
            this.pdfViewerDocs.LookAndFeel.UseDefaultLookAndFeel = false;
            this.pdfViewerDocs.Margin = new System.Windows.Forms.Padding(2);
            this.pdfViewerDocs.Name = "pdfViewerDocs";
            this.pdfViewerDocs.NavigationPaneInitialVisibility = DevExpress.XtraPdfViewer.PdfNavigationPaneVisibility.Hidden;
            this.pdfViewerDocs.NavigationPanePageVisibility = DevExpress.XtraPdfViewer.PdfNavigationPanePageVisibility.None;
            this.pdfViewerDocs.Size = new System.Drawing.Size(459, 588);
            this.pdfViewerDocs.TabIndex = 469;
            this.pdfViewerDocs.PopupMenuShowing += new DevExpress.XtraPdfViewer.PdfPopupMenuShowingEventHandler(this.pdfViewerDocs_PopupMenuShowing);
            this.pdfViewerDocs.DockChanged += new System.EventHandler(this.pdfViewerDocs_DockChanged);
            // 
            // btnImportarContrato
            // 
            this.btnImportarContrato.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.btnImportarContrato.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImportarContrato.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.btnImportarContrato.Appearance.Options.UseBackColor = true;
            this.btnImportarContrato.Appearance.Options.UseFont = true;
            this.btnImportarContrato.Appearance.Options.UseForeColor = true;
            this.btnImportarContrato.Appearance.Options.UseTextOptions = true;
            this.btnImportarContrato.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnImportarContrato.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnImportarContrato.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_import_32x32;
            this.btnImportarContrato.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnImportarContrato.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnImportarContrato.Location = new System.Drawing.Point(47, 121);
            this.btnImportarContrato.LookAndFeel.SkinName = "Whiteprint";
            this.btnImportarContrato.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnImportarContrato.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnImportarContrato.Name = "btnImportarContrato";
            this.btnImportarContrato.Size = new System.Drawing.Size(239, 37);
            this.btnImportarContrato.TabIndex = 470;
            this.btnImportarContrato.Text = "&Importar contrato firmado";
            this.btnImportarContrato.Click += new System.EventHandler(this.btnImportarContrato_Click);
            // 
            // btnGuardarContrato
            // 
            this.btnGuardarContrato.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.btnGuardarContrato.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardarContrato.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.btnGuardarContrato.Appearance.Options.UseBackColor = true;
            this.btnGuardarContrato.Appearance.Options.UseFont = true;
            this.btnGuardarContrato.Appearance.Options.UseForeColor = true;
            this.btnGuardarContrato.Appearance.Options.UseTextOptions = true;
            this.btnGuardarContrato.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnGuardarContrato.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnGuardarContrato.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.save_32x32;
            this.btnGuardarContrato.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnGuardarContrato.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnGuardarContrato.Location = new System.Drawing.Point(234, 231);
            this.btnGuardarContrato.LookAndFeel.SkinName = "Whiteprint";
            this.btnGuardarContrato.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnGuardarContrato.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnGuardarContrato.Name = "btnGuardarContrato";
            this.btnGuardarContrato.Size = new System.Drawing.Size(188, 37);
            this.btnGuardarContrato.TabIndex = 471;
            this.btnGuardarContrato.Text = "&Actualizar contrato";
            this.btnGuardarContrato.Click += new System.EventHandler(this.btnGuardarContrato_Click);
            // 
            // pdfCommandBar2
            // 
            this.pdfCommandBar2.BarName = "";
            this.pdfCommandBar2.Control = this.pdfViewerDocs;
            this.pdfCommandBar2.DockCol = 0;
            this.pdfCommandBar2.DockRow = 0;
            this.pdfCommandBar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.pdfCommandBar2.Offset = 1;
            this.pdfCommandBar2.OptionsBar.AllowQuickCustomization = false;
            this.pdfCommandBar2.OptionsBar.DisableCustomization = true;
            this.pdfCommandBar2.Text = "";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Appearance.Options.UseForeColor = true;
            this.labelControl4.Appearance.Options.UseTextOptions = true;
            this.labelControl4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl4.LineColor = System.Drawing.Color.LightGray;
            this.labelControl4.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.labelControl4.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.labelControl4.LineVisible = true;
            this.labelControl4.Location = new System.Drawing.Point(4, 200);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(686, 26);
            this.labelControl4.TabIndex = 476;
            // 
            // ssForm
            // 
            this.ssForm.ClosingDelay = 500;
            // 
            // pnlDatosCliente
            // 
            this.pnlDatosCliente.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pnlDatosCliente.Appearance.Options.UseBackColor = true;
            this.pnlDatosCliente.Controls.Add(this.lblTipoPersona);
            this.pnlDatosCliente.Controls.Add(this.labelControl4);
            this.pnlDatosCliente.Controls.Add(this.labelControl3);
            this.pnlDatosCliente.Controls.Add(this.lblTelefono);
            this.pnlDatosCliente.Controls.Add(this.btnGuardarContrato);
            this.pnlDatosCliente.Controls.Add(this.lblCorreo);
            this.pnlDatosCliente.Controls.Add(this.labelControl18);
            this.pnlDatosCliente.Controls.Add(this.labelControl8);
            this.pnlDatosCliente.Controls.Add(this.lblIdentificacion);
            this.pnlDatosCliente.Controls.Add(this.labelControl6);
            this.pnlDatosCliente.Controls.Add(this.lblNombres);
            this.pnlDatosCliente.Controls.Add(this.labelControl2);
            this.pnlDatosCliente.Location = new System.Drawing.Point(525, 258);
            this.pnlDatosCliente.LookAndFeel.SkinName = "Metropolis";
            this.pnlDatosCliente.LookAndFeel.UseDefaultLookAndFeel = false;
            this.pnlDatosCliente.Name = "pnlDatosCliente";
            this.pnlDatosCliente.Size = new System.Drawing.Size(694, 278);
            this.pnlDatosCliente.TabIndex = 481;
            this.pnlDatosCliente.Text = "Detalle del cliente";
            // 
            // lblTipoPersona
            // 
            this.lblTipoPersona.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipoPersona.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblTipoPersona.Appearance.Options.UseFont = true;
            this.lblTipoPersona.Appearance.Options.UseForeColor = true;
            this.lblTipoPersona.Appearance.Options.UseTextOptions = true;
            this.lblTipoPersona.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblTipoPersona.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblTipoPersona.LineColor = System.Drawing.Color.DimGray;
            this.lblTipoPersona.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblTipoPersona.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblTipoPersona.Location = new System.Drawing.Point(247, 153);
            this.lblTipoPersona.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblTipoPersona.Name = "lblTipoPersona";
            this.lblTipoPersona.Size = new System.Drawing.Size(376, 26);
            this.lblTipoPersona.TabIndex = 476;
            this.lblTipoPersona.Text = "[lblTipoPersona]";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Appearance.Options.UseForeColor = true;
            this.labelControl3.Appearance.Options.UseTextOptions = true;
            this.labelControl3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl3.Location = new System.Drawing.Point(97, 156);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(145, 22);
            this.labelControl3.TabIndex = 475;
            this.labelControl3.Text = "Tipo de persona:";
            // 
            // lblTelefono
            // 
            this.lblTelefono.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTelefono.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblTelefono.Appearance.Options.UseFont = true;
            this.lblTelefono.Appearance.Options.UseForeColor = true;
            this.lblTelefono.Appearance.Options.UseTextOptions = true;
            this.lblTelefono.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblTelefono.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblTelefono.LineColor = System.Drawing.Color.DimGray;
            this.lblTelefono.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblTelefono.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblTelefono.Location = new System.Drawing.Point(247, 123);
            this.lblTelefono.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblTelefono.Name = "lblTelefono";
            this.lblTelefono.Size = new System.Drawing.Size(376, 26);
            this.lblTelefono.TabIndex = 474;
            this.lblTelefono.Text = "[lblTelefono]";
            // 
            // lblCorreo
            // 
            this.lblCorreo.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCorreo.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblCorreo.Appearance.Options.UseFont = true;
            this.lblCorreo.Appearance.Options.UseForeColor = true;
            this.lblCorreo.Appearance.Options.UseTextOptions = true;
            this.lblCorreo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblCorreo.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblCorreo.LineColor = System.Drawing.Color.DimGray;
            this.lblCorreo.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblCorreo.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblCorreo.Location = new System.Drawing.Point(247, 93);
            this.lblCorreo.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblCorreo.Name = "lblCorreo";
            this.lblCorreo.Size = new System.Drawing.Size(376, 26);
            this.lblCorreo.TabIndex = 473;
            this.lblCorreo.Text = "[lblCorreo]";
            // 
            // labelControl18
            // 
            this.labelControl18.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl18.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl18.Appearance.Options.UseFont = true;
            this.labelControl18.Appearance.Options.UseForeColor = true;
            this.labelControl18.Appearance.Options.UseTextOptions = true;
            this.labelControl18.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl18.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl18.Location = new System.Drawing.Point(97, 126);
            this.labelControl18.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(145, 22);
            this.labelControl18.TabIndex = 472;
            this.labelControl18.Text = "Número de teléfono:";
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl8.Appearance.Options.UseFont = true;
            this.labelControl8.Appearance.Options.UseForeColor = true;
            this.labelControl8.Appearance.Options.UseTextOptions = true;
            this.labelControl8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl8.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl8.Location = new System.Drawing.Point(25, 96);
            this.labelControl8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(217, 22);
            this.labelControl8.TabIndex = 471;
            this.labelControl8.Text = "Correo electrónico:";
            // 
            // lblIdentificacion
            // 
            this.lblIdentificacion.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIdentificacion.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblIdentificacion.Appearance.Options.UseFont = true;
            this.lblIdentificacion.Appearance.Options.UseForeColor = true;
            this.lblIdentificacion.Appearance.Options.UseTextOptions = true;
            this.lblIdentificacion.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblIdentificacion.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblIdentificacion.LineColor = System.Drawing.Color.DimGray;
            this.lblIdentificacion.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblIdentificacion.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblIdentificacion.Location = new System.Drawing.Point(247, 33);
            this.lblIdentificacion.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblIdentificacion.Name = "lblIdentificacion";
            this.lblIdentificacion.Size = new System.Drawing.Size(376, 26);
            this.lblIdentificacion.TabIndex = 470;
            this.lblIdentificacion.Text = "[lblIdentificacion]";
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl6.Appearance.Options.UseFont = true;
            this.labelControl6.Appearance.Options.UseForeColor = true;
            this.labelControl6.Appearance.Options.UseTextOptions = true;
            this.labelControl6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl6.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl6.Location = new System.Drawing.Point(18, 37);
            this.labelControl6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(224, 22);
            this.labelControl6.TabIndex = 469;
            this.labelControl6.Text = "Número documento:";
            // 
            // lblNombres
            // 
            this.lblNombres.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombres.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblNombres.Appearance.Options.UseFont = true;
            this.lblNombres.Appearance.Options.UseForeColor = true;
            this.lblNombres.Appearance.Options.UseTextOptions = true;
            this.lblNombres.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblNombres.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblNombres.LineColor = System.Drawing.Color.DimGray;
            this.lblNombres.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblNombres.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblNombres.Location = new System.Drawing.Point(247, 63);
            this.lblNombres.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblNombres.Name = "lblNombres";
            this.lblNombres.Size = new System.Drawing.Size(376, 26);
            this.lblNombres.TabIndex = 468;
            this.lblNombres.Text = "[lblNombres]";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Appearance.Options.UseForeColor = true;
            this.labelControl2.Appearance.Options.UseTextOptions = true;
            this.labelControl2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl2.Location = new System.Drawing.Point(25, 66);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(217, 22);
            this.labelControl2.TabIndex = 467;
            this.labelControl2.Text = "Nombres o denominación social";
            // 
            // pdfZoomInBarItem4
            // 
            this.pdfZoomInBarItem4.Id = 31;
            this.pdfZoomInBarItem4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("pdfZoomInBarItem4.ImageOptions.Image")));
            this.pdfZoomInBarItem4.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("pdfZoomInBarItem4.ImageOptions.LargeImage")));
            this.pdfZoomInBarItem4.Name = "pdfZoomInBarItem4";
            // 
            // barDockControl5
            // 
            this.barDockControl5.CausesValidation = false;
            this.barDockControl5.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl5.Location = new System.Drawing.Point(43, 0);
            this.barDockControl5.Manager = null;
            this.barDockControl5.Margin = new System.Windows.Forms.Padding(2);
            this.barDockControl5.Size = new System.Drawing.Size(1185, 0);
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.pdfCommandBar3});
            this.barManager1.DockControls.Add(this.barDockControl1);
            this.barManager1.DockControls.Add(this.barDockControl2);
            this.barManager1.DockControls.Add(this.barDockControl3);
            this.barManager1.DockControls.Add(this.barDockControl4);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.pdfFindTextBarItem1,
            this.pdfPreviousPageBarItem1,
            this.pdfNextPageBarItem1,
            this.pdfSetPageNumberBarItem1,
            this.pdfZoomOutBarItem1,
            this.pdfZoomInBarItem1,
            this.pdfExactZoomListBarSubItem1,
            this.pdfZoom10CheckItem1,
            this.pdfZoom25CheckItem1,
            this.pdfZoom50CheckItem1,
            this.pdfZoom75CheckItem1,
            this.pdfZoom100CheckItem1,
            this.pdfZoom125CheckItem1,
            this.pdfZoom150CheckItem1,
            this.pdfZoom200CheckItem1,
            this.pdfZoom400CheckItem1,
            this.pdfZoom500CheckItem1,
            this.pdfSetActualSizeZoomModeCheckItem1,
            this.pdfSetPageLevelZoomModeCheckItem1,
            this.pdfSetFitWidthZoomModeCheckItem1,
            this.pdfSetFitVisibleZoomModeCheckItem1});
            this.barManager1.MaxItemId = 50;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPageNumberEdit1});
            // 
            // pdfCommandBar3
            // 
            this.pdfCommandBar3.Control = this.pdfViewerDocs;
            this.pdfCommandBar3.DockCol = 0;
            this.pdfCommandBar3.DockRow = 0;
            this.pdfCommandBar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Left;
            this.pdfCommandBar3.FloatLocation = new System.Drawing.Point(41, 428);
            this.pdfCommandBar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.pdfPreviousPageBarItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.pdfNextPageBarItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.pdfSetPageNumberBarItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.pdfZoomOutBarItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.pdfZoomInBarItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.pdfExactZoomListBarSubItem1)});
            this.pdfCommandBar3.Offset = 168;
            this.pdfCommandBar3.OptionsBar.AllowQuickCustomization = false;
            this.pdfCommandBar3.OptionsBar.DisableCustomization = true;
            // 
            // pdfPreviousPageBarItem1
            // 
            this.pdfPreviousPageBarItem1.Id = 27;
            this.pdfPreviousPageBarItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("pdfPreviousPageBarItem1.ImageOptions.Image")));
            this.pdfPreviousPageBarItem1.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("pdfPreviousPageBarItem1.ImageOptions.LargeImage")));
            this.pdfPreviousPageBarItem1.Name = "pdfPreviousPageBarItem1";
            // 
            // pdfNextPageBarItem1
            // 
            this.pdfNextPageBarItem1.Id = 28;
            this.pdfNextPageBarItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("pdfNextPageBarItem1.ImageOptions.Image")));
            this.pdfNextPageBarItem1.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("pdfNextPageBarItem1.ImageOptions.LargeImage")));
            this.pdfNextPageBarItem1.Name = "pdfNextPageBarItem1";
            // 
            // pdfSetPageNumberBarItem1
            // 
            this.pdfSetPageNumberBarItem1.Edit = this.repositoryItemPageNumberEdit1;
            this.pdfSetPageNumberBarItem1.EditValue = 0;
            this.pdfSetPageNumberBarItem1.Enabled = false;
            this.pdfSetPageNumberBarItem1.Id = 29;
            this.pdfSetPageNumberBarItem1.Name = "pdfSetPageNumberBarItem1";
            // 
            // repositoryItemPageNumberEdit1
            // 
            this.repositoryItemPageNumberEdit1.AutoHeight = false;
            this.repositoryItemPageNumberEdit1.LabelFormat = "de {0}";
            this.repositoryItemPageNumberEdit1.Mask.EditMask = "########;";
            this.repositoryItemPageNumberEdit1.Name = "repositoryItemPageNumberEdit1";
            this.repositoryItemPageNumberEdit1.Orientation = DevExpress.XtraEditors.PagerOrientation.Horizontal;
            // 
            // pdfZoomOutBarItem1
            // 
            this.pdfZoomOutBarItem1.Id = 30;
            this.pdfZoomOutBarItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("pdfZoomOutBarItem1.ImageOptions.Image")));
            this.pdfZoomOutBarItem1.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("pdfZoomOutBarItem1.ImageOptions.LargeImage")));
            this.pdfZoomOutBarItem1.Name = "pdfZoomOutBarItem1";
            // 
            // pdfZoomInBarItem1
            // 
            this.pdfZoomInBarItem1.Id = 31;
            this.pdfZoomInBarItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("pdfZoomInBarItem1.ImageOptions.Image")));
            this.pdfZoomInBarItem1.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("pdfZoomInBarItem1.ImageOptions.LargeImage")));
            this.pdfZoomInBarItem1.Name = "pdfZoomInBarItem1";
            // 
            // pdfExactZoomListBarSubItem1
            // 
            this.pdfExactZoomListBarSubItem1.Id = 32;
            this.pdfExactZoomListBarSubItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("pdfExactZoomListBarSubItem1.ImageOptions.Image")));
            this.pdfExactZoomListBarSubItem1.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("pdfExactZoomListBarSubItem1.ImageOptions.LargeImage")));
            this.pdfExactZoomListBarSubItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.pdfZoom10CheckItem1, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.pdfZoom25CheckItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.pdfZoom50CheckItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.pdfZoom75CheckItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.pdfZoom100CheckItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.pdfZoom125CheckItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.pdfZoom150CheckItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.pdfZoom200CheckItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.pdfZoom400CheckItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.pdfZoom500CheckItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.pdfSetActualSizeZoomModeCheckItem1, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.pdfSetPageLevelZoomModeCheckItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.pdfSetFitWidthZoomModeCheckItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.pdfSetFitVisibleZoomModeCheckItem1)});
            this.pdfExactZoomListBarSubItem1.Name = "pdfExactZoomListBarSubItem1";
            this.pdfExactZoomListBarSubItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionInMenu;
            // 
            // pdfZoom10CheckItem1
            // 
            this.pdfZoom10CheckItem1.Id = 33;
            this.pdfZoom10CheckItem1.Name = "pdfZoom10CheckItem1";
            // 
            // pdfZoom25CheckItem1
            // 
            this.pdfZoom25CheckItem1.Id = 34;
            this.pdfZoom25CheckItem1.Name = "pdfZoom25CheckItem1";
            // 
            // pdfZoom50CheckItem1
            // 
            this.pdfZoom50CheckItem1.Id = 35;
            this.pdfZoom50CheckItem1.Name = "pdfZoom50CheckItem1";
            // 
            // pdfZoom75CheckItem1
            // 
            this.pdfZoom75CheckItem1.Id = 36;
            this.pdfZoom75CheckItem1.Name = "pdfZoom75CheckItem1";
            // 
            // pdfZoom100CheckItem1
            // 
            this.pdfZoom100CheckItem1.Id = 37;
            this.pdfZoom100CheckItem1.Name = "pdfZoom100CheckItem1";
            // 
            // pdfZoom125CheckItem1
            // 
            this.pdfZoom125CheckItem1.Id = 38;
            this.pdfZoom125CheckItem1.Name = "pdfZoom125CheckItem1";
            // 
            // pdfZoom150CheckItem1
            // 
            this.pdfZoom150CheckItem1.Id = 39;
            this.pdfZoom150CheckItem1.Name = "pdfZoom150CheckItem1";
            // 
            // pdfZoom200CheckItem1
            // 
            this.pdfZoom200CheckItem1.Id = 40;
            this.pdfZoom200CheckItem1.Name = "pdfZoom200CheckItem1";
            // 
            // pdfZoom400CheckItem1
            // 
            this.pdfZoom400CheckItem1.Id = 41;
            this.pdfZoom400CheckItem1.Name = "pdfZoom400CheckItem1";
            // 
            // pdfZoom500CheckItem1
            // 
            this.pdfZoom500CheckItem1.Id = 42;
            this.pdfZoom500CheckItem1.Name = "pdfZoom500CheckItem1";
            // 
            // pdfSetActualSizeZoomModeCheckItem1
            // 
            this.pdfSetActualSizeZoomModeCheckItem1.Id = 43;
            this.pdfSetActualSizeZoomModeCheckItem1.Name = "pdfSetActualSizeZoomModeCheckItem1";
            // 
            // pdfSetPageLevelZoomModeCheckItem1
            // 
            this.pdfSetPageLevelZoomModeCheckItem1.Id = 44;
            this.pdfSetPageLevelZoomModeCheckItem1.Name = "pdfSetPageLevelZoomModeCheckItem1";
            // 
            // pdfSetFitWidthZoomModeCheckItem1
            // 
            this.pdfSetFitWidthZoomModeCheckItem1.Id = 45;
            this.pdfSetFitWidthZoomModeCheckItem1.Name = "pdfSetFitWidthZoomModeCheckItem1";
            // 
            // pdfSetFitVisibleZoomModeCheckItem1
            // 
            this.pdfSetFitVisibleZoomModeCheckItem1.Id = 46;
            this.pdfSetFitVisibleZoomModeCheckItem1.Name = "pdfSetFitVisibleZoomModeCheckItem1";
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Manager = this.barManager1;
            this.barDockControl1.Margin = new System.Windows.Forms.Padding(2);
            this.barDockControl1.Size = new System.Drawing.Size(1228, 0);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 766);
            this.barDockControl2.Manager = this.barManager1;
            this.barDockControl2.Margin = new System.Windows.Forms.Padding(2);
            this.barDockControl2.Size = new System.Drawing.Size(1228, 0);
            // 
            // barDockControl3
            // 
            this.barDockControl3.Appearance.BackColor = System.Drawing.Color.White;
            this.barDockControl3.Appearance.Options.UseBackColor = true;
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 0);
            this.barDockControl3.Manager = this.barManager1;
            this.barDockControl3.Margin = new System.Windows.Forms.Padding(2);
            this.barDockControl3.Size = new System.Drawing.Size(43, 766);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(1228, 0);
            this.barDockControl4.Manager = this.barManager1;
            this.barDockControl4.Margin = new System.Windows.Forms.Padding(2);
            this.barDockControl4.Size = new System.Drawing.Size(0, 766);
            // 
            // pdfFindTextBarItem1
            // 
            this.pdfFindTextBarItem1.Id = 26;
            this.pdfFindTextBarItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("pdfFindTextBarItem1.ImageOptions.Image")));
            this.pdfFindTextBarItem1.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("pdfFindTextBarItem1.ImageOptions.LargeImage")));
            this.pdfFindTextBarItem1.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F));
            this.pdfFindTextBarItem1.Name = "pdfFindTextBarItem1";
            // 
            // pdfBarController2
            // 
            this.pdfBarController2.BarItems.Add(this.pdfFindTextBarItem1);
            this.pdfBarController2.BarItems.Add(this.pdfPreviousPageBarItem1);
            this.pdfBarController2.BarItems.Add(this.pdfNextPageBarItem1);
            this.pdfBarController2.BarItems.Add(this.pdfSetPageNumberBarItem1);
            this.pdfBarController2.BarItems.Add(this.pdfZoomOutBarItem1);
            this.pdfBarController2.BarItems.Add(this.pdfZoomInBarItem1);
            this.pdfBarController2.BarItems.Add(this.pdfExactZoomListBarSubItem1);
            this.pdfBarController2.BarItems.Add(this.pdfZoom10CheckItem1);
            this.pdfBarController2.BarItems.Add(this.pdfZoom25CheckItem1);
            this.pdfBarController2.BarItems.Add(this.pdfZoom50CheckItem1);
            this.pdfBarController2.BarItems.Add(this.pdfZoom75CheckItem1);
            this.pdfBarController2.BarItems.Add(this.pdfZoom100CheckItem1);
            this.pdfBarController2.BarItems.Add(this.pdfZoom125CheckItem1);
            this.pdfBarController2.BarItems.Add(this.pdfZoom150CheckItem1);
            this.pdfBarController2.BarItems.Add(this.pdfZoom200CheckItem1);
            this.pdfBarController2.BarItems.Add(this.pdfZoom400CheckItem1);
            this.pdfBarController2.BarItems.Add(this.pdfZoom500CheckItem1);
            this.pdfBarController2.BarItems.Add(this.pdfSetActualSizeZoomModeCheckItem1);
            this.pdfBarController2.BarItems.Add(this.pdfSetPageLevelZoomModeCheckItem1);
            this.pdfBarController2.BarItems.Add(this.pdfSetFitWidthZoomModeCheckItem1);
            this.pdfBarController2.BarItems.Add(this.pdfSetFitVisibleZoomModeCheckItem1);
            this.pdfBarController2.Control = this.pdfViewerDocs;
            // 
            // frmContratoFirmado
            // 
            this.Appearance.BackColor = System.Drawing.Color.White;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1228, 766);
            this.Controls.Add(this.pnlDatosCliente);
            this.Controls.Add(this.txtNumeroTicket);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.btnBuscarRegistro);
            this.Controls.Add(this.btnImportarContrato);
            this.Controls.Add(this.pdfViewerDocs);
            this.Controls.Add(this.btnLimpiar);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.lblTituloAccion);
            this.Controls.Add(this.labelControl9);
            this.Controls.Add(this.barDockControl5);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmContratoFirmado";
            this.Text = "frmContratoFirmado";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtNumeroTicket.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlDatosCliente)).EndInit();
            this.pnlDatosCliente.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPageNumberEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pdfBarController2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton btnSalir;
        private DevExpress.XtraEditors.LabelControl lblNombreFormulario;
        private DevExpress.XtraEditors.LabelControl lblTituloAccion;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.SimpleButton btnLimpiar;
        private DevExpress.XtraEditors.SimpleButton btnBuscarRegistro;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtNumeroTicket;
        private DevExpress.XtraPdfViewer.Bars.PdfCommandBar pdfCommandBar1;
        private DevExpress.XtraPdfViewer.PdfViewer pdfViewerDocs;
        private DevExpress.XtraEditors.SimpleButton btnImportarContrato;
        private DevExpress.XtraEditors.SimpleButton btnGuardarContrato;
        private DevExpress.XtraPdfViewer.Bars.PdfCommandBar pdfCommandBar2;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraSplashScreen.SplashScreenManager ssForm;
        private DevExpress.XtraEditors.GroupControl pnlDatosCliente;
        private DevExpress.XtraEditors.LabelControl lblTipoPersona;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl lblTelefono;
        private DevExpress.XtraEditors.LabelControl lblCorreo;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl lblIdentificacion;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl lblNombres;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraPdfViewer.Bars.PdfZoomInBarItem pdfZoomInBarItem4;
        private DevExpress.XtraBars.BarDockControl barDockControl5;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraPdfViewer.Bars.PdfCommandBar pdfCommandBar3;
        private DevExpress.XtraPdfViewer.Bars.PdfPreviousPageBarItem pdfPreviousPageBarItem1;
        private DevExpress.XtraPdfViewer.Bars.PdfNextPageBarItem pdfNextPageBarItem1;
        private DevExpress.XtraPdfViewer.Bars.PdfSetPageNumberBarItem pdfSetPageNumberBarItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemPageNumberEdit repositoryItemPageNumberEdit1;
        private DevExpress.XtraPdfViewer.Bars.PdfZoomOutBarItem pdfZoomOutBarItem1;
        private DevExpress.XtraPdfViewer.Bars.PdfZoomInBarItem pdfZoomInBarItem1;
        private DevExpress.XtraPdfViewer.Bars.PdfExactZoomListBarSubItem pdfExactZoomListBarSubItem1;
        private DevExpress.XtraPdfViewer.Bars.PdfZoom10CheckItem pdfZoom10CheckItem1;
        private DevExpress.XtraPdfViewer.Bars.PdfZoom25CheckItem pdfZoom25CheckItem1;
        private DevExpress.XtraPdfViewer.Bars.PdfZoom50CheckItem pdfZoom50CheckItem1;
        private DevExpress.XtraPdfViewer.Bars.PdfZoom75CheckItem pdfZoom75CheckItem1;
        private DevExpress.XtraPdfViewer.Bars.PdfZoom100CheckItem pdfZoom100CheckItem1;
        private DevExpress.XtraPdfViewer.Bars.PdfZoom125CheckItem pdfZoom125CheckItem1;
        private DevExpress.XtraPdfViewer.Bars.PdfZoom150CheckItem pdfZoom150CheckItem1;
        private DevExpress.XtraPdfViewer.Bars.PdfZoom200CheckItem pdfZoom200CheckItem1;
        private DevExpress.XtraPdfViewer.Bars.PdfZoom400CheckItem pdfZoom400CheckItem1;
        private DevExpress.XtraPdfViewer.Bars.PdfZoom500CheckItem pdfZoom500CheckItem1;
        private DevExpress.XtraPdfViewer.Bars.PdfSetActualSizeZoomModeCheckItem pdfSetActualSizeZoomModeCheckItem1;
        private DevExpress.XtraPdfViewer.Bars.PdfSetPageLevelZoomModeCheckItem pdfSetPageLevelZoomModeCheckItem1;
        private DevExpress.XtraPdfViewer.Bars.PdfSetFitWidthZoomModeCheckItem pdfSetFitWidthZoomModeCheckItem1;
        private DevExpress.XtraPdfViewer.Bars.PdfSetFitVisibleZoomModeCheckItem pdfSetFitVisibleZoomModeCheckItem1;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraPdfViewer.Bars.PdfFindTextBarItem pdfFindTextBarItem1;
        private DevExpress.XtraPdfViewer.Bars.PdfBarController pdfBarController2;
    }
}