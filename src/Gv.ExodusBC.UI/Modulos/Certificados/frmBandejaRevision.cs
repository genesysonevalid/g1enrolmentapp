﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

using Newtonsoft.Json.Linq;
using static Gv.ExodusBc.UI.ExodusBcBase;
using Gv.Utilidades;
using static Gv.ExodusBc.EN.Request;



namespace Gv.ExodusBc.UI.Modulos.Certificados
{
    public partial class frmBandejaRevision : DevExpress.XtraEditors.XtraForm
    {
        JArray requestTray = null;

        public frmBandejaRevision()
        {
            InitializeComponent();
        }
      
        public void CargarSolicitudesCreadas()
        {
            requestTray = new JArray();
            requestTray = RequestCertificate.GetRequestByStatusRevision((int)TransactionStatus.SOLICITUDENREVISION, VariablesGlobales.OFFICE_ID);
            gcRequestAprobacion.DataSource = null;

            if (requestTray.Count > 0)
            {
                var list = new List<EN.GridRequestRevision>();
                foreach (JObject element in requestTray)
                {
                    EN.GridRequestRevision request = new EN.GridRequestRevision
                    {
                        IDENTIFICACION = element["IDENTIDAD"].ToString(),
                        NOMBRES = element["NOMBRES"].ToString(),
                        APELLIDOS = element["APELLIDOS"].ToString(),
                        SEGUIMIENTO = element["SEGUIMIENTO"].ToString(),
                        RequestId = Convert.ToInt32(element["RequestId"].ToString()),
                        PersonId = Convert.ToInt32(element["FKPersonId"].ToString()),
                        Observacion= element["Observacion"].ToString()
                    };

                    if (element["DocumentNumberLegalPerson"].ToString() as string != string.Empty && element["NameLegalPerson"].ToString() as string != string.Empty)
                    {
                        request.IDENTIFICACION = element["DocumentNumberLegalPerson"].ToString();
                        request.NOMBRES = element["NameLegalPerson"].ToString();
                        request.APELLIDOS = string.Empty;
                    }

                    list.Add(request);
                }
                gcRequestAprobacion.DataSource = list;
                gvRequestAprobacion.Columns[4].Visible = false;
            }
        }

        private void frmBandejaRevision_Load_1(object sender, EventArgs e)
        {
            try
            {

                ssForm.ShowWaitForm();
                CargarSolicitudesCreadas();
                ssForm.CloseWaitForm();
            }
            catch (Exception ex)
            {
                if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                XtraMessageBox.Show("Hubo un error al cargar las solicitudes. Intente en otro momento.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmBandejaRevision::frmBandejaRevision_Load_1", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        private void btnAprobarSolicitud_Click(object sender, EventArgs e)
        {
            int rqId = Convert.ToInt32(gvRequestAprobacion.GetRowCellValue(gvRequestAprobacion.FocusedRowHandle, "RequestId"));
            if (rqId != -1)
            {
                frmDetalleSolicitudRevision.REQUESTID = rqId;
                frmDetalleSolicitudRevision frm = new frmDetalleSolicitudRevision(null, null, null);
                frm.ShowDialog();
            }
        }

        private void btnEditarEnrolamiento_Click(object sender, EventArgs e)
        {
            int personId = Convert.ToInt32(gvRequestAprobacion.GetRowCellValue(gvRequestAprobacion.FocusedRowHandle, "PersonId"));
        }
    }
}