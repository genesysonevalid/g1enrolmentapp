﻿namespace Gv.ExodusBc.UI.Modulos.Certificados
{
    partial class frmProfesionalTitulado
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmProfesionalTitulado));
            this.txtNumeroColegio = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl61 = new DevExpress.XtraEditors.LabelControl();
            this.btnAceptar = new DevExpress.XtraEditors.SimpleButton();
            this.lblTIpoDocumento = new DevExpress.XtraEditors.LabelControl();
            this.lblProfesion = new DevExpress.XtraEditors.LabelControl();
            this.label18 = new System.Windows.Forms.Label();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.label1 = new System.Windows.Forms.Label();
            this.lueColegioProfesional = new DevExpress.XtraEditors.LookUpEdit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumeroColegio.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueColegioProfesional.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // txtNumeroColegio
            // 
            this.txtNumeroColegio.Location = new System.Drawing.Point(27, 166);
            this.txtNumeroColegio.Margin = new System.Windows.Forms.Padding(2);
            this.txtNumeroColegio.Name = "txtNumeroColegio";
            this.txtNumeroColegio.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNumeroColegio.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.txtNumeroColegio.Properties.Appearance.Options.UseFont = true;
            this.txtNumeroColegio.Properties.Appearance.Options.UseForeColor = true;
            this.txtNumeroColegio.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtNumeroColegio.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtNumeroColegio.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtNumeroColegio.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNumeroColegio.Properties.MaxLength = 20;
            this.txtNumeroColegio.Size = new System.Drawing.Size(551, 26);
            this.txtNumeroColegio.TabIndex = 2;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Appearance.Options.UseForeColor = true;
            this.labelControl1.Location = new System.Drawing.Point(27, 145);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(156, 20);
            this.labelControl1.TabIndex = 448;
            this.labelControl1.Text = "Número de colegiación";
            // 
            // labelControl61
            // 
            this.labelControl61.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl61.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl61.Appearance.Options.UseFont = true;
            this.labelControl61.Appearance.Options.UseForeColor = true;
            this.labelControl61.Location = new System.Drawing.Point(27, 15);
            this.labelControl61.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl61.Name = "labelControl61";
            this.labelControl61.Size = new System.Drawing.Size(62, 20);
            this.labelControl61.TabIndex = 550;
            this.labelControl61.Text = "Profesión";
            // 
            // btnAceptar
            // 
            this.btnAceptar.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.btnAceptar.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAceptar.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.btnAceptar.Appearance.Options.UseBackColor = true;
            this.btnAceptar.Appearance.Options.UseFont = true;
            this.btnAceptar.Appearance.Options.UseForeColor = true;
            this.btnAceptar.Appearance.Options.UseTextOptions = true;
            this.btnAceptar.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnAceptar.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnAceptar.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_login_21x32;
            this.btnAceptar.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnAceptar.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnAceptar.Location = new System.Drawing.Point(221, 238);
            this.btnAceptar.LookAndFeel.SkinName = "Office 2013";
            this.btnAceptar.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnAceptar.Margin = new System.Windows.Forms.Padding(4);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(149, 37);
            this.btnAceptar.TabIndex = 5;
            this.btnAceptar.Text = "&Aceptar";
            this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // lblTIpoDocumento
            // 
            this.lblTIpoDocumento.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTIpoDocumento.Appearance.ForeColor = System.Drawing.Color.LightGray;
            this.lblTIpoDocumento.Appearance.Options.UseFont = true;
            this.lblTIpoDocumento.Appearance.Options.UseForeColor = true;
            this.lblTIpoDocumento.Appearance.Options.UseTextOptions = true;
            this.lblTIpoDocumento.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblTIpoDocumento.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblTIpoDocumento.LineColor = System.Drawing.Color.Gainsboro;
            this.lblTIpoDocumento.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblTIpoDocumento.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblTIpoDocumento.LineVisible = true;
            this.lblTIpoDocumento.Location = new System.Drawing.Point(12, 200);
            this.lblTIpoDocumento.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblTIpoDocumento.Name = "lblTIpoDocumento";
            this.lblTIpoDocumento.Size = new System.Drawing.Size(579, 30);
            this.lblTIpoDocumento.TabIndex = 555;
            // 
            // lblProfesion
            // 
            this.lblProfesion.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProfesion.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblProfesion.Appearance.Options.UseFont = true;
            this.lblProfesion.Appearance.Options.UseForeColor = true;
            this.lblProfesion.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblProfesion.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblProfesion.LineVisible = true;
            this.lblProfesion.Location = new System.Drawing.Point(27, 35);
            this.lblProfesion.Name = "lblProfesion";
            this.lblProfesion.Size = new System.Drawing.Size(551, 31);
            this.lblProfesion.TabIndex = 556;
            this.lblProfesion.Text = "[lblProfesion]";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.ForeColor = System.Drawing.Color.Red;
            this.label18.Location = new System.Drawing.Point(188, 150);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(16, 17);
            this.label18.TabIndex = 557;
            this.label18.Text = "*";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Appearance.Options.UseForeColor = true;
            this.labelControl2.Location = new System.Drawing.Point(27, 84);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(68, 20);
            this.labelControl2.TabIndex = 558;
            this.labelControl2.Text = "Institución";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(98, 89);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(16, 17);
            this.label1.TabIndex = 560;
            this.label1.Text = "*";
            // 
            // lueColegioProfesional
            // 
            this.lueColegioProfesional.Location = new System.Drawing.Point(27, 105);
            this.lueColegioProfesional.Margin = new System.Windows.Forms.Padding(2);
            this.lueColegioProfesional.Name = "lueColegioProfesional";
            this.lueColegioProfesional.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueColegioProfesional.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lueColegioProfesional.Properties.Appearance.Options.UseFont = true;
            this.lueColegioProfesional.Properties.Appearance.Options.UseForeColor = true;
            this.lueColegioProfesional.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lueColegioProfesional.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lueColegioProfesional.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.lueColegioProfesional.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueColegioProfesional.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.lueColegioProfesional.Properties.MaxLength = 100;
            this.lueColegioProfesional.Properties.NullText = "";
            this.lueColegioProfesional.Size = new System.Drawing.Size(551, 26);
            this.lueColegioProfesional.TabIndex = 559;
            this.lueColegioProfesional.CustomDrawCell += new DevExpress.XtraEditors.Popup.LookUpCustomDrawCellEventHandler(this.lueColegioProfesional_CustomDrawCell);
            this.lueColegioProfesional.EditValueChanged += new System.EventHandler(this.lueColegioProfesional_EditValueChanged);
            // 
            // frmProfesionalTitulado
            // 
            this.Appearance.BackColor = System.Drawing.Color.White;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(617, 294);
            this.Controls.Add(this.lueColegioProfesional);
            this.Controls.Add(this.txtNumeroColegio);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.lblProfesion);
            this.Controls.Add(this.lblTIpoDocumento);
            this.Controls.Add(this.btnAceptar);
            this.Controls.Add(this.labelControl61);
            this.Controls.Add(this.labelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmProfesionalTitulado";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Profesional Titulado";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmProfesionalTitulado_FormClosing);
            this.Load += new System.EventHandler(this.frmProfesionalTitulado_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtNumeroColegio.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueColegioProfesional.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit txtNumeroColegio;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl61;
        private DevExpress.XtraEditors.SimpleButton btnAceptar;
        private DevExpress.XtraEditors.LabelControl lblTIpoDocumento;
        private DevExpress.XtraEditors.LabelControl lblProfesion;
        private System.Windows.Forms.Label label18;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.LookUpEdit lueColegioProfesional;
    }
}