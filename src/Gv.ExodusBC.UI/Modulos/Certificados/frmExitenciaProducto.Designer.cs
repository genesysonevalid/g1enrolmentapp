﻿namespace Gv.ExodusBc.UI.Modulos.Certificados
{
    partial class frmExitenciaProducto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmExitenciaProducto));
            this.gcStockProductosPn = new DevExpress.XtraGrid.GridControl();
            this.gvStockProductosPn = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.btnVisualizar1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            ((System.ComponentModel.ISupportInitialize)(this.gcStockProductosPn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvStockProductosPn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnVisualizar1)).BeginInit();
            this.SuspendLayout();
            // 
            // gcStockProductosPn
            // 
            this.gcStockProductosPn.EmbeddedNavigator.Appearance.Font = new System.Drawing.Font("Segoe UI", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gcStockProductosPn.EmbeddedNavigator.Appearance.Options.UseFont = true;
            this.gcStockProductosPn.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gcStockProductosPn.Location = new System.Drawing.Point(12, 13);
            this.gcStockProductosPn.MainView = this.gvStockProductosPn;
            this.gcStockProductosPn.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gcStockProductosPn.Name = "gcStockProductosPn";
            this.gcStockProductosPn.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.btnVisualizar1});
            this.gcStockProductosPn.Size = new System.Drawing.Size(1001, 267);
            this.gcStockProductosPn.TabIndex = 474;
            this.gcStockProductosPn.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvStockProductosPn});
            // 
            // gvStockProductosPn
            // 
            this.gvStockProductosPn.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvStockProductosPn.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.gvStockProductosPn.Appearance.HeaderPanel.Options.UseFont = true;
            this.gvStockProductosPn.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvStockProductosPn.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gvStockProductosPn.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gvStockProductosPn.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.gvStockProductosPn.Appearance.OddRow.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvStockProductosPn.Appearance.OddRow.Options.UseBackColor = true;
            this.gvStockProductosPn.Appearance.OddRow.Options.UseFont = true;
            this.gvStockProductosPn.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gvStockProductosPn.Appearance.Row.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvStockProductosPn.Appearance.Row.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.gvStockProductosPn.Appearance.Row.Options.UseBackColor = true;
            this.gvStockProductosPn.Appearance.Row.Options.UseFont = true;
            this.gvStockProductosPn.Appearance.Row.Options.UseForeColor = true;
            this.gvStockProductosPn.GridControl = this.gcStockProductosPn;
            this.gvStockProductosPn.Name = "gvStockProductosPn";
            this.gvStockProductosPn.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gvStockProductosPn.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gvStockProductosPn.OptionsView.ShowGroupPanel = false;
            this.gvStockProductosPn.RowHeight = 30;
            // 
            // btnVisualizar1
            // 
            this.btnVisualizar1.AutoHeight = false;
            editorButtonImageOptions1.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_viewer_32x32;
            this.btnVisualizar1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Visualizar documento", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.btnVisualizar1.ContextImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_viewer_32x32;
            this.btnVisualizar1.Name = "btnVisualizar1";
            this.btnVisualizar1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // frmExitenciaProducto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1025, 287);
            this.Controls.Add(this.gcStockProductosPn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmExitenciaProducto";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Stock de productos";
            this.Load += new System.EventHandler(this.frmExitenciaProducto_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gcStockProductosPn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvStockProductosPn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnVisualizar1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraGrid.GridControl gcStockProductosPn;
        internal DevExpress.XtraGrid.Views.Grid.GridView gvStockProductosPn;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnVisualizar1;
    }
}