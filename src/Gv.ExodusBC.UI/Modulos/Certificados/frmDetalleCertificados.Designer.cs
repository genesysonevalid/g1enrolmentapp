﻿namespace Gv.ExodusBc.UI.Modulos.Certificados
{
    partial class frmDetalleCertificados
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDetalleCertificados));
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.tabCertificado = new DevExpress.XtraTab.XtraTabPage();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.meEstadoActual = new DevExpress.XtraEditors.MemoEdit();
            this.lblNumeroTicket = new DevExpress.XtraEditors.LabelControl();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.meTIpoProducto = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.lblAgenteRegistro = new DevExpress.XtraEditors.LabelControl();
            this.lblCertificateOffice = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.gcSeguimientoCertificado = new DevExpress.XtraGrid.GridControl();
            this.gvSeguimientoCertificado = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.btnVisualizar1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.lblTituloCrearSolicitud = new DevExpress.XtraEditors.LabelControl();
            this.tabSolicitud = new DevExpress.XtraTab.XtraTabPage();
            this.gcSeguimientoSolcitud = new DevExpress.XtraGrid.GridControl();
            this.gvSeguimientoSolcitud = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.lblFechaPago = new DevExpress.XtraEditors.LabelControl();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.lblNumeroFactura = new DevExpress.XtraEditors.LabelControl();
            this.lbl5 = new DevExpress.XtraEditors.LabelControl();
            this.lblFechaCreacion = new DevExpress.XtraEditors.LabelControl();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.lblOficinaCapturo = new DevExpress.XtraEditors.LabelControl();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.lblUsuarioCapturo = new DevExpress.XtraEditors.LabelControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.lblTipoPersona = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.lblTipoDocumento = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.lblApellidos = new DevExpress.XtraEditors.LabelControl();
            this.lblNombres = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.lblNumeroDocumento = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.ssForm = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::Gv.ExodusBc.UI.frmWaitForm), true, true);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.tabCertificado.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.meEstadoActual.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meTIpoProducto.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcSeguimientoCertificado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSeguimientoCertificado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnVisualizar1)).BeginInit();
            this.tabSolicitud.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcSeguimientoSolcitud)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSeguimientoSolcitud)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.xtraTabControl1.Location = new System.Drawing.Point(12, 23);
            this.xtraTabControl1.LookAndFeel.SkinName = "Metropolis";
            this.xtraTabControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.tabCertificado;
            this.xtraTabControl1.Size = new System.Drawing.Size(1258, 606);
            this.xtraTabControl1.TabIndex = 1;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tabCertificado,
            this.tabSolicitud});
            // 
            // tabCertificado
            // 
            this.tabCertificado.Appearance.Header.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabCertificado.Appearance.Header.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_requesok_32x32;
            this.tabCertificado.Appearance.Header.Options.UseFont = true;
            this.tabCertificado.Appearance.Header.Options.UseImage = true;
            this.tabCertificado.Appearance.PageClient.BackColor = System.Drawing.Color.Turquoise;
            this.tabCertificado.Appearance.PageClient.Options.UseBackColor = true;
            this.tabCertificado.Controls.Add(this.labelControl21);
            this.tabCertificado.Controls.Add(this.meEstadoActual);
            this.tabCertificado.Controls.Add(this.lblNumeroTicket);
            this.tabCertificado.Controls.Add(this.labelControl20);
            this.tabCertificado.Controls.Add(this.meTIpoProducto);
            this.tabCertificado.Controls.Add(this.labelControl8);
            this.tabCertificado.Controls.Add(this.labelControl7);
            this.tabCertificado.Controls.Add(this.lblAgenteRegistro);
            this.tabCertificado.Controls.Add(this.lblCertificateOffice);
            this.tabCertificado.Controls.Add(this.labelControl6);
            this.tabCertificado.Controls.Add(this.labelControl5);
            this.tabCertificado.Controls.Add(this.labelControl4);
            this.tabCertificado.Controls.Add(this.gcSeguimientoCertificado);
            this.tabCertificado.Controls.Add(this.labelControl1);
            this.tabCertificado.Controls.Add(this.labelControl3);
            this.tabCertificado.Controls.Add(this.lblTituloCrearSolicitud);
            this.tabCertificado.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_document_24x24;
            this.tabCertificado.Name = "tabCertificado";
            this.tabCertificado.Size = new System.Drawing.Size(1256, 568);
            this.tabCertificado.Text = "CERTIFICADO";
            // 
            // labelControl21
            // 
            this.labelControl21.Appearance.Font = new System.Drawing.Font("Segoe UI Light", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl21.Appearance.ForeColor = System.Drawing.Color.DimGray;
            this.labelControl21.Appearance.Options.UseFont = true;
            this.labelControl21.Appearance.Options.UseForeColor = true;
            this.labelControl21.Appearance.Options.UseTextOptions = true;
            this.labelControl21.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl21.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl21.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl21.LineLocation = DevExpress.XtraEditors.LineLocation.Top;
            this.labelControl21.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.labelControl21.LineVisible = true;
            this.labelControl21.Location = new System.Drawing.Point(765, 164);
            this.labelControl21.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(478, 33);
            this.labelControl21.TabIndex = 488;
            // 
            // meEstadoActual
            // 
            this.meEstadoActual.EditValue = "[meEstadoActual]";
            this.meEstadoActual.Location = new System.Drawing.Point(765, 124);
            this.meEstadoActual.Name = "meEstadoActual";
            this.meEstadoActual.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.meEstadoActual.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.meEstadoActual.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.meEstadoActual.Properties.Appearance.Options.UseBackColor = true;
            this.meEstadoActual.Properties.Appearance.Options.UseFont = true;
            this.meEstadoActual.Properties.Appearance.Options.UseForeColor = true;
            this.meEstadoActual.Properties.Appearance.Options.UseTextOptions = true;
            this.meEstadoActual.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.meEstadoActual.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.meEstadoActual.Properties.ReadOnly = true;
            this.meEstadoActual.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.meEstadoActual.Size = new System.Drawing.Size(478, 58);
            this.meEstadoActual.TabIndex = 487;
            // 
            // lblNumeroTicket
            // 
            this.lblNumeroTicket.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumeroTicket.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblNumeroTicket.Appearance.Options.UseFont = true;
            this.lblNumeroTicket.Appearance.Options.UseForeColor = true;
            this.lblNumeroTicket.Appearance.Options.UseTextOptions = true;
            this.lblNumeroTicket.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblNumeroTicket.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblNumeroTicket.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblNumeroTicket.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblNumeroTicket.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblNumeroTicket.LineVisible = true;
            this.lblNumeroTicket.Location = new System.Drawing.Point(161, 140);
            this.lblNumeroTicket.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblNumeroTicket.Name = "lblNumeroTicket";
            this.lblNumeroTicket.Size = new System.Drawing.Size(416, 26);
            this.lblNumeroTicket.TabIndex = 477;
            this.lblNumeroTicket.Text = "[lblNumeroTicket]";
            // 
            // labelControl20
            // 
            this.labelControl20.Appearance.Font = new System.Drawing.Font("Segoe UI Light", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl20.Appearance.ForeColor = System.Drawing.Color.DimGray;
            this.labelControl20.Appearance.Options.UseFont = true;
            this.labelControl20.Appearance.Options.UseForeColor = true;
            this.labelControl20.Appearance.Options.UseTextOptions = true;
            this.labelControl20.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl20.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl20.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl20.LineLocation = DevExpress.XtraEditors.LineLocation.Top;
            this.labelControl20.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.labelControl20.LineVisible = true;
            this.labelControl20.Location = new System.Drawing.Point(161, 112);
            this.labelControl20.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(416, 33);
            this.labelControl20.TabIndex = 486;
            // 
            // meTIpoProducto
            // 
            this.meTIpoProducto.EditValue = "[meTIpoProducto]";
            this.meTIpoProducto.Location = new System.Drawing.Point(161, 74);
            this.meTIpoProducto.Name = "meTIpoProducto";
            this.meTIpoProducto.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.meTIpoProducto.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.meTIpoProducto.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.meTIpoProducto.Properties.Appearance.Options.UseBackColor = true;
            this.meTIpoProducto.Properties.Appearance.Options.UseFont = true;
            this.meTIpoProducto.Properties.Appearance.Options.UseForeColor = true;
            this.meTIpoProducto.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.meTIpoProducto.Properties.ReadOnly = true;
            this.meTIpoProducto.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.meTIpoProducto.Size = new System.Drawing.Size(416, 58);
            this.meTIpoProducto.TabIndex = 485;
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Segoe UI Light", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Appearance.ForeColor = System.Drawing.Color.DimGray;
            this.labelControl8.Appearance.Options.UseFont = true;
            this.labelControl8.Appearance.Options.UseForeColor = true;
            this.labelControl8.Appearance.Options.UseTextOptions = true;
            this.labelControl8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl8.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl8.LineColor = System.Drawing.Color.Gainsboro;
            this.labelControl8.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.labelControl8.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.labelControl8.LineVisible = true;
            this.labelControl8.Location = new System.Drawing.Point(20, 210);
            this.labelControl8.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(1223, 41);
            this.labelControl8.TabIndex = 484;
            this.labelControl8.Text = "Seguimiento del certificado";
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl7.Appearance.Options.UseFont = true;
            this.labelControl7.Appearance.Options.UseForeColor = true;
            this.labelControl7.Appearance.Options.UseTextOptions = true;
            this.labelControl7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl7.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl7.Location = new System.Drawing.Point(607, 123);
            this.labelControl7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(152, 26);
            this.labelControl7.TabIndex = 482;
            this.labelControl7.Text = "Estado actual:";
            // 
            // lblAgenteRegistro
            // 
            this.lblAgenteRegistro.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAgenteRegistro.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblAgenteRegistro.Appearance.Options.UseFont = true;
            this.lblAgenteRegistro.Appearance.Options.UseForeColor = true;
            this.lblAgenteRegistro.Appearance.Options.UseTextOptions = true;
            this.lblAgenteRegistro.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblAgenteRegistro.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblAgenteRegistro.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblAgenteRegistro.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblAgenteRegistro.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblAgenteRegistro.LineVisible = true;
            this.lblAgenteRegistro.Location = new System.Drawing.Point(765, 95);
            this.lblAgenteRegistro.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblAgenteRegistro.Name = "lblAgenteRegistro";
            this.lblAgenteRegistro.Size = new System.Drawing.Size(478, 26);
            this.lblAgenteRegistro.TabIndex = 481;
            this.lblAgenteRegistro.Text = "[lblAgenteRegistro]";
            // 
            // lblCertificateOffice
            // 
            this.lblCertificateOffice.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCertificateOffice.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblCertificateOffice.Appearance.Options.UseFont = true;
            this.lblCertificateOffice.Appearance.Options.UseForeColor = true;
            this.lblCertificateOffice.Appearance.Options.UseTextOptions = true;
            this.lblCertificateOffice.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblCertificateOffice.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblCertificateOffice.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblCertificateOffice.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblCertificateOffice.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblCertificateOffice.LineVisible = true;
            this.lblCertificateOffice.Location = new System.Drawing.Point(765, 68);
            this.lblCertificateOffice.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblCertificateOffice.Name = "lblCertificateOffice";
            this.lblCertificateOffice.Size = new System.Drawing.Size(478, 26);
            this.lblCertificateOffice.TabIndex = 480;
            this.lblCertificateOffice.Text = "[lblCertificateOffice]";
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Segoe UI Light", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Appearance.ForeColor = System.Drawing.Color.DimGray;
            this.labelControl6.Appearance.Options.UseFont = true;
            this.labelControl6.Appearance.Options.UseForeColor = true;
            this.labelControl6.Appearance.Options.UseTextOptions = true;
            this.labelControl6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl6.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl6.LineColor = System.Drawing.Color.Gainsboro;
            this.labelControl6.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.labelControl6.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.labelControl6.LineVisible = true;
            this.labelControl6.Location = new System.Drawing.Point(627, 19);
            this.labelControl6.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(616, 41);
            this.labelControl6.TabIndex = 479;
            this.labelControl6.Text = "Información de la emisión";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Appearance.Options.UseForeColor = true;
            this.labelControl5.Appearance.Options.UseTextOptions = true;
            this.labelControl5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl5.Location = new System.Drawing.Point(607, 70);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(152, 26);
            this.labelControl5.TabIndex = 478;
            this.labelControl5.Text = "Localidad de servicio:";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Appearance.Options.UseForeColor = true;
            this.labelControl4.Appearance.Options.UseTextOptions = true;
            this.labelControl4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl4.Location = new System.Drawing.Point(20, 144);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(135, 22);
            this.labelControl4.TabIndex = 476;
            this.labelControl4.Text = "Número de ticket:";
            // 
            // gcSeguimientoCertificado
            // 
            this.gcSeguimientoCertificado.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcSeguimientoCertificado.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gcSeguimientoCertificado.Location = new System.Drawing.Point(20, 251);
            this.gcSeguimientoCertificado.MainView = this.gvSeguimientoCertificado;
            this.gcSeguimientoCertificado.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gcSeguimientoCertificado.Name = "gcSeguimientoCertificado";
            this.gcSeguimientoCertificado.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.btnVisualizar1});
            this.gcSeguimientoCertificado.Size = new System.Drawing.Size(1223, 305);
            this.gcSeguimientoCertificado.TabIndex = 475;
            this.gcSeguimientoCertificado.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvSeguimientoCertificado});
            // 
            // gvSeguimientoCertificado
            // 
            this.gvSeguimientoCertificado.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvSeguimientoCertificado.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.gvSeguimientoCertificado.Appearance.HeaderPanel.Options.UseFont = true;
            this.gvSeguimientoCertificado.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvSeguimientoCertificado.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gvSeguimientoCertificado.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gvSeguimientoCertificado.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.gvSeguimientoCertificado.Appearance.OddRow.Options.UseBackColor = true;
            this.gvSeguimientoCertificado.Appearance.Row.Font = new System.Drawing.Font("Segoe UI Semibold", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvSeguimientoCertificado.Appearance.Row.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.gvSeguimientoCertificado.Appearance.Row.Options.UseFont = true;
            this.gvSeguimientoCertificado.Appearance.Row.Options.UseForeColor = true;
            this.gvSeguimientoCertificado.GridControl = this.gcSeguimientoCertificado;
            this.gvSeguimientoCertificado.Name = "gvSeguimientoCertificado";
            this.gvSeguimientoCertificado.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gvSeguimientoCertificado.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gvSeguimientoCertificado.OptionsBehavior.Editable = false;
            this.gvSeguimientoCertificado.OptionsView.ShowGroupPanel = false;
            // 
            // btnVisualizar1
            // 
            this.btnVisualizar1.AutoHeight = false;
            editorButtonImageOptions1.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_viewer_32x32;
            this.btnVisualizar1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Visualizar documento", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.btnVisualizar1.ContextImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_viewer_32x32;
            this.btnVisualizar1.Name = "btnVisualizar1";
            this.btnVisualizar1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Appearance.Options.UseForeColor = true;
            this.labelControl1.Appearance.Options.UseTextOptions = true;
            this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.Location = new System.Drawing.Point(607, 99);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(152, 26);
            this.labelControl1.TabIndex = 457;
            this.labelControl1.Text = "Agente de registro:";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Appearance.Options.UseForeColor = true;
            this.labelControl3.Appearance.Options.UseTextOptions = true;
            this.labelControl3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl3.Location = new System.Drawing.Point(20, 72);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(135, 22);
            this.labelControl3.TabIndex = 455;
            this.labelControl3.Text = "Tipo de producto:";
            // 
            // lblTituloCrearSolicitud
            // 
            this.lblTituloCrearSolicitud.Appearance.Font = new System.Drawing.Font("Segoe UI Light", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTituloCrearSolicitud.Appearance.ForeColor = System.Drawing.Color.DimGray;
            this.lblTituloCrearSolicitud.Appearance.Options.UseFont = true;
            this.lblTituloCrearSolicitud.Appearance.Options.UseForeColor = true;
            this.lblTituloCrearSolicitud.Appearance.Options.UseTextOptions = true;
            this.lblTituloCrearSolicitud.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblTituloCrearSolicitud.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblTituloCrearSolicitud.LineColor = System.Drawing.Color.Gainsboro;
            this.lblTituloCrearSolicitud.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblTituloCrearSolicitud.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblTituloCrearSolicitud.LineVisible = true;
            this.lblTituloCrearSolicitud.Location = new System.Drawing.Point(24, 19);
            this.lblTituloCrearSolicitud.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblTituloCrearSolicitud.Name = "lblTituloCrearSolicitud";
            this.lblTituloCrearSolicitud.Size = new System.Drawing.Size(553, 41);
            this.lblTituloCrearSolicitud.TabIndex = 454;
            this.lblTituloCrearSolicitud.Text = "Información del certificado";
            // 
            // tabSolicitud
            // 
            this.tabSolicitud.Appearance.Header.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabSolicitud.Appearance.Header.Options.UseFont = true;
            this.tabSolicitud.Controls.Add(this.gcSeguimientoSolcitud);
            this.tabSolicitud.Controls.Add(this.labelControl19);
            this.tabSolicitud.Controls.Add(this.lblFechaPago);
            this.tabSolicitud.Controls.Add(this.labelControl18);
            this.tabSolicitud.Controls.Add(this.lblNumeroFactura);
            this.tabSolicitud.Controls.Add(this.lbl5);
            this.tabSolicitud.Controls.Add(this.lblFechaCreacion);
            this.tabSolicitud.Controls.Add(this.labelControl17);
            this.tabSolicitud.Controls.Add(this.lblOficinaCapturo);
            this.tabSolicitud.Controls.Add(this.labelControl16);
            this.tabSolicitud.Controls.Add(this.lblUsuarioCapturo);
            this.tabSolicitud.Controls.Add(this.labelControl15);
            this.tabSolicitud.Controls.Add(this.labelControl14);
            this.tabSolicitud.Controls.Add(this.lblTipoPersona);
            this.tabSolicitud.Controls.Add(this.labelControl2);
            this.tabSolicitud.Controls.Add(this.lblTipoDocumento);
            this.tabSolicitud.Controls.Add(this.labelControl13);
            this.tabSolicitud.Controls.Add(this.lblApellidos);
            this.tabSolicitud.Controls.Add(this.lblNombres);
            this.tabSolicitud.Controls.Add(this.labelControl11);
            this.tabSolicitud.Controls.Add(this.labelControl12);
            this.tabSolicitud.Controls.Add(this.lblNumeroDocumento);
            this.tabSolicitud.Controls.Add(this.labelControl10);
            this.tabSolicitud.Controls.Add(this.labelControl9);
            this.tabSolicitud.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_contract_24x24;
            this.tabSolicitud.Name = "tabSolicitud";
            this.tabSolicitud.Size = new System.Drawing.Size(1256, 568);
            this.tabSolicitud.Text = "SOLICITUD";
            // 
            // gcSeguimientoSolcitud
            // 
            this.gcSeguimientoSolcitud.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.gcSeguimientoSolcitud.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gcSeguimientoSolcitud.Location = new System.Drawing.Point(20, 268);
            this.gcSeguimientoSolcitud.MainView = this.gvSeguimientoSolcitud;
            this.gcSeguimientoSolcitud.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gcSeguimientoSolcitud.Name = "gcSeguimientoSolcitud";
            this.gcSeguimientoSolcitud.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemButtonEdit1});
            this.gcSeguimientoSolcitud.Size = new System.Drawing.Size(1222, 296);
            this.gcSeguimientoSolcitud.TabIndex = 480;
            this.gcSeguimientoSolcitud.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvSeguimientoSolcitud});
            // 
            // gvSeguimientoSolcitud
            // 
            this.gvSeguimientoSolcitud.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvSeguimientoSolcitud.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.gvSeguimientoSolcitud.Appearance.HeaderPanel.Options.UseFont = true;
            this.gvSeguimientoSolcitud.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvSeguimientoSolcitud.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gvSeguimientoSolcitud.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gvSeguimientoSolcitud.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.gvSeguimientoSolcitud.Appearance.OddRow.Options.UseBackColor = true;
            this.gvSeguimientoSolcitud.Appearance.Row.Font = new System.Drawing.Font("Segoe UI Semibold", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvSeguimientoSolcitud.Appearance.Row.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.gvSeguimientoSolcitud.Appearance.Row.Options.UseFont = true;
            this.gvSeguimientoSolcitud.Appearance.Row.Options.UseForeColor = true;
            this.gvSeguimientoSolcitud.GridControl = this.gcSeguimientoSolcitud;
            this.gvSeguimientoSolcitud.Name = "gvSeguimientoSolcitud";
            this.gvSeguimientoSolcitud.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gvSeguimientoSolcitud.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gvSeguimientoSolcitud.OptionsBehavior.Editable = false;
            this.gvSeguimientoSolcitud.OptionsView.ShowGroupPanel = false;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            editorButtonImageOptions2.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_viewer_32x32;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Visualizar documento", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEdit1.ContextImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_viewer_32x32;
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // labelControl19
            // 
            this.labelControl19.Appearance.Font = new System.Drawing.Font("Segoe UI Light", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl19.Appearance.ForeColor = System.Drawing.Color.DimGray;
            this.labelControl19.Appearance.Options.UseFont = true;
            this.labelControl19.Appearance.Options.UseForeColor = true;
            this.labelControl19.Appearance.Options.UseTextOptions = true;
            this.labelControl19.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl19.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl19.LineColor = System.Drawing.Color.Gainsboro;
            this.labelControl19.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.labelControl19.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.labelControl19.LineVisible = true;
            this.labelControl19.Location = new System.Drawing.Point(24, 231);
            this.labelControl19.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(1218, 41);
            this.labelControl19.TabIndex = 485;
            this.labelControl19.Text = "Seguimiento de la solicitud";
            // 
            // lblFechaPago
            // 
            this.lblFechaPago.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFechaPago.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblFechaPago.Appearance.Options.UseFont = true;
            this.lblFechaPago.Appearance.Options.UseForeColor = true;
            this.lblFechaPago.Appearance.Options.UseTextOptions = true;
            this.lblFechaPago.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblFechaPago.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblFechaPago.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblFechaPago.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblFechaPago.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblFechaPago.LineVisible = true;
            this.lblFechaPago.Location = new System.Drawing.Point(783, 175);
            this.lblFechaPago.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblFechaPago.Name = "lblFechaPago";
            this.lblFechaPago.Size = new System.Drawing.Size(459, 26);
            this.lblFechaPago.TabIndex = 479;
            this.lblFechaPago.Text = "[lblFechaPago]";
            // 
            // labelControl18
            // 
            this.labelControl18.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl18.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl18.Appearance.Options.UseFont = true;
            this.labelControl18.Appearance.Options.UseForeColor = true;
            this.labelControl18.Appearance.Options.UseTextOptions = true;
            this.labelControl18.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl18.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl18.Location = new System.Drawing.Point(629, 175);
            this.labelControl18.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(149, 26);
            this.labelControl18.TabIndex = 478;
            this.labelControl18.Text = "Fecha de pago:";
            // 
            // lblNumeroFactura
            // 
            this.lblNumeroFactura.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumeroFactura.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblNumeroFactura.Appearance.Options.UseFont = true;
            this.lblNumeroFactura.Appearance.Options.UseForeColor = true;
            this.lblNumeroFactura.Appearance.Options.UseTextOptions = true;
            this.lblNumeroFactura.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblNumeroFactura.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblNumeroFactura.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblNumeroFactura.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblNumeroFactura.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblNumeroFactura.LineVisible = true;
            this.lblNumeroFactura.Location = new System.Drawing.Point(783, 145);
            this.lblNumeroFactura.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblNumeroFactura.Name = "lblNumeroFactura";
            this.lblNumeroFactura.Size = new System.Drawing.Size(459, 26);
            this.lblNumeroFactura.TabIndex = 477;
            this.lblNumeroFactura.Text = "[lblNumeroFactura]";
            // 
            // lbl5
            // 
            this.lbl5.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl5.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lbl5.Appearance.Options.UseFont = true;
            this.lbl5.Appearance.Options.UseForeColor = true;
            this.lbl5.Appearance.Options.UseTextOptions = true;
            this.lbl5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl5.Location = new System.Drawing.Point(629, 147);
            this.lbl5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lbl5.Name = "lbl5";
            this.lbl5.Size = new System.Drawing.Size(149, 26);
            this.lbl5.TabIndex = 476;
            this.lbl5.Text = "Número de factura:";
            // 
            // lblFechaCreacion
            // 
            this.lblFechaCreacion.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFechaCreacion.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblFechaCreacion.Appearance.Options.UseFont = true;
            this.lblFechaCreacion.Appearance.Options.UseForeColor = true;
            this.lblFechaCreacion.Appearance.Options.UseTextOptions = true;
            this.lblFechaCreacion.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblFechaCreacion.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblFechaCreacion.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblFechaCreacion.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblFechaCreacion.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblFechaCreacion.LineVisible = true;
            this.lblFechaCreacion.Location = new System.Drawing.Point(783, 118);
            this.lblFechaCreacion.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblFechaCreacion.Name = "lblFechaCreacion";
            this.lblFechaCreacion.Size = new System.Drawing.Size(459, 26);
            this.lblFechaCreacion.TabIndex = 475;
            this.lblFechaCreacion.Text = "[lblFechaCreacion]";
            // 
            // labelControl17
            // 
            this.labelControl17.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl17.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl17.Appearance.Options.UseFont = true;
            this.labelControl17.Appearance.Options.UseForeColor = true;
            this.labelControl17.Appearance.Options.UseTextOptions = true;
            this.labelControl17.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl17.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl17.Location = new System.Drawing.Point(629, 118);
            this.labelControl17.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(149, 26);
            this.labelControl17.TabIndex = 474;
            this.labelControl17.Text = "Fecha de creación:";
            // 
            // lblOficinaCapturo
            // 
            this.lblOficinaCapturo.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOficinaCapturo.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblOficinaCapturo.Appearance.Options.UseFont = true;
            this.lblOficinaCapturo.Appearance.Options.UseForeColor = true;
            this.lblOficinaCapturo.Appearance.Options.UseTextOptions = true;
            this.lblOficinaCapturo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblOficinaCapturo.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblOficinaCapturo.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblOficinaCapturo.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblOficinaCapturo.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblOficinaCapturo.LineVisible = true;
            this.lblOficinaCapturo.Location = new System.Drawing.Point(783, 62);
            this.lblOficinaCapturo.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblOficinaCapturo.Name = "lblOficinaCapturo";
            this.lblOficinaCapturo.Size = new System.Drawing.Size(459, 26);
            this.lblOficinaCapturo.TabIndex = 473;
            this.lblOficinaCapturo.Text = "[lblOficinaCapturo]";
            // 
            // labelControl16
            // 
            this.labelControl16.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl16.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl16.Appearance.Options.UseFont = true;
            this.labelControl16.Appearance.Options.UseForeColor = true;
            this.labelControl16.Appearance.Options.UseTextOptions = true;
            this.labelControl16.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl16.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl16.Location = new System.Drawing.Point(629, 64);
            this.labelControl16.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(149, 26);
            this.labelControl16.TabIndex = 472;
            this.labelControl16.Text = "Oficina que capturo:";
            // 
            // lblUsuarioCapturo
            // 
            this.lblUsuarioCapturo.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsuarioCapturo.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblUsuarioCapturo.Appearance.Options.UseFont = true;
            this.lblUsuarioCapturo.Appearance.Options.UseForeColor = true;
            this.lblUsuarioCapturo.Appearance.Options.UseTextOptions = true;
            this.lblUsuarioCapturo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblUsuarioCapturo.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblUsuarioCapturo.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblUsuarioCapturo.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblUsuarioCapturo.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblUsuarioCapturo.LineVisible = true;
            this.lblUsuarioCapturo.Location = new System.Drawing.Point(783, 92);
            this.lblUsuarioCapturo.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblUsuarioCapturo.Name = "lblUsuarioCapturo";
            this.lblUsuarioCapturo.Size = new System.Drawing.Size(459, 26);
            this.lblUsuarioCapturo.TabIndex = 471;
            this.lblUsuarioCapturo.Text = "[lblUsuarioCapturo]";
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl15.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl15.Appearance.Options.UseFont = true;
            this.labelControl15.Appearance.Options.UseForeColor = true;
            this.labelControl15.Appearance.Options.UseTextOptions = true;
            this.labelControl15.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl15.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl15.Location = new System.Drawing.Point(629, 92);
            this.labelControl15.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(149, 26);
            this.labelControl15.TabIndex = 470;
            this.labelControl15.Text = "Usuario que capturo:";
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.Font = new System.Drawing.Font("Segoe UI Light", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl14.Appearance.ForeColor = System.Drawing.Color.DimGray;
            this.labelControl14.Appearance.Options.UseFont = true;
            this.labelControl14.Appearance.Options.UseForeColor = true;
            this.labelControl14.Appearance.Options.UseTextOptions = true;
            this.labelControl14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl14.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl14.LineColor = System.Drawing.Color.Gainsboro;
            this.labelControl14.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.labelControl14.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.labelControl14.LineVisible = true;
            this.labelControl14.Location = new System.Drawing.Point(629, 11);
            this.labelControl14.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(613, 41);
            this.labelControl14.TabIndex = 469;
            this.labelControl14.Text = "Información de la transacción";
            // 
            // lblTipoPersona
            // 
            this.lblTipoPersona.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipoPersona.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblTipoPersona.Appearance.Options.UseFont = true;
            this.lblTipoPersona.Appearance.Options.UseForeColor = true;
            this.lblTipoPersona.Appearance.Options.UseTextOptions = true;
            this.lblTipoPersona.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblTipoPersona.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblTipoPersona.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblTipoPersona.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblTipoPersona.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblTipoPersona.LineVisible = true;
            this.lblTipoPersona.Location = new System.Drawing.Point(164, 59);
            this.lblTipoPersona.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblTipoPersona.Name = "lblTipoPersona";
            this.lblTipoPersona.Size = new System.Drawing.Size(425, 26);
            this.lblTipoPersona.TabIndex = 468;
            this.lblTipoPersona.Text = "[lblTipoPersona]";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Appearance.Options.UseForeColor = true;
            this.labelControl2.Appearance.Options.UseTextOptions = true;
            this.labelControl2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl2.Location = new System.Drawing.Point(24, 61);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(135, 26);
            this.labelControl2.TabIndex = 467;
            this.labelControl2.Text = "Tipo de persona:";
            // 
            // lblTipoDocumento
            // 
            this.lblTipoDocumento.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipoDocumento.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblTipoDocumento.Appearance.Options.UseFont = true;
            this.lblTipoDocumento.Appearance.Options.UseForeColor = true;
            this.lblTipoDocumento.Appearance.Options.UseTextOptions = true;
            this.lblTipoDocumento.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblTipoDocumento.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblTipoDocumento.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblTipoDocumento.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblTipoDocumento.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblTipoDocumento.LineVisible = true;
            this.lblTipoDocumento.Location = new System.Drawing.Point(164, 88);
            this.lblTipoDocumento.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblTipoDocumento.Name = "lblTipoDocumento";
            this.lblTipoDocumento.Size = new System.Drawing.Size(425, 26);
            this.lblTipoDocumento.TabIndex = 466;
            this.lblTipoDocumento.Text = "[lblTipoDocumento]";
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl13.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl13.Appearance.Options.UseFont = true;
            this.labelControl13.Appearance.Options.UseForeColor = true;
            this.labelControl13.Appearance.Options.UseTextOptions = true;
            this.labelControl13.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl13.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl13.Location = new System.Drawing.Point(20, 89);
            this.labelControl13.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(139, 26);
            this.labelControl13.TabIndex = 465;
            this.labelControl13.Text = "Tipo documento:";
            // 
            // lblApellidos
            // 
            this.lblApellidos.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblApellidos.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblApellidos.Appearance.Options.UseFont = true;
            this.lblApellidos.Appearance.Options.UseForeColor = true;
            this.lblApellidos.Appearance.Options.UseTextOptions = true;
            this.lblApellidos.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblApellidos.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblApellidos.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblApellidos.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblApellidos.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblApellidos.LineVisible = true;
            this.lblApellidos.Location = new System.Drawing.Point(164, 175);
            this.lblApellidos.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblApellidos.Name = "lblApellidos";
            this.lblApellidos.Size = new System.Drawing.Size(425, 26);
            this.lblApellidos.TabIndex = 464;
            this.lblApellidos.Text = "[lblApellidos]";
            // 
            // lblNombres
            // 
            this.lblNombres.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombres.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblNombres.Appearance.Options.UseFont = true;
            this.lblNombres.Appearance.Options.UseForeColor = true;
            this.lblNombres.Appearance.Options.UseTextOptions = true;
            this.lblNombres.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblNombres.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblNombres.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblNombres.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblNombres.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblNombres.LineVisible = true;
            this.lblNombres.Location = new System.Drawing.Point(164, 146);
            this.lblNombres.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblNombres.Name = "lblNombres";
            this.lblNombres.Size = new System.Drawing.Size(425, 26);
            this.lblNombres.TabIndex = 463;
            this.lblNombres.Text = "[lblNombres]";
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl11.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl11.Appearance.Options.UseFont = true;
            this.labelControl11.Appearance.Options.UseForeColor = true;
            this.labelControl11.Appearance.Options.UseTextOptions = true;
            this.labelControl11.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl11.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl11.Location = new System.Drawing.Point(20, 174);
            this.labelControl11.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(139, 26);
            this.labelControl11.TabIndex = 462;
            this.labelControl11.Text = "Apellidos:";
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl12.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl12.Appearance.Options.UseFont = true;
            this.labelControl12.Appearance.Options.UseForeColor = true;
            this.labelControl12.Appearance.Options.UseTextOptions = true;
            this.labelControl12.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl12.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl12.Location = new System.Drawing.Point(20, 146);
            this.labelControl12.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(139, 26);
            this.labelControl12.TabIndex = 461;
            this.labelControl12.Text = "Nombres:";
            // 
            // lblNumeroDocumento
            // 
            this.lblNumeroDocumento.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumeroDocumento.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblNumeroDocumento.Appearance.Options.UseFont = true;
            this.lblNumeroDocumento.Appearance.Options.UseForeColor = true;
            this.lblNumeroDocumento.Appearance.Options.UseTextOptions = true;
            this.lblNumeroDocumento.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblNumeroDocumento.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblNumeroDocumento.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblNumeroDocumento.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblNumeroDocumento.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblNumeroDocumento.LineVisible = true;
            this.lblNumeroDocumento.Location = new System.Drawing.Point(164, 117);
            this.lblNumeroDocumento.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblNumeroDocumento.Name = "lblNumeroDocumento";
            this.lblNumeroDocumento.Size = new System.Drawing.Size(425, 26);
            this.lblNumeroDocumento.TabIndex = 460;
            this.lblNumeroDocumento.Text = "[lblNumeroDocumento]";
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl10.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl10.Appearance.Options.UseFont = true;
            this.labelControl10.Appearance.Options.UseForeColor = true;
            this.labelControl10.Appearance.Options.UseTextOptions = true;
            this.labelControl10.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl10.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl10.Location = new System.Drawing.Point(20, 117);
            this.labelControl10.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(139, 26);
            this.labelControl10.TabIndex = 459;
            this.labelControl10.Text = "Num. Documento:";
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Segoe UI Light", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl9.Appearance.ForeColor = System.Drawing.Color.DimGray;
            this.labelControl9.Appearance.Options.UseFont = true;
            this.labelControl9.Appearance.Options.UseForeColor = true;
            this.labelControl9.Appearance.Options.UseTextOptions = true;
            this.labelControl9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl9.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl9.LineColor = System.Drawing.Color.Gainsboro;
            this.labelControl9.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.labelControl9.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.labelControl9.LineVisible = true;
            this.labelControl9.Location = new System.Drawing.Point(20, 11);
            this.labelControl9.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(569, 41);
            this.labelControl9.TabIndex = 455;
            this.labelControl9.Text = "Información del solicitante";
            // 
            // ssForm
            // 
            this.ssForm.ClosingDelay = 500;
            // 
            // frmDetalleCertificados
            // 
            this.Appearance.BackColor = System.Drawing.Color.White;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1282, 641);
            this.Controls.Add(this.xtraTabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDetalleCertificados";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Detalle de los certificados";
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.tabCertificado.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.meEstadoActual.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meTIpoProducto.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcSeguimientoCertificado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSeguimientoCertificado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnVisualizar1)).EndInit();
            this.tabSolicitud.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcSeguimientoSolcitud)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSeguimientoSolcitud)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage tabCertificado;
        private DevExpress.XtraTab.XtraTabPage tabSolicitud;
        private DevExpress.XtraEditors.LabelControl lblTituloCrearSolicitud;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        internal DevExpress.XtraGrid.GridControl gcSeguimientoCertificado;
        internal DevExpress.XtraGrid.Views.Grid.GridView gvSeguimientoCertificado;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnVisualizar1;
        private DevExpress.XtraEditors.LabelControl lblNumeroTicket;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl lblCertificateOffice;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl lblAgenteRegistro;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl lblNumeroDocumento;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LabelControl lblApellidos;
        private DevExpress.XtraEditors.LabelControl lblNombres;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl lblTipoDocumento;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl lblTipoPersona;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.LabelControl lblUsuarioCapturo;
        private DevExpress.XtraEditors.LabelControl lblOficinaCapturo;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.LabelControl lblFechaCreacion;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.LabelControl lblNumeroFactura;
        private DevExpress.XtraEditors.LabelControl lbl5;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.LabelControl lblFechaPago;
        internal DevExpress.XtraGrid.GridControl gcSeguimientoSolcitud;
        internal DevExpress.XtraGrid.Views.Grid.GridView gvSeguimientoSolcitud;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraSplashScreen.SplashScreenManager ssForm;
        private DevExpress.XtraEditors.MemoEdit meTIpoProducto;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.MemoEdit meEstadoActual;
        private DevExpress.XtraEditors.LabelControl labelControl21;
    }
}