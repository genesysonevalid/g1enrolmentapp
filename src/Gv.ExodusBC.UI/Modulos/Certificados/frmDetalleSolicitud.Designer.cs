﻿namespace Gv.ExodusBc.UI.Modulos.Certificados
{
    partial class frmDetalleSolicitud
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDetalleSolicitud));
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.btnEnviarRevision = new DevExpress.XtraEditors.SimpleButton();
            this.btnRechazar = new DevExpress.XtraEditors.SimpleButton();
            this.pnlPersonaNatural = new DevExpress.XtraEditors.PanelControl();
            this.lblNumeroColegio = new DevExpress.XtraEditors.LabelControl();
            this.lblTituloColegio = new DevExpress.XtraEditors.LabelControl();
            this.meDescripcionProductoPn = new DevExpress.XtraEditors.MemoEdit();
            this.lblOficinacCreoPn = new DevExpress.XtraEditors.LabelControl();
            this.labelControl31 = new DevExpress.XtraEditors.LabelControl();
            this.lblUsuarioCreoPn = new DevExpress.XtraEditors.LabelControl();
            this.labelControl29 = new DevExpress.XtraEditors.LabelControl();
            this.lblFechaCreacionPn = new DevExpress.XtraEditors.LabelControl();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl27 = new DevExpress.XtraEditors.LabelControl();
            this.picSolicitante = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.meDireccion = new DevExpress.XtraEditors.MemoEdit();
            this.lblMunicipio = new DevExpress.XtraEditors.LabelControl();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.lblDepartamento = new DevExpress.XtraEditors.LabelControl();
            this.lblNacionalidad = new DevExpress.XtraEditors.LabelControl();
            this.lblEstadoCivil = new DevExpress.XtraEditors.LabelControl();
            this.lblSexo = new DevExpress.XtraEditors.LabelControl();
            this.lblFechaNacimiento = new DevExpress.XtraEditors.LabelControl();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.lblApellidos = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.gcAdjuntosPn = new DevExpress.XtraGrid.GridControl();
            this.gvAdjuntosPn = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NombrePn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisualizar = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnVisualizar1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.lblTelefonoPn = new DevExpress.XtraEditors.LabelControl();
            this.lblNombresPn = new DevExpress.XtraEditors.LabelControl();
            this.lblCorreoPn = new DevExpress.XtraEditors.LabelControl();
            this.lblNumeroDocumentoPn = new DevExpress.XtraEditors.LabelControl();
            this.lblProductoPn = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.pnlPersonaJuridica = new DevExpress.XtraEditors.PanelControl();
            this.meDescripcionProductoPj = new DevExpress.XtraEditors.MemoEdit();
            this.lblOficinacCreoPj = new DevExpress.XtraEditors.LabelControl();
            this.labelControl30 = new DevExpress.XtraEditors.LabelControl();
            this.lblUsuarioCreoPj = new DevExpress.XtraEditors.LabelControl();
            this.labelControl33 = new DevExpress.XtraEditors.LabelControl();
            this.lblFechaCreacionPj = new DevExpress.XtraEditors.LabelControl();
            this.labelControl35 = new DevExpress.XtraEditors.LabelControl();
            this.gcAdjuntosPj = new DevExpress.XtraGrid.GridControl();
            this.gvAdjuntosPj = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NameDocument = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnVisualizarDocPj = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.gcRepresentantes = new DevExpress.XtraGrid.GridControl();
            this.gvRepresentantes = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnVisualizarRepresentante = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryItemButtonEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.lblTelefonoPj = new DevExpress.XtraEditors.LabelControl();
            this.labelControl26 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.lblCorreoPj = new DevExpress.XtraEditors.LabelControl();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.lblNumeroDocumentoPj = new DevExpress.XtraEditors.LabelControl();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.lblNombrePj = new DevExpress.XtraEditors.LabelControl();
            this.lblProductoPj = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.btnAprobarSolicitud = new DevExpress.XtraEditors.SimpleButton();
            this.picPaso1 = new DevExpress.XtraEditors.PictureEdit();
            this.ssForm = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::Gv.ExodusBc.UI.frmWaitForm), true, true);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlPersonaNatural)).BeginInit();
            this.pnlPersonaNatural.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.meDescripcionProductoPn.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSolicitante.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meDireccion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcAdjuntosPn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvAdjuntosPn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnVisualizar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlPersonaJuridica)).BeginInit();
            this.pnlPersonaJuridica.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.meDescripcionProductoPj.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcAdjuntosPj)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvAdjuntosPj)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnVisualizarDocPj)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcRepresentantes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvRepresentantes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnVisualizarRepresentante)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPaso1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.AppearanceCaption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.AppearanceCaption.Options.UseForeColor = true;
            this.groupControl1.AppearanceCaption.Options.UseTextOptions = true;
            this.groupControl1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.groupControl1.Controls.Add(this.btnEnviarRevision);
            this.groupControl1.Controls.Add(this.btnRechazar);
            this.groupControl1.Controls.Add(this.pnlPersonaNatural);
            this.groupControl1.Controls.Add(this.pnlPersonaJuridica);
            this.groupControl1.Controls.Add(this.btnAprobarSolicitud);
            this.groupControl1.Controls.Add(this.picPaso1);
            this.groupControl1.Location = new System.Drawing.Point(7, 12);
            this.groupControl1.LookAndFeel.SkinMaskColor = System.Drawing.Color.AliceBlue;
            this.groupControl1.LookAndFeel.SkinName = "Whiteprint";
            this.groupControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.groupControl1.Margin = new System.Windows.Forms.Padding(2);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1433, 709);
            this.groupControl1.TabIndex = 313;
            this.groupControl1.Text = "      Aprobación de la solicitud";
            // 
            // btnEnviarRevision
            // 
            this.btnEnviarRevision.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.btnEnviarRevision.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEnviarRevision.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.btnEnviarRevision.Appearance.Options.UseBackColor = true;
            this.btnEnviarRevision.Appearance.Options.UseFont = true;
            this.btnEnviarRevision.Appearance.Options.UseForeColor = true;
            this.btnEnviarRevision.Appearance.Options.UseTextOptions = true;
            this.btnEnviarRevision.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnEnviarRevision.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnEnviarRevision.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_import_32x32;
            this.btnEnviarRevision.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnEnviarRevision.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnEnviarRevision.Location = new System.Drawing.Point(199, 666);
            this.btnEnviarRevision.LookAndFeel.SkinName = "Whiteprint";
            this.btnEnviarRevision.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnEnviarRevision.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnEnviarRevision.Name = "btnEnviarRevision";
            this.btnEnviarRevision.Size = new System.Drawing.Size(174, 35);
            this.btnEnviarRevision.TabIndex = 461;
            this.btnEnviarRevision.Text = "&Enviar a revisión";
            this.btnEnviarRevision.Click += new System.EventHandler(this.btnEnviarRevision_Click);
            // 
            // btnRechazar
            // 
            this.btnRechazar.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.btnRechazar.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRechazar.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.btnRechazar.Appearance.Options.UseBackColor = true;
            this.btnRechazar.Appearance.Options.UseFont = true;
            this.btnRechazar.Appearance.Options.UseForeColor = true;
            this.btnRechazar.Appearance.Options.UseTextOptions = true;
            this.btnRechazar.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnRechazar.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnRechazar.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.cancel_form;
            this.btnRechazar.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnRechazar.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnRechazar.Location = new System.Drawing.Point(388, 666);
            this.btnRechazar.LookAndFeel.SkinName = "Whiteprint";
            this.btnRechazar.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnRechazar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnRechazar.Name = "btnRechazar";
            this.btnRechazar.Size = new System.Drawing.Size(174, 35);
            this.btnRechazar.TabIndex = 460;
            this.btnRechazar.Text = "&Rechazar solicitud";
            this.btnRechazar.Click += new System.EventHandler(this.btnRechazar_Click);
            // 
            // pnlPersonaNatural
            // 
            this.pnlPersonaNatural.Appearance.BorderColor = System.Drawing.Color.Gray;
            this.pnlPersonaNatural.Appearance.Options.UseBorderColor = true;
            this.pnlPersonaNatural.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.pnlPersonaNatural.Controls.Add(this.lblNumeroColegio);
            this.pnlPersonaNatural.Controls.Add(this.lblTituloColegio);
            this.pnlPersonaNatural.Controls.Add(this.meDescripcionProductoPn);
            this.pnlPersonaNatural.Controls.Add(this.lblOficinacCreoPn);
            this.pnlPersonaNatural.Controls.Add(this.labelControl31);
            this.pnlPersonaNatural.Controls.Add(this.lblUsuarioCreoPn);
            this.pnlPersonaNatural.Controls.Add(this.labelControl29);
            this.pnlPersonaNatural.Controls.Add(this.lblFechaCreacionPn);
            this.pnlPersonaNatural.Controls.Add(this.labelControl25);
            this.pnlPersonaNatural.Controls.Add(this.labelControl27);
            this.pnlPersonaNatural.Controls.Add(this.picSolicitante);
            this.pnlPersonaNatural.Controls.Add(this.labelControl23);
            this.pnlPersonaNatural.Controls.Add(this.meDireccion);
            this.pnlPersonaNatural.Controls.Add(this.lblMunicipio);
            this.pnlPersonaNatural.Controls.Add(this.labelControl20);
            this.pnlPersonaNatural.Controls.Add(this.lblDepartamento);
            this.pnlPersonaNatural.Controls.Add(this.lblNacionalidad);
            this.pnlPersonaNatural.Controls.Add(this.lblEstadoCivil);
            this.pnlPersonaNatural.Controls.Add(this.lblSexo);
            this.pnlPersonaNatural.Controls.Add(this.lblFechaNacimiento);
            this.pnlPersonaNatural.Controls.Add(this.labelControl18);
            this.pnlPersonaNatural.Controls.Add(this.labelControl15);
            this.pnlPersonaNatural.Controls.Add(this.labelControl17);
            this.pnlPersonaNatural.Controls.Add(this.lblApellidos);
            this.pnlPersonaNatural.Controls.Add(this.labelControl11);
            this.pnlPersonaNatural.Controls.Add(this.labelControl12);
            this.pnlPersonaNatural.Controls.Add(this.labelControl7);
            this.pnlPersonaNatural.Controls.Add(this.labelControl5);
            this.pnlPersonaNatural.Controls.Add(this.labelControl9);
            this.pnlPersonaNatural.Controls.Add(this.labelControl4);
            this.pnlPersonaNatural.Controls.Add(this.labelControl1);
            this.pnlPersonaNatural.Controls.Add(this.labelControl2);
            this.pnlPersonaNatural.Controls.Add(this.gcAdjuntosPn);
            this.pnlPersonaNatural.Controls.Add(this.labelControl3);
            this.pnlPersonaNatural.Controls.Add(this.labelControl13);
            this.pnlPersonaNatural.Controls.Add(this.lblTelefonoPn);
            this.pnlPersonaNatural.Controls.Add(this.lblNombresPn);
            this.pnlPersonaNatural.Controls.Add(this.lblCorreoPn);
            this.pnlPersonaNatural.Controls.Add(this.lblNumeroDocumentoPn);
            this.pnlPersonaNatural.Controls.Add(this.lblProductoPn);
            this.pnlPersonaNatural.Controls.Add(this.labelControl6);
            this.pnlPersonaNatural.Location = new System.Drawing.Point(10, 49);
            this.pnlPersonaNatural.Margin = new System.Windows.Forms.Padding(2);
            this.pnlPersonaNatural.Name = "pnlPersonaNatural";
            this.pnlPersonaNatural.Size = new System.Drawing.Size(1405, 340);
            this.pnlPersonaNatural.TabIndex = 459;
            // 
            // lblNumeroColegio
            // 
            this.lblNumeroColegio.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumeroColegio.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblNumeroColegio.Appearance.Options.UseFont = true;
            this.lblNumeroColegio.Appearance.Options.UseForeColor = true;
            this.lblNumeroColegio.Appearance.Options.UseTextOptions = true;
            this.lblNumeroColegio.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblNumeroColegio.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblNumeroColegio.LineColor = System.Drawing.Color.DimGray;
            this.lblNumeroColegio.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblNumeroColegio.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblNumeroColegio.LineVisible = true;
            this.lblNumeroColegio.Location = new System.Drawing.Point(700, 4);
            this.lblNumeroColegio.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblNumeroColegio.Name = "lblNumeroColegio";
            this.lblNumeroColegio.Size = new System.Drawing.Size(179, 30);
            this.lblNumeroColegio.TabIndex = 497;
            this.lblNumeroColegio.Text = "[lblNumeroColegio]";
            // 
            // lblTituloColegio
            // 
            this.lblTituloColegio.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTituloColegio.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblTituloColegio.Appearance.Options.UseFont = true;
            this.lblTituloColegio.Appearance.Options.UseForeColor = true;
            this.lblTituloColegio.Appearance.Options.UseTextOptions = true;
            this.lblTituloColegio.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblTituloColegio.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblTituloColegio.Location = new System.Drawing.Point(531, 9);
            this.lblTituloColegio.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblTituloColegio.Name = "lblTituloColegio";
            this.lblTituloColegio.Size = new System.Drawing.Size(179, 25);
            this.lblTituloColegio.TabIndex = 496;
            this.lblTituloColegio.Text = "Número de colegiación:";
            // 
            // meDescripcionProductoPn
            // 
            this.meDescripcionProductoPn.Location = new System.Drawing.Point(12, 127);
            this.meDescripcionProductoPn.Margin = new System.Windows.Forms.Padding(2);
            this.meDescripcionProductoPn.Name = "meDescripcionProductoPn";
            this.meDescripcionProductoPn.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.meDescripcionProductoPn.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 7.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.meDescripcionProductoPn.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.meDescripcionProductoPn.Properties.Appearance.Options.UseBackColor = true;
            this.meDescripcionProductoPn.Properties.Appearance.Options.UseFont = true;
            this.meDescripcionProductoPn.Properties.Appearance.Options.UseForeColor = true;
            this.meDescripcionProductoPn.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.meDescripcionProductoPn.Properties.ReadOnly = true;
            this.meDescripcionProductoPn.Size = new System.Drawing.Size(241, 32);
            this.meDescripcionProductoPn.TabIndex = 487;
            // 
            // lblOficinacCreoPn
            // 
            this.lblOficinacCreoPn.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOficinacCreoPn.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblOficinacCreoPn.Appearance.Options.UseFont = true;
            this.lblOficinacCreoPn.Appearance.Options.UseForeColor = true;
            this.lblOficinacCreoPn.Appearance.Options.UseTextOptions = true;
            this.lblOficinacCreoPn.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblOficinacCreoPn.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblOficinacCreoPn.LineColor = System.Drawing.Color.DimGray;
            this.lblOficinacCreoPn.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblOficinacCreoPn.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblOficinacCreoPn.LineVisible = true;
            this.lblOficinacCreoPn.Location = new System.Drawing.Point(1179, 297);
            this.lblOficinacCreoPn.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblOficinacCreoPn.Name = "lblOficinacCreoPn";
            this.lblOficinacCreoPn.Size = new System.Drawing.Size(220, 30);
            this.lblOficinacCreoPn.TabIndex = 495;
            this.lblOficinacCreoPn.Text = "[lblOficinacCreo]";
            // 
            // labelControl31
            // 
            this.labelControl31.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl31.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl31.Appearance.Options.UseFont = true;
            this.labelControl31.Appearance.Options.UseForeColor = true;
            this.labelControl31.Appearance.Options.UseTextOptions = true;
            this.labelControl31.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl31.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl31.Location = new System.Drawing.Point(1179, 278);
            this.labelControl31.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl31.Name = "labelControl31";
            this.labelControl31.Size = new System.Drawing.Size(219, 25);
            this.labelControl31.TabIndex = 494;
            this.labelControl31.Text = "Oficina que la creo:";
            // 
            // lblUsuarioCreoPn
            // 
            this.lblUsuarioCreoPn.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsuarioCreoPn.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblUsuarioCreoPn.Appearance.Options.UseFont = true;
            this.lblUsuarioCreoPn.Appearance.Options.UseForeColor = true;
            this.lblUsuarioCreoPn.Appearance.Options.UseTextOptions = true;
            this.lblUsuarioCreoPn.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblUsuarioCreoPn.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblUsuarioCreoPn.LineColor = System.Drawing.Color.DimGray;
            this.lblUsuarioCreoPn.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblUsuarioCreoPn.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblUsuarioCreoPn.LineVisible = true;
            this.lblUsuarioCreoPn.Location = new System.Drawing.Point(972, 298);
            this.lblUsuarioCreoPn.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblUsuarioCreoPn.Name = "lblUsuarioCreoPn";
            this.lblUsuarioCreoPn.Size = new System.Drawing.Size(180, 30);
            this.lblUsuarioCreoPn.TabIndex = 493;
            this.lblUsuarioCreoPn.Text = "[lblUsuarioCreo]";
            // 
            // labelControl29
            // 
            this.labelControl29.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl29.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl29.Appearance.Options.UseFont = true;
            this.labelControl29.Appearance.Options.UseForeColor = true;
            this.labelControl29.Appearance.Options.UseTextOptions = true;
            this.labelControl29.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl29.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl29.Location = new System.Drawing.Point(972, 274);
            this.labelControl29.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl29.Name = "labelControl29";
            this.labelControl29.Size = new System.Drawing.Size(180, 25);
            this.labelControl29.TabIndex = 492;
            this.labelControl29.Text = "Usuario que la creo:";
            // 
            // lblFechaCreacionPn
            // 
            this.lblFechaCreacionPn.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFechaCreacionPn.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblFechaCreacionPn.Appearance.Options.UseFont = true;
            this.lblFechaCreacionPn.Appearance.Options.UseForeColor = true;
            this.lblFechaCreacionPn.Appearance.Options.UseTextOptions = true;
            this.lblFechaCreacionPn.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblFechaCreacionPn.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblFechaCreacionPn.LineColor = System.Drawing.Color.DimGray;
            this.lblFechaCreacionPn.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblFechaCreacionPn.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblFechaCreacionPn.LineVisible = true;
            this.lblFechaCreacionPn.Location = new System.Drawing.Point(790, 298);
            this.lblFechaCreacionPn.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblFechaCreacionPn.Name = "lblFechaCreacionPn";
            this.lblFechaCreacionPn.Size = new System.Drawing.Size(159, 30);
            this.lblFechaCreacionPn.TabIndex = 491;
            this.lblFechaCreacionPn.Text = "[lblFechaCreacion]";
            // 
            // labelControl25
            // 
            this.labelControl25.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl25.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl25.Appearance.Options.UseFont = true;
            this.labelControl25.Appearance.Options.UseForeColor = true;
            this.labelControl25.Appearance.Options.UseTextOptions = true;
            this.labelControl25.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl25.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl25.Location = new System.Drawing.Point(790, 278);
            this.labelControl25.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(159, 25);
            this.labelControl25.TabIndex = 490;
            this.labelControl25.Text = "Fecha de creación:";
            // 
            // labelControl27
            // 
            this.labelControl27.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl27.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl27.Appearance.Options.UseFont = true;
            this.labelControl27.Appearance.Options.UseForeColor = true;
            this.labelControl27.Appearance.Options.UseTextOptions = true;
            this.labelControl27.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl27.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl27.Location = new System.Drawing.Point(790, 48);
            this.labelControl27.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl27.Name = "labelControl27";
            this.labelControl27.Size = new System.Drawing.Size(159, 25);
            this.labelControl27.TabIndex = 489;
            this.labelControl27.Text = "Fotografía:";
            // 
            // picSolicitante
            // 
            this.picSolicitante.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picSolicitante.EditValue = global::Gv.ExodusBc.UI.Properties.Resources.SinFoto;
            this.picSolicitante.Location = new System.Drawing.Point(790, 76);
            this.picSolicitante.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.picSolicitante.Name = "picSolicitante";
            this.picSolicitante.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picSolicitante.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.picSolicitante.Properties.Appearance.Options.UseBackColor = true;
            this.picSolicitante.Properties.Appearance.Options.UseForeColor = true;
            this.picSolicitante.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.picSolicitante.Size = new System.Drawing.Size(159, 194);
            this.picSolicitante.TabIndex = 488;
            this.picSolicitante.ToolTip = "Visualizar imágen";
            this.picSolicitante.Click += new System.EventHandler(this.picSolicitante_Click);
            // 
            // labelControl23
            // 
            this.labelControl23.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl23.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl23.Appearance.Options.UseFont = true;
            this.labelControl23.Appearance.Options.UseForeColor = true;
            this.labelControl23.Appearance.Options.UseTextOptions = true;
            this.labelControl23.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl23.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl23.Location = new System.Drawing.Point(531, 102);
            this.labelControl23.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(241, 25);
            this.labelControl23.TabIndex = 487;
            this.labelControl23.Text = "Dirección donde vive:";
            // 
            // meDireccion
            // 
            this.meDireccion.Location = new System.Drawing.Point(531, 127);
            this.meDireccion.Margin = new System.Windows.Forms.Padding(2);
            this.meDireccion.Name = "meDireccion";
            this.meDireccion.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.meDireccion.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.meDireccion.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.meDireccion.Properties.Appearance.Options.UseBackColor = true;
            this.meDireccion.Properties.Appearance.Options.UseFont = true;
            this.meDireccion.Properties.Appearance.Options.UseForeColor = true;
            this.meDireccion.Properties.ReadOnly = true;
            this.meDireccion.Size = new System.Drawing.Size(241, 86);
            this.meDireccion.TabIndex = 486;
            // 
            // lblMunicipio
            // 
            this.lblMunicipio.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMunicipio.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblMunicipio.Appearance.Options.UseFont = true;
            this.lblMunicipio.Appearance.Options.UseForeColor = true;
            this.lblMunicipio.Appearance.Options.UseTextOptions = true;
            this.lblMunicipio.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblMunicipio.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblMunicipio.LineColor = System.Drawing.Color.DimGray;
            this.lblMunicipio.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblMunicipio.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblMunicipio.LineVisible = true;
            this.lblMunicipio.Location = new System.Drawing.Point(531, 71);
            this.lblMunicipio.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblMunicipio.Name = "lblMunicipio";
            this.lblMunicipio.Size = new System.Drawing.Size(241, 30);
            this.lblMunicipio.TabIndex = 485;
            this.lblMunicipio.Text = "[lblMunicipio]";
            // 
            // labelControl20
            // 
            this.labelControl20.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl20.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl20.Appearance.Options.UseFont = true;
            this.labelControl20.Appearance.Options.UseForeColor = true;
            this.labelControl20.Appearance.Options.UseTextOptions = true;
            this.labelControl20.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl20.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl20.Location = new System.Drawing.Point(531, 47);
            this.labelControl20.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(241, 25);
            this.labelControl20.TabIndex = 484;
            this.labelControl20.Text = "Municipio de nacimiento:";
            // 
            // lblDepartamento
            // 
            this.lblDepartamento.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDepartamento.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblDepartamento.Appearance.Options.UseFont = true;
            this.lblDepartamento.Appearance.Options.UseForeColor = true;
            this.lblDepartamento.Appearance.Options.UseTextOptions = true;
            this.lblDepartamento.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblDepartamento.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblDepartamento.LineColor = System.Drawing.Color.DimGray;
            this.lblDepartamento.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblDepartamento.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblDepartamento.LineVisible = true;
            this.lblDepartamento.Location = new System.Drawing.Point(268, 298);
            this.lblDepartamento.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblDepartamento.Name = "lblDepartamento";
            this.lblDepartamento.Size = new System.Drawing.Size(239, 30);
            this.lblDepartamento.TabIndex = 483;
            this.lblDepartamento.Text = "[lblDepartamento]";
            // 
            // lblNacionalidad
            // 
            this.lblNacionalidad.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNacionalidad.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblNacionalidad.Appearance.Options.UseFont = true;
            this.lblNacionalidad.Appearance.Options.UseForeColor = true;
            this.lblNacionalidad.Appearance.Options.UseTextOptions = true;
            this.lblNacionalidad.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblNacionalidad.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblNacionalidad.LineColor = System.Drawing.Color.DimGray;
            this.lblNacionalidad.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblNacionalidad.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblNacionalidad.LineVisible = true;
            this.lblNacionalidad.Location = new System.Drawing.Point(268, 241);
            this.lblNacionalidad.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblNacionalidad.Name = "lblNacionalidad";
            this.lblNacionalidad.Size = new System.Drawing.Size(239, 30);
            this.lblNacionalidad.TabIndex = 479;
            this.lblNacionalidad.Text = "[lblNacionalidad]";
            // 
            // lblEstadoCivil
            // 
            this.lblEstadoCivil.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstadoCivil.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblEstadoCivil.Appearance.Options.UseFont = true;
            this.lblEstadoCivil.Appearance.Options.UseForeColor = true;
            this.lblEstadoCivil.Appearance.Options.UseTextOptions = true;
            this.lblEstadoCivil.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblEstadoCivil.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblEstadoCivil.LineColor = System.Drawing.Color.DimGray;
            this.lblEstadoCivil.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblEstadoCivil.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblEstadoCivil.LineVisible = true;
            this.lblEstadoCivil.Location = new System.Drawing.Point(268, 186);
            this.lblEstadoCivil.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblEstadoCivil.Name = "lblEstadoCivil";
            this.lblEstadoCivil.Size = new System.Drawing.Size(239, 30);
            this.lblEstadoCivil.TabIndex = 477;
            this.lblEstadoCivil.Text = "[lblEstadoCivil]";
            // 
            // lblSexo
            // 
            this.lblSexo.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSexo.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblSexo.Appearance.Options.UseFont = true;
            this.lblSexo.Appearance.Options.UseForeColor = true;
            this.lblSexo.Appearance.Options.UseTextOptions = true;
            this.lblSexo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblSexo.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblSexo.LineColor = System.Drawing.Color.DimGray;
            this.lblSexo.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblSexo.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblSexo.LineVisible = true;
            this.lblSexo.Location = new System.Drawing.Point(268, 132);
            this.lblSexo.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblSexo.Name = "lblSexo";
            this.lblSexo.Size = new System.Drawing.Size(239, 30);
            this.lblSexo.TabIndex = 475;
            this.lblSexo.Text = "[lblSexo]";
            // 
            // lblFechaNacimiento
            // 
            this.lblFechaNacimiento.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFechaNacimiento.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblFechaNacimiento.Appearance.Options.UseFont = true;
            this.lblFechaNacimiento.Appearance.Options.UseForeColor = true;
            this.lblFechaNacimiento.Appearance.Options.UseTextOptions = true;
            this.lblFechaNacimiento.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblFechaNacimiento.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblFechaNacimiento.LineColor = System.Drawing.Color.DimGray;
            this.lblFechaNacimiento.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblFechaNacimiento.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblFechaNacimiento.LineVisible = true;
            this.lblFechaNacimiento.Location = new System.Drawing.Point(268, 73);
            this.lblFechaNacimiento.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblFechaNacimiento.Name = "lblFechaNacimiento";
            this.lblFechaNacimiento.Size = new System.Drawing.Size(239, 30);
            this.lblFechaNacimiento.TabIndex = 472;
            this.lblFechaNacimiento.Text = "[lblFechaNacimiento]";
            // 
            // labelControl18
            // 
            this.labelControl18.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl18.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl18.Appearance.Options.UseFont = true;
            this.labelControl18.Appearance.Options.UseForeColor = true;
            this.labelControl18.Appearance.Options.UseTextOptions = true;
            this.labelControl18.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl18.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl18.Location = new System.Drawing.Point(269, 106);
            this.labelControl18.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(237, 25);
            this.labelControl18.TabIndex = 474;
            this.labelControl18.Text = "Sexo:";
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl15.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl15.Appearance.Options.UseFont = true;
            this.labelControl15.Appearance.Options.UseForeColor = true;
            this.labelControl15.Appearance.Options.UseTextOptions = true;
            this.labelControl15.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl15.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl15.Location = new System.Drawing.Point(268, 274);
            this.labelControl15.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(239, 25);
            this.labelControl15.TabIndex = 482;
            this.labelControl15.Text = "Departamento de nacimiento:";
            // 
            // labelControl17
            // 
            this.labelControl17.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl17.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl17.Appearance.Options.UseFont = true;
            this.labelControl17.Appearance.Options.UseForeColor = true;
            this.labelControl17.Appearance.Options.UseTextOptions = true;
            this.labelControl17.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl17.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl17.Location = new System.Drawing.Point(12, 271);
            this.labelControl17.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(241, 25);
            this.labelControl17.TabIndex = 481;
            this.labelControl17.Text = "Apellidos:";
            // 
            // lblApellidos
            // 
            this.lblApellidos.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblApellidos.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblApellidos.Appearance.Options.UseFont = true;
            this.lblApellidos.Appearance.Options.UseForeColor = true;
            this.lblApellidos.Appearance.Options.UseTextOptions = true;
            this.lblApellidos.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblApellidos.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblApellidos.LineColor = System.Drawing.Color.DimGray;
            this.lblApellidos.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblApellidos.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblApellidos.LineVisible = true;
            this.lblApellidos.Location = new System.Drawing.Point(12, 297);
            this.lblApellidos.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblApellidos.Name = "lblApellidos";
            this.lblApellidos.Size = new System.Drawing.Size(241, 30);
            this.lblApellidos.TabIndex = 480;
            this.lblApellidos.Text = "[lblApellidos]";
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl11.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl11.Appearance.Options.UseFont = true;
            this.labelControl11.Appearance.Options.UseForeColor = true;
            this.labelControl11.Appearance.Options.UseTextOptions = true;
            this.labelControl11.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl11.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl11.Location = new System.Drawing.Point(268, 162);
            this.labelControl11.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(239, 25);
            this.labelControl11.TabIndex = 476;
            this.labelControl11.Text = "Estado civil:";
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl12.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl12.Appearance.Options.UseFont = true;
            this.labelControl12.Appearance.Options.UseForeColor = true;
            this.labelControl12.Appearance.Options.UseTextOptions = true;
            this.labelControl12.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl12.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl12.Location = new System.Drawing.Point(268, 219);
            this.labelControl12.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(239, 25);
            this.labelControl12.TabIndex = 478;
            this.labelControl12.Text = "Nacionalidad:";
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl7.Appearance.Options.UseFont = true;
            this.labelControl7.Appearance.Options.UseForeColor = true;
            this.labelControl7.Appearance.Options.UseTextOptions = true;
            this.labelControl7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl7.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl7.Location = new System.Drawing.Point(531, 271);
            this.labelControl7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(241, 25);
            this.labelControl7.TabIndex = 467;
            this.labelControl7.Text = "Teléfono:";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Appearance.Options.UseForeColor = true;
            this.labelControl5.Appearance.Options.UseTextOptions = true;
            this.labelControl5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl5.Location = new System.Drawing.Point(531, 214);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(241, 25);
            this.labelControl5.TabIndex = 462;
            this.labelControl5.Text = "Correo electrónico:";
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl9.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl9.Appearance.Options.UseFont = true;
            this.labelControl9.Appearance.Options.UseForeColor = true;
            this.labelControl9.Appearance.Options.UseTextOptions = true;
            this.labelControl9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl9.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl9.Location = new System.Drawing.Point(268, 47);
            this.labelControl9.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(239, 30);
            this.labelControl9.TabIndex = 471;
            this.labelControl9.Text = "Fecha de Nacimiento:";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Appearance.Options.UseForeColor = true;
            this.labelControl4.Appearance.Options.UseTextOptions = true;
            this.labelControl4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl4.Location = new System.Drawing.Point(12, 159);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(241, 25);
            this.labelControl4.TabIndex = 461;
            this.labelControl4.Text = "Número del documento:";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Appearance.Options.UseForeColor = true;
            this.labelControl1.Appearance.Options.UseTextOptions = true;
            this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.Location = new System.Drawing.Point(12, 102);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(241, 25);
            this.labelControl1.TabIndex = 459;
            this.labelControl1.Text = "Descripción del producto:";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Appearance.Options.UseForeColor = true;
            this.labelControl2.Appearance.Options.UseTextOptions = true;
            this.labelControl2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl2.Location = new System.Drawing.Point(12, 47);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(241, 25);
            this.labelControl2.TabIndex = 457;
            this.labelControl2.Text = "Código del producto:";
            // 
            // gcAdjuntosPn
            // 
            this.gcAdjuntosPn.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gcAdjuntosPn.Location = new System.Drawing.Point(972, 76);
            this.gcAdjuntosPn.MainView = this.gvAdjuntosPn;
            this.gcAdjuntosPn.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gcAdjuntosPn.Name = "gcAdjuntosPn";
            this.gcAdjuntosPn.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.btnVisualizar1});
            this.gcAdjuntosPn.Size = new System.Drawing.Size(427, 193);
            this.gcAdjuntosPn.TabIndex = 473;
            this.gcAdjuntosPn.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvAdjuntosPn});
            // 
            // gvAdjuntosPn
            // 
            this.gvAdjuntosPn.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvAdjuntosPn.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.gvAdjuntosPn.Appearance.HeaderPanel.Options.UseFont = true;
            this.gvAdjuntosPn.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvAdjuntosPn.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gvAdjuntosPn.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gvAdjuntosPn.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.gvAdjuntosPn.Appearance.OddRow.Options.UseBackColor = true;
            this.gvAdjuntosPn.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.NombrePn,
            this.colVisualizar});
            this.gvAdjuntosPn.GridControl = this.gcAdjuntosPn;
            this.gvAdjuntosPn.Name = "gvAdjuntosPn";
            this.gvAdjuntosPn.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gvAdjuntosPn.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gvAdjuntosPn.OptionsView.ShowGroupPanel = false;
            this.gvAdjuntosPn.RowHeight = 40;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "gridColumn1";
            this.gridColumn1.FieldName = "Folder";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // NombrePn
            // 
            this.NombrePn.Caption = "Tipo de documento";
            this.NombrePn.FieldName = "NameDocument";
            this.NombrePn.Name = "NombrePn";
            this.NombrePn.OptionsColumn.ReadOnly = true;
            this.NombrePn.Visible = true;
            this.NombrePn.VisibleIndex = 1;
            this.NombrePn.Width = 219;
            // 
            // colVisualizar
            // 
            this.colVisualizar.Caption = "Visualizar";
            this.colVisualizar.ColumnEdit = this.btnVisualizar1;
            this.colVisualizar.FieldName = "colVisualizar";
            this.colVisualizar.Name = "colVisualizar";
            this.colVisualizar.ToolTip = "Visualizar documento";
            this.colVisualizar.Visible = true;
            this.colVisualizar.VisibleIndex = 2;
            this.colVisualizar.Width = 60;
            // 
            // btnVisualizar1
            // 
            this.btnVisualizar1.AutoHeight = false;
            editorButtonImageOptions1.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_viewer_32x32;
            this.btnVisualizar1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Visualizar documento", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.btnVisualizar1.ContextImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_viewer_32x32;
            this.btnVisualizar1.Name = "btnVisualizar1";
            this.btnVisualizar1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btnVisualizar1.Click += new System.EventHandler(this.btnVisualizar1_Click);
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Appearance.Options.UseForeColor = true;
            this.labelControl3.Appearance.Options.UseTextOptions = true;
            this.labelControl3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl3.Location = new System.Drawing.Point(12, 214);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(241, 25);
            this.labelControl3.TabIndex = 465;
            this.labelControl3.Text = "Nombres:";
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl13.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl13.Appearance.Options.UseFont = true;
            this.labelControl13.Appearance.Options.UseForeColor = true;
            this.labelControl13.Appearance.Options.UseTextOptions = true;
            this.labelControl13.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl13.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl13.Location = new System.Drawing.Point(972, 49);
            this.labelControl13.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(231, 22);
            this.labelControl13.TabIndex = 469;
            this.labelControl13.Text = "Documentos adjuntos:";
            // 
            // lblTelefonoPn
            // 
            this.lblTelefonoPn.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTelefonoPn.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblTelefonoPn.Appearance.Options.UseFont = true;
            this.lblTelefonoPn.Appearance.Options.UseForeColor = true;
            this.lblTelefonoPn.Appearance.Options.UseTextOptions = true;
            this.lblTelefonoPn.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblTelefonoPn.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblTelefonoPn.LineColor = System.Drawing.Color.DimGray;
            this.lblTelefonoPn.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblTelefonoPn.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblTelefonoPn.LineVisible = true;
            this.lblTelefonoPn.Location = new System.Drawing.Point(531, 297);
            this.lblTelefonoPn.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblTelefonoPn.Name = "lblTelefonoPn";
            this.lblTelefonoPn.Size = new System.Drawing.Size(241, 30);
            this.lblTelefonoPn.TabIndex = 468;
            this.lblTelefonoPn.Text = "[lblTelefono]";
            // 
            // lblNombresPn
            // 
            this.lblNombresPn.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombresPn.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblNombresPn.Appearance.Options.UseFont = true;
            this.lblNombresPn.Appearance.Options.UseForeColor = true;
            this.lblNombresPn.Appearance.Options.UseTextOptions = true;
            this.lblNombresPn.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblNombresPn.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblNombresPn.LineColor = System.Drawing.Color.DimGray;
            this.lblNombresPn.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblNombresPn.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblNombresPn.LineVisible = true;
            this.lblNombresPn.Location = new System.Drawing.Point(12, 240);
            this.lblNombresPn.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblNombresPn.Name = "lblNombresPn";
            this.lblNombresPn.Size = new System.Drawing.Size(241, 30);
            this.lblNombresPn.TabIndex = 466;
            this.lblNombresPn.Text = "[lblNombres]";
            // 
            // lblCorreoPn
            // 
            this.lblCorreoPn.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCorreoPn.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblCorreoPn.Appearance.Options.UseFont = true;
            this.lblCorreoPn.Appearance.Options.UseForeColor = true;
            this.lblCorreoPn.Appearance.Options.UseTextOptions = true;
            this.lblCorreoPn.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblCorreoPn.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblCorreoPn.LineColor = System.Drawing.Color.DimGray;
            this.lblCorreoPn.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblCorreoPn.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblCorreoPn.LineVisible = true;
            this.lblCorreoPn.Location = new System.Drawing.Point(531, 240);
            this.lblCorreoPn.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblCorreoPn.Name = "lblCorreoPn";
            this.lblCorreoPn.Size = new System.Drawing.Size(241, 30);
            this.lblCorreoPn.TabIndex = 464;
            this.lblCorreoPn.Text = "[lblCorreo]";
            // 
            // lblNumeroDocumentoPn
            // 
            this.lblNumeroDocumentoPn.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumeroDocumentoPn.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblNumeroDocumentoPn.Appearance.Options.UseFont = true;
            this.lblNumeroDocumentoPn.Appearance.Options.UseForeColor = true;
            this.lblNumeroDocumentoPn.Appearance.Options.UseTextOptions = true;
            this.lblNumeroDocumentoPn.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblNumeroDocumentoPn.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblNumeroDocumentoPn.LineColor = System.Drawing.Color.DimGray;
            this.lblNumeroDocumentoPn.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblNumeroDocumentoPn.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblNumeroDocumentoPn.LineVisible = true;
            this.lblNumeroDocumentoPn.Location = new System.Drawing.Point(12, 185);
            this.lblNumeroDocumentoPn.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblNumeroDocumentoPn.Name = "lblNumeroDocumentoPn";
            this.lblNumeroDocumentoPn.Size = new System.Drawing.Size(241, 30);
            this.lblNumeroDocumentoPn.TabIndex = 463;
            this.lblNumeroDocumentoPn.Text = "[lblNumeroDocumento]";
            // 
            // lblProductoPn
            // 
            this.lblProductoPn.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProductoPn.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblProductoPn.Appearance.Options.UseFont = true;
            this.lblProductoPn.Appearance.Options.UseForeColor = true;
            this.lblProductoPn.Appearance.Options.UseTextOptions = true;
            this.lblProductoPn.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblProductoPn.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblProductoPn.LineColor = System.Drawing.Color.DimGray;
            this.lblProductoPn.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblProductoPn.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblProductoPn.LineVisible = true;
            this.lblProductoPn.Location = new System.Drawing.Point(12, 71);
            this.lblProductoPn.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblProductoPn.Name = "lblProductoPn";
            this.lblProductoPn.Size = new System.Drawing.Size(241, 30);
            this.lblProductoPn.TabIndex = 458;
            this.lblProductoPn.Text = "[lblProducto]";
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.labelControl6.Appearance.Options.UseFont = true;
            this.labelControl6.Appearance.Options.UseForeColor = true;
            this.labelControl6.Appearance.Options.UseTextOptions = true;
            this.labelControl6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl6.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl6.Location = new System.Drawing.Point(48, 6);
            this.labelControl6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(491, 22);
            this.labelControl6.TabIndex = 456;
            this.labelControl6.Text = "PERSONA NATURAL";
            // 
            // pnlPersonaJuridica
            // 
            this.pnlPersonaJuridica.Appearance.BorderColor = System.Drawing.Color.Gray;
            this.pnlPersonaJuridica.Appearance.Options.UseBorderColor = true;
            this.pnlPersonaJuridica.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.pnlPersonaJuridica.Controls.Add(this.meDescripcionProductoPj);
            this.pnlPersonaJuridica.Controls.Add(this.lblOficinacCreoPj);
            this.pnlPersonaJuridica.Controls.Add(this.labelControl30);
            this.pnlPersonaJuridica.Controls.Add(this.lblUsuarioCreoPj);
            this.pnlPersonaJuridica.Controls.Add(this.labelControl33);
            this.pnlPersonaJuridica.Controls.Add(this.lblFechaCreacionPj);
            this.pnlPersonaJuridica.Controls.Add(this.labelControl35);
            this.pnlPersonaJuridica.Controls.Add(this.gcAdjuntosPj);
            this.pnlPersonaJuridica.Controls.Add(this.labelControl10);
            this.pnlPersonaJuridica.Controls.Add(this.gcRepresentantes);
            this.pnlPersonaJuridica.Controls.Add(this.lblTelefonoPj);
            this.pnlPersonaJuridica.Controls.Add(this.labelControl26);
            this.pnlPersonaJuridica.Controls.Add(this.labelControl24);
            this.pnlPersonaJuridica.Controls.Add(this.lblCorreoPj);
            this.pnlPersonaJuridica.Controls.Add(this.labelControl21);
            this.pnlPersonaJuridica.Controls.Add(this.lblNumeroDocumentoPj);
            this.pnlPersonaJuridica.Controls.Add(this.labelControl22);
            this.pnlPersonaJuridica.Controls.Add(this.labelControl19);
            this.pnlPersonaJuridica.Controls.Add(this.labelControl16);
            this.pnlPersonaJuridica.Controls.Add(this.labelControl8);
            this.pnlPersonaJuridica.Controls.Add(this.lblNombrePj);
            this.pnlPersonaJuridica.Controls.Add(this.lblProductoPj);
            this.pnlPersonaJuridica.Controls.Add(this.labelControl14);
            this.pnlPersonaJuridica.Location = new System.Drawing.Point(10, 400);
            this.pnlPersonaJuridica.Margin = new System.Windows.Forms.Padding(2);
            this.pnlPersonaJuridica.Name = "pnlPersonaJuridica";
            this.pnlPersonaJuridica.Size = new System.Drawing.Size(1405, 235);
            this.pnlPersonaJuridica.TabIndex = 458;
            this.pnlPersonaJuridica.Visible = false;
            // 
            // meDescripcionProductoPj
            // 
            this.meDescripcionProductoPj.Location = new System.Drawing.Point(12, 123);
            this.meDescripcionProductoPj.Margin = new System.Windows.Forms.Padding(2);
            this.meDescripcionProductoPj.Name = "meDescripcionProductoPj";
            this.meDescripcionProductoPj.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.meDescripcionProductoPj.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 7.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.meDescripcionProductoPj.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.meDescripcionProductoPj.Properties.Appearance.Options.UseBackColor = true;
            this.meDescripcionProductoPj.Properties.Appearance.Options.UseFont = true;
            this.meDescripcionProductoPj.Properties.Appearance.Options.UseForeColor = true;
            this.meDescripcionProductoPj.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.meDescripcionProductoPj.Properties.ReadOnly = true;
            this.meDescripcionProductoPj.Size = new System.Drawing.Size(268, 32);
            this.meDescripcionProductoPj.TabIndex = 502;
            // 
            // lblOficinacCreoPj
            // 
            this.lblOficinacCreoPj.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOficinacCreoPj.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblOficinacCreoPj.Appearance.Options.UseFont = true;
            this.lblOficinacCreoPj.Appearance.Options.UseForeColor = true;
            this.lblOficinacCreoPj.Appearance.Options.UseTextOptions = true;
            this.lblOficinacCreoPj.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblOficinacCreoPj.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblOficinacCreoPj.LineColor = System.Drawing.Color.DimGray;
            this.lblOficinacCreoPj.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblOficinacCreoPj.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblOficinacCreoPj.LineVisible = true;
            this.lblOficinacCreoPj.Location = new System.Drawing.Point(519, 185);
            this.lblOficinacCreoPj.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblOficinacCreoPj.Name = "lblOficinacCreoPj";
            this.lblOficinacCreoPj.Size = new System.Drawing.Size(173, 30);
            this.lblOficinacCreoPj.TabIndex = 501;
            this.lblOficinacCreoPj.Text = "[lblOficinacCreo]";
            // 
            // labelControl30
            // 
            this.labelControl30.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl30.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl30.Appearance.Options.UseFont = true;
            this.labelControl30.Appearance.Options.UseForeColor = true;
            this.labelControl30.Appearance.Options.UseTextOptions = true;
            this.labelControl30.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl30.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl30.Location = new System.Drawing.Point(519, 164);
            this.labelControl30.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl30.Name = "labelControl30";
            this.labelControl30.Size = new System.Drawing.Size(170, 25);
            this.labelControl30.TabIndex = 500;
            this.labelControl30.Text = "Oficina que creo:";
            // 
            // lblUsuarioCreoPj
            // 
            this.lblUsuarioCreoPj.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsuarioCreoPj.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblUsuarioCreoPj.Appearance.Options.UseFont = true;
            this.lblUsuarioCreoPj.Appearance.Options.UseForeColor = true;
            this.lblUsuarioCreoPj.Appearance.Options.UseTextOptions = true;
            this.lblUsuarioCreoPj.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblUsuarioCreoPj.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblUsuarioCreoPj.LineColor = System.Drawing.Color.DimGray;
            this.lblUsuarioCreoPj.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblUsuarioCreoPj.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblUsuarioCreoPj.LineVisible = true;
            this.lblUsuarioCreoPj.Location = new System.Drawing.Point(519, 128);
            this.lblUsuarioCreoPj.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblUsuarioCreoPj.Name = "lblUsuarioCreoPj";
            this.lblUsuarioCreoPj.Size = new System.Drawing.Size(170, 30);
            this.lblUsuarioCreoPj.TabIndex = 499;
            this.lblUsuarioCreoPj.Text = "[lblUsuarioCreo]";
            // 
            // labelControl33
            // 
            this.labelControl33.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl33.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl33.Appearance.Options.UseFont = true;
            this.labelControl33.Appearance.Options.UseForeColor = true;
            this.labelControl33.Appearance.Options.UseTextOptions = true;
            this.labelControl33.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl33.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl33.Location = new System.Drawing.Point(519, 106);
            this.labelControl33.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl33.Name = "labelControl33";
            this.labelControl33.Size = new System.Drawing.Size(170, 25);
            this.labelControl33.TabIndex = 498;
            this.labelControl33.Text = "Usuario que creo:";
            // 
            // lblFechaCreacionPj
            // 
            this.lblFechaCreacionPj.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFechaCreacionPj.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblFechaCreacionPj.Appearance.Options.UseFont = true;
            this.lblFechaCreacionPj.Appearance.Options.UseForeColor = true;
            this.lblFechaCreacionPj.Appearance.Options.UseTextOptions = true;
            this.lblFechaCreacionPj.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblFechaCreacionPj.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblFechaCreacionPj.LineColor = System.Drawing.Color.DimGray;
            this.lblFechaCreacionPj.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblFechaCreacionPj.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblFechaCreacionPj.LineVisible = true;
            this.lblFechaCreacionPj.Location = new System.Drawing.Point(519, 64);
            this.lblFechaCreacionPj.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblFechaCreacionPj.Name = "lblFechaCreacionPj";
            this.lblFechaCreacionPj.Size = new System.Drawing.Size(170, 30);
            this.lblFechaCreacionPj.TabIndex = 497;
            this.lblFechaCreacionPj.Text = "[lblFechaCreacion]";
            // 
            // labelControl35
            // 
            this.labelControl35.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl35.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl35.Appearance.Options.UseFont = true;
            this.labelControl35.Appearance.Options.UseForeColor = true;
            this.labelControl35.Appearance.Options.UseTextOptions = true;
            this.labelControl35.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl35.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl35.Location = new System.Drawing.Point(519, 37);
            this.labelControl35.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl35.Name = "labelControl35";
            this.labelControl35.Size = new System.Drawing.Size(170, 25);
            this.labelControl35.TabIndex = 496;
            this.labelControl35.Text = "Fecha de creación:";
            // 
            // gcAdjuntosPj
            // 
            this.gcAdjuntosPj.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gcAdjuntosPj.Location = new System.Drawing.Point(702, 34);
            this.gcAdjuntosPj.MainView = this.gvAdjuntosPj;
            this.gcAdjuntosPj.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gcAdjuntosPj.Name = "gcAdjuntosPj";
            this.gcAdjuntosPj.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.btnVisualizarDocPj});
            this.gcAdjuntosPj.Size = new System.Drawing.Size(336, 180);
            this.gcAdjuntosPj.TabIndex = 490;
            this.gcAdjuntosPj.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvAdjuntosPj});
            // 
            // gvAdjuntosPj
            // 
            this.gvAdjuntosPj.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvAdjuntosPj.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.gvAdjuntosPj.Appearance.HeaderPanel.Options.UseFont = true;
            this.gvAdjuntosPj.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvAdjuntosPj.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gvAdjuntosPj.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gvAdjuntosPj.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.gvAdjuntosPj.Appearance.OddRow.Options.UseBackColor = true;
            this.gvAdjuntosPj.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn2,
            this.NameDocument,
            this.gridColumn17});
            this.gvAdjuntosPj.GridControl = this.gcAdjuntosPj;
            this.gvAdjuntosPj.Name = "gvAdjuntosPj";
            this.gvAdjuntosPj.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gvAdjuntosPj.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gvAdjuntosPj.OptionsView.ShowGroupPanel = false;
            this.gvAdjuntosPj.RowHeight = 40;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "gridColumn2";
            this.gridColumn2.FieldName = "Folder";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            // 
            // NameDocument
            // 
            this.NameDocument.Caption = "Tipo de documento";
            this.NameDocument.FieldName = "NameDocument";
            this.NameDocument.Name = "NameDocument";
            this.NameDocument.OptionsColumn.ReadOnly = true;
            this.NameDocument.Visible = true;
            this.NameDocument.VisibleIndex = 1;
            this.NameDocument.Width = 193;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Visualizar";
            this.gridColumn17.ColumnEdit = this.btnVisualizarDocPj;
            this.gridColumn17.FieldName = "colVisualizar";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.ToolTip = "Visualizar documento";
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 2;
            this.gridColumn17.Width = 60;
            // 
            // btnVisualizarDocPj
            // 
            this.btnVisualizarDocPj.AutoHeight = false;
            editorButtonImageOptions2.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_viewer_32x32;
            this.btnVisualizarDocPj.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Visualizar documento", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.btnVisualizarDocPj.ContextImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_viewer_32x32;
            this.btnVisualizarDocPj.Name = "btnVisualizarDocPj";
            this.btnVisualizarDocPj.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btnVisualizarDocPj.Click += new System.EventHandler(this.btnVisualizarDocPj_Click);
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl10.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl10.Appearance.Options.UseFont = true;
            this.labelControl10.Appearance.Options.UseForeColor = true;
            this.labelControl10.Appearance.Options.UseTextOptions = true;
            this.labelControl10.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl10.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl10.Location = new System.Drawing.Point(1045, 4);
            this.labelControl10.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(377, 25);
            this.labelControl10.TabIndex = 489;
            this.labelControl10.Text = "Representantes:";
            // 
            // gcRepresentantes
            // 
            this.gcRepresentantes.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gcRepresentantes.Location = new System.Drawing.Point(1045, 34);
            this.gcRepresentantes.MainView = this.gvRepresentantes;
            this.gcRepresentantes.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gcRepresentantes.Name = "gcRepresentantes";
            this.gcRepresentantes.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemButtonEdit4,
            this.btnVisualizarRepresentante});
            this.gcRepresentantes.Size = new System.Drawing.Size(353, 180);
            this.gcRepresentantes.TabIndex = 489;
            this.gcRepresentantes.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvRepresentantes});
            // 
            // gvRepresentantes
            // 
            this.gvRepresentantes.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvRepresentantes.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.gvRepresentantes.Appearance.HeaderPanel.Options.UseFont = true;
            this.gvRepresentantes.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvRepresentantes.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gvRepresentantes.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gvRepresentantes.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.gvRepresentantes.Appearance.OddRow.Options.UseBackColor = true;
            this.gvRepresentantes.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12});
            this.gvRepresentantes.GridControl = this.gcRepresentantes;
            this.gvRepresentantes.Name = "gvRepresentantes";
            this.gvRepresentantes.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gvRepresentantes.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gvRepresentantes.OptionsView.ShowGroupPanel = false;
            this.gvRepresentantes.RowHeight = 40;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Foto";
            this.gridColumn8.FieldName = "Foto";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 0;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Identificacion";
            this.gridColumn9.FieldName = "DocumentNumber";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 1;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Nombres";
            this.gridColumn10.FieldName = "Names";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 2;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Nacionalidad";
            this.gridColumn11.FieldName = "Nationality";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 3;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Visualizar";
            this.gridColumn12.ColumnEdit = this.btnVisualizarRepresentante;
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.ToolTip = "Visualizar documento";
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 4;
            // 
            // btnVisualizarRepresentante
            // 
            this.btnVisualizarRepresentante.AutoHeight = false;
            editorButtonImageOptions3.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_viewer_32x32;
            this.btnVisualizarRepresentante.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "Visualizar documento", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.btnVisualizarRepresentante.ContextImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_viewer_32x32;
            this.btnVisualizarRepresentante.Name = "btnVisualizarRepresentante";
            this.btnVisualizarRepresentante.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btnVisualizarRepresentante.Click += new System.EventHandler(this.btnVisualizarRepresentante_Click);
            // 
            // repositoryItemButtonEdit4
            // 
            this.repositoryItemButtonEdit4.AutoHeight = false;
            editorButtonImageOptions4.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_winclose26x26;
            this.repositoryItemButtonEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "Eliminar documento", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEdit4.ContextImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_winclose26x26;
            this.repositoryItemButtonEdit4.Name = "repositoryItemButtonEdit4";
            this.repositoryItemButtonEdit4.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // lblTelefonoPj
            // 
            this.lblTelefonoPj.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTelefonoPj.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblTelefonoPj.Appearance.Options.UseFont = true;
            this.lblTelefonoPj.Appearance.Options.UseForeColor = true;
            this.lblTelefonoPj.Appearance.Options.UseTextOptions = true;
            this.lblTelefonoPj.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblTelefonoPj.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblTelefonoPj.LineColor = System.Drawing.Color.DimGray;
            this.lblTelefonoPj.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblTelefonoPj.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblTelefonoPj.LineVisible = true;
            this.lblTelefonoPj.Location = new System.Drawing.Point(300, 185);
            this.lblTelefonoPj.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblTelefonoPj.Name = "lblTelefonoPj";
            this.lblTelefonoPj.Size = new System.Drawing.Size(203, 30);
            this.lblTelefonoPj.TabIndex = 488;
            this.lblTelefonoPj.Text = "[lblTelefonoPj]";
            // 
            // labelControl26
            // 
            this.labelControl26.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl26.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl26.Appearance.Options.UseFont = true;
            this.labelControl26.Appearance.Options.UseForeColor = true;
            this.labelControl26.Appearance.Options.UseTextOptions = true;
            this.labelControl26.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl26.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl26.Location = new System.Drawing.Point(300, 156);
            this.labelControl26.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl26.Name = "labelControl26";
            this.labelControl26.Size = new System.Drawing.Size(203, 25);
            this.labelControl26.TabIndex = 487;
            this.labelControl26.Text = "Teléfono";
            // 
            // labelControl24
            // 
            this.labelControl24.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl24.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl24.Appearance.Options.UseFont = true;
            this.labelControl24.Appearance.Options.UseForeColor = true;
            this.labelControl24.Appearance.Options.UseTextOptions = true;
            this.labelControl24.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl24.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl24.Location = new System.Drawing.Point(300, 96);
            this.labelControl24.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(203, 25);
            this.labelControl24.TabIndex = 483;
            this.labelControl24.Text = "Correo electrónico:";
            // 
            // lblCorreoPj
            // 
            this.lblCorreoPj.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCorreoPj.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblCorreoPj.Appearance.Options.UseFont = true;
            this.lblCorreoPj.Appearance.Options.UseForeColor = true;
            this.lblCorreoPj.Appearance.Options.UseTextOptions = true;
            this.lblCorreoPj.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblCorreoPj.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblCorreoPj.LineColor = System.Drawing.Color.DimGray;
            this.lblCorreoPj.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblCorreoPj.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblCorreoPj.LineVisible = true;
            this.lblCorreoPj.Location = new System.Drawing.Point(300, 124);
            this.lblCorreoPj.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblCorreoPj.Name = "lblCorreoPj";
            this.lblCorreoPj.Size = new System.Drawing.Size(204, 30);
            this.lblCorreoPj.TabIndex = 484;
            this.lblCorreoPj.Text = "[lblCorreoPj]";
            // 
            // labelControl21
            // 
            this.labelControl21.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl21.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl21.Appearance.Options.UseFont = true;
            this.labelControl21.Appearance.Options.UseForeColor = true;
            this.labelControl21.Appearance.Options.UseTextOptions = true;
            this.labelControl21.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl21.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl21.Location = new System.Drawing.Point(12, 37);
            this.labelControl21.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(268, 25);
            this.labelControl21.TabIndex = 476;
            this.labelControl21.Text = "Código del producto:";
            // 
            // lblNumeroDocumentoPj
            // 
            this.lblNumeroDocumentoPj.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumeroDocumentoPj.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblNumeroDocumentoPj.Appearance.Options.UseFont = true;
            this.lblNumeroDocumentoPj.Appearance.Options.UseForeColor = true;
            this.lblNumeroDocumentoPj.Appearance.Options.UseTextOptions = true;
            this.lblNumeroDocumentoPj.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblNumeroDocumentoPj.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblNumeroDocumentoPj.LineColor = System.Drawing.Color.DimGray;
            this.lblNumeroDocumentoPj.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblNumeroDocumentoPj.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblNumeroDocumentoPj.LineVisible = true;
            this.lblNumeroDocumentoPj.Location = new System.Drawing.Point(300, 64);
            this.lblNumeroDocumentoPj.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblNumeroDocumentoPj.Name = "lblNumeroDocumentoPj";
            this.lblNumeroDocumentoPj.Size = new System.Drawing.Size(203, 30);
            this.lblNumeroDocumentoPj.TabIndex = 475;
            this.lblNumeroDocumentoPj.Text = "[lblNumeroDocumentoPj]";
            // 
            // labelControl22
            // 
            this.labelControl22.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl22.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl22.Appearance.Options.UseFont = true;
            this.labelControl22.Appearance.Options.UseForeColor = true;
            this.labelControl22.Appearance.Options.UseTextOptions = true;
            this.labelControl22.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl22.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl22.Location = new System.Drawing.Point(300, 37);
            this.labelControl22.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(204, 25);
            this.labelControl22.TabIndex = 474;
            this.labelControl22.Text = "Número del documento:";
            // 
            // labelControl19
            // 
            this.labelControl19.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl19.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl19.Appearance.Options.UseFont = true;
            this.labelControl19.Appearance.Options.UseForeColor = true;
            this.labelControl19.Appearance.Options.UseTextOptions = true;
            this.labelControl19.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl19.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl19.Location = new System.Drawing.Point(12, 96);
            this.labelControl19.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(268, 25);
            this.labelControl19.TabIndex = 472;
            this.labelControl19.Text = "Descripción del producto:";
            // 
            // labelControl16
            // 
            this.labelControl16.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl16.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl16.Appearance.Options.UseFont = true;
            this.labelControl16.Appearance.Options.UseForeColor = true;
            this.labelControl16.Appearance.Options.UseTextOptions = true;
            this.labelControl16.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl16.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl16.Location = new System.Drawing.Point(702, 10);
            this.labelControl16.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(300, 22);
            this.labelControl16.TabIndex = 470;
            this.labelControl16.Text = "Documentos adjuntos:";
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.labelControl8.Appearance.Options.UseFont = true;
            this.labelControl8.Appearance.Options.UseForeColor = true;
            this.labelControl8.Appearance.Options.UseTextOptions = true;
            this.labelControl8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl8.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl8.Location = new System.Drawing.Point(48, 6);
            this.labelControl8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(464, 22);
            this.labelControl8.TabIndex = 457;
            this.labelControl8.Text = "PERSONA JURÍDICA";
            // 
            // lblNombrePj
            // 
            this.lblNombrePj.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombrePj.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblNombrePj.Appearance.Options.UseFont = true;
            this.lblNombrePj.Appearance.Options.UseForeColor = true;
            this.lblNombrePj.Appearance.Options.UseTextOptions = true;
            this.lblNombrePj.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblNombrePj.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblNombrePj.LineColor = System.Drawing.Color.DimGray;
            this.lblNombrePj.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblNombrePj.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblNombrePj.LineVisible = true;
            this.lblNombrePj.Location = new System.Drawing.Point(12, 185);
            this.lblNombrePj.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblNombrePj.Name = "lblNombrePj";
            this.lblNombrePj.Size = new System.Drawing.Size(268, 31);
            this.lblNombrePj.TabIndex = 452;
            this.lblNombrePj.Text = "[lblNombrePj]";
            // 
            // lblProductoPj
            // 
            this.lblProductoPj.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProductoPj.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblProductoPj.Appearance.Options.UseFont = true;
            this.lblProductoPj.Appearance.Options.UseForeColor = true;
            this.lblProductoPj.Appearance.Options.UseTextOptions = true;
            this.lblProductoPj.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblProductoPj.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblProductoPj.LineColor = System.Drawing.Color.DimGray;
            this.lblProductoPj.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblProductoPj.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblProductoPj.LineVisible = true;
            this.lblProductoPj.Location = new System.Drawing.Point(12, 64);
            this.lblProductoPj.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblProductoPj.Name = "lblProductoPj";
            this.lblProductoPj.Size = new System.Drawing.Size(268, 30);
            this.lblProductoPj.TabIndex = 451;
            this.lblProductoPj.Text = "[lblProductoPj]";
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl14.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl14.Appearance.Options.UseFont = true;
            this.labelControl14.Appearance.Options.UseForeColor = true;
            this.labelControl14.Appearance.Options.UseTextOptions = true;
            this.labelControl14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl14.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl14.Location = new System.Drawing.Point(12, 156);
            this.labelControl14.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(268, 25);
            this.labelControl14.TabIndex = 444;
            this.labelControl14.Text = "Nombre o razón social:";
            // 
            // btnAprobarSolicitud
            // 
            this.btnAprobarSolicitud.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.btnAprobarSolicitud.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAprobarSolicitud.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.btnAprobarSolicitud.Appearance.Options.UseBackColor = true;
            this.btnAprobarSolicitud.Appearance.Options.UseFont = true;
            this.btnAprobarSolicitud.Appearance.Options.UseForeColor = true;
            this.btnAprobarSolicitud.Appearance.Options.UseTextOptions = true;
            this.btnAprobarSolicitud.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnAprobarSolicitud.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnAprobarSolicitud.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_ticketnone_32x32;
            this.btnAprobarSolicitud.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnAprobarSolicitud.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnAprobarSolicitud.Location = new System.Drawing.Point(10, 666);
            this.btnAprobarSolicitud.LookAndFeel.SkinName = "Whiteprint";
            this.btnAprobarSolicitud.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnAprobarSolicitud.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnAprobarSolicitud.Name = "btnAprobarSolicitud";
            this.btnAprobarSolicitud.Size = new System.Drawing.Size(174, 35);
            this.btnAprobarSolicitud.TabIndex = 2;
            this.btnAprobarSolicitud.Text = "&Aprobar solicitud";
            this.btnAprobarSolicitud.Click += new System.EventHandler(this.btnAprobarSolicitud_Click);
            // 
            // picPaso1
            // 
            this.picPaso1.Cursor = System.Windows.Forms.Cursors.Default;
            this.picPaso1.EditValue = global::Gv.ExodusBc.UI.Properties.Resources.Circulo4;
            this.picPaso1.Location = new System.Drawing.Point(5, 4);
            this.picPaso1.Margin = new System.Windows.Forms.Padding(2);
            this.picPaso1.Name = "picPaso1";
            this.picPaso1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picPaso1.Properties.Appearance.Options.UseBackColor = true;
            this.picPaso1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.picPaso1.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picPaso1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.picPaso1.Size = new System.Drawing.Size(33, 30);
            this.picPaso1.TabIndex = 430;
            // 
            // ssForm
            // 
            this.ssForm.ClosingDelay = 500;
            // 
            // frmDetalleSolicitud
            // 
            this.Appearance.BackColor = System.Drawing.Color.White;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1458, 726);
            this.Controls.Add(this.groupControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDetalleSolicitud";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Detalle de la solicitud";
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlPersonaNatural)).EndInit();
            this.pnlPersonaNatural.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.meDescripcionProductoPn.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSolicitante.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meDireccion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcAdjuntosPn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvAdjuntosPn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnVisualizar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlPersonaJuridica)).EndInit();
            this.pnlPersonaJuridica.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.meDescripcionProductoPj.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcAdjuntosPj)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvAdjuntosPj)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnVisualizarDocPj)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcRepresentantes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvRepresentantes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnVisualizarRepresentante)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPaso1.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.SimpleButton btnAprobarSolicitud;
        private DevExpress.XtraEditors.PictureEdit picPaso1;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.PanelControl pnlPersonaNatural;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl lblTelefonoPn;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl lblNombresPn;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl lblCorreoPn;
        private DevExpress.XtraEditors.LabelControl lblNumeroDocumentoPn;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl lblProductoPn;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.PanelControl pnlPersonaJuridica;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.LabelControl lblNombrePj;
        private DevExpress.XtraEditors.LabelControl lblProductoPj;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.LabelControl lblNumeroDocumentoPj;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.LabelControl lblFechaNacimiento;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl lblEstadoCivil;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl lblSexo;
        internal DevExpress.XtraGrid.GridControl gcAdjuntosPn;
        internal DevExpress.XtraGrid.Views.Grid.GridView gvAdjuntosPn;
        private DevExpress.XtraGrid.Columns.GridColumn colVisualizar;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnVisualizar1;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.LabelControl lblApellidos;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LabelControl lblNacionalidad;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.MemoEdit meDireccion;
        private DevExpress.XtraEditors.LabelControl lblMunicipio;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.LabelControl lblDepartamento;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.LabelControl lblCorreoPj;
        private DevExpress.XtraEditors.LabelControl lblTelefonoPj;
        private DevExpress.XtraEditors.LabelControl labelControl26;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        internal DevExpress.XtraGrid.GridControl gcRepresentantes;
        internal DevExpress.XtraGrid.Views.Grid.GridView gvRepresentantes;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        internal DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        internal DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        internal DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnVisualizarRepresentante;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit4;
        private DevExpress.XtraEditors.PictureEdit picSolicitante;
        private DevExpress.XtraEditors.LabelControl labelControl27;
        private DevExpress.XtraEditors.SimpleButton btnRechazar;
        private DevExpress.XtraGrid.Columns.GridColumn NombrePn;
        private DevExpress.XtraSplashScreen.SplashScreenManager ssForm;
        internal DevExpress.XtraGrid.GridControl gcAdjuntosPj;
        internal DevExpress.XtraGrid.Views.Grid.GridView gvAdjuntosPj;
        private DevExpress.XtraGrid.Columns.GridColumn NameDocument;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnVisualizarDocPj;
        private DevExpress.XtraEditors.LabelControl lblOficinacCreoPn;
        private DevExpress.XtraEditors.LabelControl labelControl31;
        private DevExpress.XtraEditors.LabelControl lblUsuarioCreoPn;
        private DevExpress.XtraEditors.LabelControl labelControl29;
        private DevExpress.XtraEditors.LabelControl lblFechaCreacionPn;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraEditors.LabelControl lblOficinacCreoPj;
        private DevExpress.XtraEditors.LabelControl labelControl30;
        private DevExpress.XtraEditors.LabelControl lblUsuarioCreoPj;
        private DevExpress.XtraEditors.LabelControl labelControl33;
        private DevExpress.XtraEditors.LabelControl lblFechaCreacionPj;
        private DevExpress.XtraEditors.LabelControl labelControl35;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.MemoEdit meDescripcionProductoPn;
        private DevExpress.XtraEditors.MemoEdit meDescripcionProductoPj;
        private DevExpress.XtraEditors.SimpleButton btnEnviarRevision;
        private DevExpress.XtraEditors.LabelControl lblNumeroColegio;
        private DevExpress.XtraEditors.LabelControl lblTituloColegio;
    }
}