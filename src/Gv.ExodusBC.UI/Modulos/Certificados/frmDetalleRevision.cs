﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Gv.Utilidades;
using Newtonsoft.Json.Linq;
using static Gv.ExodusBc.UI.ExodusBcBase;
using static Gv.ExodusBc.EN.Request;

namespace Gv.ExodusBc.UI.Modulos.Certificados
{
    public partial class frmDetalleRevision : DevExpress.XtraEditors.XtraForm
    {

        public static int REQUESTID = 0;
        public static int OFICINAID = 0;
        public static string NOMBRES = string.Empty;
        public static string APELLIDOS = string.Empty;
        public static string TIPOPERSONA = string.Empty;
        public static string PRODUCTO = string.Empty;
        public static string TIPODOCUMENTO = string.Empty;
        public static string NUMERODOCUMENTO = string.Empty;


        public frmDetalleRevision()
        {
            InitializeComponent();
            try
            {
                ssForm.ShowWaitForm();
                Init();
                labelControl9.Select();
                ssForm.CloseWaitForm();
            }
            catch (Exception ex)
            {
                if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                XtraMessageBox.Show("Hubo un error al tratar de mostrar la información del ticket.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmDetalleRevision::InitializeComponent", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        void Init()
        {
            meTipoProducto.Text = PRODUCTO;
            lblTipoPersona.Text = TIPOPERSONA;
            lblTipoDocumento.Text = TIPODOCUMENTO;
            lblNumeroDocumento.Text = NUMERODOCUMENTO;
            lblNombres.Text = NOMBRES;
            lblApellidos.Text = APELLIDOS;

            //**Seguimiento de la solicitud**
            JArray rq = new JArray();
            rq = RequestCertificate.GetSolicitudesEnRevision(REQUESTID, OFICINAID);
            if (rq != null)
            {
                if (rq.Count > 0)
                {
                    foreach (JObject element in rq)
                    {
                        //***Creación de la solicitud***
                        if (Convert.ToInt32(element["FKTransactionEstatusId"].ToString()) == (int)TransactionStatus.SOLICITUDCREADA)
                        {
                            lblOficinaCapturo.Text = element["OFICINA"].ToString();
                            lblUsuarioCapturo.Text = element["USUARIO"].ToString();
                            lblFechaCreacion.Text = element["FECHAREGISTRO"].ToString();
                        }
                        //***Rechazo de la solicitud***
                        else if (Convert.ToInt32(element["FKTransactionEstatusId"].ToString()) == (int)TransactionStatus.SOLICITUDENREVISION)
                        {
                            lblUsuarioRevision.Text = element["USUARIO"].ToString();
                            lblFechaRevision.Text = element["FECHAREGISTRO"].ToString();
                            lblEstadoSolicitud.Text = element["ESTADO"].ToString();
                            meMotivoRevision.Text = element["OBSERVACION"].ToString();
                        }
                    }
                }
            }
        }

        private void btnEditarSolicitud_Click(object sender, EventArgs e)
        {
            frmDetalleSolicitudRevision.REQUESTID = REQUESTID;
            frmDetalleSolicitudRevision frm = new frmDetalleSolicitudRevision(null, null, null );
            frm.ShowDialog();
            Hide();
        }
    }
}