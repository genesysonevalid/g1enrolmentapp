﻿namespace Gv.ExodusBc.UI.Modulos.Certificados
{
    partial class frmCertificados
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSalir = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.lblNombreFormulario = new DevExpress.XtraEditors.LabelControl();
            this.lblTituloAccion = new DevExpress.XtraEditors.LabelControl();
            this.groupControl = new DevExpress.XtraEditors.GroupControl();
            this.lblFechaFin = new DevExpress.XtraEditors.LabelControl();
            this.pnlProfesionalTitulado = new DevExpress.XtraEditors.DateEdit();
            this.lblFechaInicio = new DevExpress.XtraEditors.LabelControl();
            this.btnConsultaFacturaManual = new DevExpress.XtraEditors.SimpleButton();
            this.dteFin = new DevExpress.XtraEditors.DateEdit();
            this.lueFacturasCreadas = new DevExpress.XtraEditors.LookUpEdit();
            this.chkFacturaManual = new DevExpress.XtraEditors.CheckEdit();
            this.btnLeerIdentidad = new DevExpress.XtraEditors.SimpleButton();
            this.btnCrearCliente = new DevExpress.XtraEditors.SimpleButton();
            this.pnlClientes = new DevExpress.XtraEditors.PanelControl();
            this.lblApellidosCliente = new DevExpress.XtraEditors.LabelControl();
            this.lblNombreCliente = new DevExpress.XtraEditors.LabelControl();
            this.lblTituloApellidoCliente = new DevExpress.XtraEditors.LabelControl();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.lueClientes = new DevExpress.XtraEditors.LookUpEdit();
            this.chkOtrosClientes = new DevExpress.XtraEditors.CheckEdit();
            this.pnlBusquedaBasica = new DevExpress.XtraEditors.PanelControl();
            this.txtNumeroDocumentoPn = new DevExpress.XtraEditors.TextEdit();
            this.lueTipoDocumentoPn = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.pnlBusquedaAvanzada = new DevExpress.XtraEditors.PanelControl();
            this.dteFechaNacimiento = new DevExpress.XtraEditors.DateEdit();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.lueSexo = new DevExpress.XtraEditors.LookUpEdit();
            this.txtPrimerApellido = new DevExpress.XtraEditors.TextEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.txtPrimerNombre = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.btnLimpiar = new DevExpress.XtraEditors.SimpleButton();
            this.btnBuscarRegistro = new DevExpress.XtraEditors.SimpleButton();
            this.picPaso1 = new DevExpress.XtraEditors.PictureEdit();
            this.chkPersonaJ = new DevExpress.XtraEditors.CheckEdit();
            this.grpDetalleSolicitante = new DevExpress.XtraEditors.GroupControl();
            this.lueColegioProfesional = new DevExpress.XtraEditors.LookUpEdit();
            this.lblcantidadproductosPN = new DevExpress.XtraEditors.LabelControl();
            this.chkProfesionalTitulo = new DevExpress.XtraEditors.CheckEdit();
            this.lblTituloPj = new DevExpress.XtraEditors.LabelControl();
            this.lblTituloPn = new DevExpress.XtraEditors.LabelControl();
            this.pnlPersonaNatural = new DevExpress.XtraEditors.PanelControl();
            this.btnAcreditarTitulo = new DevExpress.XtraEditors.SimpleButton();
            this.btnPasswordClient = new DevExpress.XtraEditors.SimpleButton();
            this.meDescripcionProductoPn = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.txtTelefonoPn = new DevExpress.XtraEditors.TextEdit();
            this.txtCorreoPn = new DevExpress.XtraEditors.TextEdit();
            this.lblFilesPn = new DevExpress.XtraEditors.LabelControl();
            this.picAddPn = new DevExpress.XtraEditors.PictureEdit();
            this.btnAdjuntarPn = new DevExpress.XtraEditors.SimpleButton();
            this.picSolicitante = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.lueProductosPn = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.lblTIpoDocumento = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.lblIdentificacion = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.lblApellidos = new DevExpress.XtraEditors.LabelControl();
            this.lblNombres = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.btnSolicitarTicket = new DevExpress.XtraEditors.SimpleButton();
            this.pnlSolicitarTicket = new DevExpress.XtraEditors.PanelControl();
            this.pnlVerificacionHuellas = new DevExpress.XtraEditors.PanelControl();
            this.lblGifValidando = new DevExpress.XtraEditors.LabelControl();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.lblEstadoValidacion = new DevExpress.XtraEditors.LabelControl();
            this.lblTituloCrearSolicitud = new DevExpress.XtraEditors.LabelControl();
            this.btnValidarPersona = new DevExpress.XtraEditors.SimpleButton();
            this.picPaso3 = new DevExpress.XtraEditors.PictureEdit();
            this.pnlPersonaJuridica = new DevExpress.XtraEditors.PanelControl();
            this.meDireccionPj = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.btnPasswordClientPj = new DevExpress.XtraEditors.SimpleButton();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.lblRepresentantes = new DevExpress.XtraEditors.LabelControl();
            this.btnAsociarRepresentante = new DevExpress.XtraEditors.SimpleButton();
            this.lblFilesPj = new DevExpress.XtraEditors.LabelControl();
            this.picAddPj = new DevExpress.XtraEditors.PictureEdit();
            this.btnAdjuntarPj = new DevExpress.XtraEditors.SimpleButton();
            this.lueProductosPj = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.txtNumeroTelefonoPj = new DevExpress.XtraEditors.TextEdit();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.txtCorreoPj = new DevExpress.XtraEditors.TextEdit();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.lueTipoDocumentoPj = new DevExpress.XtraEditors.LookUpEdit();
            this.txtNumeroDocumentoPj = new DevExpress.XtraEditors.TextEdit();
            this.txtNombresPj = new DevExpress.XtraEditors.TextEdit();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.picPaso2 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.ssForm = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::Gv.ExodusBc.UI.frmWaitForm), true, true);
            this.lblRespuestaGenerado = new DevExpress.XtraEditors.LabelControl();
            this.picTicketGenerado = new DevExpress.XtraEditors.PictureEdit();
            this.btnSyncCatalogos = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl)).BeginInit();
            this.groupControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlProfesionalTitulado.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlProfesionalTitulado.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteFin.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteFin.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueFacturasCreadas.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFacturaManual.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlClientes)).BeginInit();
            this.pnlClientes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lueClientes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkOtrosClientes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlBusquedaBasica)).BeginInit();
            this.pnlBusquedaBasica.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumeroDocumentoPn.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTipoDocumentoPn.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlBusquedaAvanzada)).BeginInit();
            this.pnlBusquedaAvanzada.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dteFechaNacimiento.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteFechaNacimiento.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueSexo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrimerApellido.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrimerNombre.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPaso1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPersonaJ.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpDetalleSolicitante)).BeginInit();
            this.grpDetalleSolicitante.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lueColegioProfesional.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkProfesionalTitulo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlPersonaNatural)).BeginInit();
            this.pnlPersonaNatural.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.meDescripcionProductoPn.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTelefonoPn.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCorreoPn.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAddPn.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSolicitante.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueProductosPn.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSolicitarTicket)).BeginInit();
            this.pnlSolicitarTicket.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlVerificacionHuellas)).BeginInit();
            this.pnlVerificacionHuellas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picPaso3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlPersonaJuridica)).BeginInit();
            this.pnlPersonaJuridica.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.meDireccionPj.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAddPj.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueProductosPj.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumeroTelefonoPj.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCorreoPj.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTipoDocumentoPj.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumeroDocumentoPj.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNombresPj.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPaso2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picTicketGenerado.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSalir
            // 
            this.btnSalir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSalir.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.btnSalir.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalir.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.btnSalir.Appearance.Options.UseBackColor = true;
            this.btnSalir.Appearance.Options.UseFont = true;
            this.btnSalir.Appearance.Options.UseForeColor = true;
            this.btnSalir.Appearance.Options.UseTextOptions = true;
            this.btnSalir.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnSalir.AppearanceHovered.BackColor = System.Drawing.Color.Crimson;
            this.btnSalir.AppearanceHovered.Options.UseBackColor = true;
            this.btnSalir.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btnSalir.Location = new System.Drawing.Point(1421, 1);
            this.btnSalir.Margin = new System.Windows.Forms.Padding(2);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(50, 27);
            this.btnSalir.TabIndex = 72;
            this.btnSalir.Text = "X";
            this.btnSalir.ToolTip = "&Salir";
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // panelControl1
            // 
            this.panelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.panelControl1.Appearance.Options.UseBackColor = true;
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.btnSalir);
            this.panelControl1.Controls.Add(this.lblNombreFormulario);
            this.panelControl1.Location = new System.Drawing.Point(-6, 0);
            this.panelControl1.LookAndFeel.SkinName = "Blue";
            this.panelControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1477, 71);
            this.panelControl1.TabIndex = 117;
            // 
            // lblNombreFormulario
            // 
            this.lblNombreFormulario.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblNombreFormulario.Appearance.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombreFormulario.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lblNombreFormulario.Appearance.Options.UseFont = true;
            this.lblNombreFormulario.Appearance.Options.UseForeColor = true;
            this.lblNombreFormulario.Appearance.Options.UseTextOptions = true;
            this.lblNombreFormulario.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblNombreFormulario.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblNombreFormulario.Location = new System.Drawing.Point(9, 12);
            this.lblNombreFormulario.Margin = new System.Windows.Forms.Padding(2);
            this.lblNombreFormulario.Name = "lblNombreFormulario";
            this.lblNombreFormulario.Size = new System.Drawing.Size(1460, 44);
            this.lblNombreFormulario.TabIndex = 72;
            this.lblNombreFormulario.Text = "CERTIFICADOS";
            // 
            // lblTituloAccion
            // 
            this.lblTituloAccion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTituloAccion.Appearance.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblTituloAccion.Appearance.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTituloAccion.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblTituloAccion.Appearance.Options.UseBackColor = true;
            this.lblTituloAccion.Appearance.Options.UseFont = true;
            this.lblTituloAccion.Appearance.Options.UseForeColor = true;
            this.lblTituloAccion.Appearance.Options.UseTextOptions = true;
            this.lblTituloAccion.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblTituloAccion.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblTituloAccion.LineColor = System.Drawing.Color.Gainsboro;
            this.lblTituloAccion.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblTituloAccion.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblTituloAccion.LineVisible = true;
            this.lblTituloAccion.Location = new System.Drawing.Point(-6, 74);
            this.lblTituloAccion.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblTituloAccion.Name = "lblTituloAccion";
            this.lblTituloAccion.Size = new System.Drawing.Size(1492, 32);
            this.lblTituloAccion.TabIndex = 64;
            this.lblTituloAccion.Text = "Emisión de certificados digitales";
            // 
            // groupControl
            // 
            this.groupControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl.AppearanceCaption.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl.AppearanceCaption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.groupControl.AppearanceCaption.Options.UseFont = true;
            this.groupControl.AppearanceCaption.Options.UseForeColor = true;
            this.groupControl.AppearanceCaption.Options.UseTextOptions = true;
            this.groupControl.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.groupControl.Controls.Add(this.lblFechaFin);
            this.groupControl.Controls.Add(this.pnlProfesionalTitulado);
            this.groupControl.Controls.Add(this.lblFechaInicio);
            this.groupControl.Controls.Add(this.btnConsultaFacturaManual);
            this.groupControl.Controls.Add(this.dteFin);
            this.groupControl.Controls.Add(this.lueFacturasCreadas);
            this.groupControl.Controls.Add(this.chkFacturaManual);
            this.groupControl.Controls.Add(this.btnLeerIdentidad);
            this.groupControl.Controls.Add(this.btnCrearCliente);
            this.groupControl.Controls.Add(this.pnlClientes);
            this.groupControl.Controls.Add(this.lueClientes);
            this.groupControl.Controls.Add(this.chkOtrosClientes);
            this.groupControl.Controls.Add(this.pnlBusquedaBasica);
            this.groupControl.Controls.Add(this.pnlBusquedaAvanzada);
            this.groupControl.Controls.Add(this.btnLimpiar);
            this.groupControl.Controls.Add(this.btnBuscarRegistro);
            this.groupControl.Controls.Add(this.picPaso1);
            this.groupControl.Controls.Add(this.chkPersonaJ);
            this.groupControl.Location = new System.Drawing.Point(8, 112);
            this.groupControl.LookAndFeel.SkinMaskColor = System.Drawing.Color.AliceBlue;
            this.groupControl.LookAndFeel.SkinName = "Whiteprint";
            this.groupControl.LookAndFeel.UseDefaultLookAndFeel = false;
            this.groupControl.Margin = new System.Windows.Forms.Padding(2);
            this.groupControl.Name = "groupControl";
            this.groupControl.Size = new System.Drawing.Size(1431, 185);
            this.groupControl.TabIndex = 311;
            this.groupControl.Text = "      Búsqueda de personas";
            // 
            // lblFechaFin
            // 
            this.lblFechaFin.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lblFechaFin.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFechaFin.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblFechaFin.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblFechaFin.Appearance.Options.UseBackColor = true;
            this.lblFechaFin.Appearance.Options.UseFont = true;
            this.lblFechaFin.Appearance.Options.UseForeColor = true;
            this.lblFechaFin.Appearance.Options.UseImageAlign = true;
            this.lblFechaFin.Appearance.Options.UseTextOptions = true;
            this.lblFechaFin.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblFechaFin.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblFechaFin.LineColor = System.Drawing.Color.Gainsboro;
            this.lblFechaFin.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblFechaFin.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblFechaFin.Location = new System.Drawing.Point(1173, -2);
            this.lblFechaFin.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblFechaFin.Name = "lblFechaFin";
            this.lblFechaFin.Size = new System.Drawing.Size(100, 13);
            this.lblFechaFin.TabIndex = 528;
            this.lblFechaFin.Text = "Fecha final";
            this.lblFechaFin.Visible = false;
            // 
            // pnlProfesionalTitulado
            // 
            this.pnlProfesionalTitulado.EditValue = null;
            this.pnlProfesionalTitulado.Location = new System.Drawing.Point(1068, 13);
            this.pnlProfesionalTitulado.Name = "pnlProfesionalTitulado";
            this.pnlProfesionalTitulado.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlProfesionalTitulado.Properties.Appearance.Options.UseFont = true;
            this.pnlProfesionalTitulado.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.pnlProfesionalTitulado.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.pnlProfesionalTitulado.Properties.NullText = "Fecha inicio";
            this.pnlProfesionalTitulado.Size = new System.Drawing.Size(99, 22);
            this.pnlProfesionalTitulado.TabIndex = 524;
            this.pnlProfesionalTitulado.Visible = false;
            // 
            // lblFechaInicio
            // 
            this.lblFechaInicio.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lblFechaInicio.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFechaInicio.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblFechaInicio.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblFechaInicio.Appearance.Options.UseBackColor = true;
            this.lblFechaInicio.Appearance.Options.UseFont = true;
            this.lblFechaInicio.Appearance.Options.UseForeColor = true;
            this.lblFechaInicio.Appearance.Options.UseImageAlign = true;
            this.lblFechaInicio.Appearance.Options.UseTextOptions = true;
            this.lblFechaInicio.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblFechaInicio.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblFechaInicio.LineColor = System.Drawing.Color.Gainsboro;
            this.lblFechaInicio.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblFechaInicio.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblFechaInicio.Location = new System.Drawing.Point(1067, -2);
            this.lblFechaInicio.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblFechaInicio.Name = "lblFechaInicio";
            this.lblFechaInicio.Size = new System.Drawing.Size(100, 13);
            this.lblFechaInicio.TabIndex = 526;
            this.lblFechaInicio.Text = "Fecha inicio";
            this.lblFechaInicio.Visible = false;
            // 
            // btnConsultaFacturaManual
            // 
            this.btnConsultaFacturaManual.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.btnConsultaFacturaManual.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConsultaFacturaManual.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.btnConsultaFacturaManual.Appearance.Options.UseBackColor = true;
            this.btnConsultaFacturaManual.Appearance.Options.UseFont = true;
            this.btnConsultaFacturaManual.Appearance.Options.UseForeColor = true;
            this.btnConsultaFacturaManual.Appearance.Options.UseTextOptions = true;
            this.btnConsultaFacturaManual.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnConsultaFacturaManual.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnConsultaFacturaManual.Location = new System.Drawing.Point(1275, 9);
            this.btnConsultaFacturaManual.LookAndFeel.SkinName = "Whiteprint";
            this.btnConsultaFacturaManual.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnConsultaFacturaManual.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnConsultaFacturaManual.Name = "btnConsultaFacturaManual";
            this.btnConsultaFacturaManual.Size = new System.Drawing.Size(147, 26);
            this.btnConsultaFacturaManual.TabIndex = 525;
            this.btnConsultaFacturaManual.Text = "&Consultar facturas";
            this.btnConsultaFacturaManual.Visible = false;
            this.btnConsultaFacturaManual.Click += new System.EventHandler(this.btnConsultaFacturaManual_Click);
            // 
            // dteFin
            // 
            this.dteFin.EditValue = null;
            this.dteFin.Location = new System.Drawing.Point(1173, 13);
            this.dteFin.Name = "dteFin";
            this.dteFin.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dteFin.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dteFin.Properties.NullText = "Fecha fin";
            this.dteFin.Size = new System.Drawing.Size(96, 22);
            this.dteFin.TabIndex = 523;
            this.dteFin.Visible = false;
            // 
            // lueFacturasCreadas
            // 
            this.lueFacturasCreadas.Location = new System.Drawing.Point(1209, 44);
            this.lueFacturasCreadas.Margin = new System.Windows.Forms.Padding(2);
            this.lueFacturasCreadas.Name = "lueFacturasCreadas";
            this.lueFacturasCreadas.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueFacturasCreadas.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lueFacturasCreadas.Properties.Appearance.Options.UseFont = true;
            this.lueFacturasCreadas.Properties.Appearance.Options.UseForeColor = true;
            this.lueFacturasCreadas.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueFacturasCreadas.Properties.AppearanceDropDown.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lueFacturasCreadas.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lueFacturasCreadas.Properties.AppearanceDropDown.Options.UseForeColor = true;
            this.lueFacturasCreadas.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lueFacturasCreadas.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lueFacturasCreadas.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.lueFacturasCreadas.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueFacturasCreadas.Properties.NullText = "";
            this.lueFacturasCreadas.Properties.PopupFilterMode = DevExpress.XtraEditors.PopupFilterMode.Contains;
            this.lueFacturasCreadas.Size = new System.Drawing.Size(213, 26);
            this.lueFacturasCreadas.TabIndex = 522;
            this.lueFacturasCreadas.Visible = false;
            this.lueFacturasCreadas.CustomDrawCell += new DevExpress.XtraEditors.Popup.LookUpCustomDrawCellEventHandler(this.lueFacturasCreadas_CustomDrawCell);
            // 
            // chkFacturaManual
            // 
            this.chkFacturaManual.Location = new System.Drawing.Point(913, 6);
            this.chkFacturaManual.Margin = new System.Windows.Forms.Padding(2);
            this.chkFacturaManual.Name = "chkFacturaManual";
            this.chkFacturaManual.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkFacturaManual.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.chkFacturaManual.Properties.Appearance.Options.UseFont = true;
            this.chkFacturaManual.Properties.Appearance.Options.UseForeColor = true;
            this.chkFacturaManual.Properties.Caption = "Factura manual";
            this.chkFacturaManual.Properties.LookAndFeel.SkinName = "Sharp Plus";
            this.chkFacturaManual.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.chkFacturaManual.Size = new System.Drawing.Size(150, 24);
            this.chkFacturaManual.TabIndex = 522;
            this.chkFacturaManual.CheckedChanged += new System.EventHandler(this.chkFacturaManual_CheckedChanged);
            // 
            // btnLeerIdentidad
            // 
            this.btnLeerIdentidad.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.btnLeerIdentidad.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLeerIdentidad.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.btnLeerIdentidad.Appearance.Options.UseBackColor = true;
            this.btnLeerIdentidad.Appearance.Options.UseFont = true;
            this.btnLeerIdentidad.Appearance.Options.UseForeColor = true;
            this.btnLeerIdentidad.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnLeerIdentidad.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.id_card;
            this.btnLeerIdentidad.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnLeerIdentidad.Location = new System.Drawing.Point(616, 64);
            this.btnLeerIdentidad.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnLeerIdentidad.Name = "btnLeerIdentidad";
            this.btnLeerIdentidad.Size = new System.Drawing.Size(152, 35);
            this.btnLeerIdentidad.TabIndex = 451;
            this.btnLeerIdentidad.Text = "&Leer Identidad";
            this.btnLeerIdentidad.Click += new System.EventHandler(this.btnLeerIdentidad_Click);
            // 
            // btnCrearCliente
            // 
            this.btnCrearCliente.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.btnCrearCliente.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCrearCliente.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.btnCrearCliente.Appearance.Options.UseBackColor = true;
            this.btnCrearCliente.Appearance.Options.UseFont = true;
            this.btnCrearCliente.Appearance.Options.UseForeColor = true;
            this.btnCrearCliente.Appearance.Options.UseTextOptions = true;
            this.btnCrearCliente.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnCrearCliente.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnCrearCliente.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_customer;
            this.btnCrearCliente.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnCrearCliente.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnCrearCliente.Location = new System.Drawing.Point(1260, 101);
            this.btnCrearCliente.LookAndFeel.SkinName = "Whiteprint";
            this.btnCrearCliente.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnCrearCliente.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnCrearCliente.Name = "btnCrearCliente";
            this.btnCrearCliente.Size = new System.Drawing.Size(161, 30);
            this.btnCrearCliente.TabIndex = 521;
            this.btnCrearCliente.Text = "&Crear clientes";
            this.btnCrearCliente.Click += new System.EventHandler(this.btnCrearCliente_Click);
            // 
            // pnlClientes
            // 
            this.pnlClientes.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlClientes.Controls.Add(this.lblApellidosCliente);
            this.pnlClientes.Controls.Add(this.lblNombreCliente);
            this.pnlClientes.Controls.Add(this.lblTituloApellidoCliente);
            this.pnlClientes.Controls.Add(this.labelControl23);
            this.pnlClientes.Location = new System.Drawing.Point(913, 134);
            this.pnlClientes.Margin = new System.Windows.Forms.Padding(2);
            this.pnlClientes.Name = "pnlClientes";
            this.pnlClientes.Size = new System.Drawing.Size(507, 49);
            this.pnlClientes.TabIndex = 468;
            // 
            // lblApellidosCliente
            // 
            this.lblApellidosCliente.Appearance.Font = new System.Drawing.Font("Segoe UI", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblApellidosCliente.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblApellidosCliente.Appearance.Options.UseFont = true;
            this.lblApellidosCliente.Appearance.Options.UseForeColor = true;
            this.lblApellidosCliente.Appearance.Options.UseTextOptions = true;
            this.lblApellidosCliente.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblApellidosCliente.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblApellidosCliente.LineColor = System.Drawing.Color.DimGray;
            this.lblApellidosCliente.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblApellidosCliente.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblApellidosCliente.LineVisible = true;
            this.lblApellidosCliente.Location = new System.Drawing.Point(268, 18);
            this.lblApellidosCliente.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblApellidosCliente.Name = "lblApellidosCliente";
            this.lblApellidosCliente.Size = new System.Drawing.Size(231, 26);
            this.lblApellidosCliente.TabIndex = 457;
            this.lblApellidosCliente.Text = "[lblApellidosCliente]";
            // 
            // lblNombreCliente
            // 
            this.lblNombreCliente.Appearance.Font = new System.Drawing.Font("Segoe UI", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombreCliente.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblNombreCliente.Appearance.Options.UseFont = true;
            this.lblNombreCliente.Appearance.Options.UseForeColor = true;
            this.lblNombreCliente.Appearance.Options.UseTextOptions = true;
            this.lblNombreCliente.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblNombreCliente.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblNombreCliente.LineColor = System.Drawing.Color.DimGray;
            this.lblNombreCliente.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblNombreCliente.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblNombreCliente.LineVisible = true;
            this.lblNombreCliente.Location = new System.Drawing.Point(8, 18);
            this.lblNombreCliente.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblNombreCliente.Name = "lblNombreCliente";
            this.lblNombreCliente.Size = new System.Drawing.Size(254, 26);
            this.lblNombreCliente.TabIndex = 456;
            this.lblNombreCliente.Text = "[lblNombreCliente]";
            // 
            // lblTituloApellidoCliente
            // 
            this.lblTituloApellidoCliente.Appearance.Font = new System.Drawing.Font("Segoe UI", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTituloApellidoCliente.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblTituloApellidoCliente.Appearance.Options.UseFont = true;
            this.lblTituloApellidoCliente.Appearance.Options.UseForeColor = true;
            this.lblTituloApellidoCliente.Location = new System.Drawing.Point(268, 2);
            this.lblTituloApellidoCliente.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblTituloApellidoCliente.Name = "lblTituloApellidoCliente";
            this.lblTituloApellidoCliente.Size = new System.Drawing.Size(54, 17);
            this.lblTituloApellidoCliente.TabIndex = 440;
            this.lblTituloApellidoCliente.Text = "Apellidos";
            // 
            // labelControl23
            // 
            this.labelControl23.Appearance.Font = new System.Drawing.Font("Segoe UI", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl23.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl23.Appearance.Options.UseFont = true;
            this.labelControl23.Appearance.Options.UseForeColor = true;
            this.labelControl23.Location = new System.Drawing.Point(7, 2);
            this.labelControl23.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(138, 17);
            this.labelControl23.TabIndex = 439;
            this.labelControl23.Text = "Nombre o razón social:";
            // 
            // lueClientes
            // 
            this.lueClientes.Location = new System.Drawing.Point(913, 101);
            this.lueClientes.Margin = new System.Windows.Forms.Padding(2);
            this.lueClientes.Name = "lueClientes";
            this.lueClientes.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueClientes.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lueClientes.Properties.Appearance.Options.UseFont = true;
            this.lueClientes.Properties.Appearance.Options.UseForeColor = true;
            this.lueClientes.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueClientes.Properties.AppearanceDropDown.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lueClientes.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lueClientes.Properties.AppearanceDropDown.Options.UseForeColor = true;
            this.lueClientes.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lueClientes.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lueClientes.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.lueClientes.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueClientes.Properties.NullText = "";
            this.lueClientes.Size = new System.Drawing.Size(341, 26);
            this.lueClientes.TabIndex = 450;
            this.lueClientes.EditValueChanged += new System.EventHandler(this.lueClientes_EditValueChanged);
            // 
            // chkOtrosClientes
            // 
            this.chkOtrosClientes.Location = new System.Drawing.Point(913, 64);
            this.chkOtrosClientes.Margin = new System.Windows.Forms.Padding(2);
            this.chkOtrosClientes.Name = "chkOtrosClientes";
            this.chkOtrosClientes.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkOtrosClientes.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.chkOtrosClientes.Properties.Appearance.Options.UseFont = true;
            this.chkOtrosClientes.Properties.Appearance.Options.UseForeColor = true;
            this.chkOtrosClientes.Properties.Caption = "Factura a nombre de otros clientes";
            this.chkOtrosClientes.Properties.LookAndFeel.SkinName = "Sharp Plus";
            this.chkOtrosClientes.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.chkOtrosClientes.Size = new System.Drawing.Size(299, 24);
            this.chkOtrosClientes.TabIndex = 446;
            this.chkOtrosClientes.CheckedChanged += new System.EventHandler(this.chkOtrosClientes_CheckedChanged);
            // 
            // pnlBusquedaBasica
            // 
            this.pnlBusquedaBasica.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlBusquedaBasica.Controls.Add(this.txtNumeroDocumentoPn);
            this.pnlBusquedaBasica.Controls.Add(this.lueTipoDocumentoPn);
            this.pnlBusquedaBasica.Controls.Add(this.labelControl1);
            this.pnlBusquedaBasica.Controls.Add(this.labelControl4);
            this.pnlBusquedaBasica.Location = new System.Drawing.Point(12, 42);
            this.pnlBusquedaBasica.Margin = new System.Windows.Forms.Padding(2);
            this.pnlBusquedaBasica.Name = "pnlBusquedaBasica";
            this.pnlBusquedaBasica.Size = new System.Drawing.Size(468, 66);
            this.pnlBusquedaBasica.TabIndex = 445;
            // 
            // txtNumeroDocumentoPn
            // 
            this.txtNumeroDocumentoPn.Location = new System.Drawing.Point(222, 30);
            this.txtNumeroDocumentoPn.Margin = new System.Windows.Forms.Padding(2);
            this.txtNumeroDocumentoPn.Name = "txtNumeroDocumentoPn";
            this.txtNumeroDocumentoPn.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNumeroDocumentoPn.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.txtNumeroDocumentoPn.Properties.Appearance.Options.UseFont = true;
            this.txtNumeroDocumentoPn.Properties.Appearance.Options.UseForeColor = true;
            this.txtNumeroDocumentoPn.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtNumeroDocumentoPn.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtNumeroDocumentoPn.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtNumeroDocumentoPn.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNumeroDocumentoPn.Properties.Mask.EditMask = "0000-0000-00000";
            this.txtNumeroDocumentoPn.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
            this.txtNumeroDocumentoPn.Properties.Mask.SaveLiteral = false;
            this.txtNumeroDocumentoPn.Properties.MaxLength = 20;
            this.txtNumeroDocumentoPn.Size = new System.Drawing.Size(240, 26);
            this.txtNumeroDocumentoPn.TabIndex = 1;
            this.txtNumeroDocumentoPn.ToolTip = "Número de identificación";
            // 
            // lueTipoDocumentoPn
            // 
            this.lueTipoDocumentoPn.Location = new System.Drawing.Point(3, 30);
            this.lueTipoDocumentoPn.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lueTipoDocumentoPn.Name = "lueTipoDocumentoPn";
            this.lueTipoDocumentoPn.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.lueTipoDocumentoPn.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueTipoDocumentoPn.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lueTipoDocumentoPn.Properties.Appearance.Options.UseBackColor = true;
            this.lueTipoDocumentoPn.Properties.Appearance.Options.UseFont = true;
            this.lueTipoDocumentoPn.Properties.Appearance.Options.UseForeColor = true;
            this.lueTipoDocumentoPn.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueTipoDocumentoPn.Properties.AppearanceDropDown.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lueTipoDocumentoPn.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lueTipoDocumentoPn.Properties.AppearanceDropDown.Options.UseForeColor = true;
            this.lueTipoDocumentoPn.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lueTipoDocumentoPn.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lueTipoDocumentoPn.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.lueTipoDocumentoPn.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueTipoDocumentoPn.Properties.NullText = "";
            this.lueTipoDocumentoPn.Size = new System.Drawing.Size(211, 26);
            this.lueTipoDocumentoPn.TabIndex = 0;
            this.lueTipoDocumentoPn.ToolTip = "Tipo de documento";
            this.lueTipoDocumentoPn.EditValueChanged += new System.EventHandler(this.lueTipoDocumentoPn_EditValueChanged);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Appearance.Options.UseForeColor = true;
            this.labelControl1.Location = new System.Drawing.Point(222, 5);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(172, 20);
            this.labelControl1.TabIndex = 440;
            this.labelControl1.Text = "Número de identificación:";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Appearance.Options.UseForeColor = true;
            this.labelControl4.Location = new System.Drawing.Point(3, 5);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(134, 20);
            this.labelControl4.TabIndex = 439;
            this.labelControl4.Text = "Tipo de documento:";
            // 
            // pnlBusquedaAvanzada
            // 
            this.pnlBusquedaAvanzada.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlBusquedaAvanzada.Controls.Add(this.dteFechaNacimiento);
            this.pnlBusquedaAvanzada.Controls.Add(this.labelControl16);
            this.pnlBusquedaAvanzada.Controls.Add(this.labelControl12);
            this.pnlBusquedaAvanzada.Controls.Add(this.lueSexo);
            this.pnlBusquedaAvanzada.Controls.Add(this.txtPrimerApellido);
            this.pnlBusquedaAvanzada.Controls.Add(this.labelControl11);
            this.pnlBusquedaAvanzada.Controls.Add(this.txtPrimerNombre);
            this.pnlBusquedaAvanzada.Controls.Add(this.labelControl10);
            this.pnlBusquedaAvanzada.Location = new System.Drawing.Point(12, 113);
            this.pnlBusquedaAvanzada.Margin = new System.Windows.Forms.Padding(2);
            this.pnlBusquedaAvanzada.Name = "pnlBusquedaAvanzada";
            this.pnlBusquedaAvanzada.Size = new System.Drawing.Size(889, 64);
            this.pnlBusquedaAvanzada.TabIndex = 444;
            this.pnlBusquedaAvanzada.Visible = false;
            // 
            // dteFechaNacimiento
            // 
            this.dteFechaNacimiento.EditValue = null;
            this.dteFechaNacimiento.Location = new System.Drawing.Point(526, 28);
            this.dteFechaNacimiento.Margin = new System.Windows.Forms.Padding(2);
            this.dteFechaNacimiento.Name = "dteFechaNacimiento";
            this.dteFechaNacimiento.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dteFechaNacimiento.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.dteFechaNacimiento.Properties.Appearance.Options.UseFont = true;
            this.dteFechaNacimiento.Properties.Appearance.Options.UseForeColor = true;
            this.dteFechaNacimiento.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dteFechaNacimiento.Properties.AppearanceDropDown.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.dteFechaNacimiento.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dteFechaNacimiento.Properties.AppearanceDropDown.Options.UseForeColor = true;
            this.dteFechaNacimiento.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dteFechaNacimiento.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dteFechaNacimiento.Properties.AppearanceFocused.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dteFechaNacimiento.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.dteFechaNacimiento.Properties.AppearanceFocused.Options.UseFont = true;
            this.dteFechaNacimiento.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dteFechaNacimiento.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dteFechaNacimiento.Size = new System.Drawing.Size(175, 26);
            this.dteFechaNacimiento.TabIndex = 6;
            // 
            // labelControl16
            // 
            this.labelControl16.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl16.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl16.Appearance.Options.UseFont = true;
            this.labelControl16.Appearance.Options.UseForeColor = true;
            this.labelControl16.Location = new System.Drawing.Point(707, 8);
            this.labelControl16.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(35, 20);
            this.labelControl16.TabIndex = 448;
            this.labelControl16.Text = "Sexo:";
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl12.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl12.Appearance.Options.UseFont = true;
            this.labelControl12.Appearance.Options.UseForeColor = true;
            this.labelControl12.Location = new System.Drawing.Point(530, 9);
            this.labelControl12.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(140, 20);
            this.labelControl12.TabIndex = 449;
            this.labelControl12.Text = "Fecha de nacimiento:";
            // 
            // lueSexo
            // 
            this.lueSexo.Location = new System.Drawing.Point(703, 28);
            this.lueSexo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lueSexo.Name = "lueSexo";
            this.lueSexo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.lueSexo.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueSexo.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lueSexo.Properties.Appearance.Options.UseBackColor = true;
            this.lueSexo.Properties.Appearance.Options.UseFont = true;
            this.lueSexo.Properties.Appearance.Options.UseForeColor = true;
            this.lueSexo.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueSexo.Properties.AppearanceDropDown.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lueSexo.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lueSexo.Properties.AppearanceDropDown.Options.UseForeColor = true;
            this.lueSexo.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lueSexo.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lueSexo.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.lueSexo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueSexo.Properties.NullText = "";
            this.lueSexo.Size = new System.Drawing.Size(156, 26);
            this.lueSexo.TabIndex = 7;
            // 
            // txtPrimerApellido
            // 
            this.txtPrimerApellido.Location = new System.Drawing.Point(262, 28);
            this.txtPrimerApellido.Margin = new System.Windows.Forms.Padding(2);
            this.txtPrimerApellido.Name = "txtPrimerApellido";
            this.txtPrimerApellido.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrimerApellido.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.txtPrimerApellido.Properties.Appearance.Options.UseFont = true;
            this.txtPrimerApellido.Properties.Appearance.Options.UseForeColor = true;
            this.txtPrimerApellido.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtPrimerApellido.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtPrimerApellido.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtPrimerApellido.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPrimerApellido.Size = new System.Drawing.Size(255, 26);
            this.txtPrimerApellido.TabIndex = 5;
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl11.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl11.Appearance.Options.UseFont = true;
            this.labelControl11.Appearance.Options.UseForeColor = true;
            this.labelControl11.Location = new System.Drawing.Point(268, 4);
            this.labelControl11.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(105, 20);
            this.labelControl11.TabIndex = 447;
            this.labelControl11.Text = "Primer apellido:";
            // 
            // txtPrimerNombre
            // 
            this.txtPrimerNombre.Location = new System.Drawing.Point(5, 28);
            this.txtPrimerNombre.Margin = new System.Windows.Forms.Padding(2);
            this.txtPrimerNombre.Name = "txtPrimerNombre";
            this.txtPrimerNombre.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrimerNombre.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.txtPrimerNombre.Properties.Appearance.Options.UseFont = true;
            this.txtPrimerNombre.Properties.Appearance.Options.UseForeColor = true;
            this.txtPrimerNombre.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtPrimerNombre.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtPrimerNombre.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtPrimerNombre.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPrimerNombre.Size = new System.Drawing.Size(250, 26);
            this.txtPrimerNombre.TabIndex = 4;
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl10.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl10.Appearance.Options.UseFont = true;
            this.labelControl10.Appearance.Options.UseForeColor = true;
            this.labelControl10.Location = new System.Drawing.Point(6, 5);
            this.labelControl10.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(102, 20);
            this.labelControl10.TabIndex = 446;
            this.labelControl10.Text = "Primer nombre:";
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLimpiar.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.btnLimpiar.Appearance.Options.UseFont = true;
            this.btnLimpiar.Appearance.Options.UseForeColor = true;
            this.btnLimpiar.Appearance.Options.UseTextOptions = true;
            this.btnLimpiar.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnLimpiar.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnLimpiar.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_eraser_32x32;
            this.btnLimpiar.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnLimpiar.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLimpiar.Location = new System.Drawing.Point(777, 64);
            this.btnLimpiar.LookAndFeel.SkinName = "Whiteprint";
            this.btnLimpiar.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnLimpiar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(122, 35);
            this.btnLimpiar.TabIndex = 8;
            this.btnLimpiar.Text = "&Limpiar";
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // btnBuscarRegistro
            // 
            this.btnBuscarRegistro.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.btnBuscarRegistro.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuscarRegistro.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.btnBuscarRegistro.Appearance.Options.UseBackColor = true;
            this.btnBuscarRegistro.Appearance.Options.UseFont = true;
            this.btnBuscarRegistro.Appearance.Options.UseForeColor = true;
            this.btnBuscarRegistro.Appearance.Options.UseTextOptions = true;
            this.btnBuscarRegistro.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnBuscarRegistro.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnBuscarRegistro.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_search_32x32;
            this.btnBuscarRegistro.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnBuscarRegistro.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnBuscarRegistro.Location = new System.Drawing.Point(485, 64);
            this.btnBuscarRegistro.LookAndFeel.SkinName = "Whiteprint";
            this.btnBuscarRegistro.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnBuscarRegistro.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnBuscarRegistro.Name = "btnBuscarRegistro";
            this.btnBuscarRegistro.Size = new System.Drawing.Size(122, 35);
            this.btnBuscarRegistro.TabIndex = 2;
            this.btnBuscarRegistro.Text = "&Buscar ";
            this.btnBuscarRegistro.Click += new System.EventHandler(this.btnBuscarRegistro_Click);
            // 
            // picPaso1
            // 
            this.picPaso1.Cursor = System.Windows.Forms.Cursors.Default;
            this.picPaso1.EditValue = global::Gv.ExodusBc.UI.Properties.Resources.Circulo;
            this.picPaso1.Location = new System.Drawing.Point(3, -1);
            this.picPaso1.Margin = new System.Windows.Forms.Padding(2);
            this.picPaso1.Name = "picPaso1";
            this.picPaso1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picPaso1.Properties.Appearance.Options.UseBackColor = true;
            this.picPaso1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.picPaso1.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picPaso1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.picPaso1.Size = new System.Drawing.Size(28, 28);
            this.picPaso1.TabIndex = 430;
            // 
            // chkPersonaJ
            // 
            this.chkPersonaJ.Location = new System.Drawing.Point(913, 37);
            this.chkPersonaJ.Margin = new System.Windows.Forms.Padding(2);
            this.chkPersonaJ.Name = "chkPersonaJ";
            this.chkPersonaJ.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkPersonaJ.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.chkPersonaJ.Properties.Appearance.Options.UseFont = true;
            this.chkPersonaJ.Properties.Appearance.Options.UseForeColor = true;
            this.chkPersonaJ.Properties.Caption = "Trámite para persona jurídica";
            this.chkPersonaJ.Properties.LookAndFeel.SkinName = "Sharp Plus";
            this.chkPersonaJ.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.chkPersonaJ.Size = new System.Drawing.Size(262, 24);
            this.chkPersonaJ.TabIndex = 438;
            this.chkPersonaJ.CheckedChanged += new System.EventHandler(this.chkPersonaJ_CheckedChanged);
            // 
            // grpDetalleSolicitante
            // 
            this.grpDetalleSolicitante.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpDetalleSolicitante.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.grpDetalleSolicitante.Appearance.Options.UseBorderColor = true;
            this.grpDetalleSolicitante.AppearanceCaption.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpDetalleSolicitante.AppearanceCaption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.grpDetalleSolicitante.AppearanceCaption.Options.UseFont = true;
            this.grpDetalleSolicitante.AppearanceCaption.Options.UseForeColor = true;
            this.grpDetalleSolicitante.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.grpDetalleSolicitante.Controls.Add(this.lueColegioProfesional);
            this.grpDetalleSolicitante.Controls.Add(this.lblcantidadproductosPN);
            this.grpDetalleSolicitante.Controls.Add(this.chkProfesionalTitulo);
            this.grpDetalleSolicitante.Controls.Add(this.lblTituloPj);
            this.grpDetalleSolicitante.Controls.Add(this.lblTituloPn);
            this.grpDetalleSolicitante.Controls.Add(this.pnlPersonaNatural);
            this.grpDetalleSolicitante.Controls.Add(this.btnSolicitarTicket);
            this.grpDetalleSolicitante.Controls.Add(this.pnlSolicitarTicket);
            this.grpDetalleSolicitante.Controls.Add(this.pnlPersonaJuridica);
            this.grpDetalleSolicitante.Controls.Add(this.picPaso2);
            this.grpDetalleSolicitante.Location = new System.Drawing.Point(8, 300);
            this.grpDetalleSolicitante.LookAndFeel.SkinName = "Whiteprint";
            this.grpDetalleSolicitante.LookAndFeel.UseDefaultLookAndFeel = false;
            this.grpDetalleSolicitante.Margin = new System.Windows.Forms.Padding(2);
            this.grpDetalleSolicitante.Name = "grpDetalleSolicitante";
            this.grpDetalleSolicitante.Size = new System.Drawing.Size(1431, 406);
            this.grpDetalleSolicitante.TabIndex = 312;
            this.grpDetalleSolicitante.Text = "      Detalle del solicitante";
            this.grpDetalleSolicitante.Visible = false;
            // 
            // lueColegioProfesional
            // 
            this.lueColegioProfesional.Location = new System.Drawing.Point(556, 41);
            this.lueColegioProfesional.Margin = new System.Windows.Forms.Padding(4);
            this.lueColegioProfesional.Name = "lueColegioProfesional";
            this.lueColegioProfesional.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueColegioProfesional.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lueColegioProfesional.Properties.Appearance.Options.UseFont = true;
            this.lueColegioProfesional.Properties.Appearance.Options.UseForeColor = true;
            this.lueColegioProfesional.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lueColegioProfesional.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lueColegioProfesional.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.lueColegioProfesional.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueColegioProfesional.Properties.NullText = "Seleccione colegio profesional";
            this.lueColegioProfesional.Size = new System.Drawing.Size(249, 26);
            this.lueColegioProfesional.TabIndex = 451;
            this.lueColegioProfesional.Visible = false;
            this.lueColegioProfesional.CustomDrawCell += new DevExpress.XtraEditors.Popup.LookUpCustomDrawCellEventHandler(this.lueColegioProfesional_CustomDrawCell);
            this.lueColegioProfesional.EditValueChanged += new System.EventHandler(this.lueColegioProfesional_EditValueChanged);
            this.lueColegioProfesional.CustomDisplayText += new DevExpress.XtraEditors.Controls.CustomDisplayTextEventHandler(this.lueColegioProfesional_CustomDisplayText);
            // 
            // lblcantidadproductosPN
            // 
            this.lblcantidadproductosPN.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcantidadproductosPN.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lblcantidadproductosPN.Appearance.Options.UseFont = true;
            this.lblcantidadproductosPN.Appearance.Options.UseForeColor = true;
            this.lblcantidadproductosPN.Appearance.Options.UseTextOptions = true;
            this.lblcantidadproductosPN.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblcantidadproductosPN.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblcantidadproductosPN.LineColor = System.Drawing.Color.Transparent;
            this.lblcantidadproductosPN.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblcantidadproductosPN.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblcantidadproductosPN.LineVisible = true;
            this.lblcantidadproductosPN.Location = new System.Drawing.Point(287, 372);
            this.lblcantidadproductosPN.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblcantidadproductosPN.Name = "lblcantidadproductosPN";
            this.lblcantidadproductosPN.Size = new System.Drawing.Size(1132, 26);
            this.lblcantidadproductosPN.TabIndex = 521;
            this.lblcantidadproductosPN.Text = "[lblcantidadproductosPN]";
            // 
            // chkProfesionalTitulo
            // 
            this.chkProfesionalTitulo.Location = new System.Drawing.Point(379, 43);
            this.chkProfesionalTitulo.Name = "chkProfesionalTitulo";
            this.chkProfesionalTitulo.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semilight", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkProfesionalTitulo.Properties.Appearance.Options.UseFont = true;
            this.chkProfesionalTitulo.Properties.Appearance.Options.UseTextOptions = true;
            this.chkProfesionalTitulo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.chkProfesionalTitulo.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.chkProfesionalTitulo.Properties.AutoHeight = false;
            this.chkProfesionalTitulo.Properties.Caption = "Profesional Titulado";
            this.chkProfesionalTitulo.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.SvgToggle1;
            this.chkProfesionalTitulo.Properties.CheckBoxOptions.SvgColorChecked = DevExpress.LookAndFeel.DXSkinColors.ForeColors.Information;
            this.chkProfesionalTitulo.Properties.CheckBoxOptions.SvgColorUnchecked = DevExpress.LookAndFeel.DXSkinColors.ForeColors.DisabledText;
            this.chkProfesionalTitulo.Properties.CheckBoxOptions.SvgImageSize = new System.Drawing.Size(23, 23);
            this.chkProfesionalTitulo.Size = new System.Drawing.Size(170, 24);
            this.chkProfesionalTitulo.TabIndex = 565;
            this.chkProfesionalTitulo.CheckedChanged += new System.EventHandler(this.chkProfesionalTitulo_CheckedChanged);
            // 
            // lblTituloPj
            // 
            this.lblTituloPj.Appearance.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTituloPj.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.lblTituloPj.Appearance.Options.UseFont = true;
            this.lblTituloPj.Appearance.Options.UseForeColor = true;
            this.lblTituloPj.Appearance.Options.UseTextOptions = true;
            this.lblTituloPj.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblTituloPj.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblTituloPj.LineColor = System.Drawing.Color.Gainsboro;
            this.lblTituloPj.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblTituloPj.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblTituloPj.LineVisible = true;
            this.lblTituloPj.Location = new System.Drawing.Point(980, 41);
            this.lblTituloPj.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblTituloPj.Name = "lblTituloPj";
            this.lblTituloPj.Size = new System.Drawing.Size(439, 30);
            this.lblTituloPj.TabIndex = 455;
            this.lblTituloPj.Text = "Persona jurídica";
            // 
            // lblTituloPn
            // 
            this.lblTituloPn.Appearance.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTituloPn.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.lblTituloPn.Appearance.Options.UseFont = true;
            this.lblTituloPn.Appearance.Options.UseForeColor = true;
            this.lblTituloPn.Appearance.Options.UseTextOptions = true;
            this.lblTituloPn.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblTituloPn.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblTituloPn.LineColor = System.Drawing.Color.Gainsboro;
            this.lblTituloPn.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblTituloPn.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblTituloPn.LineVisible = true;
            this.lblTituloPn.Location = new System.Drawing.Point(9, 37);
            this.lblTituloPn.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblTituloPn.Name = "lblTituloPn";
            this.lblTituloPn.Size = new System.Drawing.Size(283, 30);
            this.lblTituloPn.TabIndex = 454;
            this.lblTituloPn.Text = "Persona natural";
            // 
            // pnlPersonaNatural
            // 
            this.pnlPersonaNatural.Appearance.BorderColor = System.Drawing.Color.Gray;
            this.pnlPersonaNatural.Appearance.Options.UseBorderColor = true;
            this.pnlPersonaNatural.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.pnlPersonaNatural.Controls.Add(this.btnAcreditarTitulo);
            this.pnlPersonaNatural.Controls.Add(this.btnPasswordClient);
            this.pnlPersonaNatural.Controls.Add(this.meDescripcionProductoPn);
            this.pnlPersonaNatural.Controls.Add(this.labelControl24);
            this.pnlPersonaNatural.Controls.Add(this.txtTelefonoPn);
            this.pnlPersonaNatural.Controls.Add(this.txtCorreoPn);
            this.pnlPersonaNatural.Controls.Add(this.lblFilesPn);
            this.pnlPersonaNatural.Controls.Add(this.picAddPn);
            this.pnlPersonaNatural.Controls.Add(this.btnAdjuntarPn);
            this.pnlPersonaNatural.Controls.Add(this.picSolicitante);
            this.pnlPersonaNatural.Controls.Add(this.labelControl18);
            this.pnlPersonaNatural.Controls.Add(this.lueProductosPn);
            this.pnlPersonaNatural.Controls.Add(this.labelControl8);
            this.pnlPersonaNatural.Controls.Add(this.lblTIpoDocumento);
            this.pnlPersonaNatural.Controls.Add(this.labelControl7);
            this.pnlPersonaNatural.Controls.Add(this.lblIdentificacion);
            this.pnlPersonaNatural.Controls.Add(this.labelControl6);
            this.pnlPersonaNatural.Controls.Add(this.lblApellidos);
            this.pnlPersonaNatural.Controls.Add(this.lblNombres);
            this.pnlPersonaNatural.Controls.Add(this.labelControl5);
            this.pnlPersonaNatural.Controls.Add(this.labelControl3);
            this.pnlPersonaNatural.Controls.Add(this.labelControl2);
            this.pnlPersonaNatural.Location = new System.Drawing.Point(9, 70);
            this.pnlPersonaNatural.Margin = new System.Windows.Forms.Padding(2);
            this.pnlPersonaNatural.Name = "pnlPersonaNatural";
            this.pnlPersonaNatural.Size = new System.Drawing.Size(796, 245);
            this.pnlPersonaNatural.TabIndex = 453;
            // 
            // btnAcreditarTitulo
            // 
            this.btnAcreditarTitulo.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.btnAcreditarTitulo.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAcreditarTitulo.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.btnAcreditarTitulo.Appearance.Options.UseBackColor = true;
            this.btnAcreditarTitulo.Appearance.Options.UseFont = true;
            this.btnAcreditarTitulo.Appearance.Options.UseForeColor = true;
            this.btnAcreditarTitulo.Appearance.Options.UseTextOptions = true;
            this.btnAcreditarTitulo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnAcreditarTitulo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnAcreditarTitulo.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_certificate_32x32;
            this.btnAcreditarTitulo.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnAcreditarTitulo.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnAcreditarTitulo.Location = new System.Drawing.Point(582, 106);
            this.btnAcreditarTitulo.LookAndFeel.SkinName = "Whiteprint";
            this.btnAcreditarTitulo.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnAcreditarTitulo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnAcreditarTitulo.Name = "btnAcreditarTitulo";
            this.btnAcreditarTitulo.Size = new System.Drawing.Size(201, 35);
            this.btnAcreditarTitulo.TabIndex = 566;
            this.btnAcreditarTitulo.Text = "&Número colegiación";
            this.btnAcreditarTitulo.Visible = false;
            this.btnAcreditarTitulo.Click += new System.EventHandler(this.btnAcreditarTitulo_Click);
            // 
            // btnPasswordClient
            // 
            this.btnPasswordClient.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.btnPasswordClient.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPasswordClient.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.btnPasswordClient.Appearance.Options.UseBackColor = true;
            this.btnPasswordClient.Appearance.Options.UseFont = true;
            this.btnPasswordClient.Appearance.Options.UseForeColor = true;
            this.btnPasswordClient.Appearance.Options.UseTextOptions = true;
            this.btnPasswordClient.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnPasswordClient.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnPasswordClient.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.password1;
            this.btnPasswordClient.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnPasswordClient.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnPasswordClient.Location = new System.Drawing.Point(582, 177);
            this.btnPasswordClient.LookAndFeel.SkinName = "Whiteprint";
            this.btnPasswordClient.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnPasswordClient.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnPasswordClient.Name = "btnPasswordClient";
            this.btnPasswordClient.Size = new System.Drawing.Size(201, 35);
            this.btnPasswordClient.TabIndex = 520;
            this.btnPasswordClient.Text = "&Crear contraseña";
            this.btnPasswordClient.Click += new System.EventHandler(this.btnPasswordClient_Click);
            // 
            // meDescripcionProductoPn
            // 
            this.meDescripcionProductoPn.Location = new System.Drawing.Point(148, 44);
            this.meDescripcionProductoPn.Margin = new System.Windows.Forms.Padding(2);
            this.meDescripcionProductoPn.Name = "meDescripcionProductoPn";
            this.meDescripcionProductoPn.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.meDescripcionProductoPn.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.meDescripcionProductoPn.Properties.Appearance.Options.UseFont = true;
            this.meDescripcionProductoPn.Properties.Appearance.Options.UseForeColor = true;
            this.meDescripcionProductoPn.Properties.ReadOnly = true;
            this.meDescripcionProductoPn.Size = new System.Drawing.Size(257, 37);
            this.meDescripcionProductoPn.TabIndex = 458;
            // 
            // labelControl24
            // 
            this.labelControl24.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl24.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl24.Appearance.Options.UseFont = true;
            this.labelControl24.Appearance.Options.UseForeColor = true;
            this.labelControl24.Appearance.Options.UseTextOptions = true;
            this.labelControl24.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl24.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl24.Location = new System.Drawing.Point(-2, 44);
            this.labelControl24.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(143, 22);
            this.labelControl24.TabIndex = 458;
            this.labelControl24.Text = "Descrip. de producto:";
            // 
            // txtTelefonoPn
            // 
            this.txtTelefonoPn.EditValue = "";
            this.txtTelefonoPn.Location = new System.Drawing.Point(148, 217);
            this.txtTelefonoPn.Margin = new System.Windows.Forms.Padding(2);
            this.txtTelefonoPn.Name = "txtTelefonoPn";
            this.txtTelefonoPn.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTelefonoPn.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.txtTelefonoPn.Properties.Appearance.Options.UseFont = true;
            this.txtTelefonoPn.Properties.Appearance.Options.UseForeColor = true;
            this.txtTelefonoPn.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTelefonoPn.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTelefonoPn.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtTelefonoPn.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTelefonoPn.Properties.Mask.EditMask = "[0-9]+";
            this.txtTelefonoPn.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtTelefonoPn.Properties.MaxLength = 20;
            this.txtTelefonoPn.Size = new System.Drawing.Size(257, 26);
            this.txtTelefonoPn.TabIndex = 454;
            // 
            // txtCorreoPn
            // 
            this.txtCorreoPn.EditValue = "[txtCorreoPn]";
            this.txtCorreoPn.Location = new System.Drawing.Point(148, 188);
            this.txtCorreoPn.Margin = new System.Windows.Forms.Padding(2);
            this.txtCorreoPn.Name = "txtCorreoPn";
            this.txtCorreoPn.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCorreoPn.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.txtCorreoPn.Properties.Appearance.Options.UseFont = true;
            this.txtCorreoPn.Properties.Appearance.Options.UseForeColor = true;
            this.txtCorreoPn.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtCorreoPn.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtCorreoPn.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtCorreoPn.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.txtCorreoPn.Properties.Mask.EditMask = "(\\w|[.\\-])+@(\\w|[\\-]+.)*(\\w|[\\-]){2,100}.[a-zA-Z]{2,4}";
            this.txtCorreoPn.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtCorreoPn.Properties.MaxLength = 100;
            this.txtCorreoPn.Size = new System.Drawing.Size(257, 26);
            this.txtCorreoPn.TabIndex = 454;
            // 
            // lblFilesPn
            // 
            this.lblFilesPn.Appearance.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFilesPn.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblFilesPn.Appearance.Options.UseFont = true;
            this.lblFilesPn.Appearance.Options.UseForeColor = true;
            this.lblFilesPn.Appearance.Options.UseTextOptions = true;
            this.lblFilesPn.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblFilesPn.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblFilesPn.Location = new System.Drawing.Point(626, 51);
            this.lblFilesPn.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblFilesPn.Name = "lblFilesPn";
            this.lblFilesPn.Size = new System.Drawing.Size(157, 33);
            this.lblFilesPn.TabIndex = 519;
            this.lblFilesPn.Text = "(0) archivos";
            // 
            // picAddPn
            // 
            this.picAddPn.Cursor = System.Windows.Forms.Cursors.Default;
            this.picAddPn.EditValue = global::Gv.ExodusBc.UI.Properties.Resources.ic_clip_32x32;
            this.picAddPn.Location = new System.Drawing.Point(594, 52);
            this.picAddPn.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.picAddPn.Name = "picAddPn";
            this.picAddPn.Properties.AllowFocused = false;
            this.picAddPn.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.picAddPn.Properties.Appearance.Font = new System.Drawing.Font("Arial Black", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.picAddPn.Properties.Appearance.ForeColor = System.Drawing.Color.DarkGray;
            this.picAddPn.Properties.Appearance.Options.UseBackColor = true;
            this.picAddPn.Properties.Appearance.Options.UseFont = true;
            this.picAddPn.Properties.Appearance.Options.UseForeColor = true;
            this.picAddPn.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.picAddPn.Properties.NullText = "5";
            this.picAddPn.Properties.ShowMenu = false;
            this.picAddPn.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.picAddPn.Size = new System.Drawing.Size(28, 33);
            this.picAddPn.TabIndex = 518;
            // 
            // btnAdjuntarPn
            // 
            this.btnAdjuntarPn.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.btnAdjuntarPn.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdjuntarPn.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.btnAdjuntarPn.Appearance.Options.UseBackColor = true;
            this.btnAdjuntarPn.Appearance.Options.UseFont = true;
            this.btnAdjuntarPn.Appearance.Options.UseForeColor = true;
            this.btnAdjuntarPn.Appearance.Options.UseTextOptions = true;
            this.btnAdjuntarPn.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnAdjuntarPn.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnAdjuntarPn.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_import_32x32;
            this.btnAdjuntarPn.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnAdjuntarPn.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnAdjuntarPn.Location = new System.Drawing.Point(581, 11);
            this.btnAdjuntarPn.LookAndFeel.SkinName = "Whiteprint";
            this.btnAdjuntarPn.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnAdjuntarPn.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnAdjuntarPn.Name = "btnAdjuntarPn";
            this.btnAdjuntarPn.Size = new System.Drawing.Size(202, 35);
            this.btnAdjuntarPn.TabIndex = 455;
            this.btnAdjuntarPn.Text = "&Adjuntar documentos";
            this.btnAdjuntarPn.Click += new System.EventHandler(this.btnAdjuntarPn_Click);
            // 
            // picSolicitante
            // 
            this.picSolicitante.Cursor = System.Windows.Forms.Cursors.Default;
            this.picSolicitante.EditValue = global::Gv.ExodusBc.UI.Properties.Resources.SinFoto;
            this.picSolicitante.Location = new System.Drawing.Point(411, 11);
            this.picSolicitante.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.picSolicitante.Name = "picSolicitante";
            this.picSolicitante.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picSolicitante.Properties.Appearance.Options.UseBackColor = true;
            this.picSolicitante.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.picSolicitante.Size = new System.Drawing.Size(159, 201);
            this.picSolicitante.TabIndex = 448;
            // 
            // labelControl18
            // 
            this.labelControl18.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl18.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl18.Appearance.Options.UseFont = true;
            this.labelControl18.Appearance.Options.UseForeColor = true;
            this.labelControl18.Appearance.Options.UseTextOptions = true;
            this.labelControl18.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl18.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl18.Location = new System.Drawing.Point(-4, 219);
            this.labelControl18.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(145, 22);
            this.labelControl18.TabIndex = 446;
            this.labelControl18.Text = "Número de teléfono:";
            // 
            // lueProductosPn
            // 
            this.lueProductosPn.Location = new System.Drawing.Point(148, 10);
            this.lueProductosPn.Margin = new System.Windows.Forms.Padding(2);
            this.lueProductosPn.Name = "lueProductosPn";
            this.lueProductosPn.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueProductosPn.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lueProductosPn.Properties.Appearance.Options.UseFont = true;
            this.lueProductosPn.Properties.Appearance.Options.UseForeColor = true;
            this.lueProductosPn.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueProductosPn.Properties.AppearanceDropDown.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lueProductosPn.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lueProductosPn.Properties.AppearanceDropDown.Options.UseForeColor = true;
            this.lueProductosPn.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lueProductosPn.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lueProductosPn.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.lueProductosPn.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueProductosPn.Properties.NullText = "";
            this.lueProductosPn.Properties.PopupFilterMode = DevExpress.XtraEditors.PopupFilterMode.Contains;
            this.lueProductosPn.Size = new System.Drawing.Size(257, 26);
            this.lueProductosPn.TabIndex = 8;
            this.lueProductosPn.EditValueChanged += new System.EventHandler(this.lueProductosPn_EditValueChanged);
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl8.Appearance.Options.UseFont = true;
            this.labelControl8.Appearance.Options.UseForeColor = true;
            this.labelControl8.Appearance.Options.UseTextOptions = true;
            this.labelControl8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl8.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl8.Location = new System.Drawing.Point(7, 190);
            this.labelControl8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(134, 22);
            this.labelControl8.TabIndex = 444;
            this.labelControl8.Text = "Correo electrónico:";
            // 
            // lblTIpoDocumento
            // 
            this.lblTIpoDocumento.Appearance.Font = new System.Drawing.Font("Segoe UI", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTIpoDocumento.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblTIpoDocumento.Appearance.Options.UseFont = true;
            this.lblTIpoDocumento.Appearance.Options.UseForeColor = true;
            this.lblTIpoDocumento.Appearance.Options.UseTextOptions = true;
            this.lblTIpoDocumento.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblTIpoDocumento.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblTIpoDocumento.LineColor = System.Drawing.Color.DimGray;
            this.lblTIpoDocumento.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblTIpoDocumento.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblTIpoDocumento.LineVisible = true;
            this.lblTIpoDocumento.Location = new System.Drawing.Point(148, 81);
            this.lblTIpoDocumento.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblTIpoDocumento.Name = "lblTIpoDocumento";
            this.lblTIpoDocumento.Size = new System.Drawing.Size(257, 26);
            this.lblTIpoDocumento.TabIndex = 443;
            this.lblTIpoDocumento.Text = "[lblTIpoDocumento]";
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl7.Appearance.Options.UseFont = true;
            this.labelControl7.Appearance.Options.UseForeColor = true;
            this.labelControl7.Appearance.Options.UseTextOptions = true;
            this.labelControl7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl7.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl7.Location = new System.Drawing.Point(7, 85);
            this.labelControl7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(134, 22);
            this.labelControl7.TabIndex = 442;
            this.labelControl7.Text = "Tipo de documento:";
            // 
            // lblIdentificacion
            // 
            this.lblIdentificacion.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIdentificacion.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblIdentificacion.Appearance.Options.UseFont = true;
            this.lblIdentificacion.Appearance.Options.UseForeColor = true;
            this.lblIdentificacion.Appearance.Options.UseTextOptions = true;
            this.lblIdentificacion.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblIdentificacion.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblIdentificacion.LineColor = System.Drawing.Color.DimGray;
            this.lblIdentificacion.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblIdentificacion.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblIdentificacion.LineVisible = true;
            this.lblIdentificacion.Location = new System.Drawing.Point(148, 107);
            this.lblIdentificacion.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblIdentificacion.Name = "lblIdentificacion";
            this.lblIdentificacion.Size = new System.Drawing.Size(257, 26);
            this.lblIdentificacion.TabIndex = 441;
            this.lblIdentificacion.Text = "[lblIdentificacion]";
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl6.Appearance.Options.UseFont = true;
            this.labelControl6.Appearance.Options.UseForeColor = true;
            this.labelControl6.Appearance.Options.UseTextOptions = true;
            this.labelControl6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl6.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl6.Location = new System.Drawing.Point(0, 112);
            this.labelControl6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(141, 22);
            this.labelControl6.TabIndex = 440;
            this.labelControl6.Text = "Número documento:";
            // 
            // lblApellidos
            // 
            this.lblApellidos.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblApellidos.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblApellidos.Appearance.Options.UseFont = true;
            this.lblApellidos.Appearance.Options.UseForeColor = true;
            this.lblApellidos.Appearance.Options.UseTextOptions = true;
            this.lblApellidos.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblApellidos.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblApellidos.LineColor = System.Drawing.Color.DimGray;
            this.lblApellidos.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblApellidos.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblApellidos.LineVisible = true;
            this.lblApellidos.Location = new System.Drawing.Point(148, 160);
            this.lblApellidos.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblApellidos.Name = "lblApellidos";
            this.lblApellidos.Size = new System.Drawing.Size(257, 26);
            this.lblApellidos.TabIndex = 439;
            this.lblApellidos.Text = "[lblApellidos]";
            // 
            // lblNombres
            // 
            this.lblNombres.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombres.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblNombres.Appearance.Options.UseFont = true;
            this.lblNombres.Appearance.Options.UseForeColor = true;
            this.lblNombres.Appearance.Options.UseTextOptions = true;
            this.lblNombres.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblNombres.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblNombres.LineColor = System.Drawing.Color.DimGray;
            this.lblNombres.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblNombres.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblNombres.LineVisible = true;
            this.lblNombres.Location = new System.Drawing.Point(148, 134);
            this.lblNombres.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblNombres.Name = "lblNombres";
            this.lblNombres.Size = new System.Drawing.Size(257, 26);
            this.lblNombres.TabIndex = 438;
            this.lblNombres.Text = "[lblNombres]";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Appearance.Options.UseForeColor = true;
            this.labelControl5.Appearance.Options.UseTextOptions = true;
            this.labelControl5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl5.Location = new System.Drawing.Point(12, 165);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(129, 22);
            this.labelControl5.TabIndex = 437;
            this.labelControl5.Text = "Apellidos:";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Appearance.Options.UseForeColor = true;
            this.labelControl3.Appearance.Options.UseTextOptions = true;
            this.labelControl3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl3.Location = new System.Drawing.Point(6, 15);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(135, 22);
            this.labelControl3.TabIndex = 436;
            this.labelControl3.Text = "Tipo de producto:";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Appearance.Options.UseForeColor = true;
            this.labelControl2.Appearance.Options.UseTextOptions = true;
            this.labelControl2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl2.Location = new System.Drawing.Point(7, 140);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(134, 22);
            this.labelControl2.TabIndex = 435;
            this.labelControl2.Text = "Nombres:";
            // 
            // btnSolicitarTicket
            // 
            this.btnSolicitarTicket.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.btnSolicitarTicket.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSolicitarTicket.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.btnSolicitarTicket.Appearance.Options.UseBackColor = true;
            this.btnSolicitarTicket.Appearance.Options.UseFont = true;
            this.btnSolicitarTicket.Appearance.Options.UseForeColor = true;
            this.btnSolicitarTicket.Appearance.Options.UseTextOptions = true;
            this.btnSolicitarTicket.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnSolicitarTicket.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnSolicitarTicket.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_ticketnone_32x32;
            this.btnSolicitarTicket.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnSolicitarTicket.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnSolicitarTicket.Location = new System.Drawing.Point(10, 367);
            this.btnSolicitarTicket.LookAndFeel.SkinName = "Office 2013";
            this.btnSolicitarTicket.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnSolicitarTicket.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnSolicitarTicket.Name = "btnSolicitarTicket";
            this.btnSolicitarTicket.Size = new System.Drawing.Size(272, 35);
            this.btnSolicitarTicket.TabIndex = 447;
            this.btnSolicitarTicket.Text = "&Enviar solicitud para aprobación";
            this.btnSolicitarTicket.Click += new System.EventHandler(this.btnSolicitarTicket_Click);
            // 
            // pnlSolicitarTicket
            // 
            this.pnlSolicitarTicket.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlSolicitarTicket.Appearance.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pnlSolicitarTicket.Appearance.BorderColor = System.Drawing.Color.Gainsboro;
            this.pnlSolicitarTicket.Appearance.Options.UseBackColor = true;
            this.pnlSolicitarTicket.Appearance.Options.UseBorderColor = true;
            this.pnlSolicitarTicket.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.pnlSolicitarTicket.Controls.Add(this.pnlVerificacionHuellas);
            this.pnlSolicitarTicket.Controls.Add(this.lblTituloCrearSolicitud);
            this.pnlSolicitarTicket.Controls.Add(this.btnValidarPersona);
            this.pnlSolicitarTicket.Controls.Add(this.picPaso3);
            this.pnlSolicitarTicket.Location = new System.Drawing.Point(8, 314);
            this.pnlSolicitarTicket.Margin = new System.Windows.Forms.Padding(2);
            this.pnlSolicitarTicket.Name = "pnlSolicitarTicket";
            this.pnlSolicitarTicket.Size = new System.Drawing.Size(1417, 49);
            this.pnlSolicitarTicket.TabIndex = 450;
            // 
            // pnlVerificacionHuellas
            // 
            this.pnlVerificacionHuellas.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlVerificacionHuellas.Appearance.BackColor = System.Drawing.Color.White;
            this.pnlVerificacionHuellas.Appearance.Options.UseBackColor = true;
            this.pnlVerificacionHuellas.Appearance.Options.UseBorderColor = true;
            this.pnlVerificacionHuellas.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlVerificacionHuellas.Controls.Add(this.lblGifValidando);
            this.pnlVerificacionHuellas.Controls.Add(this.labelControl22);
            this.pnlVerificacionHuellas.Controls.Add(this.lblEstadoValidacion);
            this.pnlVerificacionHuellas.Location = new System.Drawing.Point(672, 8);
            this.pnlVerificacionHuellas.LookAndFeel.SkinName = "Foggy";
            this.pnlVerificacionHuellas.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.pnlVerificacionHuellas.LookAndFeel.UseDefaultLookAndFeel = false;
            this.pnlVerificacionHuellas.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pnlVerificacionHuellas.Name = "pnlVerificacionHuellas";
            this.pnlVerificacionHuellas.Size = new System.Drawing.Size(487, 34);
            this.pnlVerificacionHuellas.TabIndex = 456;
            this.pnlVerificacionHuellas.Visible = false;
            // 
            // lblGifValidando
            // 
            this.lblGifValidando.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lblGifValidando.Appearance.BackColor2 = System.Drawing.Color.Transparent;
            this.lblGifValidando.Appearance.Image = global::Gv.ExodusBc.UI.Properties.Resources.loading;
            this.lblGifValidando.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblGifValidando.Appearance.Options.UseBackColor = true;
            this.lblGifValidando.Appearance.Options.UseImage = true;
            this.lblGifValidando.Appearance.Options.UseImageAlign = true;
            this.lblGifValidando.Location = new System.Drawing.Point(171, 10);
            this.lblGifValidando.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblGifValidando.Name = "lblGifValidando";
            this.lblGifValidando.Size = new System.Drawing.Size(50, 16);
            this.lblGifValidando.TabIndex = 2;
            this.lblGifValidando.Visible = false;
            // 
            // labelControl22
            // 
            this.labelControl22.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl22.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl22.Appearance.Options.UseFont = true;
            this.labelControl22.Appearance.Options.UseForeColor = true;
            this.labelControl22.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl22.Location = new System.Drawing.Point(7, 6);
            this.labelControl22.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(161, 22);
            this.labelControl22.TabIndex = 1;
            this.labelControl22.Text = "Verificación de huellas:";
            // 
            // lblEstadoValidacion
            // 
            this.lblEstadoValidacion.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstadoValidacion.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblEstadoValidacion.Appearance.Options.UseFont = true;
            this.lblEstadoValidacion.Appearance.Options.UseForeColor = true;
            this.lblEstadoValidacion.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblEstadoValidacion.Location = new System.Drawing.Point(237, 6);
            this.lblEstadoValidacion.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblEstadoValidacion.Name = "lblEstadoValidacion";
            this.lblEstadoValidacion.Size = new System.Drawing.Size(235, 22);
            this.lblEstadoValidacion.TabIndex = 0;
            this.lblEstadoValidacion.Text = "No Iniciada";
            // 
            // lblTituloCrearSolicitud
            // 
            this.lblTituloCrearSolicitud.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTituloCrearSolicitud.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lblTituloCrearSolicitud.Appearance.Options.UseFont = true;
            this.lblTituloCrearSolicitud.Appearance.Options.UseForeColor = true;
            this.lblTituloCrearSolicitud.Appearance.Options.UseTextOptions = true;
            this.lblTituloCrearSolicitud.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblTituloCrearSolicitud.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblTituloCrearSolicitud.LineColor = System.Drawing.Color.Gainsboro;
            this.lblTituloCrearSolicitud.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblTituloCrearSolicitud.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblTituloCrearSolicitud.LineVisible = true;
            this.lblTituloCrearSolicitud.Location = new System.Drawing.Point(43, 11);
            this.lblTituloCrearSolicitud.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblTituloCrearSolicitud.Name = "lblTituloCrearSolicitud";
            this.lblTituloCrearSolicitud.Size = new System.Drawing.Size(357, 23);
            this.lblTituloCrearSolicitud.TabIndex = 453;
            this.lblTituloCrearSolicitud.Text = "Creación de la solicitud";
            // 
            // btnValidarPersona
            // 
            this.btnValidarPersona.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.btnValidarPersona.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnValidarPersona.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.btnValidarPersona.Appearance.Options.UseBackColor = true;
            this.btnValidarPersona.Appearance.Options.UseFont = true;
            this.btnValidarPersona.Appearance.Options.UseForeColor = true;
            this.btnValidarPersona.Appearance.Options.UseTextOptions = true;
            this.btnValidarPersona.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnValidarPersona.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnValidarPersona.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_fingerscan_24x24;
            this.btnValidarPersona.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnValidarPersona.Location = new System.Drawing.Point(493, 8);
            this.btnValidarPersona.LookAndFeel.SkinName = "Whiteprint";
            this.btnValidarPersona.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnValidarPersona.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnValidarPersona.Name = "btnValidarPersona";
            this.btnValidarPersona.Size = new System.Drawing.Size(159, 34);
            this.btnValidarPersona.TabIndex = 457;
            this.btnValidarPersona.Text = "&Validar persona";
            this.btnValidarPersona.Visible = false;
            this.btnValidarPersona.Click += new System.EventHandler(this.btnValidarPersona_Click);
            // 
            // picPaso3
            // 
            this.picPaso3.Cursor = System.Windows.Forms.Cursors.Default;
            this.picPaso3.EditValue = global::Gv.ExodusBc.UI.Properties.Resources.Circulo3;
            this.picPaso3.Location = new System.Drawing.Point(5, 9);
            this.picPaso3.Margin = new System.Windows.Forms.Padding(2);
            this.picPaso3.Name = "picPaso3";
            this.picPaso3.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picPaso3.Properties.Appearance.Options.UseBackColor = true;
            this.picPaso3.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.picPaso3.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picPaso3.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.picPaso3.Size = new System.Drawing.Size(28, 28);
            this.picPaso3.TabIndex = 449;
            // 
            // pnlPersonaJuridica
            // 
            this.pnlPersonaJuridica.Appearance.BorderColor = System.Drawing.Color.Gray;
            this.pnlPersonaJuridica.Appearance.Options.UseBorderColor = true;
            this.pnlPersonaJuridica.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.pnlPersonaJuridica.Controls.Add(this.meDireccionPj);
            this.pnlPersonaJuridica.Controls.Add(this.labelControl13);
            this.pnlPersonaJuridica.Controls.Add(this.btnPasswordClientPj);
            this.pnlPersonaJuridica.Controls.Add(this.pictureEdit1);
            this.pnlPersonaJuridica.Controls.Add(this.lblRepresentantes);
            this.pnlPersonaJuridica.Controls.Add(this.btnAsociarRepresentante);
            this.pnlPersonaJuridica.Controls.Add(this.lblFilesPj);
            this.pnlPersonaJuridica.Controls.Add(this.picAddPj);
            this.pnlPersonaJuridica.Controls.Add(this.btnAdjuntarPj);
            this.pnlPersonaJuridica.Controls.Add(this.lueProductosPj);
            this.pnlPersonaJuridica.Controls.Add(this.labelControl21);
            this.pnlPersonaJuridica.Controls.Add(this.txtNumeroTelefonoPj);
            this.pnlPersonaJuridica.Controls.Add(this.labelControl20);
            this.pnlPersonaJuridica.Controls.Add(this.txtCorreoPj);
            this.pnlPersonaJuridica.Controls.Add(this.labelControl19);
            this.pnlPersonaJuridica.Controls.Add(this.lueTipoDocumentoPj);
            this.pnlPersonaJuridica.Controls.Add(this.txtNumeroDocumentoPj);
            this.pnlPersonaJuridica.Controls.Add(this.txtNombresPj);
            this.pnlPersonaJuridica.Controls.Add(this.labelControl14);
            this.pnlPersonaJuridica.Controls.Add(this.labelControl17);
            this.pnlPersonaJuridica.Controls.Add(this.labelControl15);
            this.pnlPersonaJuridica.Location = new System.Drawing.Point(811, 70);
            this.pnlPersonaJuridica.Margin = new System.Windows.Forms.Padding(2);
            this.pnlPersonaJuridica.Name = "pnlPersonaJuridica";
            this.pnlPersonaJuridica.Size = new System.Drawing.Size(614, 240);
            this.pnlPersonaJuridica.TabIndex = 446;
            this.pnlPersonaJuridica.Visible = false;
            // 
            // meDireccionPj
            // 
            this.meDireccionPj.Location = new System.Drawing.Point(169, 199);
            this.meDireccionPj.Margin = new System.Windows.Forms.Padding(2);
            this.meDireccionPj.Name = "meDireccionPj";
            this.meDireccionPj.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.meDireccionPj.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.meDireccionPj.Properties.Appearance.Options.UseFont = true;
            this.meDireccionPj.Properties.Appearance.Options.UseForeColor = true;
            this.meDireccionPj.Size = new System.Drawing.Size(223, 38);
            this.meDireccionPj.TabIndex = 15;
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl13.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl13.Appearance.Options.UseFont = true;
            this.labelControl13.Appearance.Options.UseForeColor = true;
            this.labelControl13.Appearance.Options.UseTextOptions = true;
            this.labelControl13.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl13.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl13.Location = new System.Drawing.Point(26, 201);
            this.labelControl13.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(132, 22);
            this.labelControl13.TabIndex = 524;
            this.labelControl13.Text = "Direccion:";
            // 
            // btnPasswordClientPj
            // 
            this.btnPasswordClientPj.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.btnPasswordClientPj.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPasswordClientPj.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.btnPasswordClientPj.Appearance.Options.UseBackColor = true;
            this.btnPasswordClientPj.Appearance.Options.UseFont = true;
            this.btnPasswordClientPj.Appearance.Options.UseForeColor = true;
            this.btnPasswordClientPj.Appearance.Options.UseTextOptions = true;
            this.btnPasswordClientPj.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnPasswordClientPj.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnPasswordClientPj.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.password1;
            this.btnPasswordClientPj.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnPasswordClientPj.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnPasswordClientPj.Location = new System.Drawing.Point(398, 200);
            this.btnPasswordClientPj.LookAndFeel.SkinName = "Whiteprint";
            this.btnPasswordClientPj.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnPasswordClientPj.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnPasswordClientPj.Name = "btnPasswordClientPj";
            this.btnPasswordClientPj.Size = new System.Drawing.Size(210, 35);
            this.btnPasswordClientPj.TabIndex = 18;
            this.btnPasswordClientPj.Text = "&Crear contraseña";
            this.btnPasswordClientPj.Click += new System.EventHandler(this.btnPasswordClientPj_Click);
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit1.EditValue = global::Gv.ExodusBc.UI.Properties.Resources.ic_groupperson_32x32;
            this.pictureEdit1.Location = new System.Drawing.Point(398, 143);
            this.pictureEdit1.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.AllowFocused = false;
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.pictureEdit1.Properties.Appearance.Font = new System.Drawing.Font("Arial Black", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pictureEdit1.Properties.Appearance.ForeColor = System.Drawing.Color.DarkGray;
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Properties.Appearance.Options.UseFont = true;
            this.pictureEdit1.Properties.Appearance.Options.UseForeColor = true;
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit1.Properties.NullText = "5";
            this.pictureEdit1.Properties.ShowMenu = false;
            this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit1.Size = new System.Drawing.Size(27, 33);
            this.pictureEdit1.TabIndex = 522;
            // 
            // lblRepresentantes
            // 
            this.lblRepresentantes.Appearance.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRepresentantes.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblRepresentantes.Appearance.Options.UseFont = true;
            this.lblRepresentantes.Appearance.Options.UseForeColor = true;
            this.lblRepresentantes.Appearance.Options.UseTextOptions = true;
            this.lblRepresentantes.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblRepresentantes.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblRepresentantes.Location = new System.Drawing.Point(429, 144);
            this.lblRepresentantes.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblRepresentantes.Name = "lblRepresentantes";
            this.lblRepresentantes.Size = new System.Drawing.Size(179, 33);
            this.lblRepresentantes.TabIndex = 523;
            this.lblRepresentantes.Text = "(0) representantes";
            // 
            // btnAsociarRepresentante
            // 
            this.btnAsociarRepresentante.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.btnAsociarRepresentante.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAsociarRepresentante.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.btnAsociarRepresentante.Appearance.Options.UseBackColor = true;
            this.btnAsociarRepresentante.Appearance.Options.UseFont = true;
            this.btnAsociarRepresentante.Appearance.Options.UseForeColor = true;
            this.btnAsociarRepresentante.Appearance.Options.UseTextOptions = true;
            this.btnAsociarRepresentante.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnAsociarRepresentante.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnAsociarRepresentante.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_legal_32x32;
            this.btnAsociarRepresentante.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnAsociarRepresentante.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnAsociarRepresentante.Location = new System.Drawing.Point(398, 99);
            this.btnAsociarRepresentante.LookAndFeel.SkinName = "Whiteprint";
            this.btnAsociarRepresentante.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnAsociarRepresentante.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnAsociarRepresentante.Name = "btnAsociarRepresentante";
            this.btnAsociarRepresentante.Size = new System.Drawing.Size(210, 35);
            this.btnAsociarRepresentante.TabIndex = 16;
            this.btnAsociarRepresentante.Text = "&Agregar representantes";
            this.btnAsociarRepresentante.Click += new System.EventHandler(this.btnAsociarRepresentante_Click);
            // 
            // lblFilesPj
            // 
            this.lblFilesPj.Appearance.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFilesPj.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblFilesPj.Appearance.Options.UseFont = true;
            this.lblFilesPj.Appearance.Options.UseForeColor = true;
            this.lblFilesPj.Appearance.Options.UseTextOptions = true;
            this.lblFilesPj.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblFilesPj.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblFilesPj.Location = new System.Drawing.Point(429, 49);
            this.lblFilesPj.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lblFilesPj.Name = "lblFilesPj";
            this.lblFilesPj.Size = new System.Drawing.Size(187, 33);
            this.lblFilesPj.TabIndex = 520;
            this.lblFilesPj.Text = "(0) archivos";
            // 
            // picAddPj
            // 
            this.picAddPj.Cursor = System.Windows.Forms.Cursors.Default;
            this.picAddPj.EditValue = global::Gv.ExodusBc.UI.Properties.Resources.ic_clip_32x32;
            this.picAddPj.Location = new System.Drawing.Point(398, 49);
            this.picAddPj.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.picAddPj.Name = "picAddPj";
            this.picAddPj.Properties.AllowFocused = false;
            this.picAddPj.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.picAddPj.Properties.Appearance.Font = new System.Drawing.Font("Arial Black", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.picAddPj.Properties.Appearance.ForeColor = System.Drawing.Color.DarkGray;
            this.picAddPj.Properties.Appearance.Options.UseBackColor = true;
            this.picAddPj.Properties.Appearance.Options.UseFont = true;
            this.picAddPj.Properties.Appearance.Options.UseForeColor = true;
            this.picAddPj.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.picAddPj.Properties.NullText = "5";
            this.picAddPj.Properties.ShowMenu = false;
            this.picAddPj.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.picAddPj.Size = new System.Drawing.Size(28, 33);
            this.picAddPj.TabIndex = 519;
            // 
            // btnAdjuntarPj
            // 
            this.btnAdjuntarPj.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.btnAdjuntarPj.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdjuntarPj.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.btnAdjuntarPj.Appearance.Options.UseBackColor = true;
            this.btnAdjuntarPj.Appearance.Options.UseFont = true;
            this.btnAdjuntarPj.Appearance.Options.UseForeColor = true;
            this.btnAdjuntarPj.Appearance.Options.UseTextOptions = true;
            this.btnAdjuntarPj.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnAdjuntarPj.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnAdjuntarPj.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_import_32x32;
            this.btnAdjuntarPj.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnAdjuntarPj.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnAdjuntarPj.Location = new System.Drawing.Point(398, 4);
            this.btnAdjuntarPj.LookAndFeel.SkinName = "Whiteprint";
            this.btnAdjuntarPj.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnAdjuntarPj.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnAdjuntarPj.Name = "btnAdjuntarPj";
            this.btnAdjuntarPj.Size = new System.Drawing.Size(210, 35);
            this.btnAdjuntarPj.TabIndex = 17;
            this.btnAdjuntarPj.Text = "&Adjuntar documentos";
            this.btnAdjuntarPj.Click += new System.EventHandler(this.btnAdjuntarPj_Click);
            // 
            // lueProductosPj
            // 
            this.lueProductosPj.Location = new System.Drawing.Point(169, 5);
            this.lueProductosPj.Margin = new System.Windows.Forms.Padding(2);
            this.lueProductosPj.Name = "lueProductosPj";
            this.lueProductosPj.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueProductosPj.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lueProductosPj.Properties.Appearance.Options.UseFont = true;
            this.lueProductosPj.Properties.Appearance.Options.UseForeColor = true;
            this.lueProductosPj.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueProductosPj.Properties.AppearanceDropDown.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lueProductosPj.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lueProductosPj.Properties.AppearanceDropDown.Options.UseForeColor = true;
            this.lueProductosPj.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lueProductosPj.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lueProductosPj.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.lueProductosPj.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueProductosPj.Properties.NullText = "";
            this.lueProductosPj.Size = new System.Drawing.Size(223, 26);
            this.lueProductosPj.TabIndex = 9;
            this.lueProductosPj.EditValueChanged += new System.EventHandler(this.lueProductosPj_EditValueChanged);
            // 
            // labelControl21
            // 
            this.labelControl21.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl21.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl21.Appearance.Options.UseFont = true;
            this.labelControl21.Appearance.Options.UseForeColor = true;
            this.labelControl21.Appearance.Options.UseTextOptions = true;
            this.labelControl21.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl21.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl21.Location = new System.Drawing.Point(12, 6);
            this.labelControl21.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(146, 22);
            this.labelControl21.TabIndex = 450;
            this.labelControl21.Text = "Tipo de producto:";
            // 
            // txtNumeroTelefonoPj
            // 
            this.txtNumeroTelefonoPj.Location = new System.Drawing.Point(169, 135);
            this.txtNumeroTelefonoPj.Margin = new System.Windows.Forms.Padding(2);
            this.txtNumeroTelefonoPj.Name = "txtNumeroTelefonoPj";
            this.txtNumeroTelefonoPj.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNumeroTelefonoPj.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.txtNumeroTelefonoPj.Properties.Appearance.Options.UseFont = true;
            this.txtNumeroTelefonoPj.Properties.Appearance.Options.UseForeColor = true;
            this.txtNumeroTelefonoPj.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtNumeroTelefonoPj.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtNumeroTelefonoPj.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtNumeroTelefonoPj.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNumeroTelefonoPj.Properties.Mask.EditMask = "\\d+";
            this.txtNumeroTelefonoPj.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtNumeroTelefonoPj.Properties.MaxLength = 20;
            this.txtNumeroTelefonoPj.Size = new System.Drawing.Size(223, 26);
            this.txtNumeroTelefonoPj.TabIndex = 13;
            // 
            // labelControl20
            // 
            this.labelControl20.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl20.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl20.Appearance.Options.UseFont = true;
            this.labelControl20.Appearance.Options.UseForeColor = true;
            this.labelControl20.Appearance.Options.UseTextOptions = true;
            this.labelControl20.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl20.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl20.Location = new System.Drawing.Point(24, 169);
            this.labelControl20.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(132, 22);
            this.labelControl20.TabIndex = 447;
            this.labelControl20.Text = "Correo electrónico:";
            // 
            // txtCorreoPj
            // 
            this.txtCorreoPj.Location = new System.Drawing.Point(169, 167);
            this.txtCorreoPj.Margin = new System.Windows.Forms.Padding(2);
            this.txtCorreoPj.Name = "txtCorreoPj";
            this.txtCorreoPj.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCorreoPj.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.txtCorreoPj.Properties.Appearance.Options.UseFont = true;
            this.txtCorreoPj.Properties.Appearance.Options.UseForeColor = true;
            this.txtCorreoPj.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtCorreoPj.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtCorreoPj.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtCorreoPj.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.txtCorreoPj.Properties.Mask.EditMask = "\\w+([-+.\']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
            this.txtCorreoPj.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtCorreoPj.Properties.MaxLength = 100;
            this.txtCorreoPj.Size = new System.Drawing.Size(223, 26);
            this.txtCorreoPj.TabIndex = 14;
            // 
            // labelControl19
            // 
            this.labelControl19.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl19.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl19.Appearance.Options.UseFont = true;
            this.labelControl19.Appearance.Options.UseForeColor = true;
            this.labelControl19.Appearance.Options.UseTextOptions = true;
            this.labelControl19.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl19.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl19.Location = new System.Drawing.Point(12, 135);
            this.labelControl19.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(144, 22);
            this.labelControl19.TabIndex = 445;
            this.labelControl19.Text = "Número de teléfono:";
            // 
            // lueTipoDocumentoPj
            // 
            this.lueTipoDocumentoPj.Location = new System.Drawing.Point(169, 70);
            this.lueTipoDocumentoPj.Margin = new System.Windows.Forms.Padding(2);
            this.lueTipoDocumentoPj.Name = "lueTipoDocumentoPj";
            this.lueTipoDocumentoPj.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueTipoDocumentoPj.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.lueTipoDocumentoPj.Properties.Appearance.Options.UseFont = true;
            this.lueTipoDocumentoPj.Properties.Appearance.Options.UseForeColor = true;
            this.lueTipoDocumentoPj.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lueTipoDocumentoPj.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lueTipoDocumentoPj.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.lueTipoDocumentoPj.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueTipoDocumentoPj.Properties.NullText = "";
            this.lueTipoDocumentoPj.Size = new System.Drawing.Size(223, 26);
            this.lueTipoDocumentoPj.TabIndex = 11;
            this.lueTipoDocumentoPj.EditValueChanged += new System.EventHandler(this.lueTipoDocumentoPj_EditValueChanged);
            // 
            // txtNumeroDocumentoPj
            // 
            this.txtNumeroDocumentoPj.Location = new System.Drawing.Point(169, 102);
            this.txtNumeroDocumentoPj.Margin = new System.Windows.Forms.Padding(2);
            this.txtNumeroDocumentoPj.Name = "txtNumeroDocumentoPj";
            this.txtNumeroDocumentoPj.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNumeroDocumentoPj.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.txtNumeroDocumentoPj.Properties.Appearance.Options.UseFont = true;
            this.txtNumeroDocumentoPj.Properties.Appearance.Options.UseForeColor = true;
            this.txtNumeroDocumentoPj.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtNumeroDocumentoPj.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtNumeroDocumentoPj.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtNumeroDocumentoPj.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNumeroDocumentoPj.Properties.Mask.EditMask = "0000-0000-000000";
            this.txtNumeroDocumentoPj.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
            this.txtNumeroDocumentoPj.Properties.MaxLength = 20;
            this.txtNumeroDocumentoPj.Size = new System.Drawing.Size(223, 26);
            this.txtNumeroDocumentoPj.TabIndex = 12;
            // 
            // txtNombresPj
            // 
            this.txtNombresPj.Location = new System.Drawing.Point(169, 37);
            this.txtNombresPj.Margin = new System.Windows.Forms.Padding(2);
            this.txtNombresPj.Name = "txtNombresPj";
            this.txtNombresPj.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombresPj.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.txtNombresPj.Properties.Appearance.Options.UseFont = true;
            this.txtNombresPj.Properties.Appearance.Options.UseForeColor = true;
            this.txtNombresPj.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtNombresPj.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtNombresPj.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.txtNombresPj.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNombresPj.Size = new System.Drawing.Size(223, 26);
            this.txtNombresPj.TabIndex = 10;
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl14.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl14.Appearance.Options.UseFont = true;
            this.labelControl14.Appearance.Options.UseForeColor = true;
            this.labelControl14.Appearance.Options.UseTextOptions = true;
            this.labelControl14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl14.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl14.Location = new System.Drawing.Point(-1, 38);
            this.labelControl14.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(159, 22);
            this.labelControl14.TabIndex = 444;
            this.labelControl14.Text = "Nombre o razón social:";
            // 
            // labelControl17
            // 
            this.labelControl17.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl17.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl17.Appearance.Options.UseFont = true;
            this.labelControl17.Appearance.Options.UseForeColor = true;
            this.labelControl17.Appearance.Options.UseTextOptions = true;
            this.labelControl17.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl17.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl17.Location = new System.Drawing.Point(24, 71);
            this.labelControl17.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(134, 22);
            this.labelControl17.TabIndex = 443;
            this.labelControl17.Text = "Tipo de documento:";
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl15.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl15.Appearance.Options.UseFont = true;
            this.labelControl15.Appearance.Options.UseForeColor = true;
            this.labelControl15.Appearance.Options.UseTextOptions = true;
            this.labelControl15.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl15.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl15.Location = new System.Drawing.Point(6, 103);
            this.labelControl15.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(150, 22);
            this.labelControl15.TabIndex = 442;
            this.labelControl15.Text = "Número documento:";
            // 
            // picPaso2
            // 
            this.picPaso2.Cursor = System.Windows.Forms.Cursors.Default;
            this.picPaso2.EditValue = global::Gv.ExodusBc.UI.Properties.Resources.Circulo2;
            this.picPaso2.Location = new System.Drawing.Point(3, -1);
            this.picPaso2.Margin = new System.Windows.Forms.Padding(2);
            this.picPaso2.Name = "picPaso2";
            this.picPaso2.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picPaso2.Properties.Appearance.Options.UseBackColor = true;
            this.picPaso2.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.picPaso2.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picPaso2.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.picPaso2.Size = new System.Drawing.Size(28, 28);
            this.picPaso2.TabIndex = 431;
            // 
            // labelControl9
            // 
            this.labelControl9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl9.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.labelControl9.Appearance.Options.UseFont = true;
            this.labelControl9.Appearance.Options.UseForeColor = true;
            this.labelControl9.Appearance.Options.UseTextOptions = true;
            this.labelControl9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl9.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl9.LineColor = System.Drawing.Color.Gainsboro;
            this.labelControl9.LineLocation = DevExpress.XtraEditors.LineLocation.Top;
            this.labelControl9.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.labelControl9.LineVisible = true;
            this.labelControl9.Location = new System.Drawing.Point(-3, 66);
            this.labelControl9.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(1473, 43);
            this.labelControl9.TabIndex = 417;
            // 
            // ssForm
            // 
            this.ssForm.ClosingDelay = 500;
            // 
            // lblRespuestaGenerado
            // 
            this.lblRespuestaGenerado.Appearance.Font = new System.Drawing.Font("Segoe UI", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRespuestaGenerado.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(111)))), ((int)(((byte)(85)))));
            this.lblRespuestaGenerado.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblRespuestaGenerado.Appearance.Options.UseFont = true;
            this.lblRespuestaGenerado.Appearance.Options.UseForeColor = true;
            this.lblRespuestaGenerado.Appearance.Options.UseImageAlign = true;
            this.lblRespuestaGenerado.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblRespuestaGenerado.Location = new System.Drawing.Point(57, 710);
            this.lblRespuestaGenerado.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblRespuestaGenerado.Name = "lblRespuestaGenerado";
            this.lblRespuestaGenerado.Size = new System.Drawing.Size(1379, 27);
            this.lblRespuestaGenerado.TabIndex = 448;
            this.lblRespuestaGenerado.Text = "Ticket solicitado, puede ver el estado del ticket en la bandeja de solicitudes.";
            this.lblRespuestaGenerado.Visible = false;
            // 
            // picTicketGenerado
            // 
            this.picTicketGenerado.Cursor = System.Windows.Forms.Cursors.Default;
            this.picTicketGenerado.EditValue = global::Gv.ExodusBc.UI.Properties.Resources.about_32x32;
            this.picTicketGenerado.Location = new System.Drawing.Point(15, 709);
            this.picTicketGenerado.Margin = new System.Windows.Forms.Padding(2);
            this.picTicketGenerado.Name = "picTicketGenerado";
            this.picTicketGenerado.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picTicketGenerado.Properties.Appearance.Options.UseBackColor = true;
            this.picTicketGenerado.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.picTicketGenerado.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picTicketGenerado.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.picTicketGenerado.Size = new System.Drawing.Size(33, 30);
            this.picTicketGenerado.TabIndex = 450;
            this.picTicketGenerado.Visible = false;
            // 
            // btnSyncCatalogos
            // 
            this.btnSyncCatalogos.Appearance.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnSyncCatalogos.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.btnSyncCatalogos.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSyncCatalogos.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.btnSyncCatalogos.Appearance.Options.UseBackColor = true;
            this.btnSyncCatalogos.Appearance.Options.UseBorderColor = true;
            this.btnSyncCatalogos.Appearance.Options.UseFont = true;
            this.btnSyncCatalogos.Appearance.Options.UseForeColor = true;
            this.btnSyncCatalogos.Appearance.Options.UseTextOptions = true;
            this.btnSyncCatalogos.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.btnSyncCatalogos.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btnSyncCatalogos.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btnSyncCatalogos.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_sync_24x24;
            this.btnSyncCatalogos.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnSyncCatalogos.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnSyncCatalogos.Location = new System.Drawing.Point(9, 74);
            this.btnSyncCatalogos.LookAndFeel.SkinName = "The Bezier";
            this.btnSyncCatalogos.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnSyncCatalogos.Margin = new System.Windows.Forms.Padding(4);
            this.btnSyncCatalogos.Name = "btnSyncCatalogos";
            this.btnSyncCatalogos.Size = new System.Drawing.Size(112, 30);
            this.btnSyncCatalogos.TabIndex = 569;
            this.btnSyncCatalogos.Text = "Sincronizar";
            this.btnSyncCatalogos.ToolTip = "Sincronizar catálogos";
            this.btnSyncCatalogos.Click += new System.EventHandler(this.btnSyncCatalogos_Click);
            // 
            // frmCertificados
            // 
            this.Appearance.BackColor = System.Drawing.Color.White;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1464, 746);
            this.Controls.Add(this.btnSyncCatalogos);
            this.Controls.Add(this.picTicketGenerado);
            this.Controls.Add(this.lblRespuestaGenerado);
            this.Controls.Add(this.groupControl);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.lblTituloAccion);
            this.Controls.Add(this.labelControl9);
            this.Controls.Add(this.grpDetalleSolicitante);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "frmCertificados";
            this.Text = "frmCertificados";
            this.Load += new System.EventHandler(this.frmCertificados_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl)).EndInit();
            this.groupControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlProfesionalTitulado.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlProfesionalTitulado.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteFin.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteFin.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueFacturasCreadas.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFacturaManual.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlClientes)).EndInit();
            this.pnlClientes.ResumeLayout(false);
            this.pnlClientes.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lueClientes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkOtrosClientes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlBusquedaBasica)).EndInit();
            this.pnlBusquedaBasica.ResumeLayout(false);
            this.pnlBusquedaBasica.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumeroDocumentoPn.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTipoDocumentoPn.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlBusquedaAvanzada)).EndInit();
            this.pnlBusquedaAvanzada.ResumeLayout(false);
            this.pnlBusquedaAvanzada.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dteFechaNacimiento.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteFechaNacimiento.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueSexo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrimerApellido.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrimerNombre.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPaso1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPersonaJ.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpDetalleSolicitante)).EndInit();
            this.grpDetalleSolicitante.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lueColegioProfesional.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkProfesionalTitulo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlPersonaNatural)).EndInit();
            this.pnlPersonaNatural.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.meDescripcionProductoPn.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTelefonoPn.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCorreoPn.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAddPn.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSolicitante.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueProductosPn.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSolicitarTicket)).EndInit();
            this.pnlSolicitarTicket.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlVerificacionHuellas)).EndInit();
            this.pnlVerificacionHuellas.ResumeLayout(false);
            this.pnlVerificacionHuellas.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picPaso3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlPersonaJuridica)).EndInit();
            this.pnlPersonaJuridica.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.meDireccionPj.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAddPj.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueProductosPj.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumeroTelefonoPj.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCorreoPj.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTipoDocumentoPj.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumeroDocumentoPj.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNombresPj.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPaso2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picTicketGenerado.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnSalir;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl lblNombreFormulario;
        private DevExpress.XtraEditors.LabelControl lblTituloAccion;
        private DevExpress.XtraEditors.GroupControl groupControl;
        private DevExpress.XtraEditors.SimpleButton btnBuscarRegistro;
        private DevExpress.XtraEditors.GroupControl grpDetalleSolicitante;
        private DevExpress.XtraEditors.PictureEdit picPaso1;
        private DevExpress.XtraEditors.PictureEdit picPaso2;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraSplashScreen.SplashScreenManager ssForm;
        private DevExpress.XtraEditors.PanelControl pnlBusquedaAvanzada;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LookUpEdit lueSexo;
        private DevExpress.XtraEditors.TextEdit txtPrimerApellido;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.TextEdit txtPrimerNombre;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.DateEdit dteFechaNacimiento;
        private DevExpress.XtraEditors.PanelControl pnlBusquedaBasica;
        private DevExpress.XtraEditors.TextEdit txtNumeroDocumentoPn;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.CheckEdit chkPersonaJ;
        private DevExpress.XtraEditors.PanelControl pnlPersonaJuridica;
        private DevExpress.XtraEditors.TextEdit txtNumeroTelefonoPj;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.TextEdit txtCorreoPj;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.LookUpEdit lueTipoDocumentoPj;
        private DevExpress.XtraEditors.TextEdit txtNumeroDocumentoPj;
        private DevExpress.XtraEditors.TextEdit txtNombresPj;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.PanelControl pnlSolicitarTicket;
        private DevExpress.XtraEditors.PictureEdit picPaso3;
        private DevExpress.XtraEditors.LabelControl lblRespuestaGenerado;
        private DevExpress.XtraEditors.SimpleButton btnSolicitarTicket;
        private DevExpress.XtraEditors.PictureEdit picTicketGenerado;
        private DevExpress.XtraEditors.LookUpEdit lueProductosPj;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.LabelControl lblTituloCrearSolicitud;
        private DevExpress.XtraEditors.PanelControl pnlPersonaNatural;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.LookUpEdit lueProductosPn;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl lblTIpoDocumento;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl lblIdentificacion;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl lblApellidos;
        private DevExpress.XtraEditors.LabelControl lblNombres;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.PictureEdit picSolicitante;
        private DevExpress.XtraEditors.LabelControl lblTituloPn;
        private DevExpress.XtraEditors.SimpleButton btnAdjuntarPn;
        private DevExpress.XtraEditors.LabelControl lblTituloPj;
        private DevExpress.XtraEditors.LabelControl lblFilesPn;
        private DevExpress.XtraEditors.PictureEdit picAddPn;
        private DevExpress.XtraEditors.SimpleButton btnAdjuntarPj;
        private DevExpress.XtraEditors.PictureEdit picAddPj;
        private DevExpress.XtraEditors.LabelControl lblFilesPj;
        private DevExpress.XtraEditors.SimpleButton btnLimpiar;
        private DevExpress.XtraEditors.SimpleButton btnAsociarRepresentante;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.LabelControl lblRepresentantes;
        private DevExpress.XtraEditors.TextEdit txtTelefonoPn;
        private DevExpress.XtraEditors.TextEdit txtCorreoPn;
        private DevExpress.XtraEditors.SimpleButton btnPasswordClient;
        private DevExpress.XtraEditors.SimpleButton btnPasswordClientPj;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.CheckEdit chkOtrosClientes;
        private DevExpress.XtraEditors.LookUpEdit lueClientes;
        private DevExpress.XtraEditors.PanelControl pnlClientes;
        private DevExpress.XtraEditors.LabelControl lblTituloApellidoCliente;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.LabelControl lblApellidosCliente;
        private DevExpress.XtraEditors.LabelControl lblNombreCliente;
        private DevExpress.XtraEditors.SimpleButton btnCrearCliente;
        private DevExpress.XtraEditors.MemoEdit meDireccionPj;
        private DevExpress.XtraEditors.SimpleButton btnLeerIdentidad;
        private DevExpress.XtraEditors.PanelControl pnlVerificacionHuellas;
        private DevExpress.XtraEditors.LabelControl lblGifValidando;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.LabelControl lblEstadoValidacion;
        private DevExpress.XtraEditors.SimpleButton btnValidarPersona;
        private DevExpress.XtraEditors.MemoEdit meDescripcionProductoPn;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.LabelControl lblcantidadproductosPN;
        private DevExpress.XtraEditors.CheckEdit chkFacturaManual;
        private DevExpress.XtraEditors.LookUpEdit lueFacturasCreadas;
        private DevExpress.XtraEditors.DateEdit pnlProfesionalTitulado;
        private DevExpress.XtraEditors.DateEdit dteFin;
        private DevExpress.XtraEditors.SimpleButton btnConsultaFacturaManual;
        private DevExpress.XtraEditors.LabelControl lblFechaInicio;
        private DevExpress.XtraEditors.LabelControl lblFechaFin;
        private DevExpress.XtraEditors.CheckEdit chkProfesionalTitulo;
        private DevExpress.XtraEditors.SimpleButton btnAcreditarTitulo;
        private DevExpress.XtraEditors.LookUpEdit lueColegioProfesional;
        private DevExpress.XtraEditors.LookUpEdit lueTipoDocumentoPn;
        private DevExpress.XtraEditors.SimpleButton btnSyncCatalogos;
    }
}