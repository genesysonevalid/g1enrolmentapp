﻿namespace Gv.ExodusBc.UI.Modulos.Certificados
{
    partial class frmAdjuntos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions7 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject25 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject26 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject27 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject28 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions8 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject29 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject30 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject31 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject32 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions9 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject33 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject34 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject35 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject36 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions10 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject37 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject38 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject39 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject40 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions11 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject41 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject42 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject43 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject44 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions12 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject45 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject46 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject47 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject48 = new DevExpress.Utils.SerializableAppearanceObject();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAdjuntos));
            this.btnEliminar = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.btnVisualizar1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.btnEliminar1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.btnVisualizar = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.btnImportar = new DevExpress.XtraEditors.SimpleButton();
            this.btnScanear = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.lueTipoDocumentoAdjunto = new DevExpress.XtraEditors.LookUpEdit();
            this.gcDocs = new DevExpress.XtraGrid.GridControl();
            this.gvDocumentosAdjuntos = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.Numero = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FKDocumentType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Type = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Nombre = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Eliminar = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnEliminar2 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.Visualizar = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnVisualizar2 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.ssForm = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::Gv.ExodusBc.UI.frmWaitForm), true, true);
            this.ckeAlimentadorAutomatico = new DevExpress.XtraEditors.CheckEdit();
            this.btnReporteContrato = new DevExpress.XtraEditors.SimpleButton();
            this.pdfViewerDocs = new DevExpress.XtraPdfViewer.PdfViewer();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.pdfCommandBar1 = new DevExpress.XtraPdfViewer.Bars.PdfCommandBar();
            this.pdfPreviousPageBarItem1 = new DevExpress.XtraPdfViewer.Bars.PdfPreviousPageBarItem();
            this.pdfNextPageBarItem1 = new DevExpress.XtraPdfViewer.Bars.PdfNextPageBarItem();
            this.pdfSetPageNumberBarItem1 = new DevExpress.XtraPdfViewer.Bars.PdfSetPageNumberBarItem();
            this.repositoryItemPageNumberEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPageNumberEdit();
            this.pdfZoomOutBarItem1 = new DevExpress.XtraPdfViewer.Bars.PdfZoomOutBarItem();
            this.pdfZoomInBarItem1 = new DevExpress.XtraPdfViewer.Bars.PdfZoomInBarItem();
            this.pdfExactZoomListBarSubItem1 = new DevExpress.XtraPdfViewer.Bars.PdfExactZoomListBarSubItem();
            this.pdfZoom10CheckItem1 = new DevExpress.XtraPdfViewer.Bars.PdfZoom10CheckItem();
            this.pdfZoom25CheckItem1 = new DevExpress.XtraPdfViewer.Bars.PdfZoom25CheckItem();
            this.pdfZoom50CheckItem1 = new DevExpress.XtraPdfViewer.Bars.PdfZoom50CheckItem();
            this.pdfZoom75CheckItem1 = new DevExpress.XtraPdfViewer.Bars.PdfZoom75CheckItem();
            this.pdfZoom100CheckItem1 = new DevExpress.XtraPdfViewer.Bars.PdfZoom100CheckItem();
            this.pdfZoom125CheckItem1 = new DevExpress.XtraPdfViewer.Bars.PdfZoom125CheckItem();
            this.pdfZoom150CheckItem1 = new DevExpress.XtraPdfViewer.Bars.PdfZoom150CheckItem();
            this.pdfZoom200CheckItem1 = new DevExpress.XtraPdfViewer.Bars.PdfZoom200CheckItem();
            this.pdfZoom400CheckItem1 = new DevExpress.XtraPdfViewer.Bars.PdfZoom400CheckItem();
            this.pdfZoom500CheckItem1 = new DevExpress.XtraPdfViewer.Bars.PdfZoom500CheckItem();
            this.pdfSetActualSizeZoomModeCheckItem1 = new DevExpress.XtraPdfViewer.Bars.PdfSetActualSizeZoomModeCheckItem();
            this.pdfSetPageLevelZoomModeCheckItem1 = new DevExpress.XtraPdfViewer.Bars.PdfSetPageLevelZoomModeCheckItem();
            this.pdfSetFitWidthZoomModeCheckItem1 = new DevExpress.XtraPdfViewer.Bars.PdfSetFitWidthZoomModeCheckItem();
            this.pdfSetFitVisibleZoomModeCheckItem1 = new DevExpress.XtraPdfViewer.Bars.PdfSetFitVisibleZoomModeCheckItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.pdfFindTextBarItem1 = new DevExpress.XtraPdfViewer.Bars.PdfFindTextBarItem();
            this.imgScanPreview = new DevExpress.XtraEditors.PictureEdit();
            this.pdfBarController2 = new DevExpress.XtraPdfViewer.Bars.PdfBarController(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.rdgTipoContratoJuridico = new DevExpress.XtraEditors.RadioGroup();
            ((System.ComponentModel.ISupportInitialize)(this.btnEliminar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnVisualizar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEliminar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnVisualizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTipoDocumentoAdjunto.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDocs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvDocumentosAdjuntos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEliminar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnVisualizar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckeAlimentadorAutomatico.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPageNumberEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgScanPreview.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pdfBarController2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdgTipoContratoJuridico.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // btnEliminar
            // 
            this.btnEliminar.AutoHeight = false;
            editorButtonImageOptions7.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_winclose26x26;
            this.btnEliminar.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions7, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject25, serializableAppearanceObject26, serializableAppearanceObject27, serializableAppearanceObject28, "Eliminar documento", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.btnEliminar.ContextImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_winclose26x26;
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // btnVisualizar1
            // 
            this.btnVisualizar1.AutoHeight = false;
            editorButtonImageOptions8.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_viewer_32x32;
            this.btnVisualizar1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions8, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject29, serializableAppearanceObject30, serializableAppearanceObject31, serializableAppearanceObject32, "Visualizar documento", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.btnVisualizar1.ContextImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_viewer_32x32;
            this.btnVisualizar1.Name = "btnVisualizar1";
            this.btnVisualizar1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // btnEliminar1
            // 
            this.btnEliminar1.Appearance.Image = global::Gv.ExodusBc.UI.Properties.Resources.close_32x321;
            this.btnEliminar1.Appearance.Options.UseImage = true;
            this.btnEliminar1.AutoHeight = false;
            editorButtonImageOptions9.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_winclose26x26;
            serializableAppearanceObject33.Options.UseImage = true;
            this.btnEliminar1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions9, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject33, serializableAppearanceObject34, serializableAppearanceObject35, serializableAppearanceObject36, "Eliminar documento", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.btnEliminar1.ContextImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_winclose26x26;
            this.btnEliminar1.Name = "btnEliminar1";
            this.btnEliminar1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // btnVisualizar
            // 
            this.btnVisualizar.Appearance.Image = global::Gv.ExodusBc.UI.Properties.Resources.show_32x323;
            this.btnVisualizar.Appearance.Options.UseImage = true;
            this.btnVisualizar.AutoHeight = false;
            editorButtonImageOptions10.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_viewer_32x32;
            this.btnVisualizar.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions10, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject37, serializableAppearanceObject38, serializableAppearanceObject39, serializableAppearanceObject40, "Visualizar documento", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.btnVisualizar.Name = "btnVisualizar";
            this.btnVisualizar.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // btnImportar
            // 
            this.btnImportar.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.btnImportar.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImportar.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.btnImportar.Appearance.Options.UseBackColor = true;
            this.btnImportar.Appearance.Options.UseFont = true;
            this.btnImportar.Appearance.Options.UseForeColor = true;
            this.btnImportar.Appearance.Options.UseTextOptions = true;
            this.btnImportar.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnImportar.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnImportar.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_import_32x32;
            this.btnImportar.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnImportar.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnImportar.Location = new System.Drawing.Point(1099, 246);
            this.btnImportar.LookAndFeel.SkinName = "Whiteprint";
            this.btnImportar.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnImportar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnImportar.Name = "btnImportar";
            this.btnImportar.Size = new System.Drawing.Size(132, 35);
            this.btnImportar.TabIndex = 459;
            this.btnImportar.Text = "&Importar";
            this.btnImportar.Click += new System.EventHandler(this.btnImportar_Click);
            // 
            // btnScanear
            // 
            this.btnScanear.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.btnScanear.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnScanear.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.btnScanear.Appearance.Options.UseBackColor = true;
            this.btnScanear.Appearance.Options.UseFont = true;
            this.btnScanear.Appearance.Options.UseForeColor = true;
            this.btnScanear.Appearance.Options.UseTextOptions = true;
            this.btnScanear.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnScanear.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnScanear.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_scanner_32x32;
            this.btnScanear.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnScanear.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnScanear.Location = new System.Drawing.Point(520, 246);
            this.btnScanear.LookAndFeel.SkinName = "Whiteprint";
            this.btnScanear.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnScanear.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnScanear.Name = "btnScanear";
            this.btnScanear.Size = new System.Drawing.Size(139, 35);
            this.btnScanear.TabIndex = 458;
            this.btnScanear.Text = "&Escanear ";
            this.btnScanear.Click += new System.EventHandler(this.btnScanear_Click);
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Appearance.Options.UseForeColor = true;
            this.labelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl4.Location = new System.Drawing.Point(519, 65);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(398, 22);
            this.labelControl4.TabIndex = 464;
            this.labelControl4.Text = "Seleccione un tipo de documento";
            // 
            // lueTipoDocumentoAdjunto
            // 
            this.lueTipoDocumentoAdjunto.Location = new System.Drawing.Point(519, 89);
            this.lueTipoDocumentoAdjunto.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.lueTipoDocumentoAdjunto.Name = "lueTipoDocumentoAdjunto";
            this.lueTipoDocumentoAdjunto.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.lueTipoDocumentoAdjunto.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueTipoDocumentoAdjunto.Properties.Appearance.Options.UseBackColor = true;
            this.lueTipoDocumentoAdjunto.Properties.Appearance.Options.UseFont = true;
            this.lueTipoDocumentoAdjunto.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lueTipoDocumentoAdjunto.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lueTipoDocumentoAdjunto.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.lueTipoDocumentoAdjunto.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueTipoDocumentoAdjunto.Properties.NullText = "";
            this.lueTipoDocumentoAdjunto.Size = new System.Drawing.Size(713, 30);
            this.lueTipoDocumentoAdjunto.TabIndex = 463;
            this.lueTipoDocumentoAdjunto.EditValueChanged += new System.EventHandler(this.lueTipoDocumentoAdjunto_EditValueChanged);
            // 
            // gcDocs
            // 
            this.gcDocs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcDocs.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gcDocs.Location = new System.Drawing.Point(520, 297);
            this.gcDocs.MainView = this.gvDocumentosAdjuntos;
            this.gcDocs.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gcDocs.Name = "gcDocs";
            this.gcDocs.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.btnVisualizar2,
            this.btnEliminar2});
            this.gcDocs.Size = new System.Drawing.Size(712, 324);
            this.gcDocs.TabIndex = 465;
            this.gcDocs.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvDocumentosAdjuntos});
            // 
            // gvDocumentosAdjuntos
            // 
            this.gvDocumentosAdjuntos.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvDocumentosAdjuntos.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.gvDocumentosAdjuntos.Appearance.HeaderPanel.Options.UseFont = true;
            this.gvDocumentosAdjuntos.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvDocumentosAdjuntos.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gvDocumentosAdjuntos.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gvDocumentosAdjuntos.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.gvDocumentosAdjuntos.Appearance.OddRow.Options.UseBackColor = true;
            this.gvDocumentosAdjuntos.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.Numero,
            this.FKDocumentType,
            this.Type,
            this.Nombre,
            this.Eliminar,
            this.Visualizar});
            this.gvDocumentosAdjuntos.GridControl = this.gcDocs;
            this.gvDocumentosAdjuntos.Name = "gvDocumentosAdjuntos";
            this.gvDocumentosAdjuntos.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gvDocumentosAdjuntos.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gvDocumentosAdjuntos.OptionsView.ShowGroupPanel = false;
            this.gvDocumentosAdjuntos.RowHeight = 40;
            // 
            // Numero
            // 
            this.Numero.Caption = "Numero";
            this.Numero.FieldName = "Numero";
            this.Numero.Name = "Numero";
            this.Numero.OptionsColumn.AllowEdit = false;
            this.Numero.OptionsColumn.ReadOnly = true;
            // 
            // FKDocumentType
            // 
            this.FKDocumentType.Caption = "Tipo Documento";
            this.FKDocumentType.FieldName = "FKDocumentType";
            this.FKDocumentType.Name = "FKDocumentType";
            // 
            // Type
            // 
            this.Type.Caption = "Tipo";
            this.Type.FieldName = "Type";
            this.Type.Name = "Type";
            this.Type.OptionsColumn.AllowEdit = false;
            this.Type.OptionsColumn.ReadOnly = true;
            this.Type.Visible = true;
            this.Type.VisibleIndex = 0;
            this.Type.Width = 69;
            // 
            // Nombre
            // 
            this.Nombre.Caption = "Nombre";
            this.Nombre.FieldName = "Name";
            this.Nombre.Name = "Nombre";
            this.Nombre.OptionsColumn.AllowEdit = false;
            this.Nombre.OptionsColumn.ReadOnly = true;
            this.Nombre.Visible = true;
            this.Nombre.VisibleIndex = 1;
            this.Nombre.Width = 517;
            // 
            // Eliminar
            // 
            this.Eliminar.Caption = "Eliminar";
            this.Eliminar.ColumnEdit = this.btnEliminar2;
            this.Eliminar.Name = "Eliminar";
            this.Eliminar.ToolTip = "Eliminar documento";
            this.Eliminar.Visible = true;
            this.Eliminar.VisibleIndex = 2;
            this.Eliminar.Width = 80;
            // 
            // btnEliminar2
            // 
            this.btnEliminar2.AutoHeight = false;
            editorButtonImageOptions11.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_winclose26x26;
            this.btnEliminar2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions11, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject41, serializableAppearanceObject42, serializableAppearanceObject43, serializableAppearanceObject44, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.btnEliminar2.ContextImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_winclose26x26;
            this.btnEliminar2.Name = "btnEliminar2";
            this.btnEliminar2.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btnEliminar2.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btnEliminar2_ButtonClick);
            // 
            // Visualizar
            // 
            this.Visualizar.Caption = "Visualizar";
            this.Visualizar.ColumnEdit = this.btnVisualizar2;
            this.Visualizar.Name = "Visualizar";
            this.Visualizar.OptionsFilter.AllowFilter = false;
            this.Visualizar.ToolTip = "Visualizar documento";
            this.Visualizar.Visible = true;
            this.Visualizar.VisibleIndex = 3;
            this.Visualizar.Width = 80;
            // 
            // btnVisualizar2
            // 
            this.btnVisualizar2.AutoHeight = false;
            editorButtonImageOptions12.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_viewer_32x32;
            this.btnVisualizar2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions12, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject45, serializableAppearanceObject46, serializableAppearanceObject47, serializableAppearanceObject48, "Ver documento", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.btnVisualizar2.ContextImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_viewer_32x32;
            this.btnVisualizar2.Name = "btnVisualizar2";
            this.btnVisualizar2.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btnVisualizar2.Click += new System.EventHandler(this.btnVisualizar2_Click);
            // 
            // ssForm
            // 
            this.ssForm.ClosingDelay = 500;
            // 
            // ckeAlimentadorAutomatico
            // 
            this.ckeAlimentadorAutomatico.Location = new System.Drawing.Point(520, 214);
            this.ckeAlimentadorAutomatico.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ckeAlimentadorAutomatico.Name = "ckeAlimentadorAutomatico";
            this.ckeAlimentadorAutomatico.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckeAlimentadorAutomatico.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(51)))), ((int)(((byte)(53)))));
            this.ckeAlimentadorAutomatico.Properties.Appearance.Options.UseFont = true;
            this.ckeAlimentadorAutomatico.Properties.Appearance.Options.UseForeColor = true;
            this.ckeAlimentadorAutomatico.Properties.Caption = "Usar alimentador automático";
            this.ckeAlimentadorAutomatico.Size = new System.Drawing.Size(233, 24);
            this.ckeAlimentadorAutomatico.TabIndex = 466;
            // 
            // btnReporteContrato
            // 
            this.btnReporteContrato.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(247)))));
            this.btnReporteContrato.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReporteContrato.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.btnReporteContrato.Appearance.Options.UseBackColor = true;
            this.btnReporteContrato.Appearance.Options.UseFont = true;
            this.btnReporteContrato.Appearance.Options.UseForeColor = true;
            this.btnReporteContrato.Appearance.Options.UseTextOptions = true;
            this.btnReporteContrato.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnReporteContrato.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnReporteContrato.ImageOptions.Image = global::Gv.ExodusBc.UI.Properties.Resources.ic_printgray36x36;
            this.btnReporteContrato.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnReporteContrato.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnReporteContrato.Location = new System.Drawing.Point(1053, 126);
            this.btnReporteContrato.LookAndFeel.SkinName = "Whiteprint";
            this.btnReporteContrato.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnReporteContrato.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnReporteContrato.Name = "btnReporteContrato";
            this.btnReporteContrato.Size = new System.Drawing.Size(179, 35);
            this.btnReporteContrato.TabIndex = 467;
            this.btnReporteContrato.Text = "&Imprimir contrato";
            this.btnReporteContrato.Click += new System.EventHandler(this.btnReporteContrato_Click);
            // 
            // pdfViewerDocs
            // 
            this.pdfViewerDocs.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pdfViewerDocs.Location = new System.Drawing.Point(23, 77);
            this.pdfViewerDocs.Margin = new System.Windows.Forms.Padding(2);
            this.pdfViewerDocs.MenuManager = this.barManager1;
            this.pdfViewerDocs.Name = "pdfViewerDocs";
            this.pdfViewerDocs.NavigationPaneInitialVisibility = DevExpress.XtraPdfViewer.PdfNavigationPaneVisibility.Hidden;
            this.pdfViewerDocs.NavigationPanePageVisibility = DevExpress.XtraPdfViewer.PdfNavigationPanePageVisibility.None;
            this.pdfViewerDocs.Size = new System.Drawing.Size(475, 546);
            this.pdfViewerDocs.TabIndex = 468;
            this.pdfViewerDocs.PopupMenuShowing += new DevExpress.XtraPdfViewer.PdfPopupMenuShowingEventHandler(this.pdfViewerDocs_PopupMenuShowing);
            this.pdfViewerDocs.DockChanged += new System.EventHandler(this.pdfViewerDocs_DockChanged);
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.pdfCommandBar1});
            this.barManager1.DockControls.Add(this.barDockControl1);
            this.barManager1.DockControls.Add(this.barDockControl2);
            this.barManager1.DockControls.Add(this.barDockControl3);
            this.barManager1.DockControls.Add(this.barDockControl4);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.pdfFindTextBarItem1,
            this.pdfPreviousPageBarItem1,
            this.pdfNextPageBarItem1,
            this.pdfSetPageNumberBarItem1,
            this.pdfZoomOutBarItem1,
            this.pdfZoomInBarItem1,
            this.pdfExactZoomListBarSubItem1,
            this.pdfZoom10CheckItem1,
            this.pdfZoom25CheckItem1,
            this.pdfZoom50CheckItem1,
            this.pdfZoom75CheckItem1,
            this.pdfZoom100CheckItem1,
            this.pdfZoom125CheckItem1,
            this.pdfZoom150CheckItem1,
            this.pdfZoom200CheckItem1,
            this.pdfZoom400CheckItem1,
            this.pdfZoom500CheckItem1,
            this.pdfSetActualSizeZoomModeCheckItem1,
            this.pdfSetPageLevelZoomModeCheckItem1,
            this.pdfSetFitWidthZoomModeCheckItem1,
            this.pdfSetFitVisibleZoomModeCheckItem1});
            this.barManager1.MaxItemId = 50;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPageNumberEdit1});
            // 
            // pdfCommandBar1
            // 
            this.pdfCommandBar1.Control = this.pdfViewerDocs;
            this.pdfCommandBar1.DockCol = 0;
            this.pdfCommandBar1.DockRow = 0;
            this.pdfCommandBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.pdfCommandBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.pdfPreviousPageBarItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.pdfNextPageBarItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.pdfSetPageNumberBarItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.pdfZoomOutBarItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.pdfZoomInBarItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.pdfExactZoomListBarSubItem1)});
            this.pdfCommandBar1.Offset = 1;
            this.pdfCommandBar1.OptionsBar.AllowQuickCustomization = false;
            this.pdfCommandBar1.OptionsBar.DisableCustomization = true;
            // 
            // pdfPreviousPageBarItem1
            // 
            this.pdfPreviousPageBarItem1.Id = 27;
            this.pdfPreviousPageBarItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("pdfPreviousPageBarItem1.ImageOptions.Image")));
            this.pdfPreviousPageBarItem1.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("pdfPreviousPageBarItem1.ImageOptions.LargeImage")));
            this.pdfPreviousPageBarItem1.Name = "pdfPreviousPageBarItem1";
            // 
            // pdfNextPageBarItem1
            // 
            this.pdfNextPageBarItem1.Id = 28;
            this.pdfNextPageBarItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("pdfNextPageBarItem1.ImageOptions.Image")));
            this.pdfNextPageBarItem1.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("pdfNextPageBarItem1.ImageOptions.LargeImage")));
            this.pdfNextPageBarItem1.Name = "pdfNextPageBarItem1";
            // 
            // pdfSetPageNumberBarItem1
            // 
            this.pdfSetPageNumberBarItem1.Edit = this.repositoryItemPageNumberEdit1;
            this.pdfSetPageNumberBarItem1.EditValue = 0;
            this.pdfSetPageNumberBarItem1.Enabled = false;
            this.pdfSetPageNumberBarItem1.Id = 29;
            this.pdfSetPageNumberBarItem1.Name = "pdfSetPageNumberBarItem1";
            // 
            // repositoryItemPageNumberEdit1
            // 
            this.repositoryItemPageNumberEdit1.AutoHeight = false;
            this.repositoryItemPageNumberEdit1.LabelFormat = "de {0}";
            this.repositoryItemPageNumberEdit1.Mask.EditMask = "########;";
            this.repositoryItemPageNumberEdit1.Name = "repositoryItemPageNumberEdit1";
            this.repositoryItemPageNumberEdit1.Orientation = DevExpress.XtraEditors.PagerOrientation.Horizontal;
            // 
            // pdfZoomOutBarItem1
            // 
            this.pdfZoomOutBarItem1.Id = 30;
            this.pdfZoomOutBarItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("pdfZoomOutBarItem1.ImageOptions.Image")));
            this.pdfZoomOutBarItem1.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("pdfZoomOutBarItem1.ImageOptions.LargeImage")));
            this.pdfZoomOutBarItem1.Name = "pdfZoomOutBarItem1";
            // 
            // pdfZoomInBarItem1
            // 
            this.pdfZoomInBarItem1.Id = 31;
            this.pdfZoomInBarItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("pdfZoomInBarItem1.ImageOptions.Image")));
            this.pdfZoomInBarItem1.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("pdfZoomInBarItem1.ImageOptions.LargeImage")));
            this.pdfZoomInBarItem1.Name = "pdfZoomInBarItem1";
            // 
            // pdfExactZoomListBarSubItem1
            // 
            this.pdfExactZoomListBarSubItem1.Id = 32;
            this.pdfExactZoomListBarSubItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("pdfExactZoomListBarSubItem1.ImageOptions.Image")));
            this.pdfExactZoomListBarSubItem1.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("pdfExactZoomListBarSubItem1.ImageOptions.LargeImage")));
            this.pdfExactZoomListBarSubItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.pdfZoom10CheckItem1, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.pdfZoom25CheckItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.pdfZoom50CheckItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.pdfZoom75CheckItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.pdfZoom100CheckItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.pdfZoom125CheckItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.pdfZoom150CheckItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.pdfZoom200CheckItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.pdfZoom400CheckItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.pdfZoom500CheckItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.pdfSetActualSizeZoomModeCheckItem1, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.pdfSetPageLevelZoomModeCheckItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.pdfSetFitWidthZoomModeCheckItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.pdfSetFitVisibleZoomModeCheckItem1)});
            this.pdfExactZoomListBarSubItem1.Name = "pdfExactZoomListBarSubItem1";
            this.pdfExactZoomListBarSubItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionInMenu;
            // 
            // pdfZoom10CheckItem1
            // 
            this.pdfZoom10CheckItem1.Id = 33;
            this.pdfZoom10CheckItem1.Name = "pdfZoom10CheckItem1";
            // 
            // pdfZoom25CheckItem1
            // 
            this.pdfZoom25CheckItem1.Id = 34;
            this.pdfZoom25CheckItem1.Name = "pdfZoom25CheckItem1";
            // 
            // pdfZoom50CheckItem1
            // 
            this.pdfZoom50CheckItem1.Id = 35;
            this.pdfZoom50CheckItem1.Name = "pdfZoom50CheckItem1";
            // 
            // pdfZoom75CheckItem1
            // 
            this.pdfZoom75CheckItem1.Id = 36;
            this.pdfZoom75CheckItem1.Name = "pdfZoom75CheckItem1";
            // 
            // pdfZoom100CheckItem1
            // 
            this.pdfZoom100CheckItem1.Id = 37;
            this.pdfZoom100CheckItem1.Name = "pdfZoom100CheckItem1";
            // 
            // pdfZoom125CheckItem1
            // 
            this.pdfZoom125CheckItem1.Id = 38;
            this.pdfZoom125CheckItem1.Name = "pdfZoom125CheckItem1";
            // 
            // pdfZoom150CheckItem1
            // 
            this.pdfZoom150CheckItem1.Id = 39;
            this.pdfZoom150CheckItem1.Name = "pdfZoom150CheckItem1";
            // 
            // pdfZoom200CheckItem1
            // 
            this.pdfZoom200CheckItem1.Id = 40;
            this.pdfZoom200CheckItem1.Name = "pdfZoom200CheckItem1";
            // 
            // pdfZoom400CheckItem1
            // 
            this.pdfZoom400CheckItem1.Id = 41;
            this.pdfZoom400CheckItem1.Name = "pdfZoom400CheckItem1";
            // 
            // pdfZoom500CheckItem1
            // 
            this.pdfZoom500CheckItem1.Id = 42;
            this.pdfZoom500CheckItem1.Name = "pdfZoom500CheckItem1";
            // 
            // pdfSetActualSizeZoomModeCheckItem1
            // 
            this.pdfSetActualSizeZoomModeCheckItem1.Id = 43;
            this.pdfSetActualSizeZoomModeCheckItem1.Name = "pdfSetActualSizeZoomModeCheckItem1";
            // 
            // pdfSetPageLevelZoomModeCheckItem1
            // 
            this.pdfSetPageLevelZoomModeCheckItem1.Id = 44;
            this.pdfSetPageLevelZoomModeCheckItem1.Name = "pdfSetPageLevelZoomModeCheckItem1";
            // 
            // pdfSetFitWidthZoomModeCheckItem1
            // 
            this.pdfSetFitWidthZoomModeCheckItem1.Id = 45;
            this.pdfSetFitWidthZoomModeCheckItem1.Name = "pdfSetFitWidthZoomModeCheckItem1";
            // 
            // pdfSetFitVisibleZoomModeCheckItem1
            // 
            this.pdfSetFitVisibleZoomModeCheckItem1.Id = 46;
            this.pdfSetFitVisibleZoomModeCheckItem1.Name = "pdfSetFitVisibleZoomModeCheckItem1";
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Manager = this.barManager1;
            this.barDockControl1.Margin = new System.Windows.Forms.Padding(2);
            this.barDockControl1.Size = new System.Drawing.Size(1248, 37);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 671);
            this.barDockControl2.Manager = this.barManager1;
            this.barDockControl2.Margin = new System.Windows.Forms.Padding(2);
            this.barDockControl2.Size = new System.Drawing.Size(1248, 0);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 37);
            this.barDockControl3.Manager = this.barManager1;
            this.barDockControl3.Margin = new System.Windows.Forms.Padding(2);
            this.barDockControl3.Size = new System.Drawing.Size(0, 634);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(1248, 37);
            this.barDockControl4.Manager = this.barManager1;
            this.barDockControl4.Margin = new System.Windows.Forms.Padding(2);
            this.barDockControl4.Size = new System.Drawing.Size(0, 634);
            // 
            // pdfFindTextBarItem1
            // 
            this.pdfFindTextBarItem1.Id = 26;
            this.pdfFindTextBarItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("pdfFindTextBarItem1.ImageOptions.Image")));
            this.pdfFindTextBarItem1.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("pdfFindTextBarItem1.ImageOptions.LargeImage")));
            this.pdfFindTextBarItem1.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F));
            this.pdfFindTextBarItem1.Name = "pdfFindTextBarItem1";
            // 
            // imgScanPreview
            // 
            this.imgScanPreview.Cursor = System.Windows.Forms.Cursors.Default;
            this.imgScanPreview.Location = new System.Drawing.Point(23, 76);
            this.imgScanPreview.Margin = new System.Windows.Forms.Padding(2);
            this.imgScanPreview.Name = "imgScanPreview";
            this.imgScanPreview.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.imgScanPreview.Size = new System.Drawing.Size(474, 547);
            this.imgScanPreview.TabIndex = 469;
            // 
            // pdfBarController2
            // 
            this.pdfBarController2.BarItems.Add(this.pdfFindTextBarItem1);
            this.pdfBarController2.BarItems.Add(this.pdfPreviousPageBarItem1);
            this.pdfBarController2.BarItems.Add(this.pdfNextPageBarItem1);
            this.pdfBarController2.BarItems.Add(this.pdfSetPageNumberBarItem1);
            this.pdfBarController2.BarItems.Add(this.pdfZoomOutBarItem1);
            this.pdfBarController2.BarItems.Add(this.pdfZoomInBarItem1);
            this.pdfBarController2.BarItems.Add(this.pdfExactZoomListBarSubItem1);
            this.pdfBarController2.BarItems.Add(this.pdfZoom10CheckItem1);
            this.pdfBarController2.BarItems.Add(this.pdfZoom25CheckItem1);
            this.pdfBarController2.BarItems.Add(this.pdfZoom50CheckItem1);
            this.pdfBarController2.BarItems.Add(this.pdfZoom75CheckItem1);
            this.pdfBarController2.BarItems.Add(this.pdfZoom100CheckItem1);
            this.pdfBarController2.BarItems.Add(this.pdfZoom125CheckItem1);
            this.pdfBarController2.BarItems.Add(this.pdfZoom150CheckItem1);
            this.pdfBarController2.BarItems.Add(this.pdfZoom200CheckItem1);
            this.pdfBarController2.BarItems.Add(this.pdfZoom400CheckItem1);
            this.pdfBarController2.BarItems.Add(this.pdfZoom500CheckItem1);
            this.pdfBarController2.BarItems.Add(this.pdfSetActualSizeZoomModeCheckItem1);
            this.pdfBarController2.BarItems.Add(this.pdfSetPageLevelZoomModeCheckItem1);
            this.pdfBarController2.BarItems.Add(this.pdfSetFitWidthZoomModeCheckItem1);
            this.pdfBarController2.BarItems.Add(this.pdfSetFitVisibleZoomModeCheckItem1);
            this.pdfBarController2.Control = this.pdfViewerDocs;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 37);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Margin = new System.Windows.Forms.Padding(2);
            this.barDockControlTop.Size = new System.Drawing.Size(1248, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 671);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Margin = new System.Windows.Forms.Padding(2);
            this.barDockControlBottom.Size = new System.Drawing.Size(1248, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 37);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Margin = new System.Windows.Forms.Padding(2);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 634);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1248, 37);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Margin = new System.Windows.Forms.Padding(2);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 634);
            // 
            // rdgTipoContratoJuridico
            // 
            this.rdgTipoContratoJuridico.Location = new System.Drawing.Point(756, 126);
            this.rdgTipoContratoJuridico.Margin = new System.Windows.Forms.Padding(2);
            this.rdgTipoContratoJuridico.Name = "rdgTipoContratoJuridico";
            this.rdgTipoContratoJuridico.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdgTipoContratoJuridico.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.rdgTipoContratoJuridico.Properties.Appearance.Options.UseFont = true;
            this.rdgTipoContratoJuridico.Properties.Appearance.Options.UseForeColor = true;
            this.rdgTipoContratoJuridico.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.rdgTipoContratoJuridico.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Empresa Privada"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Empresa Publica")});
            this.rdgTipoContratoJuridico.Properties.LookAndFeel.SkinName = "The Bezier";
            this.rdgTipoContratoJuridico.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.rdgTipoContratoJuridico.Size = new System.Drawing.Size(292, 35);
            this.rdgTipoContratoJuridico.TabIndex = 478;
            // 
            // frmAdjuntos
            // 
            this.Appearance.BackColor = System.Drawing.Color.White;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1248, 671);
            this.Controls.Add(this.rdgTipoContratoJuridico);
            this.Controls.Add(this.pdfViewerDocs);
            this.Controls.Add(this.btnReporteContrato);
            this.Controls.Add(this.ckeAlimentadorAutomatico);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.lueTipoDocumentoAdjunto);
            this.Controls.Add(this.gcDocs);
            this.Controls.Add(this.btnImportar);
            this.Controls.Add(this.btnScanear);
            this.Controls.Add(this.imgScanPreview);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmAdjuntos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Documentos adjuntos";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmAdjuntos_FormClosing);
            this.Load += new System.EventHandler(this.frmAdjuntos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.btnEliminar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnVisualizar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEliminar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnVisualizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTipoDocumentoAdjunto.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDocs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvDocumentosAdjuntos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEliminar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnVisualizar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckeAlimentadorAutomatico.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPageNumberEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgScanPreview.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pdfBarController2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdgTipoContratoJuridico.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraEditors.SimpleButton btnImportar;
        private DevExpress.XtraEditors.SimpleButton btnScanear;
        private DevExpress.XtraEditors.LookUpEdit lueTipoDocumentoAdjunto;
        internal DevExpress.XtraGrid.GridControl gcDocs;
        internal DevExpress.XtraGrid.Views.Grid.GridView gvDocumentosAdjuntos;
        internal DevExpress.XtraGrid.Columns.GridColumn Numero;
        private DevExpress.XtraGrid.Columns.GridColumn FKDocumentType;
        internal DevExpress.XtraGrid.Columns.GridColumn Type;
        internal DevExpress.XtraGrid.Columns.GridColumn Nombre;
        internal DevExpress.XtraGrid.Columns.GridColumn Eliminar;
        private DevExpress.XtraGrid.Columns.GridColumn Visualizar;
        internal DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnEliminar1;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnVisualizar;
        private DevExpress.XtraSplashScreen.SplashScreenManager ssForm;
        private DevExpress.XtraEditors.CheckEdit ckeAlimentadorAutomatico;
        private DevExpress.XtraEditors.SimpleButton btnReporteContrato;
        private DevExpress.XtraPdfViewer.PdfViewer pdfViewerDocs;
        private DevExpress.XtraEditors.PictureEdit imgScanPreview;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraPdfViewer.Bars.PdfFindTextBarItem pdfFindTextBarItem1;
        private DevExpress.XtraPdfViewer.Bars.PdfPreviousPageBarItem pdfPreviousPageBarItem1;
        private DevExpress.XtraPdfViewer.Bars.PdfNextPageBarItem pdfNextPageBarItem1;
        private DevExpress.XtraPdfViewer.Bars.PdfSetPageNumberBarItem pdfSetPageNumberBarItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemPageNumberEdit repositoryItemPageNumberEdit1;
        private DevExpress.XtraPdfViewer.Bars.PdfZoomOutBarItem pdfZoomOutBarItem1;
        private DevExpress.XtraPdfViewer.Bars.PdfZoomInBarItem pdfZoomInBarItem1;
        private DevExpress.XtraPdfViewer.Bars.PdfExactZoomListBarSubItem pdfExactZoomListBarSubItem1;
        private DevExpress.XtraPdfViewer.Bars.PdfZoom10CheckItem pdfZoom10CheckItem1;
        private DevExpress.XtraPdfViewer.Bars.PdfZoom25CheckItem pdfZoom25CheckItem1;
        private DevExpress.XtraPdfViewer.Bars.PdfZoom50CheckItem pdfZoom50CheckItem1;
        private DevExpress.XtraPdfViewer.Bars.PdfZoom75CheckItem pdfZoom75CheckItem1;
        private DevExpress.XtraPdfViewer.Bars.PdfZoom100CheckItem pdfZoom100CheckItem1;
        private DevExpress.XtraPdfViewer.Bars.PdfZoom125CheckItem pdfZoom125CheckItem1;
        private DevExpress.XtraPdfViewer.Bars.PdfZoom150CheckItem pdfZoom150CheckItem1;
        private DevExpress.XtraPdfViewer.Bars.PdfZoom200CheckItem pdfZoom200CheckItem1;
        private DevExpress.XtraPdfViewer.Bars.PdfZoom400CheckItem pdfZoom400CheckItem1;
        private DevExpress.XtraPdfViewer.Bars.PdfZoom500CheckItem pdfZoom500CheckItem1;
        private DevExpress.XtraPdfViewer.Bars.PdfSetActualSizeZoomModeCheckItem pdfSetActualSizeZoomModeCheckItem1;
        private DevExpress.XtraPdfViewer.Bars.PdfSetPageLevelZoomModeCheckItem pdfSetPageLevelZoomModeCheckItem1;
        private DevExpress.XtraPdfViewer.Bars.PdfSetFitWidthZoomModeCheckItem pdfSetFitWidthZoomModeCheckItem1;
        private DevExpress.XtraPdfViewer.Bars.PdfSetFitVisibleZoomModeCheckItem pdfSetFitVisibleZoomModeCheckItem1;
        private DevExpress.XtraPdfViewer.Bars.PdfBarController pdfBarController2;
        private DevExpress.XtraPdfViewer.Bars.PdfCommandBar pdfCommandBar1;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnEliminar;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnVisualizar1;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnVisualizar2;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnEliminar2;
        private DevExpress.XtraEditors.RadioGroup rdgTipoContratoJuridico;
    }
}