﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Newtonsoft.Json.Linq;
using Gv.Utilidades;
using static Gv.ExodusBc.UI.ExodusBcBase;

namespace Gv.ExodusBc.UI.Modulos.Certificados
{
    public partial class frmDetalleCertificados : DevExpress.XtraEditors.XtraForm
    {
        public static int REQUEST_ID = 0;
        //Variable para visualizar solicitudes de otras oficinas
        public static int _OficinaId = 0;


        public frmDetalleCertificados()
        {
            InitializeComponent();
            Init();
        }


        void Init()
        {
            try
            {
                SetControles();
                ssForm.ShowWaitForm();
                
                JArray requestById = new JArray();
                requestById = RequestCertificate.GetRequestByIdCertificados(REQUEST_ID, _OficinaId);
                if (requestById.Count > 0 | requestById != null)
                {
                    foreach (JObject element in requestById)
                    {
                        //***Certificado***
                        meTIpoProducto.Text = element["PRODUCTO"].ToString();
                        lblNumeroTicket.Text = element["TICKET"].ToString();
                        lblCertificateOffice.Text = element["LOCALIDAD"].ToString();
                        lblAgenteRegistro.Text = element["AGENTE"].ToString();
                        meEstadoActual.Text = element["ESTADOACTUAL"].ToString();

                        //***Solicitud***
                        lblTipoPersona.Text = element["TIPOPERSONA"].ToString();
                        lblTipoDocumento.Text = element["TIPODOCUMENTO"].ToString();
                        lblNumeroDocumento.Text = element["DOCUMENTO"].ToString();
                        lblNombres.Text = element["NOMBRES"].ToString();
                        lblApellidos.Text = element["APELLIDOS"].ToString(); 
                        lblOficinaCapturo.Text = element["OFICINA"].ToString();
                        lblUsuarioCapturo.Text = element["USUARIO"].ToString();
                        lblFechaCreacion.Text = element["FECHA"].ToString();
                        lblNumeroFactura.Text = element["FACTURA"].ToString();
                        lblFechaPago.Text = element["FECHAPAGO"].ToString();

                        if (element["RequestStatusId"].ToString() == "6") meEstadoActual.ForeColor = Color.DarkGreen;
                    }

                    //***Seguimiento del certificado***
                    JArray requestFollowCrt = new JArray();
                    requestFollowCrt = RequestCertificate.GetSeguimientoCertificados(REQUEST_ID, _OficinaId);
                    if (requestFollowCrt.Count > 0 )
                    {
                        var list = new List<EN.GridSeguimientoCertificados>();
                        foreach (JObject element in requestFollowCrt)
                        {
                            EN.GridSeguimientoCertificados request = new EN.GridSeguimientoCertificados
                            {
                                FECHAREGISTRO = element["FECHAREGISTRO"].ToString(),
                                USUARIO = element["USUARIO"].ToString(),
                                OFICINA = element["OFICINA"].ToString(),
                                ESTACION = element["ESTACION"].ToString(),
                                OBSERVACION = element["ESTADO"].ToString(),
                            };
                            list.Add(request);
                        }
                        gcSeguimientoCertificado.DataSource = list;  
                    }

                    //***Seguimiento de la solicitd***
                    JArray requestFollowRequest = new JArray();
                    requestFollowRequest = RequestCertificate.GetSeguimientoSolicitudes(REQUEST_ID, _OficinaId);
                    if (requestFollowRequest.Count > 0)
                    {
                        var list = new List<EN.GridSeguimientoCertificados>();
                        foreach (JObject element in requestFollowRequest)
                        {
                            EN.GridSeguimientoCertificados request = new EN.GridSeguimientoCertificados
                            {
                                FECHAREGISTRO = element["FECHAREGISTRO"].ToString(),
                                USUARIO = element["USUARIO"].ToString(),
                                OFICINA = element["OFICINA"].ToString(),
                                ESTACION = element["ESTACION"].ToString(),
                                OBSERVACION = element["ESTADO"].ToString(),
                            };
                            list.Add(request);
                        }
                        gcSeguimientoSolcitud.DataSource = list; 
                    }
                    ssForm.CloseWaitForm();
                }
            }
            catch (Exception ex)
            {
                if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                XtraMessageBox.Show("Hubo un error al tratar de cargar algunos valores iniciales.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmDetalleCertificados::Init", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        void SetControles()
        {
            //***Certificado***
            meTIpoProducto.Text = string.Empty;
            lblNumeroTicket.Text = string.Empty;
            lblCertificateOffice.Text = string.Empty;
            lblAgenteRegistro.Text = string.Empty;
            meEstadoActual.Text = string.Empty;

            //***Solicitud***
            lblTipoPersona.Text = string.Empty;
            lblTipoDocumento.Text = string.Empty;
            lblNumeroDocumento.Text = string.Empty;
            lblNombres.Text = string.Empty;
            lblApellidos.Text = string.Empty;
            lblOficinaCapturo.Text = string.Empty;
            lblUsuarioCapturo.Text = string.Empty;
            lblFechaCreacion.Text = string.Empty; 
            lblNumeroFactura.Text = string.Empty;
            lblFechaPago.Text = string.Empty;
        }


    }
}