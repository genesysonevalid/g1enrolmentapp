﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Newtonsoft.Json.Linq;
using static Gv.ExodusBc.UI.ExodusBcBase;
using Newtonsoft.Json;
using DevExpress.XtraEditors.ViewInfo;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid.Views.Grid;
using Gv.Utilidades;
using static Gv.ExodusBc.EN.Request;
using Gv.ExodusBc.LN;
using DevExpress.Utils.Menu;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using System.Net;
using System.IO;
using DevExpress.XtraGrid.Views.Base;

namespace Gv.ExodusBc.UI.Modulos.Certificados
{
    public partial class frmBandeja : DevExpress.XtraEditors.XtraForm
    {
        //***Validaciones Factura Pagada***
        System.Windows.Forms.Timer tmrFacturaPagada = new System.Windows.Forms.Timer();
        public  List<EN.GridRequestBandeja> listaSolicitudes = new List<EN.GridRequestBandeja>();
        DateTime _fechaminima;
        string _NumeroFactura = string.Empty;
        bool resultado = false;
        bool pintarFila = true;



        #region VALORES INICIALES DEL FOCRMULARIO
        public frmBandeja()
        {
            InitializeComponent();
        }

        private void frmBandeja_Load(object sender, EventArgs e)
        {
            try
            {
                ssForm.ShowWaitForm();
                dteFechaInicial.EditValue = DateTime.Now;
                dteFechaFinal.EditValue = DateTime.Now;
                dteFechaInicial.Properties.MaxValue = DateTime.Now;
                dteFechaFinal.Properties.MaxValue = DateTime.Now;

                CargarSolicitudesAprobadas();
                tmrFacturaPagada.Interval = 30000;
                tmrFacturaPagada.Tick += tmrFacturaPagada_Tick;
                tmrFacturaPagada.Start();
                ssForm.CloseWaitForm();
            }
            catch (Exception ex)
            {
                if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                XtraMessageBox.Show("Hubo un error al hacer la búsqueda de la persona. Intente en otro momento.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmBandeja::frmBandeja_Load", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        #endregion

        #region FUNCIONES DE LOS CONTROLES

        private void gvRequest_CustomDrawEmptyForeground(object sender, DevExpress.XtraGrid.Views.Base.CustomDrawEventArgs e)
        {
            GridView view = sender as GridView;
            if (view.RowCount != 0) return;
            StringFormat drawFormat = new StringFormat();
            drawFormat.Alignment = drawFormat.LineAlignment = StringAlignment.Center;
            e.Graphics.DrawString("NO EXISTEN SOLICITUDES PARA ESTE USUARIO.", e.Appearance.Font,
                SystemBrushes.ControlDark, new RectangleF(e.Bounds.X, e.Bounds.Y, e.Bounds.Width, e.Bounds.Height), drawFormat);

            gvRequest.OptionsView.ShowAutoFilterRow = false;
            HideOptionGrid();
        }

        private void btnVerTicket_Click(object sender, EventArgs e)
        {
            try
            {
                int rqId = Convert.ToInt32(gvRequest.GetRowCellValue(gvRequest.FocusedRowHandle, "RequestId"));
                int statusId = Convert.ToInt32(gvRequest.GetRowCellValue(gvRequest.FocusedRowHandle, "IDSTATUS"));
                int statusTrnId = Convert.ToInt32(gvRequest.GetRowCellValue(gvRequest.FocusedRowHandle, "TRANSACCIONSTATUS"));

                if (statusTrnId == (int)TransactionStatus.SOLICITUDRECHAZADA | statusTrnId == (int)TransactionStatus.SOLICITUDENREVISION) return;

                if (rqId != 0 )
                {
                    JArray requestById = new JArray();
                    requestById = RequestCertificate.GetRequestByIdNoTicket(rqId);
                    if (requestById != null)
                    {
                        if (requestById.Count > 0)
                        {
                            foreach (JObject element in requestById)
                            {
                                if (element["TicketNumber"].ToString() != "" && element["NumeroPedido"].ToString() != "")
                                {
                                    XtraMessageBox.Show("Esta solicitud ya tiene asignado número de ticket y ya fué enviada a facturación.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    return;
                                }

                                if (element["TicketNumber"].ToString() != "" && element["NumeroFacturaManual"].ToString() != "")
                                {
                                    XtraMessageBox.Show("Esta solicitud ya tiene asignado número de ticket y la factura ya esta pagada.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    return;
                                }

                                frmEnvioSolicitud.CODIGOPRODUCTO = RequestCertificate.GetItemFromJArray(requestById, "CodigoProducto").ToString();
                                frmEnvioSolicitud.DESCRIPCIONPRODUCTO = RequestCertificate.GetItemFromJArray(requestById, "DescripcionProducto").ToString();
                                frmEnvioSolicitud.NUMEROFACTURA = RequestCertificate.GetItemFromJArray(requestById, "NumeroPedido").ToString();

                                if (RequestCertificate.GetItemFromJArray(requestById, "DocumentNumberLegalPerson").ToString() != "")
                                {
                                    frmEnvioSolicitud.NUMERODOCUMENTO = RequestCertificate.GetItemFromJArray(requestById, "DocumentNumberLegalPerson").ToString();
                                    frmEnvioSolicitud.NOMBRES = RequestCertificate.GetItemFromJArray(requestById, "NameLegalPerson").ToString();
                                    frmEnvioSolicitud.TIPOPERSONA = (int)TypePerson.PRSJURIDICA;
                                }
                                else
                                {
                                    frmEnvioSolicitud.NUMERODOCUMENTO = RequestCertificate.GetItemFromJArray(requestById, "DocumentNumberNaturalPerson").ToString();
                                    frmEnvioSolicitud.NOMBRES = RequestCertificate.GetItemFromJArray(requestById, "Names").ToString().Trim() + " " + RequestCertificate.GetItemFromJArray(requestById, "LastNames").ToString().Trim();
                                    frmEnvioSolicitud.TIPOPERSONA = (int)TypePerson.PRSNATURAL;
                                }

                                frmEnvioSolicitud.CORREO = RequestCertificate.GetItemFromJArray(requestById, "Email").ToString();
                                frmEnvioSolicitud.TELEFONO = RequestCertificate.GetItemFromJArray(requestById, "CellPhonePersonal").ToString();

                                if (element["TicketNumber"].ToString() == "") frmEnvioSolicitud.NUMERODETICKETFINAL = string.Empty;
                                else frmEnvioSolicitud.NUMERODETICKETFINAL = element["TicketNumber"].ToString();

                                if(element["NumeroFacturaManual"].ToString() == "") frmEnvioSolicitud.NUMERODEFACTURAMANUAL = string.Empty;
                                else frmEnvioSolicitud.NUMERODEFACTURAMANUAL = element["NumeroFacturaManual"].ToString();

                            }
                            frmEnvioSolicitud.ARRAYREQUEST = requestById;
                            frmEnvioSolicitud.ESTADOSOLICITUD = statusId;
                            frmEnvioSolicitud.ESTADOTRANSACION = statusTrnId;
                            frmEnvioSolicitud.REQUESTID = rqId;
                            frmEnvioSolicitud.TIPOTRANSACCION = (int)TransactionType.GENERATETICKET;

                            frmEnvioSolicitud frm = new frmEnvioSolicitud();
                            frm.ShowDialog();
                        }
                    }
                }
                else XtraMessageBox.Show("Esta solicitud esta pendiente de generación de ticket.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Hubo un error al tratar de mostrar la información del ticket.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmBandeja::btnVerTicket", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        private void btnVerSolicitud_Click(object sender, EventArgs e)
        {
            try
            {
                int rqId = Convert.ToInt32(gvRequest.GetRowCellValue(gvRequest.FocusedRowHandle, "RequestId"));
                int statusRqId = Convert.ToInt32(gvRequest.GetRowCellValue(gvRequest.FocusedRowHandle, "IDSTATUS"));
                int statusTrnId = Convert.ToInt32(gvRequest.GetRowCellValue(gvRequest.FocusedRowHandle, "TRANSACCIONSTATUS"));

                if (statusTrnId == (int)TransactionStatus.SOLICITUDRECHAZADA | statusTrnId == (int)TransactionStatus.SOLICITUDENREVISION) return;

                JArray factura = new JArray();
                factura = RequestCertificate.GetRequestByStatusIdRequestId((int)TransactionStatus.FACTURAPAGADA, rqId);
                if (factura.Count == 0)
                {
                    XtraMessageBox.Show("No se ha comprobado la creación de la factura, o no se ha generado un número ticket para esta solicitud.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                foreach (JObject element in factura)
                {
                    if ((int)element["FKRequestStatusId"] == (int)RequestStatusList.SOLICITUDCOMPROBADA)
                    {
                        XtraMessageBox.Show("Esta solicitud ya fue enviada, esta en espera de emitir el certificado.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }

                if (rqId != 0)
                {
                    JArray requestById = new JArray();
                    requestById = RequestCertificate.GetRequestByIdNoTicket(rqId);

                    if(requestById != null)
                    {
                        if (requestById.Count > 0)
                        {
                            string numeroFactura = string.Empty;
                            foreach (JObject element in requestById)
                            {
                                frmEnvioSolicitud.CODIGOPRODUCTO = RequestCertificate.GetItemFromJArray(requestById, "CodigoProducto").ToString();
                                frmEnvioSolicitud.DESCRIPCIONPRODUCTO = RequestCertificate.GetItemFromJArray(requestById, "DescripcionProducto").ToString();

                                if(RequestCertificate.GetItemFromJArray(requestById, "NumeroFactura").ToString() != "")
                                    frmEnvioSolicitud.NUMEROFACTURA = RequestCertificate.GetItemFromJArray(requestById, "NumeroFactura").ToString();
                                else frmEnvioSolicitud.NUMEROFACTURA = RequestCertificate.GetItemFromJArray(requestById, "NumeroFacturaManual").ToString();

                                if (RequestCertificate.GetItemFromJArray(requestById, "DocumentNumberLegalPerson").ToString() != "")
                                {
                                    frmEnvioSolicitud.NUMERODOCUMENTO = RequestCertificate.GetItemFromJArray(requestById, "DocumentNumberLegalPerson").ToString();
                                    frmEnvioSolicitud.NOMBRES = RequestCertificate.GetItemFromJArray(requestById, "NameLegalPerson").ToString();
                                    frmEnvioSolicitud.CORREO = RequestCertificate.GetItemFromJArray(requestById, "LegalPersonEmail").ToString();
                                    frmEnvioSolicitud.TELEFONO = RequestCertificate.GetItemFromJArray(requestById, "LegalPersonPhone").ToString();
                                    frmEnvioSolicitud.TIPOPERSONA = (int)TypePerson.PRSJURIDICA;
                                }
                                else
                                {
                                    frmEnvioSolicitud.NUMERODOCUMENTO = RequestCertificate.GetItemFromJArray(requestById, "DocumentNumberNaturalPerson").ToString();
                                    frmEnvioSolicitud.NOMBRES = RequestCertificate.GetItemFromJArray(requestById, "Names").ToString().Trim() + " " + RequestCertificate.GetItemFromJArray(requestById, "LastNames").ToString().Trim();
                                    frmEnvioSolicitud.CORREO = RequestCertificate.GetItemFromJArray(requestById, "Email").ToString();
                                    frmEnvioSolicitud.TELEFONO = RequestCertificate.GetItemFromJArray(requestById, "CellPhonePersonal").ToString();
                                    frmEnvioSolicitud.TIPOPERSONA = (int)TypePerson.PRSNATURAL;
                                }
                            }
                            frmEnvioSolicitud.ARRAYREQUEST = requestById;
                            frmEnvioSolicitud.ESTADOSOLICITUD = statusRqId;
                            frmEnvioSolicitud.ESTADOTRANSACION = statusTrnId;
                            frmEnvioSolicitud.REQUESTID = rqId;
                            frmEnvioSolicitud.TIPOTRANSACCION = (int)TransactionType.GENERATEREQUEST;
                            frmEnvioSolicitud.NUMERODETICKETFINAL = RequestCertificate.GetItemFromJArray(requestById, "TicketNumber").ToString();

                            frmEnvioSolicitud frm = new frmEnvioSolicitud();
                            frm.ShowDialog();
                        }
                    }
                }
                else XtraMessageBox.Show("Esta solicitud esta pendiente de generación de ticket.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Hubo un error al tratar de mostrar la información del ticket.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmBandeja::btnVerTicket", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        private void btnVerRechazo_Click(object sender, EventArgs e)
        {
            try
            {
                int rqId = Convert.ToInt32(gvRequest.GetRowCellValue(gvRequest.FocusedRowHandle, "RequestId"));
                int statusId = Convert.ToInt32(gvRequest.GetRowCellValue(gvRequest.FocusedRowHandle, "IDSTATUS"));
                int statusTrnId = Convert.ToInt32(gvRequest.GetRowCellValue(gvRequest.FocusedRowHandle, "TRANSACCIONSTATUS"));

                if (statusTrnId != (int)TransactionStatus.SOLICITUDRECHAZADA) return;

                if(rqId > 0)
                {
                    JArray rq = new JArray();
                    rq = RequestCertificate.GetRequestByRejection(rqId);
                    if (rq != null)
                    {
                        if (rq.Count > 0)
                        {
                            foreach (JObject element in rq)
                            {
                                if (RequestCertificate.GetItemFromJArray(rq, "DOCUMENTOJURIDICA").ToString() != "") 
                                {
                                    frmDetalleRechazo.NUMERODOCUMENTO = element["DOCUMENTOJURIDICA"].ToString(); 
                                    frmDetalleRechazo.NOMBRES = element["NOMBREJURIDICA"].ToString();
                                }
                                else
                                {
                                    frmDetalleRechazo.NUMERODOCUMENTO = element["DOCUMENTONATURAL"].ToString();
                                    frmDetalleRechazo.NOMBRES = element["NOMBRES"].ToString().Trim();
                                    frmDetalleRechazo.APELLIDOS = element["APELLIDOS"].ToString().Trim();
                                }

                                frmDetalleRechazo.TIPOPERSONA = element["TIPOPERSONA"].ToString();
                                frmDetalleRechazo.PRODUCTO = element["PRODUCTO"].ToString();
                                frmDetalleRechazo.TIPODOCUMENTO = element["TIPODOCUMENTO"].ToString();
                                frmDetalleRechazo.OFICINAID = Convert.ToInt32(element["FKOfficeId"].ToString());
                                frmDetalleRechazo.REQUESTID = rqId;
                            }

                            frmDetalleRechazo frm = new frmDetalleRechazo();
                            frm.ShowDialog();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Hubo un error al tratar de mostrar la información de la solicitid.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmBandeja::btnVerRechazo_Click", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        private void btnVerRevision_Click(object sender, EventArgs e)
        {
            try
            {
                int rqId = Convert.ToInt32(gvRequest.GetRowCellValue(gvRequest.FocusedRowHandle, "RequestId"));
                int statusTrnId = Convert.ToInt32(gvRequest.GetRowCellValue(gvRequest.FocusedRowHandle, "TRANSACCIONSTATUS"));

                if (statusTrnId != (int)TransactionStatus.SOLICITUDENREVISION) return;

                if (rqId > 0)
                {
                    JArray rq = new JArray();
                    rq = RequestCertificate.GetRequestByReview(rqId);
                    if (rq != null)
                    {
                        if (rq.Count > 0)
                        {
                            foreach (JObject element in rq)
                            {
                                if (RequestCertificate.GetItemFromJArray(rq, "DOCUMENTOJURIDICA").ToString() != "") 
                                {
                                    frmDetalleRevision.NUMERODOCUMENTO = element["DOCUMENTOJURIDICA"].ToString();
                                    frmDetalleRevision.NOMBRES = element["NOMBREJURIDICA"].ToString();
                                }
                                else
                                {
                                    frmDetalleRevision.NUMERODOCUMENTO = element["DOCUMENTONATURAL"].ToString();
                                    frmDetalleRevision.NOMBRES = element["NOMBRES"].ToString().Trim();
                                    frmDetalleRevision.APELLIDOS = element["APELLIDOS"].ToString().Trim();
                                }

                                frmDetalleRevision.TIPOPERSONA = element["TIPOPERSONA"].ToString();
                                frmDetalleRevision.PRODUCTO = element["PRODUCTO"].ToString();
                                frmDetalleRevision.TIPODOCUMENTO = element["TIPODOCUMENTO"].ToString();
                                frmDetalleRevision.OFICINAID = Convert.ToInt32(element["FKOfficeId"].ToString());
                                frmDetalleRevision.REQUESTID = rqId;
                            }

                            frmDetalleRevision frm = new frmDetalleRevision();
                            frm.ShowDialog();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Hubo un error al tratar de mostrar la información de la solicitid.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmBandeja::btnVerRevision_Click", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        private void btnRefrescar_Click(object sender, EventArgs e)
        {
            try
            {
                ssForm.ShowWaitForm();
                CargarSolicitudesAprobadas();
                ConsultarEstadosValid();
                ssForm.CloseWaitForm();
            }
            catch (Exception ex)
            {
                if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                XtraMessageBox.Show("Hubo un error al hacer la búsqueda de la personas. Intente en otro momento.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmBandeja::btnRefrescar_Click", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
            
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            frmMain2 openForm = (frmMain2)Application.OpenForms["frmMain2"];
            openForm.OpenForms("frmBandeja");
            CerrarBandejaSolicitudes();
            btnVerTicket.ContextImageOptions.Image = Properties.Resources.ic_requestnone_32x32;
        }

        private void frmBandeja_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                tmrFacturaPagada.Stop();
            }
            catch (Exception ex)
            {
                Log.InsertarLog(Log.ErrorType.Error, "frmBandeja::frmBandeja_FormClosing()", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }

        }

        #endregion

        #region METODOS Y PROCEDIMIENTOS

        void tmrFacturaPagada_Tick(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < gvRequest.DataRowCount; i++)
                {
                    int t = Convert.ToInt32(gvRequest.GetRowCellValue(i, "TRANSACCIONSTATUS").ToString());
                    string numeroTicket = gvRequest.GetRowCellValue(i, "TICKET").ToString();

                    //Consultar si la factura es automatica
                    if (t == Convert.ToInt32(TransactionStatus.FACTURAENVIADA))
                    {
                        var consultaNumeroPedido = RequestCertificate.GetNumeroPedidoByTicket(numeroTicket);
                        if (consultaNumeroPedido.Count > 0 | consultaNumeroPedido != null)
                        {
                            foreach (JObject element in consultaNumeroPedido)
                            {
                                string numeroPedido = RequestCertificate.GetItemFromJArray(consultaNumeroPedido, "NumeroPedido").ToString();
                                int requestId = Convert.ToInt32(RequestCertificate.GetItemFromJArray(consultaNumeroPedido, "RequestId"));
                                if (numeroPedido != "")
                                {
                                    //**Consultar si la factura fue pagada en GP**
                                    GP.DatosGP wsFunction = new GP.DatosGP();
                                    DataSet ds = new DataSet();
                                    ds = wsFunction.ConsultaFacturaPagada(numeroPedido);

                                    if (ds != null)
                                    {
                                        if (ds.Tables.Count > 0)
                                        {
                                            if (ds.Tables[0].Rows.Count != 0)
                                            {
                                                string numeroFactura = string.Empty;
                                                foreach (DataRow dr in ds.Tables[0].Rows)
                                                    if (dr["Factura"].ToString() != string.Empty) numeroFactura = dr["Factura"].ToString();

                                                if (RequestCertificate.UpdateRequestStatusFacturaPagada(requestId, (int)TransactionStatus.FACTURAPAGADA, numeroFactura))
                                                {
                                                    //**GUARDAR SEGUIMIENTO**
                                                    EN.RequestFollow rqf = new EN.RequestFollow();
                                                    rqf.Observation = "FACTURA PAGADA";
                                                    rqf.FKRequestId = requestId;
                                                    rqf.FKTransactionEstatusId = (int)TransactionStatus.FACTURAPAGADA;
                                                    rqf.FKCreateUserId = Convert.ToInt32(VariablesGlobales.USER_ID);
                                                    rqf.FKOfficeId = Convert.ToInt32(VariablesGlobales.OFFICE_ID);
                                                    rqf.FKTransactionStatusPrevious = (int)TransactionStatus.FACTURAENVIADA;
                                                    rqf.FKWorkstationId = (int)VariablesGlobales.WS_ID;
                                                    var insertRqFallow = RequestCertificate.InsertNewRequestFollow(rqf);
                                                    if (!insertRqFallow)
                                                    {
                                                        Log.InsertarLog(Log.ErrorType.Error, "frmBandeja::tmrFacturaPagada_Tick", "Error al guardar el seguimiento", VariablesGlobales.PathDataLog);
                                                        return;
                                                    }
                                                }
                                                else
                                                {
                                                    Log.InsertarLog(Log.ErrorType.Error, "frmBandeja::tmrFacturaPagada_Tick", "Error al editar el estado de la factura", VariablesGlobales.PathDataLog);
                                                    return;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                CargarSolicitudesAprobadas();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Hubo un error al consultar el estado de las facturas en GP.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmBandeja::tmrFacturaPagada_Tick", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
            
           
            gvRequest.UpdateCurrentRow();
        }

        public void CargarSolicitudesAprobadas()
        {
            try
            {
                if (listaSolicitudes.Count > 0) listaSolicitudes.Clear();
                JArray requestTray = new JArray();
                if (VariablesGlobales.USER_ID == 1) requestTray = RequestCertificate.GetRequestBandejaTodas(Convert.ToDateTime(dteFechaInicial.EditValue), Convert.ToDateTime(dteFechaFinal.EditValue));
                else requestTray = RequestCertificate.GetRequestBandeja(VariablesGlobales.USER_ID, VariablesGlobales.OFFICE_ID, Convert.ToDateTime(dteFechaInicial.EditValue), Convert.ToDateTime(dteFechaFinal.EditValue));

                gcRequest.DataSource = null;

                if(requestTray != null)
                {
                    if (requestTray.Count > 0)
                    {
                        var list = new List<EN.GridRequestBandeja>();
                        foreach (JObject element in requestTray)
                        {
                            var statusrq = element["FKRequestStatusId"];
                            EN.GridRequestBandeja request = new EN.GridRequestBandeja
                            {
                                TICKET = element["TICKET"].ToString(),
                                PRODUCTO = element["PRODUCTO"].ToString(),
                                NOMBRES = element["NOMBRES"].ToString(),
                                APELLIDOS = element["APELLIDOS"].ToString(),
                                SEGUIMIENTO = element["TRANSACCION"].ToString(),
                                IDSTATUS = (statusrq.Type != JTokenType.Null) ? Convert.ToInt32(element["FKRequestStatusId"].ToString()) : 0,
                                TRANSACCIONSTATUS = Convert.ToInt32(element["FKTransactionStatusId"].ToString()),
                                RequestId = Convert.ToInt32(element["RequestId"].ToString()),
                                FECHACREACION = Convert.ToDateTime(element["CreationDate"].ToString())
                            };

                            if (request.TRANSACCIONSTATUS < (int)TransactionStatus.FACTURAENVIADA) request.SEGUIMIENTO = element["TRANSACCION"].ToString();
                            else request.FACTURA = element["TRANSACCION"].ToString();

                            if (request.IDSTATUS > 0) request.SEGUIMIENTO = element["SOLICITUD"].ToString();

                            if (element["DocumentNumberLegalPerson"].ToString() as string != string.Empty && element["NameLegalPerson"].ToString() as string != string.Empty)
                            {
                                request.NOMBRES = element["NameLegalPerson"].ToString();
                                request.APELLIDOS = string.Empty;
                            }
                            list.Add(request);
                            listaSolicitudes.Add(request);
                        }
                        gcRequest.DataSource = list;
                        UpdateStatusRequest();
                    }
                    UpdateStatusRequest();
                }
            }
            catch (Exception ex)
            {
                Log.InsertarLog(Log.ErrorType.Error, "frmBandeja::CargarSolicitudesAprobadas", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        void UpdateStatusRequest()
        {
            for (int i = 0; i < gvRequest.DataRowCount; i++)
            {
                //Estado del certificado
                int statusRequest = Convert.ToInt32(gvRequest.GetRowCellValue(i, "IDSTATUS"));
                if (statusRequest == (int)TransactionStatus.SOLICITUDAPROBADA) btnVerTicket.Buttons[0].Image = Properties.Resources.ic_ticket2none_32x32;
                else if (statusRequest == (int)RequestStatusList.PENDIENTESOLICITUD) btnVerTicket.Buttons[0].Image = Properties.Resources.ic_ticket2ok_32x32;
                else if (statusRequest == (int)RequestStatusList.SOLICITUDCOMPROBADA) btnVerSolicitud.Buttons[0].Image = Properties.Resources.ic_ticketok_32x32;

                if (statusRequest == (int)RequestStatusList.CERTIFICADOEMITIDO) btnVerSolicitud.Buttons[0].Image = Properties.Resources.ic_requesok_32x32;
            }
            HideOptionGrid();
        }

        void HideOptionGrid()
        {
            gvRequest.Columns[6].Visible = false;
            gvRequest.Columns[7].Visible = false;
            gvRequest.Columns[8].Visible = false;
        }

        public void CerrarBandejaSolicitudes()
        {
            Close();
        }


        #endregion

     
        private void gvRequest_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            if (!e.HitInfo.InDataRow) return;
            DevExpress.Utils.ImageCollection imageList = DevExpress.Utils.Controls.ImageHelper.CreateImageCollectionFromResources("DevExpress.XtraEditors.Images.TextEditMenu.png", typeof(TextEdit).Assembly, new Size(16, 16), Color.Empty);
            DXMenuItem item = new DXMenuItem("Copiar", item_Click, imageList.Images[2]);
            item.Tag = e.HitInfo;
            e.Menu.Items.Add(item);
        }

        void item_Click(object sender, EventArgs e)
        {
            DXMenuItem item = sender as DXMenuItem;
            GridHitInfo info = item.Tag as GridHitInfo;
            Clipboard.SetText(gvRequest.GetRowCellDisplayText(info.RowHandle, info.Column));
        }

        public void ConsultarEstadosValid()
        {
            DateTime _fechaSeleccionada = Utilities.getFechaActual();
            foreach (var item in listaSolicitudes)
            {
                if (item.FECHACREACION <= _fechaSeleccionada)
                {
                    _fechaminima = item.FECHACREACION;
                    _fechaSeleccionada = item.FECHACREACION;
                }
            }

            DateTime fechainicio = Utilities.getFechaActual().AddDays(-29);
            DateTime fechafinal = Utilities.getFechaActual();

            JObject json = new JObject();
            string fi = fechainicio.ToString("yyyy-MM-dd'T'HH:mm:sss'Z'");
            string ff = fechafinal.ToString("yyyy-MM-dd'T'HH:mm:sss'Z'");
            JProperty lenguage = new JProperty("language", VariablesGlobales.IDIOMA.ToLower());
            JProperty dateBegin = new JProperty("dateBegin", fi);
            JProperty dateEnd = new JProperty("dateEnd", ff);
            json.Add(lenguage);
            json.Add(dateBegin);
            json.Add(dateEnd);

            //**ARCHIVO DE SALIDA**
            string jsonOut = JsonConvert.SerializeObject(json, Formatting.None);
            try
            {
                string result = string.Empty;
                result = ResponseRequestValid(jsonOut);
                if (result != string.Empty)
                {
                    foreach (var solicitud in listaSolicitudes)
                    {
                        JObject jObj = (JObject)JsonConvert.DeserializeObject(result);
                        foreach (var rq in jObj)
                        {
                            if (rq.Key == "listUpdatedGlobalRequestStatus")
                            {
                                if (rq.Value.Count() > 0)
                                {
                                    foreach (JObject element in rq.Value)
                                    {
                                        foreach (var item in element)
                                        {
                                            if (item.Key == "ticket")
                                            {
                                                if (item.Value.ToString() == solicitud.TICKET)
                                                {
                                                    if (element["requestStatus"] != null)
                                                    {
                                                        var request = JObject.FromObject(element["requestStatus"]);
                                                        int id = int.Parse(request.GetValue("idRequestStatus").ToString());
                                                        string name = request.GetValue("name").ToString();
                                                        RequestCertificate.UpdateStatusRequestValid(id, solicitud.RequestId);
                                                        if(id == (int)RequestStatusList.CERTIFICADOINSTALADO & id != solicitud.IDSTATUS)
                                                            GuardarSeguimientoSolicitud((int)TransactionStatus.SOLICITUDFINALIZADA, solicitud.RequestId, solicitud.TRANSACCIONSTATUS);

                                                        if (solicitud.IDSTATUS != id)
                                                            GuardarSeguimientoCertificado(id, solicitud.RequestId, solicitud.IDSTATUS, name);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    CargarSolicitudesAprobadas();
                }
            }
            catch (Exception ex)
            {
                Log.InsertarLog(Log.ErrorType.Error, "frmBandeja::ConsutarEstadosValid", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        void GuardarSeguimientoSolicitud(int statusId, int rqId, int statusPrevious)
        {
            //**GUARDAR SEGUIMIENTO SOLICITUD**
            EN.RequestFollow rqf = new EN.RequestFollow();
            rqf.Observation = "TRANSACCION FINALIZADA";
            rqf.FKRequestId = rqId;
            rqf.FKTransactionEstatusId = statusId;
            rqf.FKCreateUserId = Convert.ToInt32(VariablesGlobales.USER_ID);
            rqf.FKOfficeId = Convert.ToInt32(VariablesGlobales.OFFICE_ID);
            rqf.FKTransactionStatusPrevious = statusPrevious;
            rqf.FKWorkstationId = Convert.ToInt32(VariablesGlobales.WS_ID);
            var insertRqFallow = RequestCertificate.InsertNewRequestFollow(rqf);
            if (!insertRqFallow)
            {
                Log.InsertarLog(Log.ErrorType.Error, "frmBandeja::GuardarSeguimientoSolicitud", "Error al guardar el seguimiento", VariablesGlobales.PathDataLog);
            }
        }

        void GuardarSeguimientoCertificado(int status, int rqId, int statusPrevious, string descripcion)
        {
            //**GUARDAR SEGUIMIENTO CERTIFICADO**
            EN.RequestFollowValid rqf = new EN.RequestFollowValid();
            rqf.Observation = descripcion;
            rqf.FKRequestId = rqId;
            rqf.FKRequestStatusId = status;
            rqf.FKCreateUserId = Convert.ToInt32(VariablesGlobales.USER_ID);
            rqf.FKOfficeId = Convert.ToInt32(VariablesGlobales.OFFICE_ID);
            rqf.FKRequestStatusPreviousId = statusPrevious;
            rqf.FKWorkstationId = Convert.ToInt32(VariablesGlobales.WS_ID);
            var insertRqFallow = RequestCertificate.InsertNewRequestFollowValid(rqf);
            if (!insertRqFallow)
            {
                Log.InsertarLog(Log.ErrorType.Error, "frmBandeja::GuardarSeguimientoCertificad0", "Error al guardar el seguimiento.", VariablesGlobales.PathDataLog);
            }
        }

        public string ResponseRequestValid(string json)
        {
            string result = string.Empty;
            string url = string.Empty;
            url = "http://ar.tecnisign.net/ra-ws-rest/updatedGlobalRequestStatus";

            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.ProtocolVersion = HttpVersion.Version10;
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                streamWriter.Write(json);

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();

            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                result = streamReader.ReadToEnd();

            return result;
        }

        private void btnActualizarEstadosValid_Click(object sender, EventArgs e)
        {
            frmBandejaCertificados frm = new frmBandejaCertificados();
            frm.ShowDialog();
        }

        private List<string> Contains(List<string> list1, List<string> list2)
        {
            List<string> result = new List<string>();

            result.AddRange(list1.Except(list2, StringComparer.OrdinalIgnoreCase));
            result.AddRange(list2.Except(list1, StringComparer.OrdinalIgnoreCase));

            return result;
        }

        private void gvRequest_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            try
            {
                if (e.RowHandle >= 0)
                {
                    int statusTrn = Convert.ToInt32(gvRequest.GetRowCellValue(e.RowHandle, "TRANSACCIONSTATUS"));
                    if (statusTrn == (int)TransactionStatus.SOLICITUDRECHAZADA) e.Appearance.ForeColor = Color.FromArgb(146, 43, 33);
                    else if(statusTrn == (int)TransactionStatus.SOLICITUDENREVISION) e.Appearance.ForeColor = Color.FromArgb(179, 131, 52);
                    else Color.FromArgb(55, 71, 79);

                    e.Appearance.Font = new Font("Segoe UI Semibold", 10, FontStyle.Bold);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Hubo un error al tratar de visualizar las solicitudes.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmBandeja::gvRequest_RowCellStyle", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        private void btnBuscarSolicitudes_Click(object sender, EventArgs e)
        {
            if (dteFechaInicial.EditValue.ToString() == string.Empty)
                XtraMessageBox.Show("La fecha inicial es obligatoria.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            else if (dteFechaFinal.EditValue.ToString() == string.Empty)
                XtraMessageBox.Show("La fecha final es obligatoria.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            else if (Convert.ToDateTime(dteFechaInicial.EditValue).Date > Convert.ToDateTime(dteFechaFinal.EditValue).Date)
                XtraMessageBox.Show("La fecha de inicio no puede ser mayor a la fecha final.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            else
            {
                try
                {
                    ssForm.ShowWaitForm();
                    CargarSolicitudesAprobadas();
                    ssForm.CloseWaitForm();
                }
                catch (Exception ex)
                {
                    if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                    XtraMessageBox.Show("Hubo un error al tratar de visualizar los certificados.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Log.InsertarLog(Log.ErrorType.Error, "frmBandejaSeguimientoCertificados::btnBuscarSolicitudes_Click", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                }
            }
        }

        
    }
}