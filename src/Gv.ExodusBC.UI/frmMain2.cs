﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using System.Drawing;
using DevExpress.XtraEditors;
using Newtonsoft.Json.Linq;
using Gv.ExodusBc.EN;
using Gv.ExodusBc.UI;
using System.Collections.Generic;
using Gv.ExodusBc.LN;
using System.Timers;
using DevExpress.LookAndFeel;
using System.ServiceProcess;
using System.Runtime.InteropServices;
using System.Globalization;
using Gv.ExodusBc.UI.Modulos.InspeccionPrimaria;
using DevExpress.Utils;
using AForge.Video.DirectShow;
using Gv.ExodusBc.UI.Properties;
using Gv.Logitech.SDK;
using Gv.ExodusBc.UI.Modulos.Certificados;
using Gv.ExodusBc.UI.Modulos.Registro;
using System.Diagnostics;
using Gv.Utilidades;
using static Gv.ExodusBc.UI.ExodusBcBase;
using System.IO;
using Gv.ExodusBc.UI.Modulos.Mantenimientos;

namespace Gv.ExodusBc.UI
{
    public partial class frmMain2 : XtraForm
    {
        public static bool ActualizarEstdistica = true;
        delegate void SetVisibleControlCallBack(bool visible, SimpleButton ctr, bool enabled);
        delegate void SetVisibleControlTileItem(bool visible, TileItem ctr);
        delegate void SetTextLabelModoTrabajoCallBack(string text, LabelControl labelEdit);
        delegate void SetTextLabelCallBack(string text, LabelControl lbl);
        delegate void SetImageLabelCallBack(Image img, LabelControl lbl);

        public static Panel pnlActualizacion;
        public static Panel pnlActualizacionBienvenida;

        private Form LastFormActive = null;

        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);

        private WebReference.ExodusBcService service = new WebReference.ExodusBcService();
        //public static Utilidades.MRZ.OCR640AccessIS AccessISOCR = new Utilidades.MRZ.OCR640AccessIS();
        public static CS500E cs500e = new CS500E();
        public static UserLookAndFeel lookAndFeelMsgBox;
        public static bool confirmToClose = true;
        public static AForge.Video.DirectShow.FilterInfoCollection DispositivosDeVideo;
        public static List<CamarasDisponibles> lstCamaras;
        bool ExisteCamara;

        private string UserName = "";
        private string Hostname = string.Empty;

        private bool isWindowMaximized;
        private bool caidaConexion;
        Size _normalWindowSize;
        Point _normalWindowLocation = Point.Empty;
        private System.Timers.Timer tmrUpdateLiveTime = new System.Timers.Timer();
        private System.Timers.Timer tmrInactivityCheck = new System.Timers.Timer();

        EN.WorkStations workstation = new EN.WorkStations();
        EN.Office office = new EN.Office();
        OfficeGP OfficeGP = new OfficeGP();
        EN.Users users = new EN.Users();
        bool checkStatusWorking;

        public static string frmNombre = string.Empty;
        public static Form frmForm;
        public static bool cerrarForm;

        public void OpenForms(string form = "")
        {
            if (form == "frmCertificados") SetEstiloBotonesInactivos(btnCertificados);
            if (form == "frmInsPrimariaEntrada") SetEstiloBotonesInactivos(btnEnrolamiento);
            if (form == "frmBandeja") SetEstiloBotonesInactivos(btnBandeja);
            if (form == "frmBandejaSeguimientoCertificados") SetEstiloBotonesInactivos(btnBandejaCertificados); 

            pnlBienvenida.Visible = true;
        }

        public frmMain2()
        {
            InitializeComponent();
            Icon = Program.AppIco;
            ShowIcon = false;
            //lblVersionApp.Text = "Version: " + VariablesGlobales.AppVersion;
            lblVersionDescripcion.Text = VariablesGlobales.AppVersion;
            lookAndFeelMsgBox = new UserLookAndFeel(this);
            lookAndFeelMsgBox.SkinName = VariablesGlobales.MessageBoxDevExpressStyle;
            lookAndFeelMsgBox.Style = LookAndFeelStyle.Skin;
            lookAndFeelMsgBox.UseDefaultLookAndFeel = false;

            UserName = VariablesGlobales.USER_NAME;
            Hostname = VariablesGlobales.HOST_NAME;

            if (VariablesGlobales.MODOTRABAJO.Equals("ON"))
            {
                lblEstadoConexion.Appearance.Image = Resources.enlineaN;
                lblEstadoConexion.Text = "En línea";
                lblEstadoConexion.ForeColor = Color.FromArgb(46, 204, 113);
                lblIPDescripcion.Text = Utilities.getLocalIP();
            }
            else
            {
                lblEstadoConexion.Appearance.Image = Resources.fueradelineaN;
                lblEstadoConexion.Text = "Fuera de línea";
                lblEstadoConexion.ForeColor = Color.FromArgb(241, 1, 1);
                lblIP.Text = string.Concat("IP: ", Utilities.getLocalIP());
            }

            Init();

            try
            {
                ssForm.ShowWaitForm();
                //*Verificarmos la conexión y los servicios al cargar el sistema, luego lo estará monitoreando el timer*           
                CheckStatus(); 
                ssForm.CloseWaitForm();
            }
            catch (Exception ex)
            {
                if (ssForm.IsSplashFormVisible) ssForm.CloseWaitForm();
                XtraMessageBox.Show(lookAndFeelMsgBox, "Imposible iniciar sesión, favor comuniquese con el Administrador del Sistema.", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Log.InsertarLog(Log.ErrorType.Error, "frmMain::init", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }

            //*Timer para comprobar el estatus de los servicios*
            System.Timers.Timer aTimer = new System.Timers.Timer();
            aTimer.Elapsed += new ElapsedEventHandler(CheckStatusTimer);
            aTimer.Interval = 60000;
            aTimer.Enabled = true;
            aTimer.Start();
        }

        private void CheckStatusTimer(object source, ElapsedEventArgs e)
        {
            if (checkStatusWorking) return;

            CheckStatus();
        }

        private void CheckStatus()
        {
            checkStatusWorking = true;
            try
            {
                bool GenesysOneStatus = (LoginWebService.getStatusConecction() == string.Empty) ? false : true;
                if (GenesysOneStatus)
                {
                    VariablesGlobales.MODOTRABAJO = "ON";
                    SetLabelModoTrabajoText("En línea", lblEstadoConexion);
                    if (caidaConexion)
                    {
                        caidaConexion = false;
                        GetAccesos();
                    }
                }
                else
                {
                    VariablesGlobales.MODOTRABAJO = "OFF";
                    SetLabelModoTrabajoText("Fuera de línea", lblEstadoConexion);
                    caidaConexion = true;
                    GetAccesos();
                }

                //***Revisar servicio de envío de transacciones***

                //**Envio de transacciones 1 = directo a BD, 0 = servicio de windows**
                if(VariablesGlobales.ENVIOTRANSACCION == 0)
                {
                    try
                    {
                        ServiceController sc = new ServiceController("GenesysOneSendValid");
                        if (sc != null && sc.Status == ServiceControllerStatus.Stopped)
                        {
                            SetImageLabel(Resources.ic_downloadred16x16, lblServicioEnvio);
                            toolTip1.SetToolTip(lblServicioEnvio, "Servicio de envio de transacciones detenido");
                            Log.InsertarLog(Log.ErrorType.Informacion, "frmMain2::CheckStatus", "Caída del servicio de Envío", VariablesGlobales.PathDataLog);
                        }
                        else
                        {
                            SetImageLabel(Resources.ic_uploadgreen16x16, lblServicioEnvio);
                            toolTip1.SetToolTip(lblServicioEnvio, "Servicio  de envio de transacciones activo");
                        }

                        sc.Close();
                    }
                    catch (Exception)
                    {
                        SetImageLabel(Resources.ic_downloadred16x16, lblServicioEnvio);
                        Log.InsertarLog(Log.ErrorType.Informacion, "frmMain2::CheckStatus", "Caída del servicio de Envío", VariablesGlobales.PathDataLog);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InsertarLog(Log.ErrorType.Error, "frmMain::CheckStatus", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
            finally
            {
                checkStatusWorking = false;
            }

        }
      
        private void frmMain2_Load(object sender, EventArgs e)
        {
            pnlActualizacion = pnlContenedor;
            pnlActualizacionBienvenida = pnlBienvenida;
           
            
            var rol = USERROLESLN.GetById(VariablesGlobales.USER_ROL_ID);
            if (rol != null) lblRol.Text = rol.Name;

            var user = USERSLN.GetUserByUserName(UserName);
            if (user != null) lblUsuario.Text = new CultureInfo("en-US", false).TextInfo.ToTitleCase(user.Name);

            
        }

        private void CargarCamara(AForge.Video.DirectShow.FilterInfoCollection Dispositivos)
        {
            lstCamaras = new List<CamarasDisponibles>();
            for (int item = 0; item <= Dispositivos.Count - 1; item++)
            {
                CamarasDisponibles camara = new CamarasDisponibles() { Index = item, Nombre = Dispositivos[item].Name };
                lstCamaras.Add(camara);
            }
            foreach (var camara in lstCamaras)
            {
                if (camara.Nombre.ToString().Contains(VariablesGlobales.NOMBRECAMARA))
                    ExisteCamara = true;
                else
                    ExisteCamara = false;
            }
        }

        private void Init()
        {
            try
            {
                DateTime fechaHoraMaquina = DateTime.Now;
                lblDia.Text = fechaHoraMaquina.ToString("dddd").ToUpper();
                lblDiaNumero.Text = fechaHoraMaquina.ToString("dd");
                lblMes.Text = fechaHoraMaquina.ToString("MMMM yyyy").ToUpper();
                Text = "GenesysOne - Enrollment System v " + ExodusBcBase.Helper.GetVersion() + " Powered by Grupo Visión";


                users = LN.USERSLN.GetUserByUserName(UserName);
                if (users != null && workstation != null)
                {
                    workstation = LN.WORKSTATIONSLN.GeWorkstationbyHostName(Hostname);
                    office = LN.OFFICELN.GetOfficebyOfficeCode(workstation.FKOfficeId);
                    OfficeGP = OFFICEGPLN.GetOfficegpbyOfficeCode(Convert.ToInt32(office.OfficeId));
                    VariablesGlobales.OFFICEIDGP = (OfficeGP.CodeOffice != string.Empty) ? OfficeGP.CodeOffice : string.Empty;
                    VariablesGlobales.OFFICECODEGP = (OfficeGP.Code != string.Empty) ? OfficeGP.Code : string.Empty;
                    VariablesGlobales.OFFICEDESCRIPTIONGP = (OfficeGP.Description != string.Empty) ? OfficeGP.Description : string.Empty;

                    lblEstacionDescripcion.Text = Hostname;
                    lblDelegacion.Text = office.Name.ToUpper();
                    VariablesGlobales.USER_ID = Convert.ToInt32(users.UserId);
                    VariablesGlobales.OFFICENAME = office.Name;
                    VariablesGlobales.USER_ID = Convert.ToInt32(users.UserId);
                    VariablesGlobales.USER_ROL_ID = USERSLN.GetAccessDatebyUserId(users.UserId).FKUserRolesId;
                    VariablesGlobales.WS_ID = workstation.WorkStationsId;
                    VariablesGlobales.HOST_NAME = Hostname;
                    VariablesGlobales.OFFICE_ID = Convert.ToInt32(office.OfficeId);
                    VariablesGlobales.OFFICE_CERTIFICATE_ID = Convert.ToInt32(office.FKCertificateOfficeId);
                    VariablesGlobales.OFFICE_REGISTRATIONAUTHORITY_ID = Convert.ToInt32(office.FKRegistrationAuthorityId);
                    VariablesGlobales.OFFICE_CODE = Convert.ToString(office.OfficeId);
                    VariablesGlobales.OFFICEZIPCODE = office.FKZipCodeId.ToString();

                    string certOffficeCode = CERTIFICATEOFFICELN.GetCodeCertOffice(Convert.ToInt32(office.FKCertificateOfficeId));
                    if (certOffficeCode != string.Empty) VariablesGlobales.CERTOFFICE = certOffficeCode;

                    string rACode = REGISTRATIONAUTHORITYLN.GetCodeRA(Convert.ToInt32(office.FKRegistrationAuthorityId));
                    if (rACode != string.Empty) VariablesGlobales.RGAURORITY = rACode;

                    var agAutorizado = REGISTRATIONAGENTLN.GetCodeAgent(Convert.ToInt32(VariablesGlobales.USER_ID));
                    if (agAutorizado != null)
                    {
                        VariablesGlobales.AGAUTORITY = agAutorizado.Code;
                        VariablesGlobales.REGISTRATION_AGENT_ID = Convert.ToInt32(agAutorizado.RegistrationAgentId);
                    }
                    GetAccesos();
                    LimpiarCarpetas();
                }
                else
                {
                    XtraMessageBox.Show(lookAndFeelMsgBox, "Imposible iniciar sesión, favor comuniquese con el Administrador del Sistema.", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Log.InsertarLog(Log.ErrorType.Informacion, "frmMain::init", "Objeto users=" + users + ", Objeto workstation=" + workstation, VariablesGlobales.PathDataLog);
                    Dispose();
                    Application.Exit();
                }

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(lookAndFeelMsgBox, "Error obteniendo datos: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.InsertarLog(Log.ErrorType.Error, "frmMain::init", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        void LimpiarCarpetas()
        {
            try
            {
                DirectoryInfo system = new DirectoryInfo(VariablesGlobales.PathDataSystem);

                //***Eliminar Pdfs***
                foreach (FileInfo file in system.GetFiles())
                    file.Delete();
            }
            catch (Exception ex)
            {
                Log.InsertarLog(Log.ErrorType.Error, "frmMain2::LimpiarCarpetas", Log.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
            }
        }

        public void GetAccesos()
        {
            if (VariablesGlobales.MODOTRABAJO.Equals("ON"))
            {
                //***Cargar Permisos del Usuario***
                VariablesGlobales.CargarPermisosUsuarioEnLinea(Convert.ToInt32(VariablesGlobales.USER_ROL_ID));
                SetVisibleControl(VariablesGlobales.PERMISO_Enrolamiento, btnEnrolamiento, true);
                SetVisibleControl(VariablesGlobales.PERMISO_Certificados, btnCertificados, true);
                SetVisibleControl(VariablesGlobales.PERMISO_Aprobacion, btnBandejaAprobacion, true);
                SetVisibleControl(VariablesGlobales.PERMISO_Solicitudes, btnBandeja, true);
                SetVisibleControl(VariablesGlobales.PERMISO_Emisiones, btnConsultas, true);
                SetVisibleControl(VariablesGlobales.PERMISO_Revocaciones, btnReportes, true);
                SetVisibleControl(VariablesGlobales.PERMISO_EstadoSolicitudes, btnBandejaCertificados, true);
                SetVisibleControl(VariablesGlobales.PERMISO_ContratoFirmado, btnContratoFirmado, true); 
            }
            else
            {
                VariablesGlobales.CargarPermisosUsuarioFueraDeLinea(Convert.ToInt32(VariablesGlobales.USER_ROL_ID));
                SetVisibleControl(VariablesGlobales.PERMISO_Enrolamiento, btnEnrolamiento, true);
                SetVisibleControl(VariablesGlobales.PERMISO_Certificados, btnCertificados, true);
                SetVisibleControl(VariablesGlobales.PERMISO_Aprobacion, btnBandejaAprobacion, true);
                SetVisibleControl(VariablesGlobales.PERMISO_Solicitudes, btnBandeja, true);
                SetVisibleControl(VariablesGlobales.PERMISO_Emisiones, btnConsultas, true);
                SetVisibleControl(VariablesGlobales.PERMISO_Revocaciones, btnReportes, true);
                SetVisibleControl(VariablesGlobales.PERMISO_EstadoSolicitudes, btnBandejaCertificados, true);
                SetVisibleControl(VariablesGlobales.PERMISO_ContratoFirmado, btnContratoFirmado, true);
            }
           
        }


        #region Timers
        private void TmrInactivityCheck_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                if (VariablesGlobales.IsLocked)
                    return;

                var secondTotalInactivity = TimeSpan.FromSeconds(IdleTimeFinder.GetIdleTime());

                if (secondTotalInactivity.TotalMinutes >= VariablesGlobales.AUTO_LOGOUT_TIME)
                {
                    VariablesGlobales.IsLocked = true;
                    mensajeSesion();
                }
            }
            catch (Exception ex)
            {
                Utilidades.Log.InsertarLog(Utilidades.Log.ErrorType.Error, "frmMain::TmrInactivityCheck_Elapsed", ExodusBcBase.Helper.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);

            }
        }

        private void TmrUpdateLiveTime_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (VariablesGlobales.MODOTRABAJO.Equals("OFF"))
                return;

            //Llamar al método
            var resp = ExodusBcBase.LoginWebService.setTimeLogin(VariablesGlobales.sessionActiveId, VariablesGlobales.HOST_NAME);

            //if (resp)
            //    VariablesGlobales.sessionLastTime = ExodusBcBase.Utilities.getFechaActual();
        }


        //FUnciones de Timers
        private void InactivityCheck()
        {
            //tmrInactivityCheck.Elapsed += TmrInactivityCheck_Elapsed;
            //tmrInactivityCheck.Interval = 30000;
            //tmrInactivityCheck.Start();
        }
        
       
      
        public class CamarasDisponibles
        {
            public int Index { get; set; }
            public string Nombre { get; set; }
        }
        #endregion


        #region ButtonsMenu
     
        void LimpiarFormsActivos()
        {
            var frmcrt = Application.OpenForms.OfType<frmCertificados>().FirstOrDefault(); 
            if (frmcrt != null)
            {
                SetEstiloBotonesInactivos(btnCertificados);
                frmCertificados close = (frmCertificados)Application.OpenForms["frmCertificados"];
                close.CerrarFormCertificados();
            }

            var frmBanApro = Application.OpenForms.OfType<frmBandejaAprobacion>().FirstOrDefault(); 
            if (frmBanApro != null)
            {
                SetEstiloBotonesInactivos(btnBandejaAprobacion);
                frmBandejaAprobacion close = (frmBandejaAprobacion)Application.OpenForms["frmBandejaAprobacion"];
                close.CerrarBandejaAprobacion();
            } 

            var frmBj= Application.OpenForms.OfType<frmBandeja>().FirstOrDefault(); 
            if (frmBj != null)
            {
                SetEstiloBotonesInactivos(btnBandeja);
                frmBandeja close = (frmBandeja)Application.OpenForms["frmBandeja"];
                close.CerrarBandejaSolicitudes();
            }

            var frmBjCertificados = Application.OpenForms.OfType<frmBandejaSeguimientoCertificados>().FirstOrDefault(); 
            if (frmBjCertificados != null)
            {
                SetEstiloBotonesInactivos(btnBandejaCertificados); 
                frmBandejaSeguimientoCertificados close = (frmBandejaSeguimientoCertificados)Application.OpenForms["frmBandejaSeguimientoCertificados"];
                close.CerrarBandejaCertificados();
            }

            var frmEnrolamiento = Application.OpenForms.OfType<frmInsPrimariaEntrada>().FirstOrDefault(); 
            if (frmEnrolamiento != null)
            {
                SetEstiloBotonesInactivos(btnEnrolamiento);
                frmInsPrimariaEntrada close = (frmInsPrimariaEntrada)Application.OpenForms["frmInsPrimariaEntrada"];
                close.CerrarBandejaEnrolamiento();
            }

            var frmContrato = Application.OpenForms.OfType<frmContratoFirmado>().FirstOrDefault();
            if (frmContrato != null)
            {
                SetEstiloBotonesInactivos(btnContratoFirmado);
                frmContratoFirmado close = (frmContratoFirmado)Application.OpenForms["frmContratoFirmado"];
                close.CerrarContratoFirmado();
            }


        }

        private void mniCambiarPassword_Click(object sender, EventArgs e)
        {
            Modulos.Mantenimientos.frmCambioContraseña frm = new Modulos.Mantenimientos.frmCambioContraseña(VariablesGlobales.USER_ID, VariablesGlobales.USER_NAME, true);
            frm.ShowDialog();
        }

        private void mniSalir_Click(object sender, EventArgs e)
        {
            cerrar();
        }

        private void mniBloquear_Click(object sender, EventArgs e)
        {
            mensajeSesion();
        }

        private void btnConsultas_Click(object sender, EventArgs e)
        {
            SetEstiloBotones(btnConsultas);
            Process.Start("IExplore.exe", "https://ar.tecnisign.net/webtrust-web/pages/public/certificate/findcertificateInstall.jsf?skin=SKTEC0001");
            //Process.Start("IExplore.exe", "https://hml-ar-global.validcertificadora.com.br/webtrust-web/pages/public/certificate/findcertificateInstall.jsf?skin=SKTEC0001");
        }

        private void btnReportes_Click(object sender, EventArgs e)
        {
            SetEstiloBotones(btnReportes);
            Process.Start("IExplore.exe", "https://ar.tecnisign.net/webtrust-web/pages/public/certificate/findCertificateRevoke.jsf?skin=SKTEC0001");
            //Process.Start("IExplore.exe", "https://hml-ar-global.validcertificadora.com.br/webtrust-web/pages/public/certificate/findCertificateRevoke.jsf?faces-redirect=true&skin=SKTEC0001");
        }

        #endregion

        private void mensajeSesion()
        {
            Invoke((MethodInvoker)delegate
            {
                frmLocked frm = new frmLocked();
                frm.UserID = VariablesGlobales.USER_ID;
                frm.UserName = VariablesGlobales.USER_NAME;
                frm.Password = VariablesGlobales.USER_PASSWORD;
                var result = frm.ShowDialog();
                if (result == DialogResult.Abort)
                {

                }
            });
            
        }
    
        public void SetVisibleControl(bool visible, SimpleButton ctr,bool enabled)
        {
            if (ctr.InvokeRequired)
            {
                SetVisibleControlCallBack d = new SetVisibleControlCallBack(SetVisibleControl);
                Invoke(d, new object[] { visible, ctr, enabled});
            }
            else
            {
                ctr.Visible = visible;
                ctr.Enabled = enabled;
            }
        }

        public void SetVisibleTileItem(bool visible, TileItem ctr)
        {
            if (InvokeRequired)
            {
                SetVisibleControlTileItem d = new SetVisibleControlTileItem(SetVisibleTileItem);
                Invoke(d, new object[] { visible, ctr });
            }
            else
            {
                ctr.Visible = visible;
            }
        }

        private void cerrar()
        {
            XtraMessageBox.AllowCustomLookAndFeel = true;
            if (confirmToClose)
            {
                if (XtraMessageBox.Show(lookAndFeelMsgBox, "¿Está seguro que desea salir del sistema?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    var InsertarBitacora = RequestCertificate.InsertarRegistro((int)RequestCertificate.TipoEvento.CerrarSesion,
                          VariablesGlobales.USER_ID, (int)RequestCertificate.Modulos.Registro, "Cerro sesión");

                    if (!InsertarBitacora)
                        Log.InsertarLog(Log.ErrorType.Error, "frmMain2::cerrar", "Error al guardar la bitácora", VariablesGlobales.PathDataLog);

                    Application.Exit();
                }
            }
        }

        private void pnlHeader_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void pictureEdit4_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void label1_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void lblHeader_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void lblModoTrabajo_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void lblServicioEnvio_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                ServiceController sc = new ServiceController("GenesysOneSendValid");
                TimeSpan timeout = TimeSpan.FromMilliseconds(30000);
                if (sc != null && sc.Status == ServiceControllerStatus.Stopped) sc.Start();

                sc.WaitForStatus(ServiceControllerStatus.Running, timeout);
                if (sc.Status == ServiceControllerStatus.Running) lblServicioEnvio.Appearance.Image = Properties.Resources.ic_uploadgreen16x16;

                sc.Close();
            }
            catch (Exception ex)
            {
                Utilidades.Log.InsertarLog(Utilidades.Log.ErrorType.Error, "frmMain2::lblServicioEnvio_DoubleClick", ExodusBcBase.Helper.ExtraerExcepcion(ex), VariablesGlobales.PathDataLog);
                XtraMessageBox.Show(frmMain2.lookAndFeelMsgBox, "Error al iniciar servicio", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void SetLabelModoTrabajoText(string text, LabelControl labelEdit)
        {
            if (labelEdit.InvokeRequired)
            {
                SetTextLabelModoTrabajoCallBack d = new SetTextLabelModoTrabajoCallBack(SetLabelModoTrabajoText);
                Invoke(d, new object[] { text, labelEdit });
            }
            else
            {
                labelEdit.Text = text;
                if (text == "En línea")
                {
                    lblEstadoConexion.Appearance.Image = Properties.Resources.enlineaN;
                    lblEstadoConexion.ForeColor = Color.FromArgb(46, 204, 113);
                }
                else
                {
                    lblEstadoConexion.Appearance.Image = Properties.Resources.fueradelineaN;
                    lblEstadoConexion.ForeColor = Color.FromArgb(241, 1, 1);
                }
            }
        }

        public void SetTextLabel(string text, LabelControl lbl)
        {
            if (lbl.InvokeRequired)
            {
                SetTextLabelCallBack d = new SetTextLabelCallBack(SetTextLabel);
                Invoke(d, new object[] { text, lbl });
            }
            else lbl.Text = text;
        }

        public void SetImageLabel(Image img, LabelControl lbl)
        {
            if (lbl.InvokeRequired)
            {
                SetImageLabelCallBack d = new SetImageLabelCallBack(SetImageLabel);
                Invoke(d, new object[] { img, lbl });
            }
            else lbl.Appearance.Image = img;
        }

        private void SetEstiloBotones(SimpleButton btnActivo)
        {
            foreach (Control ctr in flpMenu.Controls)
            {
                if (ctr is SimpleButton)
                {
                    (ctr as SimpleButton).Appearance.BackColor = Color.FromArgb(224, 224, 224);
                    (ctr as SimpleButton).AppearanceHovered.BackColor = Color.Silver;
                    (ctr as SimpleButton).ForeColor = Color.Black;
                }
            }

            btnActivo.Appearance.BackColor = Color.FromArgb(48, 51, 53);
            btnActivo.ForeColor = Color.FromArgb(224, 224, 224); 
            btnActivo.AppearanceHovered.BackColor = Color.FromArgb(48, 51, 53);
        }

        public void SetEstiloBotonesInactivos(SimpleButton btnInaActivo)
        {
            btnInaActivo.Appearance.BackColor = Color.FromArgb(224, 224, 224);
            btnInaActivo.ForeColor = Color.Black;
            btnInaActivo.AppearanceHovered.BackColor = Color.Silver;
            btnInaActivo.AppearancePressed.BackColor = Color.Silver;
        }
     
        private void frmMain2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.P) btnCertificados_Click(sender, e);
        }
       
        private void btnEnrolamiento_Click(object sender, EventArgs e)
        {
            SetEstiloBotones(btnEnrolamiento);
            LimpiarFormsActivos();
            frmForm = Application.OpenForms.OfType<Form>().Where(c => c.Name == "frmInsPrimariaEntrada").SingleOrDefault();
            if (frmForm == null)
            {
                frmInsPrimariaEntrada frm = new frmInsPrimariaEntrada();
                if (frm.DialogResult != DialogResult.Cancel) SetForm(frm);
            }
        }

        private void btnCertificados_Click(object sender, EventArgs e)
        {
            LimpiarFormsActivos();
            SetEstiloBotones(btnCertificados);
            frmForm = Application.OpenForms.OfType<Form>().Where(c => c.Name == "frmCertificados").SingleOrDefault();
            if (frmForm == null)
            {
                frmAdjuntos frmA = new frmAdjuntos();
                frmAsociarRepresentantes frmRep = new frmAsociarRepresentantes();
                frmCertificados frm = new frmCertificados(frmA, frmRep);
                SetForm(frm);
            }
        }

        private void btnBandejaAprobacion_Click(object sender, EventArgs e)
        {
            ssForm.ShowWaitForm();
            LimpiarFormsActivos();
            SetEstiloBotones(btnBandejaAprobacion);
            frmForm = Application.OpenForms.OfType<Form>().Where(c => c.Name == "frmBandejaAprobacion").SingleOrDefault();
            if (frmForm == null)
            {
                frmBandejaAprobacion frm = new frmBandejaAprobacion(); 
                SetForm(frm);
            }
            ssForm.CloseWaitForm();
        }

        private void btnBandeja_Click(object sender, EventArgs e)
        {
            ssForm.ShowWaitForm();
            LimpiarFormsActivos();
            SetEstiloBotones(btnBandeja);
            frmForm = Application.OpenForms.OfType<Form>().Where(c => c.Name == "frmBandeja").SingleOrDefault(); 
            if (frmForm == null)
            {
                frmBandeja frm = new frmBandeja();
                SetForm(frm);
            }
            ssForm.CloseWaitForm();
        }

        void SetForm(XtraForm form)
        {
            pnlBienvenida.Visible = false;
            form.TopLevel = false;
            form.Dock = DockStyle.Fill;
            form.Parent = MdiParent;
            pnlContenedor.Controls.Add(form);
            form.Show();
        }

        bool ValidarPermisos()
        {
            var agAutorizado = REGISTRATIONAGENTLN.GetCodeAgent(Convert.ToInt32(VariablesGlobales.USER_ID));
            if (agAutorizado != null)
            {
                VariablesGlobales.AGAUTORITY = agAutorizado.Code;
                VariablesGlobales.REGISTRATION_AGENT_ID = Convert.ToInt32(agAutorizado.RegistrationAgentId);
                return true;
            }
            else return false;
        }

        private void btnBandejaCertificados_Click(object sender, EventArgs e)
        {
            ssForm.ShowWaitForm();
            LimpiarFormsActivos();
            SetEstiloBotones(btnBandejaCertificados);
            frmForm = Application.OpenForms.OfType<Form>().Where(c => c.Name == "frmBandejaSeguimientoCertificados").SingleOrDefault(); 
            if (frmForm == null)
            {
                frmBandejaSeguimientoCertificados frm = new frmBandejaSeguimientoCertificados();
                SetForm(frm);
            }
            ssForm.CloseWaitForm();
        }

        private void btnContratoFirmado_Click(object sender, EventArgs e)
        {
            ssForm.ShowWaitForm();
            LimpiarFormsActivos();
            SetEstiloBotones(btnContratoFirmado);
            frmForm = Application.OpenForms.OfType<Form>().Where(c => c.Name == "frmContratoFirmado").SingleOrDefault();
            if (frmForm == null)
            {
                frmContratoFirmado frm = new frmContratoFirmado(); 
                SetForm(frm);
            }
            ssForm.CloseWaitForm();
        }

        private void picCambiarPassword_Click(object sender, EventArgs e)
        {
            frmCambioContraseña frm = new frmCambioContraseña(VariablesGlobales.USER_ID, VariablesGlobales.USER_NAME, true);
            frm.ShowDialog();
        }
    }
}