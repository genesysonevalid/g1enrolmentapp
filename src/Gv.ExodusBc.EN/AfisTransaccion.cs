﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.EN
{
    public class AfisTransaccion
    {
        public enum ValidacionAfis
        {
            NO_INICIADO = 0,
            ENVIANDO_NIST = 1,
            NIST_ENVIADO = 2,
            ERROR_ENVIO_NIST = 3,
            ESPERANDO_RESPUESTA = 4,
            COMPLETADO = 5,
            TIMEOUT = 6
        }
        public string TransNo { get; set; }
        public string TOT { get; set; }
        public string ExternalID { get; set; }
        public string Status { get; set; }
        public string ConfirmStatus { get; set; }
        public string CAN_TransNo { get; set; }
        public string CAN_ExternalId { get; set; }
        public string CAN_TCN { get; set; }
        public string CAN_LastName { get; set; }
        public string CAN_FirstName { get; set; }
        public string CAND_ID1 { get; set; }
        public string CAN_SID { get; set; }
        public string CAN_Sex { get; set; }
        public string CAN_DateOfBirth { get; set; }
        public string CAN_TOT { get; set; }
        public string CAND_POB { get; set; }
    }
}
