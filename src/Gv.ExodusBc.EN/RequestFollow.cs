﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.EN
{
    public class RequestFollow
    {
        public DateTime CreationDate { get; set; }
        public string Observation { get; set; } 
        public int FKRequestId { get; set; }
        public int FKTransactionEstatusId { get; set; }
        public int FKCreateUserId { get; set; }
        public int FKOfficeId { get; set; }
        public int FKTransactionStatusPrevious { get; set; }
        public int FKWorkstationId { get; set; }

    }
}
