﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.EN
{
    public class JsonItem
    {
        public JsonItem(){}
        public JsonItem(string id, string val)
        {
            this.id = id;
            this.val = val;
        }
        public string id { set; get; }
        public string val { set; get; }
    }
}
