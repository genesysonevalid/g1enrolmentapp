﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.EN
{
    public class RequestAttach
    {
        public int Numero { get; set; }
        public string NameDocument { get; set; }
        public string Folder { get; set; }
        public string Type { get; set; }
        public string Size { get; set; }
        public DateTime CreationDate { get; set; }
        public int TipoPersona { get; set; }
        public int FKRequestId { get; set; }
        public int FKDocumentType { get; set; }
        public int FKCreateUserId { get; set; }
        public byte[] Imagen { get; set; }

    }
}
