﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.EN
{
    public class EnrolmentsHuellas
    {
        public EnrolmentsHuellas() { }
        public EnrolmentsHuellas(int dedo, int estado, int calidad, string wsq)
        {
            this.dedo = dedo;
            this.estado = estado;
            this.calidad = calidad;
            this.wsq = wsq;
        }
        public int dedo { set; get; }
        public int estado { set; get; }
        public int calidad { set; get; }
        public string wsq { set; get; }
    }
}
