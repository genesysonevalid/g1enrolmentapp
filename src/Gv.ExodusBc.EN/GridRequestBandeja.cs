﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.EN
{
    public class GridRequest
    {
        public string IDENTIFICACION { get; set; }
        public string TICKET { get; set; }
        public string PRODUCTO { get; set; }
        public string NOMBRES { get; set; }
        public string APELLIDOS { get; set; }
        public string SEGUIMIENTO { get; set; }
        public string FACTURA { get; set; }
        public int IDSTATUS { get; set; }
        public int RequestId { get; set; }

    }

    public class GridRequestBandeja
    {
        public string TICKET { get; set; }
        public string PRODUCTO { get; set; }
        public string NOMBRES { get; set; }
        public string APELLIDOS { get; set; }
        public string SEGUIMIENTO { get; set; }
        public string FACTURA { get; set; }
        public int IDSTATUS { get; set; }
        public int TRANSACCIONSTATUS { get; set; }
        public int RequestId { get; set; }
        public DateTime FECHACREACION { get; set; }
    }
    public class GridRequestRevision
    {
        public string IDENTIFICACION { get; set; }
        public string TICKET { get; set; }
        public string PRODUCTO { get; set; }
        public string NOMBRES { get; set; }
        public string APELLIDOS { get; set; }
        public string SEGUIMIENTO { get; set; }
        public string FACTURA { get; set; }
        public int IDSTATUS { get; set; }
        public int RequestId { get; set; }
        public int PersonId { get; set; }
        public string Observacion { get; set; }

    }


    public class GridRequestStatusCertificados
    {
        public string NUMEROTICKET { get; set; }
        public string PRODUCTO { get; set; }
        public string NOMBRES { get; set; }
        public string APELLIDOS { get; set; }
        public string SEGUIMIENTO { get; set; }
        public string OFICINA { get; set; }
        public DateTime FECHACREACION { get; set; }
        public int TRANSACCIONSTATUSTID { get; set; }
        public int RequestId { get; set; }
    }

    public class GridSeguimientoCertificados
    {
        public string FECHAREGISTRO { get; set; }
        public string USUARIO { get; set; }
        public string OFICINA { get; set; }
        public string ESTACION { get; set; }
        public string OBSERVACION { get; set; }
    }

}
