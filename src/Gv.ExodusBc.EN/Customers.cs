﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.EN
{
    public class Customers
    {
        public string DocumentNumber { get; set; }
        public string Names { get; set; }
        public string LastNames { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public string PersonalMovilPhone { get; set; }
        public string WorkPhone { get; set; }
        public DateTime CreationDate { get; set; }
        public int FKPersonType { get; set; }
        public int FKCreateUserId { get; set; }


    }
}
