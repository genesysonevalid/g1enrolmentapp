﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.EN
{
    public class AddRepresentatives
    {
        public int Number { get; set; }
        public string DocumentNumber { get; set; }
        public string Names { get; set; }
        public string LastNames { get; set; }
        public string Nationality { get; set; }
        public int RequestId { get; set; }
        public int UserId { get; set; }
        public byte[] Foto { get; set; }
    }
}
