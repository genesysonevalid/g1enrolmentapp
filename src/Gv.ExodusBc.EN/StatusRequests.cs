﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.EN
{
    public class StatusRequests
    {
        public enum EstadoSolicitudes
        {
            TICKETENVIADO = 1,
            TICKETGENERADO = 2,
            SOLICITUDENVIADA = 3,
            SOLICITUDGENERADA = 4,
            FALLAENVIO = 5
        }
    }
}
