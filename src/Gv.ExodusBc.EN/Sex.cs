﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.EN
{
    public class Sex
    {
        public enum SexoIndex
        {
            FEMENINO = 1,
            MASCULINO = 2
        }

        public enum SexoCode
        {
            F = '1',
            M = '2'
        }

     }
  
}
