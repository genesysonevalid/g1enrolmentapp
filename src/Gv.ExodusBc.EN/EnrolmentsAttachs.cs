﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.EN
{
    public class EnrolmentsAttachs
    {
        public int Numero { get; set; }
        public string Nombre { get; set; }
        public string Type { get; set; }
        public int FKDocumentType { get; set; }
        public string Size { get; set; }
        public byte[] Documento { get; set; }
        public DateTime CreationDate { get; set; }
        public int FKCreateUserId { get; set; }
    }
}
