﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.EN
{
    public class RequestFollowValid
    {
        public DateTime CreationDate { get; set; }
        public string Observation { get; set; }
        public int FKRequestId { get; set; }
        public int FKRequestStatusId { get; set; }
        public int FKCreateUserId { get; set; }
        public int FKOfficeId { get; set; }
        public int FKRequestStatusPreviousId { get; set; }
        public int FKWorkstationId { get; set; }

    }
}
