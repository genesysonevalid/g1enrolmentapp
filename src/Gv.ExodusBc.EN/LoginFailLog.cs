//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Gv.ExodusBc.EN
{
    using System;
    using System.Collections.Generic;
    
    public partial class LoginFailLog
    {
        public long LoginFailId { get; set; }
        public long FailCount { get; set; }
        public System.DateTime LastFailTime { get; set; }
        public long FKUserId { get; set; }
    }
}
