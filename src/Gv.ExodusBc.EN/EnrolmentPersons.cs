﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.EN
{
    public class EnrolmentPersons
    {
    
        public string FirstName { set; get; }
        public string SecondName { set; get; }
        public string FirstLastName { set; get; }
        public string SecondLastName { set; get; }
        public DateTime DateOfBirth { set; get; }
        public DateTime CreationDate { set; get; }
        public DateTime ModifyDate { set; get; }
        public string PersonalMovilPhone { set; get; }
        public string HomePhone { set; get; }
        public string Address { set; get; }
        public string WorkPhone { set; get; }
        public string WorkAddress { set; get; }
        public string PersonalEmail { set; get; }
        public string WorkEmail { set; get; }
        public int FKSexId { set; get; }
        public int FKCivilStatusId { set; get; }
        public int FKPersonProfessionsId { set; get; }
        public int FKEducationLevelId { set; get; }
        public int FKStateCityId { set; get; }
        public int FKStateCountryId { set; get; }
        public int FKNeighborhoodId { set; get; }
        public int FKNationalityId { set; get; }
        public int FKCatalogStatusId { set; get; }
        public int FKCountryId { set; get; }
        public int FKCountryResidenceId { set; get; }
        public int FKCreateUserId { set; get; }
        public  bool AfisHuellasHit { get; set; }
        public string hitPersonId { get; set; }

    }
}
