﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.EN
{
    public class EnrolmentTemplate
    {
        public EnrolmentTemplate() { }
        public EnrolmentTemplate(int index, int status, int score, string wsq)
        {
            this.index = index;
            this.status = status;
            this.score = score;
            this.wsq = wsq;
        }
        public int index { set; get; }
        public int status { set; get; }
        public int score { set; get; }
        public string wsq { set; get; }
    }
}
