﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.EN
{
    public class Bitacoras
    {
        public DateTime FechaRegistro { get; set; }
        public string Detalle { get; set; }
        public string RegistroXmlOld { get; set; }
        public string RegistroXmlNew { get; set; }
        public int FKEventoId { get; set; }
        public int FKUsuarioId { get; set; }
        public int FKModuloId { get; set; }
    }
}
