﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.EN
{
    public class Imagen
    {
        public Imagen()
        {
        }

        public Imagen(byte[] foto, DateTime fecha)
        {
            Foto = foto;
            Fecha = fecha;
        }

        public Imagen(byte[] foto, DateTime fecha, char tipo)
        {
            Foto = foto;
            Fecha = fecha;
            Tipo = tipo;
        }

        public byte[] Foto { set; get; }
        public DateTime Fecha { set; get; }
        public char Tipo { set; get; }
    }
}
