﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.EN
{
    public class Dossie
    {
        public string Archivo { get; set; }
        public string Fecha { get; set; }
        public string Descripcion { get; set; }
        public string Hash { get; set; }
    }
}
