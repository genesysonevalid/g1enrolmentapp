﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.EN
{
    public class Attached
    {
        public int Numero { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Size { get; set; }
        public int FKDocumentType { get; set; }
        public byte[] Documento { get; set; }
        public int TipoPersona { get; set; }
    }
}
