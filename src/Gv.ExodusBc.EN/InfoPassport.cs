﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.EN
{
    public class InfoPassport
    {
        public string tipoDocumento { set; get; }
        public string paisEmisor { set; get; }
        public string numDocumento { set; get; }
        public string fechaVencimiento { set; get; }
        public string nombres { set; get; }
        public string apellidos { set; get; }
        public string fechaNacimiento { set; get; }
        public string sexo { set; get; }
        public string nacionalidad { set; get; }
        public string imgNormal { set; get; }
        public string imgInfraroja { set; get; }
        public string imgUltravioleta { set; get; }
    }
}
