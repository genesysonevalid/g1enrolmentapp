﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.EN
{

    public class Request
    {
        public enum RequestStatusList
        {
            TRANSACCIONOINICIADA = 0, //Para los casos en los que la transaccion sea nula, porque no se ha iniciado en valid
            PENDIENTESOLICITUD = 1,
            PENDIENTEVALIDACION = 2,
            SOLICITUDCOMPROBADA = 4,
            CERTIFICADOEMITIDO = 5,
            CERTIFICADOINSTALADO = 6,
            CERTIFICADORECHAZADOVALIDACION = 8,
            CERTIFICADOREVOCADOATITULAR = 12,
            CERTIFICADOREVOCADOAGENTE = 13,
            CERTIFICADOEMITIDOPENDIENTEINSTALACION = 15,
            SOLICITUDRECHAZADA = 16,
        }


        public enum TransactionStatus
        {
            SOLICITUDCREADA = 1,
            SOLICITUDAPROBADA = 2,
            SOLICITUDRECHAZADA = 3,
            SOLICITUDENREVISION = 4,
            SOLICITUDFINALIZADA = 5,
            FACTURAENVIADA = 6,
            FACTURAPAGADA = 7,
            FACTURAANULADA = 8,
            SOLICITUDREENVIADA = 9,
        }


        public enum TypePerson
        {
            PRSNATURAL = 1,
            PRSJURIDICA = 2,
            PROFTITULADO = 3
        }

        public enum TransactionType
        {
            GENERATETICKET = 1,
            GENERATEREQUEST = 2
        }


        public int RequestId { get; set; }
        public string TicketNumber { get; set; }
        public string DocumentNumberNaturalPerson { get; set; }
        public string Names { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public int ZipCode { get; set; }
        public string CellPhonePersonal { get; set; }
        public string PhoneWork { get; set; }
        public string PhoneHome{ get; set; }
        public string NameLegalPerson { get; set; }
        public string DocumentNumberLegalPerson { get; set; }
        public string LegalPersonPhone { get; set; }
        public string LegalPersonEmail { get; set; } 
        public string DocumentNumberCustomer { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public string PasswordClient { get; set; }
        public string FacturaManual { get; set; }
        public string Profession { get; set; }
        public string RegistrationNumber { get; set; }
        public string InstitutionIssues { get; set; }
        public int FKDocumentTypeNaturalPerson { get; set; }
        public int FKDocumentTypeLegalPerson { get; set; }
        public int FKCountryId { get; set; }
        public int FKCountryResidenceId { get; set; }
        public int FKStateCountryId { get; set; }
        public int FKStateCityId { get; set; }
        public int FKNeighborhoodId { get; set; }
        public long FKEnrolmentId { get; set; }
        public int FKPersonId { get; set; }
        public int FKCatalogStatusId { get; set; }
        public int FKCreateUserId { get; set; }
        public int FKRequestStatusId { get; set; }
        public int FKTransactionStatusId { get; set; }
        public int FKProductId { get; set; }
        public int FKOfficeId { get; set; }
        public int FKCertificateOfficeId { get; set; }
        public int FKRegistrationAuthorityId { get; set; }
        public int FKRegistrationAgentId { get; set; }
        public int FKWorkStationId { get; set; }
        public int FKPersonTypeId { get; set; }

    }
}
