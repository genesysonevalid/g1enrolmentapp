﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.EN
{
    public class ActualizarInformacionEN
    {
        public int PersonId { get; set; }
        public long EnrolmentId { get; set; }
        public byte[] Foto { set; get; }
        public string TipoDocumento { get; set; }
        public string NumeroDocumento { get; set; }
        public string PrimerNombre { get; set; }
        public string SegundoNombre { get; set; }
        public string PrimerApellido { get; set; }
        public string SegundoApellido { get; set; }
        public string Sexo { get; set; }
        public string Pais { get; set; }
        public string Nacionalidad { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public string EstadoCivil { get; set; }
        public string Departamento { get; set; }
        public string Municipio { get; set; }
        public string Colonia { get; set; }
        public string DireccionPersonal { get; set; }
        public string Movil { get; set; }
        public string TelefonoCasa { get; set; }
        public string Correo { get; set; }
        public string Profesion { get; set; }
        public string NivelEducativo { get; set; }
        public string TelefonoTrabajo { get; set; }
        public string DireccionTrabajo { get; set; }
        public string CorreoTrabajo { get; set; }
        public byte[] Firma { set; get; }
    }
}
