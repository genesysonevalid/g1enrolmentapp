﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.EN
{
    public class RepresentativeRequest
    {
        public int Numero { get; set; }
        public string DocumentNumber { get; set; }
        public string Names { get; set; }
        public string LastNames { get; set; }
        public string Nationality { get; set; }
        public DateTime CreationDate { get; set; }
        public int FKRequestId { get; set; }
        public int FKCreateUserId { get; set; }
        public byte[] Foto { get; set; }

    }
}
