﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.EN
{
    public class Enrolments
    {
        public string sequenceEnrolment { set; get; }
        public string DocumentNumber { set; get; }
        public int FPCaptured { set; get; }
        public int Attachments { set; get; }
        public bool Signature { set; get; }
        public bool RegisteredAfis { set; get; }
        public DateTime CreationDate { set; get; }
        public DateTime ModifyDate { set; get; }
        public int FKDocumentType { set; get; }
        public int FKPersonId { set; get; }
        public int FKWorkStationId { set; get; }
        public int FKCreationUserId { set; get; }
        public int FKOficinaId { set; get; }
        public bool NoPresential { get; set; }

    }
}
