//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Gv.ExodusBc.EN
{
    using System;
    using System.Collections.Generic;
    
    public partial class WorkStations
    {
        public long WorkStationsId { get; set; }
        public string HostName { get; set; }
        public string IPAddress { get; set; }
        public string Code { get; set; }
        public string Details { get; set; }
        public System.DateTime CreationDate { get; set; }
        public System.DateTime ModifyDate { get; set; }
        public long FKOfficeId { get; set; }
        public long FKCatalogStatusId { get; set; }
        public long FKCreateUserId { get; set; }
    }
}
