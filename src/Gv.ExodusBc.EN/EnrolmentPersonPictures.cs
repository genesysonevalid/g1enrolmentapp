﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.EN
{
    public class EnrolmentPersonPictures
    {
        public string Image { set; get; }
        public DateTime CreationDate { set; get; }
       
    }
}
