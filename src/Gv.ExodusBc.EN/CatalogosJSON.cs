﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.EN
{
    public class CatalogosJSON
    {
        public List<Catalogs> Catalogs { get; set; }
        public List<CatalogStatus> CatalogStatus { get; set; }
        public List<CatalogTypes> CatalogTypes { get; set; }
        public List<CertificateOffice> CertificateOffice { get; set; }
        public List<Country> Country { get; set; }
        public List<DetailLicense> DetailLicense { get; set; }
        public List<DocumentType> DocumentType { get; set; }
        public List<DocumentTypePerson> DocumentTypePerson { get; set; }
        public List<License> License { get; set; }
        public List<LoginFailLog> LoginFailLog { get; set; }
        public List<ModuleOptions> ModuleOptions { get; set; }
        public List<Modules> Modules { get; set; }
        public List<Nationality> Nationality { get; set; }
        public List<Neighborhood> Neighborhood { get; set; }
        public List<Office> Office { get; set; }
        public List<OfficeGP> OfficeGP { get; set; }
        public List<PersonProfessions> PersonProfessions { get; set; }
        public List<PersonType> PersonType { get; set; }
        public List<Products> Products { get; set; }
        public List<ProfessionalAssociations> ProfessionalAssociations { get; set; }
        public List<ReasonRejection> ReasonRejection { get; set; }
        public List<ReasonReview> ReasonReview { get; set; }
        public List<RegistrationAgent> RegistrationAgent { get; set; }
        public List<RegistrationAuthority> RegistrationAuthority { get; set; }
        public List<RequestStatus> RequestStatus { get; set; }
        public List<Settings> Settings { get; set; }
        public List<StateCity> StateCity { get; set; }
        public List<StateCountry> StateCountry { get; set; }
        public List<TransactionStatus> TransactionStatus { get; set; }
        public List<UserPermissions> UserPermissions { get; set; }
        public List<UserRoles> UserRoles { get; set; }
        public List<Users> Users { get; set; }
        public List<WebServiceTokens> WebServiceTokens { get; set; }
        public List<WorkStations> WorkStations { get; set; }
        public List<ZipCodes> ZipCodes { get; set; }
    }
}
