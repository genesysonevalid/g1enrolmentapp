﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gv.ExodusBc.EN
{
    public class EnrolmentSignature
    {
        public string Image { set; get; }
        public int FKCatalogStatusId { get; set; }
        public DateTime CreationDate { set; get; }
        public string Observation { get; set; }
        public int FKCreateUserId { get; set; }
    }
}
